#!/bin/bash

#pdftoppm -f 92 -l 92 ../Apple\ IIe\ Reference\ Manual.pdf a.png
#tesseract a.png-092.ppm z.txt

reset # Clears TUI's mess
TMP=/mnt/data2/tmp/rusty.log
#/usr/bin/python3 data/rustgen6502.py

TARGET=$1 # --release or nothing

# cargo build

if [ $? -eq 0 ]; then
    CHOPLIFTER=/mnt/data2/roms/Apple/choplifter.do
    # /mnt/data2/roms/Apple/skyfox.dsk
    # /mnt/data2/roms/Apple/karateka.dsk
    # "Robotron 2084 (4am crack).dsk"
    # Lode Runner.dsk
    GALAXIAN="/mnt/data2/roms/Apple/Galaxian (Thunder Mountain) (4am crack).dsk"
    # /mnt/data2/roms/Apple/BASIC Sample Disk.dsk

    # Speaker --------------------------------------------------------
    BUG_ATTACK="additional/games/Bug Attack.woz"
    #BUG_ATTACK="additional/games/Bug Attack (1981)(Cavalier Computer).dsk"
    #--script "WAIT_UNTIL 3, SPEAKER_REC_START, WAIT_UNTIL 18, SPEAKER_REC_STOP bug_attack,QUIT"

    BOULDER="/mnt/data2/roms/Apple/Boulder Dash (4am and san inc crack)/Boulder Dash (4am and san inc crack).dsk"
    #ARCHON=/mnt/data2/roms/Apple/archon_i.dsk
    ARCHON=additional/games/Archon.woz
    # --script "WAIT_UNTIL 7, VIDEO_REC_START archon, KEY_SPACE,WAIT_UNTIL 8, KEY_SPACE,WAIT_UNTIL 9, KEY_SPACE, VIDEO_REC_STOP, WAIT_UNTIL 10, KEY_SPACE, WAIT_UNTIL 17, SPKR_REC_START, WAIT_UNTIL 20, SCREENSHOT archon, WAIT_UNTIL 48, SPKR_REC_STOP archon, QUIT"
    # ffmpeg -y -framerate 50 -i /tmp/vidrec%05d.png -pix_fmt yuv420p -vf "scale=560:384:flags=neighbor"  -r 50 -profile:v baseline -preset slow -crf 10 -shortest /mnt/data2/tmp/lowtech.mp4

    # This to make 2K video which are digested correctly by YouTube.
    # You may have to set the settings in YouTube to see it a 720p 50fps though.
    # 1400*1080 was set as to maximize the size in the video whil preserving aspect ratio
    # The ratio was set manually.
    # I add padding to keep the 2048*1080 2K resolution (my goal).
    # -shortest is to add soundtrack (when available)
    # ffmpeg -y -i /tmp/accurapple.wav -framerate 50 -i /tmp/picka_%05d.png -pix_fmt yuv420p -vf "scale=1400:1080:flags=neighbor,pad=2048:1080:(ow-iw)/2:(oh-ih)/2"  -r 50 -profile:v baseline -preset slow -crf 10 -shortest cc.mp4

    POP="additional/games/prince of persia side a (san inc crack).dsk"
    POP="additional/games/Prince of Persia side A.woz"



    # /mnt/data2/roms/Apple/Electric Duet (clean crack).dsk
    CONAN="/mnt/data2/roms/Apple/Conan (4am crack) side A.dsk"
    SEA_DRAGON="/mnt/data2/roms/Apple/seadragon (krakowicz).dsk"
    DARK_LORD="/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/adventure/dark_lord/dark_lord_1.dsk"
    # "WAIT_UNTIL 7,SPKR_REC_START,VIDEO_REC_START darklord,WAIT_UNTIL 57,VIDEO_REC_STOP,SPKR_REC_STOP darklord,QUIT"
    WIND_WALKER=/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/rpg/wind_walker/wind_walker_1.dsk
    # /mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/action/Ballblazer (4am crack).dsk
    MIAMI=/mnt/data2/roms/Apple/Miami_Sound_Machine3_Dckd.dsk
    MI2="/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/action/impossible_mission/Impossible Mission II (4am crack) side A.dsk"
    ROBOCOP="/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/action/robocop/robocop1.dsk"
    SSBB="/mnt/data2/roms/Apple/asi_games/sports/street_sports/baseball/Street Sport Baseball (Digital Gang crack).dsk"
    RTSYNTH="/mnt/data2/roms/Apple/RT.SYNTH.DSK"
    # "WAIT_UNTIL 7, KEY_SPACE, WAIT_UNTIL 18, SPKR_REC_START,VIDEO_REC_START ssbb, WAIT_UNTIL 127, VIDEO_REC_STOP,SPKR_REC_STOP ssbb, QUIT"
    PICKA=additional/games/pick_a_dilly_pair.dsk


    GHOSTBUSTERS="/mnt/data2/roms/Apple/asi_games/action/ghostbusters.dsk"
    # --script "WAIT_UNTIL 20, SPKR_REC_START, WAIT_UNTIL 60, SPKR_REC_STOP ghostbsuter.bin, QUIT"

    GIJOE="/mnt/data2/roms/Apple/asi_games/action/G.I. Joe (4am crack) side A.dsk"

    # DHGR -----------------------------------------------------------
    BRUCE="/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/action/bruce_lee/Bruce Lee (enhanced by Raven).dsk"
    SPY="/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/action/spy_vs_spy/spy_vs_spy_3.dsk"
    OBSCOZ="additional/games/mario.do"

    # Mockingboard ---------------------------------------------------
    # Various ways to initalize the IRQ's
    LOWTECH=additional/LOWTECH.WOZ
    PT3=/mnt/data2/roms/Apple/pt3_player.dsk
    SKYFOX=/mnt/data2/roms/Apple/skyfox_mb.dsk
    # Mad2 uses both AY's
    LATECOMER=/mnt/data2/roms/Apple/latecomer_finalv2/latecomer_finalv2.dsk
    CHP=additional/CHP.dsk
    MAD2=additional/MAD2.dsk
    MAD3=additional/MAD3.dsk
    MADEF=additional/MADEF.dsk
    TL3=/mnt/data2/roms/Apple/TL3.dsk
    OLDSKOOL=/mnt/data2/roms/Apple/demo/oldskool.dsk
    ANSI=additional/AS-S1.dsk

    # 80 COLUMNS -----------------------------------------------------
    MAGIC_SLATE=/home/stefan/Downloads/magicslate.dsk

    # Cycles counting ------------------------------------------------
    # "SOUND_REC_START,VIDEO_REC_START fastloader,WAIT_UNTIL 18, KEYS Q, WAIT_UNTIL 80, KEYS Q, WAIT_UNTIL 85, VIDEO_REC_STOP,SOUND_REC_STOP fastloader,QUIT"
    CC=/home/stefan/Projects/accurapple/additional/CC.dsk
    DD2=/home/stefan/Projects/accurapple/additional/DD2.woz

    # WOZ test images
    # Work
    WT_DOS33="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/DOS 3.3 System Master.woz"
    WT_BLAZING_PADDLES="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Blazing Paddles (Baudville).woz"
    WT_KAMUNGAS="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Bouncing Kamungas - Disk 1, Side A.woz"
    WT_FIRST_MATH="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/First Math Adventures - Understanding Word Problems.woz"
    WT_HARD_HAT="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Hard Hat Mack - Disk 1, Side A.woz"
    WT_SAMMY="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Sammy Lightfoot - Disk 1, Side A.woz"
    WT_STARGATE="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Stargate - Disk 1, Side A.woz"
    WT_TAKE1="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Take 1 (Baudville).woz"
    WT_DINO="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Dino Eggs - Disk 1, Side A.woz"
    WT_RESCUE_RAIDERS="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Rescue Raiders - Disk 1, Side B.woz"
    WT_BILESTOAD="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/The Bilestoad - Disk 1, Side A.woz"
    WT_COMMANDO="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Commando - Disk 1, Side A.woz"
    WT_PRINTSHOP="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/The Print Shop Companion - Disk 1, Side A.woz"
    # Choplifter needs Apple2e rom, not the xtended one
    WOZ_CHOPLIFTER="/mnt/data2/torrents/wozaday_Choplifter/Choplifter (woz-a-day collection)/Choplifter.woz"
    WT_STICKYBEAR="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Stickybear Town Builder - Disk 1, Side A.woz"
    WIZARDRY=/mnt/data2/roms/Apple/Wizardry-\ Proving\ Grounds\ of\ the\ Mad\ Overlord\ v05-SEP-81\ \(woz-a-day\ collection\)/Wizardry-\ Proving\ Grounds\ of\ the\ Mad\ Overlord\ v05-SEP-81\ side\ B\ -\ boot.woz
    TEST_DRIVE="/mnt/data2/roms/Apple/woz test images/FromSlack/Test Drive side A.woz"
    COMICS_A="/mnt/data2/roms/Apple/woz test images/FromSlack/Accolade's Comics - A.woz"
    WT_MINER="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Miner 2049er II - Disk 1, Side A.woz"
    WT_PLANET="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Planetfall - Disk 1, Side A.woz"
    WT_CRISIS="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Crisis Mountain - Disk 1, Side A.woz"

    # Flux
    WT_PRODOS="/mnt/data2/roms/Apple/woz test images/WOZ 2.1 (with FLUX chunks)/ProDOS User's Disk - Disk 1, Side A.woz"
    WOZ_BANDITS="/mnt/data2/roms/Apple/woz test images/Bandits.woz"
    WOZ_FLYWARS="/mnt/data2/roms/Apple/woz test images/Fly Wars.woz"
    WOZ_CYCLOD="/mnt/data2/roms/Apple/woz test images/Cyclod.woz"
    WOZ_LEMMINGS="/mnt/data2/roms/Apple/woz test images/Lemmings.woz"
    WOZ_JELLYFISH="/mnt/data2/roms/Apple/woz test images/Jellyfish.woz"
    WOZ_MINOTAUR="/mnt/data2/roms/Apple/woz test images/Minotaur.woz"
    WOZ_LODE_RUNNER="/mnt/data2/roms/Apple/woz test images/Lode Runner.woz"
    WOZ_SUNDOG="/mnt/data2/roms/Apple/woz test images/Sundog Frozen Legacy v2.0 - Utility Side (boot).woz"
    WOZ_SUNDOG2="/mnt/data2/roms/Apple/woz test images/Sundog Frozen Legacy v2.0 - Play Side.woz"

    # But doesn't change resolution in lemonade stand
    WT_APPLE_AT_PLAY="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/The Apple at Play.woz"

    # fail
    WT_DOS32="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/DOS 3.2 System Master.woz"

    # Dunno
    # needs joystick

    # Untested
    WT_WOF1="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Wings of Fury - Disk 1, Side A.woz"
    WT_WOF2="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Wings of Fury - Disk 1, Side B.woz"
    WT_BZONE1="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Border Zone - Disk 1, Side A.woz"
    WT_BZONE2="/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Border Zone - Disk 1, Side B.woz"

    # Bugged

    # This bugs because of some mem page set up I think. Code starts
    # at $6000 and breaks at $6016 (or righ after, when the IRQ
    # triggers. Note that the IRQ triggers *BUT* the meulator has non
    # registered, so maybe it's an issue of when I handle SEI/CLI
    # instructions)

    OUTLINE="additional/outline2021.dsk"


    echo "-----------------------------------------------------------\n"
    date
    echo "-----------------------------------------------------------\n"


    # wozpack=error,accurapple::diskii=error,
    export RUST_LOG="accurapple=info"

    #export RUST_LOG="accurapple=info,accurapple::gfx_tv=info,accurapple::emulator=info,accurapple::a2=info,accurapple::speaker=error,accurapple::sound=error,accurapple::mos6522=info,accurapple::diskii_stepper=trace,accurapple::diskii=trace,accurapple::wozpack=debug,accurapple::main=info"

    export RUST_LOG="accurapple=info,accurapple::sound=debug"

    # --script "PAUSE, LOAD_MEM additional/test 800, SET_PC 800, RUN" --symbols additional/symbols
    #RUST_BACKTRACE=full cargo run $TARGET -- --floppy1 "$RTSYNTH" --script $'WAIT_UNTIL 12, KEYS 1, WAIT_UNTIL 15, KEYS PACK1.VP§, WAIT_UNTIL 21, KEYS Y§, WAIT_UNTIL 22, KEYS START.MUSIC§, WAIT_UNTIL 29, SPKR_REC_START, KEYS P, WAIT_UNTIL 33, SPKR_REC_STOP rtsynth.bin' # KEYS Y
    # --script "LOAD_MEM additional/scott.bin 300, SET_PC 300, RUN"
    # Test drive --script "BP_CYCLE 15316263, TURBO_START, WAIT_UNTIL 10, KEYS Q, WAIT_UNTIL 13, TURBO_STOP"
    # /home/stefan/Downloads/fastloader(1).woz
    # "/mnt/data2/roms/Apple/demo/oldskool.dsk" --script "SOUND_REC_START,VIDEO_REC_START fastloader, WAIT_UNTIL 6, KEYS BRUN~OLDSKOOL§, WAIT_UNTIL 65, VIDEO_REC_STOP,SOUND_REC_STOP fastloader,QUIT"

    # /mnt/data2/roms/Apple/demo/TRIBU.dsk
    # /home/stefan/Downloads/MineComp200721.do
    # RUST_BACKTRACE=full cargo run $TARGET -- --floppy1 "composite/NEW.DSK" # --script "WAIT_UNTIL 6, KEYS BRUN~OLDSKOOL§, WAIT_UNTIL 15.4, PAUSE, SAVE_MEM additional/oldskool.hgr 2000 4000, SAVE_MEM additional/oldskool.gr 400 800"
    #RUST_BACKTRACE=full cargo run $TARGET -- --symbols "additional/symbols.txt" --floppy1 "$POP" --script "WAIT_UNTIL 11, SOUND_REC_START, SPEAKER_REC_START, WAIT_UNTIL 51, SOUND_REC_STOP pop, SPEAKER_REC_STOP pop, QUIT"

    #RUST_BACKTRACE=full cargo run $TARGET -- --symbols "additional/symbols.txt" --floppy1 "$BUG_ATTACK" --script "WAIT_UNTIL 3, SPEAKER_REC_START, WAIT_UNTIL 18, SPEAKER_REC_STOP speaker/ticks/bug_attack2.ticks,QUIT"

    # RUST_BACKTRACE=full cargo run $TARGET -- --no-sound --symbols "additional/symbols.txt" --floppy1 "$CHOPLIFTER" --script "WAIT_UNTIL 15, SAVE_MEM choplifter.hgr 2000 4000"
    RUST_BACKTRACE=full
    ./target/release/accurapple  --log-stdout --cpu F6502 --symbols "additional/symbols.txt" --floppy1 "$BUG_ATTACK"
    # --script "WAIT_UNTIL 10, SOUND_REC_START cc_snd, VIDEO_REC_START cc_crt, WAIT_UNTIL 40, VIDEO_REC_STOP,SOUND_REC_STOP,QUIT"

    #--script "SOUND_REC_START, SPEAKER_REC_START, WAIT_UNTIL 120, SOUND_REC_STOP pop, SPEAKER_REC_STOP pop, QUIT"
    # RUST_BACKTRACE=full cargo run $TARGET -- --floppy1 "additional/Mines.DLR-8.dsk"
    # --rom data/Apple2e.rom
    # /home/stefan/Downloads/Mines.DLR-8.dsk

#SOUND_REC_START,VIDEO_REC_START fastloader,WAIT_UNTIL 18, KEYS Q, WAIT_UNTIL 80, KEYS Q, WAIT_UNTIL 85, VIDEO_REC_STOP,SOUND_REC_STOP fastloader,QUIT

    # --rom data/Apple2e.rom
    #--script "WAIT_UNTIL 85,SPKR_REC_START,VIDEO_REC_START master_of_lamp,WAIT_UNTIL 140,VIDEO_REC_STOP,SPKR_REC_STOP master_of_lamp,QUIT"

    # --script "BP_CYCLE 27386769" #--floppy1 $LOWTECH  # --script "TURBO_START"  # --turbo --debug --stop-cycle 1744543 --charset data/3410265A.bin --rom /mnt/data2/apple2/apple2/ROMs/apple2e-enhanced.rom #> $TMP
    # RUST_LOG="accurapple::a2=debug,accurapple::sound=warn,accurapple::speaker=warn,accurapple::gfx=warn,accurapple::diskii=trace,accurapple::emulator=debug,accurapple=warn,wgpu_core=warn" RUST_BACKTRACE=1 cargo run $TARGET -- --floppy1 "/mnt/data2/roms/Apple/Boulder Dash (4am and san inc crack)/Boulder Dash (4am and san inc crack).dsk"   --debug # --turbo --debug --stop-cycle 1744543 --charset data/3410265A.bin --rom /mnt/data2/apple2/apple2/ROMs/apple2e-enhanced.rom #> $TMP

fi

# /mnt/data2/rust_target/debug/accurapple --floppy1 "/mnt/data2/roms/Apple/Galaxian (Thunder Mountain) (4am crack).dsk"
