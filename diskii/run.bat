python ..\utils\make_convtab.py
del NEW.DSK
copy BLANK_PRODOS2.DSK NEW.DSK
c:\users\stephanec\opt\acme disk_test.a

java -jar AppleCommander-1.3.5.13-ac.jar -p NEW.DSK STARTUP BIN 0x6000 < STARTUP
python dsk2nic.py --source NEW.DSK --target disk.nic

set RUST_LOG=accurapple=info
set RUST_LOG=accurapple::emulator=info,%RUST_LOG%

@REM ..\target\release\accurapple.exe --no-greetings --floppy1 NEW.DSK --script "LOAD_MEM STARTUP 0C00, SET_PC 0C00"
cargo run --release  -- --symbols ../additional/symbols.txt --cpu 65C02 --no-greetings  --floppy1 NEW.DSK --script "WAIT_UNTIL 2, LOAD_MEM STARTUP 6000, SET_PC 6000, PAUSE"

@REM cargo run --release  -- --floppy1 NEW.DSK
@REM cargo run --release  -- --floppy1 NEW.DSK --script "LOAD_MEM noise.o 0C00, SET_PC 0C00"
@REM cargo run --release -- --script "LOAD_MEM noise.o 0C00, SET_PC 0C00, SOUND_REC_START, SPEAKER_REC_START, WAIT_UNTIL 60, SPEAKER_REC_STOP recordings/noise, SOUND_REC_STOP recordings/noise, QUIT" --no-greetings

@REM acme speaker_test.a
@REM java -jar AppleCommander-1.3.5.13-ac.jar -d NEW.DSK STARTUP
@REM java -jar AppleCommander-1.3.5.13-ac.jar -p NEW.DSK STARTUP BIN 0xC00 < STARTUP
@REM /usr/bin/python3 dsk2nic.py --source NEW.DSK --target disk.nic
@REM /mnt/data2/rust_target/release/accurapple --debug --script "LOAD_MEM STARTUP 0C00, SET_PC 0C00"
