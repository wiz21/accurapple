import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv("f15_stepper.txt")
print(df)
plt.plot(df["Cycle"], df["Angle (deg)"]/180, label="Position")
plt.xlabel("Cycle")
#plt.plot(df["Cycle"], df["Track"]/4, label="Track")
plt.legend()
plt.show()
