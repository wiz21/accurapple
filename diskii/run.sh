#!/bin/bash

/usr/bin/python3 make_convtab.py
rm NEW.DSK
cp BLANK_PRODOS2.DSK NEW.DSK
acme disk_test.a

java -jar AppleCommander-1.3.5.13-ac.jar -p NEW.DSK STARTUP BIN 0x6000 < STARTUP
/usr/bin/python3 dsk2nic.py --source NEW.DSK --target disk.nic

export RUST_LOG=accurapple=warn
export RUST_LOG=accurapple::emulator=warn,%RUST_LOG%

cargo run --release  -- --symbols ../additional/symbols.txt --cpu 65C02 --no-greetings  --floppy1 NEW.DSK
#--script "WAIT_UNTIL 2, LOAD_MEM STARTUP 6000, SET_PC 6000, PAUSE"
