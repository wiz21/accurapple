from pathlib import Path
import numpy as np
from matplotlib import pyplot as plt

# ROM layout is described in 8-40 in UTAe

#with open(r"data/Apple IIe Video - Unenhanced - 342-0133-A - 2732.bin","rb") as fin:
ROM_FILE = r"../data/3410265A.bin"
with open(ROM_FILE,"rb") as fin:
    rom = fin.read()

img = np.zeros( (16*8,32*8))


linear_rom = np.unpackbits(np.array(list(rom), dtype=np.uint8)).reshape((32768//8,8))
linear_rom[0:0x800] = 1 - linear_rom[0:0x800]
linear_rom = np.fliplr(linear_rom)
rom2 = np.packbits(linear_rom)
# Sather example on page 8-40.
# See also the display of charset rom page 8-25, Figure 8.8
print([f"{x:02X}" for x in rom[0x610:0x618]])
# Sather: "It becomes clear that the ZEROes in the numbers represent the dots in
# a matrix that form the letter "B". "


a = []
CHARS_PER_COLUMN=16
LINES_PER_CHAR=8
rom_size = Path(ROM_FILE).stat().st_size
for i in range(4096//(LINES_PER_CHAR*CHARS_PER_COLUMN)):
    a.append( linear_rom[i*LINES_PER_CHAR*CHARS_PER_COLUMN:(i+1)*LINES_PER_CHAR*CHARS_PER_COLUMN, :] )
ur = np.hstack(a)

# Testing table 8.3 (and the one on the previous page)
"""
ALTCHRSET VID7 VID6 RA10 RA9
          $80  $40
-------------------------------------------------------
0          0    0    0    0  INVERSE control/special
0          0    1   flash 0  Flash INVERSE
0          1    0    1    0  Normal control/special
0          1    1    1    1  normal upper/lower

1          0    0    0    0  inverse control/special
1          0    1    0    1  inverse upper/lower
1          1    0    1    0  Normal control/special
1          1    1    1    1  normal upper/lower
"""
c =  0xA0 # inverse space: 0b1100_0000 - 0xA0
v05 = c & 0x3f
v6 = (c & 0b0100_0000) >> 6
v7 = (c & 0b1000_0000) >> 7
flash = 0
altcharset = 0
a10 = v7 | (v6 & flash & (1-altcharset))
a9 = v6 & (v7 | altcharset)
final_c = (a10 << 7) | (a9 << 6) | v05
print(f"${c:02X} ({c:08b}) v7={v7} v6={v6} v05={v05:b} a10={a10} a9={a9} => {final_c:08b} (${final_c:02X})")
for line in linear_rom[final_c*8:final_c*8+8]:
    print(f"{line}".replace("0",".").replace('1','#'))

plt.title(f"{ROM_FILE}, {rom_size} bytes")
plt.imshow(ur[0:256,:])
t = np.arange(0,256,8)
plt.xticks(t+4,[f"{x*2:02X}" for x in t])
plt.show()