from pprint import pprint
import numpy as np
from time import sleep
import pygame
import pickle

with open('transitions.pkl', 'rb') as outp:
    transitions = pickle.load( outp)
    text = transitions["text"]
pygame.init()
pygame.mixer.init()
crash_sound = pygame.mixer.Sound('/home/stefan/Downloads/mech-keyboard-02-102918.mp3')

pprint(transitions)
prev_letter = None
for letter in text:
    tx = (prev_letter, letter)
    if tx in transitions:
        dt = transitions[tx]
        sleep(np.mean(dt))
    pygame.mixer.Sound.play(crash_sound)
    print(f"{letter}", end="", flush=True)
    prev_letter = letter
print()
