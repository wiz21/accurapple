import pickle
import keyboard
from pprint import pprint

transitions = dict()
prev_time = None
record = keyboard.record()
text = ""
for ke in record:
    if ke.event_type == keyboard.KEY_UP:
        continue
    print(ke)
    if ke.name not in ("enter", "space", "esc"):
        text += ke.name
        letter = ke.name
    if ke.name == "space":
        text += " "
        letter = " "

    if prev_time is not None:
        dt = ke.time - prev_time
        tx = (prev_name, letter)
        if tx not in transitions:
            transitions[tx] = []
        transitions[tx].append(dt)

    prev_time = ke.time
    prev_name = letter

with open('transitions.pkl', 'wb') as outp:
    transitions["text"] = text
    pickle.dump(transitions, outp, pickle.HIGHEST_PROTOCOL)
