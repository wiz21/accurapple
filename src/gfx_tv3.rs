use std::collections::VecDeque;
use bitflags::bitflags;
use bytemuck::Pod;
use crate::a2::{
    AppleII, ClockEvent, ClockEventData, GfxBaseMode, GfxMode, VideoSoftSwitches, BYTES_PER_LINE, CYCLES_PER_FRAME, CYCLES_PER_LINE, CYCLES_PER_SECOND, FIRST_NON_BLANK_LINE, HBL_CYCLES, SOFT_SWITCH_BIT_GR, SOFT_SWITCH_BIT_MODEMIX, SOFT_SWITCH_BIT_POS_TEXT, FIRST_VISIBLE_CYCLE
};

use crate::emulator::MonoColourSwitch;
use crate::gfx_utils::{
    hgr_address, text_address,
};

const LINE_START: usize = 25;


#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum GfxTransition {
    HgrToGr,
    GrToHgr
}

use bytemuck::Zeroable;

bitflags! {
    /// Represents a set of flags.
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    #[repr(C)]
    #[derive(bytemuck::NoUninit, bytemuck::Zeroable)]
    pub struct HBLFlags: u8 {
        /// Is the colour burst present in the HBL ?
        const COLOUR_BURST = 0b00000001;
        /// PAL phase
        const PAL_PHASE = 0b00000010;
    }
}

pub struct GfxRenderState {
    cycle: u64,
    text: bool,
    page2: bool,
    mixed: bool,
    dhgr: bool,
    gr: bool,
    mode: GfxMode,
    pub gfx_mem: Vec<u8>,
    pub soft_switches: Vec<u8>,
    pub scanned_bytes: Vec<u8>,
    gfx_aux_mem: Vec<u8>,
    should_redraw: bool,
    lines_modes: Vec<usize>,
    scanner: NScanner,
    shifter: Shifter,

    // The Apple 2 emits the composite signal. It also emit the colour burst if
    // it sees it fit. But in both case, the Apple 2 decides. And that's why
    // it's done here and not in the composite to RGB conversion (which is the
    // job of the CRT). A 1 means there's a colour burst, 0 means no colour burst.
    pub colour_bursts: Vec<HBLFlags>,
}

impl GfxRenderState {
    pub fn new(mode: GfxMode) -> GfxRenderState {

        GfxRenderState {
            cycle: 0,
            text: true,
            page2: false,
            mixed: false,
            dhgr: false,
            gr: false,
            mode,
            // Only the part of the RAM dedicated to
            // display. Starting from 0 to avoid addresses conversion
            // when updating this array.
            gfx_mem: vec![0; 0x6000],
            soft_switches: vec![0; 192*65],
            scanned_bytes: vec![0; 192*80],
            gfx_aux_mem: vec![0; 0x6000],
            colour_bursts: vec![HBLFlags::empty(); 192],
            should_redraw: true,
            lines_modes: vec![0; 192 + 60 + 60], // FIXME Put a const here !
            scanner: NScanner::new(mode),
            shifter: Shifter::new(mode),
        }
    }
}

pub struct NScanner {
    mode: GfxMode,
    line: usize,
    x: usize,
}

impl NScanner {
    fn new(mode: GfxMode) -> NScanner {
        NScanner {
            mode,
            line: 0,
            x: 0,
        }
    }

    fn set_mode(&mut self, mode: GfxMode) {
        self.mode = mode;
    }

    fn set_on_line(&mut self, line: usize) {
        assert!(line < 192);
        self.line = line;
        self.x = 0;
    }

    fn tick(&mut self, main_ram: &[u8], aux_ram: &[u8], soft_switches: &[VideoSoftSwitches]) -> (u8, u8) {
        let ssw = &soft_switches[self.x];

        // Scanner progresses on SCNCLK,
        // SCANCLK ticks on RASRISE,
        // RASRISE doesn't depend on GR.
        // But on table 5.3 and figure 5.3, we see address mutpliexing is driven by GR+1

        // Sater p. 5-7: "When set, PAGE2 selects secondary display memory pages
        // for scanning. 80STORE, when set, overrides the effect of PAGE2 on
        // memory scanning, thus inhibiting display of screen page 2."

        let page2 = ssw.page2 & !ssw.store80;

        let gr1 = compute_gr(&soft_switches[ if self.x <= 1 {0} else {self.x - 1}], self.line);
        let hires_time_gr1 = gr1 && ssw.hires == 1;

        let low_res_gr1 = !hires_time_gr1;

		let ofs = if low_res_gr1 {
			if page2 > 0 {
				0x800
			} else {
				0x400
			}
		} else if page2 > 0 { 0x4000 } else { 0x2000 };


        let addr = if low_res_gr1 {
            text_address(self.line)
        } else {
            hgr_address(self.line)
        };

        let r = if ssw.col80 == 1 {
            (main_ram[ofs + addr + self.x],
             aux_ram[ofs + addr + self.x],
            )
        } else {
            (main_ram[ofs + addr + self.x],
            aux_ram[ofs + addr + self.x],
            )
        };

        self.x += 1;
        r
    }
}

struct Shifter {
    mode: GfxMode,
    line: usize,
    scanned_byte: u8,
    byte2: u8,
    last_bit_out: u8,
    shift: bool,
    transition: Option<GfxTransition>,
}

impl Shifter {
    fn new(mode: GfxMode) -> Shifter {
        Shifter {
            mode,
            line: 0,
            scanned_byte: 0,
            byte2: 0,
            last_bit_out: 0,
            shift: false,
            transition: None
        }
    }

    fn set_mode(&mut self, mode: GfxMode, check_transition: bool) {
        self.transition = None;
        if check_transition {
            //println!("TX {} {}", self.mode.mode, mode.mode);
            if self.mode.mode == GfxBaseMode::Hgr && (mode.mode == GfxBaseMode::Gr || self.mode.mode == GfxBaseMode::Text40) {
                self.transition = Some(GfxTransition::HgrToGr);
            } else if (self.mode.mode == GfxBaseMode::Gr || self.mode.mode == GfxBaseMode::Text40) && mode.mode == GfxBaseMode::Hgr {
                self.transition = Some(GfxTransition::GrToHgr);
            }
        }
        self.mode = mode;
    }

    fn set_on_line(&mut self, line: usize) {
        assert!(line < 192);
        self.line = line;
        self.last_bit_out = 0;
        self.shift = false;
    }
/*
    fn load_byte(&mut self, byte: u8, byte_aux_mem: u8, cycle: u64, charset_rom: &Vec<u8>, alt_charset: u8) {
        match self.mode.mode {
            GfxBaseMode::Hgr => {
                self.scanned_byte = byte;
                self.shift = ((byte & 0x80) >> 7) != 0;
            }
            GfxBaseMode::Dhgr => {
                self.scanned_byte = byte;
                self.byte2 = byte_aux_mem;
            }
            GfxBaseMode::Text40 => {
                // See Video Generation 8-15
                let vid05 = byte & 0b0011_1111;
                let vid6 = (byte & 0b0100_0000) >> 6;
                let vid7 = (byte & 0b1000_0000) >> 7;

                /*
                 The Apple IIe can switch between Apple II
                 compatible inverse and flashing video and full
                 ASCII inverse via the ALTCHARSET. The Apple II
                 can operate only in ALTCHARSET' mode.
                */

                let flash = ((cycle / (10*20280)) % 2) as u8;
                let ra10 = vid7 | (vid6 & flash & (1-alt_charset));
                let ra9 = vid6 & (vid7 | alt_charset);
                let ra11 = 0;

                //print!("{}\n", cycle);
                let rom_address = (((vid05 as usize) << 3) + self.line % 8) | ((ra9 as usize) << 9) | ((ra10 as usize) << 10) | (ra11 << 11);
                self.scanned_byte = ! charset_rom[rom_address];

                self.shift = false;
            }
            GfxBaseMode::Text80 => {
                let vid05 = byte & 0b0011_1111;
                let vid6 = (byte & 0b0100_0000) >> 6;
                let vid7 = (byte & 0b1000_0000) >> 7;
                let flash = 0; // ((cycle / (10*20280)) % 2) as u8;

                let ra10 = vid7 | (vid6 & flash & (1-alt_charset));
                let ra9 = vid6 & (vid7 | alt_charset);
                let ra11 = 0;

                let rom_address = (((vid05 as usize) << 3) + self.line % 8) | ((ra9 as usize) << 9) | ((ra10 as usize) << 10) | (ra11 << 11);

                // self.scanned_byte = charset_rom[(byte as usize) * 8 + self.line % 8];
                // self.byte2 = charset_rom[(byte_aux_mem as usize) * 8 + self.line % 8];
                self.scanned_byte = !charset_rom[rom_address];

                let vid05 = byte_aux_mem & 0b0011_1111;
                let vid6 = (byte_aux_mem & 0b0100_0000) >> 6;
                let vid7 = (byte_aux_mem & 0b1000_0000) >> 7;
                let flash = 0; // ((cycle / (10*20280)) % 2) as u8;
                let ra10 = vid7 | (vid6 & flash & (1-alt_charset));
                let ra9 = vid6 & (vid7 | alt_charset);
                let ra11 = 0;

                let rom_address = (((vid05 as usize) << 3) + self.line % 8) | ((ra9 as usize) << 9) | ((ra10 as usize) << 10) | (ra11 << 11);

                self.byte2 = !charset_rom[rom_address];

                self.shift = false;
            }
            GfxBaseMode::Gr => {
                // In GR mode, there are two colors stored in each byte.
                // Each color is 4 bits.
                // Each block is 4 scanline tall.
                // The 4 hi bits are used to display the odd 4-lines-tall blocks
                // The 4 lo bits are used to display the even 4-lines-tall blocks
                // So the test here figures out on which of odd or even 4-lines-tall
                // block we're drawing.
                let mut val = if (self.line / 4) & 1 == 0 {
                    byte & 0b1111
                } else {
                    (byte & 0b11110000) >> 4
                };

                val = (val << 4) | val;
                if cycle & 1 == 1 {
                    val = val.rotate_right(2);
                }

                self.scanned_byte = val;
            }
            GfxBaseMode::Dgr => {

                let mut val = if (self.line / 4) & 1 == 0 {
                    byte & 0b1111
                } else {
                    (byte & 0b11110000) >> 4
                };

                val = (val << 4) | val;
                if cycle & 1 == 0 {
                     val = val.rotate_right(2);
                }
                self.scanned_byte = val;

                // Aux mem

                let mut val = if (self.line / 4) & 1 == 0 {
                    byte_aux_mem & 0b1111
                } else {
                    (byte_aux_mem & 0b11110000) >> 4
                };

                val = (val << 4) | val;
                if cycle & 1 == 0 {
                     val = val.rotate_right(2);
                }

                self.byte2 = val;
            }
        }
    }

    fn transition_shift(&mut self, cycle:u64, transition: GfxTransition) -> Vec<u8> {
        let mut byte = self.scanned_byte; // Just a shorthand
        let mut memory: Vec<u8> = Vec::with_capacity(14);
        let shift = (byte & 0x80) != 0;

        match transition {
            GfxTransition::GrToHgr => {
                if cycle & 1 == 0 {
                    byte = byte.rotate_left(2);
                }

                for _ in 0..2 {
                    let bit = byte & 1;
                    byte >>= 1;
                    memory.push(bit);
                }

                for _ in 0..6 {
                    let bit = byte & 1;
                    self.last_bit_out = bit;
                    byte >>= 1;
                    memory.push(bit);
                    memory.push(bit);
                }



            },
            GfxTransition::HgrToGr => {
                if shift {
                    memory.push(self.last_bit_out);

                    let bit = byte & 1;
                    byte >>= 1;
                    memory.push(bit);
                    memory.push(bit);

                } else {
                    let bit = byte & 1;
                    byte >>= 1;
                    memory.push(bit);
                    memory.push(bit);

                    let bit = byte & 1;
                    byte >>= 1;
                    memory.push(bit);
                }

                for _ in 0..6 {
                    let bit = byte & 1;
                    byte >>= 1;
                    memory.push(bit);
                }

                memory.push(0);

                byte = self.scanned_byte;
                for _ in 0..4 {
                    let bit = byte & 1;
                    byte >>= 1;
                    memory.push(bit);
                }

                self.last_bit_out = 0;
            }
        };

        /* for i in 0..14 {
            memory[i] = 1-memory[i];
        } */
        assert!(memory.len() == 14);
        memory
    }

    fn regular_shift(&mut self, cycle:u64) -> Vec<u8> {
        let mut byte = self.scanned_byte; // Just a shorthand
        let mut byte2 = self.byte2; // Just a shorthand
        let mut memory: Vec<u8> = Vec::new();

        match self.mode.mode {
            GfxBaseMode::Hgr => {
                let shift = ((byte & 0x80) ^ self.mode.an3) != 0;

                if shift {
                    memory.push(self.last_bit_out)
                }

                for i in 0..6 {
                    let bit = byte & 1;
                    memory.push(bit);
                    memory.push(bit);
                    byte >>= 1;
                }

                let bit = byte & 1;
                if !shift {
                    memory.push(bit);
                    memory.push(bit);
                } else {
                    memory.push(bit)
                }

                self.last_bit_out = bit
            }
            GfxBaseMode::Gr => {
                let mut bit = 0;
                for _ in 0..14 {
                    bit = byte & 1;
                    byte = byte.rotate_right(1);
                    memory.push(bit);
                }

                self.last_bit_out = bit;
                self.shift = false;
            }
            GfxBaseMode::Dgr => {
                let mut bit = 0;

                for i in 0..7 {
                    byte = byte.rotate_right(1);
                    bit = byte & 1;
                    memory.push(bit);
                }

                //byte2 = 0x0;
                for i in 0..7 {
                    byte2 = byte2.rotate_right(1);
                    bit = byte2 & 1;
                    memory.push(bit);
                }

                self.last_bit_out = bit;
            }
            GfxBaseMode::Dhgr | GfxBaseMode::Text80 => {
                fn seven_rol(b: u8) -> u8 {
                    ((b & 64) >> 6) | ((b & 0b00111111) << 1)
                };

                let mut byte = ((self.scanned_byte & 0b111_1111) << 1) | (self.last_bit_out);
                //byte = seven_rol(byte);
                for i in 0..8 {
                    memory.push(byte & 1);
                    byte = byte.rotate_right(1);
                }
                let mut byte = self.byte2 & 0b111_1111;
                //byte = seven_rol(byte);
                for i in 0..6 {
                    memory.push(byte & 1);
                    byte = byte.rotate_right(1);
                }
                self.last_bit_out = byte & 1;
            }
            GfxBaseMode::Text40 => {
                for _ in 0..7 {
                    let bit = byte & 1;
                    byte = byte.rotate_right(1);
                    memory.push(bit);
                    memory.push(bit);
                }

                self.last_bit_out = 0;
                self.shift = false;
            }
            _ => panic!("Unsupported gfx mode {}", self.mode.mode),
        }

        assert!(memory.len() == 14);
        memory

    }

    fn tick(&mut self, cycle: u64) -> Vec<u8> {
        if let Some(tx) = self.transition {
            let r = self.transition_shift(cycle, tx);
            self.transition = None;
            r
        } else {
            self.regular_shift(cycle)
        }
    }
*/
}

fn apply_event(
    event: &ClockEvent,
    memory: &mut Vec<u8>,
    aux_memory: &mut Vec<u8>,
    scanner: &mut NScanner,
    shifter: &mut Shifter,
    mark_transitions: bool
) {
    match event.event {
        ClockEventData::GfxMemWrite { addr, data } => memory[addr as usize] = data,
        ClockEventData::GfxAuxMemWrite { addr, data } => { aux_memory[addr as usize] = data }
        ClockEventData::GfxModeChange { mode } => {
            scanner.set_mode(mode);
            shifter.set_mode(mode, mark_transitions);
        }
        _ => (),
    }
}

fn apply_events_up_to_cycle(
    events: &mut VecDeque<ClockEvent>,
    last_cycle: u64,
    memory: &mut Vec<u8>,
    aux_memory: &mut Vec<u8>,
    scanner: &mut NScanner,
    shifter: &mut Shifter,
    _y: usize
) {
    //let mut GR_delayed = compute_GR(&scanner.mode.video_flags, y);
    //let frame_start = last_cycle / (CYCLES_PER_FRAME as u64) * (CYCLES_PER_FRAME as u64);
    if !events.is_empty() {
        //println!("Apply event up to {}, frame start {}", last_cycle, frame_start);
    }
    while let Some(event) = events.back() {
        if event.tick < last_cycle {
            //GR_delayed = compute_GR(&scanner.mode.video_flags, y);
            apply_event(event, memory, aux_memory, scanner, shifter, false);
            events.pop_back();
        } else {
            break;
        }
    }

}

fn compute_gr(switches: &VideoSoftSwitches, y: usize) -> bool {
    if y < 192-4*8 {
        switches.text == 0
    } else if switches.modemix != 0 {
        false
    } else {
        switches.text == 0
    }
}

pub fn render_one_frame2(
    machine: &mut AppleII,
    cpu_cycle: u64,
    _rgba: &mut [u8],
    state: &mut GfxRenderState
) {
    //let frame_ofs: usize = (cpu_cycle % (CYCLES_PER_FRAME as u64)) as usize;
    let frame_ofs = 0_usize;
    let frame_start_cycle = if frame_ofs == 0 {
        cpu_cycle - (CYCLES_PER_FRAME as u64)
    } else {
        cpu_cycle - (frame_ofs as u64)
    };

    // We first handle the events that occurs on the lines before the visible
    // area. In particular we don't consume the first HBL before the actually
    // drawn area because this is part of the "visible lines" rendering.

    let visible_lines_area_start: u64 = frame_start_cycle + (FIRST_NON_BLANK_LINE*CYCLES_PER_LINE) as u64;
    apply_events_up_to_cycle(
        &mut machine.clock_events,
        visible_lines_area_start,
        &mut state.gfx_mem,
        &mut state.gfx_aux_mem,
        &mut state.scanner,
        &mut state.shifter,
        0
    );

    let mut pal_in_phase = true;
    let _color_bursts: Vec<bool> = Vec::with_capacity(192);

    // if frame_ofs >= FIRST_NON_BLANK_LINE * CYCLES_PER_LINE  &&
    //    frame_ofs < (FIRST_NON_BLANK_LINE+192) * CYCLES_PER_LINE {

    // ----------------------------------------------------------------
    // Now we render the visible part of the screen

    //let mut mode_at_switch = None;
    //let mut gr_delayed_by_one_cycle: bool = compute_GR(&state.mode.video_flags, 0);

    for line in 0..192 {

        /* Table 3.2 UTAe: a line is divided in

        -  9 cycles of horizontal blanking (HBL) [B]
        -  4 cycles of SYNC [S in the ascii chart below]
        -  4 cycles of COLOR BURST [C]
        -  8 cycles of left margin [m]
        - 40 cycles of display [D]

        For a total of 65 cycles.

        I guess the SYNC is read by the PAL circuit
        CLRGATE* = TEXT' & HBL & H3 & H2 (with H2 == 4 and H3 == 8)
        According to table 3-2 in UTA2e, HBL & H3 & H2 == COLOUR BURST


                                blank                        Display
        display or blank: -------------------------DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        HBL stuff       : BBBBBBBBBSSSSCCCCmmmmmmmm----------------------------------------

        */

        let line_start_cycle = visible_lines_area_start + (line * CYCLES_PER_LINE) as u64;
        let line_end_cycle = line_start_cycle + CYCLES_PER_LINE as u64;

        // Handle the 4 text lines at the bottom of the screen
        // if needed. We do this here because the mode change doesn't
        // come from a read in $C0xx and so is not recorded as an event.
        // We could record it as an event but then we could have a situation
        // where two events occur at the same cycle (and that I don't support).
        if line >= 192 - 4 * 8 {
            if let Some(mixed) = state.scanner.mode.mixed {
                //println!("line {} mixed? {}", line, mixed);
                state.scanner.mode.set_base_mode(mixed);
                state.scanner.mode.video_flags.modemix = 1;
                //mode_at_switch = Some(state.shifter.mode.set_base_mode(mixed))
            }
        };

        let mut line_soft_switches: Vec<VideoSoftSwitches> = Vec::with_capacity(CYCLES_PER_LINE);


        // color_bursts.push(
        //     state.scanner.mode.mode != GfxBaseMode::Text40
        //         && state.scanner.mode.mode != GfxBaseMode::Text80,
        // );

        state.scanner.set_on_line(line);
        state.shifter.set_on_line(line);
        for cycle in 0..CYCLES_PER_LINE as u64 {
            // The 25 first entries are what happens around the displayed part
            // of the line (the HBL). The next 40 entries are the displayed line.

            line_soft_switches.push(state.scanner.mode.video_flags);

            if let Some(event) = machine.clock_events.back() {
                if event.tick   == line_start_cycle + cycle {
                    apply_event(
                        event,
                        &mut state.gfx_mem,
                        &mut state.gfx_aux_mem,
                        &mut state.scanner,
                        &mut state.shifter,
                        true
                    );
                    machine.clock_events.pop_back();
                }
            }
        } // for over line


        for inline_cycle in 0..BYTES_PER_LINE as u64 {
            let (scanned_byte_main_ram, scanned_byte_aux_mem) =
                state.scanner.tick(
                    &state.gfx_mem, &state.gfx_aux_mem,
                    &line_soft_switches[HBL_CYCLES..CYCLES_PER_LINE]);

            let sb_ofs = (line*BYTES_PER_LINE + inline_cycle as usize)*2;
            state.scanned_bytes[sb_ofs] = scanned_byte_main_ram;
            state.scanned_bytes[sb_ofs+1] = scanned_byte_aux_mem;
        }


        for inline_cycle in 0..line_soft_switches.len() {
            let ssw: &VideoSoftSwitches = &line_soft_switches[inline_cycle];
            state.soft_switches[line*CYCLES_PER_LINE + inline_cycle] = ssw.to_bit_flags();
            if compute_gr(ssw, line) {
                state.soft_switches[line*CYCLES_PER_LINE + inline_cycle] |= 1 << SOFT_SWITCH_BIT_GR;
            }
        }

        let mut gr_flags = 0;

        // For the i range, check the comment above where I describe how to
        // decompose a line.

        // Special offsets values were found by using the BADBURST test.
        const START:usize = 13;
        for i in START..START+4 {
            let ssw = state.soft_switches[line*CYCLES_PER_LINE + i];
            if ssw & (1 << SOFT_SWITCH_BIT_POS_TEXT) == 0 && !(line >= 160 && ssw & (1 << SOFT_SWITCH_BIT_MODEMIX) != 0) {
                gr_flags += 1;
            }
        }

        if gr_flags >= 1 && machine.motherboard_mono_colour_switch == MonoColourSwitch::Colour {
            state.colour_bursts[line] = HBLFlags::COLOUR_BURST;
        } else {
            state.colour_bursts[line] = HBLFlags::empty();
        }

        // when in TXT PAL doesn't alternate phase.
        // when in GR alternating start again, from where it left off.
        // So if one stops alternating during one line (by going to TXT),
        // then the PAL pahse will alternate, sure, but *reversed* !!!

        if line >= 1 {
            const D: usize = 14;
            let old_line_txt = state.soft_switches[(line-1)*CYCLES_PER_LINE + D] & (1 << SOFT_SWITCH_BIT_POS_TEXT) != 0;
            let this_line_txt = state.soft_switches[line*CYCLES_PER_LINE + D] & (1 << SOFT_SWITCH_BIT_POS_TEXT) != 0;

            if this_line_txt && !old_line_txt {
                pal_in_phase = !pal_in_phase;
            }
        }

        // FIXME Disabled for the moment because I don't
        // understand the behaviour (and it is highly dependent
        // on the CRT monitor)
        if true || pal_in_phase {
            state.colour_bursts[line] |= HBLFlags::PAL_PHASE;
        }


        // if cpu_cycle > (10 * CYCLES_PER_SECOND) as u64 {
        //     println!("line:{} colour_burst:{}", line, state.colour_bursts[line]);
        // }

        //screen_bytes.extend(line_bytes);

        apply_events_up_to_cycle(
            &mut machine.clock_events,
            line_end_cycle,
            &mut state.gfx_mem,
            &mut state.gfx_aux_mem,
            &mut state.scanner,
            &mut state.shifter,
            line
        );

    } // line



    apply_events_up_to_cycle(
        &mut machine.clock_events,
        frame_start_cycle + CYCLES_PER_FRAME as u64,
        &mut state.gfx_mem,
        &mut state.gfx_aux_mem,
        &mut state.scanner,
        &mut state.shifter,
        193 // Outside the mixed tet zone
    );

}
