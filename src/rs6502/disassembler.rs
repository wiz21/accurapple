use byteorder::{ByteOrder, LittleEndian};
use indexmap::IndexMap;
use std::collections::HashMap;

//use super::opcodes::{AddressingMode, OpCode};

//use crate::rs6502::opcodes;

use super::opcodes::{AddressingMode,OpCode,ADDRESSING_MODES};

//extern crate accurapple;
//use super::accurapple::emulator;
//use accurapple::emulator;
//use super::super::accurapple;
//use accurapple::emulator;
//use crate::emulator;
use crate::emulator::CpuDebugState;
use crate::debug2::{Symbol, SymbolFlags};

// https://doc.rust-lang.org/book/ch07-02-defining-modules-to-control-scope-and-privacy.html

pub struct DisassembledInstruction {
    // I need
    // PC and PC's label are put outside of the strings because
    // the rendering will differ given I have one or the other
    // - "PC": I need it because it's computed depending on each instruction's size
    // - option: "label of PC" : NOT needed, rendering will be done by caller
    // Standard strings:
    // - string: "instruction bytes"
    // - string "mnemonic + operands + accessed address and label"
    // Other:
    // - u16: accessed (read or write) address : will be used to read the memory
    //   or the execution trace to find the proper value. But that's really
    //   tricky 'cos maybe you're in the trace, maybe not; maybe we want
    //   to pre-execute some code etc. A usabilityu nightmare.

    pub pc: u16,
    pub bytes: String,
    pub source: String,
    // The fully decoded address (fully because sometimes
    // one has indirect mode addressing).

    // 1234: JMP ($5597) may be disassembled as:

    // $1234-jump_point_label:
    //     XX 97 55    JMP (jump_ptr_label=$5597) jump_target_label=$9876

    // Indexed indirect:

    // This mode is only used with the X register. Consider a
    // situation where the instruction is LDA ($20,X), X contains $04,
    // and memory at $24 contains 0024: 74 20, First, X is added to
    // $20 to get $24. The target address will be fetched from $24
    // resulting in a target address of $2074. Register A will be
    // loaded with the contents of memory at $2074.

    // 1234: LDA ($20,X)

    // $1234-lda_label:
    //        XX 20      LDA (zp_ptr_label=$20, X) lda_read_label=$2074
    // ah but I must know X to compute that ! FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
    // or
    // $1234- XX 20      LDA ($20, X) $2074

    pub accessed_address: u16
}

impl DisassembledInstruction {
    fn new() -> DisassembledInstruction {
	DisassembledInstruction {
	    pc: 0,
	    bytes: "".to_string(),
	    source: "".to_string(),
	    accessed_address: 0
	}
    }
}

pub struct Disassembler {
    /// Determines whether byte offsets are generated
    /// in the Assembly output
    disable_offsets: bool,

    /// Determines whether opcodes are generated
    /// in the Assembly output
    disable_opcodes: bool,

    /// Hints the disassembler at the code offset
    /// in memory so that it can adjust its memory
    /// offsets
    pub code_offset: u16,
}

/// A 6502 instruction disassembler
impl Disassembler {
    /// Creates a new, default instance of the Disassembler
    ///
    /// # Example
    /// ```
    /// use rs6502::Disassembler;
    ///
    /// let dasm = Disassembler::new();
    ///
    /// let code: Vec<u8> = vec![0xA9, 0x20, 0x8D, 0x00, 0x44];
    /// let asm = dasm.disassemble(&code);
    ///
    /// assert_eq!(Disassembler::clean_asm("
    ///
    ///     0000 LDA #$20
    ///     0002 STA $4400
    ///
    /// "), Disassembler::clean_asm(asm));
    /// ```
    pub fn new() -> Disassembler {
        Disassembler {
            disable_offsets: false,
            disable_opcodes: true,
            code_offset: 0,
        }
    }

    /// Creates an instance of the Disassembler where no
    /// byte offsets are generated in the Assembly output
    ///
    /// # Example
    /// ```
    /// use rs6502::Disassembler;
    ///
    /// let dasm = Disassembler::with_code_only();
    ///
    /// let code: Vec<u8> = vec![0xA9, 0x20, 0x8D, 0x00, 0x44];
    /// let asm = dasm.disassemble(&code);
    ///
    /// assert_eq!(Disassembler::clean_asm("
    ///
    ///     LDA #$20
    ///     STA $4400
    ///
    /// "), Disassembler::clean_asm(asm));
    /// ```
    pub fn with_code_only() -> Disassembler {
        Disassembler {
            disable_offsets: true,
            disable_opcodes: true,
            code_offset: 0,
        }
    }

    /// Creates an instance of the Disassembler with all
    /// available information generated into the output
    ///
    /// # Example
    /// ```
    /// use rs6502::Disassembler;
    ///
    /// let dasm = Disassembler::with_verbose_output();
    ///
    /// let code: Vec<u8> = vec![0xA9, 0x20, 0x8D, 0x00, 0x44];
    /// let asm = dasm.disassemble(&code);
    ///
    /// assert_eq!(Disassembler::clean_asm("
    ///
    ///     0000 A9 20    LDA #$20
    ///     0002 8D 00 44 STA $4400
    ///
    /// "), Disassembler::clean_asm(asm));
    /// ```
    pub fn with_verbose_output() -> Disassembler {
        Disassembler {
            disable_offsets: false,
            disable_opcodes: false,
            code_offset: 0,
        }
    }

    pub fn with_offset(offset: u16) -> Disassembler {
        Disassembler {
            disable_offsets: false,
            disable_opcodes: false,
            code_offset: offset,
        }
    }


    pub fn disassemble(&self, raw: &[u8]) -> String {
	let a = IndexMap::new();
	let b = HashMap::new();
        self.disassemble_with_addresses(raw, &None, 0, 0, None, &a, &b)
            .into_iter()
            .map(|di: DisassembledInstruction| di.source.replace(':',""))
            .collect::<Vec<_>>()
            .join("\n")
    }

    /// Accepts a slice of 6502 bytecodes and translates them
    /// into an assembly String representation
    ///
    /// # Example
    /// ```
    /// use rs6502::Disassembler;
    ///
    /// let dasm = Disassembler::new();
    ///
    /// let code: Vec<u8> = vec![0xA9, 0x20, 0x8D, 0x00, 0x44];
    /// let asm = dasm.disassemble(&code);
    ///
    /// assert_eq!(Disassembler::clean_asm("
    ///
    ///     0000 LDA #$20
    ///     0002 STA $4400
    ///
    /// "), Disassembler::clean_asm(asm));
    /// ```

    pub fn disassemble_with_addresses(
	&self,
	raw: &[u8],
	_memory: &Option<Vec<u8>>,
	_x: u8,
	_y: u8,
	max_instr: Option<usize>,
	labels: &IndexMap<u16, Symbol>,
	_trace_data: &HashMap<u16,&CpuDebugState>) -> Vec<DisassembledInstruction> {

	fn peek(memory: &Option<Vec<u8>>, addr:u16) -> String {
	    if memory.is_some() {
		// ${:02X} memory.as_ref().unwrap()[addr as usize]
		format!("<${:04X}=${:02X}>",addr,memory.as_ref().unwrap()[addr as usize])
	    } else {
		"$??".to_string()
	    }
	}

	fn symbol_address(labels: &IndexMap<u16, Symbol>, addr:u16) -> String {
	    let _addr_plus_one = addr.saturating_add(1);
	    let addr_minus_one = addr.saturating_sub(1);

        let addr_str = if addr < 0x100 {
            format!("${:02X}", addr)
        } else {
            format!("${:04X}", addr)
        };

	    if let Some(symbol) = labels.get(&addr) {
		    format!("{} ({})", symbol.symbol, addr_str)
	    } else if let Some(symbol) = labels.get(&addr_minus_one) {
            if symbol.flags.contains(&SymbolFlags::WordWatch) {
                format!("{}+1 ({})", symbol.symbol, addr_str)
            } else {
                format!("${:02X}", addr)
            }
	    } else {
		format!("${:02X}", addr)
	    }
	}

        //let mut result = Vec::new();
        let mut result2 = Vec::new();

	let mut decoded = 0;
        let mut i: usize = 0;
        while i < raw.len() {
	    let mut r = DisassembledInstruction::new();
	    r.pc = i as u16;

            if let Some(opcode) = OpCode::from_raw_byte(raw[i]) {

                // Each branch returns the opcode output and the
                // disassembled output
                let val = match opcode.mode {
                    AddressingMode::Immediate => {
                        let imm = raw[i + 0x01];
                        (format!("{:02X} {:02X}", opcode.code, imm),
			 format!("#${:02X}", imm))
                    }
                    AddressingMode::Indirect => {
                        let b1 = raw[i + 0x01];
                        let b2 = raw[i + 0x02];

                        let addr = LittleEndian::read_u16(&[b1, b2]);

                        (format!("{:02X} {:02X} {:02X}", opcode.code, b1, b2),
                         format!("(${:04X})", addr))
                    }
                    AddressingMode::Relative => {
                        let b1: u8 = raw[i + 0x01];
                        let offset: i8 = b1 as i8;
			let base: usize = self.code_offset as usize + i;

			// i: usize
                        let addr = if offset == -128 {
			    b1 as usize
			} else if offset < 0 {
			    if base >= (-offset) as usize - 0x02 {
                                base - ((-offset) as usize - 0x02)
                            } else {
                                b1 as usize   // Failsafe for potential overflow when disassembling raw bytes .. just dump the byte
                            }
                        } else { // offset >= 0
                            base + (offset as usize) + 0x02
                        };

			//println!("b1:{:X} offset:{} i:{:} base:{:X} code_offset:{:X}, addr:{:X} ", b1, offset, i, base, self.code_offset, addr);

			let addr_field = symbol_address(labels, addr as u16);
                        (format!("{:02X} {:02X}", opcode.code, b1),
                         addr_field)
                    }
                    AddressingMode::ZeroPage => {
                        let b1 = raw[i + 0x01];
			let addr = b1 as u16;
			let addr_field = symbol_address(labels, addr);

                        (format!("{:02X} {:02X}", opcode.code, b1),	 addr_field)
                    }
                    AddressingMode::ZeroPageX => {
                        let b1 = raw[i + 0x01];

                        (format!("{:02X} {:02X}", opcode.code, b1),
			 format!("${:02X},X", b1))
                    }
                    AddressingMode::ZeroPageY => {
                        let b1 = raw[i + 0x01];
                        (format!("{:02X} {:02X}", opcode.code, b1),
			 format!("${:02X},Y", b1))
                    }
                    AddressingMode::Absolute => {
                        let b1 = raw[i + 0x01];
                        let b2 = raw[i + 0x02];
                        let addr = LittleEndian::read_u16(&[b1, b2]);

			let addr_field = symbol_address(labels, addr);

                        (format!("{:02X} {:02X} {:02X}", opcode.code, b1, b2),
                         addr_field)
                    }
                    AddressingMode::AbsoluteX => {
                        let b1 = raw[i + 0x01];
                        let b2 = raw[i + 0x02];
                        let addr = LittleEndian::read_u16(&[b1, b2]);
			let addr_field = symbol_address(labels, addr);
                        (format!("{:02X} {:02X} {:02X}", opcode.code, b1, b2),
                         format!("{},X", addr_field))
                    }
                    AddressingMode::AbsoluteY => {
                        let b1 = raw[i + 0x01];
                        let b2 = raw[i + 0x02];
                        let addr = LittleEndian::read_u16(&[b1, b2]);
			let addr_field = symbol_address(labels, addr);
                        (format!("{:02X} {:02X} {:02X}", opcode.code, b1, b2),
                         format!("{},Y", addr_field))
                    }
                    AddressingMode::IndirectX => {
                        let b1 = raw[i + 0x01];
                        let addr_field = symbol_address(labels, b1 as u16);
                        (format!("{:02X} {:02X}", opcode.code, b1),
			            format!("({},X)", addr_field))
                    }
                    AddressingMode::IndirectY => {
                        let b1 = raw[i + 0x01];
                        let addr_field = symbol_address(labels, b1 as u16);
                        (format!("{:02X} {:02X}", opcode.code, b1),
			             format!("({}),Y", addr_field))
                    }
                    _ => (format!("{:02X}", opcode.code), "".into()),
                };

                let opcode_text = if self.disable_offsets {
                    if self.disable_opcodes {
                        format!("{}{}\n", opcode.mnemonic, val.1)
                    } else {
                        format!("{:<8} {}{}\n", val.0, opcode.mnemonic, val.1)
                    }
                } else {
		    let pc = (i as u16) + self.code_offset;

                    if self.disable_opcodes {
                        format!("{:04X}: {}{}",
                                pc,
                                opcode.mnemonic,
                                val.1)
                    } else {
                        format!("{:04X}: {:<8}  {} {}",
                                pc,
                                val.0,
                                opcode.mnemonic,
                                val.1)
                    }
                };
                //result.push((opcode_text.clone(), i as u16));
                i += opcode.length as usize;
		r.source = opcode_text;
            } else {
		// Unrecognized opcode
                let opcode_text = if self.disable_offsets {
                    format!("{:02X}        ???", { raw[i] })
                } else {
                    format!("{:04X}: {:02X}        ???",
                            i + self.code_offset as usize,
                            { raw[i] })
                };
                //result.push((opcode_text.clone(), i as u16));
                i += 0x01;
		r.source = opcode_text;
            }
	    result2.push(r);
	    decoded += 1;
	    if max_instr.is_some() && decoded >= max_instr.unwrap() {
     		    break
     		}
        }

        result2
    }

    /// Returns a Vector of Strings where each entry
    /// is a non-empty line of assembly instructions, with
    /// all leading and trailing whitespace removed.
    ///
    /// # Example
    ///
    /// ```
    /// use rs6502::Disassembler;
    ///
    /// assert_eq!(Disassembler::clean_asm("
    ///
    ///     0000 LDA #$20
    ///     0002 STA $4400
    ///
    /// "), &["0000 LDA #$20", "0002 STA $4400"]);
    /// ```
    pub fn clean_asm<I>(input: I) -> Vec<String>
        where I: Into<String>
    {
        input.into()
            .lines()
            .map(|line| line.trim())
            .map(String::from)
            .filter(|line| !line.is_empty())
            .collect()
    }
}


pub fn decode_address<MemAccess>(raw: &[u8], memory: MemAccess, x: u8, y: u8) -> Option<(u16, u8)>
    where MemAccess: Fn(u16) -> u8
{
    unsafe {
	let mode = ADDRESSING_MODES[raw[0] as usize];

	match mode {
            AddressingMode::Immediate => {
                None // No data
            }
            AddressingMode::Relative => {
		None // Only for jumps
            }
            AddressingMode::Indirect => {
                let b1 = raw[0x01];
                let b2 = raw[0x02];
                let addr = LittleEndian::read_u16(&[b1, b2]);
                Some((addr, memory(addr)))
            }
            AddressingMode::ZeroPage => {
                let addr = raw[0x01] as u16;
                Some((addr, memory(addr)))
            }
            AddressingMode::ZeroPageX => {
                let addr = raw[0x01] as u16;
		Some((addr, memory(addr.wrapping_add(x as u16))))
            }
            AddressingMode::ZeroPageY => {
                let addr = raw[0x01] as u16;
		Some((addr, memory(addr.wrapping_add(y as u16))))
            }
            AddressingMode::IndirectX => {
		// Read the value of the immediate byte. Use this
		// value + X (low nibble), and this value + X + 1
		// (high nibble) as an address

                let b1 = raw[0x01];
		let indir = b1.wrapping_add(x);
		let addr = LittleEndian::read_u16(&[indir, indir.wrapping_add(1)]);
		Some((addr, memory(addr)))
            }
            AddressingMode::IndirectY => {
		// Read the value of the immediate byte. Use this
		// value (low nibble), and this value + 1 (high
		// nibble) as an address. Add Y to this address

                let b1 = raw[0x01];
		let addr = LittleEndian::read_u16(&[memory(b1 as u16), memory(b1.wrapping_add(1) as u16)]).wrapping_add(y as u16);
                Some((addr, memory(addr)))
            }
            AddressingMode::Absolute => {
                let b1 = raw[0x01];
                let b2 = raw[0x02];
                let addr = LittleEndian::read_u16(&[b1, b2]);
		Some((addr, memory(addr)))
            }
            AddressingMode::AbsoluteX => {
                let b1 = raw[0x01];
                let b2 = raw[0x02];
                let addr = LittleEndian::read_u16(&[b1, b2]).wrapping_add(x as u16);
		Some((addr, memory(addr)))
            }
            AddressingMode::AbsoluteY => {
                let b1 = raw[0x01];
                let b2 = raw[0x02];
                let addr = LittleEndian::read_u16(&[b1, b2]).wrapping_add(y as u16);
		Some((addr, memory(addr)))
            }
            _ => None
	}
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_disassemble_basic_instructions() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0xA9, 0x20, 0x8D, 0x00, 0x44];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 LDA #$20
            0002 STA $4400

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn can_disassemble_indirect_jmp() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0x6C, 0x00, 0x44];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 JMP ($4400)

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn can_disassemble_relative_addressing() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0xA9, 0x20, 0x69, 0x10, 0xD0, 0xFA];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 LDA #$20
            0002 ADC #$10
            0004 BNE $0000

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn can_disassemble_zero_page_addressing() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0xA5, 0x35];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 LDA $35

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn can_disassemble_zero_page_indexed_addressing() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0x95, 0x44, 0x96, 0xFE];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 STA $44,X
            0002 STX $FE,Y

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn can_disassemble_absolute_addressing() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0x8D, 0x00, 0x44];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 STA $4400

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn can_disassemble_absolute_indexed_addressing() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0x9D, 0x00, 0x44, 0x99, 0xFE, 0xFF];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 STA $4400,X
            0003 STA $FFFE,Y

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn can_disassemble_indirect_indexed_addressing() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0x81, 0x44, 0x91, 0xFE];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 STA ($44,X)
            0002 STA ($FE),Y

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn can_disassemble_without_byte_offsets() {
        let dasm = Disassembler::with_code_only();
        let code: Vec<u8> = vec![0x81, 0x35, 0x91, 0xFE];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            STA ($35,X)
            STA ($FE),Y

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn move_memory_down_test() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0xA0, 0x00, 0xAE, 0x00, 0x00, 0xF0, 0x10, 0xB1, 0x02, 0x91, 0x03,
                                 0xC8, 0xD0, 0xF9, 0xEE, 0x02, 0x00, 0xEE, 0x03, 0x00, 0xCA, 0xD0,
                                 0xF0, 0xAE, 0x01, 0x00, 0xF0, 0x08, 0xB1, 0x02, 0x91, 0x03, 0xC8,
                                 0xCA, 0xD0, 0xF8, 0x60];

        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 LDY #$00
            0002 LDX $0000
            0005 BEQ $0017
            0007 LDA ($02),Y
            0009 STA ($03),Y
            000B INY
            000C BNE $0007
            000E INC $0002
            0011 INC $0003
            0014 DEX
            0015 BNE $0007
            0017 LDX $0001
            001A BEQ $0024
            001C LDA ($02),Y
            001E STA ($03),Y
            0020 INY
            0021 DEX
            0022 BNE $001C
            0024 RTS

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn test_memset_implementation() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0xA9, 0x00, 0xA8, 0x91, 0xFF, 0xC8, 0xCA, 0xD0, 0xFA, 0x60];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 LDA #$00
            0002 TAY
            0003 STA ($FF),Y
            0005 INY
            0006 DEX
            0007 BNE $0003
            0009 RTS

        "),
                   Disassembler::clean_asm(asm));
    }

    #[test]
    fn dumps_unknown_bytes() {
        let dasm = Disassembler::new();
        let code: Vec<u8> = vec![0xA9, 0xC8, 0x43];
        let asm = dasm.disassemble(&code);

        assert_eq!(Disassembler::clean_asm("

            0000 LDA #$C8
            0002 43

        "),
                   Disassembler::clean_asm(asm));
    }
}
