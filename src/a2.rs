// ![deny(unreachable_patterns)]


use std::fmt;
use std::collections::{VecDeque, HashMap};
use std::path::PathBuf;
use std::rc::Rc;
use std::cell::{RefCell, RefMut, Ref};
use log::{debug, warn, info, trace, error};
use realfft::num_traits::ToPrimitive;
use std::fs::File;
use std::io::{Read,Error};
use std::sync::{Arc, Mutex};



use crc::{Crc, CRC_16_IBM_SDLC};
const X25: Crc<u16> = Crc::<u16>::new(&CRC_16_IBM_SDLC);

use crate::sound::SoundRenderer;
use crate::speaker2::SpeakerRenderState;
use crate::diskii::{STOP_DRIVE1,START_DRIVE1};
use crate::mem::Mem;
use crate::emulator::{MonoColourSwitch, OutputQueues};
use crate::generic_peripheral::{IRQAction, IRQRequest, GenericPeripheral, DummyPeripheral, BreakPointTriggerRef, IrqChange};

pub const NB_SLOTS: usize = 8;

pub const BANKS_OF_INTEREST: [u16; 16]=[
	0x0000,0x200,0x400,0x800,0x2000,0x4000,
	0xC000,0xC100,0xC200,0xC300,0xC600,0xC700,0xC800,
	0xD000,0xE000,0xF000];

// FIXME DUPLICATE CODE !!!
pub fn hgr_address(y: usize) -> usize {
    let ofs = match y {
        0..=63 => 0,
        64..=127 => 0x28,
        128..=191 => 0x50,
        _ => panic!(),
    };

    let i = (y % 64) >> 3; // div by 8
    let j = (y % 64) % 8;

    ofs + 0x80 * i + 0x400 * j
}

pub const EMULATOR_SHORT_DESCRIPTION: &str = "an Apple 2e (PAL, rev. B) emulator";

// Number of frames per second of the simulated monitor
// (it's not the FPS of the PC screen)
// The emulator is tuned to render CPU, sound and graphics
// by unit of 1/FRAMES_PER_SECOND. So it is the responsibility
// of the host to convert the FRAMES_PER_SECOND rate
// to its display rate (which may be doomed to fail as 50fps
// will be hard to reproduce on a 60fps monitor...)

pub const FRAMES_PER_SECOND: usize = 50;
pub const CYCLES_PER_FRAME: usize = 20280; // 20280 / 65 == 312
pub const CYCLES_PER_LINE: usize = 65;
pub const FIRST_NON_BLANK_LINE: usize = 60; // First beamer line on screen which is not in VBL (so drawing).
pub const CYCLES_PER_SECOND: usize = CYCLES_PER_FRAME * FRAMES_PER_SECOND;
// +1 on both makes MADEF.dsk works, but breaks oldskool.dsk :-/
pub const HBL_CYCLES: usize = 25;
pub const BYTES_PER_LINE: usize = 40;

/// VBL bit (C019) area end. The VBL bit is 1 as soon as something is drawn on
/// the screen. Therefore, it occurs on the first drawn horizontal line but
/// right after the HBL part.
pub const FIRST_VISIBLE_CYCLE: usize = CYCLES_PER_LINE*FIRST_NON_BLANK_LINE - 1;
pub const LAST_VISIBLE_CYCLE: usize = CYCLES_PER_LINE*(FIRST_NON_BLANK_LINE+192);

// Flash frequency as reported in figure 3.8 of UTAe
pub const FLASH_FREQUENCY: f32 = 1.87; // Hz

const SPEAKER: u16 = 0xC030;

#[derive(Debug, PartialEq, Eq, Display, Copy, Clone)]
pub enum AppleModel {
    IIe,
    IIc
}
const APPLE_MODEL:AppleModel = AppleModel::IIe;

// See Sather Bus Structure of the Apple II 2-44

// If I understood staher well, kdb is replicated
// on C000-C00F and strobe on C010-C01F. According
// to apple2 rom, C01F is special and not the keyboard
// Also, they use only C000 and C010. If I do that,
// kbd works better (see choplitfer+.dsk menu)

// Sather p. 2-16:

// The MPU reads the keyboard input via a read access to $0000. Any read access
// in the $C00X range can be used for this purpose, but the programming
// convention is to use $C000. When the MMU detects a read to $C00X on the
// address bus, it pulls the enabling KBD' signal low (*). This results in the
// transfer of the 7-bit ASCII of the last keypress from the keyboard ROM to
// MD0-MD6 of the data bus. Additionally, the lOU detects the read to $C00X and
// places the state of its KEYSTROBE soft switch on MD7 of the data bus. The MPU
// thus reads the state of KEYSTROBE and the latched ASCII of the last keypress
// with a single access to $C00X.

// The KEYSTROBE soft switch is set by the KSTRB signal which goes high
// momentarily any time a matrix key is pressed. KSTRB is output by the keyboard
// encoder and processed inside the IOU. The strobe soft switch is reset when
// the MPU makes a _read_ access to $C010 or a _write_ access to $C01X, This
// provides programmers with a means of detecting a keypress and
// distinguishing between multiple keypresses The program polls $C000 until it
// finds the MSB high (KEYSTROBE). Then it resets KEYSTROBE, processes the
// ASCII, then resumes polling $C000.

// A second flag related to the keyboard is the AKD (any key dow n) flag, read
// at $C010. The AKD signal is routed from the keyboard encoder to the lOU and
// relayed to MD7 when the IOU detects a read to $0010. This gives programmers a
// little more versa- tility in interpreting keypresses. Not« that reading the
// AKD flag also resets the KEYSTROBE soft switch.

const KBD_IN_RNG_START:u16 = 0xC000;
const KBD_IN_RNG_END:u16 = 0xC00F;
const KBD_STROBE_RNG_START: u16 = 0xC010;
const KBD_STROBE_RNG_END: u16 = 0xC01F;

const ALTCHARSET_READ: u16 = 0xC01E;
const ALTCHARSET_SET_ON: u16 = 0xC00F;  // write switch (cf. table 8.6, p. 8-20 in UTAE)
const ALTCHARSET_SET_OFF: u16 = 0xC00E; // write switch
pub const PAGE2_OFF: u16 = 0xC054;
pub const PAGE2_ON: u16 = 0xC055;
const PAGE2_READ: u16 = 0xC01C;
pub const MIXED_OFF: u16 = 0xC052;
pub const MIXED_ON: u16 = 0xC053;
const MIXED_READ: u16 = 0xC01B;
pub const TEXT_OFF: u16 = 0xC050;
pub const TEXT_ON: u16 = 0xC051;
const TEXT_READ: u16 = 0xC01A;

pub const HIRES_OFF: u16 = 0xC056; // for IIe; IIc tech manual says : 0xC057
pub const HIRES_ON: u16 = 0xC057; // for IIe; IIc tech manual says : 0xC058

const HIRES_READ: u16 = 0xC01D;
const TAPEIN: u16 = 0xC068;

const COL80_ON: u16 = 0xC00D; // write switch (cf. table 8.6, p. 8-20 in UTAE)
const COL80_OFF: u16 = 0xC00C; // write switch
const COL80_READ: u16 = 0xC01F;

const STORE80_ON: u16 = 0xC001; // write switch (cf. table 8.6, p. 8-20 in UTAE)
const STORE80_OFF: u16 = 0xC000; // write switch
const STORE80_READ: u16 = 0xC018;

// IOU = Input/Output Unit
// See A2c tech ref page 145
const IOUDIS_ON: u16 = 0xC07E;
const IOUDIS_OFF: u16 = 0xC07F;
const RDIOUDIS: u16 = 0xC07E;
pub const AN3_OFF: u16 = 0xC05E;
pub const AN3_ON: u16 = 0xC05F;
const DHIRES_READ: u16 = 0xC07F;

const BANK1_READ: u16 = 0xC011; // See "***" in UtA2E Tale 5.4.
const RAMRD_READ: u16 = 0xC013; // read
const RAMWRT_READ: u16 = 0xC014; // read
const CXROM_READ: u16 = 0xC015;
const ALTZP_READ: u16 = 0xC016;
const VERTBLANK: u16 = 0xC019;
const TAPEOUT_RNG_START: u16 = 0xC020;
const TAPEOUT_RNG_END: u16 = 0xC02F;

const RAMRD_ON: u16 = 0xC003; // write to set on
const RAMRD_OFF: u16 = 0xC002; // write to set off
const RAMWRT_ON: u16 = 0xC005; // write to set on
const RAMWRT_OFF: u16 = 0xC004; // write to set off
const ALTZP_ON: u16 = 0xC009; // write to set on
const ALTZP_OFF: u16 = 0xC008; // write to set off
const C3ROM_ON: u16 = 0xC00B;
const C3ROM_OFF: u16 = 0xC00A;
const C3ROM_READ: u16 = 0xC017;

const SLOTCXROM_ON: u16 = 0xC007; // write to set on
const SLOTCXROM_OFF: u16 = 0xC006; // write to set off

const PADDLE0: u16 = 0xC064;
const PADDLE1: u16 = 0xC065;
// Read Apple2C tech ref and Sather's page 7-24
const PADDLE2: u16 = 0xC066;
const PADDLE3: u16 = 0xC067;
const PADDLE_TIMER: u16 = 0xC070;
// Make code a little more easy to read
const SET: bool = true;
const RESET: bool = false;

use strum_macros::Display;
use std::hash::Hash;

use crate::apple_key::{key_to_kbd_code, Key, KeyAction, KeyEvent, KeyboardQueue};


#[derive(Copy, Clone, PartialEq, Eq)]
pub enum OnOff {
    On, Off, No
}
pub const ON: OnOff = OnOff::On;
pub const OFF: OnOff = OnOff::Off;


impl fmt::Display for OnOff {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            OnOff::On => write!(f, "on "),
            OnOff::Off => write!(f, "off"),
            OnOff::No => write!(f, "---")
        }
    }
}

fn bool2oo(b:bool) -> OnOff {
    if b {OnOff::On} else {OnOff::Off}
}


#[derive(Copy, Clone)]
pub struct ModeInfo {
    pub col80: OnOff,
    pub store80: OnOff,
    pub page2: OnOff,
    pub text: OnOff,
    pub mixed: OnOff,
    pub hires: OnOff,
    pub dhires: OnOff
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct VideoSoftSwitches {
	pub text: u8,
	pub hires: u8,
	pub page2: u8,
	pub col80: u8,
	pub an3: u8,
	pub modemix: u8,
	pub store80: u8,
	pub altcharset: u8
}

pub const SOFT_SWITCH_BIT_GR:u32 = 0; // 0x01
pub const SOFT_SWITCH_BIT_MODEMIX:u32 = 1; // 0x02
const SOFT_SWITCH_BIT_AN3:u32 = 2; // 0x04
pub const SOFT_SWITCH_BIT_POS_TEXT:u32 = 3; // 0x08
const SOFT_SWITCH_BIT_POS_COL80:u32 = 4; // 0x10
const SOFT_SWITCH_BIT_POS_HIRES:u32 = 5; // 0x20
const SOFT_SWITCH_BIT_POS_ALT_CHARSET:u32 = 6; // 0x40
//const SOFT_SWITCH_BIT_PAGE2:u32 = 7;

impl VideoSoftSwitches {

	pub fn to_bit_flags(&self) -> u8 {
		// if false && self.gr > 0 && self.hires > 0 {
		// 	(1 << SOFT_SWITCH_BIT_POS_TEXT) | (0 << SOFT_SWITCH_BIT_POS_HIRES) | (self.page2 << SOFT_SWITCH_BIT_PAGE2)
		// } else {
			  (self.modemix << SOFT_SWITCH_BIT_MODEMIX)
			| (self.an3 << SOFT_SWITCH_BIT_AN3)
			| (self.text << SOFT_SWITCH_BIT_POS_TEXT)
			| (self.col80 << SOFT_SWITCH_BIT_POS_COL80)
			| (self.hires << SOFT_SWITCH_BIT_POS_HIRES)
			// | (self.page2 << SOFT_SWITCH_BIT_PAGE2)
			| (self.altcharset << SOFT_SWITCH_BIT_POS_ALT_CHARSET)
		// }
	}

	pub fn to_string(&self) -> String {
		format!("{} {} {} {} {} {}",
			if self.text > 0  {"TXT"} else {"txt"},
			if self.col80 > 0 {"COL80"} else {"col80"},
			if self.hires > 0 {"HI"} else {"hi"},
			if self.page2 > 0 {"PAGE2"} else {"page2"},
			if self.an3 > 0 {"AN3"} else {"an3"},
			if self.modemix > 0 {"MIX"} else {"mix"},
		)
	}
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct GfxMode {
    pub mode: GfxBaseMode,
    /* If mixed is set to Some, then we have mixed mode and
    mixed gives the mode at the bottom part of the screen). */
    pub mixed: Option<GfxBaseMode>,
    pub page2: bool,
    pub an3: u8,
	pub altcharset: u8,
	//private_constructor: bool
	pub video_flags: VideoSoftSwitches
}

impl GfxMode {


	pub fn set_base_mode(&mut self, base_mode: GfxBaseMode) -> GfxBaseMode {
		let old_mode = self.mode;
		self.mode = base_mode;
		old_mode
	}

	pub fn ram_base(&self) -> u16 {
		//let video_ram_low = if state.mode.page2 { 0x800 } else { 0x400 };
		if self.mode == GfxBaseMode::Dgr || self.mode == GfxBaseMode::Gr || self.mode == GfxBaseMode::Text40  || self.mode == GfxBaseMode::Text80 {
			if self.video_flags.page2 > 0{
				0x800
			} else {
				0x400
			}
		} else if self.video_flags.page2 > 0{ 0x4000 } else { 0x2000 }
	}

	pub fn is_mem_visible(&self, offset: usize) -> bool {
		// Don't optimze too fast as some mode have 2 address fields...
		let (begin, end) = if self.mode == GfxBaseMode::Gr || self.mode == GfxBaseMode::Text40  {
			if self.page2 {
				(0x800, 0x800+0x400 )
			} else {
				(0x400 , 0x400+0x400)
			}
		} else if self.page2 { (0x4000, 0x4000+0x2000) } else { (0x2000, 0x2000+0x2000) };

		begin <= offset && offset < end
	}

	/* pub fn code_for_line(&self, y:usize) -> usize {
		assert!(y < 192);

		// page2 and an3 are two bits
		let mut r: usize = if self.page2 {1} else {0} +
			(self.an3 as usize & 1) << 1;

		if y < 160 {
			// Non mixed part of the screen
			r |= (self.mode as usize) << 2;
		} else if let Some( mode2) = self.mixed {
			r |= (mode2 as usize) << 2;
		}

		return r;
	} */

}

#[derive(Debug, PartialEq, Eq, Display, Copy, Clone)]
#[repr(usize)]
pub enum GfxBaseMode {
    Text40 = 1,
    Text80 = 2,
    Gr = 3, // 40*48
    Hgr = 4,
    Dhgr = 5,
    Dgr = 6 // 80x48
}




fn bool_to_bit7(v: bool) -> u8 {
    // Any time you read a soft switch, you get a byte of data.  However, the
    // only information the byte contains is the state of the switch, and
    // this occupies only one bit — bit 7, the high-order bit. The other bits
    // in the byte are unpredictable.
    // (see page 29 of AIIe Ref Man)
    // See table 2-10 in AIIe Ref Man

    if v {
	0x80
    } else {
	0x00
    }
}


#[derive(PartialEq)]
pub enum ALTCHARSETSelect {
    // See table 2-10 in apple2e ref
    Off = 0x0E,
    On = 0x0F,
}


#[derive(PartialEq)]
enum CXROMSelect {
    Off = 1,
    On = 2,
}

#[derive(Clone,Copy)]
pub enum MemoryBankType {
	/* Here I follow UA2e's convention (fig. 5.11): Bank2
	is in the RAM/AUXRAM and Bank1 is outside.

	Although Bank1 and AuxBank1 are a bit artificial (since
	they are inside RAM and AUXRAM), I prefer to keep them
	aside so that the code explanation is easier.
	 */
    Motherboard,
    Bank1,
    Bank2,
    Aux,
    AuxBank1,
    AuxBank2,
    AppleRom,
    DiskRom
}

impl fmt::Display for MemoryBankType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MemoryBankType::AppleRom => write!(f, "AppleRom"),
            MemoryBankType::Aux => write!(f, "Aux"),
            MemoryBankType::AuxBank2 => write!(f, "AuxBank2"),
            MemoryBankType::AuxBank1 => write!(f, "AuxBank1"),
            MemoryBankType::Bank2 => write!(f, "Bank2"),
            MemoryBankType::Bank1 => write!(f, "Bank1"),
            MemoryBankType::DiskRom => write!(f, "DiskRom"),
            MemoryBankType::Motherboard => write!(f, "Motherboard"),
        }
    }
}

impl MemoryBankType {
	pub fn short_str(&self) -> &str {
        match self {
            MemoryBankType::AppleRom => "Rom",
            MemoryBankType::Aux => "AuxRam",
            MemoryBankType::AuxBank2 => "AuxBnk2",
            MemoryBankType::AuxBank1 => "AuxBnk1",
            MemoryBankType::Bank2 => "Bnk2",
            MemoryBankType::Bank1 => "Bnk1",
            MemoryBankType::DiskRom => "DskRom",
            MemoryBankType::Motherboard => "Ram",
        }

	}
}

impl fmt::Display for CXROMSelect {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
	match self {
	    CXROMSelect::Off => write!(f,"SLOTCXROM = OFF"),
	    CXROMSelect::On => write!(f,"SLOTCXROM = ON")
        }
    }
}

#[derive(PartialEq)]
enum C3ROMSelect {
    Off = 1,
    On = 2
}


impl fmt::Display for C3ROMSelect {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
	match self {
	    C3ROMSelect::Off => write!(f,"SLOTC3ROM = OFF"),
	    C3ROMSelect::On => write!(f,"SLOTC3ROM = ON")
        }
    }
}

const DISK_ROM: [u8; 0x100] = [
    0xA2, 0x20, 0xA0, 0x00, 0xA2, 0x03, 0x86, 0x3C, 0x8A, 0x0A, 0x24, 0x3C, 0xF0, 0x10, 0x05, 0x3C,
    0x49, 0xFF, 0x29, 0x7E, 0xB0, 0x08, 0x4A, 0xD0, 0xFB, 0x98, 0x9D, 0x56, 0x03, 0xC8, 0xE8, 0x10,
    0xE5, 0x20, 0x58, 0xFF, 0xBA, 0xBD, 0x00, 0x01, 0x0A, 0x0A, 0x0A, 0x0A, 0x85, 0x2B, 0xAA, 0xBD,
    0x8E, 0xC0, 0xBD, 0x8C, 0xC0, 0xBD, 0x8A, 0xC0, 0xBD, 0x89, 0xC0, 0xA0, 0x50, 0xBD, 0x80, 0xC0,
    0x98, 0x29, 0x03, 0x0A, 0x05, 0x2B, 0xAA, 0xBD, 0x81, 0xC0, 0xA9, 0x56, 0x20, 0xA8, 0xFC, 0x88,
    0x10, 0xEB, 0x85, 0x26, 0x85, 0x3D, 0x85, 0x41, 0xA9, 0x08, 0x85, 0x27, 0x18, 0x08, 0xBD, 0x8C,
    0xC0, 0x10, 0xFB, 0x49, 0xD5, 0xD0, 0xF7, 0xBD, 0x8C, 0xC0, 0x10, 0xFB, 0xC9, 0xAA, 0xD0, 0xF3,
    0xEA, 0xBD, 0x8C, 0xC0, 0x10, 0xFB, 0xC9, 0x96, 0xF0, 0x09, 0x28, 0x90, 0xDF, 0x49, 0xAD, 0xF0,
    0x25, 0xD0, 0xD9, 0xA0, 0x03, 0x85, 0x40, 0xBD, 0x8C, 0xC0, 0x10, 0xFB, 0x2A, 0x85, 0x3C, 0xBD,
    0x8C, 0xC0, 0x10, 0xFB, 0x25, 0x3C, 0x88, 0xD0, 0xEC, 0x28, 0xC5, 0x3D, 0xD0, 0xBE, 0xA5, 0x40,
    0xC5, 0x41, 0xD0, 0xB8, 0xB0, 0xB7, 0xA0, 0x56, 0x84, 0x3C, 0xBC, 0x8C, 0xC0, 0x10, 0xFB, 0x59,
    0xD6, 0x02, 0xA4, 0x3C, 0x88, 0x99, 0x00, 0x03, 0xD0, 0xEE, 0x84, 0x3C, 0xBC, 0x8C, 0xC0, 0x10,
    0xFB, 0x59, 0xD6, 0x02, 0xA4, 0x3C, 0x91, 0x26, 0xC8, 0xD0, 0xEF, 0xBC, 0x8C, 0xC0, 0x10, 0xFB,
    0x59, 0xD6, 0x02, 0xD0, 0x87, 0xA0, 0x00, 0xA2, 0x56, 0xCA, 0x30, 0xFB, 0xB1, 0x26, 0x5E, 0x00,
    0x03, 0x2A, 0x5E, 0x00, 0x03, 0x2A, 0x91, 0x26, 0xC8, 0xD0, 0xEE, 0xE6, 0x27, 0xE6, 0x3D, 0xA5,
    0x3D, 0xCD, 0x00, 0x08, 0xA6, 0x2B, 0x90, 0xDB, 0x4C, 0x01, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00];



pub struct Ram {
    // Rust won't allocate byte array of unknown size
    // Box<> has the same problem.
    // So one can use unsafe stuff or vec...
    pub name: String,
    is_writable: bool,
    ram: Vec<u8>
}

impl Ram {
    pub fn new( name: &str, size: usize, writable: bool) -> Ram {
	Ram {
	    name: String::from(name),
	    ram: vec![0; size],
	    is_writable: writable
	}
    }

    pub fn load_bytes(&mut self, data: &[u8], offset: usize) {
	self.ram[offset .. offset + data.len()].copy_from_slice( data);
    }

    pub fn load_data(&mut self, fname:&PathBuf, offset:usize) {
	let data = &std::fs::read(fname).unwrap()[..];
	info!("Loading {} bytes from {} at ${:04X}", data.len(), fname.to_string_lossy(), offset);
	self.ram[offset .. offset + data.len()].copy_from_slice( data);
    }

    pub fn slice(&self, rng: std::ops::Range<usize>) -> &[u8] {
	&self.ram[rng]
    }

    // pub fn get(&self, addr:usize) -> u8 {
    // 	return self.ram[addr];
    // }

    // pub fn set(&mut self, addr:usize, val: u8){
    // 	self.ram[addr] = val
    // }
}

use std::ops::{Index, IndexMut};
impl Index<usize> for Ram {
    type Output = u8;
    fn index(&self, i: usize) -> &u8 {
        &self.ram[i]
    }
}

impl IndexMut<usize> for Ram {
    fn index_mut(&mut self, i: usize) -> &mut u8 {
	if self.is_writable {
            &mut self.ram[i]
	} else {
	    panic!("Writing to ROM at ${:04X} ???", i)
	}
    }
}


pub enum ClockEventData {
    GfxMemWrite { addr: u16, data: u8 },
    GfxAuxMemWrite { addr: u16, data: u8 },
    GfxModeChange { mode: GfxMode },
    SpeakerIORead { addr: u16 },
    MockingBoardWrite { register: u8, data: u8 },
    MockingBoardReset
}

pub struct ClockEvent {
    pub tick: u64,
    pub event: ClockEventData
}



pub struct AppleII
{
    pub clock_events : VecDeque<ClockEvent>,
    pub speaker_events : VecDeque<ClockEvent>, // FIXME Merge with cock_events

    pub planned_irq : Vec<IRQRequest>,
    pub last_irq_delay: usize,

    //cpu_state : CpuState,
    pub mb_ram: Ram,
    pub apple_rom: Ram,
    pub charset_rom: Vec<u8>,
    pub aux_ram: Ram,
    //pub bank1_ram: Ram,
    pub bank1_ram: Ram,
    //pub bank1_auxram: Ram,
    pub bank1_auxram: Ram,
    //aux: LangCardState,
    nreads: u16, // counts # of reads for noise() fn

    // FIXME Option is not necessary anymore
    // FIXME Rc<...> might not be necessary anymore 'cos this class
    //       may be end up owning all the peripherals
    slots: Vec<Option<Rc<RefCell<dyn GenericPeripheral>>>>,

    // See Table 5-4 in Understanding A2
    col80: bool,
    store80: bool,
    ramrd: bool,
    ramwrt: bool,
    cxrom_select: CXROMSelect,
    altzp: bool,
    c3rom_select: C3ROMSelect,
    pub page2: bool,
    hires: bool,
    dhires: bool,
    iou_disabled : bool,
    bank1: bool, // True = bank1 selected; False= bank2.
    pub hramrd: bool,
    hramwrt: bool, // In Understanding A2, this is called HRAMWRT' (table 5.4)
    prewrite:bool,

    pub altcharset_select: ALTCHARSETSelect,
    pub mixed: bool,
    text_switch: bool,
    an3_cycle: bool,
    pub mos6522_irq_tick: u64,

    _kbd_strobe:Option<u64>,
	last_kbd_strobe_clear_cycle: u64,
	pub keyboard_queue: KeyboardQueue,
	keys_events_record: VecDeque<(Key, u64)>,
    analog_input_reset_cycle: Vec<u64>,
    analog_input_value: Vec<Option<usize>>,
    switch_input_value: Vec<bool>,
    //pub last_set_gfx_mode: GfxMode,
    pub last_vblank_read: u64,
    pub speaker: Rc<RefCell<SpeakerRenderState>>,
	mem_setup_change: bool, // Set to True each time the memory layout is changed
	bpt: Option<BreakPointTriggerRef>,
    //keys_events: Vec<KeyAction>
	pub motherboard_mono_colour_switch: MonoColourSwitch
 }



impl AppleII
{
    pub fn new(sample_rate: usize, output_queues: Arc<Mutex<OutputQueues>>) -> AppleII {
	let mut r = AppleII {
	    //cpu_state: CpuState::new(),

	    // Like in Shamus, init RAM to zero.
	    // mem:   [ 0; 0x13000 ],

	    // // The Apple "internal" ROM.
	    // apple_rom: [0 as u8; 0x13000],

	    // aux_ram: [0; 0x13000],
	    // bank1_ram: [0; 0x1000],
	    // bank2_ram: [0; 0x1000],

	    clock_events: VecDeque::new(),
	    speaker_events: VecDeque::new(),
	    planned_irq : Vec::new(),
	    last_irq_delay: 0,

	    // See figure 2-3 in Apple IIc Reference Manual - Volume 1.pdf

	    // Main ram : 0x0000 to 0xBFFF and 0xE000 to 0xFFFF
	    mb_ram: Ram::new("Motherboard RAM", 0x13000, true),
	    apple_rom: Ram::new("Apple ROM", 0x13000, false),
	    charset_rom: Vec::new(),
	    // Aux ram : 0x0000 to 0xBFFF and 0xE000 to 0xFFFF (like MainRam)
	    aux_ram: Ram::new("AUX", 0x13000, true),
	    // MainRam bank1 0xD000 -> 0xDFFF
	    //bank1_ram: Ram::new("Bank1", 0x13000, true),
	    // MainRam bank2 0xD000 -> 0xDFFF (switched)
	    bank1_ram: Ram::new("Bank2", 0x13000, true),
	    // AuxRam bank1 0xD000 -> 0xDFFF
	    //bank1_auxram: Ram::new("Bank1 AUX", 0x13000, true),
	    // AuxRam bank2 0xD000 -> 0xDFFF (switched)
	    bank1_auxram: Ram::new("Bank2 AUX", 0x13000, true),

	    // TODO: slots: [ None, ..8 ],
	    // https://gist.github.com/carl-eastlund/6264938
	    slots: Vec::new(),

	    //aux:   LangCardState::new(false, 1, true),
	    nreads: 0,
	    cxrom_select: CXROMSelect::Off,
	    c3rom_select: C3ROMSelect::Off,

	    col80: false,
	    store80: false,

		// On page 202 of the new UTAE, a table says that ALTCHRSET is "flashing
		// active" after reset. On table 8.4's header, flashing active means
		// ALTCHRSET == 0.
		// altchrset is set on a IIe, see remark 6 on page I-7 of UTAe

	    altcharset_select: ALTCHARSETSelect::Off,
	    page2: false,
	    mixed: false,
	    text_switch: false,
	    an3_cycle: false,
	    hires: false,
	    dhires: false,
	    iou_disabled: true,

	    /* A2e Ref Man : "When you turn power on or reset the
	    Apple IIe, it initializes the bank switches for reading the ROM and
	    writing the RAM, using the second bank of RAM."
	    */

	    hramrd: false, // Read ROM
	    hramwrt: true, // Write RAM
	    bank1: false, // Bank 2

	    altzp: false,
	    prewrite: false,
	    ramrd: false,
	    ramwrt: false,

	    mos6522_irq_tick: 0xFFFFFFFFFFFFFFFF, // FIXME Prefer Option<u64>

	    _kbd_strobe:None,
		last_kbd_strobe_clear_cycle: 0,
		keyboard_queue: KeyboardQueue::new(),
		keys_events_record: VecDeque::new(),
	    analog_input_reset_cycle: vec![0; 4],
	    analog_input_value: vec![None; 4],
	    switch_input_value: vec![false; 3],
	    //speaker: Rc::new(RefCell::new(SpeakerRenderState::new(CYCLES_PER_FRAME * sample_rate / CYCLES_PER_SECOND)))
	    // last_set_gfx_mode: GfxMode::new(),
	    last_vblank_read: 0,
	    speaker: Rc::new(RefCell::new(SpeakerRenderState::new(sample_rate,
								  output_queues))),
		mem_setup_change: false,
		bpt:None,
		motherboard_mono_colour_switch: MonoColourSwitch::Colour
		//keys_events: Vec::new()
	};

	// In the FT Fort et vert demo, they rely on memory to have some zero.
	// BLOAD the demo and look at memory at 2FF0
	for i in 0x000..0xC000 {
	    r.mb_ram[i] = ((((i+2) >> 1) & 1)*0xFF) as u8 ;
	}
	for i in 0x400..0xC00 {
	    r.mb_ram[i] = (i & 0xF) as u8;
	}

	r.load_default_charset_rom();
	r.load_default_main_rom();

	// Slot 0 doesn't exist, it's just here to help with
	// the indexing
	// FIXME provide a sensible accessor

	// Wikipedia: Apple had seven slots at least
	// FIXME Apple IIe had an auciliary slot for 80col card: how to implement ath aux thing ?
	for s in 0..NB_SLOTS { // remember, end of range is not included !
	    r.slots.push(Some(Rc::new(RefCell::new(DummyPeripheral::new(s)))));
	}

	// let mut mos6522: Box<MOS6522> = Box::new(MOS6522::new(&mut machine));
	//r.set_slot(4, Box::new(MOS6522::new(&mut r)));
	r
    }

	pub fn has_mem_layout_changed(&mut self) -> bool {
		let r = self.mem_setup_change;
		self.mem_setup_change = false;
		r
	}
    pub fn power_up_reset(&mut self, cycle: u64) {
	/* Sather's 4-45:

	The power-up byte ($3F4) must be the
	exclusive-OR between $A5 and the contents of $3F3,
	or a power-up RESET will be performed when
	RESET is pressed.
	 */

	// See also https://github.com/AppleWin/AppleWin/blob/dfb8802763f3d10ec374e035ef9ecea88ee181c6/source/Memory.cpp
	// Test with BugBattle, Eliminator.

	// debug!("0x3F3=${:02X}, 0x3F4=${:02X}, 0x3F3 ^ A5={:02X}",
	//        self.mb_ram[0x3F3], self.mb_ram[0x3F4],
	//        self.mb_ram[0x3F3] ^ 0xA5);

	self.mb_ram[0x3F4] = 0xFF;
	self.mb_ram[0x3F3] = 0xFE;

	for i in 0xD000..0xFFFF {
		self.mb_ram[i] = 0;
	}

	// These two seems important to be able to reset
	// from eliminator.
	self.cxrom_select = CXROMSelect::Off;
	self.c3rom_select = C3ROMSelect::Off;

	// Enable ROM access
	self.hramrd = RESET;
	self.hramwrt = SET;
	self.ramrd = RESET;
	self.ramwrt = RESET; // Not SET !

	// Changing these doesn't affect BugBattle
	// self.bank1 = true;
	// self.altzp = true;
	// self.prewrite = false;
	// self.store80 = false;
	// self.altcharset_select = ALTCHARSETSelect::Off;

	// When resetting, BRK will trigger a irq-like behvaiour
	// calling whatever is at $FFFC/D
	// debug!("ROM 0xFFFC=${:02X}, 0xFFFD=${:02X}",
	//        self.apple_rom[0xFFFC], self.apple_rom[0xFFFD]);
	// debug!("Next read at $FFFC: {}", self.which_memory_bank(0xFFFC, false));
	// debug!("Next read at $03F4: {}", self.which_memory_bank(0x3F4, false));


	// FIXME Maybe I shouldn't init these because after reset, the
	// BIOS will update them. Doesn't it ?
	self.col80 = false;
	self.store80 = false;
	self.page2 = false;
	self.text_switch = true;
	self.mixed = false;
	self.hires = false;
	self.dhires = false;
	self.an3_cycle = false;
	self.gfx_mode_change_clock_event(cycle);

	for x in 0..self.slots.len() {
		self.slot_mut(x).reset();
	}

    }


    pub fn tick_floppy(&mut self, data_on_bus: u8) {
		// FIXME Don't hard code that 6
		self.slot_mut(6).cycle_tick(data_on_bus)
    }


    pub fn add_peripheral(&mut self, p: Rc<RefCell<dyn GenericPeripheral>>) {
	let s = p.borrow().get_slot();
	self.slots[s] = Some(p);
    }

    fn slot_mut(&mut self, n:usize) -> RefMut<dyn GenericPeripheral>  {
		self.slots[n].as_mut().expect("Empty slot ?").borrow_mut()
    }

	pub fn reset_slot(&mut self, n:usize) {
		self.slot_mut(n).reset();
    }

    pub fn slot(&self, n:usize) -> Ref<dyn GenericPeripheral>  {
		self.slots[n].as_ref().expect("Empty slot ?").borrow()
    }

    pub fn slots_status(&self, cycle: u64) -> Vec<String> {
		let mut s = Vec::new();

		for n in 0..self.slots.len() {
			let ss = if self.slots[n].is_some() {
				self.slots[n].as_ref().expect("sss").borrow().status_str(cycle)
			} else {
				String::from("")
			};
			s.push(format!("{:04X}:{}",n*256 + 0xC000,ss));
		}

		fn analog_to_str(a2: &AppleII , ndx: usize, cycle: u64) -> String {
			// Status of analog devices (joystick)
			let mut s = if let Some(v) = a2.analog_input_value[ndx] {
				format!("${:02X}", v)
			} else {
				"?".to_string()
			};

			s.push_str(format!(" button:{}", if a2.switch_input_value[ndx] {"Pushed"} else {"Released"}).as_str());
			let rst = if a2.analog_input_reset_cycle[ndx] > 0 {
				format!("reset {}", cycle - a2.analog_input_reset_cycle[ndx])
			} else {
				"".to_string()
			};
			s.push_str(&rst);
			s
		}

		let ana1 = analog_to_str(self, 0, cycle);
		let ana2 = analog_to_str(self, 1, cycle);

		s.push(format!("Paddles: [1] {} [2] {}", ana1, ana2));
		s
    }

    pub fn process_irq_requests(&mut self, irq_requests: Vec<IRQRequest>, current_tick: u64) {
		trace!("process_irq_requests: {} irq pending", self.planned_irq.len());
		let mut has_new_cycles = false;
		for irq_request in irq_requests {
			trace!("process_irq_requests: src:{} sub_src:{}", irq_request.source_slot, irq_request.sub_source);

			if irq_request.action == IRQAction::Replace || irq_request.action == IRQAction::ClearAll {
				// Keep requests which are not like the new one.
				// (so remove all requests which are like the new one)
				self.planned_irq.retain(|r|
							!(r.source_slot == irq_request.source_slot &&
							r.sub_source == irq_request.sub_source));
			};

			if irq_request.action == IRQAction::Replace {
				assert!(irq_request.irq_cycle > current_tick,
					"You're not planning in the future... {} is not > {}, coming from source:{}/{}",
					irq_request.irq_cycle, current_tick,
					irq_request.source_slot, irq_request.sub_source);
					self.planned_irq.push(irq_request);
				has_new_cycles = true;
				// Always keep the next IRQ to occur at the beginning of the vector.
			};
			// A ClearAll action will not lead to a call to ackIRQ.
		}
		if has_new_cycles {
			self.planned_irq.sort_by(|a,b| a.irq_cycle.cmp(&b.irq_cycle));
		}
		trace!("process_irq_requests: {} irq pending", self.planned_irq.len());
    }

    pub fn handle_first_available_irq(&mut self, cycle: u64) -> Option<IrqChange> {
		// FIXME Removing the first elment is o(n) => you
		// should reverse the order of the vector...

		// Get the first IRQ in the list an dremove it from there.
		let irq_req = self.planned_irq.remove(0);
		assert!(irq_req.irq_cycle == cycle, "You're messing things up");

		// For the debugger
		// If we're late, cycle > req.irq_cycle, a positive delay is noted.
		self.last_irq_delay += (cycle as usize) - (irq_req.irq_cycle as usize);

		// Ask the source of the IRQ to acknowledge it
		let (reqs, irq_change) = self.slots[irq_req.source_slot].as_mut().expect("Empty slot ?").borrow_mut().ack_irq(irq_req, cycle);
		// The source may want to trigger new additional IRQ's, for ex. timers.
		if reqs.is_some() {
			self.process_irq_requests(reqs.unwrap(), cycle);
		};

		irq_change
    }

    pub fn ram(&self) -> &[u8] {
		// Main RAM (0-BFFF), Rom(C000-CFFF) Bank1 Ram (D000-DFFF) and Main Ram (E000-FFFF)
		self.mb_ram.slice(0..0x10000)
    }

    pub fn ram_bank2(&self) -> &[u8] {
	self.bank1_ram.slice(0xD000..0xE000)
    }

    pub fn aux_ram(&self) -> &[u8] {
	self.aux_ram.slice(0..0x10000)
    }

    pub fn aux_ram_bank2(&self) -> &[u8] {
	self.bank1_auxram.slice(0xD000..0xE000)
    }


    pub fn hgr2_ram(&self) -> &[u8] {
	self.mb_ram.slice(0x4000..0x6000)
    }

    pub fn gfx_mode_info(&self) -> ModeInfo {
	ModeInfo{col80: bool2oo(self.col80),
		 store80: bool2oo(self.store80),
		 page2: bool2oo(self.page2),
		 text: bool2oo(self.text_switch),
		 mixed: bool2oo(self.mixed),
		 hires: bool2oo(self.hires),
		 dhires: bool2oo(self.dhires)
	}
    }

    pub fn irq_vector(&self) -> u16 {
		let m = self.get_active_bank_at(0xFF);
		(m[0xFE] as u16) + (m[0xFF] as u16) * 256
    }

    pub fn active_ram(&self) -> Vec<u8> {
		// This is a COPY of the RAM. It's a copy
		// because it will be sent across threads.

		let mut r:Vec<u8> = Vec::new();
		for bank in 0..256 {
			let s = self.get_active_bank_at(bank);
			assert!(s.len() == 256);
			r.extend_from_slice( s);
		}
		r
    }

    fn mem_watch(&self, start: u16, length: u16) -> Vec<u8> {
	let mut m: Vec<u8> = Vec::new();

	for addr in start..start + length {
	    m.push(self.loadb_no_side_effect(addr))
	}

	m
    }






    pub fn which_memory_bank(&self, addr_: u16, for_write: bool) -> MemoryBankType {
	/* Depending on the address given, the state of the various
	   memory switches and the fact that we're reading or writing
	   in the memory (for_write), this returns the proper memory
	   bank to look at.
	*/

	let for_read = !for_write;

	// Why this function ?
	// Because for the debugger I want to be able to read memory
	// without modifying it. Therefore, I want to refer to it
	// as a non-mutable &. When executing, I want to refer to it
	// as mutable. So I end up having two ways of retrieving
	// the same information and Rust doesn't like that. So I
	// put a level of indirection. First I select which bank
	// I'm interested in and second I build a mutable or
	// immutable reference to it. This function only answers
	// to the question "which bank am I interested in" ?

	let addr = addr_ as usize;

	// RAM access is guarded by HRAMWRT and HRAMRD.  but
	// of course, one or the other is to be listened to
	// depending on if we want to read or write

	/* From Inside the Apple IIe, page 295:

	The RAMRD switches are used to control whether read opera-
	tions are to use the memory locations from $200 ... $BFFF in main
	memory or the same locations in auxiliary memory. The RAMWRT
	switches control write operations for the same area of memory.

	(but the whole story needs 80STORE too !!!
	*/

	// See table 5-4 in UA2e
	// HRAMWRT selects between ROM (set) and RAM (reset)
	// HRAMRD  selects between ROM (reset) and RAM (set) (opposite of HRAMWRT !)
	let is_high_ram_access = (for_write && self.hramwrt == RESET)
	                               || (for_read && self.hramrd == SET);

	// RAMRD ON == auxiliary memory enabled for reading (A2e ref p. 74, $C003)
	let is_aux_ram_access = (for_write && self.ramwrt == SET)
	                              || (for_read && self.ramrd == SET);


	// See Understanding A2, page 5-25
	match addr {
	    0x0000..=0x01FF => {
		/* A2e Ref Man, page 74 : "A single soft switch named
		ALTZP (for alternate zero page) switches the
		bank-switched memory and the associated stack and zero
		page area between main and auxiliary memory. As shown
		in Table 4-6, writing to location $C009 turns ALTZP on
		and selects auxiliary-memory stack and zero page;
		writing to the soft switch at location $C008 turns
		ALTZP off and selects main-memory stack and zero page
		for both reading and writing." */

		if self.altzp {
		    //&mut self.aux_ram
		    MemoryBankType::Aux
		} else {
		    //&mut self.mb_ram
		    MemoryBankType::Motherboard
		}
	    }
	    0x0200..=0x03FF | 0x0800..=0x1FFF | 0x4000..=0xBFFF  => {
		if is_aux_ram_access {
		    MemoryBankType::Aux
		} else {
		    MemoryBankType::Motherboard
		}
	    }
	    0x0400..=0x07FF => {
			/* A2e Ref Man, page 74 : "As shown in Table 4-6, the
			* 80STORE switch functions as an enabling switch:
			* with it on, the PAGE2 switch selects main memory or
			* auxiliary memory. With the HIRES switch off, the
			* memory space switched by PAGE2 is the text display
			* Page 1, from $0400 to $07FF; with HIRES on, PAGE2
			* switches both text Page 1 and high-resolution
			* graphics Page 1, from $2000 to $3FFF."
			*/

			/* A2e Ref Man, page 74 : "If you are using both the
			auxiliary-RAM control switches and the
			auxiliary-display-page control switches, the
			display-page control switches take priority: if
			80STORE is off, RAMRD and RAMWRT work for the entire
			memory space from $0200 to $BFFF, but if 80STORE is
			on, RAMRD and RAMWRT have no effect on the display
			page. Specifically, if 80STORE is on and HIRES is off,
			PAGE2 controls text Page 1 regardless of the settings
			of RAMRD and RAMWRT.  Likewise, if 80STORE and HIRES
			are both on, PAGE2 controls both text Page 1 and
			high-resolution graphics Page 1, again regardless of
			RAMRD and RAMWRT."
			*/

			/* From Inside the Apple IIe, page 295:

			If RAMRDON ($C003) or RAMWRTON ($C005) is selected, and
			the 80STOREOFF ($C000) switch is active, then the entire block
			of auxiliary memory from $200 ... $BFFF will be selected for read-
			ing or writing, respectively. If RAMRDOFF ($C002) or RAM-
			WRTOFF ($C004) is selected, then main memory will be selected
			for reading or writing, respectively, instead. The memory areas
			that are switched by RAMRD or RAMWRT in each of three different
			situations are summarized in Figure 8-5.

			The area of memory that is affected when the RAMRD and
			RAMWRT switches are used is slightly different if the switching
			occurs when 80STOREON ($C001) is active. As you will recall from
			Chapter 7, the 80STORE switches are used to define the effect of
			the lie's PAGE2 switches. If 80STORE is ON, then PAGE20N ($C055)
			and PAGE20FF ($C054) are used to select whether the text screen
			video RAM page ($400 ... $7FF) in auxiliary or main memory is
			to be selected. In addition, if HIRESON ($C057) is active, then the
			PAGE2 switches will also select whether the high-resolution graph-
			ics screen video RAM page ($2000 ... $3FFF) in auxiliary or main
			memory is to be selected. The important point to note is that when-
			ever 80STORE is ON, the PAGE2 switches take priority over the
			RAMRD and RAMWRT switches and so these latter two switches
			cannot be used to control which of the video RAM areas are active.
			The effect of switching PAGE2 with 80STOREON is summarized
			in Figure 8-6 */

			match self.store80  {
				RESET => {
					// UA2e : "If 80STORE is reset, then RAMRD and RAMWRT
					// will bank switch the $400-$7FF and $2000-$3FFF
					// ranges along with the rest of the $200-$BFFF
					// range."

					if is_aux_ram_access {
						MemoryBankType::Aux
					} else {
						MemoryBankType::Motherboard
					}
				},
				SET => {
					// UA2e : "If 80STORE is set and HIRES is RESET, then
					// PAGE2 switches between motherboard RAM and auxiliary
					// card RAM fro reading and writing in the $400-$7FF
					// range"
					// -> if I remove that constraint, miami sound machine works.
					// What happens if HIRES is SET and STORE80 is SET ? See
					// paragraph below...

					// Inside the Apple //e p295: "In addition, if HIRESON ($C057) is
					// active, then the PAGE2 switches will ALSO select whether the
					// high-resolution graphics screen video RAM page ($2000 ... $3FFF)
					// in auxiliary or main memory is to be selected.

					// Inside the Apple //e p295: "As you will recall from Chapter 7,
					// the 80STORE switches are used to define the effect of the //e's
					// PAGE2 switches."

					// Inside the Apple //e p295: "The important point to note is that
					// whenever 80STORE is ON, the PAGE2 switches take priority over
					// the RAMRD and RAMWRT switches and so these latter two switches
					// cannot be used to control which of the video RAM areas are
					// active."

					// UA2e : "PAGE2 set selects auxiliary card RAM, and PAGE2 reset
					// selects motherboard RAM"

					if self.page2 == SET {
						MemoryBankType::Aux
					} else {
						MemoryBankType::Motherboard
					}
				}
			} // match store80
		}
	    0x2000..=0x3FFF => {
			match self.store80 {
				RESET => {
					if is_aux_ram_access {
						MemoryBankType::Aux
					} else {
						MemoryBankType::Motherboard
					}
				}
				SET => match self.hires {
					SET => {
		    			if self.page2 == SET {
							MemoryBankType::Aux
		    			} else {
							MemoryBankType::Motherboard
		    			}
					}
					RESET =>  {
		    			MemoryBankType::Motherboard
					}
				}
	    	}
		}
	    0xC000..=0xC0FF => {
		// FIXME Check the manuals to see if this is correct
		MemoryBankType::Motherboard
	    }
	    0xC100..=0xC7FF => {
		let slot = (addr >> 8) & 0x7;

		if self.cxrom_select == CXROMSelect::Off {
		    /* See UtAIIe page 5-28.

		    SLOTCXROM  SLOTC3ROM $C1,2,4-7xx $C3xx
		    reset/off  reset     slot        internal
		    reset/off  set       slot        slot
		    set        reset     internal    internal
		    set        set       internal    internal
		     */

		    if slot == 3 {
			if self.c3rom_select == C3ROMSelect::On {
			    MemoryBankType::Motherboard
			} else {
			    MemoryBankType::AppleRom
			}
		    } else {
			// Look at slot ROM
			if slot == 6 {
			    //trace!("Using disk rom");
			    // self.slots[slot].doHighIO(
			    // 	addr,
			    // 	DISK_ROM[(addr - 0xC600) as usize])
			    MemoryBankType::DiskRom
			} else {
			    // CXROM Off => slot ROM
			    MemoryBankType::Motherboard
			}
		    }
		} else {
		    // CXROM On => internal ROM (regardless of C3ROM)
		    MemoryBankType::AppleRom
		}
	    },
	    0xC800..=0xCFFF => {
		MemoryBankType::AppleRom
	    },
	    0xD000..=0xDFFF => {
		if is_high_ram_access {
		    // ALTZP selects between motherboard RAM (reset) and AUXRAM (set)
			/* UA2e p. 5-23: When high RAM is enabled. $DOOO-$FFFF
			   addressing causes access to either motherboard
			   high RAM or auxiliary card high RAM as controlled
			   by the ALTZP soft switch. Switching between motherboard RAM
			   and auxiliary card RAM is discussed in the next section.
			   */
		    if self.altzp {
			// It's auxram

			// BANK1 selects AUXRAM bank one or two.
			if self.bank1 {
			    MemoryBankType::AuxBank2
			} else {
			    MemoryBankType::AuxBank1
			}
		    } else {
			// BANK1 selects motherboard high RAM bank one or two.
			if self.bank1 {
			    MemoryBankType::Bank2
			} else {
			    MemoryBankType::Bank1
			}
		    }
		} else {
		    MemoryBankType::AppleRom
		}
	    }
	    0xE000..=0xFFFF => {
		if is_high_ram_access {

		    // UA2e p. 5-25 : "ALTZP switches the $0-$1FF
		    // range and, *if high RAM is enabled*, the $D000
		    // -$FFFF range between motherboard RAM and
		    // auxiliary card RAM. ALTZP set selects auxiliary
		    // card RAM, and ALTZP reset selects motherboard
		    // RAM."

		    if self.altzp {
			MemoryBankType::Aux
		    } else {
			MemoryBankType::Motherboard
		    }
		} else {
		    MemoryBankType::AppleRom
		}
	    }
	    _ => { panic!("${:04X} unsupported", addr)}
	}
    }



    fn configure_bank_switched_memory_access(&mut self, write: bool, addr: u16) {
		let read = !write;

	// See Understanig A2, 5-23, point 1 and 2.
	// See also table 4-5 in A2e ref man
	// See table page 384 of Inisde Apple //e (much easier to read)
	match addr {
	    0xC080|0xC084 /* READBSR2*/ => {self.bank1 = false; self.hramrd = true; }
	    0xC081|0xC085 => {self.bank1 = false; self.hramrd = false; }
	    0xC082|0xC086 => {self.bank1 = false; self.hramrd = false; }
	    0xC083|0xC087 => {self.bank1 = false; self.hramrd = true; }

	    0xC088|0xC08C => {self.bank1 = true;  self.hramrd = true; }
	    0xC089|0xC08D => {self.bank1 = true;  self.hramrd = false; }
	    0xC08A|0xC08E => {self.bank1 = true;  self.hramrd = false; }
	    0xC08B|0xC08F => {self.bank1 = true;  self.hramrd = true; }

	    _ => panic!("This function only works for limited addresses !")
	}

	// Handling HRAMWRT

	// See Understanding A2, 5-23, point 3.

	// UA2e : "HRAMWRT' is reset by odd read access in the
	// $C08X range when PRE-WRITE is set.
	// It is set by even access in the $C08X range.
	// Any other type of access causes HRAMWRT' to hold its current state."

	// C082 : even access => HRAMWRT = True
	// C082 : Write Ram = false

	if read && (addr & 1 == 1) &&  self.prewrite {
	    self.hramwrt = false
	} else if addr & 1 == 0 {
	    self.hramwrt = true
	}

	// Handling PRE-WRITE

	// Note that we update prewrite after using it, not before.

	// UA2e : "Writing to high RAM is enabled when the HRAMWRT'
	// soft switch is reset. The controlling MPU program must set
	// the PRE-WRITE soft switch before it can reset HRAMWRT'

	// PRE-WRITE
	// - is set by odd read access in the $C08X range.
	// - It is reset by even read access
	// - or any write access in the $C08X range."

	if read && (addr & 1 == 1) {
		self.prewrite = true;
	} else if read && (addr & 1 == 0) {
		self.prewrite = false;
	} else if write {
		self.prewrite = false;
	}

	debug!("SwitchMemory: {}",
		 self.log_bank_switched_config(addr));
    }


    pub fn log_bank_switched_config(&self, addr: u16) -> String {
		let bank_msg = if self.bank1 {"Bank 1"} else {"Bank 2"};
		let hramrd_msg = if self.hramrd {"High RAM"} else {"ROM"};
		let hramwrt_msg = if !self.hramwrt {"High RAM"} else {"ROM"};

		format!("at ${:04X} HRAMRD:{:} HRAMWRT:{} PREWRITE:{} BANK1:{:} ALTZP:{:} 80STORE:{} HIRES:{} COL80:{} PAGE2:{}",
			addr,
			hramrd_msg, hramwrt_msg, self.prewrite, bank_msg,
			self.altzp, self.store80, self.hires, self.col80, self.page2)
    }

	pub fn get_active_bank_at(&self, bank: usize) -> &[u8] {
		assert!(bank <= 255, "Banks !");
		let a: usize = bank*256;
		let b: usize = a + 256;

		match self.which_memory_bank(a as u16, false) {
			MemoryBankType::Motherboard|MemoryBankType::Bank2 => {self.mb_ram.slice(a..b)},
			MemoryBankType::Bank1 => {self.bank1_ram.slice(a..b)},
			MemoryBankType::Aux|MemoryBankType::AuxBank2 => {self.aux_ram.slice(a..b)},
			MemoryBankType::AuxBank1 => {self.bank1_auxram.slice(a..b)},
			MemoryBankType::AppleRom => {self.apple_rom.slice(a..b)}
			MemoryBankType::DiskRom => {&DISK_ROM}
		}

	}

	pub fn loadw_no_side_effect(&self, addr: u16) -> u16 {
		((self.loadb_no_side_effect(addr+1) as u16) << 8) + self.loadb_no_side_effect(addr) as u16
	}

    pub fn loadb_no_side_effect(&self, addr: u16) -> u8 {
	// FIXME Not finished yet, we should give the I/O
	// values.

	match self.which_memory_bank(addr, false) {
	    MemoryBankType::Motherboard|MemoryBankType::Bank2 => {self.mb_ram[addr as usize]},
	    MemoryBankType::Bank1 => {self.bank1_ram[addr as usize]},
	    MemoryBankType::Aux|MemoryBankType::AuxBank2 => {self.aux_ram[addr as usize]},
	    MemoryBankType::AuxBank1 => {self.bank1_auxram[addr as usize]},
	    MemoryBankType::AppleRom => {self.apple_rom[addr as usize]}
	    MemoryBankType::DiskRom => {DISK_ROM[(addr - 0xC600) as usize]}
	}
   }




    pub fn load_ram(&mut self, fname: &PathBuf, offset: usize, cycle: u64) {
	self.mb_ram.load_data(fname, offset);

	if offset == 0x400 {
	    // FIXME Ugly !!!! Reuse what you've loaded !
	    let data = &std::fs::read(fname).unwrap()[..];
	    for x in 0..data.len() {
		self.clock_events.push_front(
		    ClockEvent {
			tick: cycle+(x as u64),
			event: ClockEventData::GfxMemWrite{addr:(offset+x) as u16, data:data[x]}});
	    }
	}

    }

    pub fn save_ram(&mut self, fname: &PathBuf, offset: usize, len: usize) {
	use std::fs;

	info!("Saving mem from {:04X} to {:04X}", offset, offset+len);
	let mem = &self.active_ram()[offset..offset+len];
	fs::write(fname, mem).unwrap();
    }

    pub fn read_roms(&mut self, rom_filename: &PathBuf)
    {
	self.apple_rom.load_data(rom_filename, 0xC000);
    }

    fn load_default_main_rom(&mut self) {
	info!("Loading default ROM");

	self.apple_rom.load_bytes(include_bytes!("../data/apple2e-enhanced.rom"), 0xC000);

	let ram = &mut self.apple_rom.ram;

	// See note from 4am regarding Eggs-It protection and "M"
	// error code. This basically morphs the ROM into seomthing
	// the game will recognize as genuine Apple.

	ram[0xFFFE] = 0x40;
	ram[0xFFFF] = 0xFA;
    }


    fn prepare_charset_rom(&mut self) {
	let cs = X25.checksum(self.charset_rom.as_slice());

	match cs {
	    0x8087 => {
		info!("Charset ROM checksum is {:X}: known ROM !", cs);
		for ndx in (0x0*8)..(0xFF*8) {
		    self.charset_rom[ndx] = !self.charset_rom[ndx];
		}
	    },
	    _ => {
		info!("Charset ROM checksum is {:X} : unknown !", cs);
		for ndx in (0x0*8)..(0xFF*8) {
		    self.charset_rom[ndx] = !self.charset_rom[ndx];
		}
	    }
	}
	// Reverse bit order
	// for ndx in 0..self.charset_rom.len() {
	//     let mut byte = self.charset_rom[ndx];
	//     let mut nb = 0;
	//     for i in 0..7 {
	// 	nb <<= 1;
	// 	nb += byte & 1;
	// 	byte >>= 1
	//     }
	//     self.charset_rom[ndx] = nb;
	// }

	//(0x20*8)..(0x80*8) tout est inversé, sauf le bas (donc tout)
	//(0x80*8)..(0xC0*8): Apple //e logo est inversé, le bas n'est pas inversé.
	// for ndx in (0x0*8)..(0xFF*8) {
	//     self.charset_rom[ndx] = !self.charset_rom[ndx];
	// }
	//self.charset_rom.remove(0);

    }

    fn load_default_charset_rom(&mut self) {
	self.charset_rom.clear();
	// MAny ROMs here : https://downloads.reactivemicro.com/Apple%20II%20Items/ROM_and_JEDEC/IIe/Video%20ROM/
	//self.charset_rom.extend_from_slice(include_bytes!("../data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin"));

	// See Sater 8-40 : PROGRAMMING SCREEN CHARACTER SETS IN EPROM
	// The TEXT patterns are in the lower half of the
	// video ROM ($000—$7FF). They are laid out identi-
	// cally to the ALTCHRSET ASCII (see bottom half of Table 8.4).

	self.charset_rom.extend_from_slice(include_bytes!("../data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin"));

	// Current (enhanced, with mouse text)
	//self.charset_rom.extend_from_slice(include_bytes!("../data/3410265A.bin"));

	self.prepare_charset_rom()
    }

    pub fn read_charset_rom(&mut self, rom_filename: &PathBuf) -> Result<(), Error> {
    	info!("Loading charset ROM {}", rom_filename.to_string_lossy());
		self.charset_rom.clear();
		let mut file = File::open(rom_filename)?;
		file.read_to_end(&mut self.charset_rom)?;
		self.prepare_charset_rom();
		Ok(())
    }

    pub fn vbl_bit(&self, cycle:u64) -> usize {
		// Apple 2e vertical blank
		// https://groups.google.com/g/comp.sys.apple2.programmer/c/sKxb-kkvdvo/m/JE-E_j_ADgAJ
		// > c019: Bit 7 = 1 when not in VBL

		// Sather 8-14: "European television has 625 scans in two interlaced fields"
		// VBL is 120 scans long
		// Eurapple has 312 scans

		// Sather 3-3: /composite/ frequency of the 6502 is 1.0205 MHz
		//  (composite is "sum" of 64 cycle at 1.0227Mhz, 1 cycle at 0.8949Mhz.)
		// => 1020500 / 50 = 20410 cycles per frame

		// Horizontal timing
		// -----------------
		// Sather 8-15 : there are 40 video cycles
		// Sather 8-3: The horizontal sync puls occur in the middle of HBL, the 25 cycles
		//      horizontal blanking gate
		// => 40+25 = 65 CPU cycles per line
		// Sather 3-11 (horizontal scanning) : a line lasts 64 cycles + 1 long cycle

		// Vertical timing
		// ---------------
		// NTSC : 525/2 = 262 lines, out of which 192 are drawn (Sather 8-5)
		// => VBL=70 lines == 70*65 == 4550 CPU cycles (sather 8-4, VBL pulse occur in the middle)
		// PAL: 625/2 = 312 lines == 20280 cycles out of which VBL is 120 lines == 7800 cycles
		// So scan lines are like this:
		//   0 -  59 = VBL (bit = 0)      (top of screen) --> 65*60 = 3900
		//  60 - 251 = Not in VBL (bit=1) screen draw (60 + 192 = 252)
		// 252 - 311 = VBL (bit = 0)      (bottom of screen)

		// Sather 3-32: "VBL goes high just after the last displayed address is
		// scanned at the bottom right of the Apple screen, and it goes low at the
		// same horizontal point in the last undisplayed horizontal scan at the
		// top of the screen."

		// const VBL_END: usize = CYCLES_PER_LINE*FIRST_NON_BLANK_LINE+25-13;
		// const VBL_START: usize = CYCLES_PER_LINE*(FIRST_NON_BLANK_LINE+192);

		let tick_in_frame = cycle % (CYCLES_PER_FRAME as u64);
		if tick_in_frame >= (LAST_VISIBLE_CYCLE as u64)
			|| tick_in_frame < (FIRST_VISIBLE_CYCLE as u64)	{
			// We're in the VBL
			0
		} else {
			// We're drawing on the screen
			1
		}
    }

	pub fn hbl_bit(&self, cycle:u64) -> usize {
		if self.vbl_bit(cycle) == 1 {
			return 0;
		} else {
			let tick_in_frame = cycle % (CYCLES_PER_FRAME as u64);
			let pos_on_line = tick_in_frame % (CYCLES_PER_LINE as u64);
			if pos_on_line <= 24 {
				return 1
			} else {
				return 0
			}
		}

	}

	fn compute_V50CBA(&self, line: usize) -> usize {
		if line <= 255 {
			return 0b100_000_000 + line
		} else {
			assert!(line > 255 && line < (192+120));
			return 0b011_001_000 + (line - 256)
		}
	}

	fn scanned_addr(&self, cycle: u64) -> usize {

		// 25, 25+1 fails on line=16 video.scanner test
		//
		// 25-1 works on all video.scanner tests
		// 0 works with CC.DSK and somewhat AS-S1/S2.DSK and double.dsk
		let cycle_in_frame = (cycle-25 + 25) % (CYCLES_PER_FRAME as u64);

		let cycle = (if cycle_in_frame >= 60*65 {
			cycle_in_frame - 60*65
		} else {
			cycle_in_frame + (192+60)*65
		}).to_usize().unwrap();


		let line = cycle / 65;
        let hpos = cycle % 65;

		let H05 = if hpos >= 1 {
			hpos - 1
		} else {
		 	hpos
		};

		let V50CBA = self.compute_V50CBA(line);

		let V4_V3 = (V50CBA & 0b11000_000) >> 6;
		let V012  = (V50CBA & 0b00111_000) >> 3;
		let VABC  = V50CBA & 0b111;

		let H543 = (H05 & 0b111_000) >> 3;
		let H012 = H05 & 0b111;
		let SUMS = (0b1101 + H543 + (V4_V3 | (V4_V3 << 2))) & 0b1111;

		let mut scan = H012 | (SUMS << 3) | (V012 << 7);

		if !self.hires || self.text_switch {
    	    // LORES or TEXT
			if !self.page2 {
        		scan |= 0x400;
			} else {
				scan |= 0x800;
			}
		} else {
        	scan |= VABC << 10;
			if !self.page2 {
        		scan |= 0x2000;
			} else {
				scan |= 0x4000;
			}
		}

		//println!("scan={:04X}", scan);

		return scan;
	}




    pub fn floating_addr(&self, cycle:u64) -> Option<usize> {
		return Some(self.scanned_addr(cycle));

		let hgr_page = if self.text_switch {
			if self.page2 {0x800} else {0x400}
		} else if self.page2 {0x4000} else {0x2000};

		let cycle_in_frame = (cycle as usize) % CYCLES_PER_FRAME;
		let line = cycle_in_frame / CYCLES_PER_LINE;
		let cycle_on_line = cycle_in_frame % CYCLES_PER_LINE;

		let hbl = cycle_on_line < HBL_CYCLES;

		/*
		    .....................
			.....................
			HHHHHH...............
			HHHHHH...............
			.....................
			.....................
		 */

		// 20, 23, 26 OK (but don't hit the key to accelerate the demo, it breaks eveyrthing)
		// With 26, the square in CrazyCycles demo is well centered horizontally.
		// It's strange because a line is 65 cycles. So with 26 I'll miss bytes (40-(65-26)).
		// FIXME is that a off by one ?

		// 10,25,24,16,22,21,19 NOK
		const HACKY_DELAY: usize = 1;
		if self.vbl_bit(cycle) != 0 && (HBL_CYCLES+HACKY_DELAY..HBL_CYCLES+HACKY_DELAY+65).contains(&cycle_on_line) {
			let byte_on_line = cycle_on_line - (HBL_CYCLES+HACKY_DELAY);
			//debug!("Floating bus read : {:04X} lines:{:03} offset:{:03}", hgr_page, line, byte_on_line);
			if !self.text_switch {
			Some( (hgr_page as usize) + hgr_address(line - FIRST_NON_BLANK_LINE) + byte_on_line)
			} else {
			Some( (hgr_page as usize) + crate::gfx_utils::text_address(line - FIRST_NON_BLANK_LINE) + byte_on_line)
			}
		} else {
			//debug!("Floating bus read : {:04X} lines:{:03} outside redraw", hgr_page, line);
			None
		}
    }

    pub fn floating_bus(&self, cycle:u64) -> u8 {
		if let Some(addr) = self.floating_addr(cycle) {
			//println!("cycle:{} floating addr:{:04X}", cycle, addr);
			self.mb_ram[addr]
		} else {
			0xFF
		}
    }

    fn read_write_io(&mut self, addr: u16, _side_effects: bool, tick: u64, mem_byte: u8) -> u8 {
	/* Logic for addresses which behave the SAME when read or written.
	*/

	let rom_byte = mem_byte;

	/* According to 5.6 p104 of Apple2c refernce manual, reading or
	writing is equivalent to activate some flags.

	Same story at page 28 of AIIe Technical Reference Manual.
	Two addresse where one can R or W to set flag. And one address where one can read bit 7.

	 */

	match addr {
	    TEXT_OFF  /* 0xC050 */ => {
		//debug!("text off");
		self.text_switch = false;
		let z = self.gfx_mode_change_clock_event(tick);
		self.clock_events.push_front(z);
		self.floating_bus(tick)
	    }
	    TEXT_ON   /* 0xC051 */ => {
		//debug!("text on");
		self.text_switch = true;
		let z = self.gfx_mode_change_clock_event(tick);
		self.clock_events.push_front(z);
		rom_byte
	    }
	    MIXED_OFF /* 0xC052 */ => {
		debug!("mixed off");
		self.mixed = false;
		let z = self.gfx_mode_change_clock_event(tick);
		self.clock_events.push_front(z);
		rom_byte
	    }
	    MIXED_ON  /* 0xC053 */ => {
		debug!("mixed on");
		self.mixed = true;
		let z = self.gfx_mode_change_clock_event(tick);
		self.clock_events.push_front(z);
		rom_byte
	    }
	    PAGE2_OFF /* 0xC054 */ => {
		//println!("page2 off");
		self.page2 = false;
		let z = self.gfx_mode_change_clock_event(tick);
		self.clock_events.push_front(z);
		rom_byte
	    }
	    PAGE2_ON  /* 0xC055 */ => {
		//println!("page2 on");
		self.page2 = true;
		let z = self.gfx_mode_change_clock_event(tick);
		self.clock_events.push_front(z);
		rom_byte}
	    HIRES_OFF /* 0xC056 */ => {
		debug!("hires off, ioudis:{}", self.iou_disabled);
		self.hires = false;
		let z = self.gfx_mode_change_clock_event(tick);
		self.clock_events.push_front(z);
		self.floating_bus(tick)
	    }
	    HIRES_ON /* 0xC057 */ => {
		debug!("hires on, ioudis:{}", self.iou_disabled);
		self.hires = true;
		let z = self.gfx_mode_change_clock_event(tick);
		self.clock_events.push_front(z);
		rom_byte}
	    AN3_OFF /* 0xC05E */ => {

			if self.iou_disabled {
				//println!("AN3OFF");
				self.dhires = true;
				self.an3_cycle = false;
				let z = self.gfx_mode_change_clock_event(tick);
				self.clock_events.push_front(z);
			};
			rom_byte }
	    AN3_ON /* 0xC05F */ => {
			if self.iou_disabled {
				//println!("AN3ON");
				self.dhires = false;
				self.an3_cycle = true;
				let z = self.gfx_mode_change_clock_event(tick);
				self.clock_events.push_front(z);
			};
			rom_byte }
	    SPEAKER..=0xC03F /*  C030 */ => {
		self.speaker.borrow_mut().record(ClockEvent {
		    tick,
		    event: ClockEventData::SpeakerIORead{addr}});

		// self.speaker_events.push_front(
		//     ClockEvent {
		// 	tick: tick,
		// 	event: ClockEventData::SpeakerIORead{addr:addr}});
		rom_byte
	    },
	    PADDLE_TIMER => {
		self.analog_input_reset_cycle.fill(tick);
		self.floating_bus(tick)
	    },
	    STOP_DRIVE1 | START_DRIVE1 => {/* Unused */ 0},
	    _ => {warn!("Unimplemented IO R/W {:04X}", addr); /*self.doIO(addr, rom_byte)*/
		  0}
	}
    }

    fn slot_read(&mut self, slot:usize,addr:u16, ram_byte:u8, tick:u64) -> (u8, Option<IrqChange>) {
		let (byte, reqs, irq_change) = self.slot_mut(slot).read_io(addr, ram_byte, tick);

		if let Some(irq_requests) = reqs {
			self.process_irq_requests(irq_requests, tick);
		};

		(byte, irq_change)
    }

    pub fn readb_cycle(&mut self, addr: u16, side_effects: bool, tick: u64) -> (u8, Option<IrqChange>)
    {
		// if (0xD000..0xD100).contains(&addr) {
		// 	debug!("D000: {} rom:${:02X}",self.which_memory_bank(addr, false),
		// 	self.apple_rom[addr as usize]);
		// }

	let mem_byte = self.loadb_no_side_effect(addr);

	let rom_byte = mem_byte;
	let ram_byte = mem_byte;

	// Rmemeber that code can be executed at address
	// such as $C200 which will imply loadb calls :-)

	if side_effects {
	    self.nreads = self.nreads.wrapping_add(1);
	}

	let mut irq_change = None;
	let read_byte = match addr {
	    0xC000..=0xC0FF => {

		// Any of these addresses will clear the keyboard strobe flag.
		// This code is here to avoid duplicating code in the next
		// match statement.

		// See Sather : "The keyboard read address is $C00X,
		// and the strobe flip-flop reset address is $C01X."


		// let kbd_strobe:u8 = match addr {
		//     KBD_STROBE_RNG_START..=KBD_STROBE_RNG_END => {

		//     },
		//     _ => 0
		// };


		match addr {
		    KBD_IN_RNG_START..=KBD_IN_RNG_END => {
				// If a key was hit, we simulate the fact
				// that it was hit and released.

				self.read_keyboard_strobe(tick) | self.keyboard_current_key(tick)
				// if let Some(kbd_strobe_start) = self.kbd_strobe {
				// 	if true || tick < kbd_strobe_start + 20*1000 {
				// 		// Sather: The fact that the outputs are latched
				// 		// means that you can press a key and its 7-bit
				// 		// ASCII can be checked anytime before another key
				// 		// is pressed.

				// 		// Sather: The Apple II Reference Manual says the STROBE
				// 		// lasts a maximum of 10 microseconds. This is
				// 		// probably a safe number they arrived at,
				// 		// figuring the ring oscillator would never get
				// 		// above 100 KHz.

				// 		// Sather:  The STROBE output works as it does in the
				// 		// MM5740 with a high level, one clock strobe
				// 		// output (12 microseconds) any time a matrix key
				// 		// is pressed.

				// 		// if tick - self.key_hit_cycle.unwrap() > 12 {
				// 		// 	self.key_hit_cycle = None;
				// 		// };

				// 		0x80 | self.keyboard_current_key(tick)
				// 	} else {
				// 		self.keyboard_current_key(tick)
				// 	}
				// } else {
				// 	self.keyboard_current_key(tick)
				// }
		    },
		    KBD_STROBE_RNG_START => {
				self.keyboard_strobe_read_and_flip(tick, side_effects);

				// Any key down (AKD)

				// See Sather p 2-16 : "A second flag related to the keyboard is
				// the AKD (any key down) flag, read at $C010. The AKD signal is
				// routed from the keyboard encoder to the IOU and relayed to
				// MD7 when the IOU detects a read to $C010. This gives
				// programmers a little more versatility in interpreting
				// keypresses. Not that reading the AKD flag also resets the
				// KEYSTROBE soft switch.""

				if self.keyboard_current_key(tick) != 0 {
					0x80
				} else {
					0
				}
			},
		    //0xC011|0xC012|0xC017 => kbd_strobe,
			BANK1_READ => if self.bank1 { 0 } else { 0x80 },
		    RAMRD_READ => /* 0xC013 */ { self.mem_setup_change = true; bool_to_bit7(self.ramrd) }
		    RAMWRT_READ => /* 0xC014 */ { self.mem_setup_change = true; bool_to_bit7(self.ramwrt) }
		    CXROM_READ => /* 0xC015 */ {
				self.mem_setup_change = true;
			// C015 49173 	RDCXROM 	E G 	R7 	Status of Periph/ROM Access
			// 0
			match self.cxrom_select {
			    CXROMSelect::Off => 0,
			    CXROMSelect::On => 1 << 7
			}

		    },
		    ALTZP_READ => /* 0xC016 */ {
				self.mem_setup_change = true;
				bool_to_bit7(self.altzp)
			}
			C3ROM_READ => /* c017 */ {
				match self.c3rom_select {
					// FIXME a2audit says so. I should re-read UtA2e to make sure.
					C3ROMSelect::On => 0x80,
					C3ROMSelect::Off => 0,
				}
			}
		    STORE80_READ => /* 0xC018 */{
			//debug!("STORE80_READ : {}",self.store80);
			self.mem_setup_change = true;
			bool_to_bit7(self.store80) },
		    VERTBLANK => /* 0xC019  */ {
				self.last_vblank_read = tick;
				//println!("Tick! {}", self.vbl_bit(tick) * 0x80);
				(self.vbl_bit(tick) * 0x80) as u8
		    }
		    TEXT_READ  => /* 0xC01A */ { bool_to_bit7(self.text_switch) }
		    MIXED_READ => /* 0xC01B */ { bool_to_bit7(self.mixed) }
		    PAGE2_READ => /* 0xC01C */ { bool_to_bit7(self.page2) }
		    HIRES_READ => /* 0xC01D */ { bool_to_bit7(self.hires) }
		    ALTCHARSET_READ => /* 0xC01E */ {
				self.mem_setup_change = true;
			match self.altcharset_select {
			    ALTCHARSETSelect::On => 1 << 7,
			    ALTCHARSETSelect::Off => 0,
			}
		    },
		    COL80_READ => /* 0xC01F */ {
				//self.mem_setup_change = true;
			//debug!("COL80_READ : {}",self.col80);
			bool_to_bit7(self.col80) }
		    DHIRES_READ => /* 0xC07F */ {
			debug!("DHIRES_READ : {}", self.dhires);
			bool_to_bit7(self.dhires)
		    }
		    RDIOUDIS => /* 0xC07E */ {
			debug!("RDIOUDIS : {}", self.iou_disabled);
			bool_to_bit7(!self.iou_disabled)
		    },
		    TAPEOUT_RNG_START..=TAPEOUT_RNG_END => /* 0xC02x */ {rom_byte},
		    // 0xC030 => {
		    // 	if side_effects {
		    // 	    self.soundstate = !self.soundstate;
		    // 	}
		    // 	return 0;
		    // }
		    0xC059..=0xC05D => {
			// FIXME This collides with C05E's DHGR address...

			// See table 2-11 ref manual
			return (self.read_annunciator(addr), None);
		    }
		    TAPEIN => {
			// According to https://github.com/mamedev/mame/blob/master/src/mame/drivers/apple2.cpp
			// "cassette in (accidentally read at $C068 by ProDOS to attempt IIgs STATE register)"
			// This is what I think happens here when booting a ProDOS disk
			0xFF
		    }
		    0xC080..=0xC08F => {
			if side_effects {
			    // self.doLanguageCardIO(addr);
			    self.configure_bank_switched_memory_access(false, addr)
			}
			//self.noise()
			0 // Like SHAMUS
		    }
		    0xC090..=0xC0FF => {
			// For example : X=$60, $C08D (disk][ stuff) => $C0ED
			let slot = ((addr >> 4) & 0x0f) as usize;

			// TODO: See SHAMUS for Floating Ram impolementaion

			if slot <= 7 {
			    trace!("doIO({:X}, ram byte={:X}) slot:{:X}", addr, ram_byte, slot);
			} else {
			    trace!("readb_cycle({:X}, ram byte={:X}) slot:{:X} (ex {:X})", addr, ram_byte, slot - 8, slot);
			}
			// FIXME side effects
			//self.slots[slot-8].doIO(addr, ram_byte);
			//self.slot_mut(slot-8).read_io(addr, ram_byte, tick)
			let (b, irq) = self.slot_read(slot-8, addr, ram_byte, tick);
			irq_change = irq;
			b
		    }
		    0xC061..=0xC063 => {
				bool_to_bit7(self.switch_input_value[(addr - 0xC061) as usize])
		    },
		    PADDLE0|PADDLE1|PADDLE2|PADDLE3 => {
			let ndx = (addr - PADDLE0) as usize;
			if self.analog_input_reset_cycle[ndx] > 0 {
			    // The reset was done. PADDLE high bit will remain
			    // high until joystick has read value or until
			    // 3ms have passed (3ms is given in the A2e Reference Manual
			    // while Sather p. 7-24 prefers 11-cyles wait loop but his calculations
			    // seems off a bit).
			    // 12*256 = 3072 which is about 3ms. This value was 11
			    // first but it didn't work well in Boulder Dash.

			    const MUL:u64 = 12;

			    if tick > self.analog_input_reset_cycle[ndx] + 256*MUL {
				self.analog_input_reset_cycle[ndx] = 0;
				0
			    } else if let Some(v) = self.analog_input_value[ndx] {
				// I add a little dead zone for the noise
				let dead_zone = 10;
				let v2 = if v > 128-dead_zone && v < 128+dead_zone {128} else {v};
				if tick > self.analog_input_reset_cycle[ndx] + ((v2*(MUL as usize)) as u64) {
				    self.analog_input_reset_cycle[ndx] = 0;
				    0
				} else {
				    128
				}
			    } else {
				128
			    }
			} else {
			    0
			}
		    },
		    _ => {
				self.read_write_io(addr, side_effects, tick, mem_byte)
			}
		}
	    },
	    0xC100..=0xC7FF => {
		let rom_byte = self.apple_rom[addr as usize];
		let slot = ((addr >> 8) & 0x7) as usize;

		trace!("Slot #{:} read {:X} {:} {:}", slot, addr, self.cxrom_select, self.c3rom_select);

		if self.cxrom_select == CXROMSelect::Off {
		    /* See UtAIIe page 5-28.

		    SLOTCXROM  SLOTC3ROM $C1,2,4-7xx $C3xx
		    reset/off  reset     slot        internal
		    reset/off  set       slot        slot
		    set        reset     internal    internal
		    set        set       internal    internal
		     */

		    if slot == 3 {
			if self.c3rom_select == C3ROMSelect::On {
			    // FIXME Have no ROM in slot 3 => 0xFF
			    //self.slots[slot].doHighIO(addr, 0xFF)
			    //self.slot_mut(slot).read_io(addr, 0xFF, tick)
			    let (b, irq) = self.slot_read(slot, addr, 0xFF, tick);
				irq_change = irq;
				b
			} else {
			    let (b, irq) = self.slot_read(slot, addr, rom_byte, tick);
				irq_change = irq;
				b
			}
		    } else {
			// Look at slot ROM
			if slot == 6 {
			    //trace!("Using disk rom");
			    DISK_ROM[(addr - 0xC600) as usize]
			} else if slot == 4 {
			    // FIXME hardcoded
			    let (b, irq) = self.slot_read(slot, addr, rom_byte, tick);
				irq_change = irq;
				b
			}
			else {
			    // CXROM Off => slot ROM
			    0xFF
			}
		    }
		} else {
		    // CXROM On => internal ROM (regardless of C3ROM)
		    rom_byte
		}
	    },
	    // 0xC800..=0xC8FF => {
	    // 	// Read "the seventh rom chip" in Sather's book (page 6-4) (and pages before)
	    // 	// FIXME I think here one's writing to ROM
	    // 	// but i'm not sure, there's also this I/O STROBE' thing I don't understand.
	    // 	//self.doIO(addr, val);
	    // 	self.do_language_card_io(addr); self.noise()
	    // }
	    _ => {
		mem_byte
	    }
	};

	(read_byte, irq_change)
	}

	// fn keyboard_plan(&mut self, key: Key, cycle: u64) {
	// 	self.process_irq_requests([IRQRequest {}], current_tick);
	// }

	fn read_keyboard_strobe(&self, cycle:u64) -> u8 {
		let mut b = 0;

		if let Some(ke) = self.keyboard_queue.last_key_press(cycle) {

			const CYCLES_PER_SECOND_U64: u64 = CYCLES_PER_SECOND as u64;

			if let Some(active_keypress) = self.keyboard_queue.key_currently_pressed_at(cycle) {
				//println!("read_keyboard_strobe() cycle diff:{}: {} {}", cycle - active_keypress.cycle, active_keypress.cycle, active_keypress.key);
				if cycle - active_keypress.cycle > CYCLES_PER_SECOND_U64 / 2 {
					// A key is beind pressed since at least half a second

					let retrigger_period = CYCLES_PER_SECOND_U64 / 15;
					let next_retrigger = active_keypress.cycle + ((cycle - active_keypress.cycle) / retrigger_period) * retrigger_period;

					//println!("retrigger {} {}", next_retrigger,  self.last_kbd_strobe_clear_cycle);
					if next_retrigger > self.last_kbd_strobe_clear_cycle {
						b = 0x80;
					}
				}
			}

			// but sometimes, the user clears the strobe
			if ke.cycle > self.last_kbd_strobe_clear_cycle {
				b = 0x80;
			}
		}

		return b;
	}

    fn keyboard_strobe_read_and_flip(&mut self, cycle:u64, side_effects: bool) -> u8 {
		/* The KEYSTROBE soft switch is	set by the KSTRB signal which
		   goes high momentarily any time a matrix key is pressed.

		   If a key is held down continuously for .5 to .8 seconds
		   (32 to 48 television scans), the lOU will start setting the
		   KEYSTROBE soft switch 15times every second (once every four
		   television scans).
		*/

		let b = self.read_keyboard_strobe(cycle);
		if side_effects {
			self.last_kbd_strobe_clear_cycle = cycle;
		}

		return b;
    }


    pub fn keyboard_pressed(&mut self, cycle: u64, key: Key) {
		self.keyboard_queue.insert(
			KeyEvent {
				action: KeyAction::KeyPressed,
				cycle,
				key } );
    }

    pub fn keyboard_released(&mut self, cycle: u64, key: Key) {
		//println!("keyboard_released(): Key Released {} at cycle {} buf:{} current key:{}", key, _cycle, self.pressed_keys.len(), self.keyboard_current_key(_cycle));
		self.keyboard_queue.insert(
			KeyEvent {
				action: KeyAction::KeyReleased,
				cycle,
				key } );
    }


	pub fn keyboard_current_key(&self, emu_cycle: u64) -> u8 {
		if let Some(ke) = self.keyboard_queue.key_currently_pressed_at(emu_cycle) {
			return key_to_kbd_code(&ke.key);
		} else {
			return 0;
		}
	}

	pub fn record_key_event(&mut self, cycle: u64, key: Key) {
		//println!("record_key_event: {} {}", cycle, key);

		if let Some(latest) = self.keys_events_record.front() {
			if cycle <= latest.1 {
				self.keys_events_record.push_front((key, latest.1 + 1000));
				return;
			}
		}
		self.keys_events_record.push_front((key, cycle));
	}



    pub fn gfx_mode_change_clock_event(&mut self, tick: u64) -> ClockEvent {
	ClockEvent {
	    tick,
	    event: ClockEventData::GfxModeChange {
		mode: self.compute_gfx_mode() }}
    }

    pub fn compute_gfx_mode(&self) -> GfxMode {

	let mi = ModeInfo{col80: bool2oo(self.col80),
			  store80: bool2oo(self.store80),
			  page2: bool2oo(self.page2),
			  text: bool2oo(self.text_switch),
			  mixed: bool2oo(self.mixed),
			  hires: bool2oo(self.hires),
			  dhires: bool2oo(self.dhires)
	};

	let video_flags = VideoSoftSwitches {
		text: self.text_switch as u8,
		hires: self.hires as u8,
		page2: self.page2 as u8,
		col80: self.col80 as u8,
		an3: if self.an3_cycle {1} else {0},
		modemix: self.mixed as u8,
		store80: self.store80 as u8,
		altcharset: (self.altcharset_select == ALTCHARSETSelect::On) as u8
	};

	//println!("altcharset={}", video_flags.altcharset);



	let an3 = if self.an3_cycle {0x80} else {0x0};
	let altcharset = if self.altcharset_select == ALTCHARSETSelect::On  {1} else {0};
	let mut bad_mode = false;


	// We only display debug when the graphics mode actually changes.
	/* if !bad_mode && (self.last_set_gfx_mode.mode != zzz.mode || self.last_set_gfx_mode.mixed != zzz.mixed) {
	    self.last_set_gfx_mode = zzz;
	    //debug!("Regs: Col80: {} Store80:{} page2:{} Text:{} Mixed:{} Hires:{}  DHires:{}  IOUDis:{}",
	    //	       self.col80, self.store80, self.page2, self.text, self.mixed, self.hires, self.dhires, self.iou_disabled);
	    // debug!("Gfx mode set: {} - Mixed:{} - Page2:{}", zzz.mode, if zzz.mixed.is_some() {zzz.mixed.unwrap().to_string()} else {"no".to_string()}, zzz.page2)
	} */
	match mi {

	    // Table 5-9 of A2c Reference Manual
	    // 40 column
	    ModeInfo{col80:OFF, store80:_,  page2:OFF, text:ON, mixed:OFF, hires:OFF, dhires:OFF } =>
		GfxMode{mode:GfxBaseMode::Text40, mixed:None, page2:false, an3, altcharset, video_flags},

	    ModeInfo{col80:ON,  store80:_, page2:_,   text:ON,  mixed:_,   hires:_,   dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Text80, mixed:None, page2:false, an3, altcharset, video_flags},
	    ModeInfo{col80:OFF, store80:_, page2:OFF, text:OFF, mixed:OFF, hires:OFF, dhires:OFF } =>
		GfxMode{mode:GfxBaseMode::Gr, mixed:None, page2:false, an3, altcharset, video_flags},
	    ModeInfo{col80:OFF, store80:_, page2:OFF, text:OFF, mixed:ON, hires:OFF, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Gr, mixed:Some(GfxBaseMode::Text40), page2:false, an3, altcharset, video_flags},
	    ModeInfo{col80:ON,  store80:_, page2:OFF, text:OFF, mixed:ON, hires:OFF, dhires:OFF } =>
		GfxMode{mode:GfxBaseMode::Gr, mixed:Some(GfxBaseMode::Text80), page2:false, an3, altcharset, video_flags},

	    ModeInfo{col80:OFF,  store80:_,   page2:OFF, text:OFF, mixed:OFF, hires:ON, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Hgr, mixed:None, page2:false, an3, altcharset, video_flags},
		ModeInfo{col80:OFF,  store80:OFF, page2:ON,  text:OFF, mixed:OFF, hires:ON, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Hgr, mixed:None, page2:false, an3, altcharset, video_flags},
		ModeInfo{col80:OFF,  store80:ON,  page2:ON,  text:OFF, mixed:OFF, hires:ON, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Hgr, mixed:None, page2:true, an3, altcharset, video_flags},

		ModeInfo{col80:OFF,  store80:_, page2:OFF, text:OFF, mixed:ON, hires:ON, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Hgr, mixed:Some(GfxBaseMode::Text40), page2:false, an3, altcharset, video_flags},
	    ModeInfo{col80:OFF,  store80:_, page2:ON, text:OFF, mixed:ON, hires:ON, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Hgr, mixed:Some(GfxBaseMode::Text40), page2:true, an3, altcharset, video_flags},
	    ModeInfo{col80:ON,  store80:_, page2:OFF, text:OFF, mixed:ON, hires:ON, dhires:OFF } =>
		GfxMode{mode:GfxBaseMode::Hgr, mixed:Some(GfxBaseMode::Text80), page2:true, an3, altcharset, video_flags}, // FIXME page2 is strange
	    ModeInfo{col80:ON,  store80:OFF, page2:ON, text:OFF, mixed:ON, hires:ON, dhires:OFF } =>
		GfxMode{mode:GfxBaseMode::Hgr, mixed:Some(GfxBaseMode::Text80), page2:true, an3, altcharset, video_flags},

	    // For Wizardry 1 FIXME double check with Apple2 reference book; maybe
	    // I missed something there
	    ModeInfo{col80:ON,  store80:ON, page2:OFF, text:OFF, mixed:OFF, hires:ON, dhires:OFF } =>
		GfxMode{mode:GfxBaseMode::Hgr, mixed:None, page2:false, an3, altcharset, video_flags},

	    // Table 5-10 of A2c Reference Manual
	    // 40 column page 2
	    ModeInfo{col80:OFF,  store80:_, page2:ON, text:ON, mixed:_, hires:_, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Text40, mixed:None, page2:true, an3, altcharset, video_flags},
	    //ModeInfo{col80:OFF,  store80:ON, page2:ON, text:ON, mixed:_, hires:_, dhires:_ } =>
		//GfxMode{mode:GfxBaseMode::Text40, mixed:None, page2:false, an3, altcharset},

	    // 80 column page 2
	    ModeInfo{col80:OFF,  store80:ON, page2:ON, text:_, mixed:_, hires:_, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Text80, mixed:None, page2:true, an3, altcharset, video_flags},
	    // Low res page 2
	    ModeInfo{col80:OFF,  store80:_, page2:ON, text:OFF, mixed:OFF, hires:OFF, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Gr, mixed:None, page2:true, an3, altcharset, video_flags},
	    // 40/low
	    ModeInfo{col80:OFF,  store80:_, page2:ON, text:OFF, mixed:ON, hires:OFF, dhires:_ } =>
		GfxMode{mode:GfxBaseMode::Gr, mixed:Some(GfxBaseMode::Text40), page2:true, an3, altcharset, video_flags},
	    // 80/low
	    ModeInfo{col80:ON,  store80:OFF, page2:ON, text:OFF, mixed:ON, hires:OFF, dhires:OFF } =>
		GfxMode{mode:GfxBaseMode::Gr, mixed:Some(GfxBaseMode::Text80), page2:true, an3, altcharset, video_flags},
	    // Dbl low page1
	    ModeInfo{col80:ON,  store80:_, page2:OFF, text:OFF, mixed:OFF, hires:OFF, dhires:ON } =>
		GfxMode{mode:GfxBaseMode::Dgr, mixed:None, page2:false, an3, altcharset, video_flags},
	    // Dbl low page2
	    ModeInfo{col80:ON,  store80:OFF, page2:ON, text:OFF, mixed:OFF, hires:OFF, dhires:ON } =>
		GfxMode{mode:GfxBaseMode::Dgr, mixed:None, page2:true, an3, altcharset, video_flags},

	    // 80/dbl low page1
	    ModeInfo{col80:ON,  store80:_, page2:OFF, text:OFF, mixed:ON, hires:OFF, dhires:ON } =>
		GfxMode{mode:GfxBaseMode::Dgr, mixed:Some(GfxBaseMode::Text80), page2:false, an3, altcharset, video_flags},

	    // 80/dbl low page2
	    ModeInfo{col80:ON,  store80:OFF, page2:ON, text:OFF, mixed:ON, hires:OFF, dhires:ON } =>
		GfxMode{mode:GfxBaseMode::Dgr, mixed:Some(GfxBaseMode::Text80), page2:true, an3, altcharset, video_flags},

	    // dbl-high
	    ModeInfo{col80:ON,  store80:_,   page2:OFF, text:OFF, mixed:OFF, hires:ON, dhires:ON } =>
		GfxMode{mode:GfxBaseMode::Dhgr, mixed:None, page2:false, an3, altcharset, video_flags},
	    // dbl-high
	    ModeInfo{col80:ON,  store80:OFF, page2:ON,  text:OFF, mixed:OFF, hires:ON, dhires:ON } =>
		GfxMode{mode:GfxBaseMode::Dhgr, mixed:None, page2:true, an3, altcharset, video_flags},


	    // 80/dbl-high
	    ModeInfo{col80:ON,  store80:_, page2:OFF, text:OFF, mixed:ON, hires:ON, dhires:ON } =>
		GfxMode{mode:GfxBaseMode::Dhgr, mixed:Some(GfxBaseMode::Text80), page2:false, an3, altcharset, video_flags},
	    // 80/dbl-high
	    ModeInfo{col80:ON,  store80:OFF, page2:ON, text:OFF, mixed:ON, hires:ON, dhires:ON } =>
		GfxMode{mode:GfxBaseMode::Dhgr, mixed:Some(GfxBaseMode::Text80), page2:true, an3, altcharset, video_flags},

// This is specifically for the chiptune demo of French Touch
	    // They set together the text and hires flag. According to table
	    // in A2c Ref. manual, this shouldn't happen in 40 column.
	    ModeInfo{col80:OFF, store80:OFF,  page2:OFF, text:ON, mixed:OFF, hires:ON, dhires:OFF } =>
		GfxMode{mode:GfxBaseMode::Text40, mixed:None, page2:false, an3, altcharset, video_flags},
	    ModeInfo{col80:_,  store80:_, page2:_, text:_, mixed:_, hires:_, dhires:_ } => {
		// If you come here it means that either the gfx mode is totally wrong
		// either it is being build by touching the flags. So it may be "half" set.
		// Therefore it's quite likely that an unrecgnized gfx mode
		// it just a temporary situation. Therefore we don't report about it.

		// warn!("Unrecognized Gfx Mode. Col80: {} Store80:{} page2:{} Text:{} Mixed:{} Hires:{}  DHires:{}  IOUDis:{}", self.col80, self.store80, self.page2, self.text, self.mixed, self.hires, self.dhires, self.iou_disabled);
		bad_mode = true;
		//self.last_set_gfx_mode;
		GfxMode{mode:GfxBaseMode::Text40, mixed:None, page2:false, an3, altcharset, video_flags}
	    }
	}

    }

    fn read_annunciator(&self, addr: u16) -> u8
    {
	trace!("Annunciator at ${:X} mem={:X}", addr, self.mb_ram[addr as usize]);
        if addr == 0xC05F {
	    0x00
	} else {
	    0xFF
	}
    }

    pub fn set_analog_input(&mut self, num:usize, x: usize) {
	// Simple test program:
	// 10 PRINT PDL(0)" "PDL(1):GOTO 10
	self.analog_input_value[num] = Some(x);
    }

    pub fn set_switch_input(&mut self, num:usize, set:bool) {
		//println!("set_switch_input {} {}",num,set);
		self.switch_input_value[num] = set;
    }

	pub fn set_motherboard_mono_colour_switch(&mut self, switch: MonoColourSwitch) {
		self.motherboard_mono_colour_switch = switch;
	}

	pub fn set_breakpoints(&mut self, bpt: BreakPointTriggerRef) {
		// Clone for me
		self.bpt = Some(bpt.clone());

		// Clone for slots
		for s in self.slots.iter().by_ref() {
			if let Some(slot) = s {
				slot.borrow_mut().set_debugger(bpt.clone());
			}
		}
	}

}



impl Mem for AppleII
{
    // fn mem_slice(&self, start: u16, length: u16) -> &[u8] {
    // 	&self.mb_ram[start as usize..(start+length) as usize]
    // }

    fn peekb(&mut self, addr: u16) -> u8 {
	self.loadb_no_side_effect(addr)
    }

    fn loadb(&mut self, _addr: u16) -> u8
    {
	panic!("Loadb !!!");
	//return self.readb_cycle(addr, true, 12345);
    }

    fn storeb(&mut self, addr: u16, val: u8, tick: u64) -> (u8, Option<IrqChange>)
    {
	let mut old_val:u8 = 0xFF;
	let mut irq_change = None;

	//trace!("Storeb {:04X} = {:02X}", addr, val);
	match addr {
	    0x0000..=0xBFFF | 0xD000..=0xFFFF => {
		let bank = match self.which_memory_bank(addr, true) {
		    // FIXME MemoryBankType::Bank1 is useful but dunno why. Check Latecomer for a test.
		    MemoryBankType::Motherboard|MemoryBankType::Bank2/*|MemoryBankType::Bank1*/ => {
			// If we write in the video RAM, then warn the renderer.
			if (0x2000..0x6000).contains(&addr) || (0x400..0xC00).contains(&addr) {
				//println!("RegularMem write {:04X} <- {:02X}",addr, val);
			    self.clock_events.push_front(
				ClockEvent {
				    tick,
				    event: ClockEventData::GfxMemWrite{addr, data:val}});
			}
			&mut self.mb_ram
		    },
		    MemoryBankType::Aux|MemoryBankType::AuxBank2/*|MemoryBankType::AuxBank1*/ => {
			// If we write in the video AUX RAM, then tell the renderer.
			if (0x2000..0x6000).contains(&addr) || (0x400..0xC00).contains(&addr) {
				//println!("AuxMem write {:04X} <- {:02X}",addr, val);
			    self.clock_events.push_front(
				ClockEvent {
				    tick,
				    event: ClockEventData::GfxAuxMemWrite{addr, data:val}});
			}
			&mut self.aux_ram
		    },
		    //MemoryBank::Bank1 => {trace!("Mem bank : Bank1");&mut self.bank1_ram},
		    MemoryBankType::Bank1 => {trace!("Mem bank : Bank2 ");&mut self.bank1_ram},
		    //MemoryBank::AuxBank1 => {trace!("Mem bank : AuxBank1");&mut self.bank1_auxram},
		    MemoryBankType::AuxBank1 => {trace!("Mem bank : AuxBank2");&mut self.bank1_auxram},
		    // You don't write to ROM
		    // and if you do, nothing happens (hence the returns)
		    MemoryBankType::AppleRom => {trace!("Mem bank : AppleRom"); return (0xFF, None)}
		    MemoryBankType::DiskRom => {trace!("Mem bank : DiskRom"); return (0xFF, None)}
		};

		old_val = bank[addr as usize];
		bank[addr as usize] = val;
	    },
	    0xC000..=0xC0FF => {

		// Any of these addresses will clear the keyboard strobe flag.
		// This code is here to avoid duplicating code in the next
		// match statement.

		// See Sather : "The keyboard read address is $C00X,
		// and the strobe flip-flop reset address is $C01X."

		match addr {
		    KBD_STROBE_RNG_START..=KBD_STROBE_RNG_END => {
				self.keyboard_strobe_read_and_flip(tick, true);
		    },
		    _ => ()
		};

		match addr {
		    KBD_STROBE_RNG_START..=KBD_STROBE_RNG_END => (),
		    COL80_ON => /* C00D */ {
			debug!("COL80_ON");
			self.col80 = true;

			// See table 5-9 in A2c tech ref, page 107.
			// Note (1) : 80STORE is set by the firmware when
			// 80COL is turned on.
			// NOTE: this behaviour is not described in the Apple IIe tech ref !

			// FIXME If I enable this (as explained above), BruceLee doesn't work anymore.
			// And SpyVsSpy breaks too. SpyVsSpy breaks with some messed
			// up graphics, as if I was mixing auxmem and main mem...
			// I should look at how STORE80/COL80 affects the main/aux RAM
			// memory access (see Apple's tech ref manuals)

			if APPLE_MODEL == AppleModel::IIc {
			    self.store80 = true;
			}
			let z = self.gfx_mode_change_clock_event(tick);
			self.clock_events.push_front(z);
		    },
		    COL80_OFF => /* C00C */ {
			debug!("COL80_OFF");
			self.col80 = false;
			let z = self.gfx_mode_change_clock_event(tick);
			self.clock_events.push_front(z);
		    },
		    STORE80_OFF => {
			self.store80 = false;
			debug!("STORE80_OFF: {}", self.log_bank_switched_config(addr));
			let z = self.gfx_mode_change_clock_event(tick);
			self.clock_events.push_front(z);
		    },
		    STORE80_ON => {
			self.store80 = true;
			debug!("STORE80_ON: {}", self.log_bank_switched_config(addr));
			let z = self.gfx_mode_change_clock_event(tick);
			self.clock_events.push_front(z);
		    },
		    TAPEOUT_RNG_START..=TAPEOUT_RNG_END => /* 0xC02x */ {
		    },
		    RAMRD_ON => { self.ramrd = true; trace!("MEM_WRITE: {}", self.log_bank_switched_config(addr)) }
		    RAMRD_OFF => { self.ramrd = false; trace!("MEM_WRITE: {}", self.log_bank_switched_config(addr)) }
		    RAMWRT_ON => { self.ramwrt = true; trace!("MEM_WRITE: {}", self.log_bank_switched_config(addr)) }
		    RAMWRT_OFF => { self.ramwrt = false; trace!("MEM_WRITE: {}", self.log_bank_switched_config(addr)) }
		    ALTZP_ON => { self.altzp = true; trace!("MEM_WRITE: {}", self.log_bank_switched_config(addr)) }
		    ALTZP_OFF => { self.altzp = false; trace!("MEM_WRITE: {}", self.log_bank_switched_config(addr)) }
		    C3ROM_OFF => {
				trace!("IO write : SLOTC3ROM {:X} := {:X}", addr, val);
				self.c3rom_select = C3ROMSelect::Off;
		    },
		    C3ROM_ON => {
				trace!("IO write : SLOTC3ROM {:X} := {:X}", addr, val);
				self.c3rom_select = C3ROMSelect::On;
		    },
		    SLOTCXROM_OFF => /* 0xC006 */ {
			trace!("SLOTCXROM set OFF {:X} := {:X} => {:}", addr, val, self.cxrom_select);
			// SLOTCXROM off => use SLOT ROM !
			// (see doc in Apple2Info-Hardware.pdf, page 227)
			self.cxrom_select = CXROMSelect::Off;
		    },
		    SLOTCXROM_ON  => /* 0xC007 */ {
			trace!("SLOTCXROM set ON {:X} := {:X}", addr, val);
			self.cxrom_select = CXROMSelect::On
		    },
		    ALTCHARSET_SET_ON => {
				trace!("ALTCHARSET_SET_ON");
				self.altcharset_select = ALTCHARSETSelect::On;
				let z = self.gfx_mode_change_clock_event(tick);
				self.clock_events.push_front(z);
		    },
		    ALTCHARSET_SET_OFF => {
				trace!("ALTCHARSET_SET_OFF");
				self.altcharset_select = ALTCHARSETSelect::Off;
				let z = self.gfx_mode_change_clock_event(tick);
				self.clock_events.push_front(z);
		    },
		    IOUDIS_ON /* 0xC07E */ => {
			debug!("IOUDIS_ON");
			self.iou_disabled = true
		    },
		    IOUDIS_OFF /* 0xC07F */ => {
			debug!("IOUDIS_OFF");
			self.iou_disabled = false
		    },
		    0xC080..=0xC08F => {
			self.configure_bank_switched_memory_access(true, addr)
		    },
		    _ => {self.read_write_io(addr, true, tick, val);}
//trace!("Dummy ! IO write address {:X} := {:X}",addr,val)
		}
	    },
	    // if addr == 0xC00B {
	    //     trace!("IO write : SLOTC3ROM (select slot ROM) {:X}", addr);
	    // } else
	    //     if addr == 0xC006 {
	    // 	  trace!("IO write : SLOTCXROM (select internal ROM) {:X}", addr);
	    //     } else {
	    // 	  trace!("IO write address {:X} := {:X}",addr,val);
	    //     }
	    0xC100..=0xC7FF => {
			let slot = ((addr >> 8) & 0x7) as usize;
			let (reqs, irqc) =
				self.slot_mut(slot).write_io(addr, val, tick);
			irq_change = irqc;

			if let Some(irq_requests) = reqs {
				self.process_irq_requests(irq_requests, tick);
			};
			//return (old_val, irq_change)
	    },
	    0xC800..=0xC8FF => {
		// Read "the seventh rom chip" in Sather's book (page 6-4) (and pages before)
		// FIXME I think here one's writing to ROM
		// but i'm not sure, there's also this I/O STROBE' thing I don't understand.
		//self.doIO(addr, val);
		// self.do_language_card_io(addr); self.noise();
	    },
	    _ => { error!("Write to unsupported address ${:04X}", addr)}
	}

	(old_val, irq_change)
    }
}
