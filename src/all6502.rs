use core::fmt;

use log::{debug, info};
use strum_macros::Display;
use crate::m6502::{
    reset_cpu, Cycle6502, M6502_BF, M6502_CF, M6502_DF, M6502_IF, M6502_IRQ, M6502_NF, M6502_RES,
    M6502_RW, M6502_SYNC, M6502_VF, M6502_XF, M6502_ZF
};

//const CYCLES_BEFORE_PULL_UP: usize=14;

#[derive(Clone, Copy)]
pub enum IrqLineState {
    PulledUp,
    PulledDown,
    Unknown(u8)
}

impl fmt::Display for IrqLineState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            IrqLineState::PulledUp => write!(f, "Up"),
            IrqLineState::PulledDown => write!(f, "Down"),
            IrqLineState::Unknown(x) => write!(f, "Unknown {}",x),
        }
    }
}


#[derive(Display)]
pub enum CpuEmulator {
    None,
    Obscoz6502,
    Obscoz65c02,
    Floooh6502
}

#[derive(Clone,Copy)]
pub struct CpuSnaphsot {
    // FIXME Ideally this one should be immutable.
    pub cycle: u64,
    pub a: u8,
    pub x: u8,
    pub y: u8,
    pub sp: u16,
    pub pc: u16,
    pub ad: u16,
    pub flags: u8,
    pub ir: u8,
    pub irq_line: IrqLineState
}

impl fmt::Display for CpuSnaphsot {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f,
            "SP=01{:02X} A={:02X}, X={:02X}, Y={:02X} flags:{}{}{}{}{}{}{}{} AD:{:04X} IR:{:02X} IrqLine:{}",
            self.sp,
            self.a,
            self.x,
            self.y,
            if self.flags & M6502_NF != 0 { "N" } else { "-" },
            if self.flags & M6502_VF != 0 { "V" } else { "-" },
            if self.flags & M6502_XF != 0 { "X" } else { "-" },
            if self.flags & M6502_BF != 0 { "B" } else { "-" },
            if self.flags & M6502_DF != 0 { "D" } else { "-" },
            if self.flags & M6502_IF != 0 { "I" } else { "-" },
            if self.flags & M6502_ZF != 0 { "Z" } else { "-" },
            if self.flags & M6502_CF != 0 { "C" } else { "-" },
            self.ad, self.ir, self.irq_line
        )

        //write!(f, "Cycle:{:>10} PC:${:04X} AD:${:04X} IR:{:02X} IrqLine:{}", self.cycle, self.pc, self.ad, self.ir, self.irq_line)
    }
}

pub trait Cpu6502 {
    fn ir(&self) -> u8;
    fn pc(&self) -> u16;
    fn s(&self) -> u16;
    fn a(&self) -> u8;
    fn x(&self) -> u8;
    fn y(&self) -> u8;
    fn flags(&self) -> u8;

    fn is_reading(&self) -> bool;

    fn is_writing(&self) -> bool;

    fn data(&self) -> u8;

    fn set_data(&mut self, d:u8);

    fn tick(&mut self);

    fn address(&self) -> u16;

    fn pull_reset_pin_down(&mut self);

    fn pull_reset_pin_up(&mut self);

    fn pull_irq_pin_down(&mut self);

    fn pull_irq_pin_up(&mut self);

    fn cycles(&self) -> u64;

    fn reset_cycles(&mut self);

    fn begins_instruction(&self) -> Option<u16>;

    fn snapshot(&self) -> CpuSnaphsot {
        CpuSnaphsot {
            cycle: self.cycles(),
            a: self.a(),
            x: self.x(),
            y: self.y(),
            sp: self.s(),
            pc: self.pc(),
            ad: self.address(),
            flags: self.flags(),
            irq_line: self.irq_line_state(),
            ir: self.ir()
        }
    }

    fn snapshot_begin_instruction(&self) -> CpuSnaphsot {
        assert!(self.begins_instruction().is_some());
        CpuSnaphsot {
            cycle: self.cycles(),
            a: self.a(),
            x: self.x(),
            y: self.y(),
            sp: self.s(),
            pc: self.begins_instruction().unwrap(),
            ad: self.address(),
            flags: self.flags(),
            irq_line: self.irq_line_state(),
            ir: self.ir()
        }
    }

    fn set_pc(&mut self, pc:u16, mem_at_pc: u8);

    fn accepts_irq(&self) -> bool {
        const IRQ_DISABLE: u8 = 0b0000_0100;
        self.flags() & IRQ_DISABLE == 0
    }

    fn irq_handling_has_started(&self) -> bool {
        false
    }

    fn emulator(&self) -> CpuEmulator;

    fn irq_line_state(&self) -> IrqLineState {
        IrqLineState::Unknown(0)
    }
}



pub struct Obscoz6502 {
    cpu: oz65c02::n6502::Cpu,
    //irq_pulled_down: usize,
    pub cycles: u64
}

impl Obscoz6502 {
    pub fn new() -> Obscoz6502 {
        Obscoz6502 {
            cpu: oz65c02::n6502::Cpu::new(),
            //irq_pulled_down: 0,
            cycles: 0
        }
    }
}

pub fn show_debug(cpu: &dyn Cpu6502) {
    debug!("IRQ in progress {} AD={:04X} PC={:04X} P={:08b} A={}", cpu.cycles(), cpu.address(), cpu.pc(), cpu.flags(), cpu.a());
}

impl Cpu6502 for Obscoz6502 {
    fn ir(&self) -> u8 {
        self.cpu.ir
    }

    fn pc(&self) -> u16 {
        self.cpu.pc.into()
    }

    fn s(&self) -> u16 {
        self.cpu.sp.into()
    }
    fn a(&self) -> u8 {
        self.cpu.a
    }
    fn x(&self) -> u8 {
        self.cpu.x
    }
    fn y(&self) -> u8 {
        self.cpu.y
    }

    fn flags(&self) -> u8 {
        self.cpu.p.into()
    }


    fn is_reading(&self) -> bool {
        self.cpu.rw != 0
    }

    fn is_writing(&self) -> bool {
        self.cpu.rw == 0
    }

    fn data(&self) -> u8 {
        self.cpu.d
    }

    fn set_data(&mut self, d:u8) {
        self.cpu.d = d;
    }

    fn tick(&mut self) {
        self.cpu.cycle();
        self.cycles += 1;

        // if self.irq_pulled_down > 0 {
        //     show_debug(self);
        // }

        // if self.irq_pulled_down > 1 {
        //     self.irq_pulled_down -= 1;
        // } if self.irq_pulled_down == 1 {
        //     debug!("6502 Pull up!");
        //     self.cpu.irq_pull_up();
        //     self.irq_pulled_down = 0;
        // }
    }

    fn address(&self) -> u16 {
        self.cpu.ad.into()
    }

    fn pull_reset_pin_down(&mut self) {
        // Assert pin is up
        self.cpu.res_pull_down()
    }

    fn pull_reset_pin_up(&mut self) {
        // Assert pin is up
    }

    fn pull_irq_pin_down(&mut self) {
        show_debug(self);
        self.cpu.irq_pull_down();
        //self.irq_pulled_down = CYCLES_BEFORE_PULL_UP;
    }

    fn pull_irq_pin_up(&mut self) {
        self.cpu.irq_pull_up();
    }

    fn cycles(&self) -> u64 {
        self.cycles
    }

    fn reset_cycles(&mut self) {
        self.cycles = 0;
    }

    fn begins_instruction(&self) -> Option<u16> {
        /* IR=0x00 tick=5 A=0xaa X=0x00 Y=0x00 NV1BDIZC=0x0000010100010100 SP=0xfd PC=0x0000 CGL=0xfffc Q=6
        Read  ad=0xfffd v=0x0c sync=false

        IR=0x00 tick=6 A=0xaa X=0x00 Y=0x00 NV1BDIZC=0x0000010100010100 SP=0xfd PC=0x0c01 CGL=0xfffe Q=0
        Read  ad=0x0c00 v=0x4c sync=true <<<<<------

        IR=0x4c tick=7 A=0xaa X=0x00 Y=0x00 NV1BDIZC=0x0000010100010100 SP=0xfd PC=0x0c02 CGL=0xfffe Q=1
        Read  ad=0x0c01 v=0xc4 sync=false */

        if self.cpu.sync() {
            // We're at the start of an instruction. Actually, the IR will be
            // loaded on the next cycle and the PC is one byte ahead of the
            // actual beginning of the instruction. So neither of these are
            // useful, that's why we look at the address.
            Some(self.address())
        } else {
            None
        }
    }

    fn set_pc(&mut self, pc:u16, mem_at_pc: u8) {
        self.cpu.cycle_at(pc, mem_at_pc);
        //println!("pc: {:04X} ad {:04X} ir:{:02X} ", pc, self.address(), self.ir());
    }

    fn irq_handling_has_started(&self) -> bool {
        let micro_instuction = 8 * self.cpu.ir as usize + self.cpu.q as usize;

        // Detect an IRQ is ongoing by watching the CPU fetch the
        // IRQ vector while in the interrupt procedure.
        let irq_vector: u16 = 0xFFFE;
        micro_instuction == 5 && self.address() == irq_vector
    }

    fn emulator(&self) -> CpuEmulator {
        CpuEmulator::Obscoz6502
    }

    fn irq_line_state(&self) -> IrqLineState {
        match self.cpu.n_int_pnd {
            0 => IrqLineState::PulledDown,
            1 => IrqLineState::PulledUp,
            _ => IrqLineState::Unknown(self.cpu.n_int_pnd)
        }
    }

}



pub struct Floooh6502 {
    cpu: Cycle6502,
    pins: u64,
    //irq_pulled_down: usize
}

impl Floooh6502 {
    pub fn new() -> Floooh6502 {
        Floooh6502 {
            cpu: reset_cpu(),
            pins: 0,
            //irq_pulled_down : 0
        }
    }

    fn set_data(&mut self, data: u8) {
        self.pins =  (self.pins & !0xFF0000) | (((data as u64) << 16) & 0xFF0000);
    }
}


impl Cpu6502 for Floooh6502 {
    fn ir(&self) -> u8 {
        (self.cpu.IR >> 3) as u8
    }

    fn pc(&self) -> u16 {
        self.cpu.PC
    }

    fn s(&self) -> u16 {
        self.cpu.S as u16 + 0x100u16
    }
    fn a(&self) -> u8 {
        self.cpu.A
    }
    fn x(&self) -> u8 {
        self.cpu.X
    }
    fn y(&self) -> u8 {
        self.cpu.Y
    }

    fn flags(&self) -> u8 {
        self.cpu.P
    }


    fn is_reading(&self) -> bool {
        self.pins & M6502_RW != 0
    }

    fn is_writing(&self) -> bool {
        !self.is_reading()
    }

    fn data(&self) -> u8 {
        ((self.pins & 0x00FF0000) >> 16) as u8
    }

    fn set_data(&mut self, d:u8) {
        self.pins = (self.pins & !0xFF0000) | (((d as u64) << 16) & 0xFF0000);
    }

    fn tick(&mut self) {
        self.pins = self.cpu.tick(self.pins);

        // Automagically pull down IRQ if it's up.

        // FIXME Totally dirty, this should be handled by the device
        // which serves the IRQ !!!

        // if self.irq_pulled_down > 0 {
        //     show_debug(self);
        // }

        // if self.irq_pulled_down > 1 {
        //     self.irq_pulled_down -= 1;
        // } if self.irq_pulled_down == 1 {
        //     // Pull up
        //     debug!("Floooh6502 Pull up!");
        //     self.pins &= !M6502_IRQ;
        //     self.irq_pulled_down = 0;
        // }

        //self.pull_irq_pin_up();

    }

    fn address(&self) -> u16 {
        (self.pins & 0xFFFF) as u16
    }

    fn pull_reset_pin_down(&mut self) {
        // Assert pin is up
        self.pins |= M6502_SYNC | M6502_RES;
        self.cpu.S = 0;
    }

    fn pull_reset_pin_up(&mut self) {
        // Assert pin is up
    }

    fn pull_irq_pin_down(&mut self) {
        debug!("Floooh6502: pull_irq_pin_down");
        self.pins |= M6502_IRQ;
        //self.irq_pulled_down = CYCLES_BEFORE_PULL_UP;
    }

    fn irq_line_state(&self) -> IrqLineState {
        if self.pins & M6502_IRQ > 0 {
            IrqLineState::PulledDown
        } else  {
            IrqLineState::PulledUp
        }
    }

    fn pull_irq_pin_up(&mut self) {
        self.pins &= !M6502_IRQ;
    }

    fn begins_instruction(&self) -> Option<u16> {
        if self.pins & M6502_SYNC != 0 {
            Some(self.cpu.PC)
        } else {
            None
        }
    }

    fn cycles(&self) -> u64 {
        self.cpu.cycles
    }

    fn reset_cycles(&mut self) {
        self.cpu.cycles = 0
    }

    fn set_pc(&mut self, pc:u16, mem_at_pc: u8) {
        info!("Setting PC to ${:04X}, mem=${:02X}", pc, mem_at_pc);
        self.cpu.PC = pc;
        self.pins |= M6502_SYNC;
        self.pins = (self.pins & 0x00FF0000) | ((mem_at_pc as u64) << 16);
        self.set_data(mem_at_pc);
    }

    fn emulator(&self) -> CpuEmulator {
        CpuEmulator::Floooh6502
    }

}







pub struct Obscoz65C02 {
    cpu: oz65c02::wdc65c02::Cpu,
    pub cycles: u64,
    //irq_pulled_down: usize
}

impl Obscoz65C02 {
    pub fn new() -> Obscoz65C02 {
        Obscoz65C02 {
            cpu: oz65c02::wdc65c02::Cpu::new(),
            cycles: 0,
            //irq_pulled_down: 0
        }
    }
}


impl Cpu6502 for Obscoz65C02 {
    fn ir(&self) -> u8 {
        self.cpu.ir
    }

    fn pc(&self) -> u16 {
        self.cpu.pc.into()
    }

    fn s(&self) -> u16 {
        self.cpu.sp.into()
    }
    fn a(&self) -> u8 {
        self.cpu.a
    }
    fn x(&self) -> u8 {
        self.cpu.x
    }
    fn y(&self) -> u8 {
        self.cpu.y
    }

    fn flags(&self) -> u8 {
        self.cpu.p.into()
    }


    fn is_reading(&self) -> bool {
        self.cpu.rw != 0
    }

    fn is_writing(&self) -> bool {
        self.cpu.rw == 0
    }

    fn data(&self) -> u8 {
        self.cpu.d
    }

    fn set_data(&mut self, d:u8) {
        self.cpu.d = d;
    }

    fn tick(&mut self) {
        self.cpu.cycle();
        self.cycles += 1;

        // if self.irq_pulled_down > 0 {
        //     show_debug(self);
        // }

        // if self.irq_pulled_down > 1 {
        //     self.irq_pulled_down -= 1;
        // } if self.irq_pulled_down == 1 {
        //     // Pull up
        //     self.cpu.irq_pull_up();
        //     self.irq_pulled_down = 0;
        // }

    }

    fn address(&self) -> u16 {
        self.cpu.ad.into()
    }

    fn pull_reset_pin_down(&mut self) {
        // Assert pin is up
        self.cpu.res_pull_down();
    }

    fn pull_reset_pin_up(&mut self) {
        // Assert pin is up
    }

    fn pull_irq_pin_down(&mut self) {
        // Assert pin is up
        self.cpu.irq_pull_down();
        // self.irq_pulled_down = CYCLES_BEFORE_PULL_UP;
    }

    fn pull_irq_pin_up(&mut self) {
        // Assert pin is up
        self.cpu.irq_pull_up()
    }

    fn cycles(&self) -> u64 {
        self.cycles
    }
    fn reset_cycles(&mut self) {
        self.cycles = 0;
    }

    fn begins_instruction(&self) -> Option<u16> {
        /* IR=0x00 tick=5 A=0xaa X=0x00 Y=0x00 NV1BDIZC=0x0000010100010100 SP=0xfd PC=0x0000 CGL=0xfffc Q=6
        Read  ad=0xfffd v=0x0c sync=false

        IR=0x00 tick=6 A=0xaa X=0x00 Y=0x00 NV1BDIZC=0x0000010100010100 SP=0xfd PC=0x0c01 CGL=0xfffe Q=0
        Read  ad=0x0c00 v=0x4c sync=true <<<<<------

        IR=0x4c tick=7 A=0xaa X=0x00 Y=0x00 NV1BDIZC=0x0000010100010100 SP=0xfd PC=0x0c02 CGL=0xfffe Q=1
        Read  ad=0x0c01 v=0xc4 sync=false */

        if self.cpu.sync() {
            // We're at the start of an instruction. Actually, the IR will be
            // loaded on the next cycle and the PC is one byte ahead of the
            // actual beginning of the instruction. So neither of these are
            // useful, that's why we look at the address.
            Some(self.address())
        } else {
            None
        }
    }

    fn set_pc(&mut self, pc:u16, mem_at_pc: u8) {
        self.cpu.cycle_at(pc, mem_at_pc);
        //println!("pc: {:04X} ad {:04X} ir:{:02X} ", pc, self.address(), self.ir());
    }

    fn irq_handling_has_started(&self) -> bool {
        let micro_instuction = 8 * self.cpu.ir as usize + self.cpu.q as usize;

        // Detect an IRQ is ongoing by watching the CPU fetch the
        // IRQ vector while in the interrupt procedure.
        let irq_vector: u16 = 0xFFFE;
        micro_instuction == 5 && self.address() == irq_vector
    }

    fn emulator(&self) -> CpuEmulator {
        CpuEmulator::Obscoz65c02
    }

}
