//pub mod modulo {
    pub fn distance(a: f64, b: f64) -> f64 {
	// shortest distance on a modulo ring
	let u = (a - b).rem_euclid(360.0);
	let v = (b - a).rem_euclid(360.0);
	if u < v {
	    u
	} else {
	    v
	}
    }

    pub fn direction(from: f64, to: f64) -> f64 {
	// Direction to go from a to b by the shortest route

	let a = from.rem_euclid(360.0);
	let b = to.rem_euclid(360.0);

	if a == b {
            0.0
	} else  if a < b && (b - a) < 180.0 {
            // zone 1 : there's no crossing around zero degree; b is on the left of a.
            return 1.0
	} else if a > 180.0 && a < b {
            // zone 2: a > 180 and still no crossing of the zero degree angle.
            return 1.0
	} else if a > 180.0 && (360.0-a) + b < 180.0 {
            // zone 3: a crossing occur
            return 1.0
	} else {
            return -1.0
	}
    }

    pub fn middle(a: isize,b: isize) -> isize {
	// Center of the smallest interval on a modulo ring between a and b
	let u = (a - b).rem_euclid(360);
	let v = (b - a).rem_euclid(360);
	if u < v {
            b.rem_euclid(360) + u/2
	} else {
            a.rem_euclid(360) + v/2
	}
    }
//}
