const PI: f32 = std::f32::consts::PI;

pub const SIGNAL_14M_FREQ: f32 = 14_318_180.0;

pub const CHROMA_BANDWIDTH: f32 = 1_900_000.0f32;
pub const NTSC_CARRIER_FREQ: f32 = 3_579_545.0;
// pub const NTSC_I_BANDWIDTH: f32 = 1_300_000.0;
// pub const NTSC_Q_BANDWIDTH: f32 =   400_000.0;

// According to wikipedia (look for "chrominance")
// const PAL_CARRIER_FREQ: f32 = 4_433_618.75;
// const PAL_LUMINANCE_BANDWIDTH: f32 = PAL_CARRIER_FREQ;
// const PAL_I_BANDWIDTH: f32 = 1_600_000.0;
// const PAL_Q_BANDWIDTH: f32 = 1_600_000.0;

fn sin(x:f32) -> f32 {
    x.sin()
}

fn cos(x:f32) -> f32 {
    x.cos()
}

fn normalized_sinc(t: f32) -> f32 {
    if t == 0.0 {
        1.0
    } else {
        sin(PI * t) / (PI * t)
    }
}

fn blackmann_window(n: i32) -> Vec<f32> {
    let mut window: Vec<f32> = vec![];

    for i in 0..n {
        window.push(
            0.42f32 - 0.5*cos(2.0*PI*(i as f32)/((n-1) as f32)) + 0.08f32*cos(4.0*PI*(i as f32)/((n-1) as f32))
        );
    }
    // w[n]=0.42−0.5cos(2πnN−1)+0.08cos(4πnN−1),

    window
}


fn sinc_kernel( cutoff: f32, sampling_rate: f32, nb_taps: i32) -> Vec<f32> {
    // https://www.analog.com/media/en/technical-documentation/dsp-book/dsp_book_Ch16.pdf
    // https://tomroelandts.com/articles/how-to-create-a-simple-low-pass-filter

    assert!(nb_taps % 2 == 1);
    let mut kernel: Vec<f32> = vec![];
    let order = nb_taps / 2;

    // we go from -order to order, inclusive. So the filter is centered on zero
    // and has 2*order + 1 places.

    for i in -order..order+1 {
        // FC expressed as a fraction of the sampling rate
        let fc = cutoff / sampling_rate;
        kernel.push( 2.0*fc*normalized_sinc(2.0*fc*(i as f32)));
    }

    // Windowing

    let bmw = blackmann_window(2*order+1);

    for (ndx, bm) in bmw.iter().enumerate() {
        kernel[ndx] *= bm;
    }


    // Normalizing

    let sum = kernel.iter().sum::<f32>();
    for elem in kernel.iter_mut(){
        *elem /= sum;
    }

    kernel
}


pub fn filters(signal_sampling_rate: f32, luma_freq: f32,  chroma_freq: f32, nb_taps: i32, filter: &mut Vec<f32>) {
    // To store filters, I have 4 lines of 256 floats. (see GPU Buffer
    // intialisation)
    for (i, f) in sinc_kernel(luma_freq / 2.0, signal_sampling_rate, nb_taps).iter().enumerate() {
        filter[i] = *f;
        //print!("{},", filter[i]);
    }
    for (i, f) in sinc_kernel(chroma_freq, signal_sampling_rate, nb_taps).iter().enumerate() {
        filter[256+i] = *f;
        //print!("{},", filter[i]);
    }
    for (i, f) in sinc_kernel(luma_freq, signal_sampling_rate, nb_taps).iter().enumerate() {
        filter[512+i] = *f;
        //print!("{},", filter[i]);
    }
    //println!("");

    // Then build anoter filter in filter[256+i]...
}