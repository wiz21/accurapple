// Publish modules to the whole project
// see : https://users.rust-lang.org/t/how-to-use-a-module-from-another-module/27882/2

mod apple_key;
mod a2;
mod diskii;
mod mem;
//mod util;
//mod cpu;
mod m6502;
mod mos6522;
//mod speaker;
mod speaker2;
mod sound;
mod emulator;
mod gfx_utils;
//mod gfx_tv;
mod gfx_tv3;
//mod debugger;
mod debug2;
mod wozfile;
mod wozpack;
mod rs6502;
mod modulo;
mod diskii_stepper;
mod stepper2;
mod configuration;

//mod diskii_stepper2;
mod lss;
//mod renderers;
//mod scaling;
mod generic_peripheral;
mod all6502;

mod filtering;
//mod disassembler;

//mod  all_peripherals;


use std::borrow::{Borrow, BorrowMut};
use std::collections::HashMap;
use std::fs::File;
use std::ops::Index;
use std::path::{PathBuf};
use std::str::FromStr;
use std::thread::{JoinHandle};
use std::time::{Duration, Instant};
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{Receiver, Sender, channel};
use std::io::{Read, Write};
use a2::{EMULATOR_SHORT_DESCRIPTION,CYCLES_PER_SECOND};
use apple_key::Key;
use debug2::{DebugFrame, Debugger, ScreenLabels, CRTColourBurstInterpretation};
use eframe::egui::{Context, self};
use egui::{FontData, FontDefinitions, FontFamily, FontId, IconData, Modifiers, Pos2, TextStyle, Visuals};
use egui_wgpu::{ScreenDescriptor};
use log::{debug, error, info, LevelFilter};

use circular_queue::CircularQueue;

use strum_macros::Display;
use wgpu::util::DownloadBuffer;


//use crossterm::terminal::disable_raw_mode;
use cpal::traits::StreamTrait;

use emulator::{start_emulator_thread, Command, make_logo, OutputQueues};
use sound::{find_configuration,start_cpal_audio_stream, SoundConfig};
//use debugger::{init_debugger,SimpleLogger,MyLogRecord};

use crate::debug2::{MyLogRecord, AccurappleSimpleLogger};
use crate::emulator::{Signals, CpuType};
use crate::configuration::{Configuration};

use wgpu_render::COMPUTED_COMPOSITE_WIDTH;

const SCREEN_WIDTH: u32 = 280*2 + 2;
const SCREEN_HEIGHT: u32 = 192*2;




fn save_screenshot(path: &std::path::Path, screen: &Vec<u8>, width: usize, height: usize) {
	let file = std::fs::File::create(path).unwrap();
	let w = &mut std::io::BufWriter::new(file);

	let mut encoder = png::Encoder::new(w,
		width as u32, height as u32);
		encoder.set_color(png::ColorType::Rgba);
		encoder.set_depth(png::BitDepth::Eight);
	let mut writer = encoder.write_header().unwrap();

	writer.write_image_data(
		&screen[0..height*width*4]
	).unwrap(); // Save
}


fn start_emulator(cli: &CliArgs, sample_rate: usize, headless:bool, configuration: &mut Configuration) -> (
    Arc<Mutex<OutputQueues>>,
    Sender<Command>,
    Receiver<DebugFrame>,
    Receiver<Signals>,
    JoinHandle<()>,
) {
	let (quit_tx, quit_rx): (Sender<Signals>, Receiver<Signals>) = channel();

	let mut initial_commands: Vec<Command> = Vec::new();

	//control_channel.send(Command::Run).expect("Must work");
	initial_commands.push(Command::Run);


	if cli.script.is_some() {
		//control_channel.send(Command::SetScript(cli.value_of("script").unwrap().to_string())).expect("Must work");
		initial_commands.push(Command::SetScript(cli.script.as_ref().unwrap().clone()))
	}


    // If tests order is important!
    let floppy = & if cli.floppy1.is_some() {
            cli.floppy1.clone()
        } else {
             configuration.last_floppy_file.clone()
        };

	if let Some(path) = floppy {
        if path.exists() {
            initial_commands.push(Command::LoadFloppy(path.clone()));
            initial_commands.push(Command::Reset);
        } else {
            let err = format!("Can't find path for disk image {}", path.to_string_lossy());
            error!("{}", err);
            println!("ERROR! {}", err);
            std::process::exit(-1);
        }
	}

	//control_channel.send(Command::StartDebugStream).expect("Must work");
    if !headless {
	    initial_commands.push(Command::StartDebugStream);
    }

	if cli.rom.is_some() {
		//control_channel.send(Command::ReadRom(PathBuf::from(cli.value_of("rom").unwrap()))).expect("Must work");
		initial_commands.push(Command::ReadRom(cli.rom.as_ref().unwrap().clone()));
	}

    // if cli.value_of("charset").is_some() {
	// 	//control_channel.send(Command::ReadCharsetRom(PathBuf::from(cli.value_of("charset").unwrap()))).expect("Must work");
	// 	initial_commands.push(Command::ReadCharsetRom(PathBuf::from(cli.value_of("charset").unwrap())));
	// }

	if cli.no_sound {
        println!("Sound disabled");
		initial_commands.push(Command::SoundStop);
	}
    // If tests order is important!
    configuration.cpu = match cli.cpu {
        Cpus::Obscoz6502 => CpuType::Obscoz6502,
        Cpus::Obscoz65c02 => CpuType::Obscoz65C02,
        Cpus::Floooh6502 => CpuType::Floooh6502,
    };

	let (output_queues, control_channel, debug_stream_rx, emu_thread) =
		start_emulator_thread(
			sample_rate,
			configuration.cpu.clone(),
			quit_tx,
			initial_commands);

	(output_queues, control_channel, debug_stream_rx, quit_rx, emu_thread)
}

#[derive(PartialEq, Display)]
enum KeyState {
    Pressed,
    Released,
    PressedProcessed
}

struct TimedEvent {
    time: u128,
    event: egui::Event,
    pressed: KeyState,
    key: apple_key::Key
}

fn egui_event_to_apple_key(event: &egui::Event) -> Option<apple_key::Key> {
    match event {
        egui::Event::Key { key, pressed, repeat,
                modifiers: Modifiers { alt, ctrl, shift, mac_cmd, command }, physical_key: _ } => {

            //println!("egui_event_to_apple_key: {}", key.symbol_or_name());
            let akey = match key {
                egui::Key::Enter => Some(Key::Return),
                egui::Key::ArrowLeft => Some(Key::Left),
                egui::Key::ArrowRight => Some(Key::Right),
                egui::Key::Escape => Some(Key::Escape),
                egui::Key::ArrowUp => Some(Key::Up),
                egui::Key::ArrowDown => Some(Key::Down),
                egui::Key::Tab => Some(Key::Tab),
                egui::Key::Backspace => Some(Key::Delete),
                egui::Key::F2 => Some(Key::Reset),
                egui::Key::F3 => None,
                egui::Key::F4 => None,
                egui::Key::Space => Some(Key::Character(' ')),
                egui::Key::Quote => Some(Key::Character('\'')),
                _ => {
                    let char = key.symbol_or_name().chars().next().unwrap();
                    Some(Key::Character(char))
                }
            };

            akey
        },
        _ => None
    }
}


pub struct AccurappleApp {
    angle: f32,
    apple_ram_bytes: Arc<Mutex<Box<[u8; 24576]>>>,
    soft_switches_bytes: Arc<Mutex<Box<[u8; 7680]>>>,
	output_queues: Arc<Mutex<OutputQueues>>,
    nb_frames: usize,
    last_nb_frames: usize,
    total_frames: usize,
    time_base: Instant,
    old_time_base: Instant,
    time_frame_chrono: Instant,
    debugger: Debugger,
    control_channel: Sender<Command>,
    emulator_pause: bool,
    //signals_channel: Receiver<Signals>
    ram_to_composite_uniforms: RAMToCompositeUniform,
    keyboard: HashMap<String, TimedEvent>
}

// Heavily based on
// https://stackoverflow.com/questions/72855505/what-is-the-best-way-to-update-app-fields-from-another-thread-in-egui-in-rust
fn trigger_app_update(state_clone: Arc<Mutex<Context>>) {
    loop {
		/*

		Pour le moment, je fais avec un time based trigger.
		Après je devrais le faire avec un channel (ou un Mutex<bool>, ou un Struct tokio::sync::Notify ?)
        dans lequel je mets un flag "frame ready".


		Alors ici:

		J'attends la notif.
		Dès que je l'ai j'averti. L'App ira alors voir dans la OutputQueue s'il y a quelque chose (donc bien remplir la outputqueue avant de lancer la notif !)

		*/
		// 1000 ms / 50fps = 20 ms
        std::thread::sleep(std::time::Duration::from_millis(20u64));
        let ctx = &state_clone.lock().ok();
        match ctx {
            Some(x) => x.request_repaint(),
            None => panic!("error in Option<>"),
        }
    }
}


use accurashader::egui::{init_accurapple_render_pipeline, insert_accurapple_render_pipeline_in_egui};
use accurashader::wgpu_render::{self, Pipeline, RAMToCompositePipeline, RAMToCompositeUniform};

pub struct AccurappleImageCallback {
    pub left_top_corner: Pos2,
    pub output_queues: Arc<Mutex<OutputQueues>>,
    pub composite_to_rgb_uniforms: wgpu_render::CompositeToRGBUniform,
    pub uniforms2: wgpu_render::RAMToCompositeUniform,
    pub rgb_to_host_uniforms: wgpu_render::RGBToHostShaderConstants,
    pub filters : Vec<f32>,
    debug_gfx_planned: bool
}

fn record_iou_debug_information(
    device: &wgpu::Device,
    queue: &wgpu::Queue,
    accurapple_pipeline: &Pipeline,
    filename_: &PathBuf,
    debugged_line: u32) {
    let filename = filename_.clone();

    let buffer_slice: wgpu::BufferSlice<'_> = accurapple_pipeline.ram_to_composite_pipeline.debug_buffer.slice(..);

    DownloadBuffer::read_buffer(device, queue, &buffer_slice,
        move |r| {
        if r.is_ok() {
            // This buffer is in fact an f32's buffer.
            // But so far we don't re-interpret it.
            // FIXME Is this portable across GPU's ?
            let r_result = &r.unwrap()[..];
            //let d2: &[f32] = bytemuck::cast_slice( r_result );

            let filename2 = filename.to_string_lossy().to_string();
            let file = std::fs::File::create(filename).unwrap();
            let w = &mut std::io::BufWriter::new(file);
            use byteorder::WriteBytesExt;
            let _ = w.write(r_result);
            let _ = w.write_i32::<byteorder::LittleEndian>(debugged_line as i32); // Save

            let r = w.flush();

            if r.is_ok() {
                info!("Saved debug gfx data in {}", filename2);
            } else {
                error!("Failed to save debug gfx data in {}", filename2);
            }
        } else {
            error!("IOU debug info download buffer not ready to read ?")
        }
    });


}

fn take_screenshot_crt(
    device: &wgpu::Device,
    queue: &wgpu::Queue,
    accurapple_pipeline: &Pipeline,
    filename_: &PathBuf) {

    let filename = filename_.clone();

    let buffer_slice = accurapple_pipeline.composite_to_rgb_pipeline.apple_rgb_pixels_buffer.slice(..);

    DownloadBuffer::read_buffer(device, queue, &buffer_slice,
        |r | {
        if r.is_ok() {
            let r_result = &r.unwrap()[..];
            let d2: &[f32] = bytemuck::cast_slice( r_result );

            let mut pixels_float: Vec<u8> = vec![];

            // Make lines twice their height so that we get a proper aspect ratio.
            const LINE_THICKNESS:u32=2;

            for line in 0..192 {
                let ofs = line*COMPUTED_COMPOSITE_WIDTH*4;
                for _thickness in 0..LINE_THICKNESS {
                    for i in (0..COMPUTED_COMPOSITE_WIDTH*4).step_by(4) {
                        pixels_float.push((d2[ofs+i]*255.0) as u8);
                        pixels_float.push((d2[ofs+i+1]*255.0) as u8);
                        pixels_float.push((d2[ofs+i+2]*255.0) as u8);
                    }
                }
            }


            // info!("Saving CRT screenshot in {}", filename.to_string_lossy());
            let file = std::fs::File::create(filename).unwrap();
            let w = &mut std::io::BufWriter::new(file);

            let mut encoder = png::Encoder::new(w, COMPUTED_COMPOSITE_WIDTH as u32, 192*LINE_THICKNESS);
            encoder.set_color(png::ColorType::Rgb);
            encoder.set_depth(png::BitDepth::Eight);
            let mut writer = encoder.write_header().unwrap();

            writer
                .write_image_data(&pixels_float)
                .unwrap(); // Save
        } else {
            error!("Texture download buffer not ready to read ?")
        }
    });

}


fn take_screenshot_composite(
    device: &wgpu::Device,
    queue: &wgpu::Queue,
    accurapple_pipeline: &Pipeline,
    filename_: &PathBuf) {

    let buffer_slice = accurapple_pipeline.ram_to_composite_pipeline.composite_copy_buffer.slice(..);
    let filename = filename_.clone();

    DownloadBuffer::read_buffer(device, queue, &buffer_slice,
        |r | {
            if r.is_ok() {
                let r_result = &r.unwrap()[..];
                let d2: &[f32] = bytemuck::cast_slice( r_result );

                let mut pixels_float: Vec<u8> = vec![];
                for x in d2.iter() {
                    pixels_float.push(if *x > 0.0f32 { 255 } else { 0 });
                }

                info!("Saving composite signal screenshot in {}", filename.to_string_lossy());
                let file = std::fs::File::create(filename).unwrap();
                let w = &mut std::io::BufWriter::new(file);

                let mut encoder = png::Encoder::new(w, COMPUTED_COMPOSITE_WIDTH as u32, 192);
                encoder.set_color(png::ColorType::Grayscale);
                encoder.set_depth(png::BitDepth::Eight);
                let mut writer = encoder.write_header().unwrap();

                writer
                    .write_image_data(&pixels_float)
                    .unwrap(); // Save

                // use std::io::Write;
                // let mut f1 = std::fs::File::create("my_file.dat").unwrap();
                // f1.write_all(d2).expect("write must succeed");
            }
        });

    /*
    let (tx, rx) = futures_intrusive::channel::shared::oneshot_channel();
    buffer_slice.map_async(
        wgpu::MapMode::Read,
        move |result| {
            tx.send(result).unwrap();
        });
    device.poll(wgpu::Maintain::Wait);

    use futures::executor;
    executor::block_on({
        rx.receive()
    }).unwrap().unwrap();

    let data = buffer_slice.get_mapped_range();

    let d2: &[u8] = &data[..];

    use std::io::Write;
    let mut f1 = std::fs::File::create("my_file.dat").unwrap();
    f1.write_all(d2).expect("write must succeed");

    // let s:&[f32] = bytemuck::cast_slice( d2);
    // println!("{}", s.len());


    // use image::{ImageBuffer, Rgba};
    // let buffer =
    //     ImageBuffer::<Rgba<u8>, _>::from_raw(560, 192, data).unwrap();
    // buffer.save("image.png").unwrap();
    */

}

// Documentation is here:
// https://github.com/emilk/egui/blob/master/crates/egui-wgpu/src/renderer.rs
impl egui_wgpu::CallbackTrait for AccurappleImageCallback {
    fn prepare(
        &self,
        device: &wgpu::Device,
        wgpu_queue: &wgpu::Queue,
        _screen_descriptor : &ScreenDescriptor,
        _egui_encoder: &mut wgpu::CommandEncoder,
        resources: &mut egui_wgpu::CallbackResources
    ) -> Vec<wgpu::CommandBuffer> {

        let accurapple_pipeline: &Pipeline = resources.get().unwrap();

        {
            debug!("locking output queues for display");

            let mut oq_lock = self.output_queues.lock();
            let oq = oq_lock.as_mut().unwrap();

            if let Some(pathbuf) = oq.screenshot.as_ref() {
                // FIXME This not 100% correct: when oq.screenshot buffer is
                // true, I'll record the *previous* frame which was rendered
                // before, I don't record the frame passed in oq !
                take_screenshot_crt(device, wgpu_queue, accurapple_pipeline, pathbuf);
                // let debug_path = PathBuf::from_str("iou_debug.out").unwrap();
                // take_screenshot_iou_debug(device, queue, accurapple_pipeline, &debug_path);

                let mem_path = PathBuf::from_str("dhgr.bin").unwrap();
                let file = std::fs::File::create(mem_path).unwrap();
                let w = &mut std::io::BufWriter::new(file);
                w
                    .write(&oq.scanned_ram)
                    .unwrap(); // Save
                w
                    .write(&oq.rendered_soft_switches)
                    .unwrap(); // Save
                oq.screenshot = None; // Don't record twice
            }

            if let Some(pathbuf) = oq.screenshot_composite.as_ref() {
                // FIXME This not 100% correct: when oq.screenshot buffer is
                // true, I'll record the *previous* frame which was rendered
                // before, I don't record the frame passed in oq !
                take_screenshot_composite(device, wgpu_queue, accurapple_pipeline, pathbuf);
                oq.screenshot_composite = None; // Don't record twice
            }

            if let Some(pathbuf) = oq.screenshot_debug_timing.as_ref() {
                // FIXME This not 100% correct: when oq.screenshot buffer is
                // true, I'll record the *previous* frame which was rendered
                // before, I don't record the frame passed in oq !
// println!("\n\n\nself.uniforms2.line_to_debug {}", self.uniforms2.line_to_debug);
                record_iou_debug_information(device, wgpu_queue, accurapple_pipeline, pathbuf, self.uniforms2.line_to_debug);
                // Don't record twice
                oq.screenshot_debug_timing = None;
            }

            if !oq.consumed {

                // Note that for the moment, when the emulator
                // is paused (e.g. breakpoint) the output_queues
                // is not filled anymore, so the "consumed" field
                // never goes to false.
                accurapple_pipeline.prepare(
                    wgpu_queue, _egui_encoder,
                    &oq.scanned_ram,
                    &oq.rendered_soft_switches,
                    self.left_top_corner,
                    &self.uniforms2,
                    // self.uniforms2.sw_clock_modulo,
                    // self.uniforms2.phase0_offset,
                    // self.uniforms2.flash,
                    // oq.line_to_debug,
                    &self.filters,
                    &self.composite_to_rgb_uniforms);
                oq.consumed = true;
                //oq.last_time_displayed = Instant::now();
            }

            // For the display to work eventhough we don't update
            // the queue anymore (see note above)
            accurapple_pipeline.prepare_window_size(wgpu_queue, self.left_top_corner, &self.rgb_to_host_uniforms);
        }

        Vec::new()
    }

    fn paint(
        &self,
        info: egui::PaintCallbackInfo,
        render_pass: &mut wgpu::RenderPass<'static>,
        callback_resources: &egui_wgpu::CallbackResources,
    ) {
        let pipeline: &Pipeline = callback_resources.get().unwrap();
        pipeline.paint(render_pass);
    }
}


fn setup_styles(ctx: &egui::Context) {

        let mut fonts = FontDefinitions::default();

        // Install my own font (maybe supporting non-latin characters):
        fonts.font_data.insert(
            "my_font".to_owned(),
            FontData::from_static(include_bytes!("../data/PrintChar21.ttf")),
        );
        fonts.font_data.insert(
            "MotterTektura".to_owned(),
            FontData::from_static(include_bytes!("../data/motter-tektura-neue.ttf")),
        );

        // Put my font first (highest priority):
        fonts
            .families
            .get_mut(&FontFamily::Monospace)
            .unwrap()
            .insert(0, "my_font".to_owned());

        fonts.families.insert(
            egui::FontFamily::Name("Title".into()),
            vec!["MotterTektura".to_owned()],
        );

        ctx.set_fonts(fonts);

        ctx.all_styles_mut(|style| {

            style.visuals = Visuals::dark();

            // Redefine text_styles
            style.text_styles = [
                (
                    TextStyle::Heading,
                    FontId::new(20.0, FontFamily::Name("Title".into())),
                ),
                (
                    TextStyle::Name("Heading1".into()),
                    FontId::new(60.0, FontFamily::Name("Title".into())),
                ),
                (
                    TextStyle::Name("Heading2".into()),
                    FontId::new(25.0, FontFamily::Proportional),
                ),
                (
                    TextStyle::Name("Context".into()),
                    FontId::new(23.0, FontFamily::Proportional),
                ),
                (TextStyle::Body, FontId::new(18.0, FontFamily::Proportional)),
                (
                    TextStyle::Monospace,
                    FontId::new(14.0, FontFamily::Monospace),
                ),
                (TextStyle::Button, FontId::new(14.0, FontFamily::Monospace)),
                (
                    TextStyle::Small,
                    FontId::new(10.0, FontFamily::Proportional),
                ),
            ]
            .into();
        });
}

pub fn read_charset_rom(rom_filename: PathBuf) -> [u8; 4096] {
    let fname = rom_filename.to_string_lossy().to_string();

    let mut rom = Vec::<u8>::with_capacity(4096);
    let fna= rom_filename;
    if let Some(mut file) = File::open(fna).ok() {
        if file.read_to_end(&mut rom).is_ok() {
            println!("Loaded charset ROM {}", fname);
            let length = rom.len();
            rom.try_into().expect(
                format!( "ROM size is not correct. I expected 4096, you gave {}",
                    length).as_str())
        } else {
            panic!("Cant load ROM file {}", fname);
        }
    } else {
        panic!("Cant find ROM file {}", fname);
    }
}


impl AccurappleApp {
    pub fn new(cc: &eframe::CreationContext, output_queues: Arc<Mutex<OutputQueues>>, debugger: Debugger,
        control_channel: Sender<Command>, quit_rx: Receiver<Signals>, charset_rom: Option<PathBuf>,
        path_apple_to_composite_shader: Option<PathBuf>)  -> Self {
        // Get the WGPU render state from the eframe creation context. This can also be retrieved
        // from `eframe::Frame` when you don't have a `CreationContext` available.
        let wgpu_render_state = cc.wgpu_render_state.as_ref().unwrap();

        //println!("{:?}",wgpu_render_state.target_format);


        let rom_data = if let Some(crom) = charset_rom {
            read_charset_rom(crom)
        } else {
            *include_bytes!("../data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin")
        };

        // First we create our own Accurapple pipeline
        let pipeline = init_accurapple_render_pipeline(
            &wgpu_render_state.device, wgpu_render_state.queue.as_ref(),
            &rom_data,
            path_apple_to_composite_shader
        );
        // Then we store the pipeline in a place where it will be reachable when
        // we need it.
        insert_accurapple_render_pipeline_in_egui(cc, pipeline);

        setup_styles(&cc.egui_ctx);

        let state = Arc::new(Mutex::new(cc.egui_ctx.clone()));
        std::thread::spawn(move || {

            // This will loop eternally. It is the thread of the UI.
            loop {
                // Will utlimately call AccurappleApp::update(...)

                // The idea is to make sure we update the UI at least 50 times
                // a second or faster if the emulator produces frames a bit faster.
                let rec = quit_rx.recv_timeout(Duration::from_millis(20));

                //std::thread::sleep(std::time::Duration::from_millis(20u64));
                let ctx = &state.lock().ok();
                match ctx {
                    Some(x) => {
                        if let Ok(signal) = rec {
                            if signal == Signals::Quit {
                                break;
                            }
                            x.memory_mut(|m| m.data.insert_temp(egui::Id::NULL, Some(signal)));
                        } else {
                            x.memory_mut(|m| m.data.insert_temp(egui::Id::NULL, None::<Signals>));
                        }
                        x.request_repaint()
                    },
                    None => panic!("error while acquiring mutex"),
                }
            }
            info!("Leaving UI thread");
            std::process::exit(0);
            /*
            #[cfg(windows)]
            use winapi::um::{
                winuser::{GetActiveWindow,DestroyWindow},
            };
            #[cfg(windows)]
            let hwnd: Option<*mut winapi::shared::windef::HWND__> = if cfg!(windows) {
                unsafe {
                    let hwnd = GetActiveWindow();
                    if !hwnd.is_null() {
                        Some(hwnd)
                    } else {
                        None
                    }
                }
            } else {
                None
            };
            #[cfg(windows)]
            unsafe {
                let _ret = DestroyWindow(hwnd.unwrap());
                // dbg!(&_ret);
            }

            */
        });



        Self { angle: 0.0,
            apple_ram_bytes: Arc::new( Mutex::new (Box::new([0u8; 24576]))),
            soft_switches_bytes: Arc::new( Mutex::new (Box::new([0u8; 7680]))),
			output_queues,
            nb_frames: 0,
            last_nb_frames: 0,
            time_frame_chrono: Instant::now(),
            time_base: Instant::now(),
            old_time_base: Instant::now(),
            debugger,
            control_channel,
            total_frames: 0,
            emulator_pause: false,
            ram_to_composite_uniforms: RAMToCompositeUniform::new(),
            keyboard: HashMap::new()
        }
    }

    fn paint_accurapple_image(&mut self, ui: &mut egui::Ui,
        composite_to_rgb_uniforms: wgpu_render::CompositeToRGBUniform,
        ram_to_composite_uniforms: wgpu_render::RAMToCompositeUniform,
        rgb_to_host_uniforms: wgpu_render::RGBToHostShaderConstants,
        filters: Vec<f32>
    ) {
        let (rect, _response) =
            ui.allocate_exact_size(
                egui::vec2(
                    COMPUTED_COMPOSITE_WIDTH as f32 * 2.0/ui.ctx().pixels_per_point(),
                    192.0*2.0*2.0/ui.ctx().pixels_per_point()), egui::Sense::drag());

        let r = _response.rect.left_bottom();
        let lt = _response.rect.left_top();


        // println!("ui.ctx().pixels_per_point() {}, r.y={}, low={}",
        //     ui.ctx().pixels_per_point(), r.y,
        //     1024.0 - r.y*0.20);

        ui.painter().add(egui_wgpu::Callback::new_paint_callback(
            rect,
            AccurappleImageCallback {
                left_top_corner: Pos2 {
                    x: r.x* ui.ctx().pixels_per_point(),
                    y: lt.y * ui.ctx().pixels_per_point() },
                output_queues: self.output_queues.clone(),
                composite_to_rgb_uniforms,
                uniforms2: ram_to_composite_uniforms,
                debug_gfx_planned: false,
                filters,
                rgb_to_host_uniforms
            }
        ));
    }
}



impl eframe::App for AccurappleApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {

            let mut new_frame_from_emulator = false;
            if let Some(signal_option) = ctx.memory(|m| m.data.get_temp::<Option<Signals>>(egui::Id::NULL)) {
                if let Some(signal) = signal_option {
                    match signal {
                        Signals::FrameAvailable => new_frame_from_emulator = true,
                        Signals::RecordVideoStart(path) => {
                            info!("Video recording started {}", path.to_string_lossy());
                        },
                        Signals::EmulationPaused => self.emulator_pause = true,
                        Signals::TakeScreenshot => {
                            //take_screenshot_crt(ctx.dev, queue, accurapple_pipeline, filename_)
                        }
                        _ => ()
                    }
                }
            }

            //ctx.memory_mut(|m| m.data.insert_temp(egui::Id::NULL, None::<Signals>));

            if self.time_frame_chrono.elapsed().as_millis() > 1000 {
                // This is for measuring performances
                {
                    debug!("locking output queues for perf analysis");

                    let oq_lock = self.output_queues.lock().unwrap();
                    self.nb_frames = oq_lock.done_frames - self.last_nb_frames;
                    self.last_nb_frames = oq_lock.done_frames;
                    self.time_frame_chrono = Instant::now();
                }
            }
            self.total_frames += 1;

            // For the screenshots, either the screenshot order comes from the
            // emulator, in which case it is translated into a regular output queue
            // frame and passed through the debugger; either it comes from the
            // debugger's UI (screenshot button) directly.

            self.debugger.consume_available_debug_frames();
            self.debugger.render_to_egui(ui, ctx);

            let mut last_time_display = Instant::now();
            let mut last_frame_displayed = 0;
            {
                let mut oq_lock = self.output_queues.lock();
                let oq = oq_lock.as_mut().unwrap();
                // println!("Reading oq.line_to_debug={} self.debugger.ram_to_composite_uniform.line_to_debug {}",
                //     oq.line_to_debug,
                //     self.debugger.ram_to_composite_uniform.line_to_debug);
                last_time_display = oq.last_time_displayed;
                last_frame_displayed = oq.done_frames;

                self.ram_to_composite_uniforms.line_to_debug = oq.line_to_debug;
                self.debugger.ram_to_composite_uniform.line_to_debug = oq.line_to_debug;

                match self.debugger.crt_colourburst_interpretation {
                    CRTColourBurstInterpretation::LineByLine => {
                        // Just copying the Apple PAL circuit decision on each line.
                        for (ndx, cb_flag) in oq.colour_bursts.iter().enumerate() {
                            self.debugger.composite_to_rgb_uniform.colour_burst[ndx] = *cb_flag as i32;
                        }
                        self.debugger.composite_to_rgb_uniform.colour_burst_crt_decision = 1;
                    }
                    CRTColourBurstInterpretation::FullScreenMono => {
                        let mut missing_colour_bursts = 0;
                        for (ndx, cb_flag) in oq.colour_bursts.iter().enumerate() {
                            self.debugger.composite_to_rgb_uniform.colour_burst[ndx] = *cb_flag as i32;
                            // FIXME Still need to investigate this
                            if ndx < 160 && *cb_flag == 0 {
                                missing_colour_bursts += 1;
                            }
                        }

                        // This decision is taken here because it's the place where we
                        // are somewhat closest to the virtual CRT (we could do it in
                        // main too but the loop over all colour burst line decisions is
                        // here).

                        if missing_colour_bursts >= 1  {
                            // A value of zero means the CRT will disable colours.
                            self.debugger.composite_to_rgb_uniform.colour_burst_crt_decision = 0;
                        } else {
                            self.debugger.composite_to_rgb_uniform.colour_burst_crt_decision = 1;
                        }

                        debug!("missing_colour_bursts={} colour_burst_crt_decision={}",
                            missing_colour_bursts,
                            self.debugger.composite_to_rgb_uniform.colour_burst_crt_decision);
                    }
                }
            }

            self.ram_to_composite_uniforms.sw_clock_modulo = self.debugger.sw_clock_modulo;
            self.ram_to_composite_uniforms.phase0_offset = self.debugger.phase0_offset;
            self.debugger.ram_to_composite_uniform.flash = ((self.debugger.current_cycle() / (((a2::CYCLES_PER_SECOND as f32) / (a2::FLASH_FREQUENCY * 2.0f32)) as u64)) % 2) as f32;

            let _emulator_window = egui::Window::new("emulation").constrain(false).show(ctx, |ui| {
                let _r = ui.next_widget_position();
                self.paint_accurapple_image(
                    ui,
                    self.debugger.composite_to_rgb_uniform,
                    self.debugger.ram_to_composite_uniform,
                    self.debugger.rgb_to_host_uniform,
                    self.debugger.filters.clone()
                );
            });

                //println!("focus {}", emulator_window.unwrap().response.has_focus()); // always false


                // This is tricky and probably bugged. The idea is that if the user
                // is in the debugger, when he its "gg" then "enter", the final "enter"
                // must not be sent to the emulator. This would happen if the main interface
                // had left its debugging screen (which happens on "gg"). So we evaluate
                // the fact that the emulator is paused or not before the keyboard loop
                // because the state of the emulator can change during the keyboard
                // loop when the debugger process then "enter" following the "gg".
                // Also note that the debugger is the first to know if the emulator
                // is entering pause or not because it's the debugger which controls
                // the emulator.

                let emu_paused = self.debugger.current_screen == ScreenLabels::Debugger;
                //let emu_paused = self.emulator_pause;

                let timebase = self.time_base.elapsed().as_millis();

                // Converts the thread local time into cycles. Of course the current cycle
                // in this thread is potentially very different than the one in the
                // emulator thread. The emulator will take care of handling that
                // difference. The most important thing here is to have an idea
                // of how much time separates each key presses/releases and to ahev
                // an order on those key presses/releases.
                let thread_local_cycle = (self.time_base.elapsed().as_secs_f32() * (CYCLES_PER_SECOND as f32)) as u64;


                ctx.input( |input_state| {

                    // Keyboard handling is difficult because of egui. For the moment,
                    // egui can't reliably detect "#" (among others). When you hit
                    // AltGr-3 (to get #), egui first report Key("3") then report
                    // a Text event with "#". All my code here is there to delay
                    // the action Key('3') a bit to make sure there's not a '#' Text event
                    // that comes to "redefine" it.

                    for event in &input_state.events {

                        let text_match =
                            match event {
                                egui::Event::Text(s) => {

                                    // Locate the key corresponding to the text
                                    // event. The problem is that there' no way
                                    // to reliably convert a Text event (string)
                                    // to a Key. So I base myslef on the
                                    // obsevration that the text event always
                                    // comes immeditaley after a Key event (and in the same update call).

                                    let mut res = false;

                                    for event in self.keyboard.values_mut() {
                                        // So, is our text event immeditaley (less
                                        // than one millisec) preceeded by a another
                                        // key event ?
                                        if event.time - timebase <= 1 && event.pressed == KeyState::Pressed {

                                            // Yes, so I bet this text events is linked to that older
                                            // key event. Now I make sure that we're looking at
                                            // keys that need a text event to be properly detected.
                                            const REMAPPED: &str = "&(!)#[]%,;:?./=+-_$!\"'<>";
                                            if let Some(ndx) = REMAPPED.find(s) {

                                                // We update the preceeding key event with the new key.
                                                // Of course we keep its time.
                                                // Now the key event is the one corresponding with
                                                // what the user typed. We're done.
                                                event.key = Key::Character(REMAPPED.chars().nth(ndx).unwrap() );
                                                res = true;
                                            }
                                        }
                                    }

                                    res
                                },
                                _ => false
                            };


                        if !text_match {
                            // The event we're looking at was not text-event, so we handle it like
                            // any other events.
                            match event {
                                egui::Event::Key { key, pressed, repeat,
                                        modifiers: Modifiers { alt, ctrl, shift, mac_cmd, command }, physical_key: _ } => {

                                    if !emu_paused {
                                        if let Some(akey) = egui_event_to_apple_key(&event) {
                                            if !repeat {
                                                //println!("egui:event {} pressed? {} t={}", akey, pressed, timebase);
                                                let te= TimedEvent {
                                                    event: event.clone(),
                                                    time: timebase,
                                                    key: akey,
                                                    pressed: KeyState::Pressed
                                                };

                                                let k = key.name().to_string();
                                                if *pressed {
                                                    self.keyboard.insert(k, te);
                                                } else if !*pressed {
                                                    // FIXME What happens if we receive a key released before having
                                                    // processed the key pressed ? For that we would need a special
                                                    // state "pressed_and_released_quickly".

                                                    //self.keyboard.insert(k, te);
                                                    let k = key.name().to_string();
                                                    if let Some(v) = self.keyboard.get_mut(&k) {
                                                        v.pressed = KeyState::Released;
                                                        v.time = timebase;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if !*pressed || *repeat {
                                        // println!("key event: {} {} pressed:{}", key.name(), key.symbol_or_name(), pressed);
                                        self.debugger.handle_keyboard(key);
                                    }
                                },
                                _ => ()
                            }
                        }
                        //self.control_channel.send(Command::KeyDown(()) .handle_keyboard(input_state)
                    }
                });

                // Run over the recorded keys to check if we have to sort out egui "text"
                // events from regular "key" events.

                let mut keys_to_kill: Vec<String> = Vec::new();

                // Recheck the whole keyboard to see if there are pending actions to
                // do.
                for (key, event) in self.keyboard.iter_mut() {

                    // When we have a text event. It always occurs right after a key event.
                    // So here I make sure that if I have a
                    if event.pressed == KeyState::Pressed  {
                        //println!("main(): at {}ms {}c key: {} {}", event.time, thread_local_cycle, event.key, event.pressed);
                        // To avoid sending key press non stop, we do this:
                        event.pressed = KeyState::PressedProcessed;
                        self.control_channel.send( Command::KeyDown(event.key, Some(thread_local_cycle))).expect("Send should work");
                    } else if event.pressed == KeyState::Released {
                        //println!("main(): at {}ms {}c key: {} {}", event.time, thread_local_cycle, event.key, event.pressed);
                        self.control_channel.send( Command::KeyUp(event.key, Some(thread_local_cycle))).expect("Send should work");

                        // When a key is released, we don't need to track it anymore.
                        // Keep in mind we're in the UI thread. The handling of key releases
                        // is different in the emulator.
                        keys_to_kill.push(key.to_string());
                    }
                }

                for k in keys_to_kill {
                    self.keyboard.remove(&k);
                }
            });

            self.old_time_base = Instant::now();

    // }); // Scroll area
    }
}


fn start_sound_thread(sound_config: SoundConfig, output_queues: &Arc<Mutex<OutputQueues>>, control_channel_rx: Receiver<bool> ) {
    let oqclone = output_queues.clone();

    std::thread::spawn(move || {
        //std::thread::sleep(Duration::from_secs(10));

        let stream_in = start_cpal_audio_stream(
            &sound_config,
            oqclone);

        stream_in.play().expect("Could not play audio stream");


        // recv blocks until a message is received.
        while let Ok(_command) = control_channel_rx.recv() {
            break
        }
    }); // thread

    // Make sure the sound thread starts polling
    // before starting the emulator else the emulator
    // can produce many samples (about 0.25 seconds of them)
    // before the sound start, resulting in a delay.
    // FIXME This is cheap, one should syn with some messages.
    std::thread::sleep(std::time::Duration::from_millis(500));
}

use clap::{Parser, ValueEnum, crate_authors};

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Debug)]
enum Cpus {
    Floooh6502,
    Obscoz6502,
    Obscoz65c02
}

#[derive(Parser, Debug)]
#[command(name = "AccurApple Emulator")]
#[command(version = env!("CARGO_PKG_VERSION"))]
#[command(author = crate_authors!(" "))]
#[command(after_help = "This program is licensed under the GNU Affero GPL v3")]
#[command(about = "An Apple 2e Revision B emulator", long_about = None)]
struct CliArgs {
    /// Path to floppy disk file (.DSK, .WOZ)
    #[arg(short, long)]
    floppy1: Option<PathBuf>,

    /// Which CPU emulation to use. Not that Obscoz engines have some (rare) issues.
    #[arg(short, long, value_enum, default_value_t=Cpus::Floooh6502)]
    cpu: Cpus,

    /// Accurapple will run without any sound
    #[arg(short, long)]
    no_sound: bool,

    /// ROM file path
    #[arg(short, long)]
    rom: Option<PathBuf>,

    /// ROM charset file path
    #[arg(short, long)]
    charset: Option<PathBuf>,

    /// Apple2Composite shader path
    #[arg(short, long)]
    a2c_shader: Option<PathBuf>,

    /// Enable mode debugging tools
    #[arg(short, long)]
    power_debug: bool,

    /// Headless mode (emulator running but no display, no sound)
    #[arg(short, long)]
    headless: bool,

    /// Log sent to the terminal/console instead of the user interface of Accurapple.
    #[arg(short, long)]
    log_stdout: bool,

    /// Comma separted list of commands
    #[arg(short, long)]
    script: Option<String>,

    /// Symbols file path
    #[arg(short, long)]
    symbols: Option<PathBuf>,

    /// Don't display the greetings/copyright message.
    #[arg(short, long)]
    no_greetings: bool,

    /// Log Disk][ stepper activations
    #[arg(short, long)]
    log_stepper: bool,
}

fn new_runner2(cli: &CliArgs) {
    let mut configuration = Configuration::load();

   let sound_config = if !cli.no_sound {
		find_configuration()
    } else {
		None
    };

	let (output_queues, control_channel,
        debug_stream_rx,
        quit_rx, emu_thread) =
		start_emulator(cli,
			if let Some(sc) = sound_config.as_ref() {
			sc.sample_rate()
			} else {
				44100
			},
            cli.headless,
        &mut configuration);

    let (_sound_thread_control_tx, sound_thread_control_nnrx): (Sender<bool>, Receiver<bool>) = channel();

    if sound_config.is_some() {
        start_sound_thread(sound_config.unwrap(), &output_queues, sound_thread_control_nnrx);
    }

    if !cli.headless {


        let log_lines:Arc<Mutex<CircularQueue<MyLogRecord>>> = Arc::new(Mutex::new(CircularQueue::with_capacity(1000)));

        if !cli.log_stdout {
            log::set_boxed_logger(
                Box::new(AccurappleSimpleLogger{ records : log_lines.clone()}))
                .map(|()| log::set_max_level(LevelFilter::Info)).expect("Can't set log 'cos there's already one");
        } else {
            println!("Logging to console");
        }


        let main_ui = Debugger::new(
            control_channel.clone(), debug_stream_rx,
            log_lines.clone(),
            cli.symbols.clone(),
            configuration,
            cli.power_debug);

        let icon_data = IconData {
            rgba: make_logo(),
            width: 32,
            height: 32
        };

        let native_options = eframe::NativeOptions {
            viewport: egui::ViewportBuilder::default()
                .with_inner_size([1280.0, 1024.0])
                .with_drag_and_drop(true)
                .with_icon(icon_data),
            renderer: eframe::Renderer::Wgpu,
            run_and_return: false,
            ..Default::default()
        };

        // Send commands to emulator engine
        let cc_clone = control_channel.clone();

        let charset = cli.charset.clone();
        let shader = cli.a2c_shader.clone();
        eframe::run_native("Accurapple", native_options,
            Box::new(|cc| {
                Ok(Box::new(
                    AccurappleApp::new(cc,
                        output_queues,
                        main_ui,
                        cc_clone,
                        quit_rx,
                        charset,
                        shader)))
                    })
                ).expect("should run");
    } else {
        info!("Running in headless mode");
        while !emu_thread.is_finished() {};
    }
}

//use disasm6502;

fn main() {

    // let code: Vec<u8> = vec![0xA0, 0x00, 0xAE, 0x00, 0x00, 0xF0, 0x10, 0xB1, 0x02, 0x91, 0x03,
    //                          0xC8, 0xD0, 0xF9, 0xEE, 0x02, 0x00, 0xEE, 0x03, 0x00, 0xCA, 0xD0,
    //                          0xF0, 0xAE, 0x01, 0x00, 0xF0, 0x08, 0xB1, 0x02, 0x91, 0x03, 0xC8,
    //                          0xCA, 0xD0, 0xF8, 0x60];

    // let instructions = disasm6502::from_array(&code).unwrap();


    // // ...and print!
    // for i in instructions.iter() {
    //     println!("{}", i.address);
    // }

    // return;

    let cli = CliArgs::parse();

    // if  false && !cli.is_present("debug") {
	if cli.log_stdout {
	//println!("Logging on");
	let log_builder = &mut env_logger::builder();
	// log_builder = log_builder
	//     .format_timestamp(None)
	// 	.filter_level(LevelFilter::Debug)
	// 	.filter_module("naga", LevelFilter::Error)
	// 	.filter_module("gilrs", LevelFilter::Error)
	// 	.filter_module("wgpu_core", LevelFilter::Error)
	// 	.filter_module("wgpu_hal", LevelFilter::Error)
	// 	.filter_module("accurapple::sound", LevelFilter::Error)
	// 	.filter_module("accurapple::speaker", LevelFilter::Error)
	// 	.filter_module("accurapple::diskii", LevelFilter::Error)
	// 	.filter_module("accurapple::diskii_stepper", LevelFilter::Error)
	// 	.filter_module("accurapple::wozpack", LevelFilter::Error)
	// 	.filter_module("accurapple::a2", LevelFilter::Error)
	// 	.filter_module("accurapple::debug2", LevelFilter::Info);

	// // These lines are my attempt to shutdown the logging
	// // It doesn't work. See :
	// // https://github.com/env-logger-rs/env_logger/issues/228
	// let mut sink = Box::new(sink());
	// if cli.is_present("debug") {
	//     log_builder.filter(None, LevelFilter::Error);
	//     log_builder.target(env_logger::Target::Pipe(sink));
	// } else {
	//     log_builder.target(env_logger::Target::Stdout);
	// }
	log_builder.init();
    }

    if !cli.no_greetings {
        println!();
        println!("▢ AccurApple {}, {}", env!("CARGO_PKG_VERSION"), EMULATOR_SHORT_DESCRIPTION);
        println!();
        println!("Copyright 2022-24 Stéphane Champailler");
        println!("Unless specified otherwise, code is licensed under the General Affero Public");
        println!("License (AGPL) version 3 of which you can find a copy there:");
        println!("https://www.gnu.org/licenses/agpl-3.0.en.html");
        println!();
    }

    new_runner2(&cli);
}
