extern crate log;
use std::fs::File;
use std::io::Write;
use log::debug;

const RESOLUTION: isize = 360;
const MAGNET_ANGLE: isize = RESOLUTION / 4;
const QUARTER_ANGLE: isize = RESOLUTION / 8;
const MAGNET_ANGLES: [isize; 4] = [
    MAGNET_ANGLE * 0,
    MAGNET_ANGLE,
    MAGNET_ANGLE * 2,
    MAGNET_ANGLE * 3,
];
const RELAX: f64 = 5.0;
const ANGLE_EPSILON: f64 =0.5;

// According to apple2_IWM_INFO_19840510.pdf
// Approx 5ms for half track
// 2000: works on many titles
// In RWTS, the min delay for a quarter track is $1C*100 cycles = 2900 ms
const DEGREES_PER_USEC: f64 = (MAGNET_ANGLE as f64) / 2900.0;

struct LogEntry {
    time: u64,
    last_cycle: u64,
    angle: f64,
    magnets: [bool; 4],
    subtrack: f64,
    destination_angle: f64
}

pub struct DiskIIStepper {
    magnet_states: [bool; 4],
    // Angle from 0-90 is the first track, 90-180: 2nd,... up to 90*40 degrees
    angle: f64,
    destination_angle: f64,
    current_subtrack: Option<f64>,
    last_cycle: u64,
    log: Option<Vec<LogEntry>>
}

impl DiskIIStepper {
    pub fn new() -> DiskIIStepper {
        DiskIIStepper {
            magnet_states: [false, false, false, false],
            angle: 0.0,
            destination_angle: 0.0,
            current_subtrack: Some(0.0),
            last_cycle: 0,
            log: None
        }
    }

    fn mag_str(&self, n: usize) -> String {
        if self.magnet_states[n] {
            n.to_string()
        } else {
            "-".to_string()
        }
    }

    pub fn magnets_to_string(&self) -> String {
        format!(
            "{}{}{}{}",
            self.mag_str(0),
            self.mag_str(1),
            self.mag_str(2),
            self.mag_str(3)
        )
    }

    pub fn get_current_subtrack(&self) -> Option<usize> {
        self.current_subtrack.map(|t| (t * 4.0) as usize)
    }

    pub fn set_current_subtrack(&mut self, track: f64) {
        // Used for testing purposes.
        self.current_subtrack = Some(track);
        self.angle = track * ((RESOLUTION / 2) as f64);
    }

    pub fn set_magnet(&mut self, magnet_ndx: usize, charge: bool, cycle: u64) -> (Option<usize>, bool)  {
        // Return: number of cycles until the rotor has moved to its new
        // position (if it can actually move), provided the magnets state
        // don't change.

        // Even if nothing changes on the magnet, we process
        // the information in order to recompute the number of cycles
        // correctly (to simplify the caller's task).

        self.magnet_states[magnet_ndx] = charge;
        self.move_rotor(cycle)
    }

    fn magnets_all_off(&self) -> bool {
        let s = &self.magnet_states;
        !s[0] && !s[1] && !s[2] && !s[3]
    }

    pub fn start_logging(&mut self) {
        self.log = Some(Vec::new());
    }

    pub fn stop_logging(&mut self, path: &str) {
        let mut output = File::create(path).unwrap();

        for v in self.log.as_ref().unwrap() {
            let f = |m: usize| if v.magnets[m] {m+1} else {0};
            let _ = writeln!(output, "{},{:.3},{},{},{},{},{},{},{:.3}", v.time, v.angle,
                f(0), f(1), f(2), f(3), v.time - v.last_cycle, v.subtrack, v.destination_angle);
        }
        self.log = None;
    }

    pub fn move_rotor(&mut self, current_cycle: u64) -> (Option<usize>, bool) {
        // handle the ongoing movement of the read head.
        // Every computations is invariant here as long as the
        // magnet states do not change.

        // Return the number of cycles to run before reaching the
        // destination:
        //   None: the rotor has not moved between the last call and
        //         this call.
        //   0: the rotor has moved up to its destination between the
        //      last call and this call.
        //   > 0: the rotor has moved but didn't reach its destination
        //        between the last call and this call.

        if self.magnets_all_off() && self.angle == self.destination_angle {
            // We're already arrived, nothing to do.
            // This happens when one resets the floppy disk and there's
            // still an "irq" pending.
            return (None, false)
        }

        // First, finish the ongoing movement, as it was planned.
        let degrees_ran = (current_cycle - self.last_cycle) as f64 * DEGREES_PER_USEC;
        let distance_to_destination = (self.angle - self.destination_angle).abs();
        // The min is here to avoid that we go further
        // than the destination if, for some reason (numerical error while
        // rounding to cycles for example), the number
        // of elapsed cycles is too big.
        let degrees_to_go = distance_to_destination.min(degrees_ran).copysign(self.destination_angle - self.angle);
        debug!(
            "move_rotor(): Current angle={:.3}° dest angle: {:.3}°, ° to go {:.3} mags:{}",
            self.angle, self.destination_angle, degrees_to_go,
            self.magnets_to_string()
        );

        self.angle += degrees_to_go;

        // Now plan the next movement, redoing the plan.

        // The ultimate destination, without counting quarter tracks
        // Starting from angle.
        self.destination_angle = self.compute_destination_angle2().min(36.0*180.0);
        self.last_cycle = current_cycle;

        if self.log.is_some() {
            let log = self.log.as_mut().unwrap();
            log.push(LogEntry{
                time: current_cycle,
                last_cycle: self.last_cycle,
                angle: self.angle,
                magnets: self.magnet_states,
                subtrack: if let Some(st) = self.current_subtrack {st} else {-1.0},
                destination_angle: self.destination_angle
            });
        }

        if (self.angle - self.destination_angle).abs() < ANGLE_EPSILON {
            // < 1.0 degrees (IOW very close)
            debug!("move_rotor(): already at destination");
            self.angle = self.destination_angle;
            (None, self.angle_to_track(self.angle))
        } else {
            // let cycles_to_reach_destination =
            //     ((self.next_angle - self.angle).abs() / DEGREES_PER_USEC).round() as usize;

            let cycles_to_reach_destination = 100;
            (Some(cycles_to_reach_destination), self.angle_to_track(self.angle))
        }

        // debug!(
        //     "move_rotor(): Moved to angle={} (trk:{}). Mags:{}. Next: {}° (trk:{}) in {} cycles.",
        //     self.angle,
        //     if let Some(x) = self.current_subtrack {
        //         format!("{}", x)
        //     } else {
        //         "between".to_string()
        //     },
        //     self.magnets_to_string(),
        //     self.next_angle,
        //     self.angle_to_track(self.next_angle).unwrap(),
        //     cycles_to_reach_destination
        // );

        // Some(cycles_to_reach_destination)
    }

    fn angle_to_track(&mut self, angle: f64) -> bool {
        // Return true if there's a track change ongoing

        debug!("angle_to_track(): angle={:.3}", angle);

        // Either we're on the track, in which case we give its number
        // possibly a half or quarter track.
        // Either we're not on a track (nor a quarter, nor a half) and
        // in that case we say None.

        // Note the rounding
        let closest_quarter_angle: isize =
            ((angle / (QUARTER_ANGLE as f64)).round() as isize) * QUARTER_ANGLE;

        // If the distance between the angle and the closest quarter angle is too big
        //let between_tracks = ((closest_quarter_angle as f64) - angle).abs() > RELAX;
        let between_tracks = false;

        let new_track = if !between_tracks {
            Some(
                (closest_quarter_angle / (2 * MAGNET_ANGLE)) as f64
                    + (((closest_quarter_angle % (2 * MAGNET_ANGLE)) / QUARTER_ANGLE) as f64)
                        * 0.25,
            )
        } else {
            None
        };

        let r = if new_track.is_some() && self.current_subtrack.is_some() {
            let track_change = new_track.unwrap() != self.current_subtrack.unwrap();
            debug!("new:{} - old:{} => change? {}", new_track.unwrap(), self.current_subtrack.unwrap(), track_change);
            track_change
        } else { !(new_track.is_none() && self.current_subtrack.is_none()) };

        if r {
            self.current_subtrack = new_track
        }

        r

    }

    fn compute_destination_angle(&self) -> isize {
        const ROLL_MAGNET_ANGLES: [isize; 5] = [
            MAGNET_ANGLE * 0,
            MAGNET_ANGLE,
            MAGNET_ANGLE * 2,
            MAGNET_ANGLE * 3,
            MAGNET_ANGLE * 4,
        ];

        let rounded_angle: isize = self.angle.round() as isize;
        let mod_angle: isize = rounded_angle % 360;

        let dest = if rounded_angle % 90 == 0 {
            let tm_center = (mod_angle / MAGNET_ANGLE) as usize;
            // rm_euclid is the modulo (% is the remainder, this changes a lot
            // of things when working with negative numbers) to avoid substract
            // with overflow
            let tm_right = (tm_center as isize - 1).rem_euclid(4) as usize;
            let tm_left = (tm_center + 1) % 4;

            //debug!("full angle: left:{} center:{} right:{}",tm_left,tm_center,tm_right);

            if self.magnet_states[tm_center] && self.magnet_states[tm_right] {
                (rounded_angle - 45).max(0)
            } else if self.magnet_states[tm_center] && self.magnet_states[tm_left] {
                rounded_angle + 45
            } else if self.magnet_states[tm_center] {
                rounded_angle
            } else if self.magnet_states[tm_left] {
                rounded_angle + 90
            } else if self.magnet_states[tm_right] {
                (rounded_angle - 90).max(0)
            } else {
                rounded_angle
            }
        } else {
            let tm_right = (mod_angle / MAGNET_ANGLE) as usize;
            let tm_left = (tm_right + 1) % 4;
            let base = (rounded_angle / 360) * 360;

            //debug!("common angle: left:{} base:{} right:{}",tm_left,base,tm_right);

            if self.magnet_states[tm_left] && self.magnet_states[tm_right] {
                debug!("magnets left and right to current angle are ON");

                let r = base
                    + crate::modulo::middle(
                        ROLL_MAGNET_ANGLES[tm_right + 1],
                        MAGNET_ANGLES[tm_right],
                    );

                // We won't go below zero degrees.
                r.max(0)
            } else if self.magnet_states[tm_left] {
                base + ROLL_MAGNET_ANGLES[tm_right + 1]
            } else if self.magnet_states[tm_right] {
                base + MAGNET_ANGLES[tm_right]
            } else {
                // No magnet active => the rotor stays in place.
                debug!("no magnet active");
                rounded_angle
            }
        };

        // One full revolution == 2 tracks
        dest.clamp(0, 40 * RESOLUTION / 2)
    }

    fn compute_destination_angle2(&self) -> f64 {
        let closest90 = (self.angle / 90.0).round()*90.0;

        let dest = if (closest90 - self.angle).abs() < ANGLE_EPSILON {
            let quarter = (closest90 / 90.0).round() as i32;
            let q_before = quarter - 1;
            let q_after= quarter + 1;

            debug!("On 360 angle: quarters: before:{} [{}] on:{} [{}] after:{}[{}]",
                q_before, q_before.rem_euclid(4),
                quarter, quarter.rem_euclid(4),
                q_after, q_after.rem_euclid(4));
            // Q_before and q_after are on both sides of the angle.
            // Let's see the magnet

            let mag_before = self.magnet_states[q_before.rem_euclid(4) as usize];
            let mag_on = self.magnet_states[quarter.rem_euclid(4) as usize];
            let mag_after = self.magnet_states[q_after.rem_euclid(4) as usize];

            if mag_before && !mag_on {
                Some(q_before.max(0) as f64 * 90.0)
            } else if mag_after && !mag_on {
                Some(q_after as f64 * 90.0)
            } else if mag_before && mag_on {
                Some( (q_before.max(0) + quarter) as f64 / 2.0 * 90.0)
            } else if mag_after && mag_on {
                Some( (quarter + q_after) as f64 / 2.0 * 90.0)
            } else {
                // Stay in place
                None
            }
        } else {
            let q_before = (self.angle / 90.0).floor() as usize;
            let q_after = (self.angle / 90.0).ceil() as usize;

            let mag_before = self.magnet_states[q_before.rem_euclid(4)];
            let mag_after = self.magnet_states[q_after.rem_euclid(4)];

            if mag_before && mag_after {
                Some( (q_before + q_after) as f64 / 2.0 * 90.0)
            } else if mag_before {
                Some( q_before as f64 * 90.0)
            } else if mag_after {
                Some( q_after as f64 * 90.0)
            } else {
                None
            }
        };

        if let Some(d) = dest {
            debug!("compute_dest(): Angle: {:.3}, Dest {}, Track:{:.2}, mags:{}",
                self.angle, d, self.angle/180.0, self.magnets_to_string());
            assert!((self.angle-d).abs() < 180.0);
            d
        } else {
            self.angle
        }
        /*
        if let Some(d) = dest {
            let mt = (35 as f64)*360.0/2.0;
            if d > mt - 1.0 {
                println!("Track over");
                return mt;
            } else {
                println!("{:.3} -> {:.3}",self.angle, d);
                return d;
            }
        } else {
            println!("{:.3} doesn't move",self.angle);
            return self.angle;
        }
        */
    }

    fn compute_next_position2(&self) -> f64 {
        let dest = self.compute_destination_angle() as f64;
        if (dest-self.angle).abs() > 1.0 {
            // Destination not reached yet

            if (dest - self.angle).abs() < 45.0 {
                // Close to destination, no quarter track in between.
                dest
            } else {
                // There's at least one quarter between us and the destination
                // So that will be the intermediate target
                let direction = if dest > self.angle {
                    1
                } else if dest < self.angle {
                    -1
                } else {
                    0
                };



                if direction > 0 {
                    ((self.angle / (QUARTER_ANGLE as f64)).floor() + 1.0) * (QUARTER_ANGLE as f64)
                } else if direction < 0 {
                    ((self.angle / (QUARTER_ANGLE as f64)).ceil() - 1.0) * (QUARTER_ANGLE as f64)
                } else {
                    dest
                }
            }
        } else {
            // dest ~= rounded_angle
            // We're already there
            dest
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn test_compute_next_position3() {
        init();
        let mut ds = DiskIIStepper::new();

        // Make successive movements

        ds.angle = 10.0;
        ds.magnet_states = [false, false, false, false];
        assert!(ds.compute_next_position2() == 10.0);

        ds.angle = 10.0;
        ds.magnet_states = [true, true, false, false];
        assert!(ds.compute_next_position2() == 45.0);

        ds.angle = 45.0;
        ds.magnet_states = [false, true, false, false];
        assert!(ds.compute_next_position2() == 90.0);

        ds.angle = 90.0;
        ds.magnet_states = [false, true, true, false];
        assert!(ds.compute_next_position2() == 90.0 + 45.0);

        ds.angle = 90.0 + 45.0;
        ds.magnet_states = [false, false, true, false];
        assert!(ds.compute_next_position2() == 90.0 + 90.0);

        // Successive movements and crossing 0

        ds.angle = 271.0;
        ds.magnet_states = [true, false, false, true];
        assert!(ds.compute_next_position2() == 270.0 + 45.0);

        ds.angle = 270.0 + 45.0;
        ds.magnet_states = [true, false, false, false];
        assert!(ds.compute_next_position2() == 270.0 + 90.0);

        ds.angle = 270.0 + 90.0;
        ds.magnet_states = [true, true, false, false];
        assert!(ds.compute_next_position2() == 270.0 + 90.0 + 45.0);

        // In the other direction

        ds.angle = 360.0 + 60.0;
        ds.magnet_states = [true, true, false, false];
        assert!(ds.compute_next_position2() == 360.0 + 45.0);

        ds.angle = 360.0 + 45.0;
        ds.magnet_states = [true, false, false, false];
        assert!(ds.compute_next_position2() == 360.0);

        ds.angle = 360.0;
        ds.magnet_states = [true, false, false, true];
        assert!(ds.compute_next_position2() == 360.0 - 45.0);

        ds.angle = 360.0 - 45.0;
        ds.magnet_states = [false, false, false, true];
        assert!(ds.compute_next_position2() == 270.0);
    }

    #[test]
    fn test_compute_next_position2() {
        init();
        let mut ds = DiskIIStepper::new();

        ds.angle = 10.0;
        ds.magnet_states = [false, false, false, false];
        assert!(ds.compute_next_position2() == 10.0);

        ds.angle = 10.0;
        ds.magnet_states = [false, true, false, false];
        assert!(
            ds.compute_next_position2() == 45.0,
            "{} seems wrong",
            ds.compute_next_position2()
        );

        ds.angle = 60.0;
        ds.magnet_states = [false, true, false, false];
        assert!(ds.compute_next_position2() == 90.0);

        ds.angle = 45.0;
        ds.magnet_states = [false, true, false, false];
        assert!(ds.compute_next_position2() == 90.0);

        ds.angle = 45.0;
        ds.magnet_states = [true, false, false, false];
        assert!(ds.compute_next_position2() == 0.0);

        ds.angle = 45.0;
        ds.magnet_states = [true, true, false, false];
        assert!(ds.compute_next_position2() == 45.0);

        ds.angle = 60.0;
        ds.magnet_states = [true, false, false, false];
        assert!(
            ds.compute_next_position2() == 45.0,
            "{} seems wrong",
            ds.compute_next_position2()
        );
    }

    #[test]
    fn test_compute_destination_angle() {
        init();
        let mut ds = DiskIIStepper::new();
        ds.angle = 10.0;
        ds.magnet_states = [false, false, false, false];
        assert!(ds.compute_destination_angle() == 10);
        ds.angle = 10.0;
        ds.magnet_states = [true, false, false, false];
        assert!(ds.compute_destination_angle() == 0);
        ds.angle = 310.0;
        ds.magnet_states = [true, false, false, false];
        assert!(ds.compute_destination_angle() == 360);
        ds.angle = 310.0;
        ds.magnet_states = [false, false, false, true];
        assert!(ds.compute_destination_angle() == 270);
        ds.angle = 310.0;
        ds.magnet_states = [true, false, false, true];
        assert!(ds.compute_destination_angle() == 270 + 45);

        ds.angle = 360.0;
        ds.magnet_states = [true, false, false, true];
        assert!(
            ds.compute_destination_angle() == 270 + 45,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 0.0;
        ds.magnet_states = [true, false, false, true];
        assert!(
            ds.compute_destination_angle() == 0,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        // Around 360

        ds.angle = 360.0 + 90.0;
        ds.magnet_states = [true, false, false, false];
        assert!(
            ds.compute_destination_angle() == 360,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 360.0 + 90.0;
        ds.magnet_states = [false, true, false, false];
        assert!(
            ds.compute_destination_angle() == 360 + 90,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 360.0 + 90.0;
        ds.magnet_states = [false, false, true, false];
        assert!(
            ds.compute_destination_angle() == 360 + 180,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        // Around 90

        ds.angle = 0.0 + 90.0;
        ds.magnet_states = [true, false, false, false];
        assert!(
            ds.compute_destination_angle() == 0,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 0.0 + 90.0;
        ds.magnet_states = [false, true, false, false];
        assert!(
            ds.compute_destination_angle() == 90,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 0.0 + 90.0;
        ds.magnet_states = [false, false, true, false];
        assert!(
            ds.compute_destination_angle() == 180,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 0.0 + 90.0;
        ds.magnet_states = [false, false, false, true];
        assert!(
            ds.compute_destination_angle() == 90,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        // Around 00

        ds.angle = 0.0;
        ds.magnet_states = [true, false, false, false];
        assert!(
            ds.compute_destination_angle() == 0,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 0.0;
        ds.magnet_states = [false, true, false, false];
        assert!(
            ds.compute_destination_angle() == 90,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 0.0;
        ds.magnet_states = [false, false, true, false];
        assert!(
            ds.compute_destination_angle() == 0,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 0.0;
        ds.magnet_states = [false, false, false, true];
        assert!(
            ds.compute_destination_angle() == 0,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        // Around 60

        ds.angle = 60.0;
        ds.magnet_states = [true, false, false, false];
        assert!(
            ds.compute_destination_angle() == 0,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 60.0;
        ds.magnet_states = [true, true, false, false];
        assert!(
            ds.compute_destination_angle() == 45,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 60.0;
        ds.magnet_states = [false, true, false, false];
        assert!(
            ds.compute_destination_angle() == 90,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 60.0;
        ds.magnet_states = [false, true, true, false];
        assert!(
            ds.compute_destination_angle() == 90,
            "{} seems wrong",
            ds.compute_destination_angle()
        );

        ds.angle = 60.0;
        ds.magnet_states = [false, false, true, false];
        assert!(
            ds.compute_destination_angle() == 60,
            "{} seems wrong",
            ds.compute_destination_angle()
        );
    }

    #[test]
    fn test_angle_to_track() {
        init();
        let _ds = DiskIIStepper::new();
        // assert!(ds.angle_to_track(0.0) == Some(0.0));
        // assert!(ds.angle_to_track(45.0) == Some(0.25));
        // assert!(ds.angle_to_track(90.0) == Some(0.5));
        // assert!(ds.angle_to_track(135.0) == Some(0.75));

        // assert!(
        //     ds.angle_to_track(720.0 + 0.0) == Some(4.0),
        //     "uexpected {}",
        //     ds.angle_to_track(720.0).unwrap()
        // );
        // assert!(ds.angle_to_track(720.0 + 45.0) == Some(4.25));
        // assert!(ds.angle_to_track(720.0 + 90.0) == Some(4.5));
        // assert!(ds.angle_to_track(720.0 + 135.0) == Some(4.75));
    }

    #[test]
    fn test_compute_destination_angle2() {
        init();

        let mut ds = DiskIIStepper::new();

        let mut tester = |angle: f64, magnets: [bool; 4], dest_angle:f64| {
            ds.angle = angle;
            ds.magnet_states = magnets;
            let v = ds.compute_destination_angle2();
            assert!( v == dest_angle,
                "{:.3}, {} {} {} {}, expected {}, got {:.3}", angle, magnets[0],magnets[1],magnets[2],magnets[3],
                dest_angle, v);
        };

        tester(10.0, [false, false, false, false], 10.0);
        tester(10.0, [false, false, false, true], 10.0);
        tester(10.0, [false, false, true, false], 10.0);
        tester(10.0, [false, true, false, false], 90.0);
        tester(10.0, [true, false, false, false], 0.0);

        tester(360.0, [false, false, false, false], 360.0);
        tester(360.0, [false, false, false, true], 360.0-90.0);
        tester(360.0, [false, false, true, false], 360.0);
        tester(360.0, [false, true, false, false], 360.0+90.0);
        tester(360.0, [true, false, false, false], 360.0);

        tester(370.0, [false, false, false, false], 370.0);
        tester(370.0, [false, false, false, true], 370.0);
        tester(370.0, [false, false, true, false], 370.0);
        tester(370.0, [false, true, false, false], 360.0+90.0);
        tester(370.0, [true, false, false, false], 360.0);

    }
}
