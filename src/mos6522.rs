// BUG If I change the latch then the next IRQ gets recomputed. That's not
// correct, the next IRQ should not change because the counter has already started
// countdown and will IRQ when it reaches 0, independently of the value of the latch.

// https://github.com/rustzx/rustzx/blob/master/rustzx-core/src/zx/sound/ay.rs

use log::{debug, info, trace, warn};
use std::collections::VecDeque;
use std::sync::{Arc, Mutex};

use aym::{AyMode, AymBackend, AymPrecise, SoundChip};

use crate::a2::{ClockEvent, ClockEventData, CYCLES_PER_FRAME, FRAMES_PER_SECOND};

use crate::generic_peripheral::{GenericPeripheral, IRQAction, IRQRequest, IrqChange, IrqLineLevel};

use crate::sound::{cycle_to_sample, SamplesFrame, SoundRenderer};
//const AY_FREQ: usize = 1_773_400;
const AY_FREQ: usize = 1_000_000;

// https://www.apple2.org.za/gswv/a2zine/Docs/Mockingboard_MiniManual.html

const MOS6522_ORB: usize = 0; // Output Reigster A - Also called PCR in Grouik's code
const MOS6522_ORA: usize = 1; // Output Register B
const MOS6522_DDRB: usize = 2;
const MOS6522_DDRA: usize = 3;
const MOS6522_T1CL: usize = 4;
const MOS6522_T1CH: usize = 5;
const MOS6522_T1LL: usize = 6;
const MOS6522_T1LH: usize = 7;

const MOS6522_T2CL: usize = 8;
const MOS6522_T2CH: usize = 9;

const MOS6522_ACR: usize = 0xB;
const MOS6522_IFR: usize = 0xD;
const MOS6522_IER: usize = 0xE;

// Bit flag for Timer1 in IER (WDC doc table 2-12)
const MOS6522_IER_TIMER1: u8 = 0b0100_0000;
const MOS6522_IER_TIMER2: u8 = 0b0010_0000;

/// T1 time out; bit are numbered starting at zero !
const MOS6522_IFR6: u8 = 0b1011_1111;
/// T2 time out; bit are numbered starting at zero !
const MOS6522_IFR5: u8 = 0b1101_1111;

const MB_RESET: u8 = 0x0;
const MB_INACTIVE: u8 = 0x04;
const MB_WRITE_DATA: u8 = 0x06;
const MB_SET_REGISTER: u8 = 0x07;

struct AYRenderer {
    // We have our own clock
    cycle: u64,
    ay1: AymPrecise,
    pub sound: Arc<Mutex<VecDeque<i16>>>, // A ring buffer
    events: VecDeque<ClockEvent>,
    sample_rate: usize,
    // To spare a lot of cycle on the many applications that
    // do *not* use the MockingBoard, we avoid any work
    // up until someone asks the MB to do something.
    mb_has_started: bool,
    base_addr: usize // for logging
}

impl AYRenderer {
    fn new(sample_rate: usize, base_addr: usize) -> AYRenderer {
        AYRenderer {
            cycle: 0,
            ay1: AymPrecise::new(SoundChip::AY, AyMode::Mono, AY_FREQ, sample_rate),
            sound: Arc::new(Mutex::new(VecDeque::new())),
            events: VecDeque::new(),
            sample_rate,
            mb_has_started: false,
            base_addr
        }
    }
}

impl SoundRenderer for AYRenderer {
    fn record(&mut self, event: ClockEvent) {
        if !self.mb_has_started {
            info!("AY-3-8910 rendering has started");
            self.mb_has_started = true;
        }
        self.events.push_front(event)
    }

    fn process_frame(&mut self) -> SamplesFrame {
        //let mut data = self.sound.lock().unwrap();
        let mut data: Vec<i16> = Vec::new();

        // FIXME This computation is dangereous !
        // The remainder of this division must be 0 !
        let spf = self.sample_rate / FRAMES_PER_SECOND;
        let mut current_cycle = self.cycle;
        let last_frame_cycle = self.cycle + (CYCLES_PER_FRAME as u64);

        // FIXME i16::MAX doesnt seem to work... Should investigate limits
        const YAM_TO_I16: f64 = 16000.0;

        if self.mb_has_started {
            debug!("AYRenderer.process_frame: base:${:X}, {} events", self.base_addr, self.events.len());
            data.reserve(spf);

            while !self.events.is_empty() {
                let next_event = self.events.back().unwrap();

                if next_event.tick <= last_frame_cycle {
                    assert!(next_event.tick >= current_cycle, "MockingBoard sound gen failure: {} >= {} ???", next_event.tick, current_cycle);
                    let next_sample = cycle_to_sample(next_event.tick - current_cycle, spf);
                    for _i in 0..next_sample {
                        // FIXME We're only using the left channel !
                        data.push((self.ay1.next_sample().left * YAM_TO_I16) as i16);
                    }

                    match next_event.event {
                        ClockEventData::MockingBoardWrite { register, data } => {
                            self.ay1.write_register(register, data);
                        }
                        ClockEventData::MockingBoardReset => {
                            for register in 00..=0xf {
                                self.ay1.write_register(register, 0);
                            }
                        }
                        _ => panic!("mixing event types"),
                    }

                    current_cycle = next_event.tick;
                    self.events.pop_back();
                } else {
                    warn!("Early out");
                    break;
                }
            } // while on events

            // FIXME: The 'if' statement should be this:
            // if current_cycle < self.cycle + (CYCLES_PER_FRAME as u64) {
            // However, there's some rounding error which I don't
            // have time to investigate. So I replace it byf something
            // safer:

            if data.len() < spf {
                for _i in data.len()..spf {
                    data.push((self.ay1.next_sample().left * YAM_TO_I16) as i16);
                }
            }
        }

        self.cycle += CYCLES_PER_FRAME as u64;

        SamplesFrame::new(data, Vec::new())
    }

    fn samples(&self) -> Arc<Mutex<VecDeque<i16>>> {
        Arc::clone(&self.sound)
    }
}

pub struct MOS6522 {
    base_addr: usize, // $00 or $80
    ddra: u8,
    ddrb: u8,
    ora: u8,
    orb: u8,
    ira: u8,
    irb: u8,
    t1cl: u8,
    t1ch: u8,
    t1ll: u8, // Use t1_latch() to get the 16 bit concatenation
    t1ll_staging: u8, // Use t1_latch() to get the 16 bit concatenation
    t1lh: u8,
    t2cl: u8,
    t2ch: u8,
    t2ll: u8, // Use t2_latch() to get the 16 bit concatenation
    //t2lh: u8,


    // T1's counter current value is not stored in this struct here
    // because it depends on the current CPU cycle. So it is computed
    // each time it's needed. We do that because our emulation doesn't
    // "tick" the 6522 on every clock cycle but just interrogates
    // them when it is needed. This way we make our implementation
    // quite fast, but at the price of more complex computations.
    t1_latch_reset_tick: u64,
    t1_start_count: u64,

    t2_reset_tick: u64,
    t2_count_at_reset: u64,
    acr: u8, // bit 7&6: timer1 control
    ier: u8,
    ifr: u8,

    // MockingBoard stuff
    mb_command: u8,
    mb_register_select: u8,
    mb_submit: bool,

    irq_planned: bool,
    renderer: AYRenderer,
}

impl MOS6522 {
    fn reset(&mut self) {
        self.ier = 0;
        self.ifr = 0;
        self.t1_latch_reset_tick = 0;
        self.renderer.cycle = 0;
        let last_frame_cycle = self.renderer.cycle + (CYCLES_PER_FRAME as u64);

        self.renderer.record(ClockEvent {
            tick: last_frame_cycle,
            event: ClockEventData::MockingBoardReset,
        });
    }

    pub fn new(base_addr: usize, sample_rate: usize) -> MOS6522 {
        MOS6522 {
            base_addr,
            ddra: 0,
            ddrb: 0,
            ora: 0,
            orb: 0,
            ira: 0,
            irb: 0,
            t1cl: 0,
            t1ch: 0,
            t1ll: 0,
            t1ll_staging: 0,
            t1lh: 0,
            t2cl: 0,
            t2ch: 0,
            t2ll: 0,
            //t2lh: 0,
            t1_latch_reset_tick: 0,
            t1_start_count: 0,
            t2_reset_tick: 0,
            t2_count_at_reset: 0,
            acr: 0, // Auxiliary control register
            ier: 0,
            ifr: 0,
            // MockingBoard stuff
            mb_command: 0,
            mb_register_select: 0,
            mb_submit: false,
            irq_planned: false,
            renderer: AYRenderer::new(sample_rate, base_addr),
        }
    }

    fn t1_latch(&self) -> u16 {
        ((self.t1lh as u16) << 8) | self.t1ll as u16
    }

    // fn t2_latch(&self) -> u16 {
    //     ((self.t2lh as u16) << 8) | self.t2ll as u16
    // }

    fn _test_set_t1_latch(&mut self, latch: u16) {
        self.t1lh = ((latch & 0xFF00) >> 8) as u8;
        self.t1ll = (latch & 0x00FF) as u8;
    }

    // fn set_t2_latch(&mut self, latch: u16) {
    //     self.t2lh = ((latch & 0xFF00) >> 8) as u8;
    //     self.t2ll = (latch & 0x00FF) as u8;
    // }

    fn t1_control(&self) -> u8 {
        // 00, 10 == single interrupt
        // 01, 11 = continuous interrupt
        (self.acr & (0b11000000)) >> 6_u8
    }

    fn t2_control(&self) -> u8 {
        (self.acr & (0b00100000)) >> 5_u8
    }

    pub fn t1_next_irq_cycle(&self, tick: u64) -> u64 {
        // Next IRQ will occur at this cycle
        let nt = tick + (self.t1_current(tick) as u64);
        debug!("T1 next tick at {}", nt);
        nt
    }

    pub fn t2_restart(&mut self, tick: u64, count: u64) {
        self.t2_count_at_reset = count;
        self.t2_reset_tick = tick;
    }

    pub fn t2_current(&self, tick: u64) -> u16 {
        // Value of T1 at cycle tick.

        let elapsed_since_reset: u64 = tick - self.t2_reset_tick;

        if elapsed_since_reset >= self.t2_count_at_reset {
            // T2 value was set long ago

            assert!(self.is_t2_single_interrupt_mode(),
                "Only Single interrupt_mode is supported for T2");

            // T2 starts at $1000, counts down to 0 and generate interrupt (IFR := 1)
            // then continues to count down ($FFFF, $FFFE,...) to 0 and doesn't generate a new interrupt (IFR stays 1)
            // then start at $FFFF again.

            // T2 has looped and now counts from $FFFF
            // OxFFFF - : because we count /down/ !

            // So if elapsed_since_reset == t1_latch, t1 = 0
            // if elapsed_since_reset == t1_latch + 1, t1 = 0xFFFF
            // if elapsed_since_reset == t1_latch + 2, t1 = 0xFFFE
            // ...
            // if elapsed_since_reset == t1_latch + 0x10000, t1 = 0

            ((0x10000 - ((elapsed_since_reset - self.t2_count_at_reset) % 0x10000)) & 0xFFFF)
                as u16
        } else {
            // elapsed < self.t1_latch
            // T1 is still counting down for the first loop
            (self.t2_count_at_reset - elapsed_since_reset) as u16
        }
    }

    fn is_t1_single_interrupt_mode(&self) -> bool {
        let ddrb_pb7 = (self.ddrb & 0b1000_0000) >> 7;
        let pb7 = if ddrb_pb7 == 1 {
            // Output
            // WDC: When a line is programmed as an output, it
            // is controlled by a corresponding bit in the Output
            // Register (ORA & ORB).
            self.orb & 0b1000_0000 != 0
        } else {
            // Input
            self.irb & 0b1000_0000 != 0
        };

        // IER ultmately dictates if IRQ are exposed to the CPU when
        // it's time to trigger them.
        let mode = self.acr & 0b11000000;
        (mode == 0b0000_0000 || mode == 0b1000_0000) && !pb7
    }

    fn is_t2_single_interrupt_mode(&self) -> bool {
        let mode = self.acr & 0b0010_0000;
        mode == 0b0000_0000
    }

    fn is_t1_continuous_interrupt_mode(&self) -> bool {
        let ddrb_pb7 = (self.ddrb & 0b1000_0000) >> 7;
        let pb7 = if ddrb_pb7 == 1 {
            // Output
            // WDC: When a line is programmed as an output, it
            // is controlled by a corresponding bit in the Output
            // Register (ORA & ORB).
            self.orb & 0b1000_0000 != 0
        } else {
            // Input
            self.irb & 0b1000_0000 != 0
        };
        // IER ultmately dictates if IRQ are exposed to the CPU when
        // it's time to trigger them.
        let mode = self.acr & 0b11000000;
        (mode == 0b0100_0000 || mode == 0b1100_0000) && !pb7
    }

    pub fn t1_current(&self, tick: u64) -> u16 {
        // Virtual because the 6522 counts N cycles *plus* spend a little longer
        // on cycle 0, it actually wraps after N+2 cycles loops.

        // Actual number of cycles. And then, we'll transform that count
        // into what is actually reported by the 6522 to its user in another
        // function.

        // If latch = N, then we count N,N-1,...,1,0 inclusive.
        // Then we loop. The loop operation takes one cycle.
        // So we effectivelyt count: N,N-1,...,1,0,0. That is, we count N+2 cycles.
        // Since the latch is 16 bits, its highest possible value is 0xFFFF.

        // t = how many cycles before interrupt

        let t = self.cycles_before_t1_zero(tick);

        // t is between latch+1 (or 0xFFFF + 1, if single interrupt mode)
        // and 0.

        // If actual latch is 3, then
        // if t == 4: we return 4-1=3
        // if t == 3: we return 3-1=2
        // if t == 2: we return 2-1=1
        // if t == 1: we return 0 (t <= 1)
        // if t == 0: we return 0 (t <= 1)
        // if t == 4: go on (t==5 doesn't occur since we compute all modulo 5)

        if t <= 1 {
            0
        } else {
            (t - 1) as u16
        }
    }

    fn cycles_before_t1_zero(&self, cycle: u64) -> u64 {
        // NOTE: I don't store the timer counter current value.
        // That'sbe cause I don't want to update on every cycle
        // (for perf. reason). Therefore, what I do is to
        // compute when the counter will be zero based on the current
        // 6522 latches.

        // How many cycles to wait before the next time
        // T1 becomes 0.
        let latch_value: u64 = self.t1_latch() as u64;

        // How many cycles have passed since the counter
        // was restarted.
        let elapsed_since_reset: u64 = cycle - self.t1_latch_reset_tick;

        // Imagine that we call this after a long time and
        // no IRQ was triggered and that during that time
        // no register were changed by the user.

        let offset = 2;

        if elapsed_since_reset < latch_value + offset {
            // The counter had not enough time to reach zero.
            (latch_value + offset) - elapsed_since_reset
        } else {
            // The counter reached zero one or more times.

            // Time spent until the first time the counter reached zero.
            let t = elapsed_since_reset - (latch_value + offset);

            // After the first 0 reached, the T1 will loop.
            let cycles_for_loop = if self.is_t1_single_interrupt_mode() {
                0xFFFF + offset
            } else if self.is_t1_continuous_interrupt_mode() {
                if latch_value > 0 {
                    latch_value + offset
                } else {
                    0xFFFF // FIXME Not sure
                }
                //latch_time + offset
            } else {
                println!("Error: unknown T1 interrupt mode");
                0xFFFF + offset
            };

            // T1 counter value after 0 or after several full loops
            let t = t % cycles_for_loop;

            // Time before next 0
            cycles_for_loop - t
        }
    }


    fn cycles_before_t2_zero(&self, cycle: u64) -> u64 {
        // NOTE: I don't store the timer counter current value.
        // That's because I don't want to update on every cycle
        // (for perf. reason). Therefore, what I do is to
        // compute when the counter will be zero based on the current
        // 6522 latches.

        let offset = 0;
        (self.t2_current(cycle) as u64) + offset
    }

    fn schedule_or_unschedule_irq_request_t1_and_t2(&self, cycle: u64) -> Option<Vec<IRQRequest>> {
        let t1 = self.schedule_or_unschedule_irq_request_t1(cycle);
        let t2 = self.schedule_or_unschedule_irq_request_t2(cycle);

        if t1.is_some() && t2.is_some() {
            let mut c = t1.unwrap();
            c.extend(t2.as_ref().unwrap());
            Some(c)
        } else if t1.is_some() {
            return t1;
        } else if t2.is_some() {
            return t2;
        } else {
            None
        }
    }

    fn schedule_or_unschedule_irq_request_t1(&self, cycle: u64) -> Option<Vec<IRQRequest>> {
        /* WDC: There are three basic interrupt operations, including:
        - setting the interrupt flag within IFR (IF = interrutp flag),
        - enabling the interrupt by way of a corresponding bit in the IER (IE = interrupt enable),
        - and signaling the microprocessor using IRQB.

        WDC: Once the counter is loaded under program control, it
        decrements at Phase 2 clock rate. Upon reaching zero, bit 6 of the
        Interrupt Flag Register (IFR) is set, causing Interrupt Request ([) to
        go to Logic 0 if the corresponding bit in the Interrupt Enable
        Register (IER) is set.

        So, right before the counter reaches 0, IER must be set to one
        and IFR must be set to 0 (to be able to go from zero to one and
        trigger the IRQ on the 6502).

        When signalling IRQ to the 6502, WDC's doc says (page 31):

        "The IRQB output signal is logic 0 whenever an internal Interrupt Flag bit
        is set to logic 1 and the corresponding Interrupt Enable bit is logic 1."

        When IFR goes to one, who sets it to zero ?
        */

        trace!("schedule_or_unschedule: self.t1_latch_reset_tick={}",self.t1_latch_reset_tick);

        let base_addr = self.base_addr + 1; // Mark timer 1

        if self.ier & MOS6522_IER_TIMER1 != 0 // interrupt enable is set
		// interrupt flag is not set when we reach the moment to do the irq.
		// So at that moment, if that flag is not set yet, then it can
		// be set and trigger the IRQ.
	    //&& self.ifr & !MOS6522_IFR6 == 0
	    //&& self.t1_latch_reset_tick > 0
        {
            /* In 2.5 Timer 1 Operation

            Once the Timer reaches a count of zero, it will either disable any
            further interrupts (provided it has been programmed
            to do so) (OneShot mode I think), or it will automatically transfer
            the contents of the latches into the counter and proceed to
            decrement again (FreeRun mode I think)).
            */
            let cb0 = self.cycles_before_t1_zero(cycle);

            // FIXME This makes MADEF.DSK work.Dunno why :-(
            const ADDITIONAL_LATENCY_HACK: u64 = 1;

            let v = vec![IRQRequest::new(
                cycle + cb0 + ADDITIONAL_LATENCY_HACK,
                self.get_slot(),
                base_addr, // subsource
                true,
                IRQAction::Replace,
            )];
            trace!(
                "schedule IRQ T1: base:${:04X} IER6:{} IFR6:{} at cycle {}+{}={} / T1C={}",
                base_addr,
                self.ier & !MOS6522_IFR6 != 0,
                self.ifr & !MOS6522_IFR6 != 0,
                cycle,
                cb0,
                cycle + cb0,
                self.t1_current(cycle)
            );
            Some(v)
        } else {
            // No interrupt pending (anymore), so clear any remaining interrupt
            // request.

            trace!(
                "unschedule IRQ T1: base:${:04X} IER6:{} IFR6:{} Latch set?:{}",
                base_addr,
                self.ier & 0b0100_0000 != 0,
                self.ifr & !MOS6522_IFR6 != 0,
                self.t1_latch_reset_tick > 0
            );
            //None
            let v = vec![IRQRequest::new(
                0,
                self.get_slot(),
                base_addr,
                false,
                IRQAction::ClearAll,
            )];
            Some(v)
        }
    }

    fn schedule_or_unschedule_irq_request_t2(&self, cycle: u64) -> Option<Vec<IRQRequest>> {

        // if self.ier & MOS6522_IER_TIMER2 == 0 {
        //     return None;
        // }

        let base_addr = self.base_addr + 2; // Mark timer 2

        let cb0 = self.cycles_before_t2_zero(cycle);

        let cycles_to_next_stop = if cb0 == 0 {
                0x10000
            } else {
                cb0
            };

        let testing_latency = 0;
        let v = vec![IRQRequest::new(
            cycle + cycles_to_next_stop - testing_latency,
            self.get_slot(),
            base_addr,
            true,
            IRQAction::Replace,
        )];
        trace!(
            "schedule IRQ T2: ${:04X} IER5:{} IFR5:{} at cycle {}+{}={} / T2C={}",
            base_addr,
            self.ier & !MOS6522_IFR5 != 0,
            self.ifr & !MOS6522_IFR5 != 0,
            cycle,
            cb0,
            cycle + cb0,
            self.t2_current(cycle)
        );
        Some(v)
    }

    fn irq_line(&self) -> IrqChange {
        match self.irq_line_level() {
            IrqLineLevel::High => {
                //trace!("IrqChange: PullUp");
                IrqChange::PullUp
            },
            IrqLineLevel::Low => {
                //trace!("IrqChange: PullDown");
                IrqChange::PullDown
            }
        }
    }

}

impl GenericPeripheral for MOS6522 {
    fn get_name(&self) -> String {
        String::from("MOS6522")
    }

    fn get_slot(&self) -> usize {
        4
    }

    fn irq_line_level(&self) -> IrqLineLevel {
        // This test is bad. See 6522 IFR documentation
        // See WDC 6522 2.14:
        // IRQ = IFR6 ∧ IER6 ∨ IFR5 ∧ IER5 ∨ IFR4 ∧ IER4 ∨ IFR3 ∧ IER3 ∨ IFR2 ∧ IER2 ∨ IFR1 ∧ IER1 ∨ IFR0 ∧ IER0.
        if (self.ifr & 0b0111_1111) & self.ier != 0 {
            IrqLineLevel::Low
        } else {
            IrqLineLevel::High
        }
    }


    fn status_str(&self, cycle: u64) -> String {
        let t1c = self.t1_current(cycle);
        let _t2c = self.t2_current(cycle);
        format!("T1 C:{:5X} L:{:5} Mode:{} | T2 C:{:5} Mode:{} @rst:{:5} ACR:{:02b}-{:01b}-{:03b}-{:02b} IER:{:02b}-{:05b} IFR:{:02b}-{:05b} DDRB:{:08b} ORB:{:08b}",
		       t1c,
		       (self.t1lh as u16) << 8 | (self.t1ll as u16),
               if self.is_t1_single_interrupt_mode() { "OneShot" }
               else if self.is_t1_continuous_interrupt_mode() {"FreeRun"}
               else {"???"},
		       self.t2_current(cycle),
               if self.is_t2_single_interrupt_mode() { "OneShot" }
               else {"???"},
               self.t2_count_at_reset,
		       (self.acr & 0b11000000) >> 6,
		       (self.acr & 0b00100000) >> 5,
		       (self.acr & 0b00011100) >> 2,
		       self.acr & 0b00000011,
		       (self.ier & 0b01100000) >> 5,
		       self.ier & 0b00011111,
		       (self.ifr & 0b01100000) >> 5,
		       self.ifr & 0b00011111,
		       self.ddrb,
		       self.orb)
    }

    fn read_io(
        &mut self,
        addr: u16,
        mem_byte: u8,
        cycle: u64,
    ) -> (u8, Option<Vec<IRQRequest>>, Option<IrqChange>) {
        let register = (addr & 0xF) as usize;

        match register {
            MOS6522_ORB /* 0 */ => { //IRB or ORB
                trace!("read ORB/IRB");
                // FIXME Sill have to do input latching
                let from_orb = self.orb & self.ddrb;
                let from_irb = self.irb & !self.ddrb;
                (from_orb | from_irb, None, None)
            },
            MOS6522_ORA /* 1 */  => {
                trace!("read ORA");
                (self.ora, None, None)
                },
            MOS6522_DDRB /* 2 */ => {
                (self.ddrb, None, None)
            },
            MOS6522_DDRA /* 3 */ => {
                (self.ddra, None, None)
            },
            MOS6522_T1CL /* 4 */ => {
                trace!("{:04X} Read T1CL and clear IFR6", self.base_addr);
                // WDC: 8 bits from T1 low order counter transferred to MPU.
                //      T1 interrupt flag IFR6 is reset.
                self.ifr &= MOS6522_IFR6;
                ((self.t1_current(cycle) & 0xFF) as u8,
                self.schedule_or_unschedule_irq_request_t1(cycle),
                Some(self.irq_line()))
            },
            MOS6522_T1CH /* 5 */=> {
                trace!("{:04X} read T1CH", self.base_addr);
                // WDC: 8 bits from T1 high order counter transferred to MPU.
                (((self.t1_current(cycle) & 0xFF00) >> 8) as u8, None, None)
            },
            MOS6522_T1LL /* 6 */=> {
                trace!("{:04X} read T1LL", self.base_addr);
                // WDC: 8 bits from T1 low order latches transferred to MPU. Unlike reading the T1 Low
                //      Order Register, this does not cause reset of T1 interrupt flag IFR6.
                ((self.t1_latch() & 0xFF) as u8, None, None)
            },
            MOS6522_T1LH /* 7 */=> {
                trace!("{:04X} read T1LH", self.base_addr);
                // WDC: 8 bits from T1 high order counter transferred to MPU.
                (((self.t1_latch() & 0xFF00) >> 8) as u8, None, None)
            },
            MOS6522_T2CL /* 8 */ => {
                trace!("{:04X} Read T2CL and clear IFR6", self.base_addr);
                // WDC: 8 bits from T1 low order counter transferred to MPU.
                //      T2 interrupt flag IFR5 is reset.
                self.ifr &= MOS6522_IFR5;
                ((self.t2_current(cycle) & 0xFF) as u8,
                  self.schedule_or_unschedule_irq_request_t2(cycle),
                  Some(self.irq_line()))
            },
            MOS6522_T2CH /* 9 */=> {
                trace!("base ${:04X} read T2CH", self.base_addr);
                // WDC: 8 bits from T2 high order counter transferred to MPU.
                (((self.t2_current(cycle) & 0xFF00) >> 8) as u8, None, None)
            },
            MOS6522_IFR  /* $D */ => {
                trace!("base ${:04X} read IFR ({:b})", self.base_addr, self.ifr);
                (self.ifr, None, None)
            }
            _ => (mem_byte, None, None)
	}
    }

    fn write_io(
        &mut self,
        addr: u16,
        data: u8,
        cycle: u64,
    ) -> (Option<Vec<IRQRequest>>, Option<IrqChange>) {
        // Following a write to memory, this transfers the
        // written data to the peripheral.
        //println!("MOS6522 - writeIO");
        let register = (addr & 0xF) as usize;
        let base_io = addr & 0xFFF0;

        match register {
            MOS6522_DDRB /* $02 */ => {

                // From W65C22:

                //  Logic 0 in any bit position of the register will
                //  cause the corresponding pin to serve as an input;
                //  while a logic 1 will cause the pin to serve as an
                //  output.

                self.ddrb = data;
                trace!("{:02X} DDRB <- ${:02X}", self.base_addr, self.ddrb);
            },
            MOS6522_DDRA /* $03 */ => {
                // Data Direction Register A

                // From W65C22:

                //  Logic 0 in any bit position of the register will
                //  cause the corresponding pin to serve as an input;
                //  while a logic 1 will cause the pin to serve as an
                //  output.

                self.ddra = data;
                trace!("{:02X} DDRA <- ${:02X}", self.base_addr, self.ddra);
            },
            MOS6522_T1CL /* $04 */ => {
                // WDC: WRITE - 8 bits loaded into T1 low order
                // latches. Latch contents are transferred into low
                // order counter at the time the high order counter is
                // loaded.

                trace!("{:04X} T1CL <- ${:02X}", base_io, data);
                self.t1ll_staging = data
            },
            MOS6522_T1CH /* $05 */ => {
                trace!("{:04X} T1CH <- ${:02X} and clear IFR6 and latch set", base_io, data);
                // WDC (Table 2-6 T1H): WRITE - 8 bits loaded into T1 high order
                // latches. Also, both high and low order latches are
                // transferred into T1 counter and this initiates
                // countdown. T1 interrupt flag IFR6 is reset.

                self.t1lh = data;
                self.t1ll = self.t1ll_staging;
                self.t1cl = self.t1ll;
                self.t1ch = self.t1lh;
                self.t1_latch_reset_tick = cycle;

                //self.ifr = self.ifr | !MOS6522_IFR6;
                self.ifr &= MOS6522_IFR6;
                return (self.schedule_or_unschedule_irq_request_t1(cycle), Some(self.irq_line()));
            },
            MOS6522_T1LL /* $06 */ => {
                // WDC WRITE: 8 bits loaded into T1 low order
                // latches. This operation is no different than a
                // write into the T1 Low Order Register.

                trace!("{:04X} T1LL <- ${:02X}", base_io, data);
                self.t1ll = data
            },
            MOS6522_T1LH /* $07 */=> {
                trace!("T1LH <- ${0:X} and clear IFR6",data);

                // WDC WRITE - 8 bits loaded into T1 high order
                // latches. Unlike writing to the T1 Low Order
                // Register, no latch to counter transfers takes
                // place. T1 interrupt flag IFR6 is reset.

                self.t1lh = data;
                self.ifr &= MOS6522_IFR6;
                return (self.schedule_or_unschedule_irq_request_t1(cycle), Some(self.irq_line()));
            },
            MOS6522_T2CL /* $08 */ => {
                trace!("T2CL <- ${0:X}",data);
                // 8 bits loaded into T2 low order latch
                self.t2ll = data
            },
            MOS6522_T2CH /* $09  */ => {
                trace!("T2CH <- ${0:X} and clear IFR5",data);
                // 8 bits loaded into T2 high order counter
                self.t2ch = data;
                // Low order latch transferred to low order counter
                self.t2cl = self.t2ll;
                self.t2_restart(cycle, ( ((self.t2ch as u16) << 8) + (self.t2cl as u16)) as u64 + 1);
                // T2 interrupt flag is reset
                self.ifr &= MOS6522_IFR5;
                return (self.schedule_or_unschedule_irq_request_t2(cycle), Some(self.irq_line()));
            },

            MOS6522_ACR /* $0B */=> {
                // ACR == Auxiliary Control Register
                //      +------------+-----------+-----------------+----+----+
                // Bit  ! 7     6    !     5     ! 4    3    2     ! 1  !  0 !
                //      +------------+-----------+-----------------+----+----+
                //      ! T1  timer  ! T2  timer ! Shift  register ! PB ! PA !
                //      !  control   !  control  !     control     !    !    !
                //      +------------+-----------+-----------------+----+----+

                debug!("Setting ACR {} - cycle:{} ", self.base_addr, cycle);
                self.acr = data;

                // If interrupt mode change (single or continuous)
                // then one the WHEN of the next IRQ (if any) changes.

                return (self.schedule_or_unschedule_irq_request_t1_and_t2(cycle), None);
            },
            MOS6522_IFR /* $0D */ => {
                /* WDC: The IFR may be read directly by the microprocessor, and
                   individual flag bits may be *cleared* by *writing* a logic 1
                   into the appropriate bit of the IFR.
                */


                let old_ifr = self.ifr;

                self.ifr &= !data;
                trace!("MOS6522_IFR {} <- Clearing bits mask:{:08b}, ifr: {:08b} -> {:08b}", cycle, data, old_ifr, self.ifr);

                // Clearing the IFR indicates (usually) that the
                // client program has serviced the interrupt.
                // Here we can clear both timer 1 (IFR5) and timer 2 (IFR6).
                return (self.schedule_or_unschedule_irq_request_t1_and_t2(cycle), Some(self.irq_line()));
            },
            MOS6522_IER /* $0E */ => {
                debug!("MOS6522_IER <- data:${:02X}",data);
                if (data & 0b1000_0000) != 0 {
                    // Set values in IER
                    debug!("MOS6522_IER set operation ${:02X}", data & 0b0111_1111);
                    self.ier |= data & 0b0111_1111;
                } else {
                    debug!("MOS6522_IER clear operation ${:02X}", data & 0b0111_1111);
                    // Clear values in IER
                    self.ier &= !data;
                }
                // Here we can toggle both timer 1 and timer 2.
                return (None, Some(self.irq_line()))
                // if self.ier & 0b0100_0000 == 0 { // WDC page 26
                //     return self.clear_irq_request(cycle)
                // } else {
                //     return self.make_or_clear_irq_request(cycle)
                // }
            },
	    /* From https://www.apple2.org.za/gswv/a2zine/Docs/Mockingboard_MiniManual.html

	    Write register number to ORA
	    Write "set reg command $07" to ORB
	    Submit with write "inactive $04" to ORB

	    Write new register value to ORA
	    Write "write value $06" to ORB
	    Submit with write "inactive" to ORB
	     */
	    MOS6522_ORA /* $01 */=> {
            // ORA bits can be either output or input depending on DDRA
            // DDRA bit == 0 means ORA bit serves as input.
            // DDRA bit == 1 means ORA bit serves as output.
            // self.ora & !self.ddra : clear ORA bits which will accept input
            // | (data & self.ddra) : set cleared bits to new value, and those cleared bits only.

            let mask = self.ddra;
            self.ora = (self.ora & !mask) | (data & mask);
            trace!("ORA <- ${:02X} (ddra:${:02X}, cpu:${:02X})", self.ora, self.ddra, data);
	    },
	    MOS6522_ORB /* 0x00 */ => {
            let mask = self.ddrb;
            self.orb = (self.orb & !mask) | (data & mask);
            trace!("ORB <- ${:02X} (ddrb:${:02X}, cpu:${:02X})", self.orb, self.ddrb, data);

            match self.orb {
                MB_RESET => {
                    self.mb_command = MB_RESET;
                }
                MB_SET_REGISTER /* 07 */ => {
                    // Set register number
                    self.mb_command = MB_SET_REGISTER;
                },
                MB_WRITE_DATA /* 06 */  => {
                    // Set register number
                    self.mb_command = MB_WRITE_DATA;
                },
                MB_INACTIVE  /* 04 */ => {
                    // Inactive
                    match self.mb_command {
                        MB_SET_REGISTER => {
                            self.mb_register_select = self.ora;
                            trace!("MB reg select:${:02X}", self.mb_register_select);
                        },
                        MB_WRITE_DATA => {
                            // self.mb_registers[self.mb_register_select] = self.ora;
                            trace!("{:02X} MB write. Register:${:02X} ORA:${:02X}", self.base_addr, self.mb_register_select, self.ora);
                            self.renderer.record(
                                ClockEvent {
                                tick: cycle,
                                event: ClockEventData::MockingBoardWrite{
                                    register: self.mb_register_select,
                                    data: self.ora}});
                        },
                        MB_RESET => {
                            trace!("MB reset");
                            self.renderer.record(
                                ClockEvent {
                                tick: cycle,
                                event: ClockEventData::MockingBoardReset
                                });
                        },
                        _ => ()
                    }
                },
                _ => ()
            }
	    },
	    _ => ()
	}

        (None, None)
    }

    fn ack_irq(
        &mut self,
        _req: IRQRequest,
        cycle: u64,
    ) -> (Option<Vec<IRQRequest>>, Option<IrqChange>) {

        // Warning: here we make the assumption that ack_irq is called only when the
        // 6522 *does* an interrupt.

        // Which timer are we talking about ?

        let timer = _req.sub_source & 0b11;

        // Setting IFR flag (we assume to we end up here when a counter reaches
        // 0, i.e. when it triggers an interrupt).
        match timer {
            1 => self.ifr |= !MOS6522_IFR6,
            2 => self.ifr |= !MOS6522_IFR5,
            _ => panic!("Unexpected timer value {}", timer)
        }


        trace!(
            "Base:${:04X} T{} Ack'ing IRQ ACR - cycle:{} T1C:{}. IFR is now: {:08b}",
            self.base_addr,
            timer,
            cycle,
            self.t1_current(cycle),
            self.ifr
        );

        // In case we have a "free run" operation, we need to schedule
        // the next IRQ.
        let rescheduled = self.schedule_or_unschedule_irq_request_t1_and_t2(cycle);


        // FIXME Ther's a pull up to do here.
        // According to the doc, the pull up/down is done
        // following bit 7 of ifr which is  ifr_bit_0 * ier_bit_0 + ifr_bit_1 * ier_bit_1 + ... ifr_bit_6 * ier_bit_6
        //
        /* Read section 2-14 of WDC Doc.:
        All Interrupt Flags are contained within a single IFR. Bit 7 of this register will be Logic 1 whenever an
        Interrupt Flag is set, thus allowing convenient polling of several devices within a system to determine the
        source of the interrupt.
                and then there's theformula.
            */

        // My guess: The user will have to clear IFR6 in order to schedule
        // a new IRQ
        (rescheduled, Some(
            self.irq_line()
        ))
    }
}

pub struct MockingBoard {
    mos6522_1: MOS6522,
    mos6522_2: MOS6522,
}

impl MockingBoard {
    pub fn new(samples_per_frame: usize) -> MockingBoard {


        MockingBoard {
            mos6522_1: MOS6522::new(0x0, samples_per_frame),
            mos6522_2: MOS6522::new(0x80, samples_per_frame),
        }
    }

    fn irq_line(&self) -> IrqChange {
        if self.mos6522_1.irq_line() == IrqChange::PullDown ||
            self.mos6522_2.irq_line() == IrqChange::PullDown {
            IrqChange::PullDown
        } else {
            IrqChange::PullUp
        }
    }
}

/*
Write register number to ORA
Write "set reg command $07" to ORB
Submit with write "inactive $04" to ORB

Write new register value to ORA
Write "write value $06" to ORB
Submit with write "inactive" to ORB

*/

impl GenericPeripheral for MockingBoard {
    fn get_name(&self) -> String {
        String::from("MockingBoard")
    }

    fn get_slot(&self) -> usize {
        // FIXME Hardcoded !
        4
    }

    fn status_str(&self, cycle: u64) -> String {

        // s1.push_str(" // ");
        // s1.push_str(self.mos6522_2.status_str(cycle).as_str());
        let mut s= "".to_string();
        s.push_str("[6522-1] ");
        s.push_str(&self.mos6522_1.status_str(cycle));
        s.push_str("\n     [6522-2] ");
        s.push_str(&self.mos6522_2.status_str(cycle));
        s
    }

    fn read_io(
        &mut self,
        addr: u16,
        mem_byte: u8,
        cycle: u64,
    ) -> (u8, Option<Vec<IRQRequest>>, Option<IrqChange>) {
        let base_addr = (addr & 0xF0) as usize;
        let mut res = match base_addr {
            0 => self.mos6522_1.read_io(addr, mem_byte, cycle),
            0x80 => self.mos6522_2.read_io(addr, mem_byte, cycle),
            _ => {
                warn!("Wrong 6522!");
                (0xFF, None, None)
            }
        };
        res.2 = Some(self.irq_line());
        res
    }

    fn write_io(
        &mut self,
        addr: u16,
        data: u8,
        cycle: u64,
    ) -> (Option<Vec<IRQRequest>>, Option<IrqChange>) {
        trace!("Mockingboard: write_io ${:X} <- ${:02X}", addr, data);
        let base_addr = (addr & 0xF0) as usize;
        let mut res = match base_addr {
            0 | 0x01 => self.mos6522_1.write_io(addr, data, cycle),
            0x80 | 0x81 => self.mos6522_2.write_io(addr, data, cycle),
            _ => {
                warn!("Wrong 6522!");
                (None, None)
            }
        };

        res.1 = Some(self.irq_line());
        res
    }

    fn ack_irq(
        &mut self,
        req: IRQRequest,
        cycle: u64,
    ) -> (Option<Vec<IRQRequest>>, Option<IrqChange>) {
        let base_addr = req.sub_source & 0xF0;
        let mut res = match base_addr {
            0 => self.mos6522_1.ack_irq(req, cycle),
            0x80 => self.mos6522_2.ack_irq(req, cycle),
            _ => panic!("Wrong 6522!"),
        };
        res.1 = Some(self.irq_line());
        trace!("ACK IRQ: cycle:{}: {} : {:08b} {:08b}", cycle, res.1.as_ref().unwrap(), self.mos6522_1.ifr, self.mos6522_2.ifr);
        res
    }

    fn reset(&mut self) {
        self.mos6522_1.reset();
        self.mos6522_2.reset();
    }
}

impl SoundRenderer for MockingBoard {
    fn record(&mut self, _event: ClockEvent) {
        // Everything is done at the AY level for now
    }

    fn samples(&self) -> Arc<Mutex<VecDeque<i16>>> {
        self.mos6522_2.renderer.samples()
    }

    fn process_frame(&mut self) -> SamplesFrame {
        let a = self.mos6522_1.renderer.process_frame();
        let b = self.mos6522_2.renderer.process_frame();
        SamplesFrame::new(a.left, b.left)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_countdown() {
        env_logger::init();

        let mut p = MOS6522::new(0, 44100);
        // Latch = 0
        assert_eq!(p.t1_current(0), 0);
        assert_eq!(p.t1_current(1), 0xFFFF);
        assert_eq!(p.t1_current(2), 0xFFFE);
        assert_eq!(p.t1_current(0x10000), 0);

        p._test_set_t1_latch(1000);
        let base_cycle = 1000;
        p.write_io((0xC400 + MOS6522_ACR) as u16, 0b01000000, base_cycle); // Continuous interrupt

        // First loop
        assert_eq!(p.t1_current(base_cycle), 1000);
        assert_eq!(p.t1_current(base_cycle + 1), 999);
        assert_eq!(p.t1_current(base_cycle + 2), 998);
        assert_eq!(p.t1_current(base_cycle + 999), 1);

        // Second loop
        assert_eq!(p.t1_current(base_cycle + 1000), 1000);
        assert_eq!(p.t1_current(base_cycle + 1001), 999);
        assert_eq!(p.t1_current(base_cycle + 2000), 1000);
        assert_eq!(p.t1_current(base_cycle + 2001), 999);

        let irq_req = IRQRequest::new(base_cycle + 1000, 4, 0, true, IRQAction::Replace);

        let (r, _irq_change) = p.ack_irq(irq_req, base_cycle + 1000);
        let r = r.unwrap();
        let x = r.first().unwrap();
        debug!("{}", x.irq_cycle);
        assert_eq!(x.irq_cycle, base_cycle + 2000); // Next IRQ after the ACK
    }
}
