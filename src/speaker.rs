use log::{debug, error, info, trace, warn};
use std::sync::{Arc, Mutex};
use std::collections::VecDeque;
use crate::a2::{CYCLES_PER_FRAME, CYCLES_PER_SECOND, FRAMES_PER_SECOND,ClockEvent};
use crate::emulator::OutputQueues;
use crate::sound::{cycle_to_sample, SamplesFrame, SoundRenderer};

struct Oscillator {
    /// mass
    m: f32,
    /// lambda = friction coefficient
    lam: f32,
    /// delta =
    delta: f32,
    /// square root of delta over 2.
    sqrt_delta_over_2: f32,
    cycles_per_sample: f32,
}

impl Oscillator {
    pub fn new(cycles_per_sample: f32) -> Oscillator {
        const M: f32 = 1.0;
        const TICK_FREQ: f32 =
            2.0 * std::f32::consts::PI * 1.0 / ((CYCLES_PER_SECOND as f32) / 3875.0);

	// Lambda is the friction
        const LAMBDA: f32 = 0.002353;

        // We compute the spring force such that the spring will have the
        // proper frequency when sampled at a frequency of `CYCLES_PER_SECOND`.
        let beta: f32 = (4.0 * TICK_FREQ.powi(2) + LAMBDA.powi(2)) / 4.0;
        let delta: f32 = -((LAMBDA / M).powi(2) - 4.0 * beta / M);

        Oscillator {
            m: 1.0,
            lam: LAMBDA,
            delta,
            sqrt_delta_over_2: delta.sqrt() / 2.0,
            cycles_per_sample,
        }
    }

    fn compute_max(&self) -> f32 {
        /* Compute the value by which we'll divide the samples
        to bring them back in [-1.0,+1.0]*/
        let (a, b) = self.compute_a_b(0.0, 0.0, 1.0);
        let lm2 = -self.lam / (2.0 * self.m);
        let t = (a * lm2 - b * self.sqrt_delta_over_2) / (a * b * self.sqrt_delta_over_2 + b * lm2);
        let max = t.atan() / self.sqrt_delta_over_2;
        let max = 7.0; // FIXME make a correct computation.
        trace!("Compute max : {} (a={}, b={})", max, a, b);
        return max;
    }

    fn compute_a_b(&self, h: f32, v0: f32, V: f32) -> (f32, f32) {
        let a = (h - V) / 2.0;
        let b = -(v0 + a * self.lam / self.m) / self.delta.sqrt();
        return (a, b);
    }

    /// Value of the oscillator function at position `x`
    /// given initial position `h` and speed `v0`.
    fn oscillator_at(&self, x_in: u64, ic_h: f32, ic_v0: f32, equilbrium_position: f32) -> f32 {
        let x = x_in as f32;
        let (a, b) = self.compute_a_b(ic_h, ic_v0, equilbrium_position);

        let e = (-self.lam / (2.0 * self.m) * x).exp();
        let c = (self.sqrt_delta_over_2 * x).cos();
        let s = (self.sqrt_delta_over_2 * x).sin();

        let r = 2.0 * e * (a * c - b * s) + equilbrium_position;
        trace!(
            "oscillator: x:{} h={} -> r={} (e={}, V={})",
            x_in,
            ic_h,
            r,
            e,
            equilbrium_position
        );
        return r;
    }

    /// First derivative of the oscillator function.
    fn oscillator_dydx(&self, x: f32, ic_h: f32, ic_v0: f32, equilbrium_position: f32) -> f32 {
        let (a, b) = self.compute_a_b(ic_h, ic_v0, equilbrium_position);
        let lm2 = -self.lam / (2.0 * self.m);

        let e = (lm2 * x).exp();
        let c = (self.sqrt_delta_over_2 * x).cos() * (a * lm2 - b * self.sqrt_delta_over_2);
        let s = (self.sqrt_delta_over_2 * x).sin() * (a * self.sqrt_delta_over_2 + b * lm2);
        return 2.0 * e * (c - s);
    }
}

fn voltage(tick_ndx: usize, cycles_since_last_tick: u64, cycles_per_sample: f32) -> f32 {
    let cslt = cycles_since_last_tick as f32;

    // The first voltage will be != 0.
    let v: f32 = if tick_ndx % 2 == 1 {
        0.0
    } else {
        let k: f32 = cycles_per_sample;

        // Values were measured in 44KHz samples => I mul by K
        if cslt < 1000.0 * k {
            1.0
        } else if 1000.0 * k <= cslt && cslt < 3000.0 * k {
	    // FIXME This is linear but should be 1/x
            1.0 - (cslt - 1000.0 * k) / ((3000.0 - 1000.0) * k)
        } else {
            0.0
        }
    };

    trace!("Voltage(): ndx={}, cycles={} -> {}", tick_ndx, cslt, v);
    return v;
}

struct OscillatorSampler {
    /// Initial condition : y position
    ic_h: f32,
    /// Initial condition : speed
    ic_v0: f32,
    /// Initial condition : base voltage
    ic_voltage: f32,
    last_tick: u64,
    tick_ndx: usize,
    _subs: Vec<(f32, f32)>,
    sample_ndx: usize,
    oscillator: Oscillator,
    samples: Vec<f32>,
    zero_tick: u64,
    target_freq: usize,
    cycles_per_sample: f64,
}

impl OscillatorSampler {
    /// Create a new simple harmonic oscillator sampler that
    /// will render itself at frequency of `target_freq` Hertz.
    pub fn new(target_freq: usize) -> OscillatorSampler {
        let cycles_per_sample = (CYCLES_PER_SECOND as f32) / (target_freq as f32);
        OscillatorSampler {
            ic_h: 0.0,
            ic_v0: 0.0,
            ic_voltage: voltage(0, 0, cycles_per_sample),
            last_tick: 0,
            tick_ndx: 0,
            _subs: vec![],
            sample_ndx: 0,
            oscillator: Oscillator::new(cycles_per_sample),
            samples: vec![],
            zero_tick: 0,
            target_freq,
            cycles_per_sample: (CYCLES_PER_SECOND as f64) / (target_freq as f64),
        }
    }

    fn extract_samples_with_padding(&mut self, n: usize) -> SamplesFrame {
        // We extract only completed samples (there may be one sample in construction)
        // The length of the frame will be padded to reach `n` samples.
        let l = self.samples.len();

        if l  == 0 {
            return SamplesFrame::new( vec![0i16; n], vec![0i16; n] );
        } else {
            let nn = n.min(l);
            let f: f32 = (i16::MAX as f32) / self.oscillator.compute_max();
            let mut c_left: Vec<i16> = self.samples[0..nn]
                .iter()
                .map(|&e| (e * f) as i16)
                .collect();
            if l < n {
                // We need padding
                println!("Padding to {}-{}", n, l);
                c_left.resize(n - l + 1, *c_left.last().unwrap());
            }
            let c_right = c_left.clone();
            self.samples.drain(0..nn);
            return SamplesFrame::new(c_left, c_right);
        }
    }

    /// Compute the numbers of the cycles at the
    /// beginning and end of the sample currently in
    /// construction. Note that since the number of cycles
    /// per sample is not an integer number, these
    /// numbers are rounded and thus carry an error.
    fn current_sample_bounds(&self) -> (u64, u64) {
        let b = ((self.sample_ndx as f64) * self.cycles_per_sample).floor() as u64;
        let e = (((self.sample_ndx + 1) as f64) * self.cycles_per_sample).floor() as u64;
        return (b, e);
    }

    /// Number of cycles inside a sample.
    ///
    /// Since we describe the boundaries of a sample by (integer) cycles numbers,
    /// the size of a sample (measured in cycles) vary from one sample to
    /// the other.
    fn cycles_in_sample(&self) -> f32 {
        let (b, e) = self.current_sample_bounds();
        return (e - b) as f32;
    }

    fn next_sample(&mut self) -> (u64, u64) {
        self.sample_ndx += 1;
        return self.current_sample_bounds();
    }

    /// Generate samples up to `to_cycle`, starting at the current
    /// sample.
    ///
    /// Since each sample covers several cycles, this will stop when
    /// the `to_cycle` cycle is found to be inside a sample. IOW, we
    /// generate only full samples here.
    fn oscillate_at_samples(&mut self, to_cycle: u64) {
        let (b, e): (u64, u64) = self.current_sample_bounds();
        trace!("oscillate_at_samples to_cycle:{} [{},{}]", to_cycle, b, e);
        assert!(
            to_cycle >= b,
            "The target cycle for the generation must be after the current sample"
        );
        assert!(
            self._subs.is_empty(),
            "We can only generate full samples, so we can't start with a half-finished one"
        );
        loop {
            let (b, e): (u64, u64) = self.current_sample_bounds();
            if b <= to_cycle && to_cycle < e {
                // Destination reached
                return;
            }
            assert!(self.last_tick <= b, "{} <= {}", self.last_tick, b);

            // Compute h at the beginning of the sample.
            let time_since_tick = b - self.last_tick;
            let h = self.oscillator.oscillator_at(
                time_since_tick,
                self.ic_h,
                self.ic_v0,
                self.ic_voltage,
            );
            self.samples.push(h);
            self.next_sample();
        }
    }

    fn invariant(&self) {
        let (b, e): (u64, u64) = self.current_sample_bounds();
        assert!(
            self.last_tick == 0
		|| self._subs.len() == 0 // Last tick not yet placed into a sample
		|| (b <= self.last_tick && self.last_tick < e),
            "Invariant failed: {} <= last_tick:{} < {}",
            b,
            self.last_tick,
            e
        );
    }

    fn finish_current_sample(&mut self) {
        assert!(
            self._subs.len() >= 1,
            "Current sample is empty, so there's no reason to finish it"
        );
        self.invariant();
        let (_b, e): (u64, u64) = self.current_sample_bounds();

        // Compute the "h" value at the end boundary of the sample.
        let dx = e - self.last_tick;
        let h = self
            .oscillator
            .oscillator_at(dx, self.ic_h, self.ic_v0, self.ic_voltage);
        let weight_in_subs = dx;
        self._subs
            .push((h, (weight_in_subs as f32) / self.cycles_in_sample()));

        // Now the sample is complete. We can compute its average amplitude.
        assert!(self._subs.len() >= 1);
        let mut sample = 0.0;
        let mut all_w = 0.0;
        for (hh, weight) in &self._subs {
            sample += hh * weight;
            all_w += weight;
        }

        assert!(
            0.999 <= all_w && all_w <= 1.001,
            "The sum of the weight of each sub sample should be 1, it is {}",
            all_w
        );

        self.samples.push(sample);
        self._subs.clear();
    }

    fn ghost_tick_at(&mut self, stop_cycle: u64) {
        self.invariant();
        trace!("Ghost tick at {}", stop_cycle);

        let (b, e): (u64, u64) = self.current_sample_bounds();
        assert!(
            stop_cycle >= e,
            "Ghost tick: The stop_cycle must be outside the current bounds {} >= [{},{}]",
            stop_cycle,
            b,
            e
        );

        if self._subs.len() >= 1 {
            self.finish_current_sample();
            self.next_sample();
        }

        // Do the rest of the samples
        let dx = e - self.last_tick;
        self.oscillate_at_samples(stop_cycle);

        let (b, e): (u64, u64) = self.current_sample_bounds();
        let dx = e - self.last_tick;
        assert!(
            stop_cycle == b,
            "stop_cycle must be on a sample boundary [{},{}] and {}, bound_ndx={}",
            b,
            e,
            stop_cycle,
            self.sample_ndx
        );

        // Create the "ghost" tick
        // let new_ic_h = self.oscillator.oscillator(dx,self.ic_h,self.ic_v0,self.ic_voltage);
        // let new_ic_v0 = self.oscillator.oscillator_dydx(dx as f32,self.ic_h,self.ic_v0,self.ic_voltage);
        // self.ic_h = new_ic_h;
        // self.ic_v0 = new_ic_v0;
        // self.last_tick = stop_cycle;

        // That's the difference with the rest of the code: we don't
        // update these:
        // self.tick_ndx += 1;
        // self.ic_voltage = new_ic_voltage;
        self.invariant();
    }

    fn add_tick(&mut self, tick_in: u64, recur: bool) {
        let (b, e) : (u64,u64) = self.current_sample_bounds();
        trace!(
            "add_tick on tick_in:{} zero_tick:{} last_tick:{} recur:{} [{},{}]",
            tick_in,
            self.zero_tick,
            self.last_tick,
            recur,
            b,
            e
        );

        self.invariant();

        // if self.zero_tick == 0 {
        //     debug!("Zero tick on {}", tick_in);
        //     self.zero_tick = tick_in;
        // }

        let tick = (tick_in - self.zero_tick) as u64;
        let last_tick_ndx = self.tick_ndx; // In fact it is: -1+1 (but -1 is dangerous on usize)

        // FIXME Thse f32 will wrap around sooner or later
        // Make sure to reset ticks to zero from time to
        // time

        // Compute h based on last tick
        assert!(tick > self.last_tick);
        let dx = tick - self.last_tick;

        if b <= tick && tick < e {
            // The new tick is inside the current sample (= 1/44100 of a second)

            let h = self
                .oscillator
                .oscillator_at(dx, self.ic_h, self.ic_v0, self.ic_voltage);

            // We're inside the sample so we produce
            // only a part of the sample
            let weight_in_subs = if b <= self.last_tick && self.last_tick < e {
                // The tick is preceded by the last tick, both are in
                // the current sample.
                tick - self.last_tick
            } else {
                // The tick is the first in the current sample
                tick - b
            };
            self._subs
                .push((h, weight_in_subs as f32 / self.cycles_in_sample()));

            // Establish the new wave parameters based on the
            // current tick.
            let new_ic_h = h;
            let new_ic_voltage = voltage(
                self.tick_ndx,
                tick - self.last_tick,
                self.oscillator.cycles_per_sample,
            );
            let new_ic_v0 =
                self.oscillator
                    .oscillator_dydx(dx as f32, self.ic_h, self.ic_v0, self.ic_voltage);

            self.last_tick = tick;
            self.tick_ndx += 1;
            self.ic_h = new_ic_h;
            self.ic_v0 = new_ic_v0;
            self.ic_voltage = new_ic_voltage;
        } else {
            assert!(recur == false);
            // The new tick is past the current sample

            // First, complete the current sample.
            if self._subs.len() >= 1 {
                self.finish_current_sample();
                self.next_sample();
            }

            // ..... Step 2 ........................................
            // Do "full samples"
            let stop_cycle = tick;
            self.oscillate_at_samples(stop_cycle);

            let (b, e) = self.current_sample_bounds();
            trace!("Full sample next bounds: [{}, {}], tick={}", b, e, tick);
            assert!(
                b <= tick && tick < e,
                "After full samples fill up, I need: tick:{} in [{},{}]",
                tick,
                b,
                e
            );

            // Finally, add the brand new tick.
            self.add_tick(tick_in, true)
        }
        self.invariant();
    }
}

pub struct SpeakerRenderState {
    cycle: u64,
    pub tension: bool,
    pub sound: Arc<Mutex<VecDeque<i16>>>,
    events: VecDeque<ClockEvent>,
    target_freq: usize,
    is_recording: bool,
    pub recorded_ticks: Vec<u64>,

    record_start_cycle: u64,
    oscillo: OscillatorSampler,
}

impl SoundRenderer for SpeakerRenderState {
    fn record(&mut self, event: ClockEvent) {
        self.events.push_front(event)
    }

    fn samples(&self) -> Arc<Mutex<VecDeque<i16>>> {
        Arc::clone(&self.sound)
    }

    fn process_frame(&mut self) -> SamplesFrame {
        assert!(
            self.target_freq % FRAMES_PER_SECOND == 0,
            "Rounding will trigger problems"
        );
        // See comment in the sound CPAL thread code
        let spf = self.target_freq / FRAMES_PER_SECOND;
        debug!("process frame: spf={}", spf);

        // Iterate over the "ticks".
        while !self.events.is_empty() {
            let next_cpu_event = self.events.back().unwrap();
            let next_cpu_event_tick = next_cpu_event.tick;
            self.events.pop_back();
            self.oscillo.add_tick(next_cpu_event_tick, false);

            // Is recording set up ?
            if self.is_recording {
                self.recorded_ticks
                    .push(next_cpu_event_tick - self.record_start_cycle);
            }
        }

        // All pending ticks have been processed.
        // Now we generat the samples for the frame.
        if self.oscillo.samples.len() < spf {
            // There's not enough samples generated but we know
            // we have still a bit of the to generate. That's what
            // we do here. The "ghost" tick a trick to generate the
            // the missing sample based on the last tick up to the
            // end of this frame's samples.
            debug!(
                "processing: padding {} samples / {}",
                self.oscillo.samples.len(),
                spf
            );
            self.oscillo
                .ghost_tick_at(self.cycle + CYCLES_PER_FRAME as u64);
            trace!(
                "after ghost tick {} samples / {}",
                self.oscillo.samples.len(),
                spf
            );
        }

        // Go to next frame.
        self.cycle = self.cycle + CYCLES_PER_FRAME as u64;
        return self.oscillo.extract_samples_with_padding(spf);

        /*if self.oscillo.samples.len() < spf {
            let mut c_left: Vec<i16> = self.oscillo.samples.iter().map(|&e| (e*1000.0) as i16).collect();
            c_left.resize(spf - c_left.len(), 0);
            let c_right = c_left.clone();
            self.oscillo.samples.clear();
            return SamplesFrame::new(c_left, c_right);
        }

        // We didn't have enough ticks
        let mut c_left: Vec<i16> = self.oscillo.samples.iter().map(|&e| (e*1000.0) as i16).collect();
        debug!("resize: spf:{} c_left:{}",spf, c_left.len());
        c_left.resize(spf - c_left.len(), 0);
        let c_right = c_left.clone();
        self.oscillo.samples.clear();
        return SamplesFrame::new(c_left, c_right);*/
    }

    /* fn process_frame(&mut self) -> SamplesFrame {
        // FIXME Sort this comment out
        // This is awkward. Normally I'd like my samples to
        // be i8. But when I want to write the data to a file
        // the write functions expect u8. So I have to cast.
        // Unfortunately, casting vector is unsafe in rust
        // because the compiler can't guarantee that the
        // representation of a vector allows it be read as u8
        // if it was defined as i8... One can go around that (transmute)
        // but it's unsafe...
        // https://doc.rust-lang.org/std/vec/struct.Vec.html#method.from_raw_parts Highly unsafe!
        // So in the end, I'll put everything into a u8

        // This is very well explained here : https://stackoverflow.com/questions/48308759/how-do-i-convert-a-vect-to-a-vecu-without-copying-the-vector/55081958#55081958

        // let mut buffer = self.sound.lock().unwrap();
        // let buffer:&mut VecDeque<i16> = &mut self.output_queues.lock().expect("Lock must work").sound;
        let mut buffer: Vec<i16> = Vec::new();

        // See comment in the sound CPAL thread code
        let spf = self.samples_per_frame;

        // Tension is checked to be sure we can activate
        // the optimisation (consisting in not filling
        // the buffer)

        if self.events.is_empty() && self.tension {
            // If the tension is high, we cannot return an
            // empty buffer (because empty buffer is understood
            // as a buffer filled with zeros, not ones).
            // So, if we happen to have no sound but some
            // tension, we fade the tension out slowly
            // to zero. So, on the next buffer, if tension
            // is still off, then optimization (buffer filled
            // with zeros) will be performed.

            let mut s = i16::MAX;
            for _i in 0..spf {
                buffer.push(s);
                s = s - i16::MAX / (spf as i16);
            }
            self.tension = false;

            // if self.is_recording {
            //     self.recorded_ticks
            //         .push(self.cycle - self.record_start_cycle);
            // }
        } else if !self.events.is_empty() || self.tension {
            // Help the allocator a bit (FIXME But does it really help ?)
            buffer.reserve(spf);

            let mut current_cycle = self.cycle;
            let mut filled: usize = 0;

            while !self.events.is_empty() {
                let next_cpu_event = self.events.back().unwrap();
                let next_cpu_event_tick = next_cpu_event.tick;

                // Recording
                //let ofs_min = 10_500_000; // Archon
                // let ofs_min = 95_000_000;// Wind walker
                // let ofs_max = 140_000_000;
                // let ofs_min = 9_000_000; // Sea Dragon
                // let ofs_max = 16_000_000;
                // let ofs_min = 7_000_000; // Miami 3
                // let ofs_max = 114_000_000;
                // let ofs_min = 60_000_000; // Miami 3
                // let ofs_max = 120_000_000;
                // let ofs_min = 7_000_000; // Boulder Dash
                // let ofs_max = 29_000_000;
                // let ofs_min = 31_000_000; // Conan
                // let ofs_max = 106_000_000;
                // let ofs_min = 22_000_000; // Impossible mission
                // let ofs_max = 77_000_000;

                // Is recording set up ?
                if self.is_recording {
                    self.recorded_ticks
                        .push(next_cpu_event_tick - self.record_start_cycle);
                }

                assert!(
                    next_cpu_event.tick >= current_cycle,
                    "The next event we'll see is not in the future. current_cycle:{}, event cycle:{}",
                    current_cycle,
                    next_cpu_event.tick);

                filled += fill_samples(
                    &mut buffer,
                    current_cycle,
                    next_cpu_event_tick,
                    spf,
                    tension_to_sample(self.tension),
                );

                // When reading/writing the SPEAKER i/o address, the
                // tension is switched
                self.tension = !self.tension;
                current_cycle = next_cpu_event_tick;
                self.events.pop_back();
            } // while

            if current_cycle - self.cycle < CYCLES_PER_FRAME as u64 {
                filled += fill_samples(
                    &mut buffer,
                    current_cycle,
                    self.cycle + CYCLES_PER_FRAME as u64,
                    spf,
                    tension_to_sample(self.tension),
                );
            }

            assert!(filled == spf);
        }

        self.cycle = self.cycle + CYCLES_PER_FRAME as u64;

        debug!(
            "Sound buffer len={} samples, SPF={}, capa events={}",
            buffer.len(),
            spf,
            self.events.capacity()
        );
        let c = buffer.clone(); // FIXME Perforance hit ! Improve SamplesFrame to support Mono correctly
        return SamplesFrame::new(buffer, c);
    } */
}

impl SpeakerRenderState {
    pub fn new(
        target_freq: usize, // Hertz (usually 44100 or 48000
        output_queues: Arc<Mutex<OutputQueues>>,
    ) -> SpeakerRenderState {
        SpeakerRenderState {
            cycle: 0,
            tension: false,
            sound: Arc::new(Mutex::new(VecDeque::new())),
            events: VecDeque::new(),
            target_freq,
            is_recording: false,
            recorded_ticks: Vec::new(),
            record_start_cycle: 0,
            oscillo: OscillatorSampler::new(target_freq),
        }
    }

    pub fn start_recording(&mut self) {
        self.is_recording = true;
        self.recorded_ticks.clear();
        self.record_start_cycle = self.cycle;
    }

    pub fn stop_recording(&mut self) {
        self.is_recording = false;
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const TARGET_FREQ: usize = 44100;
    const CYCLES_PER_SAMPLE: f64 = (FRAMES_PER_SECOND * CYCLES_PER_FRAME) as f64 / (TARGET_FREQ as f64) ; // about 21.125

    #[test]
    fn test() {
        let mut sho = OscillatorSampler::new(TARGET_FREQ);
        sho.add_tick(1, false);
        sho.add_tick(2300, false);
        sho.add_tick(2300 * 2, false);
        const CYCLES_PER_SAMPLE: f64 = (FRAMES_PER_SECOND * CYCLES_PER_FRAME) as f64 / (TARGET_FREQ as f64) ; // about 21.125
        println!("Samples in OscSampler: {}", sho.samples.len());
        let frame = sho.extract_samples_with_padding((2300.0 * 2.0 / CYCLES_PER_SAMPLE) as usize);
        println!(
            "Samples in OscSampler: {}, Frame length {}",
            sho.samples.len(),
            frame.left.len()
        );
        for i in frame.left {
            print!("{} ", i);
        }
        println!()
    }

    #[test]
    fn test2() {
        let mut sho = OscillatorSampler::new(TARGET_FREQ);

        sho.add_tick(1, false);
        assert!(sho.samples.len() == 0,"The number of completed samples should be 0 as we're still inside the first sample");
        println!("Samples in OscSampler: {}, cycles per sample:{}", sho.samples.len(), CYCLES_PER_SAMPLE);
        let frame = sho.extract_samples_with_padding(3);
        println!("Frame length {}", frame.left.len());
        assert!(frame.left.len() == 3, "padding should work");

        sho.add_tick(23, false);
        assert!(sho.samples.len() == 1,"The number of completed samples should be 1");
        println!("Samples in OscSampler: {}, cycles per sample:{}", sho.samples.len(), CYCLES_PER_SAMPLE);
        let frame = sho.extract_samples_with_padding(3);
        println!("Frame length {}", frame.left.len());
        assert!(frame.left.len() == 3, "padding should work, it is {}", frame.left.len());

        sho.add_tick(44, false);
        assert!(sho.samples.len() == 1,"The number of completed samples should be 1 (45 cycles is the limit)");
        println!("Samples in OscSampler: {}, cycles per sample:{}", sho.samples.len(), CYCLES_PER_SAMPLE);
        let frame = sho.extract_samples_with_padding((24.0 / CYCLES_PER_SAMPLE) as usize);
        println!("Frame length {}", frame.left.len());
        assert!(frame.left.len() == 3, "padding should work");

    }


    #[test]
    fn test3() {
	let tgt_freq = CYCLES_PER_SECOND;
        let mut sho = OscillatorSampler::new(tgt_freq); //TARGET_FREQ);
        sho.add_tick(1, false);
        sho.add_tick(50, false);
        sho.add_tick(5000, false);
        let cycles_per_sample_var: f64 = (FRAMES_PER_SECOND * CYCLES_PER_FRAME) as f64 / (tgt_freq as f64) ; // about 21.125
        println!("Samples in OscSampler: {}", sho.samples.len());
        let frame = sho.extract_samples_with_padding((200.0 * 2.0 / cycles_per_sample_var) as usize);
        println!(
            "Samples in OscSampler: {}, Frame length {}",
            sho.samples.len(),
            frame.left.len()
        );
        for i in frame.left {
            print!("{},", i);
        }
        println!()
    }

    #[test]
    fn test4() {
	let tgt_freq = CYCLES_PER_SECOND;
        let mut sho = Oscillator::new(1.0f32);

	for i in 0..1000 {
	    //     fn oscillator_at(&self, x_in: u64, ic_h: f32, ic_v0: f32, equilbrium_position: f32) -> f32 {

	    let y = sho.oscillator_at(i, 0.0, 1.0, 1.0);
	    print!("{:.1},", y);
	}

    }

}
