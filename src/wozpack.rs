use log::{debug, trace};
use std::usize;

use crate::a2::CYCLES_PER_SECOND;
use crate::lss::LSS;
use crate::stepper2;
use crate::wozfile::{usec_str, SignalPart, TrackData, TrackDataType, WozFile, FLOPPY_ROTATION_TIME, NANOSEC125, ONE_MICRO_SEC, STABLE_READ_DURATION};



pub fn subtrack_str(subtrack: Option<usize>) -> String {
    if let Some(subtrk) = subtrack {
        format!(
            "{:02} {}",
            subtrk / 4,
            String::from(" ¼½¾")
                .chars().nth(subtrk % 4)
                .unwrap()
        )
    } else {
        "between".to_string()
    }
}


pub struct Controller {
    pub woz_file: WozFile,
	// None means that the read head is between two tracks
    current_subtrack: Option<usize>,
    pub track_bit_index: usize,
    lss: LSS,
    signal_current_time: f64,
    current_signal_part: usize,
    rand2: usize,
    pub stepper: stepper2::DiskStepper,
    _stepper_cycle: u64,
    read_write_switch: u8,
    shift_load_switch: u8,
    last_lss_data: [u8; 22],
    signal_current_pos: usize,
    signal_no_transition: f64,
    signal_current_time2: f64,
}


impl Controller {
    pub fn new() -> Controller {
        let mut c = Controller {
            woz_file: WozFile::new(),
            current_subtrack: Some(0),
            track_bit_index: 0,
            lss: LSS::new(),
            signal_current_time: 0.0,
            current_signal_part: 0,
            rand2: 0,
            stepper: crate::stepper2::DiskStepper::new(),
            _stepper_cycle: 0,
            read_write_switch: 0,
            shift_load_switch: 0,
            last_lss_data: [0; 22],
            signal_current_pos: 0,
            signal_no_transition: 0.0,
            signal_current_time2: 0.0,
        };
        c.lss.set_write_protect(false);
        c
    }

    pub fn reset(&mut self) {
        self.track_bit_index = 0;
        self.signal_current_time = 0.0;
        self.current_signal_part= 0;
        self._stepper_cycle = 0;
    }

    pub fn current_subtrack(&self) -> Option<usize> {
        self.current_subtrack
    }

    pub fn set_magnet(&mut self, magnet_ndx: usize, charge: bool, cycle: u64) {
        self.stepper.set_magnet(magnet_ndx, charge, cycle);
    }

    pub fn complete_current_stepper_eval(&mut self, cycle: u64) {
        // trace!("complete_current_stepper_eval: self._stepper_cycle={} cycle={}  " , self._stepper_cycle,cycle);

        if cycle == self._stepper_cycle {
            // Nothing to do
            trace!("Nothing to do because no time to do anything.");
        } else {
            assert!(cycle > self._stepper_cycle, "{} not > {}", cycle, self._stepper_cycle);

            let track = self.stepper.closest_quarter_track();

            self.stepper.advance(
                self._stepper_cycle,
                (cycle - self._stepper_cycle) as f32 / CYCLES_PER_SECOND as f32 );

            if self.stepper.closest_quarter_track() != track {
                self.change_subtrack();
            }
        }
    }

    pub fn prepare_next_stepper_eval(&mut self, cycle: u64) -> u64 {
        // trace!("prepare_next_stepper_eval: self._stepper_cycle={} cycle={}", self._stepper_cycle, cycle );
        assert!(cycle >= self._stepper_cycle);
        self._stepper_cycle = cycle;
        let ttne = self.stepper.time_to_next_eval() * CYCLES_PER_SECOND as f32;
        assert!( ttne >= 1.0);
        // trace!("prepare_next_stepper_eval: time_to_next_eval={:.3} at cycle {}",ttne,cycle + ttne as u64);
        cycle + ttne as u64
    }


    fn shift_last_lss_data_for_debug(&mut self) {
        for i in 0..self.last_lss_data.len() - 1 {
            self.last_lss_data[i] = self.last_lss_data[i + 1];
        }
        self.last_lss_data[self.last_lss_data.len() - 1] = self.lss.data_register();
    }

    pub fn lss_debug(&self) -> String {
        format!("{}, {}", self.lss.data_register(), self.lss.state())
    }

    fn bit_to_write(&self) -> Option<bool> {
        if self.read_write_switch /* aka Q7 */ == 1 {

            // UtAe p 9-23: "When State 8 is entered from State 7, the WRITE
            // signal switches from low to high. Why? Because the WRITE
            // signal is connected to the A7 input of the sequencer ROM. The
            // sequencing address bits are A7 A6 A0 A5 so A7 is low in
            // states 0—7 and high in states 8—F"

            let write_time = self.lss.sequence_number() & 0b0111;
            // println!("bit_to_write: write_time={}",write_time);

            if write_time == 0 {
                // UtAe p 9-15: "During both reading and writing, the data
                // register is shifted left while its most significant bit, QA,
                // is monitored. In writing, the state of QA is monitored and
                // the WRITE SIGNAL is toggled at the bit writing interval when
                // QA is set."
                let qa = (self.data_register() & 0x80) >> 7;
                // println!("bit_to_write: qa={}",qa);

                return Some(qa == 1);
            }
        }
        None
    }

    pub fn tick_lss_one_microsecond(&mut self, data_on_bus: u8) {
        /// SATHER: The LSS runs at twice the CPU speed
        const STEP: f64 = 1.0 / (CYCLES_PER_SECOND as f64) * 0.5;
        //let step = 4.0*NANOSEC125;

        // Since the LSS operates at 2MHz, we tick it twice during one
        // microsecond.

        for _ in 0..2 {
            let read_pulse = self.mc3470_sense_and_advance2(STEP);
            //let read_pulse = self.mc3470_sense_and_advance(STEP);
            // assert!(read_pulse == read_pulse2, "Diff at {} - {}, track {}",
            //     (self.signal_current_pos as f64)*NANOSEC125,
            //     self.signal_current_time,
            //     self.current_subtrack.unwrap_or(0xFFFF));

            self.lss
                .tick(self.read_write_switch, self.shift_load_switch, read_pulse,
                    data_on_bus);

            if let Some(track_num) = self.current_subtrack {
                if let Some(bit_to_write) = self.bit_to_write() {

                    let mut pos = self.signal_current_pos;
                    self.woz_file.write_at(track_num, pos, bit_to_write);
                    pos += 1;

                    for _ in 0..((ONE_MICRO_SEC/NANOSEC125*4.0).round() as usize - 1){
                        self.woz_file.write_at(track_num, pos, false);
                        pos += 1;
                    }
                } else /* if self.read_write_switch != 0 */ {
                    //println!("skipping at {}", self.signal_current_pos);
                }
            }

            self.shift_last_lss_data_for_debug();
        }

    }

    pub fn reset_lss(&mut self) {
        self.lss.reset()
    }

    pub fn set_read_write_switch(&mut self, rw: u8) {
        self.read_write_switch = rw;
    }

    pub fn set_shift_load_switch(&mut self, sl: u8) {
        self.shift_load_switch = sl;
    }

    fn default_track_duration(&self, track: Option<usize>) -> f64 {
        // Returns the duration (in seconds) of a track if there's one. If
        // there's no track ('cos no disk is in the drive or because the WOZ
        // file has none), then we return a default duration (floppy rotation is
        // 300rpm so that's about 5 rotations per seconds => 0.2 seconds for a
        // rotation)

        if let Some(t) = track {
            if let Some(d) = self.woz_file.track_data(t) {
                return d.flux_duration;
            }
        }
        FLOPPY_ROTATION_TIME
    }

    fn bit_track_duration_and_length(&self, subtrack: &Option<usize>) -> (f64, usize) {
        let (track_duration, track_len) = if subtrack.is_none()
            || self
                .woz_file
                .track_data(subtrack.unwrap())
                .is_none()
        {
            let empty_len =  (FLOPPY_ROTATION_TIME/NANOSEC125).round();
            assert!(empty_len as usize== 1_600_000, "Bad const {} != {}", empty_len, empty_len as usize);
            (FLOPPY_ROTATION_TIME, empty_len as usize)
        } else {
            let track_data = &self
            .woz_file
            .track_data(subtrack.unwrap())
            .unwrap();

            ((track_data.flux_as_bits_len() as f64) * NANOSEC125, track_data.flux_as_bits_len())
        };
        (track_duration, track_len)
    }

    fn change_subtrack(&mut self) {
        let dest_subtrk = self.stepper.closest_quarter_track();

        debug!("change_subtrack(): before {}/{} qter trk:{}",
            self.signal_current_pos,
            self.default_track_duration(self.current_subtrack()),
            self.current_subtrack().unwrap_or_default());
        self.signal_current_pos = (self.signal_current_pos as f64
            / self.default_track_duration(self.current_subtrack())
            * self.default_track_duration(dest_subtrk)) as usize;
        debug!(" after {}/{}, qter trk:{} ",self.signal_current_pos, self.default_track_duration(dest_subtrk), dest_subtrk.unwrap_or_default());

        let (src_bit_track_duration, _src_bit_track_len) = self.bit_track_duration_and_length(&self.current_subtrack());
        let (dest_bit_track_duration, _dest_bit_track_len) = self.bit_track_duration_and_length(&dest_subtrk);

        debug!(
            "change_subtrack(): Go from track {} to track {}",
            subtrack_str(self.current_subtrack),
            subtrack_str(dest_subtrk)
        );
        debug!("From qter trk:{} {:.6}/{:.6} {:.2}% ",
            self.current_subtrack().unwrap_or_default(),
            self.signal_current_time2, src_bit_track_duration,
            100.0*self.signal_current_time2 / src_bit_track_duration);
        self.signal_current_time2 = self.signal_current_time2
            / src_bit_track_duration
            * dest_bit_track_duration;
        debug!("... to qter trk:{} {:.6}/{:.6}",
            dest_subtrk.unwrap_or_default(), self.signal_current_time2, dest_bit_track_duration);


        // if self.current_subtrack() == dest_subtrk {
        //     debug!("change_subtrack(): already at track {}",subtrack_str(self.current_subtrack()));
        //     return
        // }

        // Convert the time position from the current track to the new one
        // Keep in mind that any of the two (maybe both) can be an empty
        // track and thus needs a default duration

        self.signal_current_time = self.signal_current_time
            / self.default_track_duration(self.current_subtrack())
            * self.default_track_duration(dest_subtrk);
        debug!(
            "change_subtrack(): current time at destination track={}",
            usec_str(self.signal_current_time)
        );

        if dest_subtrk.is_none() {
            self.current_subtrack = dest_subtrk;
            debug!("change_subtrack(): read head between two tracks or no floppy");
            return;
        }

        let z = self.woz_file.track_data(dest_subtrk.unwrap());
        if z.is_none() {
            self.current_subtrack = dest_subtrk;
            debug!(
                "change_subtrack(): reached empty track {}",
                subtrack_str(self.current_subtrack())
            );
            return;
        }
        let track_data = &z.unwrap();

        // Binary search
        let mut start = 0;
        let mut end = track_data.flux.len() - 1;

        let dest_track_last_time = track_data.flux.last().unwrap().end();
        if self.signal_current_time >= dest_track_last_time {
            // The current time is after the last sig_part of the
            // destination track.

            self.signal_current_time -= dest_track_last_time;
        }

        trace!("Binary search starts");
        loop {
            assert!(start <= end, "binary search invariant failed");
            trace!("start:{} end:{}", start, end);
            let ndx = (start + end) / 2;
            let sig_part: &SignalPart = &track_data.flux[ndx];

            if sig_part.contains(self.signal_current_time) {
                debug!(
                    "change_subtrack(): binary search resolved to sig part {}: time={} duration={}",
                    ndx,
                    usec_str(sig_part.time),
                    usec_str(sig_part.duration)
                );
                self.current_signal_part = ndx;
                break;
            } else {
                trace!("sig_part index {}/{} doesn't contain time:{:.5}; ({:.5},{:.5})",
                    ndx, end,
                    self.signal_current_time,
                    sig_part.time, sig_part.end());
            }

            if self.signal_current_time >= sig_part.end() {
                start = ndx+1;
            } else {
                end = ndx;
            }
        }

        trace!("Binary search complete");
        self.current_subtrack = dest_subtrk;


        // Make sure the binary search works as expected.
        let sig_part: &SignalPart = &track_data.flux[self.current_signal_part];
        assert!(sig_part.contains(self.signal_current_time), "{} <= {} ({:.1} cycles) < {}",
            sig_part.time, self.signal_current_time,
            self.signal_current_time * CYCLES_PER_SECOND as f64,
            sig_part.time + sig_part.duration);

        //self.woz_file.explain_subtrack_data(self.current_subtrack.unwrap());
    }

    pub fn status_str(&self) -> String {

        let current_track = if let Some(subtrack) = self.current_subtrack() {
		        self.woz_file.track_data(subtrack)
            } else {
                None
            };

        let mut track_bits = "".to_string();

        if let Some(track) = current_track {
            let mut pos = self.signal_current_pos;
            const N:usize = 60;
            if pos >= N { pos-= N } else {pos = track.flux_as_bits_len() - 1 - pos};
            for i in 0..2*N {
                if i == N {track_bits.push(' ')};
                 track_bits.push_str(if track.signal_at(pos) {"|"} else {"o"} );
                 if pos < track.flux_as_bits_len()-1 {
                     pos+= 1;
                 } else {
                     pos = 0;
                 }
             }
        }

        format!("trk:{}{} t:{} mags:{} LSS data={:08b} state={:02X} R/W:{} S/L:{} WriteProtect:{}, flux:{} LSS.data:{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X};{:02X}. Signal part:{} {} BitPos:{:7}",
		subtrack_str(self.stepper.closest_quarter_track()),
		if self.current_subtrack().is_some() &&
		    self.woz_file.track_data(self.current_subtrack().unwrap()).is_some() {
		    let track_data = &self.woz_file.track_data(self.current_subtrack().unwrap()).unwrap();
		    match track_data.track_type {
			TrackDataType::Flux => "F",
			TrackDataType::Standard => "S",
			TrackDataType::Unknown => "?",
		    }
		} else {
		    " "
		},
		usec_str(self.signal_current_time),
		self.stepper.magnets_to_string(),
        self.lss.data_register(),
        self.lss.state(),
        if self.read_write_switch == 0 {"R"} else {"W"},
        if self.shift_load_switch == 0 {"S"} else {"L"},
        self.lss.write_protect(),
        track_bits,
		self.last_lss_data[0],
		self.last_lss_data[1],
		self.last_lss_data[2],
		self.last_lss_data[3],
		self.last_lss_data[4],
		self.last_lss_data[5],
		self.last_lss_data[6],
		self.last_lss_data[7],
		self.last_lss_data[8],
		self.last_lss_data[9],
		self.last_lss_data[10],
		self.last_lss_data[11],
		self.last_lss_data[12],
		self.last_lss_data[13],
		self.last_lss_data[14],
		self.last_lss_data[15],
		self.last_lss_data[16],
		self.last_lss_data[17],
		self.last_lss_data[18],
		self.last_lss_data[19],
		self.last_lss_data[20],
		self.last_lss_data[21],
		self.current_signal_part,
		if self.current_subtrack().is_some() &&
            self.woz_file.track_data(self.current_subtrack().unwrap()).is_some() {
                let track_data = &self.woz_file.track_data(self.current_subtrack().unwrap()).unwrap();
                let sig_part: &SignalPart = &track_data.flux[self.current_signal_part];
                format!("{} {}", usec_str(sig_part.time), usec_str(sig_part.duration))
            } else {
                "".to_string()
            },
        self.signal_current_pos
	)
    }

    // Used for tests
    fn _current_flux(&self) -> &TrackData {
        self.woz_file
            .track_data(self.current_subtrack().unwrap())
            .unwrap()
    }

    // Shame on rust, I have to do it this way to have incompatbile borrows.
    fn get_rand(rand: usize) -> usize {
        rand.wrapping_add(137987317).rotate_left(3)
    }


    fn mc3470_sense_and_advance2(&mut self, duration: f64) -> bool {

        if let Some(subtrack) = self.current_subtrack() {
            if subtrack >= 11133*4 {
                trace!("mc3470_sense_and_advance2: duration={} self.signal_current_pos={} self.signal_current_time2={}",
                    duration,
                    self.signal_current_pos,
                    self.signal_current_time2);
            }
        }


        let (track_duration, track_len) = self.bit_track_duration_and_length(&self.current_subtrack());
        self.signal_current_pos = (self.signal_current_time2 / NANOSEC125).floor() as usize;

        assert!(self.signal_current_pos < track_len, "{} < {} at track {}, t:{:.6}", self.signal_current_pos,  track_len,
            self.current_subtrack().unwrap_or_default(),
            self.signal_current_time2);

        self.signal_current_time2 += duration;
        if self.signal_current_time2 >= track_duration {
            self.signal_current_time2 -= track_duration
        }


        // -------------------------------------------------------------
        // Blank track
        // -------------------------------------------------------------

        let result = if self.current_subtrack().is_none()
            || self
                .woz_file
                .track_data(self.current_subtrack().unwrap())
                .is_none()
        {
            self.rand2 = Controller::get_rand(self.rand2);
            self.rand2 & (512 + 128 + 64) != 0
        } else {

            let track_data = &self
                .woz_file
                .track_data(self.current_subtrack().unwrap())
                .unwrap();

            let mut transition = false;

            let mut seek_pos = self.signal_current_pos;
            for _ in 0..7 { // 1 µsec
                transition |= track_data.signal_at(seek_pos);
                if transition {
                    break
                }
                if seek_pos > 0 {
                    seek_pos -= 1;
                } else {
                    seek_pos = track_data.flux_as_bits_len() - 1;
                }
            }

            if !transition {
                self.signal_no_transition += duration;
                if self.signal_no_transition > STABLE_READ_DURATION*0.9 {
                    //if self.current_subtrack().unwrap_or_default() == 24 && self.signal_no_transition >= STABLE_READ_DURATION*0.9 {
                        trace!("fake {} at {} ({})", self.signal_no_transition, self.signal_current_time2, STABLE_READ_DURATION);
                    //}
                        self.rand2 = Controller::get_rand(self.rand2);
                    transition = self.rand2 & (512 + 128 + 64) != 0
                }
            } else {
                self.signal_no_transition = 0.0;
            }

            transition
        };

        result
    }

    pub fn set_woz(&mut self, woz: WozFile) {
        self.lss.set_write_protect(woz.write_protected);
        self.woz_file = woz;
    }

    pub fn data_register(&self) -> u8 {
        /*
        Give data_register value at cycle 'cycle'.

        There are several things to take into account:
        - the drive motor may be on or off
        - the read head may be moving or not
        - the subtrack may be empty or not
         */

        self.lss.data_register()
    }

    // Used in tests
    fn _is_qa_set(&mut self) -> bool {
        // When bit set, data on the data register is valid.
        self.lss.data_register() & 0x80 != 0
    }
}



#[cfg(test)]
mod tests {
    //use super::*;

    // use crate::wozfile::{load_disk, NANOSEC125, ONE_MICRO_SEC};
    // use std::path::Path;
    // use colored::Colorize;


    // fn decode(b1: u8, b2: u8) -> u8 {
    //     ((b1 & !0xAA) << 1) | (b2 & (!0xAA))
    // }

    // fn read_8bits(mut cycle: u64, controller: &mut Controller) -> (u64, u8) {
    //     // Wait for the data register to be ready with the
    //     // 8 next bits. Return these.

    //     let mut limit = 64;

    //     let mut b = controller.data_register();
    //     cycle += 1;

    //     // Finish current bits (once they are ready, the data register
    //     // holds them for some time).
    //     while controller.is_qa_set() {
    //         b = controller.data_register();
    //         cycle += 1;

    //         limit -= 1;
    //         if limit == 0 {
    //             assert!(false, "Limit exhausted");
    //         }
    //     }

    //     // Wait for data register to be loaded with next 8 bits.
    //     while !controller.is_qa_set() {
    //         b = controller.data_register();
    //         cycle += 1;

    //         limit -= 1;
    //         if limit == 0 {
    //             assert!(false, "Limit exhausted");
    //         }
    //     }

    //     // Read new nibble
    //     let r = b;
    //     trace!("NIBBLE! Cycle:{:6} ${:02X}", cycle, b);
    //     // Wait until the data register has finished holding
    //     // the 8 bits it presented to us.

    //     /* while controller.is_qa_set() {
    //         b = controller.data_register(cycle);
    //         cycle += 1;

    //         limit -= 1;
    //         if limit == 0 {
    //         assert!(false, "Limit exhausted");
    //         }
    //     }; */
    //     (cycle, r)
    // }

    // fn read_to_sector(mut cycle: u64, controller: &mut Controller) -> (u64, u8, u8) {
    //     loop {
    //         let (c, n) = read_8bits(cycle, controller);
    //         cycle = c;
    //         if n == 0xD5 {
    //             let (c, n) = read_8bits(cycle, controller);
    //             cycle = c;
    //             if n == 0xAA {
    //                 let (c, n) = read_8bits(cycle, controller);
    //                 cycle = c;
    //                 if n == 0x96 {
    //                     /*    let address_block: [u8; 8] = [
    //                     ((vol >> 1) | 0xaa), (vol | 0xaa),
    //                     ((trk >> 1) | 0xaa), (trk | 0xaa),
    //                     ((sector >> 1) | 0xaa), (sector | 0xaa),
    //                     ((chksum >> 1) | 0xaa), (chksum | 0xaa)
    //                     ];
    //                      */

    //                     let (cycle, _vol1) = read_8bits(cycle, controller);
    //                     let (cycle, _vol2) = read_8bits(cycle, controller);
    //                     let (cycle, trk1) = read_8bits(cycle, controller);
    //                     let (cycle, trk2) = read_8bits(cycle, controller);
    //                     let (cycle, sec1) = read_8bits(cycle, controller);
    //                     let (cycle, sec2) = read_8bits(cycle, controller);

    //                     return (cycle, decode(trk1, trk2), decode(sec1, sec2));
    //                 }
    //             }
    //         }
    //     }
    // }

    /*
    #[test]
    fn small_track_change() {
    let mut c = Controller::new();
    c.woz_file = load_disk(Path::new("conan.dsk"));

    for i in 0..10 {
        println!("{}", c.woz_file.track_map[i]);
    }

    c.current_subtrack = 3;
    let mut cycle = 1;
    let (c2, trk, sect) = read_to_sector(cycle, &mut c);
    cycle = c2;
    println!("Trk:{}, Sect:{}, Cycle:{} Bit index:{}", trk, sect, cycle, c.track_bit_index);
    assert!(c.current_subtrack == 3);

    c.begin_head_move(cycle, 1);
    cycle += 1;
    c.end_head_move();
    cycle += 1;

    let (c2, trk, sect) = read_to_sector(cycle, &mut c);
    cycle = c2;
    println!("Trk:{}, Sect:{}, Cycle:{} Bit index:{}", trk, sect, cycle, c.track_bit_index);
    assert!(c.current_subtrack == 4);

    c.begin_head_move(cycle, -1);
    cycle += 1;
    c.end_head_move();
    cycle += 1;

    let (c2, trk, sect) = read_to_sector(cycle, &mut c);
    cycle = c2;
    println!("Trk:{}, Sect:{}, Cycle:{} Bit index:{}", trk, sect, cycle, c.track_bit_index);
    assert!(c.current_subtrack == 3);
    }

    #[test]
    fn woz_load() {
    // export RUST_LOG="accurapple::wozpack=trace"
    // RUST_BACKTRACE=1 cargo test woz_load --bin accurapple -- --show-output
    env_logger::init();
    let woz = load_disk(Path::new("data/LOWTECH.WOZ"));
    }

    #[test]
    fn woz_load_flux() {
    // export RUST_LOG="accurapple::wozpack=trace"
    // RUST_BACKTRACE=1 cargo test woz_load --bin accurapple -- --show-output
    env_logger::init();
    let woz = load_disk(Path::new("data/ProDOS User's Disk - Disk 1, Side A.woz"));
    let woz = load_disk(Path::new("additional/LOWTECH.WOZ"));
    woz.track_data(0);
    }

    #[test]
    fn track_change_limits() {
    let mut c = Controller::new();
    c.woz_file = load_disk(Path::new("data/LOWTECH.WOZ"));
    c.current_subtrack = 1;
    let mut cycle = 1;

    for i in 1..(37*4) {
        c.begin_head_move(cycle, 1);
        cycle += 1;
        c.end_head_move();
        cycle += 1;

        if !c.is_current_subtrack_empty() {
        let (c2, trk, sect) = read_to_sector(cycle, &mut c);
        cycle = c2;
        debug!("i:{} Trk:{}, Sect:{}", i, trk, sect);
        }
    }

    // 36*4 = 144
    assert!(c.current_subtrack == DRIVE_SUB_TRACKS-1, "bad current subtrack {}", c.current_subtrack);


    for i in 1..(40*4) {
        c.begin_head_move(cycle, -1);
        cycle += 1;
        c.end_head_move();
        cycle += 1;
        if !c.is_current_subtrack_empty() {
        let (c2, trk, sect) = read_to_sector(cycle, &mut c);
        cycle = c2;
        }
    }

    assert!(c.current_subtrack == 0);
    }

    */

    // struct DRPointer {
    //     value: u8,
    //     offset: usize,
    // }

    // fn dedup(v: &Vec<u8>, start_ndx: usize, max_deduped: usize) -> Vec<DRPointer> {
    //     let mut dedup_data_register: Vec<DRPointer> = Vec::new();
    //     let mut last = v[0] + 1;
    //     let mut i = start_ndx;
    //     while i < v.len() {
    //         let new = v[i];
    //         if new & 0x80 != 0 {
    //             dedup_data_register.push(DRPointer {
    //                 value: new,
    //                 offset: i,
    //             });
    //             if dedup_data_register.len() >= max_deduped {
    //                 break;
    //             }

    //             last = v[i];
    //             while i < v.len() && v[i] == last {
    //                 i += 1
    //             }
    //         } else {
    //             i += 1;
    //         }
    //     }

    //     dedup_data_register
    // }

    // #[test]
    // fn display_test2() {
    //     env_logger::init();
    //     let mut controller = Controller::new();
    //     //controller.woz_file = load_disk(Path::new("additional/conan.dsk"));
    //     //controller.woz_file = load_disk(Path::new("/mnt/data2/roms/Apple/woz test images/WOZ 2.1 (with FLUX chunks)/ProDOS User's Disk - Disk 1, Side A.woz"));
    //     // controller.woz_file = load_disk(Path::new("/mnt/data2/torrents/wozaday_Choplifter/Choplifter (woz-a-day collection)/Choplifter.woz"));
    //     //controller.woz_file = load_disk(Path::new("/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Commando - Disk 1, Side A.woz"));

    //     //controller.woz_file = load_disk(Path::new("/mnt/data2/roms/Apple/Wizardry- Proving Grounds of the Mad Overlord v05-SEP-81 (woz-a-day collection)/Wizardry- Proving Grounds of the Mad Overlord v05-SEP-81 side B - boot.woz"));

    //     // controller.woz_file = load_disk(Path::new("/mnt/data2/roms/Apple/woz test images/FromSlack/Test Drive side A.woz"));
    //     controller.woz_file = load_disk(Path::new(
    //         "/mnt/data2/roms/Apple/woz test images/The School Speller - Tools.woz",
    //     ));

    //     controller.stepper.set_current_subtrack(33.0);
    //     controller.change_subtrack();

    //     let mut data_register_values: Vec<u8> = Vec::new();
    //     let mut flux_values: Vec<bool> = Vec::new();
    //     let mut track_bit_indices: Vec<usize> = Vec::new();
    //     let mut sig_part_indices: Vec<usize> = Vec::new();
    //     let mut sig_part_times: Vec<f64> = Vec::new();

    //     const unit: f64 = 1.0 * NANOSEC125;
    //     const NB_RECORDS: usize = (220_000.0 * ONE_MICRO_SEC / unit) as usize;

    //     println!("Will do {} records", NB_RECORDS);
    //     println!(
    //         "Flux has {} signal parts, lasts {:.6}s",
    //         controller._current_flux().flux.len(),
    //         controller._current_flux().flux_duration
    //     );

    //     let mut rp = false;
    //     for i in 0..NB_RECORDS {
    //         // controller.current_track_nb_bits() / 4
    //         //println!("{}", controller.current_signal_part);
    //         sig_part_indices.push(controller.current_signal_part);
    //         sig_part_times.push(controller.signal_current_time);
    //         track_bit_indices.push(controller.track_bit_index);
    //         //let (rp, dr) = controller.tick_cpu_cycle();
    //         rp = controller.mc3470_sense_and_advance(unit);
    //         //println!("{}   rp={}",i, rp);
    //         flux_values.push(rp);

    //         // 2000000 is LSS' frequency
    //         if i % (((1.0 / 2_000_000.0) / unit).round() as usize) == 0 {
    //             //println!("{}",i);
    //             controller.lss.tick(0, 0, rp, 0);
    //             //rp = false;
    //         }

    //         let dr = controller.data_register(); // 0=dummy cycle
    //         data_register_values.push(dr);
    //     }

    //     let mut data_reg: Vec<u8> = Vec::new();
    //     let mut sig_part_indices_ext: Vec<usize> = Vec::new();
    //     for i in 1..NB_RECORDS {
    //         // Track first time a valid data register is ready.
    //         if data_register_values[i] & 0x80 > 0 && data_register_values[i - 1] & 0x80 == 0 {
    //             data_reg.push(data_register_values[i]);
    //             sig_part_indices_ext.push(sig_part_indices[i]);
    //         }
    //     }

    //     let mut prev_block = 0;
    //     let mut current_sector: Option<usize> = None;
    //     let mut data_block = false;

    //     for i in 0..(data_reg.len() - 3) {
    //         if data_reg[i] == 0xD5 && data_reg[i + 1] == 0xAA && data_reg[i + 2] == 0x96 {
    //             let bytes = &data_reg[i..i + 13];
    //             let vol = decode(bytes[3], bytes[3 + 1]);
    //             let trk = decode(bytes[3 + 2], bytes[3 + 3]);
    //             let sect = decode(bytes[3 + 4], bytes[3 + 5]);
    //             let sum = decode(bytes[3 + 6], bytes[3 + 7]);
    //             let end_mark_de = bytes[11];
    //             let end_mark_aa = bytes[12];

    //             current_sector = Some(sect as usize);
    //             println!("{} [SigPart:{}] Address block: Vol: {}, Trk:{}, Sect:{}, Sum:{:02X} (exp. ${:02X}) DE:${:02X} AA:${:02X}",
	// 		 i, sig_part_indices_ext[i],
	// 		 vol, trk, sect, sum, vol ^ trk ^ sect,
	// 		 end_mark_de, end_mark_aa);
    //             prev_block = i;
    //             data_block = false;
    //         } else if data_reg[i] == 0xDE && data_reg[i + 1] == 0xAA && data_reg[i + 2] == 0xEB {
    //             println!(
    //                 "{} [SigPart:{}] End block, size was: {}",
    //                 i,
    //                 sig_part_indices_ext[i],
    //                 i - prev_block
    //             );
    //             if let Some(_sect) = current_sector {
    //                 if data_block {
    //                     let data = &data_reg[i - 346..i];
    //                     println!("Checksum : {}", data[345]);
    //                 }
    //             }
    //             prev_block = 0;
    //             data_block = false;
    //         } else if data_reg[i] == 0xD5 && data_reg[i + 1] == 0xAA && data_reg[i + 2] == 0xAD {
    //             prev_block = i;
    //             println!("{} Data block ", i);
    //             data_block = true;
    //         }
    //     }
    // }

    // #[test]
    // fn display_test() {
    //     let mut controller = Controller::new();
    //     //controller.woz_file = load_disk(Path::new("additional/conan.dsk"));
    //     //controller.woz_file = load_disk(Path::new("/mnt/data2/roms/Apple/woz test images/WOZ 2.1 (with FLUX chunks)/ProDOS User's Disk - Disk 1, Side A.woz"));
    //     // controller.woz_file = load_disk(Path::new("/mnt/data2/torrents/wozaday_Choplifter/Choplifter (woz-a-day collection)/Choplifter.woz"));
    //     // controller.woz_file = load_disk(Path::new("/mnt/data2/roms/Apple/woz test images/WOZ 2.0/Commando - Disk 1, Side A.woz"));
    //     controller.woz_file = load_disk(Path::new(
    //         "/mnt/data2/roms/Apple/woz test images/FromSlack/Test Drive side A.woz",
    //     ));

    //     controller.stepper.set_current_subtrack(17.0);
    //     controller.change_subtrack();

    //     let mut data_register_values: Vec<u8> = Vec::new();
    //     let mut flux_values: Vec<bool> = Vec::new();
    //     let mut track_bit_indices: Vec<usize> = Vec::new();
    //     let mut sig_part_indices: Vec<usize> = Vec::new();
    //     let mut sig_part_times: Vec<f64> = Vec::new();

    //     let unit = 1.0 * NANOSEC125;
    //     const NB_RECORDS: usize = 200_000 * 8;

    //     let mut rp = false;
    //     for i in 0..NB_RECORDS {
    //         // controller.current_track_nb_bits() / 4
    //         sig_part_indices.push(controller.current_signal_part);
    //         sig_part_times.push(controller.signal_current_time);
    //         track_bit_indices.push(controller.track_bit_index);
    //         //let (rp, dr) = controller.tick_cpu_cycle();
    //         rp = controller.mc3470_sense_and_advance(unit); // half µsec
    //                                                         //println!("{}   rp={}",i, rp);
    //         flux_values.push(rp);

    //         // 2000000 is LSS' frequency
    //         if i % (((1.0 / 2_000_000.0) / unit).round() as usize) == 0 {
    //             //println!("{}",i);
    //             controller.lss.tick(0, 0, rp, 0);
    //             //rp = false;
    //         }

    //         let dr = controller.data_register(); // 0=dummy cycle
    //         data_register_values.push(dr);
    //     }

    //     let mut s = "".to_string();
    //     let mut s_rp = "".to_string();
    //     let mut s_rp2 = "".to_string();
    //     let mut s_signal = "".to_string();

    //     let n = 32;
    //     let mut last_dr = 0;

    //     let mut message = None;

    //     for i in 0..NB_RECORDS {
    //         // println!("i:{} pulse:{} sigpart: #{} t:{:.3}µs",i,
    //         // 	     flux_values[i], sig_part_indices[i], sig_part_times[i]*1000000.0);

    //         let dr = data_register_values[i];

    //         let byte = format!("{:02X}", dr);

    //         if i == 0 || sig_part_indices[i] != sig_part_indices[i - 1] {
    //             let track_data = &controller
    //                 .woz_file
    //                 .track_data(controller.current_subtrack().unwrap())
    //                 .unwrap();
    //             let sig_part: &SignalPart = &track_data.flux[sig_part_indices[i]];

    //             let part_start = (sig_part.time / unit).round() as usize;
    //             // println!("changing signal part: start:{:.3}µs dur:{:.3}µs flux[{}]:{}",
    //             // 	 sig_part.time*1000000.0,
    //             // 	 sig_part.duration*1000000.0, i, flux_values[i]);
    //             let part_start_in_chars = (part_start % n) * 3;
    //             while s_signal.len() < part_start_in_chars {
    //                 s_signal.push(' ')
    //             }
    //             s_signal.push_str(
    //                 format!(
    //                     "{:.3}|{:.3}",
    //                     (sig_part.time * 1000000.0),
    //                     (sig_part.duration * 1000000.0)
    //                 )
    //                 .as_str(),
    //             );
    //         }

    //         // A new valid data register has come
    //         if dr & 0x80 != 0 && last_dr & 0x80 == 0 {
    //             // Let's peek in the future
    //             let bytes = dedup(&data_register_values, i, 11);

    //             if bytes[0].value == 0xD5
    //                 && bytes[1].value == 0xAA
    //                 && (bytes[2].value == 0x96 || bytes[2].value == 0xB5)
    //             {
    //                 let vol = decode(bytes[3].value, bytes[3 + 1].value);
    //                 let trk = decode(bytes[3 + 2].value, bytes[3 + 3].value);
    //                 let sect = decode(bytes[3 + 4].value, bytes[3 + 5].value);

    //                 //println!("{}", bytes.iter().map(|x| format!("{:02X},",x.value).to_string()).collect::<String>());
    //                 message = Some(format!(
    //                     "Address block: Vol: {}, Trk:{}, Sect:{}",
    //                     vol, trk, sect
    //                 ));

    //                 s.push_str(format!("{}", byte.color("white").on_color("red")).as_str());
    //             } else if bytes[0].value == 0xDE && bytes[1].value == 0xAA && bytes[2].value == 0xEB
    //             {
    //                 message = Some("End block".to_string());
    //                 s.push_str(format!("{}", byte.color("white").on_color("red")).as_str());
    //             } else if bytes[0].value == 0xD5 && bytes[1].value == 0xAA && bytes[2].value == 0xAD
    //             {
    //                 message = Some("Sector data block".to_string());
    //                 s.push_str(format!("{}", byte.color("white").on_color("red")).as_str());
    //             } else if bytes[0].value == 0xD5 {
    //                 message = Some("Start of header".to_string());
    //                 s.push_str(format!("{}", byte.color("white").on_color("red")).as_str());
    //             } else {
    //                 // Nothing special detected; but new valid data register nonetheless
    //                 s.push_str(format!("{}", byte.color("black").on_color("white")).as_str());
    //             }
    //         } else if dr & 0x80 != 0 {
    //             s.push_str(byte.as_str());
    //         } else {
    //             s.push_str(byte.color("blue").to_string().as_str());
    //         }

    //         s.push(' ');

    //         // read pulse
    //         let rp = flux_values[i];
    //         let p_eq = i > 0 && flux_values[i] == flux_values[i - 1];

    //         if rp {
    //             if p_eq {
    //                 s_rp.push_str("   ");
    //                 //s_rp2.push_str("┌┐_");
    //                 s_rp2.push_str("───");
    //             } else {
    //                 s_rp.push_str("   ");
    //                 //s_rp2.push_str("┌┐_");
    //                 s_rp2.push_str("───");
    //             }
    //         } else if p_eq {
    //             s_rp.push_str("   ");
    //             s_rp2.push_str("___");
    //         } else {
    //             s_rp.push_str("   ");
    //             s_rp2.push_str("___");
    //         }

    //         if i % n == n - 1 || i == NB_RECORDS - 1 {
    //             println!("        {}", s_rp);
    //             s_rp = "".to_string();
    //             println!("{:6}: {}", track_bit_indices[i - (n - 1)], s_rp2);
    //             s_rp2 = "".to_string();
    //             println!("{:6}: {}", i - (n - 1), s);
    //             s = "".to_string();
    //             if message.is_some() {
    //                 println!(
    //                     "        {}",
    //                     message.unwrap().color("white").on_color("red")
    //                 );
    //                 message = None;
    //             }
    //             println!("        {}", &s_signal);
    //             s_signal = "".to_string();
    //         }

    //         last_dr = dr;
    //     }
    // }

    /*
    #[test]
    fn read_test() {
    // export RUST_LOG="accurapple::wozpack=debug"
    // cargo test wozpack --bin accurapple -- --show-output


    let mut log_builder = &mut env_logger::builder();
    log_builder = log_builder
        .format_timestamp(None)
        .filter(None, LevelFilter::Debug);
    log_builder.init();


    let mut c = Controller::new();
    c.woz_file = load_disk(Path::new("additional/conan.dsk"));
    c.current_subtrack = 1;

    let mut cycle = 1;
    let mut bytes_read: Vec<u8> = Vec::new();

    let first_byte = DSK_FIRST_SECTOR_10BITS_SYNC*10 / 8;
    for i in 1..=first_byte+3 {
        let (c2, b) = read_8bits(cycle, &mut c);
        cycle = c2;
        bytes_read.push(b);
    }

    println!("Bytes read, first byte at index {}", first_byte);
    for i in 0..bytes_read.len() {
        println!("{:4} {:02X}", i, bytes_read[i]);
    }

    // FIXME We wrote DSK_FIRST_SECTOR_10BITS_SYNC 10-bits sync leaders.
    // So I'd expect to get as many bits back when reading. It's not
    // the case: the LSS seems to eat 2 bits for each 10 written...

    assert!(bytes_read.as_slice()[DSK_FIRST_SECTOR_10BITS_SYNC..=DSK_FIRST_SECTOR_10BITS_SYNC+2] == [0xD5,0xAA,0x96]);

    let (c2, trk, sect) = read_to_sector(cycle, &mut c);
    cycle = c2;
    println!("Cycle:{} Bit index:{}", cycle, c.track_bit_index);

    cycle += 12000;
    c.data_register(cycle);
    cycle += 1;
    let (c2, trk, sect) = read_to_sector(cycle, &mut c);
    cycle = c2;
    println!("Cycle:{} Bit index:{}", cycle, c.track_bit_index);

    cycle += 220_000;
    c.data_register(cycle);
    cycle += 1;
    let (c2, trk, sect) = read_to_sector(cycle, &mut c);
    cycle = c2;
    println!("Cycle:{} Bit index:{}", cycle, c.track_bit_index);

    // for i in 1..202_230 {
    //     let b = c.data_register(cycle);

    //     if b & 0x80 > 0 {
    // 	println!("{:3} {:02X}", cycle, b)
    //     }
    //     cycle += 1;
    // }
    }
    */
}
