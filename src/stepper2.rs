use std::{fs::File, path::PathBuf};
use std::io::{BufWriter, Write};
use maths_rs::{Vec2f, cross, Vec3f};
use log::{trace};

fn bool2f32(b: bool) -> f32 {
    if b {1.0} else {0.0}
}

fn cross2d(a: &Vec2f, b: &Vec2f) -> f32 {
    cross( Vec3f::new(a.x,a.y,0.0), Vec3f::new(b.x,b.y,0.0)).z
}

fn sign(a: f32) -> f32 {
    if a > 0.0 { 1.0 }
    else if a < 0.0 { -1.0 }
    else { 0.0 }
}

struct LogEntry {
    time: u64,
    angle: f32,
    magnets: [bool; 4],
    subtrack: Option<usize>
}

pub struct DiskStepper {
    angle_speed: f32,
    angle: f32, // Expressed in radians
    _torque: f32,
    magnets_states:[bool; 4],
    torque_factor: f32,
    friction_factor: f32,
    max_dt: f32, // seconds max value of delta t. Choos e it small enough to counter numerical precision issue due to Euler schema.
    mag_force: Vec2f,
    log: Option<Vec<LogEntry>>
}

impl DiskStepper {
    pub fn new() -> DiskStepper {
        DiskStepper {
            angle_speed: 0.0,
            angle: 0.0,
            _torque: 0.0,
            magnets_states: [false, false, false, false],
            torque_factor: 2000000.0,
            friction_factor: 2200.0,
            max_dt: 0.0001,
            mag_force: Vec2f::new(0.0,0.0),
            log: None
        }
    }

    pub fn set_magnet(&mut self, magnet_ndx:usize, charge:bool, cycle:u64) {
        self.magnets_states[magnet_ndx] = charge;
        self.mag_force = self.compute_magnet_force();
        trace!("set_magnet: cycle: {} magnets:{} {} {} {}",cycle, self.magnets_states[0],self.magnets_states[1],self.magnets_states[2],self.magnets_states[3]);
    }

    pub fn set_magnets(&mut self, mag1:bool, mag2:bool, mag3:bool, mag4:bool) {
        self.magnets_states = [mag1, mag2, mag3, mag4];
        self.mag_force = self.compute_magnet_force();
    }

    fn compute_magnet_force(&self) -> Vec2f {
        let ms = &self.magnets_states;

        let mut f = bool2f32(ms[0]) * Vec2f::new(1.0,0.0); // east
        f += bool2f32(ms[1]) * Vec2f::new(0.0,1.0); // north
        f += bool2f32(ms[2]) * Vec2f::new(-1.0,0.0); // west
        f += bool2f32(ms[3]) * Vec2f::new(0.0,-1.0); // south
        f
    }

    fn angle_to_quarter_track(angle: f32) -> Option<usize> {
        assert!(angle >= 0.0);
        // 2*PI = 2 tracks == 4 half tracks = 8 quarters track.
        // This self-less function exists to go around the borrow checker
        Some((angle/(2.0*std::f32::consts::PI)*8.0).round() as usize)
    }

    pub fn closest_quarter_track(&self) -> Option<usize> {
        // I say "closest" because this is analog, so we're always
        // close or away of something. It is the the caller's
        // responsibility to determine to which data this
        // corresponds to the actual disk (remember that the
        // read head cannot read to neighbouring quarter
        // tracks).

        // None: the read head is between two quarter tracks
        // Some(usize) : number of the quarter track.
        Self::angle_to_quarter_track(self.angle)
    }

    fn is_motionless(&self) -> bool {
        self.angle_speed.abs() < 1e-7 && self._torque.abs() < 1e-7
    }

    pub fn time_to_next_eval(&self) -> f32 {
        // How much time we can wait before re-evaluating the stepper
        // position. It is an estimation.
        // Returns a if the stepper is at rest.

        if !self.is_motionless() {
            self.max_dt
        } else {
            //print(f"Stopped {self.angle_speed} {self._torque}")
            1.0
        }
    }

    pub fn advance(&mut self, cycle: u64, dt: f32) {
        // dt: expressed in seconds
        // cycle is just here for logging purposes

        assert!( dt > 0.0, "I got nothing to do");
        // assert!( dt <= self.max_dt, "Precision will suffer");

        // FIXME Dirty. One should restore the assret above
        // and adapt the code accordingly.
        let dt = dt.min(self.max_dt);

        let mag_direction = Vec2f::new(self.angle.cos(),self.angle.sin());
        self._torque = if maths_rs::mag(self.mag_force) > 0.0 {

            // Norm of |r|. Since r has the form (0,0,r), we can
            // simplify the computation by using cross2D.
            let t = self.torque_factor * cross2d(&mag_direction, &self.mag_force);
            // norm(self.mag_force) is not always one !!!
            // The formula is: torque_factor * |r|/(|F|*|v|)
            // - |r| = torque vector norm
            // - F = torque vector direction
            // - mag = position vector

            // The norm of the magnet vector is 1 by construction.

            t / maths_rs::mag(self.mag_force)
        } else {
            0.0
        };

        // This is an Euler step. Consequently we're doing
        // an approximation on the speed as big as dt.
        // With dt small enough (<0.001s) this seems just fine.

        let friction_force = -self.friction_factor*self.angle_speed;
        let angular_acceleration = (self._torque + friction_force)*dt;
        self.angle_speed += angular_acceleration;
        trace!("cycle: {} dt:{} angle speed:{} angle:{} torque:{} friction:{}", cycle, dt, self.angle_speed, self.angle, self._torque, friction_force);
        self.angle += self.angle_speed*dt;

        // From 0 to track 34 inclusive == 35 tracks. Expressed in radians.
        const ANGLE_MAX: f32 = 2.0*std::f32::consts::PI*34.01/2.0;

        if self.angle < 0.0 {
            // Bumping against physical limits !
            self.angle = 0.0;
            self.angle_speed *= -0.1;
        } else if self.angle > ANGLE_MAX {
            self.angle = ANGLE_MAX;
            self.angle_speed *= -0.1;
        }

        if self.angle_speed.abs()*dt < 1e-7 {
            self.angle_speed = 0.0;
        }

        if self.log.is_some() {
            let log = self.log.as_mut().unwrap();
            log.push( LogEntry {
                time: cycle,
                angle: self.angle,
                magnets: self.magnets_states,
                subtrack: Self::angle_to_quarter_track(self.angle)
            });
        }

    }

    fn mag_str(&self, n: usize) -> String {
        if self.magnets_states[n] {
            n.to_string()
        } else {
            "-".to_string()
        }
    }

    pub fn magnets_to_string(&self) -> String {
        format!(
            "{}{}{}{}",
            self.mag_str(0),
            self.mag_str(1),
            self.mag_str(2),
            self.mag_str(3)
        )
    }



    pub fn start_logging(&mut self) {
        self.log = Some(Vec::new());
    }

    pub fn stop_logging(&mut self, path: &PathBuf) {
        let output = File::create(path).expect("Can't create a file");

        let mut buf_output = BufWriter::new(output);

        writeln!(buf_output, "Cycle,Angle (deg),Mag#1,Mag#2,Mag#3,Mag#4,Track").expect("Can't write ?");
        for v in self.log.as_ref().unwrap() {
            let f = |m: usize| if v.magnets[m] {m+1} else {0};
            let _ = writeln!(buf_output, "{},{:.3},{},{},{},{}, {}", v.time, v.angle/std::f32::consts::PI*180.0,
                f(0), f(1), f(2), f(3), if let Some(qt) = v.subtrack {qt as i32} else {-1});
        }
        self.log = None;
    }

    pub fn set_current_subtrack(&mut self, halftrack:f32) {
        self.angle = 2.0*std::f32::consts::PI*halftrack/2.0;
        self.angle_speed = 0.0;
    }

}

/*

    def set_mags(self, mag1, mag2, mag3, mag4):
        assert mag1 in (0,1)
        self.mag1, self.mag2, self.mag3, self.mag4 = mag1, mag2, mag3, mag4
        f = self.mag1*np.array([1,0]) # ->
        f += self.mag2*np.array([0,1]) # ^
        f += self.mag3*np.array([-1,0]) # <-
        f += self.mag4*np.array([0,-1]) # v
        self.mag_force = f

    def quarter_track(self):
        return round(self.angle/(2*pi)*8)

    def next_step_time(self):
        # How much time the stepper thinks it needs
        # to continue its movement. It is an estimation.
        # Returns 0 if the stepper is at rest.

        mag = np.array([cos(self.angle),sin(self.angle),0])
        if norm(self.mag_force) > 0:
            torque = abs(np.cross(mag, self.mag_force)[2])
        else:
            torque = 0

        if abs(self.angle_speed) > 1e-7 or abs(torque) > 1e-7:
            return self.max_dt
        else:
            #print(f"Stopped {self.angle_speed} {self._torque}")
            return 0

    def tick(self, dt):
        # dt: expressed in seconds

        assert dt > 0, "I got nothing to do"
        assert dt <= self.max_dt, "Precision will suffer"

        mag = np.array([cos(self.angle),sin(self.angle),0])
        if norm(self.mag_force) > 0:
            torque = self.torque_factor * np.cross(mag, self.mag_force)
            # norm(self.mag_force) is not always one !!!
            # The formula is: torque_factor * |r|/(|F|*|v|)
            # - |r| = torque vector norm
            # - F = torque vector direction
            # - mag = position vector
            self._torque = sign(torque[2]) * norm(torque) / (norm(self.mag_force)*norm(mag))
        else:
            self._torque = 0

        # This is an Euler step. It mens that we're doing
        # an approximation on the speed as big as dt.
        # With dt small enough (<0.001s) this seems just fine.
        friction = -self.friction_factor*self.angle_speed
        self.angle_speed += (self._torque + friction)*dt
        self.angle += self.angle_speed*dt

        if self.angle < 0:
            # Bumping against physical limits !
            self.angle = 0
            self.angle_speed = self.angle_speed*-0.1

        #print(f"{torque:.2f} {self.angle:.2f}")
*/