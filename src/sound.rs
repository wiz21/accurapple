use std::sync::{Arc, Mutex};
use std::collections::VecDeque;
use std::time::Instant;
use std::cmp;
use log::{debug, info, warn, trace};

use circular_queue::CircularQueue;
use cpal::{FromSample, SampleFormat, SampleRate, StreamInstant, StreamConfig};
use cpal::Sample;
use cpal::traits::{DeviceTrait, HostTrait};

use crate::a2::{ClockEvent,CYCLES_PER_FRAME};
use crate::emulator::OutputQueues;

/// Contains all the samples issued during one frame (i.e. in 1/50th or 1/60th of a second).
/// (note that CPAL provides constants to represent silence correctly.)
pub struct SamplesFrame {
    pub left: Vec<i16>,
    pub right: Vec<i16>
}

impl SamplesFrame {
    pub fn new(left: Vec<i16>, right: Vec<i16>) -> SamplesFrame {
	SamplesFrame {
	    left,
	    right
	}
    }

	pub fn from(mono: &Vec<f32>) -> SamplesFrame {
		SamplesFrame {
			left: mono.iter().map( |x| *x as i16 ).collect(),
			right: mono.iter().map( |x| *x as i16 ).collect()
		}
	}

    // pub fn add(&mut self, other: &SamplesFrame) -> SamplesFrame  {
    // 	if self.len() == 0 {
    // 	    // IOW: copy
    // 	    SamplesFrame::new(
    // 		self.left.extend_from_slice(other.left.as_slice()),
    // 		self.right.extend_from_slice(other.right.as_slice()));
    // 	} else {
    // 	    // FIXME Once stabilized in rust : SIMD
    // 	    self.left = self.left.iter().zip(&other.left).map(|(lhs, &rhs)| lhs.saturating_add(rhs)).collect();
    // 	    self.right = self.right.iter().zip(&other.right).map(|(lhs, &rhs)| lhs.saturating_add(rhs)).collect();
    // 	}
    // }

    pub fn len(&self) -> usize {
	assert!(self.left.len() == self.right.len());
	self.left.len()
    }
}

pub trait SoundRenderer {
    /* Record events that will be the basis for the
    actual generation (rendering) of the sound in the process_frame()
    method.*/
    fn record(&mut self, event: ClockEvent);

    /* Once events have been collected, render the sound
    for exactly one frame.

    - If some sound is produced, then this array must
    be exactly "samples_per_frame" long (samples_per_frame = sample
    rate (eg 44100) / frames per seond (eg 50))

    - If the sound producer is silent, then return an
    empty vector. This is to allow for some optimisation
    (which in practice brought about 15% speed increase)
     */
    fn process_frame(&mut self) -> SamplesFrame;

    /* Gives access to the samples data.
    This function is an accessor => doesn't mutate
    the instance.
    */
    fn samples(&self) -> Arc<Mutex<VecDeque<i16>>>;
}


pub fn cycle_to_sample(c: u64, spf: usize) -> usize {
    // c: cycles
    // spf: samples per frame
    ((c as usize) * spf) / CYCLES_PER_FRAME
}


pub struct SoundConfig{
    /* Holds the acquired sound config that
    will be used throughout the whole execution
    of the emulator */

    pub config: cpal::StreamConfig,
    pub device: cpal::Device,
    pub sample_format: cpal::SampleFormat,
}

impl SoundConfig {
    pub fn sample_rate(&self) -> usize {
	self.config.sample_rate.0 as usize
    }
    pub fn channels(&self) -> usize {
	self.config.channels as usize
    }
}

struct StatCpal {
    time: u128,
    consumed: usize
}

/// Holds the information related to sound streaming,
/// that is the last step to the sound card.
///
/// This object is normally a singleton and it is
/// held by the sound streaming thread.
pub struct SoundState {
    pub sustained_stream_start: Option<StreamInstant>,
    stream_consumed: CircularQueue<StatCpal>,
    stream_start: Instant,
    last_call: Instant,
    samples_to_wait: usize,
    first_frame: bool
}

impl SoundState {
    pub fn new() -> SoundState {
	SoundState {
	    sustained_stream_start: None,
	    stream_consumed: CircularQueue::<StatCpal>::with_capacity(10),
	    stream_start: Instant::now(),
	    last_call: Instant::now(),
	    samples_to_wait: 0, //44100/50,
	    first_frame: true
	}
    }
}

pub fn find_configuration() -> Option<SoundConfig> {
    let host = cpal::default_host();
    let device_probe = host.default_output_device();

    if device_probe.is_none() {
	warn!("No sound device found. You won't hear anything.");
	return None
    }

    let device = device_probe.expect("No output device available");
    let supported_configs_range = device.supported_output_configs()
	.expect("error while querying configs");

    debug!("Looking for supported configurations");

    let mut chosen_config:Option<cpal::StreamConfig> = None;
    let mut chosen_sample_format:Option<SampleFormat> = None;

    for (config_index, config) in supported_configs_range.into_iter().enumerate() {
	// I can operate in the [44100hz,48000hz] range. FIXME: should test others.
	if config.max_sample_rate().0 >= 44100 && config.min_sample_rate().0 <= 48000 && config.channels() <= 2 {
            debug!(
		"SoundConfig#{}.{}. {:?}",
		1, /*device_index + 1,*/
		config_index + 1,
		config
            );

	    const FREQS:[u32;2] = [44100, 48000];
	    //let mut freq = None;
	    for f in FREQS {
		let supported_sample_format = match config.sample_format() {
		    SampleFormat::F32|SampleFormat::I16|SampleFormat::U16 => true,
		    _ => false
		};

		if config.min_sample_rate().0 <= f && f <= config.max_sample_rate().0 && supported_sample_format {
		    let freq = Some(f);
		    if chosen_config.is_none() ||
			chosen_config.as_ref().unwrap().channels > config.channels() {

			chosen_config = Some(StreamConfig {
			    channels: config.channels(),
			    sample_rate: SampleRate(freq.unwrap()),
			    buffer_size: match config.buffer_size() {
				cpal::SupportedBufferSize::Unknown => cpal::BufferSize::Default,
				// FIXME As you can see here I take the default buffer
				// size even though I have a range. That's because if
				// I take a size in the range then CPAL panics...
				// I should bug report that. Also, because of that,
				// the buffer may be so long as to induce a perceptible
				// delay in the sound.
				cpal::SupportedBufferSize::Range{min:_x,max:_y} => cpal::BufferSize::Default, //cpal::BufferSize::Fixed(*x)
			    }
			});

			chosen_sample_format = Some(config.sample_format());

			//config.with_sample_rate(SampleRate{0:freq.unwrap()});
			//chosen_config = Some(config.with_sample_rate(SampleRate{0:freq.unwrap()}));
		    }
		}
	    }
	}
    }


    // let supported_config: cpal::SupportedStreamConfig = chosen_config
    // 	.expect("no suitable sound config found?!");

    // let sample_format = supported_config.sample_format();
    // // let config = supported_config.into();
    // match supported_config.buffer_size() {
    // 	cpal::SupportedBufferSize::Range{min, max} => info!("BufferSize: min:{} max:{} frames",min, max),
    // 	_ => info!("BufferSize: unknwon")
    // }
    // info!("Sample size: {}, format: {}",
    // 	     sample_format.sample_size(),
    // 	     match sample_format {
    // 		 SampleFormat::F32 => "F32",
    // 		 SampleFormat::I16 => "I16",
    // 		 SampleFormat::U16 => "U16"
    // 	     });

    // // How to pick the default
    // let config: cpal::StreamConfig = supported_config.into();


    // let config: cpal::StreamConfig =  cpal::StreamConfig {
    // 	channels: 1,
    // 	sample_rate: cpal::SampleRate(SAMPLES_PER_SECOND as u32),
    // 	// 50; 100; 200 leads to alsa error => I should query the HW to find the ranges
    // 	// (SAMPLES_PER_SECOND / FRAMES_PER_SECOND) as u32 => error too
    // 	// Check CPAL doc to know the ranges
    // 	// 1024 OK
    // 	buffer_size: cpal::BufferSize::Fixed(1024)
    // };

    let config = chosen_config.unwrap();
    let sample_format = chosen_sample_format.unwrap();

    info!("Config: channels:{}, Buffer size: {}, sample rate:{}, sample format:{}",
	  config.channels,
	  match config.buffer_size {
	      cpal::BufferSize::Fixed(frame_count) => format!("{} frames",frame_count),
	      cpal::BufferSize::Default => String::from("Default")
	  },
	  config.sample_rate.0,
	  match sample_format {
	      SampleFormat::F32 => "f32",
	      SampleFormat::I16 => "i16",
	      SampleFormat::U16 => "u16",
	      _ => "Unsupported ???"
	  });

    //std::process::exit(0);

    Some(SoundConfig {
	config,
	device,
	sample_format
    })
}


/* Copy the samples generated by the emulator
to the sound card. There may be a conversion
between sample formats occuring here. */


pub fn stream_sound<T>(
    sound_state:&mut SoundState,
    data: &mut [T], // Final destination for samples
    callback: &cpal::OutputCallbackInfo,
    samples: &mut VecDeque<(i16, i16)>, // Samples produced by the emulator
    channels: usize
)
    where T: Sample + FromSample<i16>
{
    let now = Instant::now();
    trace!("stream_sound: dt:{}ms Ready: {}, requested::{}",
	   now.duration_since(sound_state.last_call).as_millis(),
	   samples.len(), data.len());
    sound_state.last_call = now;

    // It is >= to help the turbo mode to be able to play sound
    if sound_state.sustained_stream_start.is_none() && samples.len() >= sound_state.samples_to_wait {
		sound_state.sustained_stream_start = Some(callback.timestamp().playback)
    }

	// If the emulator produces samples faster than this thread can
	// consume them (or if the thread started very late and the
	// emulator has accumulated samples), then we cut samples
	// to make up for our delay.
	// FIXME this is rough. We should look at the time delay between
	// the moment we create a sound athe moment we play it.
	// To t'est that, add std::thread::sleep(Duration::from_secs(10));
	// at the beginning of the sound thread to impose a delay.
	if samples.len() > 3*44100/50 {
		warn!("Sound thread is lagging");
		samples.drain(..(samples.len() - 2*44100/50));
	}

    if sound_state.sustained_stream_start.is_some() {

	// Handle some statistics about the streaming

	sound_state.stream_consumed.push(
	    StatCpal{
			time:Instant::now().duration_since(sound_state.stream_start).as_millis(),
			consumed: data.len()
	    });

	// CircularQueue: The iterator goes from the most recently pushed items to the oldest ones.
	let first = sound_state.stream_consumed.iter().next_back();
	let last = sound_state.stream_consumed.iter().rev().last();

	if first.and(last).is_some() {
	    let _time = last.unwrap().time - first.unwrap().time;
	    let _sum:usize = (sound_state.stream_consumed.iter().map(|s| s.consumed).sum::<usize>()) - last.unwrap().consumed;
	    // debug!("Stream rate {} bytes / {}ms = {:.2} bytes/ms, {:.2} ms/call, data:{}",
	    // 	   sum,
	    // 	   time,
	    // 	   (sum as f32) / (time as f32),
	    // 	   (time as f32) / ((sound_state.stream_consumed.len() - 1) as f32),
	    // 	   sound_state.stream_consumed.len());
	}
    }

    if sound_state.sustained_stream_start.is_none() {
		// Allow the sound generation to buffer a bit
		//debug!("Allow the sound generation to buffer a bit");
		for sample in data.iter_mut() {
			*sample = Sample::EQUILIBRIUM;
		}
    } else  {

	// Copying the sample generated by the emulator to the
	// audio stream. Converting samples format if necessary.
	let mut i = 0;
	let i_max = cmp::min(data.len()/channels, samples.len())*channels;

	while i < i_max {
	    // let l = samples_left.get(samples_left.len() - 1 - i).unwrap();
	    // let r = samples_right.get(samples_right.len() - 1 - i).unwrap();
	    let (l,r) = samples.pop_back().unwrap();
	    let left = l.to_sample::<T>();
	    let right = r.to_sample::<T>();

	    match channels {
			1 => { data[i] = left},
			2 => { data[i] = left; data[i+1] = right} // Left and right channels are the same.
			_ => panic!("Unsupported number of audio channels")
	    }
	    i += channels;
	}

	if i < data.len() {
	    /* Underrun has occured. We increase the size of the
	    time to spend filling the sound buffer before streaming
	    out. We increase it a bit a time because we never come
	    back on the decision.

	    FIXME one we have increased samples_to_wait, we should
	    try to decrease it afterwards 'cos it may have been
	    a decision that was good at some point in time but
	    not anymore afterwards.
	     */

	    if i > 0 {
			sound_state.sustained_stream_start = None;
			//sound_state.samples_to_wait += 250; // 48000 or 44110 / 50 ~= 1000

			warn!("Underrun of sound buffer. HW requested {} samples, emulator gave {}. Sample wait={} samples",
				data.len() / channels,
				i / channels,
				sound_state.samples_to_wait);
	    } else {
			// Note, underrun can also occur when the emulator is
			// paused by the debugger..
			// We detect that situation by observing that in
			// that case the emulator produces no sample
			// so "i" == 0.
			// FIXME Use state variables
			for sample in data.iter_mut().skip(i) {
				*sample = Sample::EQUILIBRIUM;
			}
	    }

	} else {
	    //debug!("Current sound buffer size: {}", samples.len())
	}
    }

    /* Running this code shows that on average, ALSA
    consumes samples at 44150 samples per second whereas I
    have asked 44100 (and my code runs at 44100 too). When
    average are taken on very long period, we reach
    44100. It means that locally (in time), the 44100
    value is not respected.

    I tried to increase the number of sample I produce a
    bit to match the consumer's speed (44150) but this is
    not fine grained enough, it leads to produce always
    faster than the consumer and, in the end, it leads to
    greater and greater delay in the sound.

    I tried to find the_message "right" number of samples
    to produce but it's always either too big (delays) or
    to small (underruns).

    Finally, I changed the speaker render code so that it
    dynamically adapt to the fact it's too fast or too
    slow. When either happens, it changes the number of
    samples it produces by a bit. This of course lead to
    some distortion in the produced sound but I can't
    quite hear it.
     */

    // stream_consumed.push(StatCpal{time: duration_since_start.unwrap().as_millis(), consumed:data.len()});
    // if stream_consumed.len() == stream_consumed.capacity() {
    // 	let first:&StatCpal = stream_consumed.iter().rev().next().unwrap();
    // 	let last:&StatCpal = stream_consumed.iter().rev().last().unwrap();

    // 	let s: usize = stream_consumed.iter().map(|sc| sc.consumed).sum();
    // 	debug!("Dest buffer: {}, sound buffer left:{}, rate:{} samples/sec",
    // 	       data.len(),
    // 	       samples.len(),
    // 	       (s as f32) / (((last.time - first.time) as f32)*0.001));
    // 	stream_consumed.clear()
    // }
}


pub fn start_cpal_audio_stream(
    config: &SoundConfig,
    output_queues: Arc<Mutex<OutputQueues>>
) -> cpal::Stream {
    // Will be captured by the move below
    let mut sound_state = SoundState::new();
    let err_fn = |err| eprintln!("an error occurred on the output audio stream: {}", err);
    let channels = config.channels();

    let stream = match config.sample_format {
	SampleFormat::F32 => config.device.build_output_stream(
	    &config.config,
	    move |data: &mut [f32], callback: &cpal::OutputCallbackInfo|
	    {
		trace!("Locking output queue f32");
		let oq = &mut output_queues.lock().expect("Sound lock");
		stream_sound::<f32>(
		    &mut sound_state,
		    data,
		    callback,
		    &mut oq.sound,
		    channels);
	    },
	    err_fn,
	    None),

	SampleFormat::I16 => config.device.build_output_stream(
	    &config.config,
	    move |data: &mut [i16], callback: &cpal::OutputCallbackInfo|
	    {
		trace!("Locking output queue i16");
		let oq = &mut output_queues.lock().expect("Sound lock");
		stream_sound::<i16>(
		    &mut sound_state,
		    data,
		    callback,
		    &mut oq.sound,
		    channels);
	    },
	    err_fn, None),

	SampleFormat::U16 => config.device.build_output_stream(
	    &config.config,
	    move |data: &mut [u16], callback: &cpal::OutputCallbackInfo|
	    {
		trace!("Locking output queue u16");

		let oq = &mut output_queues.lock().expect("Sound lock");
		stream_sound::<u16>(
		    &mut sound_state,
		    data,
		    callback,
		    &mut oq.sound,
		    channels);
	    },
	    err_fn, None),

	_ => panic!("unsupported sample size")
    }.unwrap();

    stream
}
