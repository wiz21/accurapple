use crate::a2::{
    AppleII, ClockEvent, ClockEventData, GfxBaseMode, GfxMode, CYCLES_PER_FRAME, CYCLES_PER_LINE,
    DHIRES_OFF, DHIRES_ON, FIRST_NON_BLANK_LINE, HIRES_OFF, HIRES_ON, LAST_NON_BLANK_LINE,
    MIXED_OFF, MIXED_ON, PAGE2_OFF, PAGE2_ON, TEXT_OFF, TEXT_ON,
};
use itertools::Itertools;
use log::{debug, trace, warn};

use crate::gfx::{
    hgr_address, hgr_address_to_line, text_address,
    HGR_PAGE1_OFFSET, HGR_PAGE2_OFFSET, SCREEN_HEIGHT, SCREEN_WIDTH,
    TEXT_PAGE1_OFFSET, TEXT_PAGE2_OFFSET,
};

const LINE_START:usize = 25;

pub struct Scanner {
    gfx_mem: Vec<u8>,
    gfx_aux_mem: Vec<u8>,
    mem_changes: Vec<u64>,

    mode: GfxMode,
    line: usize,            // Current video line
    line_mem_offset: usize, // Memory Offset of the current line in the current mode
    cycle_start_frame: u64, // Cycle when the frame started
    cycle_start_line: u64,  // Cycle when the line started
    cycle_on_line: usize, // Index of the first non drawn cycle. It's relative to the line start, so starts at 0 and goe to CYCLES_PER_LINE-1
}

impl Scanner {
    pub fn new(mode: GfxMode) -> Scanner {
        Scanner {
            // Only the part of the RAM dedicated to
            // display. Starting from 0 to avoid addresses conversion
            // when updating this array.
            gfx_mem: vec![0; 0x6000],
            gfx_aux_mem: vec![0; 0x6000],
            mem_changes: vec![0; 0x6000],
            mode,
            line: 0,
            line_mem_offset: 0,
            cycle_start_frame: 0,
            cycle_start_line: 0,
            cycle_on_line: 0,
        }
    }

    fn current_cycle(&self) -> u64 {
        self.cycle_start_line + (self.cycle_on_line as u64)
    }

    fn event_occurs_while_on_current_frame(&self, event_tick: u64) -> bool {
        event_tick >= self.cycle_start_frame
            && event_tick < self.cycle_start_frame + (CYCLES_PER_FRAME as u64)
    }

    fn event_occurs_before_current_frame(&self, event_tick: u64) -> bool {
        event_tick < self.cycle_start_frame
    }

    fn event_occurs_while_on_scanner_line(&self, event_tick: u64) -> bool {
        //let r = event_tick >= self.cycle_start_line + (self.cycle_on_line as u64) &&
        //	event_tick < self.cycle_start_line + (CYCLES_PER_LINE as u64);

        // FIXME A weak proof (see above for the strong one)
        let r = event_tick >= self.cycle_start_line
            && event_tick < self.cycle_start_line + (CYCLES_PER_LINE as u64);

        trace!(
            "event_occurs_while_on_scanner_line: {} because {} <= event:{} < {}",
            r,
            self.cycle_start_line + (self.cycle_on_line as u64),
            event_tick,
            self.cycle_start_line + (CYCLES_PER_LINE as u64)
        );
        r
    }

    fn at_begin_of_frame(&self) -> bool {
        let r = self.line == 0 && self.cycle_on_line == 0;
        trace!(
            "at_begin_of_frame: self.line = {}, self.cycle_on_line = {} => {}",
            self.line,
            self.cycle_on_line,
            r
        );
        r
    }

    fn at_beginning_of_line(&self) -> bool {
        self.cycle_on_line == 0
    }

    fn finish_current_line(&mut self) -> Vec<u8> {
        assert!(self.cycle_on_line <= CYCLES_PER_LINE);
        trace!(
            "finish_current_line: self.cycle_on_line={}, line={}",
            self.cycle_on_line,
            self.line
        );
        let cycles_left = CYCLES_PER_LINE - self.cycle_on_line;
        let v = if cycles_left > 0 {
            self.advance(cycles_left)
        } else {
            Vec::new()
        };
        return v;
    }

    fn get_future_mem(&self, ndx: usize) -> (u64, u8) {
        let v: u64 = self.mem_changes[ndx]; // FIXME from array
        let cycle = v >> 8;
        let byte = (v & 0xFF) as u8;
        return (cycle, byte);
    }

    fn set_future_mem(&mut self, cycle: u64, ndx: u16, byte: u8) {
        assert!(
            self.mem_changes[ndx as usize] == 0xFFFFFFFFFFFFFFFF
                || cycle > (self.mem_changes[ndx as usize] >> 8)
        );
        self.mem_changes[ndx as usize] = (cycle << 8) | (byte as u64);
        //self.mem_changes[ndx as usize] = (1 << 8) | (byte as u64);

        // Mark line to be refreshed

        // if ndx >= 0x2000 && ndx < 0x4000 {
        //     (ndx - 0x2000)
        // }
    }

    fn clear_future_mem(&mut self, ndx: usize) {
        self.mem_changes[ndx] = 0xFFFFFFFFFFFFFFFF;
    }

    fn current_line_is_visible(&self) -> bool {
        self.line >= FIRST_NON_BLANK_LINE && self.line <= LAST_NON_BLANK_LINE
    }

    fn advance(&mut self, mut org_nb_cycles: usize) -> Vec<u8> {
        let mut nb_cycles = org_nb_cycles;

        // Returns nothing if outside visible area !

        assert!(nb_cycles > 0);
        assert!(self.cycle_on_line + nb_cycles - 1 <= CYCLES_PER_LINE,
		"Bad cycle count. You requested too many cycles so I'll go past the end of my current line.");

        trace!(
            "advance: top! nb_cycles={} -- frame={}, line={}, cycle_on_line={}",
            nb_cycles,
            self.cycle_start_frame,
            self.line,
            self.cycle_on_line
        );
        let mut bytes: Vec<u8> = Vec::new();

        let mut cycles_after_visible: usize = 0;

        if self.cycle_on_line >= LINE_START + 40 {
            self.cycle_on_line += nb_cycles;
            trace!("Fully after visible zone nb_cycles:{} self.cycle_on_line:{}",nb_cycles, self.cycle_on_line);
            return bytes;
        }

        if self.cycle_on_line + nb_cycles < LINE_START {
            self.cycle_on_line += nb_cycles;
            trace!("Fully before visible zone nb_cycles:{} self.cycle_on_line:{}",nb_cycles, self.cycle_on_line);
            return bytes;
        }

        if self.cycle_on_line < LINE_START && self.cycle_on_line + nb_cycles >= LINE_START {
            nb_cycles -= LINE_START - self.cycle_on_line;
            self.cycle_on_line = LINE_START;
            trace!("Splitting at start of visible zone nb_cycles:{} self.cycle_on_line:{}",nb_cycles, self.cycle_on_line);
        }

        if self.cycle_on_line < LINE_START+40 && self.cycle_on_line + nb_cycles >= LINE_START+40 {
            cycles_after_visible = self.cycle_on_line + nb_cycles - (LINE_START+40);
            nb_cycles = LINE_START+40 - self.cycle_on_line;
            trace!("Splitting at end of visible zone nb_cycles:{} self.cycle_on_line:{} cycles_after_visible:{}",
                nb_cycles, self.cycle_on_line,cycles_after_visible);
        }


        // Handle the visible part of the line (that is, the one
        // where we actaully read video memory)

        if nb_cycles > 0 {
            // We start in the visible part of the line

            let left_before_end_of_visible_part = LINE_START+40 - self.cycle_on_line;

            // We'll draw up to the end of the visivle part first.
            let visible_drawable_bytes = nb_cycles.min(left_before_end_of_visible_part);

            assert!(
                self.cycle_on_line + visible_drawable_bytes <= 65,
                "{} + {} <= 40",
                self.cycle_on_line,
                visible_drawable_bytes
            );
            // Here we work with byte position first (and byte update time
            // second)

            if self.current_line_is_visible() {
                for i in 0..visible_drawable_bytes {
                    let byte_index =  self.cycle_on_line - LINE_START + i;

                    let (update_cycle, update_byte) = self.get_future_mem(self.line_mem_offset + byte_index);
                    let this_cycle = self.cycle_start_line + (byte_index  as u64);
                    let this_addr = self.line_mem_offset + byte_index;

                    if update_cycle > this_cycle {
                        // The byte we're looking will be updated later
                        // than the current cycle.
                        // Or will never be updated
                        bytes.push(self.gfx_mem[this_addr]);
                    } else {
                        // The byte we're looking at was updated
                        // before we reach it.
                        self.gfx_mem[this_addr] = update_byte;
                        self.clear_future_mem(this_addr);
                        bytes.push(update_byte)
                    }
                }
            }
            self.cycle_on_line += visible_drawable_bytes;
            nb_cycles -= visible_drawable_bytes;
        }

        self.cycle_on_line += cycles_after_visible;

        trace!(
            "advance: nb_cycles={}; cycle on line={}; returned bytes={}",
            nb_cycles,
            self.cycle_on_line,
            bytes.len()
        );

        assert!(
            self.cycle_on_line <= CYCLES_PER_LINE,
            "cycle_online={} NOT < {}",
            self.cycle_on_line,
            CYCLES_PER_LINE
        );

        assert!(
            bytes.len() <= org_nb_cycles,
            "bytes.len():{} <= org_nb_cycles:{}",
            bytes.len(),
            org_nb_cycles
        );
        return bytes;
    }

    fn next_line(&mut self) {
        if self.line < SCREEN_HEIGHT as usize - 1 {
            trace!("next_line: Next line {}", self.line);
            self.line += 1;
            self.cycle_start_line += CYCLES_PER_LINE as u64;
        } else {
            trace!(
                "next_line: Next frame {}",
                self.cycle_start_frame + CYCLES_PER_FRAME as u64
            );
            self.line = 0;
            self.cycle_start_frame += CYCLES_PER_FRAME as u64;
            self.cycle_start_line = self.cycle_start_frame;
        }
        self.cycle_on_line = 0;
        self.recompute_line_begin_offset();
    }

    fn change_mode(&mut self, mode: GfxMode) {
        self.mode = mode;
        self.recompute_line_begin_offset();
    }

    fn recompute_line_begin_offset(&mut self) {
        if self.current_line_is_visible() {
            if self.mode.mixed.is_some() && self.line >= FIRST_NON_BLANK_LINE + 40*8 {
                self.line_mem_offset = 0x400 + text_address(self.line - FIRST_NON_BLANK_LINE);
            } else if self.mode.mode == GfxBaseMode::Gr || self.mode.mode == GfxBaseMode::Text40 {
                self.line_mem_offset = self.mode.ram_base() as usize + text_address(self.line - FIRST_NON_BLANK_LINE);
            } else {
                self.line_mem_offset = self.mode.ram_base() as usize + hgr_address(self.line - FIRST_NON_BLANK_LINE);
            }
        }
    }
}

pub struct GfxRenderState {
    cycle: u64,
    text: bool,
    page2: bool,
    mixed: bool,
    dhgr: bool,
    gr: bool,
    mode: GfxMode,
    gfx_mem: Vec<u8>,
    gfx_aux_mem: Vec<u8>,
    should_redraw: bool,
    scanner: Scanner,
    lines_modes: Vec<usize>
}

impl GfxRenderState {
    pub fn new() -> GfxRenderState {
        let mode = GfxMode {
            mode: GfxBaseMode::Text40,
            mixed: None,
            page2: false,
            an3: 0,
            altcharset: 0
        };

        GfxRenderState {
            cycle: 0,
            text: true,
            page2: false,
            mixed: false,
            dhgr: false,
            gr: false,
            mode,
            // Only the part of the RAM dedicated to
            // display. Starting from 0 to avoid addresses conversion
            // when updating this array.
            gfx_mem: vec![0; 0x6000],
            gfx_aux_mem: vec![0; 0x6000],
            should_redraw: true,
            scanner: Scanner::new(mode),
            lines_modes: vec![0;192+60+60] // FIXME Put a const here !
        }
    }
}


static COMPOSITE_LUT: &'static [u8] = include_bytes!("../data/lut_composite.bin");


pub fn render_tv2(current_line: usize, rgba: &mut [u8], signal: Vec<u8>) {
    //println!("{} {}", current_line, signal.len());
    const FILTER_WIDTH: usize = 10;
    if signal.len() > FILTER_WIDTH && signal.len() <= 560 {
        let bytes_base_offset = current_line * (SCREEN_WIDTH as usize) * 4;

        /*
        for i in 0..signal.len() - FILTER_WIDTH {
            let mut ndx: usize = 0;
            for j in 0..FILTER_WIDTH {
                ndx <<= 1;
                assert!(
                    signal[i + j] == 0 || signal[i + j] == 1,
                    "Bad signal value {}",
                    signal[i + j]
                );
                ndx |= (signal[i + j]) as usize;
            }

            const COLOR_CARRIER_WAVELEN: usize = 8;

            let lut_ndx = (ndx * COLOR_CARRIER_WAVELEN + i % COLOR_CARRIER_WAVELEN) * 3;
            assert!(lut_ndx < COMPOSITE_LUT.len());
            rgba[bytes_base_offset + i * 4 + 0] = COMPOSITE_LUT[lut_ndx];
            rgba[bytes_base_offset + i * 4 + 1] = COMPOSITE_LUT[lut_ndx + 1];
            rgba[bytes_base_offset + i * 4 + 2] = COMPOSITE_LUT[lut_ndx + 2];
            rgba[bytes_base_offset + i * 4 + 3] = 0xff;
        }
         */

        for i in 0..signal.len() - FILTER_WIDTH {
            rgba[bytes_base_offset + i * 4 + 0] = signal[i]*255;
            rgba[bytes_base_offset + i * 4 + 1] = signal[i]*255;
            rgba[bytes_base_offset + i * 4 + 2] = signal[i]*255;
            rgba[bytes_base_offset + i * 4 + 3] = 0xff;
        }
    }

    // BLUR

    /*
    const W:usize = 4;
    for i in 0..560-W {
    for color in 0..3 {
        let mut c:usize = 0;
        let mut nz: usize = 0;
        for width in 0..W {
        let b = rgba[bytes_base_offset + (i+width)*4 + color] as usize;
        c += b;
        if b > 0 {
            nz += 1
        }
        }

        if nz >= 2 {
        rgba[bytes_base_offset + i*4 + color] = (c / W) as u8;
        }
    }

    }
    */
}


fn gr_stripe_to_pixels_black_white(
    bytes: &[u8],
    first_offset: usize,
    last_rol_in: u8,
    line_number: usize) -> (u8, Vec<u8>) {

    let mut bytes_out: Vec<u8> = Vec::with_capacity(560);
    let mut last_rol_state_out = 0;
    let bp1 = if bytes.len() >= 2 {bytes[1]} else {255};

    for (x,b) in bytes.iter().enumerate() {

        fn make_shifter( line_number: usize, byte: u8) -> u8 {
            if (line_number / 4) & 1 == 0 {
                byte & 0b1111
            } else {
                (byte & 0b11110000) >> 4
            }
        }

        let mut val: u8 = if x == 0 && first_offset > 0 {
                //println!("{:03} {:08b} {:08b} {:08b}", line_number, last_rol_in, b, bp1);
                make_shifter(line_number, last_rol_in.rotate_left(4))
            } else {
                make_shifter(line_number, *b)
            };
        val = (val << 4) | val;

        let mask = if (x + first_offset) & 1 == 0 {
            0b100
        } else {
            0b1
        };

        for _ in 0..14 {
            val = val.rotate_right(1);
            bytes_out.push( if val & mask != 0 {1} else {0});
        }


        /* let mut val: u8 = if (line_number / 4) & 1 == 0 {
                b & 0b1111
            } else {
                (b & 0b11110000) >> 4
            };

        val = (val << 4) | val;

        last_rol_state_out = val;


        let mut val16 = if x == 0 && first_offset > 0 {
            let mut val: u8 = if (line_number / 4) & 1 == 0 {
                bytes[x+1] & 0b1111
            } else {
                (bytes[x+1] & 0b11110000) >> 4
            };

            val = (val << 4) | val;


            let v32 = val as u32;
            println!("{:02X} {:02X}", v32, last_rol_in);
            // only 14 bits will be used
            //((val as u32) << 16) + ((val as u32) << 8) + 0b11111111 //(last_rol_in as u32)
            let v: u32 = 0b1111_1111;
            //((v32 << 16) + (v32 << 8) + v) << 1
            // 0b1111_1111_1101_1111_1111_1111 // blue
            // 0b1111_1111_1110_1111_1111_1111 // red
            // 0b1111_1111_1111_0111_1111_1111 // orange
            // 0b1111_1111_1011_1111_1111_1111 // green too late
            // 0b1111_1111_1111_1011_1111_1111 // !!! green seems right
             //  0b1111_1111_1111_1111_1011_1111 // !!! green seems early
             ((v32 << 16) + (v32 << 8) + (last_rol_in as u32)) >> 1
        } else {
            let mut z = ((val as u32) << 16) + ((val as u32) << 8) + (val as u32);
            if (x + first_offset) & 1 == 0 {
                z = z >> 2;
            }
                z
        };


        // Sather says one has to invert the signal. But if
        // I do so, of course, the white becomes black and
        // vice versa...
        // val = (val ^ 0xFF) & 0xFF
        for i in 0..14 {
            val16 = val16 >> 1;
            bytes_out.push((val16 & 1) as u8);
        }
        */
    }

    return (last_rol_state_out, bytes_out);
}


fn text40_stripe_to_pixels_black_white(
    bytes: &[u8],
    line_number: usize,
    charset_rom: &Vec<u8>) -> Vec<u8> {

    let mut bytes_out: Vec<u8> = Vec::with_capacity(560);

    for (x,b) in bytes.iter().enumerate() {

        let b = charset_rom[ (*b as usize)*8 + line_number % 8 ];

        let mut val = b;

        // Sather says one has to invert the signal. But if
        // I do so, of course, the white becomes black and
        // vice versa...
        // val = (val ^ 0xFF) & 0xFF

        for i in 0..7 {
            bytes_out.push(val & 1);
            bytes_out.push(val & 1);
            val = val.rotate_right(1);
        }
    }

    return bytes_out;
}


fn hgr_stripe_to_pixels_black_white(
    bytes: &[u8],
    last_bit_out: u8,
    last_rol_state_in: u8
) -> (u8,Vec<u8>) {
    let mut bytes_out: Vec<u8> = Vec::with_capacity(560);

    // let mut ofs = page + hgr_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_begin;
    // let ofs_end = page + hgr_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_end;
    //let rgb_i = y * (SCREEN_WIDTH as usize) * 4;

    let mut last_rol_state_out = 0;

    let mut ofs = 0;
    while ofs < bytes.len()  {
        let mut byte = bytes[ofs];

        last_rol_state_out = byte;

        let mut shift = false;

        if ofs == 0 {
            //println!("{:02X}", last_rol_state);
            //byte = 0b01000100;
            byte = last_rol_state_in.rotate_right(1);
            shift = false;
            //bytes_out.push(0);
        } else {

            shift = byte & 0x80 != 0;

            if shift {
                bytes_out.push(
                    if let Some(b) = bytes_out.last() {
                        *b
                    } else {
                        0
                    });
            }
        }

        for i in 0..7 {
            // the 'byte" will be shifted at the end of
            // each iteration.

            let p = byte & 1;
            bytes_out.push(p);
            bytes_out.push(p);

            //rgba[rgb_i+hpos560] = p;
            //rgba[rgb_i+hpos560+1] = p;
            byte = byte >> 1;
        }

        if shift {
            bytes_out.pop();
        }

        ofs += 1;
    }

    return (last_rol_state_out, bytes_out);
}


pub fn plot_scanned_bytes(scanner: &Scanner, scanned_bytes: &Vec<u8>, rgba: &mut [u8],
    machine: &mut AppleII,) {
    assert!(scanned_bytes.len() == 0 || scanned_bytes.len() == 40);
    if scanner.line >= FIRST_NON_BLANK_LINE
        && scanner.line <= LAST_NON_BLANK_LINE
        && scanned_bytes.len() > 0
    {

        let signal =  if scanner.line >= FIRST_NON_BLANK_LINE + 20*8 && scanner.mode.mixed.is_some() {
            text40_stripe_to_pixels_black_white(
                &scanned_bytes,
                scanner.line - FIRST_NON_BLANK_LINE,
                &machine.charset_rom)
           } else {
                match scanner.mode.mode {
                GfxBaseMode::Gr => {
                    let (last_rol, bits_out) = gr_stripe_to_pixels_black_white(
                        &scanned_bytes,
                        0,
                        0,
                        scanner.line - FIRST_NON_BLANK_LINE);
                    bits_out
                },
                GfxBaseMode::Text40 => {
                    text40_stripe_to_pixels_black_white(
                        &scanned_bytes,
                        scanner.line - FIRST_NON_BLANK_LINE,
                        &machine.charset_rom)
                },
                _ => {
                    let (last_rol, bits_out) = hgr_stripe_to_pixels_black_white(
                        &scanned_bytes, 0, 0)   ;
                    bits_out
                }
            }
        };


        render_tv2(scanner.line, rgba, signal);
    }
}


pub fn plot_scanned_bytes_with_mode_changes(
    scanner: &Scanner,
    scanned_bytes: &Vec<u8>, rgba: &mut [u8],
    machine: &mut AppleII,
    pmode_changes: &Vec<(usize, GfxMode)>) {

    if scanner.line >= FIRST_NON_BLANK_LINE
        && scanner.line <= LAST_NON_BLANK_LINE
        && scanned_bytes.len() > 0
    {
        // I build an easier vec to work with.
        // This is not as expensive as it seems because
        // mode changes don't happen often on a single
        // line.

        assert!( pmode_changes.first().unwrap().0 == 0, "The first entry of the given mode changes *must* be the inital mode on the line");

        let mut mode_changes: Vec<(usize, GfxMode)> = Vec::new();
        mode_changes.extend(pmode_changes);


        let mut expanded = false;
        if pmode_changes.last().unwrap().0 < LINE_START+40 {
            mode_changes.push( (LINE_START+40, pmode_changes.last().unwrap().1) );
            expanded = true;
        }

        if false && scanner.cycle_start_frame >= 11275684 && scanner.line == FIRST_NON_BLANK_LINE + 192/2 && mode_changes.len() >= 2 {
        // P2_2:
        // STA $C05F				; 4 DHIRES_OFF
		// STA $C056				; 4 HIRES_OFF
		// NOP : NOP : NOP : NOP		; 8
		// STA $C057				; 4 HIRES_ON
		// NOP : LDA $AC			; 5
		// LDA #00				; 2
		// STA TBUFFER,X			; 5
		// INX				; 2
		// DEY				; 2
		// NOP : STA $AC			; 5
		// STA $C056				; 4 HIRES_OFF
		// STA $AC				; 3
		// BEQ P3				; 2
		// JMP LLines_P2			; 3
            print!("line={:3} : ", scanner.line);
            for (ndx, mode) in mode_changes.iter() {
                print!("\t{:2} {} {}", ndx, mode.mode, if mode.page2 {"p2"} else {"p1"});
            }
            if expanded {
                println!(" expanded");
            } else {
                println!("");
            }

        }


        let mut signal: Vec<u8> = Vec::with_capacity(600);

        let mut last_rol_state = 0;
        for i in 0..(mode_changes.len()-1) {

            let current_mode = mode_changes[i].1;
            let mut start = mode_changes[i].0;
            let mut end = mode_changes[i+1].0;

            let mut last_bit_out= 0;
            if i > 0 {
                //start -= 1;
                //end -= 1;

                last_bit_out = 1;

            } else {
                last_bit_out = if let Some(b) = signal.last() {*b} else {0} ;
            }

            if start < LINE_START+ 40 && end >= LINE_START {

                let active_start = start.max(LINE_START)-LINE_START;
                let active_end = end.min(LINE_START+40)-LINE_START;

                if false && scanner.cycle_start_frame >= 11275684 && scanner.line == FIRST_NON_BLANK_LINE + 192/2 && mode_changes.len() >= 2 {
                    println!("{} page2:{} > {} {} - {} {}", current_mode.mode, current_mode.page2, start, end, active_start, active_end);
                }

                let slice = &scanned_bytes[active_start..active_end];
                signal.extend(
                match current_mode.mode {
                    GfxBaseMode::Gr => {
                        let (last_rol, bits_out) = gr_stripe_to_pixels_black_white( slice, active_start, last_rol_state, scanner.line - FIRST_NON_BLANK_LINE);
                        last_rol_state = last_rol;
                        bits_out
                     },
                    GfxBaseMode::Text40 => { text40_stripe_to_pixels_black_white( slice, scanner.line - FIRST_NON_BLANK_LINE, &machine.charset_rom) },
                    _ => {
                        let (last_rol, bits_out) = hgr_stripe_to_pixels_black_white( slice, last_bit_out, last_rol_state);
                        last_rol_state = last_rol;
                        bits_out
                    }
                });
            }
        }

        render_tv2(scanner.line, rgba, signal);
    }
}



pub fn plot_complete_line(scanner: &mut Scanner, rgba: &mut [u8], machine: &mut AppleII,) {
    assert!(scanner.at_beginning_of_line());
    let scanned_bytes = scanner.finish_current_line();
    let mut mode_changes:Vec<(usize, GfxMode)> = Vec::new();
    if scanner.line >= FIRST_NON_BLANK_LINE + 20*8 && scanner.mode.mixed.is_some() {
        /* mode_changes.push((0, scanner.mode.as_text40())); */ ;
    } else {
        mode_changes.push((0, scanner.mode));
    }
    plot_scanned_bytes_with_mode_changes(&scanner, &scanned_bytes, rgba, machine, &mode_changes);
    scanner.next_line();
}


pub fn render_one_frame(
    machine: &mut AppleII,
    cpu_cycle: u64,
    rgba: &mut [u8],
    scanner: &mut Scanner,
    state: &mut GfxRenderState
) {
    let mut event_were_processed = false;

    let current_code_mode_line = state.lines_modes[0];

    while !machine.clock_events.is_empty() {
        let mut next_cpu_event_tick = machine.clock_events.back().unwrap().tick;
        assert!(next_cpu_event_tick >= scanner.cycle_start_frame);
        if scanner.event_occurs_while_on_current_frame(next_cpu_event_tick) {
            event_were_processed = true;

            while !scanner.event_occurs_while_on_scanner_line(next_cpu_event_tick) {
                // Finish the current line or scan over a full new one.
                trace!("Finish line before event at tick={}", next_cpu_event_tick);
                //let scanned_bytes = scanner.finish_current_line();
                //plot_scanned_bytes(&scanner, &scanned_bytes, rgba);
                //scanner.next_line();
                plot_complete_line(scanner, rgba, machine);
            }

            let mut mode_changes:Vec<(usize, GfxMode)> = Vec::new();

            if scanner.line >= FIRST_NON_BLANK_LINE + 20*8 && scanner.mode.mixed.is_some() {
                /* mode_changes.push((0, scanner.mode.as_text40())); */ ;
            } else {
                mode_changes.push((0, scanner.mode));
            }


            let mut scanned_bytes: Vec<u8> = Vec::with_capacity(80);

            loop {
                // Process all events on one line
                let cpu_event: ClockEvent = machine.clock_events.pop_back().unwrap();

                let bytes_to_do = (cpu_event.tick - scanner.current_cycle()) as usize;
                if bytes_to_do >= 1 {
                   scanned_bytes.extend(
                        scanner.advance(bytes_to_do),
                    );
                }

                match cpu_event.event {
                    ClockEventData::GfxMemWrite { addr, data } => {
                        scanner.set_future_mem(cpu_event.tick, addr, data)
                    }
                    ClockEventData::GfxAuxMemWrite { addr, data } => {
                        // FIXME Record mem change.
                    }
                    ClockEventData::GfxModeChange { mode } => {
                        if false && scanner.cycle_start_line >= 11275684 && scanner.line == FIRST_NON_BLANK_LINE + 192/2 {
                            println!("Event: cycle:{}, scanner.cycle_on_line:{} mode:{} page2:{}, line:{}, cycle line start:{}",
                                cpu_event.tick, scanner.cycle_on_line, mode.mode, mode.page2, scanner.line, scanner.cycle_start_line);
                        }

                        scanner.change_mode(mode);
                        if scanner.line >= FIRST_NON_BLANK_LINE && scanner.line <= LAST_NON_BLANK_LINE {
                            //state.lines_modes[scanner.line] = mode.code_for_line(scanner.line - FIRST_NON_BLANK_LINE);
                            if mode_changes.len() == 1 && scanner.cycle_on_line == 0 {
                                mode_changes.pop();
                            };
                            mode_changes.push((scanner.cycle_on_line, mode));
                        }
                    }
                    _ => {
                        panic!("Unsupported event type")
                    }
                }


                if machine.clock_events.is_empty() {
                    // No more event so, specifically no more event to process on this line
                    break;
                } else {
                    next_cpu_event_tick = machine.clock_events.back().unwrap().tick;
                    if !scanner.event_occurs_while_on_scanner_line(next_cpu_event_tick) {
                        trace!("The next event is not on the current line: scanner.cycle={} - event_tick={}", scanner.current_cycle(), next_cpu_event_tick);
                        break;
                    }
                }
            } // loop over events of one line

			scanned_bytes.extend(scanner.finish_current_line());
            assert!(scanned_bytes.len() == 40 || scanned_bytes.len() == 0, "Bad length {} (0==vblank line)", scanned_bytes.len());
			assert!(scanner.cycle_on_line == CYCLES_PER_LINE);
            //plot_scanned_bytes(&scanner, &scanned_bytes, rgba, machine);
            if mode_changes.is_empty() {
                plot_scanned_bytes(&scanner, &scanned_bytes, rgba, machine);
            } else {
                plot_scanned_bytes_with_mode_changes(&scanner, &scanned_bytes, rgba, machine, &mode_changes);
            }
			scanner.next_line();
            if scanner.at_begin_of_frame() {
                // We have wrapped
                return;
            }
            assert!(scanner.at_beginning_of_line());
        } else {
            break;
        }
    }

    /*
    while false && !machine.clock_events.is_empty() {

    // The whole idea is to figure out if we must handle
    // the next event or not *without* poping it output
    // of the list.

    let mut next_cpu_event_tick = machine.clock_events.back().unwrap().tick;

    trace!("render_one_frame: next_cpu_event: {}", next_cpu_event_tick);

    if scanner.event_occurs_while_on_current_frame(next_cpu_event_tick) {

        // Process all lines between the current scanner's line and the one
        // *when* (not where) the event occurs.

        while !scanner.event_occurs_while_on_scanner_line(next_cpu_event_tick) {
        // Finish the current line or scan over a full new one.
        trace!("Finish line before event at tick={}",next_cpu_event_tick);
        let scanned_bytes = scanner.finish_current_line();
        plot_scanned_bytes(&scanner, &scanned_bytes, rgba);
        }

        // Now we have an event, we will process all of the events that
        // happen (from a temporal perspective) on the same line.

        trace!("Beginning a full line");
        let mut scanned_bytes: Vec<u8> = Vec::with_capacity(80);

        assert!(scanner.at_beginning_of_line());

        loop {
        let cpu_event: ClockEvent = machine.clock_events.pop_back().unwrap();
        trace!("render_one_frame: next_cpu_event is on current line");

        match cpu_event.event {
            ClockEventData::GfxMemWrite { addr, data } => {
            // Think about a specific example.
            // We update twice the same byte in memory.
            // Once at cycle 1000 with value $10 and another time at 1020 with value $20.
            // Imagine there's a mode change occuring at cycle 1010.
            // In that case we should see $10 on the screen but not $20.
            // This can work only if we handle the mode change event
            // before we handle the second mem change.
            // Therefore we are condemned to process all events in order.

            scanner.set_future_mem(cpu_event.tick, addr, data)
            },
            ClockEventData::GfxAuxMemWrite { addr, data } => {
            // FIXME Record mem change.
            },
            ClockEventData::GfxModeChange { mode } => {
            // scanner.change_mode(mode);
            },
            _ => {panic!("Unsupported event type")}
        }

        // Now runs the scanner up to that event.
        if cpu_event.tick > scanner.current_cycle() {
            scanned_bytes.extend( scanner.advance((cpu_event.tick - scanner.current_cycle()) as usize));
            if !(cpu_event.tick ==  scanner.current_cycle()) {
                println!("cpu_event.tick:{} != ??? scanner.current_cycle():{}",
                cpu_event.tick, scanner.current_cycle());
                }
            trace!("{} scanned_bytes", scanned_bytes.len());
        }
        event_were_processed = true;

        // Look at the upcoming event (if any). We want to know if
        // it will happen on the next line.

        if machine.clock_events.is_empty() {
            // No more event so, specifically no more event to process on this line
            break
        } else {
            next_cpu_event_tick = machine.clock_events.back().unwrap().tick;
            if !scanner.event_occurs_while_on_scanner_line(next_cpu_event_tick) {
            trace!("The next event is not on the current line: scanner.cycle={} - event_tick={}", scanner.current_cycle(), next_cpu_event_tick);
            break
            }
        }
        } // Processing all events on a single line

        // scanner.advance will not move to next line itself
        if !scanner.at_beginning_of_line() {
        trace!("Finishing line of events {} - {}", scanner.current_cycle(), next_cpu_event_tick);
        scanned_bytes.extend(scanner.finish_current_line());
        }

        plot_scanned_bytes(&scanner, &scanned_bytes, rgba);

        if scanner.at_begin_of_frame() {
        break
        }

    } else if scanner.event_occurs_before_current_frame(next_cpu_event_tick) {
        panic!("Event occured *before* the current frame ({} < {}) !?",
           next_cpu_event_tick, scanner.cycle_start_frame);
    } else {
        // The next event is after the current frame
        trace!("render_one_frame: next_cpu_event at {} is after current_frame (which starts at cycle={}) **************************************************",
           next_cpu_event_tick, scanner.cycle_start_frame);
        break
    }

    }
    */

    assert!(scanner.at_beginning_of_line());

    // At this point, if we're at the beginning of the frame
    // then it's for two possible reasons:
    // 1/ we didn't process any event and so the current line
    //    is still on the first line of the frame
    // 2/ we processed events until the beginning of then
    //    next frame.
    // The only way to know is to record if we actually processed
    // events hence "event_were_processed".

    // FIXME One can rewrite this a bit shorter to avoid code duplication.
    //       Rememebr we work line by line to be able to plot the lines
    //       on the screen.

    if !event_were_processed {
        // println!("Finishing empty frame {} {} {}", scanner.line, scanner.cycle_start_frame, machine.clock_events.len());
        loop {
            trace!("Finishing empty frame");
            plot_complete_line(scanner, rgba, machine);
            if scanner.at_begin_of_frame() {
                break;
            }
        }
    } else {
        // println!("Finishing non-empty frame {} {} {}", scanner.line, scanner.cycle_start_frame, machine.clock_events.len());
        while !scanner.at_begin_of_frame() {
            trace!("Finishing non empty frame");
            plot_complete_line(scanner, rgba, machine);
        }
    };
}


#[cfg(test)]
mod tests {
    use super::*;

    // #[test]
}
