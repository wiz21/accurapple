use std::io::Write;
use std::str::{self};
use std::{fs::File, io::Cursor, path::PathBuf, path::Path};
use std::io::Read;

use bit_vec::BitVec;
use log::{debug, info, trace, warn};
use bitstream_io::{BigEndian, BitRead, BitReader, BitWrite, BitWriter};

use strum_macros::Display;
use crc::{Crc, CRC_32_ISO_HDLC};
const CRC32: Crc<u32> = Crc::<u32>::new(&CRC_32_ISO_HDLC);

const EMPTY_SUB_TRACK_INDEX: usize = 0xFF;
const BLOCK_SIZE: usize = 512;
// DSK file stuff
const DSK_VOLUME: u8 = 254; // FIXME With that value Conan (and others) works. Why is it important ?
pub const STABLE_READ_LENGTH: usize = 16 * 8; // 16*8 * 0.125 microsec = 16 microsec (see WOZ Spec)
const ONE_MICROSECOND: usize = 8;

// Accoridng to my understanding of weak bits, a no-transition
// signal of up to 16µs is read correctly. Beyond that
// there are weak bits.
pub const STABLE_READ_DURATION: f64 = 16.0 * ONE_MICRO_SEC;

// How many 10 bits reset sequences to put at the beginning of a track
// when converting from the DSK format to the WOZ format.
const DSK_FIRST_SECTOR_10BITS_SYNC: usize = 16;

// dsk2nic seems to have (14+22) 0xFF bytes => 288 bits. But I'll make them synck block: 8 one's and 2 zero's
// I have tested with 160 bits.
const DSK_INTER_SECTOR_10BITS_GAP: usize = 36*8/10;

const SECTOR_SIZE: usize = 256;
pub const SECTORS_PER_TRACK: usize = 16;
//const TRACK_SIZE:usize = SECTOR_SIZE * SECTORS_PER_TRACK;
const TRACKS_PER_FLOPPY: usize = 35;

// DOS order
const SKEWING_TABLE: [u8; 16] = [0, 7, 14, 6, 13, 5, 12, 4, 11, 3, 10, 2, 9, 1, 8, 15];
// ProDOS order
//const SKEWING_TABLE: [u8; 16] = [0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15 ];

pub const FLOPPY_ROTATION_TIME: f64 = 0.2;
pub const ONE_NANO_SEC: f64 = 1.0 / 1_000_000_000.0;
pub const NANOSEC125: f64 = 125.0 * ONE_NANO_SEC;
pub const ONE_MICRO_SEC: f64 = 1.0 / 1_000_000.0;

pub fn usec_str(sec: f64) -> String {
    format!("{: >10.3}µs", sec * 1_000_000.0)
}

#[derive(PartialEq, Display)]
pub enum SignalPartType {
    Silence,
    ReadPulse,
    WeakBits,
}

pub struct SignalPart {
    pub part_type: SignalPartType,
    pub time: f64,     // Expressed in seconds
    pub duration: f64, // Expressed in seconds
}


impl SignalPart {
    fn new(part_type: SignalPartType, time: usize, duration: usize) -> SignalPart {
        // Duration expressed in 125 nanosecs

        // ReadPulse lasts for one µs exactly (WOZ spec : "If the bit
        // has a 1 value, then you send a 1µs pulse on the RDDATA
        // line.")
        assert!(
            part_type != SignalPartType::ReadPulse
                || (part_type == SignalPartType::ReadPulse && duration == 8)
        );

        SignalPart {
            part_type,
            time: (time as f64) * NANOSEC125,
            duration: (duration as f64) * NANOSEC125,
        }
    }

    pub fn end(&self) -> f64 {
        // The end is not contained in the part.
        self.time + self.duration
    }

    pub fn contains(&self, t: f64) -> bool {
        //self.time - 1e-11 <= t && t < self.time + self.duration - 1e-11
        self.time <= t && t < self.time + self.duration
    }

}

#[derive(Debug, PartialEq, Eq, Display, Copy, Clone)]
pub enum TrackDataType {
    Standard,
    Flux,
    Unknown,
}

pub struct TrackData {
    pub track_type: TrackDataType,

    // The data as they are stored in the WOZ file.
    // This can be regular track data or flux data
    track_bits: Vec<u8>,

    pub flux: Vec<SignalPart>,
    flux_as_bits: BitVec,
    // Bit Count
    // WOZSpec: "for flux data the Bit Count is actually a byte count."
    nb_bits: usize,
    pub flux_duration: f64, // in seconds
}

fn woz_standard_to_bits(track: &TrackData) -> BitVec {
    let mut flux = BitVec::new();

    let bv = BitVec::from_bytes(&track.track_bits);
    for bit in bv.iter().take(track.nb_bits) {
        flux.push(bit);
        flux.grow( 4 * ONE_MICROSECOND  - 1, false);
    }

    flux
}


impl TrackData {
    fn new(track_bits: Vec<u8>, nb_bits: usize) -> TrackData {
        TrackData {
            track_type: TrackDataType::Unknown,
            track_bits,
            flux: vec![],
            flux_as_bits: BitVec::new(),
            nb_bits,
            flux_duration: 0.0,
        }
    }

    fn set_flux(&mut self, flux: Vec<SignalPart>) {
        // let mut sum: f64 = 0.0;
        // for i in 0..flux.len() {
        //     sum += flux[i].duration;
        // }
        self.flux = flux;
        //self.flux_duration = sum;
        //self.flux_as_bits = signal_to_bitvec(&self.flux);
        self.flux_as_bits = woz_standard_to_bits(&self);
        self.flux_duration = self.flux_as_bits.len() as f64 * NANOSEC125;
    }

    pub fn signal_at(&self, pos: usize) -> bool {
        self.flux_as_bits[pos]
    }

    // pub fn write_at(&mut self, pos: usize, s: bool)  {
    //     self.flux_as_bits.set(pos, s);
    // }

    pub fn flux_as_bits_len(&self) -> usize {
        self.flux_as_bits.len()
    }

} // TrackData

pub struct WozFile {
    pub source_file: Option<PathBuf>,
    version: usize,
    info_version: usize,
    optimal_bit_timing: usize,
    cleaned_weak_bits: bool,
    pub write_protected: bool,

    // Links sub track number ( 0,0.25,0.5,0.75,1,1.25,...)
    // to track data
    track_map: Vec<usize>,
    flux_map: Vec<usize>,
    track_data: Vec<TrackData>,
    //pub data_register_values: Vec<Vec<u8>>,
    file_checksum: u32,
}


fn load_map(buffer: &Vec<u8>, chunk_ptr: usize, map: &mut Vec<usize>) {
    for i in 0..160 {
        let track_index = buffer[chunk_ptr + i] as usize;
        map.push(track_index);
        debug!(
            "Map: index:{} -> track index:{}",
            i,
            if track_index != EMPTY_SUB_TRACK_INDEX {
                format!("{}", track_index)
            } else {
                "Empty track (0xFF)".to_string()
            }
        );
    }
}

fn read_info_chunk(buffer: &Vec<u8>, chunk_ptr: usize, woz_file: &mut WozFile) {
    woz_file.version = buffer[chunk_ptr] as usize;
    woz_file.info_version = buffer[chunk_ptr] as usize;

    debug!("INFO Version:{} DiskType:{}, Write Protected:{}, Synchronised:{}, weak bits cleaned:{}, creator:{}!",
	   buffer[chunk_ptr], // Version
	   buffer[chunk_ptr+1], // type
	   buffer[chunk_ptr+2], // write protected
	   buffer[chunk_ptr+3], // Synchronised
	   buffer[chunk_ptr+4], // weak bits cleaned
	   str::from_utf8(&buffer[(chunk_ptr+5)..(chunk_ptr+5+32)]).unwrap());

    woz_file.cleaned_weak_bits = buffer[chunk_ptr + 4] > 0;
    woz_file.write_protected = buffer[chunk_ptr+2] > 0;

    if woz_file.version >= 2 {
        woz_file.optimal_bit_timing = buffer[chunk_ptr + 39] as usize;

        debug!("Disk sides:{} Boot sector format:{}, Optimal bit timing:{}, Compatible hardware:{:016b}",
	       buffer[chunk_ptr+37],
	       buffer[chunk_ptr+38],
	       usec_str((woz_file.optimal_bit_timing as f64) * 125e-9),
	       u16::from_le_bytes(buffer[(chunk_ptr+40)..(chunk_ptr+40+2)].try_into().unwrap()));

        debug!(
            "required ram:{} Kb largest track:{} blocks (512 bytes).",
            u16::from_le_bytes(
                buffer[(chunk_ptr + 42)..(chunk_ptr + 42 + 2)]
                    .try_into()
                    .unwrap()
            ),
            u16::from_le_bytes(
                buffer[(chunk_ptr + 44)..(chunk_ptr + 44 + 2)]
                    .try_into()
                    .unwrap()
            )
        );
    }
}

fn read_meta_chunk(
    _buffer: &Vec<u8>,
    _chunk_ptr: usize,
    _chunk_size: usize,
    _woz_file: &mut WozFile,
) {
    info!("Skipping META chunk")
}

fn read_tmap_chunk(buffer: &Vec<u8>, chunk_ptr: usize, woz_file: &mut WozFile) {
    debug!("TMAP_CHUNK: regular track data");
    load_map(buffer, chunk_ptr, &mut woz_file.track_map);
}

fn read_flux_chunk(buffer: &Vec<u8>, chunk_ptr: usize, woz_file: &mut WozFile) {
    debug!("FLUX_CHUNK: flux data");
    load_map(buffer, chunk_ptr, &mut woz_file.flux_map);
}

/* fn bits_to_cycle_data_register_values(nb_bits: usize, cursor: &mut Cursor<&[u8]>) -> Vec<u8> {
    //let mut cursor = Cursor::new(&buffer[block_start*BLOCK_SIZE..(block_start+nb_blocks)*BLOCK_SIZE]);
    let mut bit_reader = BitReader::endian(cursor, BigEndian);
    let mut lss = LSS::new();
    let mut data_regs: Vec<u8> = Vec::new();

    let mut i = 0;
    while i < nb_bits {
        // ---------- usec = 0.0 ----------
        // Floppy bit : one every 4 usec, sustained for 2usec
        let mut bit: bool = bit_reader.read_bit().unwrap();
        i += 1;
        // LSS cycle : 2 per usec
        lss.tick(0, 0, bit);
        bit = false; // LSS clears the read pulse when it reads it
                     // CPU cycle : 1 per usec
        data_regs.push(lss.data_register());
        // ---------- usec = 0.5 ----------
        lss.tick(0, 0, bit);
        // ---------- usec = 1.0 ----------
        lss.tick(0, 0, bit);
        data_regs.push(lss.data_register());
        // ---------- usec = 1.5 ----------
        lss.tick(0, 0, bit);
        let bit: bool = false;
        // ---------- usec = 2.0 ----------
        lss.tick(0, 0, bit);
        data_regs.push(lss.data_register());
        // ---------- usec = 2.5 ----------
        lss.tick(0, 0, bit);
        // ---------- usec = 3.0 ----------
        lss.tick(0, 0, bit);
        data_regs.push(lss.data_register());
        // ---------- usec = 3.5 ----------
        lss.tick(0, 0, bit);
    }

    /* FIXME What's this ???
    if nb_bits % 8 != 0 {
        for _i in 0..(8 - (nb_bits % 8)) {
            lss.tick(0, 0, false);
            data_regs.push(lss.data_register());
        }
    }
    */

    data_regs
}
*/

fn read_trks_chunk(buffer: &Vec<u8>, chunk_ptr: usize, chunk_size: usize, woz_file: &mut WozFile) {
    if woz_file.version == 1 {
        let mut track_index = 0;
        while (track_index + 1) * 6656 <= chunk_size {
            // WARNING ! This is the structure of the WOZ spec version 1
            // it is *different* than the one of version 2
            // https://applesaucefdc.com/woz/reference1/

            let track_data_start = chunk_ptr + track_index * 6656;
            let track_data_end = chunk_ptr + (track_index + 1) * 6656;
            let track_data = &buffer[track_data_start..track_data_end];
            let nb_bits =
                (u16::from_le_bytes(track_data[6648..6648 + 2].try_into().unwrap())) as usize;
            let _nb_bytes =
                (u16::from_le_bytes(track_data[6646..6646 + 2].try_into().unwrap())) as usize;

            woz_file
                .track_data
                .push(TrackData::new(track_data.into(), nb_bits));

            track_index += 1;
        }
        debug!(
            "TRKS_CHUNK v1: got {} track data",
            woz_file.track_data.len()
        );
    } else if woz_file.version == 2 || woz_file.version == 3 {
        for sub_trk in 0..160 {
            let ofs = chunk_ptr + sub_trk * 8;

            // Parsing TRK part
            // let trk_block = &buffer[(chunk_ptr + track_index*8)..(chunk_ptr + (track_index+1)*8)];
            // let first_block_bits_data = (u16::from_le_bytes(trk_block[0..0+2].try_into().unwrap())) as usize;
            // let block_count = (u16::from_le_bytes(trk_block[2..2+2].try_into().unwrap())) as usize;
            // let bit_count = (u32::from_le_bytes(trk_block[4..4+4].try_into().unwrap())) as usize;

            // debug!("[{}+{}] first_block_bits_data:512*{} block_count:{} bit_count:{}",
            // 	   chunk_ptr, track_index*8,
            // 	   first_block_bits_data, block_count, bit_count);

            let block_start: usize =
                u16::from_le_bytes(buffer[(ofs)..(ofs + 2)].try_into().unwrap()) as usize;
            let nb_blocks =
                u16::from_le_bytes(buffer[(ofs + 2)..(ofs + 4)].try_into().unwrap()) as usize;
            let nb_bits =
                u32::from_le_bytes(buffer[(ofs + 4)..(ofs + 8)].try_into().unwrap()) as usize;
            let _data_regs: Vec<u8> = Vec::new();

            trace!(
                "About to read {} 512-bytes blocks, {} bits ({} bytes), {}",
                nb_blocks,
                nb_bits,
                nb_bits / 8,
                ofs
            );
            if nb_blocks > 0 {
                // Block start is relative to the *beginning of the file*
                let track_data =
                    &buffer[block_start * BLOCK_SIZE..(block_start + nb_blocks) * BLOCK_SIZE];
                woz_file
                    .track_data
                    .push(TrackData::new(track_data.into(), nb_bits));

            } else {
                debug!("Track: {:02} has nb_blocks == 0", sub_trk);
            }

            //woz_file.data_register_values.push(data_regs);
        } // for loop
        debug!(
            "TRKS_CHUNK v2: got {} track data",
            woz_file.track_data.len()
        );
    } else {
        panic!("Unsupported WOZ version {}", woz_file.version)
    }
}

/*
 * Normal byte (lower six bits only) -> disk byte translation table.
 * See Sather 9-26; this is the RWTS 3.3 write table
 */
static BYTE_TRANSLATION: [u8; 64] = [
    0x96, 0x97, 0x9a, 0x9b, 0x9d, 0x9e, 0x9f, 0xa6, 0xa7, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb2, 0xb3,
    0xb4, 0xb5, 0xb6, 0xb7, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xcb, 0xcd, 0xce, 0xcf, 0xd3,
    0xd6, 0xd7, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe5, 0xe6, 0xe7, 0xe9, 0xea, 0xeb, 0xec,
    0xed, 0xee, 0xef, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff,
];


fn nibblize_sector(
    vol: u8,
    trk: u8,
    sector: u8,
    bytes: &[u8],
    bit_writer: &mut BitWriter<&mut Vec<u8>, BigEndian>,
) {
    let chksum: u8 = vol ^ trk ^ sector;
    let address_block: [u8; 8] = [
        ((vol >> 1) | 0xaa),
        (vol | 0xaa),
        ((trk >> 1) | 0xaa),
        (trk | 0xaa),
        ((sector >> 1) | 0xaa),
        (sector | 0xaa),
        ((chksum >> 1) | 0xaa),
        (chksum | 0xaa),
    ];

    let mut sector_buffer: Vec<u8> = Vec::new();
    sector_buffer.extend_from_slice(bytes);
    sector_buffer.extend_from_slice(&[0_u8, 0]);

    let mut prev_value = 0;
    let mut value = 0;
    let mut data_block_1 = [0u8; 86];
    for i in 0..86
    // 86*3 = 258
    {
        value = (sector_buffer[i] & 0x01) << 1;
        value |= (sector_buffer[i] & 0x02) >> 1;
        value |= (sector_buffer[i + 86] & 0x01) << 3;
        value |= (sector_buffer[i + 86] & 0x02) << 1;
        value |= (sector_buffer[i + 172] & 0x01) << 5;
        value |= (sector_buffer[i + 172] & 0x02) << 3;
        data_block_1[i] = BYTE_TRANSLATION[(value ^ prev_value) as usize];
        prev_value = value;
    }

    let mut data_block_2 = [0_u8; SECTOR_SIZE];
    for i in 0..SECTOR_SIZE {
        value = sector_buffer[i] >> 2;
        data_block_2[i] = BYTE_TRANSLATION[(value ^ prev_value) as usize];
        prev_value = value;
    }

    let checksum: u8 = BYTE_TRANSLATION[value as usize];

    if sector == 0 {
        // Beginning of track

        // Sync
        for _i in 0..DSK_FIRST_SECTOR_10BITS_SYNC {
            bit_writer.write(8, 0b11111111_u8).expect("success");
            bit_writer.write(2, 0b00_u8).expect("success");
        }
    }

    bit_writer
        .write_bytes(&([0xD5, 0xAA, 0x96]))
        .expect("success");
    bit_writer.write_bytes(&address_block).expect("success"); // 8 bytes long
    bit_writer
        .write_bytes(&([0xDE, 0xAA, 0xEB]))
        .expect("success");

    // Sync
    for _i in 0..7 {
        bit_writer.write(8, 0b11111111_u8).expect("success");
        bit_writer.write(2, 0b00_u8).expect("success");
    }

    bit_writer
        .write_bytes(&([0xD5, 0xAA, 0xAD]))
        .expect("success");
    bit_writer.write_bytes(&data_block_1).expect("success");
    bit_writer.write_bytes(&data_block_2).expect("success");
    bit_writer.write(8, checksum).expect("success");
    bit_writer
        .write_bytes(&([0xDE, 0xAA, 0xEB]))
        .expect("success");

    // Sync
    for _i in 0..DSK_INTER_SECTOR_10BITS_GAP {
        bit_writer.write(8, 0b1111_1111_u8).expect("success");
        bit_writer.write(2, 0b00_u8).expect("success");
    }
}


impl WozFile {
    pub fn new() -> WozFile {
        WozFile {
            source_file: None,
            version: 2,
            info_version: 0,
            optimal_bit_timing: 32, // times 0.125 nanosec => 4 usec
            cleaned_weak_bits: false,
            write_protected: false,
            track_data: vec![],
            track_map: vec![],
            //data_register_values: vec![],
            flux_map: vec![],
            file_checksum: 0,
        }
    }

    // fn explain_subtrack_data(&self, track_num: usize) {
    // 	if track_num < self.track_map.len() &&
    // 	    self.track_map[track_num] != EMPTY_SUB_TRACK_INDEX {
    // 		debug!("explain(): Track {} is the {}-th standard.", subtrack_str(track_num), self.track_map[track_num] );
    // 		return;
    // 	    }

    // 	if track_num < self.flux_map.len() &&
    // 	    self.flux_map[track_num] != EMPTY_SUB_TRACK_INDEX {
    // 		//debug!("From flux");
    // 		debug!("explain(): Track {} is the {}-th flux.", subtrack_str(track_num), self.flux_map[track_num] );
    // 		return;
    // 	    }

    // 	debug!("explain(): Track {} is empty.", subtrack_str(track_num));
    // }

    pub fn track_index(&self, track_num: usize) -> Option<usize> {
        if self.track_data.is_empty() {
            return None;
        }

        if track_num < self.track_map.len() && self.track_map[track_num] != EMPTY_SUB_TRACK_INDEX {
            //debug!("From tracks");
            return Some(self.track_map[track_num]);
        }

        if track_num < self.flux_map.len() && self.flux_map[track_num] != EMPTY_SUB_TRACK_INDEX {
            //debug!("From flux");
            return Some(self.flux_map[track_num]);
        }

        if track_num >= self.track_map.len() && track_num >= self.flux_map.len() {
            // The track num is outside range
            // If there are no track there are no disk, so we silence
            // the warning. FIXME Dirty, just represent "no disk" correctly !
            if track_num > 2 {
                warn!("Invalid track number track_num={}/len of track map:{}", track_num, self.track_map.len());
                panic!();
                //return Some(self.track_data.len()-1);
            }
        }

        None
    }

    // Used in tests
    pub fn _nb_track_data(&self) -> usize {
        return self.track_data.len();
    }

    pub fn track_data(&self, track_num: usize) -> Option<&TrackData> {

        if let Some(ndx) = self.track_index(track_num) {
            Some(&self.track_data[ndx])
        } else {
            None
        }
    }


    pub fn write_at(&mut self, track_num: usize, pos: usize, s: bool)  {
        if let Some(ndx) = self.track_index(track_num) {
            //println!("Write at {} : {}", pos, s);
            let len = self.track_data[ndx].flux_as_bits_len();
            if pos < len {
                self.track_data[ndx].flux_as_bits.set(pos, s);
            } else {
                self.track_data[ndx].flux_as_bits.set(pos % len, s);
            }
        }
    }


    /* pub fn data_register(&self, cycle: u64, track: usize, subtrack: usize) -> u8 {
        //println!("Track {}", track);
        let ndx = self.track_map[track * 4 + subtrack];
        trace!("track:{} subtrack:{} ndx:{}", track, subtrack, ndx);
        let track_data = &self.data_register_values[ndx];
        let cycle_ndx = (cycle % (track_data.len() as u64)) as usize;
        track_data[cycle_ndx]
    } */

    pub(crate) fn read(file_path: &Path) -> WozFile {
        let mut woz_file = WozFile::new();
        let mut f = File::open(file_path).expect("open");

	    woz_file.source_file = Some(file_path.to_path_buf());
        let mut buffer: Vec<u8> = Vec::new();

        // read the whole file
        f.read_to_end(&mut buffer).expect("read");

        let crc = u32::from_le_bytes(buffer[8..=11].try_into().unwrap());
        let crc_woz = CRC32.checksum(&buffer[12..buffer.len()]);
        info!("CRC: woz:{:08X} computed:{:08X}", crc, crc_woz);
        woz_file.file_checksum = crc_woz;

        let woz_ident = str::from_utf8(&buffer[0..4]);
        match woz_ident.unwrap() {
            "WOZ1" => woz_file.version = 1,
            "WOZ2" => woz_file.version = 2,
            _ => panic!("Unsupported WOZ version : '{}'", woz_ident.unwrap()),
        }
        debug!("Version: {}! {} bytes", woz_ident.unwrap(), buffer.len());

        let mut chunk_ptr = 12;

        while chunk_ptr < buffer.len() {
            let chunk_size = (u32::from_le_bytes(
                buffer[(chunk_ptr + 4)..(chunk_ptr + 4 + 4)]
                    .try_into()
                    .unwrap(),
            )) as usize;
            let chunk_type = str::from_utf8(&buffer[chunk_ptr..(chunk_ptr + 4)]).unwrap();
            debug!("CHUNK: Type:{} Size:{} !", chunk_type, chunk_size);
            chunk_ptr += 8;

            match chunk_type {
                "INFO" => read_info_chunk(&buffer, chunk_ptr, &mut woz_file),
                "META" => read_meta_chunk(&buffer, chunk_ptr, chunk_size, &mut woz_file),
                "TMAP" => read_tmap_chunk(&buffer, chunk_ptr, &mut woz_file),
                "FLUX" => read_flux_chunk(&buffer, chunk_ptr, &mut woz_file),
                "TRKS" => read_trks_chunk(&buffer, chunk_ptr, chunk_size, &mut woz_file),
                _ => panic!("Unrecognized chunk type"),
            }

            chunk_ptr += chunk_size;
        }

        debug!("Assembling standard and flux track map: {} tracks; track_data has {} entries; flux has {} entries",
	       woz_file.track_map.len(), woz_file.track_data.len(), woz_file.flux_map.len());

        for i in 0..woz_file.track_map.len() {
            let mut track_index = EMPTY_SUB_TRACK_INDEX;

            if woz_file.track_map[i] != EMPTY_SUB_TRACK_INDEX {
                track_index = woz_file.track_map[i];
                if track_index < woz_file.track_data.len() {
                    woz_file.track_data[track_index].track_type = TrackDataType::Standard;
                } else {
                    warn!("Standard track index {} has no corresponding track data. Making it an empty track.",
			  track_index);
                    woz_file.track_map[i] = EMPTY_SUB_TRACK_INDEX;
                }
            //debug!("{} -> {} set to standard",i, track_index);
            } else if !woz_file.flux_map.is_empty() && woz_file.flux_map[i] != EMPTY_SUB_TRACK_INDEX {
                track_index = woz_file.flux_map[i];
                if track_index < woz_file.track_data.len() {
                    woz_file.track_data[track_index].track_type = TrackDataType::Flux;
                } else {
                    warn!("Flux track index {} has no corresponding track data. Making it an empty track.",
			  track_index);
                    woz_file.flux_map[i] = EMPTY_SUB_TRACK_INDEX;
                }
                //debug!("{} -> {} set to flux",i, track_index);
            }

            let fraction = " ¼½¾".to_string();

            debug!(
                "Trk {:02} {} -> {} {}",
                i / 4,
                fraction.chars().nth(i % 4).unwrap(),
                if track_index == EMPTY_SUB_TRACK_INDEX {
                    String::from("Unused")
                } else {
                    format!("{}", track_index)
                },
                if let Some(t) = woz_file.track_data(i) {
                    match t.track_type {
                        TrackDataType::Flux => format!("Flux, {} bytes", t.nb_bits),
                        TrackDataType::Standard => format!("Standard, {} bits", t.nb_bits),
                        TrackDataType::Unknown => "Unknown".to_string(),
                    }
                } else {
                    "Empty".to_string()
                }
            );
        }

        woz_file.convert_flux_to_signal();
        woz_file
    }

    pub fn read_dsk(file_path: &Path) -> WozFile {
        // The general idea is : get the DSK, make a WOZ out of it.

        debug!("DSK import: Reading {} ", file_path.to_str().unwrap());

        let mut woz_file = WozFile::new();
        woz_file.source_file = Some(file_path.to_path_buf());
        let mut f = File::open(file_path).expect("open should work");
        let mut buffer: Vec<u8> = Vec::new();
        // read the whole file
        f.read_to_end(&mut buffer).expect("read");

        if buffer.len() < SECTORS_PER_TRACK * SECTOR_SIZE * TRACKS_PER_FLOPPY {
            buffer.extend(vec![
                0;
                SECTORS_PER_TRACK * SECTOR_SIZE * TRACKS_PER_FLOPPY
                    - buffer.len()
            ]);
        }
        woz_file.file_checksum = CRC32.checksum(buffer.as_slice());

        for track_index in 0..TRACKS_PER_FLOPPY {
            let mut track_bits: Vec<u8> = Vec::new();
            let mut bit_writer = BitWriter::endian(track_bits.by_ref(), BigEndian);

            for sector in 0..SECTORS_PER_TRACK {
                let rs = SKEWING_TABLE[sector] as usize;
                let sector_start = track_index * SECTORS_PER_TRACK * SECTOR_SIZE + rs * SECTOR_SIZE;

                nibblize_sector(
                    DSK_VOLUME,
                    track_index as u8,
                    sector as u8,
                    &buffer.as_slice()[sector_start..sector_start + SECTOR_SIZE],
                    &mut bit_writer,
                );
            }

            // Flushes output stream to writer, if necessary. Any partial bytes are not flushed.
            bit_writer.flush().expect("");

            // Consumes writer and returns any un-written partial byte as a (bits, value) tuple.
            let (last_cnt, last_bits) = bit_writer.into_unwritten();

            if track_index == 0 {
                let v = track_bits.as_slice();
                debug!(
                    "[{}]",
                    v.iter().take(378).fold(String::new(), |acc, &num| acc
                        + format!("{:02X}", num).as_str()
                        + ", ")
                );
            }

            // Notice that since we write 16 sectors
            // then the number of bits in a track is always a multiple
            // of 16. So last_cnt is always == 0
            // Anyway, I prefer leave this code here as it
            // sounds more future proof...

            let mut nb_bits = track_bits.len() * 8;
            if last_cnt > 0 {
                track_bits.push(last_bits);
                nb_bits += last_cnt as usize;
            }

            // Now add the track to the index (repeating to have quarter tracks)

            woz_file.track_map.push(track_index);
            woz_file.track_map.push(track_index);
            woz_file.track_map.push(EMPTY_SUB_TRACK_INDEX);
            if track_index < TRACKS_PER_FLOPPY - 1 {
                woz_file.track_map.push(track_index + 1);
            } else {
                woz_file.track_map.push(EMPTY_SUB_TRACK_INDEX);
            }

            debug!(
                "DSK import: Track: {:03}, {} bytes, {} bits (+{})",
                track_index,
                track_bits.len(),
                nb_bits,
                last_cnt
            );

            woz_file
                .track_data
                .push(TrackData::new(track_bits, nb_bits));
        }

        woz_file.convert_flux_to_signal();
        woz_file
    }

    pub fn checksum(&self) -> Option<u32> {
        if self.source_file.is_some() {
            Some(self.file_checksum)
        } else {
            None
        }
    }

    fn convert_flux_to_signal(&mut self) {
        // -> Vec<SignalPart> {

        for track_ndx in 0..self.track_data.len() {
            let track: &mut TrackData = &mut self.track_data[track_ndx];

            // WOZ Spec : "The second factor is to remember that the
            // read head is an analog device and isn’t reading a
            // single point on the disk surface, but actually one that
            // has a radius of about 7.5µs. As long as it has a flux
            // within its window, either coming or going, it remains
            // happy. It is when there is nothing in this windows that
            // it starts freaking out. This means that we are
            // definitely in our happy place with two 0 bits as the
            // interval between them is 12µs (vs the window diameter
            // of 15µs)."
            // FIXME Instead of 16µs it should be 15.

            // See The Print Shop companion 4am crack


            if track.track_type == TrackDataType::Flux {
                // WOZ Spec : "Each byte represents a single flux
                // transition and its value is the number of ticks
                // since the previous flux transition. A single tick
                // is 125 nanoseconds."

                // DiskBlitz: "Applesauce reads the output of the
                // drive and therefore the MC3470.  To reconstruct the
                // signal from the flux track, each timing value is
                // the rising edge of a 1us wide pulse."

                let mut signal_chunks: Vec<SignalPart> = vec![];

                debug!("Converting flux track {} to signal parts", track_ndx);

                let mut time_offset: usize = 0;
                let mut count: usize = 0;

                debug!("Dropping {} bytes", track.track_bits.len() - track.nb_bits);
                for tdata_ndx in 0..track.nb_bits {
                    let b: u8 = track.track_bits[tdata_ndx];

                    if b == 255 {
                        count += 255
                    } else if b < 255 {
                        count += b as usize;

                        if count < 8 {
                            // Since the flux represents the output of the
                            // MC3470 and that output is always 1μsec long, then
                            // you can't have a duratin inferior to 1μsec
                            // because it'd mean the MC3470 ouwld have output a
                            // pulse shorter than 1μsec.
                            warn!(
                                "Warning: Broken WOZ ? {} nanosec duration in WOZ file at {}/{}",
                                b, tdata_ndx, track.nb_bits
                            );
                        } else if  count > STABLE_READ_LENGTH {
                            // Weak bits are there
                            if self.cleaned_weak_bits {
                                signal_chunks.push(SignalPart::new(
                                    SignalPartType::ReadPulse,
                                    time_offset,
                                    ONE_MICROSECOND,
                                ));

                                time_offset += ONE_MICROSECOND;

                                signal_chunks.push(SignalPart::new(
                                    SignalPartType::Silence,
                                    time_offset,
                                    STABLE_READ_LENGTH - ONE_MICROSECOND,
                                ));

                                time_offset += STABLE_READ_LENGTH - ONE_MICROSECOND;

                                info!("Weak bits at {}*125nanos (byte offset {}) on a count of {}*125nanos", time_offset, tdata_ndx, count);
                                signal_chunks.push(SignalPart::new(
                                    SignalPartType::WeakBits,
                                    time_offset,
                                    count - STABLE_READ_LENGTH,
                                ));

                                time_offset += count - STABLE_READ_LENGTH;
                            } else {
                                time_offset += count;
                                warn!("Weak bits detected, but WOZ image not cleaned ! Skiping");
                            }
                        } else {
                            // count <= STABLE_READ_LENGTH
                            // Regular pulse
                            //debug!("{}",count);
                            signal_chunks.push(SignalPart::new(
                                SignalPartType::ReadPulse,
                                time_offset,
                                ONE_MICROSECOND,
                            ));
                            time_offset += ONE_MICROSECOND;

                            let mut silence_duration = count - ONE_MICROSECOND;
                            if silence_duration < ONE_MICROSECOND / 2 {
                                warn!(
                                    "Warning: A silence is way too small: {}nanosec at {}/{}",
                                    count - ONE_MICROSECOND,
                                    tdata_ndx,
                                    track.nb_bits
                                );
                                silence_duration = ONE_MICROSECOND / 2;
                            }

                            signal_chunks.push(SignalPart::new(
                                SignalPartType::Silence,
                                time_offset,
                                silence_duration,
                            ));
                            time_offset += count - ONE_MICROSECOND;
                        }

                        count = 0;
                    }
                }

                track.set_flux(signal_chunks);
            } else {
                debug!(
                    "Converting track {}'s standard data to signal parts",
                    track_ndx
                );
                // Standard WOZ trk
                let mut signal_chunks: Vec<SignalPart> = vec![];
                let cursor = Cursor::new(&track.track_bits);
                let mut bit_reader = BitReader::endian(cursor, BigEndian);
                let mut silence_duration = 0.0;
                let mut time_offset = 0.0;

                // For a second I though that the spped of the
                // WOZ had to be set on the one of the CPU. It
                // seems it's not the case. I leave the comments
                // to remember that I asked myself that question.

                // let cpu_microsec = 1.000001 / 1_000_000.0;
                let cpu_microsec = 1.0 / 1_000_000.0;
                let cpu_nanosec = cpu_microsec / 1000.0;

                let optimal_bit_timing = (self.optimal_bit_timing as f64) * 125.0 * cpu_nanosec;

                for _ in 0..track.nb_bits {
                    let bit: bool = bit_reader.read_bit().unwrap();
                    if bit {
                        // bit == 1

                        // 16.0 works
                        // 12 works with PrintShop but not with Math
                        // 8 doesn't


                        // Silence has been accumulated in the past.
                        if silence_duration > STABLE_READ_DURATION {
                            debug!("Weak bits at {}", time_offset);
                            signal_chunks.push(SignalPart {
                                part_type: SignalPartType::Silence,
                                time: time_offset,
                                duration: STABLE_READ_DURATION,
                            });
                            time_offset += STABLE_READ_DURATION;
                            signal_chunks.push(SignalPart {
                                part_type: SignalPartType::WeakBits,
                                time: time_offset,
                                duration: silence_duration - STABLE_READ_DURATION,
                            });
                            time_offset += silence_duration - STABLE_READ_DURATION;
                        } else if silence_duration > 0.0 {
                            signal_chunks.push(SignalPart {
                                part_type: SignalPartType::Silence,
                                time: time_offset,
                                duration: silence_duration,
                            });
                            time_offset += silence_duration;
                        };

                        // A transition is encoded by a read pulse (1 µs long)
                        // followed by a silence of optimal_bit_timing - 1µs.

                        // WOZ Spec: If you are creating a floppy
                        // drive emulator for use with a real Apple
                        // II, then you will simply be stepping to the
                        // next bit in the bitstream at the rate
                        // specified by the Optimal Bit Timing in the
                        // INFO chunk. If the bit has a 1 value, then
                        // you send a 1µs pulse on the RDDATA
                        // line. For a 3.5 drive, the pulse width
                        // would be 0.5µs.

                        signal_chunks.push(SignalPart {
                            part_type: SignalPartType::ReadPulse,
                            time: time_offset,
                            duration: cpu_microsec,
                        });
                        time_offset += cpu_microsec;
                        silence_duration = optimal_bit_timing - cpu_microsec;
                    } else {
                        // bit == 0
                        silence_duration += optimal_bit_timing;
                    }
                } // for

                if silence_duration > 0.0 {
                    signal_chunks.push(SignalPart {
                        part_type: SignalPartType::Silence,
                        time: time_offset,
                        duration: silence_duration,
                    });
                };

                track.set_flux(signal_chunks);
            }
            debug!(
                "Flux has {} parts. Lasts {:.7} seconds.",
                track.flux.len(),
                track.flux_duration
            );
        }
    }
}

pub fn load_disk(file_path: &Path) -> WozFile {
    let f = File::open(file_path);
    if f.is_ok() {
        let mut buffer: Vec<u8> = Vec::new();
        f.unwrap().take(4).read_to_end(&mut buffer).expect("read");

        let woz_ident = str::from_utf8(&buffer[0..4]);



        // let path: &Path = Path::new("track0.bin");
        // use std::fs;
        // fs::write(path, woz.track_data[0].track_bits.as_slice()).unwrap();

        if woz_ident.is_err() {
            WozFile::read_dsk(file_path)
        } else {
            match woz_ident.unwrap() {
                "WOZ1" => WozFile::read(file_path),
                "WOZ2" => WozFile::read(file_path),
                _ => WozFile::read_dsk(file_path),
            }
        }
    } else {
        panic!("File not found '{}'", file_path.to_string_lossy());
    }
}


#[cfg(test)]
mod test {
    use super::*;
    use std::path::Path;

    fn _signal_to_bitvec(flux: &Vec<SignalPart> ) -> BitVec {
        let mut bits = BitVec::new();
        let mut _total_duration_signal = 0.0;
        for sig_part in flux {
            if sig_part.part_type == SignalPartType::ReadPulse {
                bits.push(true);
                bits.grow( (sig_part.duration/NANOSEC125).round() as usize - 1, false);
            } else {
                bits.grow((sig_part.duration/NANOSEC125).round() as usize, false);
            }
            _total_duration_signal += sig_part.duration;
        }
        debug!("byte len of flux {}", bits.len() / 8);
        bits
    }


    #[test]
    fn my_test() {
        let woz = WozFile::read(Path::new("additional/LOWTECH.WOZ"));

        println!("woz.optimal_bit_timing={}", woz.optimal_bit_timing);
        for i in 0..woz._nb_track_data() {
            if let Some(track) =  woz.track_data(i) {
                let bits = _signal_to_bitvec(&track.flux);
                let woz_bits = woz_standard_to_bits(&track);
                println!("{}: {} signal_to_bitvec:{} woz bits:{} WOZTrackData {} bytes => {} bits",
                    i, track.track_type, bits.len(), woz_bits.len(), track.track_bits.len(),
                    track.nb_bits*8*4*8);

                for signal in &track.flux[0..10] {
                    println!("{} {}", signal.part_type, signal.duration);
                }
                println!("Bits   :");
                for i in 0..128 {
                    print!("{}", if bits[i] {"1"} else {"."});
                }
                println!("");
                println!("WozBits:");
                for i in 0..128 {
                    print!("{}", if woz_bits[i] {"1"} else {"."});
                }
                println!("{}", bits.len().min(woz_bits.len()));

                for i in 0..bits.len().min(woz_bits.len()) {
                    if bits[i] != woz_bits[i] {
                        println!("\n******* fail at {}", i);
                        break;
                    }
                }

            }

        }
    }
}