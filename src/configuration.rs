use std::{path::PathBuf, str::FromStr};

use serde::{Deserialize, Serialize};

use crate::emulator::CpuType;
use std::fs;

#[derive(Serialize, Deserialize)]
pub struct Configuration {
    pub moby_api_key: Option<String>,
    pub cpu: CpuType,
    pub last_floppy_file: Option<PathBuf>,
    pub last_symbol_file: Option<PathBuf>,
    pub display_composite: Option<bool>,
    pub display_phase: Option<f32>,
    pub altcharset: Option<bool>
}


fn config_path() -> PathBuf {
    if let Some(mut d) = dirs::config_local_dir() {
        d.push("accurapple.cfg");
        d
    } else {
        PathBuf::from_str("accurapple.cfg").ok().unwrap()
    }
}

impl Configuration {
    fn new() -> Configuration {
        Configuration {
            moby_api_key : None,
            cpu: CpuType::Floooh6502,
            last_floppy_file: None,
            last_symbol_file: None,
            display_composite: None,
            display_phase: None,
            altcharset: None
        }
    }

    pub fn save(&self) {
        if let Ok(json) = serde_json::to_string(self) {
            let _ = fs::write(config_path(), json);
        }
    }

    pub fn load() -> Configuration {
        println!("Loading configuration at {}", config_path().to_string_lossy());
        if let Ok(json) = fs::read_to_string(config_path()) {
            if let Ok(cfg) = serde_json::from_str::<Configuration>(&json) {
                return cfg;
            }
        }

        Configuration::new()
    }
}
