use crate::a2::{ClockEvent, CYCLES_PER_FRAME, CYCLES_PER_SECOND, FRAMES_PER_SECOND};
use crate::emulator::OutputQueues;
use crate::sound::{SamplesFrame, SoundRenderer};
use log::{debug, trace};
use realfft::{num_complex::Complex, RealFftPlanner};
use std::collections::VecDeque;
use std::sync::{Arc, Mutex};

#[allow(non_upper_case_globals)] const sin: fn(f32) -> f32 = f32::sin;
#[allow(non_upper_case_globals)] const cos: fn(f32) -> f32 = f32::cos;
#[allow(non_upper_case_globals)] const sqrt: fn(f32) -> f32 = f32::sqrt;
#[allow(non_upper_case_globals)] const exp: fn(f32) -> f32 = f32::exp;
#[allow(non_upper_case_globals)] const log: fn(f32) -> f32 = |x| f32::log(x, 10.0f32);
#[allow(non_upper_case_globals)] const pow2: fn(f32) -> f32 = |x| x.powi(2);

use std::f32::consts::PI;

const NB_LOOPS1: f32 = 3.0;
const NB_LOOPS2: f32 = 4.0;
const SHO1_FREQUENCY: f32 = 3875.0;
const SHO2_FREQUENCY: f32 = 480.0;
const V0_LOW_FREQ: f32 = 5000.0;
const WEIGHT_LO_FREQ: f32 = 0.005;
const EXTENT: usize = 100;

// FIXME This should depend on the target freq. It shouldn't be a constant.
const FFT_OVERLAPPING_SAMPLES: usize = 880;

struct TriBuffer {
    v: Vec<f32>,
    inner_size: usize,
    nb_pushes: usize,
    ext_size: usize,
}

impl TriBuffer {
    fn new(inner_size: usize, ext_size: usize) -> TriBuffer {
        let v = vec![0.0; 3 * inner_size];
        TriBuffer {
            v,
            inner_size,
            nb_pushes: 0,
            ext_size,
        }
    }

    fn extended_size(&self) -> usize {
        self.inner_size + 2 * self.ext_size
    }

    fn push(&mut self, data: &[f32]) {
        assert!(data.len() == self.inner_size);
        // Shift left
        copy_within_a_slice(&mut self.v, self.inner_size, 0, self.inner_size * 2);
        for i in 0..self.inner_size {
            self.v[2 * self.inner_size + i] = data[i]
        }
        self.nb_pushes += 1;
    }

    fn is_filled(&self) -> bool {
        self.nb_pushes >= 3
    }

    fn input_area(&mut self) -> &mut [f32] {
        &mut self.v[2 * self.inner_size..]
    }

    fn shift(&mut self) {
        // Shift left

        // function works only for non overlapping area
        copy_within_a_slice(&mut self.v, self.inner_size, 0, self.inner_size);
        copy_within_a_slice(
            &mut self.v,
            self.inner_size * 2,
            self.inner_size,
            self.inner_size,
        );
        self.nb_pushes += 1;
    }

    fn middle(&self) -> &[f32] {
        assert!(self.is_filled());
        &self.v[self.inner_size..2 * self.inner_size]
    }

    fn middle_extended(&self) -> &[f32] {
        assert!(self.is_filled());
        &self.v[self.inner_size - self.ext_size..2 * self.inner_size + self.ext_size]
    }
}

struct FoldingBuffer {
    v: Vec<f32>,
    size: usize,
    wing_size: usize,
}

// From: https://stackoverflow.com/questions/45081768/efficiently-copy-non-overlapping-slices-of-the-same-vector-in-rust
// split the slice in two mutable slices
// then copy one subslice to other
fn copy_within_a_slice<T: Clone>(v: &mut [T], from: usize, to: usize, len: usize) {
    if from > to {
        let (dst, src) = v.split_at_mut(from);
        dst[to..to + len].clone_from_slice(&src[..len]);
    } else {
        let (src, dst) = v.split_at_mut(to);
        dst[..len].clone_from_slice(&src[from..from + len]);
    }
}

use std::ops::{Index, IndexMut};

impl FoldingBuffer {
    fn new(size: usize, wing_size: usize) -> FoldingBuffer {
        let mut v: Vec<f32> = Vec::with_capacity(size + 2 * wing_size);
        v.resize(size + wing_size, 0.0);

        FoldingBuffer { v, size, wing_size }
    }

    fn fold(&mut self) {
        // [-----------EEEEE]
        // becomes
        // [EEEEE...........]
        copy_within_a_slice(&mut self.v, self.size, 0, self.wing_size);
    }

    fn full_size(&self) -> usize {
        self.size + self.wing_size
    }
}

impl Index<i32> for FoldingBuffer {
    type Output = f32;
    fn index(&self, idx: i32) -> &f32 {
        &self.v[(idx + self.wing_size as i32) as usize]
    }
}

impl IndexMut<i32> for FoldingBuffer {
    fn index_mut(&mut self, idx: i32) -> &mut f32 {
        // even here I cannot get mutable reference to self.data[idx]
        return self.v.index_mut((idx + self.wing_size as i32) as usize);
    }
}

fn convolution(sample: &[f32], coeff: &[f32]) -> Vec<f32> {
    sample
        .windows(coeff.len())
        .map(|window| {
            window
                .iter()
                .zip(coeff.iter())
                .map(|(sample, coeff)| sample * coeff)
                .sum::<f32>()
        })
        .collect()
}

fn convolve_with_stride(
    sample: &[f32],
    start_ndx: f32,
    stride: f32,
    coeff: &[f32],
    dest: &mut [f32],
) {
    assert!((stride as usize) < sample.len());
    assert!(coeff.len() > (stride as usize));

    let mut ndx = start_ndx;
    let mut ndx_dest = 0;
    while (ndx as usize) + coeff.len() < sample.len() {
        // dest[ndx_dest] = 4000.0 * sample[ (ndx as usize) ];
        dest[ndx_dest] = 4000.0
            * sample[(ndx as usize)..(ndx as usize + coeff.len())]
                .iter()
                .zip(coeff.iter())
                .map(|(sample, coeff)| sample * coeff)
                .sum::<f32>();
        ndx += stride;
        ndx_dest += 1;
    }
}

fn low_pass_filter(src_freq: f32, target_freq: f32) -> Vec<f32> {
    const N: usize = 201;

    let mut v: Vec<f32> = vec![0.0; N];

    let cutoff = (target_freq / 2.0) / src_freq;

    for n in 0..N {
        // I guess I'm doing the : https://en.wikipedia.org/wiki/Finite_impulse_response#Window_design_method

        // Sinus Cardinal ideal low pass filter (that's normally a inifinite impulse response filter)
        // See: https://ccrma.stanford.edu/~jos/sasp/Ideal_Lowpass_Filter.html
        let x = (n as f32) - ((N / 2) as f32);
        let sinc = if x != 0.0 {
            sin(2.0 * cutoff * x * PI) / (2.0 * cutoff * PI * x)
        } else {
            1.0
        };

        // Blackmann (cleanly transform the IIR filter above in a FIR filter)

        let w = 0.42 - 0.5 * cos(2.0 * PI * (n as f32) / ((N - 1) as f32))
            + 0.08 * cos(4.0 * PI * (n as f32) / ((N - 1) as f32));
        v[n] = sinc * w;
    }

    let sum: f32 = v.iter().sum();
    v.iter_mut().map(|x| *x = 8000.0 * *x / sum);

    v
}

struct Oscillator {
    sho_freq: f32,
    nb_loops: f32,
    v: f32,
    minus_lambda_over_m_over_2: f32,
    a: f32,
    b: f32,
    sqrt_delta_over_2: f32,
    start_t: u64,
    tick_ndx: usize,
    target_freq: f32,
    s1: f32,
    s2: f32,
    p1: f32,
    p2: f32,
    t: u64,
}

impl Oscillator {
    pub fn new(sho_freq: f32, nb_loops: f32, target_freq: usize) -> Oscillator {
        Oscillator {
            sho_freq,
            nb_loops,
            v: 0.0,
            minus_lambda_over_m_over_2: 0.0,
            a: 0.0,
            b: 0.0,
            sqrt_delta_over_2: 0.0,
            start_t: 0,
            tick_ndx: 0,
            target_freq: target_freq as f32,
            s1: 0.0,
            s2: 0.0,
            p1: 0.0,
            p2: 0.0,
            t: 0
        }
    }

    pub fn switch(&mut self, tick: u64) {
        //self.compute_params(self.voltage(tick), self.derivative_at(tick), self.value_at(tick));
        let speed0 = self.derivative_at(tick);
        let h0 = self.value_at(tick);
        //println!("v0={}",speed0);
        self.tick_ndx += 1;

        let old_k = self.k();
        let old_v = self.v;

        self.compute_params(self.voltage(tick), speed0, h0);
        self.start_t = tick;

        self.s1 = (self.s1-old_v)/old_k*self.k() - self.v + old_v;
        self.s2 = (self.s2-old_v)/old_k*self.k() - self.v + old_v;
    }

    fn compute_params(&mut self, v: f32, v0: f32, h0: f32) {
        self.v = v;
        // Mass = 1Kg , that's what we choose.
        // let lambda_over_m = 2.0 * (log(2.0) - log(0.01)) / (self.nb_loops/self.sho_freq);
        // let beta_over_m = (pow2(lambda_over_m)+ pow2(4.0*PI*self.sho_freq))/4.0;

        let lambda_over_m = 2.0 * (log(2.0) - log(0.01)) / (self.nb_loops / self.sho_freq);
        let beta_over_m = (pow2(lambda_over_m) + pow2(4.0 * PI * self.sho_freq)) / 4.0;

        let delta = 4.0 * beta_over_m - pow2(lambda_over_m);
        assert!(delta > 0.0);

        //println!("{} {} {} h0:{}", lambda_over_m, beta_over_m, delta, h0);
        self.a = (h0 - self.v) / 2.0;
        self.b = -(v0 + self.a * lambda_over_m) / sqrt(delta);
        self.sqrt_delta_over_2 = sqrt(delta) / 2.0;
        self.minus_lambda_over_m_over_2 = -lambda_over_m / 2.0;

        // println!("compute_params: v={} a={} b={} {} {}", V, self.a, self.b,
        // 	 self.sqrt_delta_over_2, self.minus_lambda_over_m_over_2);

        let dx = 1.0/self.target_freq;
        self.p1 = exp(2.0*self.minus_lambda_over_m_over_2*dx);
        self.p2 = 2.0*exp(self.minus_lambda_over_m_over_2*dx)*cos(sqrt(delta)/2.0*dx)
    }

    fn k(&self) -> f32 {
        2.0*sqrt(self.a*self.a + self.b*self.b)
    }

    fn voltage(&self, tick: u64) -> f32 {
        // Useful on Prince Of Persia's intro

        // The first voltage will be != 0.
        let v: f32 = if self.tick_ndx % 2 == 1 {
            0.0
        } else {
            // cslt: c_ycles s_ince l_ast t_ick
            let cslt = tick - self.start_t;
            const DROP_ZONE_START: u64 = 30000; // cycles
            const DROP_ZONE_END: u64 = DROP_ZONE_START + 70000; // cycles

            if cslt < DROP_ZONE_START {
                1.0
            } else if (DROP_ZONE_START..DROP_ZONE_END).contains(&cslt) {
                //println!("zap {}",tick);
                let t =
                    (cslt - DROP_ZONE_START) as f32 / ((DROP_ZONE_END - DROP_ZONE_START) as f32);
                assert!((0.0..=1.0).contains(&t));
                // FIXME Needs to be fitted (or compared with the capacitor's
                // value so that I can get the actual RC parameter of the exp.)
                exp(-3.0 * t)
            } else {
                //println!("zup {}",tick);
                0.0
            }
        };

        //trace!("Voltage(): ndx={}, cycles={} -> {}", tick_ndx, cslt, v);
        v
    }

    pub fn convert_tick_to_t(&self, tick: u64) -> f32 {
        assert!(tick >= self.start_t);
        let duration = tick - self.start_t;

        ((duration as f64) / (CYCLES_PER_SECOND as f64)) as f32
    }

    pub fn next_value(&mut self) -> f32 {
        let s3 = self.s1*self.p1 + self.s2*self.p2;
        self.s1 = self.s2;
        self.s2 = s3;
        s3 + self.v
    }


    /// Value of the oscillator function at time `t` (in seconds).
    pub fn value_at(&self, tick: u64) -> f32 {
        assert!(tick >= self.start_t);
        let duration = tick - self.start_t;

        // Optimisation: we know the SHO has sabilized after a short
        // period. When this has been reached, there's no need to compute
        // anymore, just return the satbilized position.

        // About 1/100 th of a second
        if duration < (CYCLES_PER_SECOND as u64) / 10 {
            let t = ((duration as f64) / (CYCLES_PER_SECOND as f64)) as f32;
            let e = exp(self.minus_lambda_over_m_over_2 * t);
            let c = self.a * cos(self.sqrt_delta_over_2 * t);
            let s = self.b * sin(self.sqrt_delta_over_2 * t);
            2.0 * e * (c - s) + self.v
        } else {
            self.v
        }
    }

    /// First derivative of the oscillator function.
    pub fn derivative_at(&self, tick: u64) -> f32 {
        assert!(tick >= self.start_t);
        let duration = tick - self.start_t;

        if duration < (CYCLES_PER_SECOND as u64) / 10 {
            let t = (duration as f32) / (CYCLES_PER_SECOND as f32);
            let a = self.a;
            let b = self.b;
            let mlm2 = self.minus_lambda_over_m_over_2;

            let e = exp(mlm2 * t);
            let c = cos(self.sqrt_delta_over_2 * t) * (a * mlm2 - b * self.sqrt_delta_over_2);
            assert!(!c.is_nan());
            let s = sin(self.sqrt_delta_over_2 * t) * (a * self.sqrt_delta_over_2 + b * mlm2);
            // println!("der: t={}s e:{} c:{} s:{} self.sqrt_delta_over_2:{} mlm2:{} a:{} b:{}",
            // 	     t, e,c,s,self.sqrt_delta_over_2,mlm2,
            // 	     a,b);
            2.0 * e * (c - s)
        } else {
            0.0
        }
    }
}

struct OscillatorPair {
    sho1: Oscillator,
    sho2: Oscillator,
    start_tick: u64,
    nb_switches: usize,
}

impl OscillatorPair {
    fn new(target_freq: usize) -> OscillatorPair {
        let mut sho1 = Oscillator::new(SHO1_FREQUENCY, NB_LOOPS1, target_freq);
        sho1.compute_params(1.0, 0.0, 1.0);
        OscillatorPair {
            sho1,
            sho2: Oscillator::new(SHO2_FREQUENCY, NB_LOOPS2, target_freq),
            start_tick: 0,
            nb_switches: 0,
        }
    }

    fn switch(&mut self, tick: u64) {
        assert!(tick > self.start_tick, "{} > {} ?", tick, self.start_tick);
        let _clicks_since_start = tick - self.start_tick;
        let h0 = self.value_at(tick);
        let v0_hi_freq = self.derivative(tick);

        let (volt, v0_lo_freq) = if self.nb_switches % 2 == 0 {
            (0.0, V0_LOW_FREQ * self.sho1.v)
        } else {
            let v = self.voltage(tick);
            assert!((0.0..=1.0).contains(&v));
            (v, -V0_LOW_FREQ * v)
        };

        self.sho1.compute_params(volt, v0_hi_freq, h0);

        // The second oscillator will be added to the first one.
        // For that addition to work, the second oscillator's "volt" base
        // must be zero.
        self.sho2.compute_params(0.0, v0_lo_freq, 0.0);

        self.start_tick = tick;
        self.nb_switches += 1;
    }

    fn value_at(&self, tick: u64) -> f32 {
        assert!(
            tick >= self.start_tick,
            "{} not >= {}",
            tick,
            self.start_tick
        );

        let clicks_since_start = tick - self.start_tick;
        self.sho1.value_at(clicks_since_start)
            + WEIGHT_LO_FREQ * self.sho2.value_at(clicks_since_start)
    }

    fn next_value(&mut self) -> f32 {
        self.sho1.next_value()
    }

    fn derivative(&self, tick: u64) -> f32 {
        assert!(
            tick >= self.start_tick,
            "{} not >= {}",
            tick,
            self.start_tick
        );
        let clicks_since_start = tick - self.start_tick;
        self.sho1.derivative_at(clicks_since_start)
            + WEIGHT_LO_FREQ * self.sho2.derivative_at(clicks_since_start)
    }

    fn voltage(&self, tick: u64) -> f32 {
        // Useful on Prince Of Persia's intro

        // The first voltage will be != 0.
        let v: f32 = if self.nb_switches % 2 == 0 {
            0.0
        } else {
            //return 1.0;
            // cslt: c_ycles s_ince l_ast t_ick
            let cslt = tick - self.start_tick;
            const DROP_ZONE_START: u64 = 30000; // cycles
            const DROP_ZONE_END: u64 = DROP_ZONE_START + 70000; // cycles

            if cslt < DROP_ZONE_START {
                1.0
            } else if (DROP_ZONE_START..DROP_ZONE_END).contains(&cslt) {
                //println!("zap {}",tick);
                let t =
                    (cslt - DROP_ZONE_START) as f32 / ((DROP_ZONE_END - DROP_ZONE_START) as f32);
                assert!((0.0..=1.0).contains(&t));
                // FIXME Needs to be fitted (or compared with the capacitor's
                // value so that I can get the actual RC parameter of the exp.)
                exp(-3.0 * t)
            } else {
                //println!("zup {}",tick);
                0.0
            }
        };

        //trace!("Voltage(): ndx={}, cycles={} -> {}", tick_ndx, cslt, v);
        v
    }
}

struct Interval<T> {
    // b,e not included in the interval.
    // FIXME Strange
    b: T,
    e: T,
}

impl<T> Interval<T> {
    fn new(b: T, e: T) -> Interval<T>
    where
        T: Ord,
    {
        assert!(b < e, "I handle only non empty intervals");
        Interval { b, e }
    }

    fn intersect(&self, i: &Interval<T>) -> Option<Interval<T>>
    where
        T: Ord + Copy + std::fmt::Display,
    {
        if i.b < self.e && i.e > self.b {
            Some(Interval {
                b: self.b.max(i.b),
                e: self.e.min(i.e),
            })
        } else {
            // panic!("There must be an intersection self: [{},{}] other [{},{}]", self.b, self.e, i.b, i.e );
            None
        }
    }
}

struct OscillatorSampler {
    ticks: VecDeque<u64>,
    current_frame: usize,
    last_tick: u64,
    sho: OscillatorPair,
}

fn sigmoid(t: f32, c: f32) -> f32 {
    1.0 / (1.0 + exp(t + c))
}

impl OscillatorSampler {
    fn new(target_freq: usize) -> OscillatorSampler {
        OscillatorSampler {
            ticks: VecDeque::new(),
            current_frame: 0,
            last_tick: 0,
            sho: OscillatorPair::new(target_freq), // A silent oscillator, corresponding to the "last tick" set at zero.
        }
    }

    fn reset_ticks(&mut self) {
        self.ticks.clear();
        self.last_tick = 0;
        self.current_frame= 0;
        self.sho.start_tick = 0;
    }

    fn add_tick(&mut self, tick_in: u64) {
        trace!("add_tick on tick_in:{}", tick_in,);
        assert!(
            self.ticks.back().is_none() || *self.ticks.back().unwrap() < tick_in,
            "The arrow of time is not respected"
        );
        self.ticks.push_back(tick_in);
    }

    fn fill_buffer_with_sho(
        &mut self,
        frame_buffer: &mut [f32],
        buffer_offset: usize,
        frame_begin: u64,
        time_interval: &Interval<u64>,
    ) {
        assert!(time_interval.b >= frame_begin);

        for t in time_interval.b..time_interval.e {
            frame_buffer[buffer_offset + (t - frame_begin) as usize] = self.sho.next_value();
        }
    }

    fn get_a_frame(&mut self, buffer: &mut [f32], buffer_start: usize) {
        //assert!(buffer.len() - buffer_start <= CYCLES_PER_FRAME);

        // Compute frames limits
        // [frame_begin, frame_end[

        let frame_begin = (CYCLES_PER_FRAME * self.current_frame) as u64;
        let frame_end = (CYCLES_PER_FRAME * (self.current_frame + 1)) as u64;
        let interval_frame = Interval::new(frame_begin, frame_end);
        debug!("{} {}", frame_begin, frame_end);

        if self.last_tick == 0 {
            // FIXME last tick should be an option to represent the first tick
            if let Some(tick) = self.ticks.pop_front() {
                debug!("Init last tick");
                self.last_tick == tick;
            }
        }

        // Drop all ticks that happened before the beginning of this frame
        while let Some(tick) = self.ticks.front() {
            // The tick we're looking at will be the end of an interval.
            // And the question we ask is : does this interval intersect
            // the frame interval ? Remember the intervals are right-open
            // and left-closed.
            if *tick <= frame_begin {
                debug!("droppin 'coz {} <= {}", *tick, frame_begin);
                self.last_tick = self.ticks.pop_front().unwrap();
                self.sho.switch(self.last_tick);
            } else {
                break;
            }
        }

        let mut filled_samples = 0;

        loop {
            if let Some(tick) = self.ticks.front() {
                let new_tick = *tick;

                // Here we use intervals intersection to solve several cases at once:
                // - Last tick in frame, new tick in frame
                // - Last tick in frame, new tick after frame
                // - Last tick before frame, new tick in frame
                // - Last tick before frame, new tick after frame

                // Note that the last interval to intersect the "frame
                // interval" will be partly or completely contained in
                // the "frame interval".  If it's partly contained,
                // then it must be rendered also in the next "frame
                // interval".

                if let Some(interval) =
                    Interval::new(self.last_tick, new_tick).intersect(&interval_frame)
                {
                    //fill_buffer_with_sho(buffer, buffer_start, frame_begin, &interval, &self.sho);
                    self.fill_buffer_with_sho(buffer, buffer_start, frame_begin, &interval);

                    filled_samples += interval.e - interval.b;

                    // Is the interval running across two frames ?

                    if new_tick < frame_end {
                        // no
                        self.last_tick = new_tick;
                        self.sho.switch(new_tick);
                        self.ticks.pop_front();
                    } else {
                        // Yes. The interval will have to be finished in the next
                        // frame. But before that it has to be finished in the
                        // current one.
                        assert!(interval.e == frame_end);
                        break;
                    }
                } else {
                    panic!("???");
                }
            } else {
                //panic!("should not happen yet");
                if let Some(interval) =
                    Interval::new(self.last_tick, u64::MAX).intersect(&interval_frame)
                {
                    self.fill_buffer_with_sho(buffer, buffer_start, frame_begin, &interval);
                }
                break;
            }
        } // loop

        trace!("Filled samples {}", filled_samples);

        self.current_frame += 1;
    }
}

pub struct SpeakerRenderState {
    cycle: u64,
    pub tension: bool,
    pub sound: Arc<Mutex<VecDeque<i16>>>,
    events: VecDeque<ClockEvent>,
    pub target_freq: usize,
    is_recording: bool,
    pub recorded_ticks: Vec<u64>,

    record_start_cycle: u64,
    oscillo: OscillatorSampler,
    low_pass_filter: Vec<f32>,
    wb_1mhz: FoldingBuffer,
    wb_44khz: FoldingBuffer,
    tribuffer_44khz: TriBuffer,
    back_buf44khz: Vec<f32>,

    old_output_segment: Vec<f32>,
    output_segment: Vec<f32>,
    convo_buffer: Vec<Complex<f32>>,
    box_filter: Vec<Complex<f32>>,
    segment_padded: Vec<f32>,
    lpf44khz_fft: Vec<Complex<f32>>,
    filter_len: usize,
    ifft_conv_buffer: Arc<dyn realfft::ComplexToReal<f32>>,
    fft: Arc<dyn realfft::RealToComplex<f32>>,
    spectrum: Vec<Complex<f32>>,
}

impl SpeakerRenderState {
    pub fn new(
        target_freq: usize, // Hertz (usually 44100 or 48000
        _output_queues: Arc<Mutex<OutputQueues>>,
    ) -> SpeakerRenderState {
        let lpf = low_pass_filter(CYCLES_PER_SECOND as f32, target_freq as f32);
        let len_lpf = lpf.len();

        // Filter to apply on the lowpass-filtered at 44khz signal
        // to do the equalisation.
        let N = target_freq / FRAMES_PER_SECOND;
        let mut lpf44khz = low_pass_filter(target_freq as f32, target_freq as f32);
        let M = lpf44khz.len();

        for _i in 0..N - 1 {
            lpf44khz.push(0.0);
        }
        let mut planner = RealFftPlanner::<f32>::new();
        let fft = planner.plan_fft_forward(lpf44khz.len());
        let mut lpf44khz_fft: Vec<Complex<f32>> = fft.make_output_vec();
        let _ = fft.process(&mut lpf44khz, &mut lpf44khz_fft);

        let mut planner = RealFftPlanner::<f32>::new();
        let fft = planner.plan_fft_forward(N + M - 1);
        let spectrum: Vec<Complex<f32>> = fft.make_output_vec();

        let mut planner = RealFftPlanner::<f32>::new();
        let ifft_conv_buffer = planner.plan_fft_inverse(M + N - 1);
        let output = ifft_conv_buffer.make_output_vec();

        //         const FILTER_DATA: &[u8] = include_bytes!("../speaker/filter.bin");
        //
        //         let f44: Vec<Complex<f32>> = FILTER_DATA.chunks(8).map(|b|
        //             realfft::num_complex::Complex32::new(
        //                 f32::from_be_bytes(b[0..4].try_into().unwrap()),
        //            f32::from_be_bytes(b[4..8].try_into().unwrap()))).collect::<Vec<realfft::num_complex::Complex32>>();

        let f44: Vec<Complex<f32>> = vec![];
        SpeakerRenderState {
            cycle: 0,
            tension: false,
            sound: Arc::new(Mutex::new(VecDeque::new())),
            events: VecDeque::new(),
            target_freq,
            is_recording: false,
            recorded_ticks: Vec::new(),
            record_start_cycle: 0,
            oscillo: OscillatorSampler::new(target_freq),
            low_pass_filter: lpf,
            wb_1mhz: FoldingBuffer::new(CYCLES_PER_FRAME, len_lpf),
            wb_44khz: FoldingBuffer::new(target_freq / FRAMES_PER_SECOND, FFT_OVERLAPPING_SAMPLES),
            tribuffer_44khz: TriBuffer::new(target_freq / FRAMES_PER_SECOND, EXTENT),
            back_buf44khz: vec![0.0; 4096],
            old_output_segment: vec![0.0; M + N - 1],
            output_segment: output,
            convo_buffer: vec![Complex { re: 0.0, im: 0.0 }; (M + N - 1) / 2 + 1],
            box_filter: f44,
            segment_padded: vec![0.0; M + N - 1],
            lpf44khz_fft,
            filter_len: M,
            ifft_conv_buffer,
            fft,
            spectrum,
        }
    }

    pub fn start_recording(&mut self) {
        self.is_recording = true;
        self.recorded_ticks.clear();
        self.record_start_cycle = self.cycle;
    }

    pub fn stop_recording(&mut self) {
        self.is_recording = false;
    }

    fn has_more_events(&self) -> bool {
        !self.oscillo.ticks.is_empty()
    }

    pub fn reset_ticks(&mut self) {
        self.oscillo.reset_ticks()
    }

}

fn digest_events(events: &mut VecDeque<ClockEvent>, oscillo: &mut OscillatorSampler) {
    while !events.is_empty() {
        let next_cpu_event = events.back().unwrap();
        let next_cpu_event_tick = next_cpu_event.tick;
        events.pop_back();
        oscillo.add_tick(next_cpu_event_tick);
    }
}

impl SoundRenderer for SpeakerRenderState {
    fn record(&mut self, event: ClockEvent) {
        // Is recording set up ?
        if self.is_recording {
            self.recorded_ticks
                .push(event.tick - self.record_start_cycle);
        }

        self.events.push_front(event);
    }

    fn samples(&self) -> Arc<Mutex<VecDeque<i16>>> {
        Arc::clone(&self.sound)
    }

    fn process_frame(&mut self) -> SamplesFrame {
        assert!(
            self.target_freq % FRAMES_PER_SECOND == 0,
            "Rounding will trigger problems"
        );
        // See comment in the sound CPAL thread code
        let spf = self.target_freq / FRAMES_PER_SECOND;
        debug!("process frame: spf={}", spf);

        digest_events(&mut self.events, &mut self.oscillo);

        // drop(vecdeque.drain(..hi))
        let l = self.wb_1mhz.v.len();
        self.wb_1mhz.fold();
        self.wb_44khz.fold();

        let N = self.target_freq / 50;
        let M = self.filter_len; // this buffer is padde = >unpad
                                 //println!("M={} N={}",M,N);

        // Downsampling from 1MHz to 44Khz
        self.oscillo
            .get_a_frame(&mut self.wb_1mhz.v[self.wb_1mhz.wing_size..l], 0);

        convolve_with_stride(
            &self.wb_1mhz.v,
            0.0,
            (CYCLES_PER_SECOND as f32) / (self.target_freq as f32),
            &self.low_pass_filter,
            &mut self.segment_padded[0..N],
        );

        for i in N..N + M - 1 {
            self.segment_padded[i] = 0.0;
        }

        // Filtering at 44Khz
        // let mut planner = RealFftPlanner::<f32>::new();
        // let fft = planner.plan_fft_forward(N+M-1);
        // let mut spectrum: Vec<Complex<f32>> = fft.make_output_vec();
        let _ = self.fft
            .process(&mut self.segment_padded, &mut self.spectrum);

        // Convolution product in freq domain to do the eqaulization
        for i in 0..self.spectrum.len() {
            // 32000.0/6_000_000.0
            // Volume adjustment constant was hand set :-(
            // The music of windwalker is especially loud.
            self.convo_buffer[i] = 1.0 / 13082.0 * self.spectrum[i] * self.lpf44khz_fft[i];
        }

        let _ = self.ifft_conv_buffer
            .process(&mut self.convo_buffer, &mut self.output_segment);

        //let mut m = 0.0f32;
        for i in 0..M - 1 {
            self.output_segment[i] += self.old_output_segment[N + i];
            //m = m.max(self.output_segment[i])
        }
        //println!("{}",m);

        // L1 Cache on my core2Duo: 32Kb data, 32Kb code; 64 bytes wide I thuink.
        // L2 cache 4MB (I guess data)
        // 20280*4 = 80KB => it fits in L2 but not in L1.
        // Convolution is totally sequential => each cache miss loads 64 bytes
        // so this gives me 64/4 = 16 mul/add to perform before the next cache hit.
        // Now convolution are 100 items wide. So there will be a lot of overlap
        // between one convolution and the next one => cache will do fine.

        let r = SamplesFrame::from(&self.output_segment[0..N].to_vec());
        self.old_output_segment.clone_from(&self.output_segment);

        r
    }
}


#[cfg(test)]
mod SpeakerTests {
    use super::{Interval, Oscillator, OscillatorPair};
    use itertools::Itertools;

    use std::{
        path::Path,
        sync::{Arc, Mutex},
        time::Instant,
    };

    use super::{SpeakerRenderState};
    use crate::sound::SoundRenderer;
    use crate::{
        a2::{ClockEvent, CYCLES_PER_SECOND},
        emulator::{save_i16_to_wav_stereo, OutputQueues},
    };

    #[test]
    fn test_sho() {
        let mut sho = Oscillator::new(3875.0, 8.0, 44100);
        for tick in (1000..2200).step_by(10) {
            sho.switch(tick);
        }
    }

    #[test]
    fn test_interval() {
        let a = Interval::new(10, 20);

        assert!(a.intersect(&Interval::new(5, 10)).is_none());
        assert!(a.intersect(&Interval::new(20, 25)).is_none());

        let _r = a.intersect(&Interval::new(12, 18)).unwrap();
        let _r = a.intersect(&Interval::new(0, 30)).unwrap();
        let _r = a.intersect(&Interval::new(5, 15)).unwrap();
        let _r = a.intersect(&Interval::new(15, 25)).unwrap();
        let _r = a.intersect(&Interval::new(10, 15)).unwrap();
        let _r = a.intersect(&Interval::new(15, 20)).unwrap();

        let _r = a.intersect(&Interval::new(0, 5)).is_none();
        let _r = a.intersect(&Interval::new(25, 30)).is_none();
    }

    #[test]
    fn test_one_frame() {
        // cargo test --release test_one_frame -- --nocapture

        // Lowpass for downsampling: seems OK (but what about edges of convolution ?)

        let output_queue = Arc::new(Mutex::new(OutputQueues::new()));
        let mut spkr = SpeakerRenderState::new(44100, output_queue);

        // let data = &std::fs::read(Path::new(
        //     r#"C:\Users\StephaneC\works\accurapple\speaker\recordings\bug_attack.ticks"#)).unwrap()[..];

        // let mut p = PathBuf::from(std::env::current_dir().ok().unwrap());
        // p.push(Path::new(r#"speaker/recordings/captain_goodnight.ticks"#));
        // println!("{}", p.display());

        let data = &std::fs::read(Path::new(
            //r#"speaker/recordings/windwalker.ticks"# // noulg
            //r#"speaker/recordings/captain_goodnight.ticks"# // ulg
            //r#"speaker/recordings/conan.bin"# // ulg
            //r#"speaker/recordings/darklord.bin"# // ulg
            //r#"speaker/recordings/archon.bin"# // ulg
            //r#"speaker/recordings/rtsynth.bin"# // ulg
            //r#"speaker/recordings/windwalker.bin"# // ulg
            r#"speaker/recordings/boulder_dash.bin"#, // ulg
                                                      //r#"speaker/recordings/death_sword.ticks"#
                                                      //r#"speaker/recordings/plasmania.ticks"#
                                                      //r#"speaker/recordings/boulder_dash.ticks"#
                                                      //r#"speaker/recordings/pickadilly.ticks"#
        ))
        .unwrap()[..];

        let now = Instant::now();

        let first_tick = u64::from_be_bytes(data[0..8].try_into().expect(""));
        for i in (0..data.len()).step_by(8) {
            let tick = u64::from_be_bytes(data[i..i + 8].try_into().expect(""));
            spkr.record(ClockEvent {
                tick: tick - first_tick,
                event: crate::a2::ClockEventData::SpeakerIORead { addr: 0xc030 },
            });
        }

        let mut wav: Vec<i16> = Vec::new();

        // Send events to sampler
        /*
        // Record the OneMHz signal
        digest_events(&mut spkr.events, &mut spkr.oscillo);

        let mut one_mhz_buffer = vec![0f32; 600*CYCLES_PER_FRAME];
        for i in 0..600 {
            let s = i * CYCLES_PER_FRAME;
            spkr.oscillo.get_a_frame(&mut one_mhz_buffer, s);
        }

        let mut outbuf = vec![0u8;0];
        for v in &one_mhz_buffer{
            for b in v.to_le_bytes(){
                outbuf.push(b);
            }
        }
        let mut f = std::fs::File::create("speaker/one_mhz.bin").expect("success");
        f.write_all(outbuf.as_slice()).expect("success");
        */

         // 1.3.4
        while wav.is_empty() || spkr.has_more_events() {
            let f = spkr.process_frame();

            let _s = 0;
            for x in 0..f.len() {
                //s += f.left[x];
                wav.push(f.left[x]);
                wav.push(f.right[x]);
            }
            //println!("Frame {}, len:{}, sum{}",i, f.len(), s);
        }

        let elapsed_time = now.elapsed();
        println!(
            "Running slow_function() took {} milliseconds. {:.1}ms per frame",
            elapsed_time.as_millis(),
            elapsed_time.as_millis() as f32 / 200f32
        );

        save_i16_to_wav_stereo(wav, Path::new("test.wav"), spkr.target_freq);
    }

    #[test]
    fn test_read_filter_data() {
        let output_queue = Arc::new(Mutex::new(OutputQueues::new()));
        let spkr = SpeakerRenderState::new(44100, output_queue);
        for c in spkr.box_filter.iter().take(5) {
            println!("{:}", c);
        }
    }

    #[test]
    fn test_voltage_tricks() {
        // cargo test --release test_voltage_tricks -- --nocapture
        let output_queue = Arc::new(Mutex::new(OutputQueues::new()));
        let mut spkr = SpeakerRenderState::new(44100, output_queue);
        println!(
            "ndx:{} start_tick:{}",
            spkr.oscillo.sho.nb_switches, spkr.oscillo.sho.start_tick
        );
        println!("volt:{}", spkr.oscillo.sho.voltage(10));
        println!("volt:{}", spkr.oscillo.sho.voltage(29000));
        println!("volt:{}", spkr.oscillo.sho.voltage(29999));
        println!("volt:{}", spkr.oscillo.sho.voltage(30000));
        println!("volt:{}", spkr.oscillo.sho.voltage(30001));
        println!("volt:{}", spkr.oscillo.sho.voltage(50000));
        println!("volt:{}", spkr.oscillo.sho.voltage(99000));
        println!("volt:{}", spkr.oscillo.sho.voltage(100000));

        const SWITCH_TICK: u64 = 1000;
        spkr.oscillo.sho.switch(SWITCH_TICK);
        println!("Switch");
        println!(
            "ndx:{} start_tick:{}",
            spkr.oscillo.sho.nb_switches, spkr.oscillo.sho.start_tick
        );
        println!("volt:{}", spkr.oscillo.sho.voltage(SWITCH_TICK + 10));
        println!("volt:{}", spkr.oscillo.sho.voltage(SWITCH_TICK + 30000));
        println!("volt:{}", spkr.oscillo.sho.voltage(SWITCH_TICK + 100000));
    }

    #[test]
    fn test_oscillator_pair() {
        // cargo test --release test_oscillator_pair -- --nocapture
        let mut op = OscillatorPair::new(44100);
        let FIRST_STOP = (0.001 * CYCLES_PER_SECOND as f64) as u64;
        let SECOND_STOP = FIRST_STOP + (0.003122 * CYCLES_PER_SECOND as f64) as u64;
        let THIRD_STOP = SECOND_STOP + (0.01 * CYCLES_PER_SECOND as f64) as u64;
        let FOURTH_STOP = THIRD_STOP + (0.01 * CYCLES_PER_SECOND as f64) as u64;
        let FIFTH_STOP = FOURTH_STOP + (0.01 * CYCLES_PER_SECOND as f64) as u64;

        let mut r = vec![0.0; 0];
        let step = 1;
        for t in (0..FIRST_STOP).step_by(step) {
            r.push(op.value_at(t));
        }

        op.switch(FIRST_STOP);
        for t in (FIRST_STOP..SECOND_STOP).step_by(step) {
            r.push(op.value_at(t));
        }

        op.switch(SECOND_STOP);
        for t in (SECOND_STOP..THIRD_STOP).step_by(step) {
            r.push(op.value_at(t));
        }

        op.switch(THIRD_STOP);
        for t in (THIRD_STOP..FOURTH_STOP).step_by(step) {
            r.push(op.value_at(t));
        }

        op.switch(FOURTH_STOP);
        for t in (FOURTH_STOP..FIFTH_STOP).step_by(step) {
            r.push(op.value_at(t));
        }

        let mut s = r.iter().map(|v| format!("{:.3}", v)).join(",");
        s.push('\n');
        //println!("{}",s);
        use std::fs;
        fs::write("C:/Users/StephaneC/works/accurapple/speaker/test", s)
            .expect("Unable to write file");
    }
}
