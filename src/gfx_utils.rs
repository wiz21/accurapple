pub const SCREEN_WIDTH: u32 = 280 * 2;
pub const SCREEN_HEIGHT: u32 = 312;
pub const TEXT_PAGE1_OFFSET: u16 = 0x400;
pub const TEXT_PAGE2_OFFSET: u16 = 0x800;
pub const HGR_PAGE1_OFFSET: u16 = 0x2000;
pub const HGR_PAGE2_OFFSET: u16 = 0x4000;

pub fn hgr_address(y: usize) -> usize {
    let ofs = match y {
        0..=63 => 0,
        64..=127 => 0x28,
        128..=191 => 0x50,
        _ => panic!("Invalid y={} coordinate to compute HGR address", y),
    };

    let i = (y % 64) >> 3; // div by 8
    let j = (y % 64) % 8;

    ofs + 0x80 * i + 0x400 * j
}

pub fn hgr_address_to_line(addr: usize) -> usize {
	assert!( addr <= 0x2000, "The address must be relative to the beginning of the page");
	let i = (addr >> 7) & 0b111; // (y // 64) // 8
    let j = (addr >> 10) & 0b111; // (y // 64) % 8
    let ofs = addr & 0x7F;
    // Real computation is (ofs // 0x28) * 64
    let ofs2 = ((ofs >> 3) & 0b1111) / 5;

    i*8+j + ofs2*64
}

pub fn text_address(display_line: usize) -> usize {
    let y = display_line / 8;
    let ofs = match y {
        0..=7 => 0,
        8..=15 => 0x28,
        16..=23 => 0x50,
        _ => panic!("Beam line not in display area: {}", display_line),
    };

    let i = y % 8;
    ofs + 0x80 * i
}
