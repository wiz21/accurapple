use std::{cell::RefCell, rc::Rc};

#[derive(PartialEq)]
pub enum IrqLineLevel {
    Low,
    High
}

pub struct BreakPointTrigger {
	triggered: bool,
    breakpoint_on_floppy: bool,
    breakpoint_on_floppy_triggered: bool,
    breakpoint_on_floppy_track:Option<usize>,
    breakpoint_on_floppy_track_triggered: bool,
}

impl BreakPointTrigger {
    pub fn new() -> BreakPointTrigger {
        BreakPointTrigger{
            triggered:false,
            breakpoint_on_floppy: false,
            breakpoint_on_floppy_triggered: false,
            breakpoint_on_floppy_track: None,
            breakpoint_on_floppy_track_triggered: false,
        }
    }

    pub fn enable_break_on_floppy_track(&mut self, track: Option<usize>) {
        self.breakpoint_on_floppy_track = track;
    }

    pub fn break_on_floppy_track(&mut self, track: usize) {
        if let Some(t) = self.breakpoint_on_floppy_track {
            println!("wtf {} / {}", t, track);
            if t == track {
                self.breakpoint_on_floppy_track_triggered = true;
                self.triggered = true;
            }
        }
    }


    pub fn enable_break_on_floppy(&mut self) {
        self.breakpoint_on_floppy = !self.breakpoint_on_floppy;
    }

    pub fn break_on_floppy(&mut self) {
        if self.breakpoint_on_floppy {
            self.breakpoint_on_floppy_triggered = true;
            self.triggered = true;
        }
    }

    pub fn has_triggered(&mut self) -> bool {
        if self.triggered {
            self.triggered = false;
            self.breakpoint_on_floppy = false;
            self.breakpoint_on_floppy_triggered = false;
            self.breakpoint_on_floppy_track= None;
            self.breakpoint_on_floppy_track_triggered = false;
            true
        } else {
            false
        }
    }
}


// Rc: shared pointer (distribut with clone), RefCell
// interior mutability
pub type BreakPointTriggerRef = Rc<RefCell<BreakPointTrigger>>;

use serde::{Serialize, Deserialize};
use strum_macros::Display;
#[derive(Serialize, Deserialize)]
struct GenericPeripheralStringStatus {
    status: String
}

#[derive(Display, PartialEq)]
pub enum IrqChange {
    PullUp,
    PullDown
}

pub trait GenericPeripheral {

    fn get_name(&self) -> String;

    /* Each peripheral has to know its slot. The slot
    number is used to point to the slot in various places,
    especially when triggering IRQ : the peripheral signals
    to the CPU it makes an IRQ and identifies itself
    with its slot number (that is useful for call backs).

    Note: we'd prefered to have objects/callback pointers
    but this makes rust quite angry regarding ownership
    and circular dependencies. */
    fn get_slot(&self) -> usize;

    fn set_debugger(&mut self, _breakpoint: BreakPointTriggerRef) {}

    fn read_io(&mut self, _addr_: u16, mem_byte: u8, _cycle:u64) -> (u8, Option<Vec<IRQRequest>>, Option<IrqChange>) {
        // Compute the value of a memory read at addr
        // knowing that some peripheral listens at that addr.
        // So we give back what the peripheral gives.
        // This has side effects on the peripheral side.
        // The cycle is given so that the peripheral can compute its progress
        // mem_byte is a syntactical shortcut : it allows
        // the peripheral to work transparrently, ie when one
        // reads memory, the read operation return the content
        // of the memory (mem_byte), not a value given by the
        // peripheral.

        (mem_byte, None, None)
    }

    fn write_io(&mut self, _addr: u16, _data: u8, _cycle:u64) -> (Option<Vec<IRQRequest>>, Option<IrqChange>) {
        // Following a write to memory, this transfers the
        // written data to the peripheral. Actually, those two things
        // occur simultaneously I think...
        (None, None)
    }


    fn ack_irq(&mut self, _req:IRQRequest, _cycle: u64) ->(Option<Vec<IRQRequest>>, Option<IrqChange>) {
    	(None, None)
    }

    fn irq_line_level(&self) -> IrqLineLevel {
        IrqLineLevel::High
    }

    fn cycle_tick(&mut self, _data_on_bus: u8) {
    }


    fn status_str(&self, _cycle: u64) -> String {
        //let s = GenericPeripheralStringStatus { status: "No status".to_string() };
	    "No Status".to_string()
    }

    fn reset(&mut self) {
    }

}


pub struct DummyPeripheral {
    pub slot: usize
}

impl DummyPeripheral {
    pub fn new(slot:usize) -> DummyPeripheral {
        DummyPeripheral {
            slot
        }
    }
}

impl GenericPeripheral for DummyPeripheral {
    fn get_name(&self) -> String {
    	String::from("Dummy")
    }

    fn get_slot(&self) -> usize {
	    self.slot
    }
}


#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum IRQAction {
    ClearAll,
    Replace,

    // FIXME Rmeeber ths was introduced with goal of using service IRQ to handle
    // keyboard events with more accurate time precision.
    Append
}

// enum KeyActionType {
//     Pressed, Released
// }
// struct KeyAction {
//     key: Key,
//     cycle: u64,
//     action: KeyActionType
// }


#[derive(Copy, Clone)]
pub struct IRQRequest {
    /* When to trigger the IRQ */
    pub irq_cycle: u64,

    /* Which peripheral (its slot) asked for the IRQ.
    This will be used to call back ack_irq() function
    on the peripheral. */
    pub source_slot: usize,

    /* Some peripheral have "sub peripheral". For example,
    the MockingBoard has two 6522 which can each be the source
    of an IRQ. This allows the peripheral to distinguish
    the sub peripheral (ie by which 6522 the IRQ was requested */
    pub sub_source: usize,

    // /* The keyboard is an IRQ source. If it is that source,
    //    then source_slot and sub_source have no meaning. */
    // pub keyboard_irq: ,

    /* Some IRQ should trigger the IRQ of the CPU, some are just
    there to handle the device at specific points in time. For example,
    the floppy disk stepper position needs to be updated on a regular basis.
    */


    pub trigger_cpu: bool,
    /* What to do to handle this request when we receive it:
     - ClearAll : clear all pending IRQ requests coming from
       the same peripheral
    - Replace : idem */
    // FIXME This should n't be in this struct
    pub action: IRQAction
}


impl IRQRequest {
    pub fn new(irq_cycle: u64,
	    source_slot: usize,
	    sub_source: usize,
	    trigger_cpu: bool,
	    action: IRQAction) -> IRQRequest {
            assert!(action != IRQAction::ClearAll || !trigger_cpu,
                "A IRQAction::ClearAll can't affect the CPU, it's only there for internal \
                management of the emulator");
        IRQRequest {
            irq_cycle,
            source_slot,
            sub_source,
            trigger_cpu,
            action
        }
    }
}
