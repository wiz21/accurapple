use log::{debug, trace, warn};
use crate::a2::{CYCLES_PER_FRAME,CYCLES_PER_LINE,FIRST_NON_BLANK_LINE,ClockEventData,AppleII,
		MIXED_ON, MIXED_OFF,
		PAGE2_ON, PAGE2_OFF,
		TEXT_ON, TEXT_OFF,
		HIRES_ON, HIRES_OFF,
		DHIRES_ON, DHIRES_OFF, GfxMode, GfxBaseMode };

pub const SCREEN_WIDTH: u32 = 280 * 2;
pub const SCREEN_HEIGHT: u32 = 312;
pub const TEXT_PAGE1_OFFSET: u16 = 0x400;
pub const TEXT_PAGE2_OFFSET: u16 = 0x800;
pub const HGR_PAGE1_OFFSET: u16 = 0x2000;
pub const HGR_PAGE2_OFFSET: u16 = 0x4000;

pub struct GfxRenderState {
    cycle: u64,
    text: bool,
    page2: bool,
    mixed: bool,
    dhgr: bool,
    gr: bool,
    mode: GfxMode,
    gfx_mem: Vec<u8>,
    gfx_aux_mem: Vec<u8>
}

impl GfxRenderState {
    pub fn new() -> GfxRenderState {
	GfxRenderState {
            cycle: 0,
	    text: true,
	    page2: false,
	    mixed: false,
	    dhgr: false,
	    gr: false,
	    mode: GfxMode{mode:GfxBaseMode::Text40,mixed:None,page2:false, an3:0, altcharset:1},
	    // Only the part of the RAM dedicated to
	    // display. Starting from 0 to avoid addresses conversion
	    // when updating this array.
	    gfx_mem: vec![0;0x6000],
	    gfx_aux_mem: vec![0;0x6000],
	}
    }

}



pub fn hgr_address(y: usize) -> usize {
    let ofs = match y {
        0..=63 => 0,
        64..=127 => 0x28,
        128..=191 => 0x50,
        _ => panic!("Invalid y={} coordinate to compute HGR address", y),
    };

    let i = (y % 64) >> 3; // div by 8
    let j = (y % 64) % 8;

    return ofs + 0x80 * i + 0x400 * j;
}

pub fn hgr_address_to_line(addr: usize) -> usize {
	assert!( addr <= 0x2000, "The address must be relative to the beginning of the page");
	let i = (addr >> 7) & 0b111; // (y // 64) // 8
    let j = (addr >> 10) & 0b111; // (y // 64) % 8
    let ofs = addr & 0x7F;
    // Real computation is (ofs // 0x28) * 64
    let ofs2 = ((ofs >> 3) & 0b1111) / 5;

    return i*8+j + ofs2*64;
}

pub fn text_address(display_line: usize) -> usize {
    let y = display_line / 8;
    let ofs = match y {
        0..=7 => 0,
        8..=15 => 0x28,
        16..=23 => 0x50,
        _ => panic!("Beam line not in display area: {}", display_line),
    };

    let i = y % 8;
    return ofs + 0x80 * i;
}


fn swap(n: u8) -> u8 {
    if n == 1 {
        return 2;
    } else if n == 2 {
        return 1;
    }
    return n;
}

fn gr_stripe_to_pixels(
    main_ram: &Vec<u8>,
    rgba: &mut [u8],
    page: usize,
    y: usize,
    byte_ofs_begin: usize,
    byte_ofs_end: usize,
) {
    if y < FIRST_NON_BLANK_LINE || y >= FIRST_NON_BLANK_LINE+192 {
	// We're in the VBL
	return
    }

    let mut ofs = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_begin;
    let ofs_end = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_end;
    let mut rgb_i = (y * (SCREEN_WIDTH as usize) + byte_ofs_begin * 7*2) * 4 ;

    let half_line = ((y-FIRST_NON_BLANK_LINE) / 4) % 2;

    while ofs < ofs_end {
        let pixel = if half_line % 2 == 0 {
            main_ram[ofs] & 0b00001111
        } else {
            (main_ram[ofs] & 0b11110000) >> 4
	} as usize;

	for _ in 0..14 {
	    rgba[rgb_i..rgb_i + 4].copy_from_slice(&GR_COLORS[pixel*4..pixel*4 + 4]);
	    rgb_i += 4;
	}

	ofs += 1;
    }
}


fn gr_stripe_to_pixels_black_white(
    main_ram: &Vec<u8>,
    rgba: &mut [u8],
    page: usize,
    y: usize,
    byte_ofs_begin: usize,
    byte_ofs_end: usize,
) {
    if y < FIRST_NON_BLANK_LINE || y >= FIRST_NON_BLANK_LINE+192 {
	// We're in the VBL
	return
    }

    let mut ofs = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_begin;
    let ofs_end = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_end;
    let mut rgb_i = (y * (SCREEN_WIDTH as usize) + byte_ofs_begin * 7*2) * 4 ;


    /*

    Sather page 3-8:
    PHASE0 = 1 Mhz signal
    PHASE1 = 1 Mhze (opposite phase to PHASE0)

    Saterh p 3-13:
    H0-H5 : horizontal scanner count (H0 changes on every byte)

    Sather Table 8-10 (p 8-27, 237):

    The byte from RAM is split in two 4 bits packets.

    if VC (vertical resolution counter C, which flips every 4 lines) == 1: take the 4 LSB, if == 0 the 4 MSB.

    "The contents of the LORES areaofthe video ROM are inverted double
    patterns, generated from VID3— VIDO or VID7— VID4 depending on VC;
    rotated or not rotated two bits depending on HO. The VC varia-
    tion results in VID3— being processed as the upper block pattern
    and VID7— 4 being processed as the lower block pattern. The HO
    variation causes a stored pattern to generate the same color
    whether it is stored in an even or odd RAM"

    If H0 == 0 : the latched byte is ROL'ed twice to the left
    If H0 == 1 : the latched byt is left untouched
    */

    let vc = ((y-FIRST_NON_BLANK_LINE) / 4) % 2;

    while ofs < ofs_end {
        let pixel = if vc % 2 == 0 {
            main_ram[ofs] & 0b00001111
        } else {
            (main_ram[ofs] & 0b11110000) >> 4
	} as u8;

	let h0 = ofs & 1;
	let dp = (pixel << 4) | pixel;
	let mut pattern = if h0 == 0 { dp.rotate_left(2) } else {dp};

	for _ in 0..14 {
	    let v = (pattern & 1) * 0xFF;
	    rgba[rgb_i] = v;
	    rgba[rgb_i+1] = v;
	    rgba[rgb_i+2] = v;
	    rgba[rgb_i+3] = 0xFF; // Alpha channel
	    rgb_i += 4;
	    pattern = pattern.rotate_right(1);
	}

	ofs += 1;
    }
}



fn dhgr_stripe_to_pixels(
    main_ram: &Vec<u8>,
    aux_ram: &Vec<u8>,
    rgba: &mut [u8],
    page: usize,
    y: usize,
    byte_ofs_begin: usize,
    byte_ofs_end: usize,
) {
    if y < FIRST_NON_BLANK_LINE || y >= FIRST_NON_BLANK_LINE+192 {
	return
    }

    // if byte_ofs_begin & 1 > 0 {
    // 	let mut ofs = page + hgr_address(y - 60) + byte_ofs_begin;
    // 	let rgb_i = (y * (SCREEN_WIDTH as usize) + (byte_ofs_begin/2) * 7 * 2) * 4 + ;

    // 	byte_ofs_begin += 1;
    // }

    let ofs_hacked = (byte_ofs_begin / 2) * 2;

    let mut ofs = page + hgr_address(y - FIRST_NON_BLANK_LINE) + ofs_hacked;
    let ofs_end = page + hgr_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_end;
    let mut rgb_i = (y * (SCREEN_WIDTH as usize) + (ofs_hacked/2) * 7 * 4) * 4;

    while ofs < ofs_end {
	// AUX_RAM is Text Page 1X (not page2, cf. page 102 of A2c refrence manual).

	let p_a = (aux_ram[ofs]     &  0b0001111) as usize;
	let p_b = (((aux_ram[ofs]   &  0b1110000) >> 4) | ((main_ram[ofs] & 0b1) << 3)) as usize;
	let p_c = ((main_ram[ofs]   &  0b0011110) >> 1) as usize;
	let p_d = (((main_ram[ofs]  &  0b1100000) >> 5) | ((aux_ram[ofs+1] & 0b11) << 2)) as usize;
	let p_e = ((aux_ram[ofs+1]  &  0b0111100) >> 2) as usize;
	let p_f = (((aux_ram[ofs+1] &  0b1000000) >> 6) | ((main_ram[ofs+1] & 0b111) << 1)) as usize;
	let p_g = ((main_ram[ofs+1] &  0b1111000) >> 3) as usize;

	fn pixel_copy(rgba: &mut [u8], rgb_i:usize, pixel:usize) -> usize {
	    rgba[rgb_i+4*0..rgb_i+4*0 + 4].copy_from_slice(&DHGR_COLORS[pixel*4..pixel*4 + 4]);
	    rgba[rgb_i+4*1..rgb_i+4*1 + 4].copy_from_slice(&DHGR_COLORS[pixel*4..pixel*4 + 4]);
	    rgba[rgb_i+4*2..rgb_i+4*2 + 4].copy_from_slice(&DHGR_COLORS[pixel*4..pixel*4 + 4]);
	    rgba[rgb_i+4*3..rgb_i+4*3 + 4].copy_from_slice(&DHGR_COLORS[pixel*4..pixel*4 + 4]);
	    return rgb_i+4*4;
	}

	// 7 pixels stored on two bytes
	rgb_i = pixel_copy(rgba, rgb_i, p_a);
	rgb_i = pixel_copy(rgba, rgb_i, p_b);
	rgb_i = pixel_copy(rgba, rgb_i, p_c);
	rgb_i = pixel_copy(rgba, rgb_i, p_d);
	rgb_i = pixel_copy(rgba, rgb_i, p_e);
	rgb_i = pixel_copy(rgba, rgb_i, p_f);
	rgb_i = pixel_copy(rgba, rgb_i, p_g);

	ofs += 2
    }
}

const DHGR_COLORS: [u8; 64] = [
    0, 0, 0, 255,  //  0: Black
    32, 54, 212, 255, // 1: Dark blue
    51, 111, 0, 255, // 2Dark green
    7, 168, 225, 255, // 3: Med blue
    99, 77, 0, 255, // 4: Brown
    126, 126, 126, 255, // 5: Grey1
    67, 200, 0, 255, // 6: Green
    93, 248, 133, 255, // 7: Aqua
    148, 12, 125, 255, // 8: Magenta
    188, 55, 255, 255, // 9: Violet
    126, 126, 126, 255, // 10: Grey2
    158, 172, 255, 255, // 11: Light blue
    249, 86, 29, 255, // 12: Orange
    255, 129, 236, 255, // 13: Pink
    221, 206, 23, 255, // 14: Yellow
    255, 255, 255,  255 // 15: White
];


const GR_COLORS: [u8; 64] = [
    0, 0, 0, 255,  //  0: Black
    148, 12, 125, 255, // 8: Magenta
    32, 54, 212, 255, // 1: Dark blue
    188, 55, 255, 255, // 9: Violet
    51, 111, 0, 255, // 2Dark green
    126, 126, 126, 255, // 5: Grey1
    7, 168, 225, 255, // 3: Med blue
    158, 172, 255, 255, // 11: Light blue
    99, 77, 0, 255, // 4: Brown
    249, 86, 29, 255, // 12: Orange
    126, 126, 126, 255, // 10: Grey2
    255, 129, 236, 255, // 13: Pink
    67, 200, 0, 255, // 6: Green
    221, 206, 23, 255, // 14: Yellow
    93, 248, 133, 255, // 7: Aqua
    255, 255, 255,  255 // 15: White
];


const HGR_COLORS1: [u8; 16] = [
      0,   0,   0, 255, // 00 Black
      0, 249,   0, 255, // 01 Green
    255,  64, 255, 255, // 10 Purple
    255, 255, 255, 255, // 11 White
];

const HGR_COLORS2: [u8; 16] = [
      0,   0,   0, 255, // 00 Black
    255, 147,   0, 255, // 01 Red
      0, 150, 255, 255, // 10 Blue
    255, 255, 255, 255, // 11 White
];


fn hgr_stripe_to_pixels(
    //bytes: &[u8],
    bytes: &Vec<u8>,
    rgba: &mut [u8],
    page: usize,
    y: usize,
    byte_ofs_begin: usize,
    byte_ofs_end: usize,
    an3 : u8
) {
    if y < FIRST_NON_BLANK_LINE || y >= FIRST_NON_BLANK_LINE+192 {
	return
    }

    let mut ofs = page + hgr_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_begin;
    let ofs_end = page + hgr_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_end;
    let mut rgb_i = (y * (SCREEN_WIDTH as usize) + byte_ofs_begin * 7 * 2) * 4;

    // println!("y:{} ${:04X} ofs:{:04X} ofs_end:{:04X} byte_ofs_begin:{} byte_ofs_end:{}",y,hgr_address(y),ofs,ofs_end,byte_ofs_begin,byte_ofs_end);

    /*
    Read Sather's at page 3-6 for COLOR REFERENCE signal.

    Page 8-6 : "dot position determines color". That's the way Apple works
    at its core.
     */

    // Color reference (3.5MHz) starts as "high" on every line.
    // Its frequency is half the one of the HIRES signal.
    // Since the dots of the HIRES signal are 7 by byte, it
    // means the phase of COLOR REFERENCE is switched at the beginning
    // of each byte. So we initialize color ref according
    // to the odd/even position of the byte. On byte 0 it is high,
    // on byte 1 it is low, byte 2 high, etc.
    // See Sather's 8-15, column 1.

    let mut color_ref:u8 = (ofs & 1) as u8 ^ 1;

    //let mut reuse_last_color = None;


    while ofs < ofs_end {
	let mut byte = bytes[ofs];

	// Detect the color shift bit.
	let shift = ((byte & 0x80) ^ an3) != 0;

	// We progress one bit (a dot) at a time, sort of builing
	// the video signal along the CRT beam.

	let mut prev_dot = if (ofs > 0) && (bytes[ofs-1] & 0x40) != 0 {
	    1
	} else {
	    0
	};

	// let mut color = 0;

	for i in 0..7 {
	    // the 'byte" will be shifted at the end of
	    // each iteration.

	    let dot = byte & 1;

	    // Compute the dot following the dot above.
	    // This may include peeking at the next byte.

	    let next_dot = if i < 6 {
		(byte >> 1) & 1
	    } else {
		bytes[ofs+1] & 1
	    };

	    /* Sather 8-6 :
	    There are four classes of Apple video concerning
	    color. One is black, the absence of luminance. Two is
	    white, the absence of color caused by PICTURE signals less
	    than 3.58 MHz. This occurs when adjacent HIRES dots are
	    turned on, increasing signal pulse width and decreasing
	    frequency so there is no 3.58 MHz signal for the TV to
	    interpret as chrominance.

	    Sather 8-19:

	    Now if you inject the same VIDEO signal into a high
	    frequency response video monitor, you will clearly see the
	    black spots between the dots in the lines that were violet
	    on the television. It is very educational to compare all
	    forms of Apple video to simultaneous displays on a
	    television and high frequency monitor. HIRES and LORES
	    graphics modes use the “slowness” of a television to
	    display

	    COLOR REF : (3.58 Mhz)
	                     ___     ___     ___     ___     ___     ___     ___
	                 ...!   !...!   !...!   !...!   !...!   !...!   !...!   !
                             |   |                   |         |   |
	    Pixels (560): (14 Mhz)                   |         |   |
	                 1 2 3 4 5 6 7 8 1 2 3 4 5 6 7 8 1 2 3 4 5 6 7 8 1 2 3 4 5 6 7 8 1
                             |   |                   |         |   |
            MSbit:      (0)  |   |                  (1) >>shift !  |
                             |   |                   |         |   |
	    HIRES : (7Mhz)   |   |                   |         |   |
	    Data example:0   1   0   1   1   0   0   >>0   1   0   1   1   0   0   0   1   0
                         1   2   3   4   5   6   7   >>1   2   3   4   5   6   7   1   2   3

	                 _   V   _   W   W   _   _   > _   B   _   W   W   _   _   _   V   _

	                 (V=violet, B=blue, W=white _=black)

	     */

	    // Simulate monitor's delay in recognizing the signal
	    let cndx = (if
		(prev_dot == 0 && dot == 1 && next_dot == 0) ||
		(prev_dot == 1 && dot == 0 && next_dot == 1) {

		    // A transition occured on the signal, it means color
		    // we're only interested in "quick" transition
		    // (not 011, 110, 001, 100, etc.)


		    // Thanks to fenarinarsa/FT! for the explanantions !
		    // See also : https://nerdlypleasures.blogspot.com/2013/09/the-overlooked-artifact-color.html
		    // Choose the color, remember that video dots are
		    // 14 and split on two bytes, so things alternate
		    // and we don't necessarily start on a odd ofs.
		    // That's why we add in the 'ofs'.

		    if ((i+ofs) & 1) == 0 {
			(dot << 1) + prev_dot
		    } else {
			(next_dot << 1) + dot
		    }

		} else if dot == 1 {
		    3
		} else {
		    0
		} as usize) * 4;

	    if !shift {
		rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS1[cndx..cndx + 4]);
		rgb_i += 4;
		rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS1[cndx..cndx + 4]);
		rgb_i += 4;
	    } else {
		rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
		rgb_i += 4;
		rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
		rgb_i += 4;
	    }

	    // FIXME the following code simulate (badly) the NTSC, hence
	    // it should be put outside these routines so we separate
	    // the video signal generation from its NTSC/PAL rendering.
	    /*
	    Sather's 8-49 : "The reason that the object appears
	    solid horizontally is that a multichannel color television is not
	    capable of turning its beam intensity on and off cleanly at 3.58
	    MHz. Instead, the dots are blurred into continuous horizontal
	    lines. For the same reason, Apple text is fairly blurred when
	    displayed ona television set."
	     */

	    // if ofs >= 1 {
	    // 	let current_pixel = rgb_i - 4;
	    // 	let previous_pixel = current_pixel - 4;

	    // 	if rgba[current_pixel+1] == 0 { // Current pixel is black
	    // 	    for j in 0..4 {
	    // 		rgba[current_pixel+j] = (( (rgba[current_pixel+j] as usize)*2 + (rgba[previous_pixel+j] as usize)*2) /4) as u8;
	    // 	    }
	    // 	}
	    // }

	    prev_dot = dot;
	    byte = byte >> 1;
	    color_ref = color_ref ^ 1;
	}

	ofs += 1;
    }

}



fn hgr_stripe_to_pixels_black_white(
    bytes: &Vec<u8>,
    rgba: &mut [u8],
    page: usize,
    y: usize,
    byte_ofs_begin: usize,
    byte_ofs_end: usize,
    an3 : u8
) {
    if y < FIRST_NON_BLANK_LINE || y >= FIRST_NON_BLANK_LINE+192 {
	return
    }

    let mut ofs = page + hgr_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_begin;
    let ofs_end = page + hgr_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_end;
    let mut rgb_i = (y * (SCREEN_WIDTH as usize) + byte_ofs_begin * 7 * 2) * 4;

    while ofs < ofs_end {
	let mut byte = bytes[ofs];

	for i in 0..7 {
	    // the 'byte" will be shifted at the end of
	    // each iteration.

	    let cndx = (((byte & 1) * 3)*4) as usize;

	    rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
	    rgb_i += 4;
	    rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
	    rgb_i += 4;

	    byte = byte >> 1;
	}

	ofs += 1;
    }

}




fn hgr_stripe_to_pixels_accelerated (
    bytes: &[u8],
    rgba: &mut [u8],
    page: usize,
    y: usize,
    byte_ofs_begin: usize,
    byte_ofs_end: usize,
    an3 : u8
) {
    if y < FIRST_NON_BLANK_LINE || y >= FIRST_NON_BLANK_LINE+192 {
	return
    }

    let ofs = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_begin;
    let mut rgb_i = (y * (SCREEN_WIDTH as usize) + byte_ofs_begin * 7 * 2) * 4;

    let mut color_ref:u8 = (ofs & 1) as u8 ^ 1;

    // Detect the color shift bit.
    //let mut byte = bytes[ofs];
    let half_line = ((y-FIRST_NON_BLANK_LINE) / 4) % 2;

    let mut byte = if half_line % 2 == 0 {
            bytes[ofs] & 0b00001111
        } else {
            (bytes[ofs] & 0b11110000) >> 4
	} as u8;
    byte = byte | (byte << 4);

    let mut prev_byte = if ofs > 0 {
	if half_line % 2 == 0 {
            bytes[ofs-1] & 0b00001111
	} else {
            (bytes[ofs-1] & 0b11110000) >> 4
	}
    } else {0};

    byte = (byte << 1) | (byte & 1);
    prev_byte = prev_byte | (prev_byte << 4);

    let shift = (( byte & 0x80) ^ an3) != 0;

    // We progress one bit (a dot) at a time, sort of builing
    // the video signal along the CRT beam.

    let mut prev_dot = if (ofs > 0) && (prev_byte & 0x40) != 0 {
	1
    } else {
	0
    };

    // let mut color = 0;

    for i in 0..7 {
	// the 'byte" will be shifted at the end of
	// each iteration.

	let dot = byte & 1;

	// Compute the dot following the dot above.
	// This may include peeking at the next byte.

	let next_dot = if i < 6 {
	    (byte >> 1) & 1
	} else {
	    bytes[ofs+1] & 1
	};

	// Simulate monitor's delay in recognizing the signal
	let cndx = (if
		    (prev_dot == 0 && dot == 1 && next_dot == 0) ||
		    (prev_dot == 1 && dot == 0 && next_dot == 1) {


			if ((i+ofs) & 1) == 0 {
			    (dot << 1) + prev_dot
			} else {
			    (next_dot << 1) + dot
			}

		    } else if dot == 1 {
			3
		    } else {
			0
		    } as usize) * 4;

	if !shift {
	    rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS1[cndx..cndx + 4]);
	    rgb_i += 4;
	    rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS1[cndx..cndx + 4]);
	    rgb_i += 4;
	} else {
	    rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
	    rgb_i += 4;
	    rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
	    rgb_i += 4;
	}

	prev_dot = dot;
	byte = byte >> 1;
	color_ref = color_ref ^ 1;
    }
}




fn text80_stripe_to_pixels(
    main_ram: &Vec<u8>,
    aux_ram: &Vec<u8>,
    rgba: &mut [u8],
    page: usize,
    y: usize,
    byte_ofs_begin: usize,
    byte_ofs_end: usize,
    charset_rom: &Vec<u8>
) {
    if y < FIRST_NON_BLANK_LINE || y >= FIRST_NON_BLANK_LINE+192 {
	// We're in the VBL
	return
    }

    let mut ofs_hacked = byte_ofs_begin;
    if ofs_hacked & 1 == 1 {
        // FIXME Dirty hack to make the code easier
        ofs_hacked -= 1
    }
    //println!("{:04} {:04X} {:04X}", y, page, page + text_address(y - 60));
    let mut ofs = page + text_address(y - FIRST_NON_BLANK_LINE) + ofs_hacked;
    let ofs_end = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_end;
    let mut rgb_i = (y * (SCREEN_WIDTH as usize) + ofs_hacked * 7 * 2) * 4;

    let char_line = (y - FIRST_NON_BLANK_LINE) % 8;

    // println!("y:{} ${:04X} ofs:{:04X} ofs_end:{:04X} byte_ofs_begin:{} byte_ofs_end:{}",y,hgr_address(y),ofs,ofs_end,byte_ofs_begin,byte_ofs_end);

    while ofs < ofs_end {
        let b1 = charset_rom[ (aux_ram[ofs] as usize)*8 + char_line ];

	let mut mask = 1;
	while mask <= 64 {
	    let cndx = (if b1 & mask != 0 {3} else {0}) * 4;
            rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
            rgb_i += 4;
	    mask = mask << 1;
	};

        let b1 = charset_rom[ (main_ram[ofs] as usize)*8 + char_line ];

	let mut mask = 1;
	while mask <= 64 {
	    let cndx = (if b1 & mask != 0 {3} else {0}) * 4;
            rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
            rgb_i += 4;
	    mask = mask << 1;
	};

        ofs += 1;
    }
}



fn text_stripe_to_pixels(
    //bytes: &[u8],
    bytes: &Vec<u8>,
    rgba: &mut [u8],
    page: usize,
    y: usize,
    byte_ofs_begin: usize,
    byte_ofs_end: usize,
    charset_rom: &Vec<u8>
) {
    if y < FIRST_NON_BLANK_LINE || y >= FIRST_NON_BLANK_LINE+192 {
	// We're in the VBL
	return
    }

    let mut ofs = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_begin;
    let ofs_end = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_end;
    let mut rgb_i = (y * (SCREEN_WIDTH as usize) + byte_ofs_begin * 7 * 2) * 4;

    let char_line = (y - FIRST_NON_BLANK_LINE) % 8;

    // println!("y:{} ${:04X} ofs:{:04X} ofs_end:{:04X} byte_ofs_begin:{} byte_ofs_end:{}",y,hgr_address(y),ofs,ofs_end,byte_ofs_begin,byte_ofs_end);

    while ofs < ofs_end {
        let b1 = charset_rom[ (bytes[ofs] as usize)*8 + char_line ];

	let mut mask = 1;
	while mask <= 64 {
	    let cndx = (if b1 & mask != 0 {3} else {0}) * 4;
            rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
            rgb_i += 4;
            rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
            rgb_i += 4;
	    mask = mask << 1;
	};

        ofs += 1;
    }
}


fn text_stripe_to_pixels_accelerated(
    //bytes: &[u8],
    bytes: &Vec<u8>,
    rgba: &mut [u8],
    page: usize,
    y: usize,
    byte_ofs_begin: usize,
    byte_ofs_end: usize,
    charset_rom: &Vec<u8>
) {
    if y < FIRST_NON_BLANK_LINE || y >= FIRST_NON_BLANK_LINE+192 {
	// We're in the VBL
	return
    }

    let mut ofs = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_begin;
    let ofs_end = page + text_address(y - FIRST_NON_BLANK_LINE) + byte_ofs_end;
    let mut rgb_i = (y * (SCREEN_WIDTH as usize) + byte_ofs_begin * 7 * 2) * 4;

    let char_line = (y - FIRST_NON_BLANK_LINE) % 8;

    // println!("y:{} ${:04X} ofs:{:04X} ofs_end:{:04X} byte_ofs_begin:{} byte_ofs_end:{}",y,hgr_address(y),ofs,ofs_end,byte_ofs_begin,byte_ofs_end);

    while ofs < ofs_end {
        let b1 = charset_rom[ (bytes[ofs] as usize)*8 + char_line ];

	let mut mask = 1;
	while mask <= 64 {
	    let cndx = (if b1 & mask != 0 {3} else {0}) * 4;
	    rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
	    rgb_i += 4;
	    mask = mask << 1;
	};

	let mut mask = 1;
	while mask <= 32 {
	    let cndx = (if b1 & mask != 0 {3} else {0}) * 4;
	    rgba[rgb_i..rgb_i + 4].copy_from_slice(&HGR_COLORS2[cndx..cndx + 4]);
	    rgb_i += 4;
	    mask = mask << 1;
	};

	rgba[rgb_i..rgb_i + 4].fill(0);
	rgb_i += 4;

        ofs += 1;
    }
}


enum GraphicDisplay {
    HgrPage1,
    HgrPage2,
    TextPage1,
    TextPage2,
}

// enum HGREventData {
//     Page1,
//     Page2,
// }

// struct HGREvent {
//     // CPU cycle at which the event occurs
//     pub tick: u64,

//     // Screen line on which the event occurs
//     pub line: usize,

//     // Byte offset at which the even occurs, relative to beginning of the line.
//     pub byte_pos: usize,
//     pub data: HGREventData,
// }


// struct SpeakerRenderState {
//     cycle: u64,
//     tension: bool,
//     sound: VecDeque<i16>
// }



// const SAMPLES_PER_SECOND: usize = 44100; // 44100;
// // FIXME this +4 stinks see comment in  cpal trhread. 882.
// const SAMPLES_PER_FRAME: usize = CYCLES_PER_FRAME * SAMPLES_PER_SECOND / CYCLES_PER_SECOND + 4; // 1,3 not enough, 5 too much
// const SOUND_DELAY: usize = 30; //16*3; // How many milliseconds to wait before starting to output sound



fn render_strip(current_line:usize, byte_ofs_begin:usize, byte_ofs_end:usize,
		machine: &AppleII, rgba: &mut [u8], state: &mut GfxRenderState,
		accel: bool ) {


    fn call_render(mode: GfxBaseMode,
		   current_line:usize, byte_ofs_begin:usize, byte_ofs_end:usize,
		   machine: &AppleII, rgba: &mut [u8], state: &mut GfxRenderState,
		   video_ram_low_res: usize, video_ram_high_res: usize, accel: bool) {
	match mode {
	    GfxBaseMode::Text40 =>
		if !accel {
		    text_stripe_to_pixels(&state.gfx_mem,
					  rgba, video_ram_low_res,
					  current_line,
					  byte_ofs_begin, byte_ofs_end,
					  &machine.charset_rom)
		} else {
		    if byte_ofs_end - byte_ofs_begin > 1 {
			text_stripe_to_pixels(&state.gfx_mem,
					      rgba, video_ram_low_res,
					      current_line,
					      byte_ofs_begin, byte_ofs_end-1,
					      &machine.charset_rom)
		    }
		    text_stripe_to_pixels_accelerated(&state.gfx_mem,
					  rgba, video_ram_low_res,
					  current_line,
					  byte_ofs_end-1, byte_ofs_end,
					  &machine.charset_rom)
		},
	    GfxBaseMode::Text80 =>
		text80_stripe_to_pixels(&state.gfx_mem,
					&state.gfx_aux_mem,
					rgba, video_ram_low_res,
					current_line,
					byte_ofs_begin, byte_ofs_end,
					&machine.charset_rom),
	    GfxBaseMode::Gr =>
		if !accel {
		    gr_stripe_to_pixels_black_white(&state.gfx_mem,
					rgba, video_ram_low_res,
					current_line,
					byte_ofs_begin, byte_ofs_end)
		} else {
		    if byte_ofs_end - byte_ofs_begin > 1 {
			gr_stripe_to_pixels(&state.gfx_mem,
					    rgba, video_ram_low_res,
					    current_line,
					    byte_ofs_begin, byte_ofs_end-1)
		    }
		    hgr_stripe_to_pixels_accelerated(&state.gfx_mem,
					 rgba, video_ram_low_res,
					 current_line,
					 byte_ofs_end-1, byte_ofs_end,
					 state.mode.an3)
		},
	    GfxBaseMode::Hgr =>
		// hgr_stripe_to_pixels(&state.gfx_mem,
		// 		     rgba, video_ram_high_res,
		// 		     current_line,
		// 		     byte_ofs_begin, byte_ofs_end,
		// 		     state.mode.an3)
		hgr_stripe_to_pixels_black_white(&state.gfx_mem,
				     rgba, video_ram_high_res,
				     current_line,
				     byte_ofs_begin, byte_ofs_end,
				     state.mode.an3),

	    GfxBaseMode::Dhgr =>
		dhgr_stripe_to_pixels(&state.gfx_mem,
				      &state.gfx_aux_mem,
				      rgba, video_ram_high_res,
				      current_line,
				      byte_ofs_begin, byte_ofs_end),
	    _ => {
		warn!("Unsupported Gfx mode {}", state.mode.mode);
	    }
	}
    }

    let video_ram_low = if state.mode.page2 { 0x800 } else { 0x400 };
    let video_ram_high = if state.mode.page2 { 0x4000 } else { 0x2000 };

    if state.mode.mixed.is_none() {
	call_render(state.mode.mode,
		    current_line, byte_ofs_begin, byte_ofs_end,
		    machine, rgba, state,
		    video_ram_low, video_ram_high, accel);
    } else { // Mixed mode
	if current_line >= FIRST_NON_BLANK_LINE + 20*8 {
	    // Low part of the screen in mixed mode
	    let video_ram_low = 0x400;
	    let video_ram_high = 0x2000;
	    call_render(state.mode.mixed.unwrap(),
			current_line, byte_ofs_begin, byte_ofs_end,
			machine, rgba, state,
			video_ram_low, video_ram_high, accel);
	} else {
	    call_render(state.mode.mode,
			current_line, byte_ofs_begin, byte_ofs_end,
			machine, rgba, state,
			video_ram_low, video_ram_high, accel);
	}
    }

    // if (state.mode.mode == GfxBaseMode::Text40) ||
    // 	(state.mode.mixed && current_line >= 60 + 20*8)
    // {

    // 	    text_stripe_to_pixels(&state.gfx_mem,
    // 				  rgba, video_ram_low,
    // 				  current_line,
    // 				  byte_ofs_begin, byte_ofs_end,
    // 				  &machine.charset_rom)
    // }  else if (state.mode.mode == GfxBaseMode::Text80) ||
    // 	(state.mode.mixed && current_line >= 60 + 20*8)
    // {
    // 	text80_stripe_to_pixels(&state.gfx_mem,
    // 				&state.gfx_aux_mem,
    // 				rgba, video_ram_low,
    // 				current_line,
    // 				byte_ofs_begin, byte_ofs_end,
    // 				&machine.charset_rom)
    // }

    // else {
    // 	call_render(state.mode.mode,
    // 		    current_line, byte_ofs_begin, byte_ofs_end,
    // 		   machine, rgba, state,
    // 		   video_ram_low, video_ram_high);
    // }

}

pub fn cycle_to_crt_line(cycle:u64) -> usize {
    ((cycle as usize) % CYCLES_PER_FRAME) / CYCLES_PER_LINE
}

pub fn render_one_frame(
    machine: &mut AppleII,
    cpu_cycle: u64,
    rgba: &mut [u8],
    state: &mut GfxRenderState,
) {

    if state.cycle <= cpu_cycle && ((cpu_cycle - state.cycle) as usize) < CYCLES_PER_FRAME {
	// not enough CPU cycles to consume
	// FIXME : this is a hack to avoid panics; this function
	// should work even in that case.
	return
    }


    // First convert CPU cycles to line/bytes
    // for example, a HGR1/2 page switch will
    // occur on the 32th byte of the 123th line
    // on screen.

    let mut current_cycle = state.cycle;

    /* Remember, the logic is :
      1/ The CPU makes computationfor N cycles => so it creates events
         that lie in the future of the graphics interpreter
      2/ The graphics interpreter starts and see those events in the future.

      ......111111
      222222e33333
      4444444444..
    */

    //debug!("Clock events capa={}, len={}", machine.clock_events.capacity(), machine.clock_events.len());

    //machine.clock_events.shrink_to_fit();

    loop {
	// We iterate over all clock events

	// gfx_render_state.cycle = cpu.cycles - 1;
        assert!(
            current_cycle < cpu_cycle,
            "We can't go past the CPU current time ('cos it'd mean we're in the future) {} >= {}",
	    current_cycle, cpu_cycle,
        );

        // FIXME Dangerous casts !

        // The y position of the current line on screen
        // Note that this position may include vblank lines
        // so we may be outside the [0, SCREEN_HEIGHT[ display interval.
        let current_line = ((current_cycle as usize) % CYCLES_PER_FRAME) / CYCLES_PER_LINE;

        // println!("loop: current_cycle:{}, current_line:{}, current_page:{}, {} queued events",
        // 	 current_cycle, current_line, current_page, machine.clock_events.len());

        // The position of the CRT beamer on the current screen line, expressed in CPU
        // cycles since the the beginning of the screen line.
	// There are 65 cycles per line, 25 of which are HBLANK. So it's 40 cycles per line
	// one per byte.
        let cycle_on_line = ((current_cycle as usize) % CYCLES_PER_FRAME) % CYCLES_PER_LINE;

        // The number of CPU cycles before reaching the end of the current line.
        let cycles_left_on_line = CYCLES_PER_LINE - cycle_on_line;

	let mut no_more_events_on_line = false;

        // Is there an event coming on the current line (before its end)?
	// The current event gives the current line (so if the first event
	// of the frame is on line 100, we automatically don't update then
	// lines 0 to 99, an optimisation that comes for free).

	if machine.clock_events.is_empty() {
	    // We have used all events of the frame, so, in particular,
	    // there are none left on the current line
	    no_more_events_on_line = true
	} else {
	    // There's an event coming. But we still don't know
	    // if it is on the current line.

            let next_cpu_event = machine.clock_events.back().unwrap();

            assert!(next_cpu_event.tick >= current_cycle,
		    "The next event we'll see is not in the future. line: {}, current_cycle:{}, event cycle:{}",
		    current_line, current_cycle, next_cpu_event.tick );

            let cycles_before_next_event = (next_cpu_event.tick - current_cycle) as usize;

            if cycles_before_next_event <= CYCLES_PER_FRAME {
		warn!(
                    "The gfx renderer is getting late {} > {}", cycles_before_next_event, CYCLES_PER_FRAME
		);
	    }

            if cycles_before_next_event >= cycles_left_on_line {
		// The next event will be on the next line. So we just have
		// to complete the current line
		no_more_events_on_line = true
	    } else {
		// The event is coming on the current line
                // So we'll run the beamer up to right before that event.

                // Plot the first part of the line, up to the event.

		let end_cycle_on_line = ((next_cpu_event.tick as usize) % CYCLES_PER_FRAME) % CYCLES_PER_LINE;
		let byte_ofs_begin = if cycle_on_line < 25 {
		    if end_cycle_on_line >= 25 {
			// Beginning of strip in the HBLANK but end of stripe in display
			25-25
		    } else {
			// Both begin and end of strip are in the HBLANK
			0
		    }
		} else {
		    cycle_on_line - 25
		};

		if end_cycle_on_line >= 25 {
		    // The event triggers before the end of the line. So we know
		    // Everything remain the same until then. Therefore we can
		    // draw up to (but not including) the moment of the event.

		    let mut accelerated = false;

                    match next_cpu_event.event {
			ClockEventData::GfxModeChange { mode } => {
			    if (state.mode.mode == GfxBaseMode::Text40 && mode.mode == GfxBaseMode::Gr) ||
				(state.mode.mode == GfxBaseMode::Gr && mode.mode == GfxBaseMode::Hgr) {
				    // From text (current) to GR (after render)
				    accelerated = true;
				}
			},
			_ => ()
		    }

		    let byte_ofs_end = end_cycle_on_line - 25;
		    // byte_ofs_end is NOT inclusive.
		    render_strip(current_line, byte_ofs_begin, byte_ofs_end,
				 machine, rgba, state, accelerated);
		}


                // Adapt to the event

                match next_cpu_event.event {
                    ClockEventData::GfxMemWrite { addr, data } => {
			//debug!("GfxMemWrite ${:04X}", addr);
			state.gfx_mem[addr as usize] = data;
		    },
                    ClockEventData::GfxAuxMemWrite { addr, data } => {
			state.gfx_aux_mem[addr as usize] = data;
		    },
		    ClockEventData::GfxModeChange { mode } => {
			state.mode = mode;

			if current_cycle >= 11275684 && current_line == FIRST_NON_BLANK_LINE + 192/2 {
				println!("Event: cycle:{}, mode:{} page2:{}, current_line:{}",next_cpu_event.tick, mode.mode, mode.page2, current_line);
			}

		    },
		    _ => {panic!("Unsupported event")}
                }

                // The rest of the line will be handled by the next iteration
                // of the loop.
                current_cycle += cycles_before_next_event as u64;
                assert!(current_cycle == next_cpu_event.tick);
                machine.clock_events.pop_back();
            }
	}

	if no_more_events_on_line {
	    // There are no more events to process on this line.
	    // We finalize the rendering by processing the last
	    // bytes of the line.

            if current_line < SCREEN_HEIGHT as usize {

		let byte_ofs_begin = if cycle_on_line < 25 {
		    0
		} else {
		    cycle_on_line - 25
		};

                //let byte_ofs_begin = cycle_on_line * 40 / CYCLES_PER_LINE;
                let byte_ofs_end = 40;

		render_strip(current_line, byte_ofs_begin, byte_ofs_end,
			     machine, rgba, state, false);

                current_cycle += (CYCLES_PER_LINE - cycle_on_line) as u64;
            } else {
                // VBlank area => nothing to draw.
                current_cycle += (CYCLES_PER_LINE - cycle_on_line) as u64;
            }
        } // end loop
        // We don't draw more than a frame

	// Current_cycle = time since state_cycle
        if current_cycle - state.cycle >= CYCLES_PER_FRAME as u64 {
            current_cycle = state.cycle + CYCLES_PER_FRAME as u64;
            break;
        }
    }

    //state.cycle = current_cycle;
    state.cycle += CYCLES_PER_FRAME as u64;
    // println!(
    //     "     Done. hgr page:{:04X}, cycle:{}, cpu cycle:{}, events left:{}",
    //     state.hgr_page,
    //     state.cycle,
    //     cpu_cycle,
    //     machine.clock_events.len()
    // );
}
