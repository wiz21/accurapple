/*
I'm in the situation where I want to have a
set of functions to operate on a given structure.
In the example below, two CPU's need to access
one single (mutable) memory (it's just an example).
From what I've read Rc<RefCell<_>> is what I need.

The code below works fine but I wonder if it is idiomatic.
Coming from OOP background, I find it quite problematic:

1/ Although the "memory" field is present in the Cpu
   struct, I have to apply a borrow_mut() on it
   each time I need to access it in the impl. I guess it's because Rust
   wants me to avoid to have two simultaneous mutable references.

2/ More problematic : I have to pass the mem parameter
   to the do_stuff function. If I have dozens of nested functions
   I'll have to pass that parameter on each call.
   Is it possible to avoid that ?

   Also, if I pass that parameter around, then it'd
   be just as simple to *not have* the memory field in the Cpu
   struct, and just passing mut &Memory references around
   which questions the need for the Rc<RefCell<Memory>>
   construct...

*/

use std::cell::{RefCell, RefMut};
use std::rc::Rc;

struct Cpu {
    memory: Rc<RefCell<Memory>>
}

impl Cpu {
    fn new(m : Rc<RefCell<Memory>>) -> Cpu {
	Cpu {
	    memory: m
	}
    }

    fn run(&mut self) {
	self.do_stuff(self.memory.borrow_mut());
	self.do_stuff(self.memory.borrow_mut());
    }

    fn do_stuff(&self, mut mem: RefMut<Memory>) {
	let i = mem.load(4) + 10;
	mem.set(4, i);
    }

}

struct Memory { pub mem: [u8; 5] }
impl Memory {
    fn new() -> Memory { Memory { mem: [0 as u8; 5] } }
    fn load( &self, ndx : usize) -> u8 { self.mem[ndx] }
    fn set( &mut self, ndx : usize, val: u8){ self.mem[ndx] = val }
}


fn main() {
    let mut a:u16 = 0x100;
    a >>= 1;
    println!("{:04X}", a);
    a <<= 1;
    a <<= 1;
    println!("{:04X} - ", a);

    let memory: Rc<RefCell<_>> = Rc::new(RefCell::new(Memory::new()));
    let mut cpu1 = Cpu::new(memory.clone());
    let mut cpu2 = Cpu::new(memory.clone());
    cpu1.run();
    cpu2.run();
    cpu1.run();
    cpu2.run();
    println!("{}",memory.borrow().mem[4]);
}
