// Code generation makes ugly code...
#![allow(warnings, unused)]
use log::{debug, error, info, trace};

use std::cell::{RefCell};
use std::rc::Rc;
use std::thread;
use std::io;
use std::io::Write;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;

// use std::time::Duration;
// use std::thread::sleep;
// use std::time::Instant;
//use clap::{Arg, App, SubCommand};


// use rs6502::disassembler;
// use self::rs6502::disassembler;
// use super::rs6502::disassembler;
use crate::rs6502::disassembler::Disassembler;
use crate::rs6502::assembler;
// use accurapple::rs6502::disassembler;
// use ::accurapple::rs6502::disassembler;
// use super::accurapple::rs6502::disassembler;
// use crate::accurapple::rs6502::disassembler;


//pub mod ms6502 {

type m6510_out_t = fn(data: u8, user_data: u8);
type m6510_in_t = fn(user_data: u8) -> u8;

struct m6502_desc_t {
    bcd_disabled: bool,              /* set to true if BCD mode is disabled */
    //m6510_in_cb: m6510_in_t,         /* optional port IO input callback (only on m6510) */
    //m6510_out_cb: m6510_out_t,       /* optional port IO output callback (only on m6510) */
    m6510_user_data: u8,          /* optional callback user data */
    m6510_io_pullup: u8,        /* IO port bits that are 1 when reading */
    m6510_io_floating: u8      /* unconnected IO port pins */
}

#[derive(Clone)]
pub struct Cycle6502 {
    pub IR: u16,        /* internal instruction register */
    pub PC: u16,        /* internal program counter register */
    pub AD: u16,        /* ADL/ADH internal register */
    pub A: u8,  /* regular registers */
    pub X: u8,  /* regular registers */
    pub Y: u8,  /* regular registers */
    pub S: u8,  /* regular registers */
    pub P: u8,  /* regular registers */
    pub PINS: u64,      /* last stored pin state (do NOT modify) */
    pub irq_pip: u16,
    nmi_pip: u16,
    pub brk_flags: u8,  /* M6502_BRK_* */
    bcd_enabled: u8,
    // /* 6510 IO port state */
    // user_data: u8,
    // in_cb: m6510_in_t ,
    // out_cb: m6510_out_t ,
    // io_ddr: u8,     /* 1: output, 0: input */
    // io_inp: u8,     /* last port input */
    // io_out: u8,     /* last port output */
    // io_pins: u8,    /* current state of IO pins (combined input/output) */
    // io_pullup: u8,
    // io_floating: u8,
    // io_drive: u8
    pub cycles: u64,
    pub last_instr_pc: u16,
}

pub const M6502_CF: u8 =     (1<<0);  /* zero */
pub const M6502_ZF: u8 =     (1<<1);  /* zero */
pub const M6502_IF: u8 =     (1<<2);  /* IRQ disable */
pub const M6502_DF: u8 =     (1<<3);  /* decimal mode */
pub const M6502_BF: u8 =     (1<<4);  /* BRK command */
pub const M6502_XF: u8 =     (1<<5);  /* unused */
pub const M6502_VF: u8 =     (1<<6);  /* overflow */
pub const M6502_NF: u8 =     (1<<7);  /* negative */


pub const M6502_RW: u64 = 1<<24; /* out: memory read or write access */
pub const M6502_SYNC: u64 = 1<<25;      /* out: start of a new instruction */
pub const M6502_IRQ: u64 = 1<<26;     /* in: maskable interrupt requested */
pub const M6502_NMI: u64 = 1<<27;      /* in: non-maskable interrupt requested */
pub const M6502_RDY: u64 = 1<<28;      /* in: freeze execution at next read cycle */
pub const M6502_RES: u64 = 1<<30;

const M6502_BRK_IRQ: u8 = (1<<0);  /* IRQ was triggered */
const M6502_BRK_NMI: u8 =  (1<<1);  /* NMI was triggered */
const M6502_BRK_RESET: u8 = (1<<2);  /* RES was triggered */

pub const OPCODE_JSR: u8 = 0x20;
pub const OPCODE_RTS: u8 = 0x60;
pub const OPCODE_RTI: u8 = 0x40;
pub const OPCODE_BRK: u8 = 0x00;
pub const OPCODE_CLI: u8 = 0x58;

fn addi8tou16(long: u16, short: u8) -> u16 {

    if short == 0 {
	return long
    } else if short & 0x80 == 0 {
	// short is positive.
	return long.wrapping_add(short as u16);
    } else  {
	// short is negative.
	// !x == -x-1. Example x=0xFF (-1) => !x == 0 => !x+1 == 1
	let s: u8 = (!short & 0x7F) + 1;
	return long.wrapping_sub( s as u16);
    }
}

/* set 16-bit address in 64-bit pin mask */
// #define _SA(addr) pins=(pins&~0xFFFF)|((addr)&0xFFFFULL)

macro_rules! SA {
    ($pins:expr, $addr:expr) => {
	$pins = ($pins & !0xFFFF) | ((u64::from($addr)) & 0xFFFF)
    }
}

/* extract 16-bit addess from pin mask */
// #define _GA() ((uint16_t)(pins&0xFFFFULL))

macro_rules! GA {
    ($pins:expr) => {
	($pins & 0xFFFF) as u16
    }
}

macro_rules! ON {
    ($pins:expr, $m:expr) => {
	$pins = $pins | $m;
    }
}

macro_rules! OFF {
    ($pins:expr, $m:expr) => {
	$pins = $pins & !$m;
    }
}

macro_rules! FETCH {
    ($pins:expr, $c:expr) => {
	SA!($pins, $c.PC);
	ON!($pins, M6502_SYNC);
    }
}

// #define _SD(data) pins=((pins&~0xFF0000ULL)|(((data&0xFF)<<16)&0xFF0000ULL))
// FIXME The last & is not necessary
macro_rules! SD {
    ($pins:expr, $data:expr) => {
	$pins = ($pins & !0xFF0000) | (((($data as u64) & 0xFF)<<16) & 0xFF0000);
    }
}

/* extract 8-bit data from 64-bit pin mask */
macro_rules! GD {
    ($pins:expr) => {
	(($pins & 0x00FF0000)>>16) as u8
    }
}

macro_rules! RD {
    ($pins:expr) => {
	ON!($pins, M6502_RW)
    }
}

// #define _WR() _OFF(M6502_RW);

macro_rules! WR {
    ($pins:expr) => {
	OFF!($pins, M6502_RW)
    }
}



// #define M6502_SET_DATA(p,d) {p=(((p)&~0xFF0000ULL)|(((d)<<16)&0xFF0000ULL));}
/* extract 16-bit address bus from 64-bit pins */
//#define M6502_GET_ADDR(p) ((uint16_t)(p&0xFFFFULL))

macro_rules! M6502_GET_ADDR {
    ($pins:expr) => {
	    ($pins & 0xFFFF) as u16
    }
}

/* extract 8-bit data bus from 64-bit pins */
// #define M6502_GET_DATA(p) ((uint8_t)((p&0xFF0000ULL)>>16))
macro_rules! M6502_GET_DATA {
    ($pins:expr) => {
	    (($pins & 0x00FF0000) >> 16) as u8
    }
}

/* merge 8-bit data bus value into 64-bit pins */
// #define M6502_SET_DATA(p,d) {p=(((p)&~0xFF0000ULL)|(((d)<<16)&0xFF0000ULL));}
macro_rules! M6502_SET_DATA {
    ($pins:expr, $data:expr) => {
	$pins = (($pins & !0xFF0000)|((($data as u64)<<16)&0xFF0000));
    }
}


// #define _M6502_NZ(p,v) ((p&~(M6502_NF|M6502_ZF))|((v&0xFF)?(v&M6502_NF):M6502_ZF))

macro_rules! M6502_NZ {
    ($p:expr, $v:expr) => {
	($p & !(M6502_NF|M6502_ZF)) |
	if ($v&0xFF) != 0 {$v & M6502_NF} else {M6502_ZF};
    }
}

// #define _SAD(addr,data) pins=(pins&~0xFFFFFF) | ((((data)&0xFF)<<16)&0xFF0000ULL) | ((addr)&0xFFFFULL)
macro_rules! SAD {
    ($pins:expr, $addr:expr, $data:expr) => {
	$pins = ($pins & !0xFFFFFF) | (((($data as u64)&0xFF)<<16) & 0xFF0000) | (($addr as u64) & 0xFFFF);
    }
}


// const fn optick( opcode: u8, subtick: u8) -> u16 {
//     return 5;
// }

impl Cycle6502 {

    // #define _NZ(v) c->P=((c->P&~(M6502_NF|M6502_ZF))
    //    | ((v&0xFF) ? (v&M6502_NF) : M6502_ZF))

    fn NZ(&mut self, v:u8) {
	self.P = self.NZNoUpdate(v);
    }

    fn NZNoUpdate(&self, v:u8) -> u8 {
	return (self.P & !(M6502_NF|M6502_ZF)) |
	   if v & 0xFF != 0 {v & M6502_NF } else {M6502_ZF};
    }


    fn adc(&mut self, val: u8) {
	let cpu = self;

	if (cpu.bcd_enabled != 0 && (cpu.P & M6502_DF != 0)) {
            /* decimal mode (credit goes to MAME) */
	    //println!("BCD adc A=${:02X} v=${:02X}", cpu.A, val);
            let c:u8 = if(cpu.P & M6502_CF != 0) {1} else {0};
            cpu.P &= !(M6502_NF|M6502_VF|M6502_ZF|M6502_CF);
            let mut al:u8 = (cpu.A & 0x0F) + (val & 0x0F) + c;
            if (al > 9) {
		al = al.wrapping_add(6);
            }
            let mut ah:u8 = (cpu.A >> 4) + (val >> 4) + if al > 0x0F {1} else {0};
            if 0 == cpu.A.wrapping_add( val.wrapping_add(c))  {
		cpu.P |= M6502_ZF;
            }
            else if (ah & 0x08 != 0) {
		cpu.P |= M6502_NF;
            }
            if (!(cpu.A^val) & (cpu.A^(ah<<4)) & 0x80 != 0) {
		cpu.P |= M6502_VF;
            }
            if (ah > 9) {
		ah = ah.wrapping_add(6);
		//ah += 6;
            }
            if (ah > 15) {
		cpu.P |= M6502_CF;
            }

            cpu.A = (ah << 4) | (al & 0x0F);

	}
	else {
	    /* default mode */
	    let sum:u16 = (cpu.A as u16) + (val as u16) + (if cpu.P & M6502_CF != 0 {1} else {0});
	    cpu.P &= !(M6502_VF|M6502_CF);
	    cpu.NZ((sum & 0xFF) as u8);
	    if (!(cpu.A^val) & (cpu.A^(sum as u8)) & 0x80 != 0) {
		cpu.P |= M6502_VF;
	    }
	    if (sum & 0xFF00 != 0) {
		cpu.P |= M6502_CF;
	    }
	    cpu.A = (sum & 0xFF) as u8;
	}
    }

    fn arr(&mut self) {

    }

    fn sbx(&mut self, v: u8) {

    }

    fn sbc(&mut self, val: u8) {
	let cpu = self;
	if (cpu.bcd_enabled != 0) && (cpu.P & M6502_DF != 0) {
            /* decimal mode (credit goes to MAME) */
            let c:u8 = if cpu.P & M6502_CF != 0 {0} else {1};
            cpu.P &= !(M6502_NF|M6502_VF|M6502_ZF|M6502_CF);
            let diff: u16 = (cpu.A as u16).wrapping_sub(val as u16).wrapping_sub(c as u16);
            let mut al:u8 = (cpu.A & 0x0F).wrapping_sub(val & 0x0F).wrapping_sub(c);

	    //println!("BCD sbc A=${:02X} v=${:02X} diff=${:04X} al=${:02X}", cpu.A, val, diff, al);

	    //if (al as i8 < 0) {
	    if (al & 0x80 != 0) {
		al = al.wrapping_sub(6);
            }

            let mut ah: u8 = (cpu.A >> 4).wrapping_sub(val >> 4).wrapping_sub(if al & 0x80 != 0 {1} else {0});
	    //if (0 == (uint8_t)diff) {
	    if (0 == diff & 0xFF) {
		cpu.P |= M6502_ZF;
	    }
	    else if (diff & 0x80 != 0) {
		cpu.P |= M6502_NF;
	    }
	    if ((cpu.A^val) & (cpu.A^((diff & 0xFF) as u8) & 0x80) != 0) {
		cpu.P |= M6502_VF;
	    }
            if (diff & 0xFF00 == 0) {
		cpu.P |= M6502_CF;
            }
            if (ah & 0x80 != 0) {
		ah = ah.wrapping_sub(6);
            }
            cpu.A = (ah<<4) | (al & 0x0F);
	}
	else {
	    /* default mode */
	    let carry: bool = cpu.P & M6502_CF != 0;
	    let diff: u16 = (cpu.A as u16).wrapping_sub(val as u16).wrapping_sub(if carry {0} else {1});

	    cpu.P &= !(M6502_VF|M6502_CF);
	    cpu.NZ(diff as u8);

	    let diff8: u8 = (diff & 0xFF) as u8;
	    if ((cpu.A^val) & (cpu.A^diff8) & 0x80 != 0) {
		cpu.P |= M6502_VF;
	    }
	    if (diff & 0xFF00 == 0) {
		cpu.P |= M6502_CF;
	    }
	    //println!("SBC A={} v={} carry={} VF={}: diff={}", cpu.A, val, carry, cpu.P & M6502_VF, diff);
	    cpu.A = diff8;
	}
    }

    fn bit(&mut self, v: u8) {
	let cpu = self; // Rename as in original code (I can't rename the self parameter in method's prototype, rust forbids)
	let t: u8 = cpu.A & v;
	cpu.P &= !(M6502_NF|M6502_VF|M6502_ZF);
	if (t == 0) {
            cpu.P |= M6502_ZF;
	}
	cpu.P |= v & (M6502_NF|M6502_VF);
    }

    fn cmp(&mut self, r: u8, v: u8) {
	let t: u16 = (r as u16).wrapping_sub(v as u16);
	self.P = (self.NZNoUpdate(t as u8) & !M6502_CF) | if t & 0xFF00 != 0 {0} else {M6502_CF};
    }

    fn lsr(&mut self, v: u8) -> u8 {
	self.P = (self.NZNoUpdate(v >> 1) & !M6502_CF) | if v & 0x01 != 0 {M6502_CF} else {0};
	return v >> 1;
    }

    fn asl(&mut self, v: u8) -> u8 {
	self.P = (self.NZNoUpdate(v << 1) & !M6502_CF) | if(v & 0x80 != 0) {M6502_CF} else {0};
	return v << 1;
    }

    fn ror(&mut self, mut v: u8) -> u8 {
	let cpu = self; // Rename as in original code (I can't rename the self parameter in method's prototype, rust forbids)
	let carry: bool = cpu.P & M6502_CF != 0;

	cpu.P &= !(M6502_NF|M6502_ZF|M6502_CF);
	if (v & 1 != 0) {
            cpu.P |= M6502_CF;
	}
	v = v >> 1;
	if (carry) {
            v |= 0x80;
	}
	cpu.NZ(v);
	return v;
    }

    fn rol(&mut self, mut v: u8) -> u8 {
	let cpu = self;
	let carry: bool = cpu.P & M6502_CF != 0;
	let v_in = v;

	cpu.P &= !(M6502_NF|M6502_ZF|M6502_CF);
	if (v & 0x80 != 0) {
            cpu.P |= M6502_CF;
	}
	v = v << 1;
	if (carry) {
            v = v | 1;
	}
	cpu.NZ(v);
	//cpu.P = M6502_NZ!(cpu.P, v); // FIXME use cpu.NZ()
	//println!("ROL {:02X} -> {:02X}", v_in, v);
	return v;
    }

    pub fn tick( self: &mut Self, pins_: u64) -> u64 {
	let c = self; // Rename as in original code (I can't rename the self parameter in method's prototype, rust forbids)
	let mut pins = pins_;

	if (pins & (M6502_SYNC|M6502_IRQ|M6502_NMI|M6502_RDY|M6502_RES) != 0) {

        // pins & M6502_IRQ != 0: IRQ line is pulled LOW ?
        // 0 == (c.P & M6502_IF) : interrupt disable flag is cleared

        if ((pins & M6502_IRQ != 0) && (0 == (c.P & M6502_IF))) {
		    trace!("6502 sees IRQ on cycle {}", c.cycles);
		    c.irq_pip |= 0x100;
        }

        if ((pins & (M6502_RW|M6502_RDY)) == (M6502_RW|M6502_RDY)) {
            trace!("pip << 1");
            //M6510_SET_PORT(pins, c->io_pins);
            c.PINS = pins;
            c.irq_pip = c.irq_pip << 1;
            return pins;
        }

	    //println!("pins OUT: {:#032b}", pins_);

	    if (pins & M6502_SYNC != 0) {
		c.IR = (GD!(pins) as u16) << 3;
		c.last_instr_pc = c.PC;
		//println!("M6502_SYNC pins OUT: {:#032b} {:04X}", pins_, c.last_instr_pc);
		OFF!(pins, M6502_SYNC);

		if 0 != (c.irq_pip & 0x400) {
		    trace!("6502 will BRK on cycle {}", c.cycles);
            c.brk_flags |= M6502_BRK_IRQ;
		}

		if (0 != (pins & M6502_RES)) {
            c.brk_flags |= M6502_BRK_RESET;
		}

		c.irq_pip &= 0x3FF;
		c.nmi_pip &= 0x3FF;

		// if interrupt or reset was requested, force a BRK instruction
		if (c.brk_flags != 0) {
            c.IR = 0;
            c.P &= !M6502_BF;
            pins &= !M6502_RES;

		    // Clear IRQ now we know we got it
		    // if (pins & M6502_IRQ != 0) {
    		// 	pins &= !M6502_IRQ;
		    // }
		}
		else {
            c.PC = c.PC.wrapping_add(1);
		}
	    }
	}
	RD!(pins);
 	match(c.IR) {
	        /* BRK  */
        0x000 /* ((0x00<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x001 /* ((0x00<<3)|1) */ => { if(0==(c.brk_flags&(M6502_BRK_IRQ|M6502_BRK_NMI))){c.PC += 1; }SAD!(pins,0x0100|(c.S as u16),c.PC>>8); c.S = c.S.wrapping_sub(1); if(0==(c.brk_flags&M6502_BRK_RESET)){WR!(pins); }; /* if(0==(c->brk_flags&(M6502_BRK_IRQ|M6502_BRK_NMI))){c->PC += 1; }_SAD(0x0100|c->S--,c->PC>>8); if(0==(c->brk_flags&M6502_BRK_RESET)){_WR(); } */ },
        0x002 /* ((0x00<<3)|2) */ => { SAD!(pins,0x0100|(c.S as u16),c.PC); c.S = c.S.wrapping_sub(1); if(0==(c.brk_flags&M6502_BRK_RESET)){WR!(pins); }; /* _SAD(0x0100|c->S--,c->PC); if(0==(c->brk_flags&M6502_BRK_RESET)){_WR(); } */ },
        0x003 /* ((0x00<<3)|3) */ => { SAD!(pins,0x0100|(c.S as u16),c.P|M6502_XF); c.S = c.S.wrapping_sub(1); if(0!=c.brk_flags&M6502_BRK_RESET){c.AD=0xFFFC; }else{WR!(pins); if(0!=c.brk_flags&M6502_BRK_NMI){c.AD=0xFFFA; }else{c.AD=0xFFFE; }}; /* _SAD(0x0100|c->S--,c->P|M6502_XF); if(c->brk_flags&M6502_BRK_RESET){c->AD=0xFFFC; }else{_WR(); if(c->brk_flags&M6502_BRK_NMI){c->AD=0xFFFA; }else{c->AD=0xFFFE; }} */ },
        0x004 /* ((0x00<<3)|4) */ => { SA!(pins,c.AD); c.AD = c.AD.wrapping_add(1); c.P|=(M6502_IF|M6502_BF); c.brk_flags=0; /* RES/NMI hijacking */; /* _SA(c->AD++); c->P|=(M6502_IF|M6502_BF); c->brk_flags=0;  /* RES/NMI hijacking */ */ },
        0x005 /* ((0x00<<3)|5) */ => { SA!(pins,c.AD); c.AD=GD!(pins) as u16;  /* NMI "half-hijacking" not possible */ /* _SA(c->AD); c->AD=_GD();  /* NMI "half-hijacking" not possible */ */ },
        0x006 /* ((0x00<<3)|6) */ => { c.PC=((GD!(pins) as u16)<<8)|c.AD; /* c->PC=(_GD()<<8)|c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ORA (zp,X) */
        0x008 /* ((0x01<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x009 /* ((0x01<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x00A /* ((0x01<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x00B /* ((0x01<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x00C /* ((0x01<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x00D /* ((0x01<<3)|5) */ => { c.A|=GD!(pins); c.NZ(c.A); /* c->A|=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x010 /* ((0x02<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x011 /* ((0x02<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* SLO (zp,X) (undoc) */
        0x018 /* ((0x03<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x019 /* ((0x03<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x01A /* ((0x03<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x01B /* ((0x03<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x01C /* ((0x03<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x01D /* ((0x03<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x01E /* ((0x03<<3)|6) */ => { c.AD=c.asl(c.AD as u8) as u16; SD!(pins, c.AD); c.A|=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_asl(c,c->AD); _SD(c->AD); c->A|=c->AD; _NZ(c->A); _WR(); */ },
        0x01F /* ((0x03<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP zp (undoc) */
        0x020 /* ((0x04<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x021 /* ((0x04<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x022 /* ((0x04<<3)|2) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ORA zp */
        0x028 /* ((0x05<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x029 /* ((0x05<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x02A /* ((0x05<<3)|2) */ => { c.A|=GD!(pins); c.NZ(c.A); /* c->A|=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ASL zp */
        0x030 /* ((0x06<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x031 /* ((0x06<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x032 /* ((0x06<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x033 /* ((0x06<<3)|3) */ => { SD!(pins, c.asl(c.AD as u8)); WR!(pins); /* _SD(_m6502_asl(c,c->AD)); _WR(); */ },
        0x034 /* ((0x06<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SLO zp (undoc) */
        0x038 /* ((0x07<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x039 /* ((0x07<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x03A /* ((0x07<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x03B /* ((0x07<<3)|3) */ => { c.AD=c.asl(c.AD as u8) as u16; SD!(pins, c.AD); c.A|=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_asl(c,c->AD); _SD(c->AD); c->A|=c->AD; _NZ(c->A); _WR(); */ },
        0x03C /* ((0x07<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* PHP  */
        0x040 /* ((0x08<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x041 /* ((0x08<<3)|1) */ => { SAD!(pins,0x0100|(c.S as u16),c.P|M6502_XF); c.S = c.S.wrapping_sub(1); WR!(pins);  /* _SAD(0x0100|c->S--,c->P|M6502_XF); _WR(); */ },
        0x042 /* ((0x08<<3)|2) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* ORA # */
        0x048 /* ((0x09<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x049 /* ((0x09<<3)|1) */ => { c.A|=GD!(pins); c.NZ(c.A); /* c->A|=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ASLA  */
        0x050 /* ((0x0A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x051 /* ((0x0A<<3)|1) */ => { c.A=c.asl(c.A); /* c->A=_m6502_asl(c,c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ANC # (undoc) */
        0x058 /* ((0x0B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x059 /* ((0x0B<<3)|1) */ => { c.A&=GD!(pins); c.NZ(c.A); if(0!=c.A&0x80){c.P|=M6502_CF; }else{c.P&=!M6502_CF; } /* c->A&=_GD(); _NZ(c->A); if(c->A&0x80){c->P|=M6502_CF; }else{c->P&=~M6502_CF; } */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP abs (undoc) */
        0x060 /* ((0x0C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x061 /* ((0x0C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x062 /* ((0x0C<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x063 /* ((0x0C<<3)|3) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ORA abs */
        0x068 /* ((0x0D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x069 /* ((0x0D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x06A /* ((0x0D<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x06B /* ((0x0D<<3)|3) */ => { c.A|=GD!(pins); c.NZ(c.A); /* c->A|=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ASL abs */
        0x070 /* ((0x0E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x071 /* ((0x0E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x072 /* ((0x0E<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x073 /* ((0x0E<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x074 /* ((0x0E<<3)|4) */ => { SD!(pins, c.asl(c.AD as u8)); WR!(pins); /* _SD(_m6502_asl(c,c->AD)); _WR(); */ },
        0x075 /* ((0x0E<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SLO abs (undoc) */
        0x078 /* ((0x0F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x079 /* ((0x0F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x07A /* ((0x0F<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x07B /* ((0x0F<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x07C /* ((0x0F<<3)|4) */ => { c.AD=c.asl(c.AD as u8) as u16; SD!(pins, c.AD); c.A|=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_asl(c,c->AD); _SD(c->AD); c->A|=c->AD; _NZ(c->A); _WR(); */ },
        0x07D /* ((0x0F<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* BPL # */
        0x080 /* ((0x10<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x081 /* ((0x10<<3)|1) */ => { SA!(pins,c.PC); c.AD = addi8tou16(c.PC, GD!(pins)); if((c.P&0x80)!=0x0){FETCH!(pins,c); }; /* _SA(c->PC); c->AD=c->PC+(int8_t)_GD(); if((c->P&0x80)!=0x0){_FETCH(); }; */ },
        0x082 /* ((0x10<<3)|2) */ => { SA!(pins,(c.PC&0xFF00)|(c.AD&0x00FF)); if((c.AD&0xFF00)==(c.PC&0xFF00)){c.PC=c.AD; c.irq_pip>>=1; c.nmi_pip>>=1; FETCH!(pins,c); }; /* _SA((c->PC&0xFF00)|(c->AD&0x00FF)); if((c->AD&0xFF00)==(c->PC&0xFF00)){c->PC=c->AD; c->irq_pip>>=1; c->nmi_pip>>=1; _FETCH(); }; */ },
        0x083 /* ((0x10<<3)|3) */ => { c.PC=c.AD; /* c->PC=c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ORA (zp),Y */
        0x088 /* ((0x11<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x089 /* ((0x11<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x08A /* ((0x11<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x08B /* ((0x11<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x08C /* ((0x11<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x08D /* ((0x11<<3)|5) */ => { c.A|=GD!(pins); c.NZ(c.A); /* c->A|=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x090 /* ((0x12<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x091 /* ((0x12<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* SLO (zp),Y (undoc) */
        0x098 /* ((0x13<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x099 /* ((0x13<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x09A /* ((0x13<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x09B /* ((0x13<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x09C /* ((0x13<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x09D /* ((0x13<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x09E /* ((0x13<<3)|6) */ => { c.AD=c.asl(c.AD as u8) as u16; SD!(pins, c.AD); c.A|=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_asl(c,c->AD); _SD(c->AD); c->A|=c->AD; _NZ(c->A); _WR(); */ },
        0x09F /* ((0x13<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP zp,X (undoc) */
        0x0A0 /* ((0x14<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0A1 /* ((0x14<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x0A2 /* ((0x14<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x0A3 /* ((0x14<<3)|3) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ORA zp,X */
        0x0A8 /* ((0x15<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0A9 /* ((0x15<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x0AA /* ((0x15<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x0AB /* ((0x15<<3)|3) */ => { c.A|=GD!(pins); c.NZ(c.A); /* c->A|=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ASL zp,X */
        0x0B0 /* ((0x16<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0B1 /* ((0x16<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x0B2 /* ((0x16<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x0B3 /* ((0x16<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x0B4 /* ((0x16<<3)|4) */ => { SD!(pins, c.asl(c.AD as u8)); WR!(pins); /* _SD(_m6502_asl(c,c->AD)); _WR(); */ },
        0x0B5 /* ((0x16<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SLO zp,X (undoc) */
        0x0B8 /* ((0x17<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0B9 /* ((0x17<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x0BA /* ((0x17<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x0BB /* ((0x17<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x0BC /* ((0x17<<3)|4) */ => { c.AD=c.asl(c.AD as u8) as u16; SD!(pins, c.AD); c.A|=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_asl(c,c->AD); _SD(c->AD); c->A|=c->AD; _NZ(c->A); _WR(); */ },
        0x0BD /* ((0x17<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* CLC  */
        0x0C0 /* ((0x18<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x0C1 /* ((0x18<<3)|1) */ => { c.P&=!0x1; /* c->P&=~0x1; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ORA abs,Y */
        0x0C8 /* ((0x19<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0C9 /* ((0x19<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x0CA /* ((0x19<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x0CB /* ((0x19<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x0CC /* ((0x19<<3)|4) */ => { c.A|=GD!(pins); c.NZ(c.A); /* c->A|=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP  (undoc) */
        0x0D0 /* ((0x1A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x0D1 /* ((0x1A<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SLO abs,Y (undoc) */
        0x0D8 /* ((0x1B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0D9 /* ((0x1B<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x0DA /* ((0x1B<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x0DB /* ((0x1B<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x0DC /* ((0x1B<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x0DD /* ((0x1B<<3)|5) */ => { c.AD=c.asl(c.AD as u8) as u16; SD!(pins, c.AD); c.A|=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_asl(c,c->AD); _SD(c->AD); c->A|=c->AD; _NZ(c->A); _WR(); */ },
        0x0DE /* ((0x1B<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP abs,X (undoc) */
        0x0E0 /* ((0x1C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0E1 /* ((0x1C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x0E2 /* ((0x1C<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x0E3 /* ((0x1C<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x0E4 /* ((0x1C<<3)|4) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ORA abs,X */
        0x0E8 /* ((0x1D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0E9 /* ((0x1D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x0EA /* ((0x1D<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x0EB /* ((0x1D<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x0EC /* ((0x1D<<3)|4) */ => { c.A|=GD!(pins); c.NZ(c.A); /* c->A|=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ASL abs,X */
        0x0F0 /* ((0x1E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0F1 /* ((0x1E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x0F2 /* ((0x1E<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x0F3 /* ((0x1E<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x0F4 /* ((0x1E<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x0F5 /* ((0x1E<<3)|5) */ => { SD!(pins, c.asl(c.AD as u8)); WR!(pins); /* _SD(_m6502_asl(c,c->AD)); _WR(); */ },
        0x0F6 /* ((0x1E<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SLO abs,X (undoc) */
        0x0F8 /* ((0x1F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x0F9 /* ((0x1F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x0FA /* ((0x1F<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x0FB /* ((0x1F<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x0FC /* ((0x1F<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x0FD /* ((0x1F<<3)|5) */ => { c.AD=c.asl(c.AD as u8) as u16; SD!(pins, c.AD); c.A|=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_asl(c,c->AD); _SD(c->AD); c->A|=c->AD; _NZ(c->A); _WR(); */ },
        0x0FE /* ((0x1F<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* JSR  */
        0x100 /* ((0x20<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x101 /* ((0x20<<3)|1) */ => { SA!(pins,0x0100|(c.S as u16)); c.AD=GD!(pins) as u16; /* _SA(0x0100|c->S); c->AD=_GD(); */ },
        0x102 /* ((0x20<<3)|2) */ => { SAD!(pins,0x0100|(c.S as u16),c.PC>>8); c.S = c.S.wrapping_sub(1); WR!(pins);  /* _SAD(0x0100|c->S--,c->PC>>8); _WR(); */ },
        0x103 /* ((0x20<<3)|3) */ => { SAD!(pins,0x0100|(c.S as u16),c.PC); c.S = c.S.wrapping_sub(1); WR!(pins);  /* _SAD(0x0100|c->S--,c->PC); _WR(); */ },
        0x104 /* ((0x20<<3)|4) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x105 /* ((0x20<<3)|5) */ => { c.PC=((GD!(pins) as u16)<<8)|c.AD; /* c->PC=(_GD()<<8)|c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* AND (zp,X) */
        0x108 /* ((0x21<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x109 /* ((0x21<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x10A /* ((0x21<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x10B /* ((0x21<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x10C /* ((0x21<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x10D /* ((0x21<<3)|5) */ => { c.A&=GD!(pins); c.NZ(c.A); /* c->A&=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x110 /* ((0x22<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x111 /* ((0x22<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* RLA (zp,X) (undoc) */
        0x118 /* ((0x23<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x119 /* ((0x23<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x11A /* ((0x23<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x11B /* ((0x23<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x11C /* ((0x23<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x11D /* ((0x23<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x11E /* ((0x23<<3)|6) */ => { c.AD=c.rol(c.AD as u8) as u16; SD!(pins, c.AD); c.A&=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_rol(c,c->AD); _SD(c->AD); c->A&=c->AD; _NZ(c->A); _WR(); */ },
        0x11F /* ((0x23<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* BIT zp */
        0x120 /* ((0x24<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x121 /* ((0x24<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x122 /* ((0x24<<3)|2) */ => { c.bit(GD!(pins)); /* _m6502_bit(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* AND zp */
        0x128 /* ((0x25<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x129 /* ((0x25<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x12A /* ((0x25<<3)|2) */ => { c.A&=GD!(pins); c.NZ(c.A); /* c->A&=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ROL zp */
        0x130 /* ((0x26<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x131 /* ((0x26<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x132 /* ((0x26<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x133 /* ((0x26<<3)|3) */ => { SD!(pins, c.rol(c.AD as u8)); WR!(pins); /* _SD(_m6502_rol(c,c->AD)); _WR(); */ },
        0x134 /* ((0x26<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RLA zp (undoc) */
        0x138 /* ((0x27<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x139 /* ((0x27<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x13A /* ((0x27<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x13B /* ((0x27<<3)|3) */ => { c.AD=c.rol(c.AD as u8) as u16; SD!(pins, c.AD); c.A&=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_rol(c,c->AD); _SD(c->AD); c->A&=c->AD; _NZ(c->A); _WR(); */ },
        0x13C /* ((0x27<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* PLP  */
        0x140 /* ((0x28<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x141 /* ((0x28<<3)|1) */ => { SA!(pins,0x0100|(c.S as u16)); c.S = c.S.wrapping_add(1);  /* _SA(0x0100|c->S++); */ },
        0x142 /* ((0x28<<3)|2) */ => { SA!(pins,0x0100|(c.S as u16)); /* _SA(0x0100|c->S); */ },
        0x143 /* ((0x28<<3)|3) */ => { c.P=(GD!(pins)|M6502_BF)&!M6502_XF; /* c->P=(_GD()|M6502_BF)&~M6502_XF; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* AND # */
        0x148 /* ((0x29<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x149 /* ((0x29<<3)|1) */ => { c.A&=GD!(pins); c.NZ(c.A); /* c->A&=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ROLA  */
        0x150 /* ((0x2A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x151 /* ((0x2A<<3)|1) */ => { c.A=c.rol(c.A); /* c->A=_m6502_rol(c,c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ANC # (undoc) */
        0x158 /* ((0x2B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x159 /* ((0x2B<<3)|1) */ => { c.A&=GD!(pins); c.NZ(c.A); if(0!=c.A&0x80){c.P|=M6502_CF; }else{c.P&=!M6502_CF; } /* c->A&=_GD(); _NZ(c->A); if(c->A&0x80){c->P|=M6502_CF; }else{c->P&=~M6502_CF; } */ FETCH!(pins,c); /* _FETCH(); */ },
    /* BIT abs */
        0x160 /* ((0x2C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x161 /* ((0x2C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x162 /* ((0x2C<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x163 /* ((0x2C<<3)|3) */ => { c.bit(GD!(pins)); if false || c.AD == 0xC48D {println!("BIT ${:04X}: A=${:02X} data=${:02X} => flags:{:08b}",  pins & 0xffff, c.A, GD!(pins), c.P)};  /* _m6502_bit(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* AND abs */
        0x168 /* ((0x2D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x169 /* ((0x2D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x16A /* ((0x2D<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x16B /* ((0x2D<<3)|3) */ => { c.A&=GD!(pins); c.NZ(c.A); /* c->A&=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ROL abs */
        0x170 /* ((0x2E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x171 /* ((0x2E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x172 /* ((0x2E<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x173 /* ((0x2E<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x174 /* ((0x2E<<3)|4) */ => { SD!(pins, c.rol(c.AD as u8)); WR!(pins); /* _SD(_m6502_rol(c,c->AD)); _WR(); */ },
        0x175 /* ((0x2E<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RLA abs (undoc) */
        0x178 /* ((0x2F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x179 /* ((0x2F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x17A /* ((0x2F<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x17B /* ((0x2F<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x17C /* ((0x2F<<3)|4) */ => { c.AD=c.rol(c.AD as u8) as u16; SD!(pins, c.AD); c.A&=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_rol(c,c->AD); _SD(c->AD); c->A&=c->AD; _NZ(c->A); _WR(); */ },
        0x17D /* ((0x2F<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* BMI # */
        0x180 /* ((0x30<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x181 /* ((0x30<<3)|1) */ => { SA!(pins,c.PC); c.AD = addi8tou16(c.PC, GD!(pins)); if((c.P&0x80)!=0x80){FETCH!(pins,c); }; /* _SA(c->PC); c->AD=c->PC+(int8_t)_GD(); if((c->P&0x80)!=0x80){_FETCH(); }; */ },
        0x182 /* ((0x30<<3)|2) */ => { SA!(pins,(c.PC&0xFF00)|(c.AD&0x00FF)); if((c.AD&0xFF00)==(c.PC&0xFF00)){c.PC=c.AD; c.irq_pip>>=1; c.nmi_pip>>=1; FETCH!(pins,c); }; /* _SA((c->PC&0xFF00)|(c->AD&0x00FF)); if((c->AD&0xFF00)==(c->PC&0xFF00)){c->PC=c->AD; c->irq_pip>>=1; c->nmi_pip>>=1; _FETCH(); }; */ },
        0x183 /* ((0x30<<3)|3) */ => { c.PC=c.AD; /* c->PC=c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* AND (zp),Y */
        0x188 /* ((0x31<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x189 /* ((0x31<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x18A /* ((0x31<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x18B /* ((0x31<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x18C /* ((0x31<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x18D /* ((0x31<<3)|5) */ => { c.A&=GD!(pins); c.NZ(c.A); /* c->A&=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x190 /* ((0x32<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x191 /* ((0x32<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* RLA (zp),Y (undoc) */
        0x198 /* ((0x33<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x199 /* ((0x33<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x19A /* ((0x33<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x19B /* ((0x33<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x19C /* ((0x33<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x19D /* ((0x33<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x19E /* ((0x33<<3)|6) */ => { c.AD=c.rol(c.AD as u8) as u16; SD!(pins, c.AD); c.A&=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_rol(c,c->AD); _SD(c->AD); c->A&=c->AD; _NZ(c->A); _WR(); */ },
        0x19F /* ((0x33<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP zp,X (undoc) */
        0x1A0 /* ((0x34<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1A1 /* ((0x34<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x1A2 /* ((0x34<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x1A3 /* ((0x34<<3)|3) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* AND zp,X */
        0x1A8 /* ((0x35<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1A9 /* ((0x35<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x1AA /* ((0x35<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x1AB /* ((0x35<<3)|3) */ => { c.A&=GD!(pins); c.NZ(c.A); /* c->A&=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ROL zp,X */
        0x1B0 /* ((0x36<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1B1 /* ((0x36<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x1B2 /* ((0x36<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x1B3 /* ((0x36<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x1B4 /* ((0x36<<3)|4) */ => { SD!(pins, c.rol(c.AD as u8)); WR!(pins); /* _SD(_m6502_rol(c,c->AD)); _WR(); */ },
        0x1B5 /* ((0x36<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RLA zp,X (undoc) */
        0x1B8 /* ((0x37<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1B9 /* ((0x37<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x1BA /* ((0x37<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x1BB /* ((0x37<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x1BC /* ((0x37<<3)|4) */ => { c.AD=c.rol(c.AD as u8) as u16; SD!(pins, c.AD); c.A&=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_rol(c,c->AD); _SD(c->AD); c->A&=c->AD; _NZ(c->A); _WR(); */ },
        0x1BD /* ((0x37<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SEC  */
        0x1C0 /* ((0x38<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x1C1 /* ((0x38<<3)|1) */ => { c.P|=0x1; /* c->P|=0x1; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* AND abs,Y */
        0x1C8 /* ((0x39<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1C9 /* ((0x39<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x1CA /* ((0x39<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x1CB /* ((0x39<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x1CC /* ((0x39<<3)|4) */ => { c.A&=GD!(pins); c.NZ(c.A); /* c->A&=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP  (undoc) */
        0x1D0 /* ((0x3A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x1D1 /* ((0x3A<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* RLA abs,Y (undoc) */
        0x1D8 /* ((0x3B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1D9 /* ((0x3B<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x1DA /* ((0x3B<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x1DB /* ((0x3B<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x1DC /* ((0x3B<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x1DD /* ((0x3B<<3)|5) */ => { c.AD=c.rol(c.AD as u8) as u16; SD!(pins, c.AD); c.A&=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_rol(c,c->AD); _SD(c->AD); c->A&=c->AD; _NZ(c->A); _WR(); */ },
        0x1DE /* ((0x3B<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP abs,X (undoc) */
        0x1E0 /* ((0x3C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1E1 /* ((0x3C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x1E2 /* ((0x3C<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x1E3 /* ((0x3C<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x1E4 /* ((0x3C<<3)|4) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* AND abs,X */
        0x1E8 /* ((0x3D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1E9 /* ((0x3D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x1EA /* ((0x3D<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x1EB /* ((0x3D<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x1EC /* ((0x3D<<3)|4) */ => { c.A&=GD!(pins); c.NZ(c.A); /* c->A&=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ROL abs,X */
        0x1F0 /* ((0x3E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1F1 /* ((0x3E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x1F2 /* ((0x3E<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x1F3 /* ((0x3E<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x1F4 /* ((0x3E<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x1F5 /* ((0x3E<<3)|5) */ => { SD!(pins, c.rol(c.AD as u8)); WR!(pins); /* _SD(_m6502_rol(c,c->AD)); _WR(); */ },
        0x1F6 /* ((0x3E<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RLA abs,X (undoc) */
        0x1F8 /* ((0x3F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x1F9 /* ((0x3F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x1FA /* ((0x3F<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x1FB /* ((0x3F<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x1FC /* ((0x3F<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x1FD /* ((0x3F<<3)|5) */ => { c.AD=c.rol(c.AD as u8) as u16; SD!(pins, c.AD); c.A&=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_rol(c,c->AD); _SD(c->AD); c->A&=c->AD; _NZ(c->A); _WR(); */ },
        0x1FE /* ((0x3F<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RTI  */
        0x200 /* ((0x40<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x201 /* ((0x40<<3)|1) */ => { SA!(pins,0x0100|(c.S as u16)); c.S = c.S.wrapping_add(1);  /* _SA(0x0100|c->S++); */ },
        0x202 /* ((0x40<<3)|2) */ => { SA!(pins,0x0100|(c.S as u16)); c.S = c.S.wrapping_add(1);  /* _SA(0x0100|c->S++); */ },
        0x203 /* ((0x40<<3)|3) */ => { SA!(pins,0x0100|(c.S as u16)); c.S = c.S.wrapping_add(1); c.P=(GD!(pins)|M6502_BF)&!M6502_XF;  /* _SA(0x0100|c->S++); c->P=(_GD()|M6502_BF)&~M6502_XF; */ },
        0x204 /* ((0x40<<3)|4) */ => { SA!(pins,0x0100|(c.S as u16)); c.AD=GD!(pins) as u16; /* _SA(0x0100|c->S); c->AD=_GD(); */ },
        0x205 /* ((0x40<<3)|5) */ => { c.PC=((GD!(pins) as u16)<<8)|c.AD; /* c->PC=(_GD()<<8)|c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* EOR (zp,X) */
        0x208 /* ((0x41<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x209 /* ((0x41<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x20A /* ((0x41<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x20B /* ((0x41<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x20C /* ((0x41<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x20D /* ((0x41<<3)|5) */ => { c.A^=GD!(pins); c.NZ(c.A); /* c->A^=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x210 /* ((0x42<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x211 /* ((0x42<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* SRE (zp,X) (undoc) */
        0x218 /* ((0x43<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x219 /* ((0x43<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x21A /* ((0x43<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x21B /* ((0x43<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x21C /* ((0x43<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x21D /* ((0x43<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x21E /* ((0x43<<3)|6) */ => { c.AD=c.lsr(c.AD as u8) as u16; SD!(pins, c.AD); c.A^=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_lsr(c,c->AD); _SD(c->AD); c->A^=c->AD; _NZ(c->A); _WR(); */ },
        0x21F /* ((0x43<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP zp (undoc) */
        0x220 /* ((0x44<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x221 /* ((0x44<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x222 /* ((0x44<<3)|2) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* EOR zp */
        0x228 /* ((0x45<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x229 /* ((0x45<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x22A /* ((0x45<<3)|2) */ => { c.A^=GD!(pins); c.NZ(c.A); /* c->A^=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LSR zp */
        0x230 /* ((0x46<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x231 /* ((0x46<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x232 /* ((0x46<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x233 /* ((0x46<<3)|3) */ => { SD!(pins, c.lsr(c.AD as u8)); WR!(pins); /* _SD(_m6502_lsr(c,c->AD)); _WR(); */ },
        0x234 /* ((0x46<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SRE zp (undoc) */
        0x238 /* ((0x47<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x239 /* ((0x47<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x23A /* ((0x47<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x23B /* ((0x47<<3)|3) */ => { c.AD=c.lsr(c.AD as u8) as u16; SD!(pins, c.AD); c.A^=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_lsr(c,c->AD); _SD(c->AD); c->A^=c->AD; _NZ(c->A); _WR(); */ },
        0x23C /* ((0x47<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* PHA  */
        0x240 /* ((0x48<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x241 /* ((0x48<<3)|1) */ => { SAD!(pins,0x0100|(c.S as u16),c.A); c.S = c.S.wrapping_sub(1); WR!(pins);  /* _SAD(0x0100|c->S--,c->A); _WR(); */ },
        0x242 /* ((0x48<<3)|2) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* EOR # */
        0x248 /* ((0x49<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x249 /* ((0x49<<3)|1) */ => { c.A^=GD!(pins); c.NZ(c.A); /* c->A^=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LSRA  */
        0x250 /* ((0x4A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x251 /* ((0x4A<<3)|1) */ => { c.A=c.lsr(c.A); /* c->A=_m6502_lsr(c,c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ASR # (undoc) */
        0x258 /* ((0x4B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x259 /* ((0x4B<<3)|1) */ => { c.A&=GD!(pins); c.A=c.lsr(c.A); /* c->A&=_GD(); c->A=_m6502_lsr(c,c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JMP  */
        0x260 /* ((0x4C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x261 /* ((0x4C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x262 /* ((0x4C<<3)|2) */ => { c.PC=((GD!(pins) as u16)<<8)|c.AD; /* c->PC=(_GD()<<8)|c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* EOR abs */
        0x268 /* ((0x4D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x269 /* ((0x4D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x26A /* ((0x4D<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x26B /* ((0x4D<<3)|3) */ => { c.A^=GD!(pins); c.NZ(c.A); /* c->A^=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LSR abs */
        0x270 /* ((0x4E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x271 /* ((0x4E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x272 /* ((0x4E<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x273 /* ((0x4E<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x274 /* ((0x4E<<3)|4) */ => { SD!(pins, c.lsr(c.AD as u8)); WR!(pins); /* _SD(_m6502_lsr(c,c->AD)); _WR(); */ },
        0x275 /* ((0x4E<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SRE abs (undoc) */
        0x278 /* ((0x4F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x279 /* ((0x4F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x27A /* ((0x4F<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x27B /* ((0x4F<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x27C /* ((0x4F<<3)|4) */ => { c.AD=c.lsr(c.AD as u8) as u16; SD!(pins, c.AD); c.A^=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_lsr(c,c->AD); _SD(c->AD); c->A^=c->AD; _NZ(c->A); _WR(); */ },
        0x27D /* ((0x4F<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* BVC # */
        0x280 /* ((0x50<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x281 /* ((0x50<<3)|1) */ => { SA!(pins,c.PC); c.AD = addi8tou16(c.PC, GD!(pins)); if((c.P&0x40)!=0x0){FETCH!(pins,c); }; /* _SA(c->PC); c->AD=c->PC+(int8_t)_GD(); if((c->P&0x40)!=0x0){_FETCH(); }; */ },
        0x282 /* ((0x50<<3)|2) */ => { SA!(pins,(c.PC&0xFF00)|(c.AD&0x00FF)); if((c.AD&0xFF00)==(c.PC&0xFF00)){c.PC=c.AD; c.irq_pip>>=1; c.nmi_pip>>=1; FETCH!(pins,c); }; /* _SA((c->PC&0xFF00)|(c->AD&0x00FF)); if((c->AD&0xFF00)==(c->PC&0xFF00)){c->PC=c->AD; c->irq_pip>>=1; c->nmi_pip>>=1; _FETCH(); }; */ },
        0x283 /* ((0x50<<3)|3) */ => { c.PC=c.AD; /* c->PC=c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* EOR (zp),Y */
        0x288 /* ((0x51<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x289 /* ((0x51<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x28A /* ((0x51<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x28B /* ((0x51<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x28C /* ((0x51<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x28D /* ((0x51<<3)|5) */ => { c.A^=GD!(pins); c.NZ(c.A); /* c->A^=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x290 /* ((0x52<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x291 /* ((0x52<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* SRE (zp),Y (undoc) */
        0x298 /* ((0x53<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x299 /* ((0x53<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x29A /* ((0x53<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x29B /* ((0x53<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x29C /* ((0x53<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x29D /* ((0x53<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x29E /* ((0x53<<3)|6) */ => { c.AD=c.lsr(c.AD as u8) as u16; SD!(pins, c.AD); c.A^=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_lsr(c,c->AD); _SD(c->AD); c->A^=c->AD; _NZ(c->A); _WR(); */ },
        0x29F /* ((0x53<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP zp,X (undoc) */
        0x2A0 /* ((0x54<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2A1 /* ((0x54<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x2A2 /* ((0x54<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x2A3 /* ((0x54<<3)|3) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* EOR zp,X */
        0x2A8 /* ((0x55<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2A9 /* ((0x55<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x2AA /* ((0x55<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x2AB /* ((0x55<<3)|3) */ => { c.A^=GD!(pins); c.NZ(c.A); /* c->A^=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LSR zp,X */
        0x2B0 /* ((0x56<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2B1 /* ((0x56<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x2B2 /* ((0x56<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x2B3 /* ((0x56<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x2B4 /* ((0x56<<3)|4) */ => { SD!(pins, c.lsr(c.AD as u8)); WR!(pins); /* _SD(_m6502_lsr(c,c->AD)); _WR(); */ },
        0x2B5 /* ((0x56<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SRE zp,X (undoc) */
        0x2B8 /* ((0x57<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2B9 /* ((0x57<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x2BA /* ((0x57<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x2BB /* ((0x57<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x2BC /* ((0x57<<3)|4) */ => { c.AD=c.lsr(c.AD as u8) as u16; SD!(pins, c.AD); c.A^=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_lsr(c,c->AD); _SD(c->AD); c->A^=c->AD; _NZ(c->A); _WR(); */ },
        0x2BD /* ((0x57<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* CLI  */
        0x2C0 /* ((0x58<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x2C1 /* ((0x58<<3)|1) */ => { c.P&=!0x4; /* c->P&=~0x4; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* EOR abs,Y */
        0x2C8 /* ((0x59<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2C9 /* ((0x59<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x2CA /* ((0x59<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x2CB /* ((0x59<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x2CC /* ((0x59<<3)|4) */ => { c.A^=GD!(pins); c.NZ(c.A); /* c->A^=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP  (undoc) */
        0x2D0 /* ((0x5A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x2D1 /* ((0x5A<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SRE abs,Y (undoc) */
        0x2D8 /* ((0x5B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2D9 /* ((0x5B<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x2DA /* ((0x5B<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x2DB /* ((0x5B<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x2DC /* ((0x5B<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x2DD /* ((0x5B<<3)|5) */ => { c.AD=c.lsr(c.AD as u8) as u16; SD!(pins, c.AD); c.A^=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_lsr(c,c->AD); _SD(c->AD); c->A^=c->AD; _NZ(c->A); _WR(); */ },
        0x2DE /* ((0x5B<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP abs,X (undoc) */
        0x2E0 /* ((0x5C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2E1 /* ((0x5C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x2E2 /* ((0x5C<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x2E3 /* ((0x5C<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x2E4 /* ((0x5C<<3)|4) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* EOR abs,X */
        0x2E8 /* ((0x5D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2E9 /* ((0x5D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x2EA /* ((0x5D<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x2EB /* ((0x5D<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x2EC /* ((0x5D<<3)|4) */ => { c.A^=GD!(pins); c.NZ(c.A); /* c->A^=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LSR abs,X */
        0x2F0 /* ((0x5E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2F1 /* ((0x5E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x2F2 /* ((0x5E<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x2F3 /* ((0x5E<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x2F4 /* ((0x5E<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x2F5 /* ((0x5E<<3)|5) */ => { SD!(pins, c.lsr(c.AD as u8)); WR!(pins); /* _SD(_m6502_lsr(c,c->AD)); _WR(); */ },
        0x2F6 /* ((0x5E<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SRE abs,X (undoc) */
        0x2F8 /* ((0x5F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x2F9 /* ((0x5F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x2FA /* ((0x5F<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x2FB /* ((0x5F<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x2FC /* ((0x5F<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x2FD /* ((0x5F<<3)|5) */ => { c.AD=c.lsr(c.AD as u8) as u16; SD!(pins, c.AD); c.A^=c.AD as u8; c.NZ(c.A); WR!(pins); /* c->AD=_m6502_lsr(c,c->AD); _SD(c->AD); c->A^=c->AD; _NZ(c->A); _WR(); */ },
        0x2FE /* ((0x5F<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RTS  */
        0x300 /* ((0x60<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x301 /* ((0x60<<3)|1) */ => { SA!(pins,0x0100|(c.S as u16)); c.S = c.S.wrapping_add(1);  /* _SA(0x0100|c->S++); */ },
        0x302 /* ((0x60<<3)|2) */ => { SA!(pins,0x0100|(c.S as u16)); c.S = c.S.wrapping_add(1);  /* _SA(0x0100|c->S++); */ },
        0x303 /* ((0x60<<3)|3) */ => { SA!(pins,0x0100|(c.S as u16)); c.AD=GD!(pins) as u16; /* _SA(0x0100|c->S); c->AD=_GD(); */ },
        0x304 /* ((0x60<<3)|4) */ => { c.PC=((GD!(pins) as u16)<<8)|c.AD; SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* c->PC=(_GD()<<8)|c->AD; _SA(c->PC++); */ },
        0x305 /* ((0x60<<3)|5) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ADC (zp,X) */
        0x308 /* ((0x61<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x309 /* ((0x61<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x30A /* ((0x61<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x30B /* ((0x61<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x30C /* ((0x61<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x30D /* ((0x61<<3)|5) */ => { c.adc(GD!(pins)); /* _m6502_adc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x310 /* ((0x62<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x311 /* ((0x62<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* RRA (zp,X) (undoc) */
        0x318 /* ((0x63<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x319 /* ((0x63<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x31A /* ((0x63<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x31B /* ((0x63<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x31C /* ((0x63<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x31D /* ((0x63<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x31E /* ((0x63<<3)|6) */ => { c.AD=c.ror(c.AD as u8) as u16; SD!(pins, c.AD); c.adc(c.AD as u8); WR!(pins); /* c->AD=_m6502_ror(c,c->AD); _SD(c->AD); _m6502_adc(c,c->AD); _WR(); */ },
        0x31F /* ((0x63<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP zp (undoc) */
        0x320 /* ((0x64<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x321 /* ((0x64<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x322 /* ((0x64<<3)|2) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ADC zp */
        0x328 /* ((0x65<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x329 /* ((0x65<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x32A /* ((0x65<<3)|2) */ => { c.adc(GD!(pins)); /* _m6502_adc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ROR zp */
        0x330 /* ((0x66<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x331 /* ((0x66<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x332 /* ((0x66<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x333 /* ((0x66<<3)|3) */ => { SD!(pins, c.ror(c.AD as u8)); WR!(pins); /* _SD(_m6502_ror(c,c->AD)); _WR(); */ },
        0x334 /* ((0x66<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RRA zp (undoc) */
        0x338 /* ((0x67<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x339 /* ((0x67<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x33A /* ((0x67<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x33B /* ((0x67<<3)|3) */ => { c.AD=c.ror(c.AD as u8) as u16; SD!(pins, c.AD); c.adc(c.AD as u8); WR!(pins); /* c->AD=_m6502_ror(c,c->AD); _SD(c->AD); _m6502_adc(c,c->AD); _WR(); */ },
        0x33C /* ((0x67<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* PLA  */
        0x340 /* ((0x68<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x341 /* ((0x68<<3)|1) */ => { SA!(pins,0x0100|(c.S as u16)); c.S = c.S.wrapping_add(1);  /* _SA(0x0100|c->S++); */ },
        0x342 /* ((0x68<<3)|2) */ => { SA!(pins,0x0100|(c.S as u16)); /* _SA(0x0100|c->S); */ },
        0x343 /* ((0x68<<3)|3) */ => { c.A=GD!(pins); c.NZ(c.A); /* c->A=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ADC # */
        0x348 /* ((0x69<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x349 /* ((0x69<<3)|1) */ => { c.adc(GD!(pins)); /* _m6502_adc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* RORA  */
        0x350 /* ((0x6A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x351 /* ((0x6A<<3)|1) */ => { c.A=c.ror(c.A); /* c->A=_m6502_ror(c,c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ARR # (undoc) */
        0x358 /* ((0x6B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x359 /* ((0x6B<<3)|1) */ => { c.A&=GD!(pins); c.arr(); /* c->A&=_GD(); _m6502_arr(c); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JMPI  */
        0x360 /* ((0x6C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x361 /* ((0x6C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x362 /* ((0x6C<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,c.AD); /* c->AD|=_GD()<<8; _SA(c->AD); */ },
        0x363 /* ((0x6C<<3)|3) */ => { SA!(pins,(c.AD&0xFF00)|((c.AD+1)&0x00FF)); c.AD=GD!(pins) as u16; /* _SA((c->AD&0xFF00)|((c->AD+1)&0x00FF)); c->AD=_GD(); */ },
        0x364 /* ((0x6C<<3)|4) */ => { c.PC=((GD!(pins) as u16)<<8)|c.AD; /* c->PC=(_GD()<<8)|c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ADC abs */
        0x368 /* ((0x6D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x369 /* ((0x6D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x36A /* ((0x6D<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x36B /* ((0x6D<<3)|3) */ => { c.adc(GD!(pins)); /* _m6502_adc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ROR abs */
        0x370 /* ((0x6E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x371 /* ((0x6E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x372 /* ((0x6E<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x373 /* ((0x6E<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x374 /* ((0x6E<<3)|4) */ => { SD!(pins, c.ror(c.AD as u8)); WR!(pins); /* _SD(_m6502_ror(c,c->AD)); _WR(); */ },
        0x375 /* ((0x6E<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RRA abs (undoc) */
        0x378 /* ((0x6F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x379 /* ((0x6F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x37A /* ((0x6F<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x37B /* ((0x6F<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x37C /* ((0x6F<<3)|4) */ => { c.AD=c.ror(c.AD as u8) as u16; SD!(pins, c.AD); c.adc(c.AD as u8); WR!(pins); /* c->AD=_m6502_ror(c,c->AD); _SD(c->AD); _m6502_adc(c,c->AD); _WR(); */ },
        0x37D /* ((0x6F<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* BVS # */
        0x380 /* ((0x70<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x381 /* ((0x70<<3)|1) */ => { SA!(pins,c.PC); c.AD = addi8tou16(c.PC, GD!(pins)); if((c.P&0x40)!=0x40){FETCH!(pins,c); }; /* _SA(c->PC); c->AD=c->PC+(int8_t)_GD(); if((c->P&0x40)!=0x40){_FETCH(); }; */ },
        0x382 /* ((0x70<<3)|2) */ => { SA!(pins,(c.PC&0xFF00)|(c.AD&0x00FF)); if((c.AD&0xFF00)==(c.PC&0xFF00)){c.PC=c.AD; c.irq_pip>>=1; c.nmi_pip>>=1; FETCH!(pins,c); }; /* _SA((c->PC&0xFF00)|(c->AD&0x00FF)); if((c->AD&0xFF00)==(c->PC&0xFF00)){c->PC=c->AD; c->irq_pip>>=1; c->nmi_pip>>=1; _FETCH(); }; */ },
        0x383 /* ((0x70<<3)|3) */ => { c.PC=c.AD; /* c->PC=c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ADC (zp),Y */
        0x388 /* ((0x71<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x389 /* ((0x71<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x38A /* ((0x71<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x38B /* ((0x71<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x38C /* ((0x71<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x38D /* ((0x71<<3)|5) */ => { c.adc(GD!(pins)); /* _m6502_adc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x390 /* ((0x72<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x391 /* ((0x72<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* RRA (zp),Y (undoc) */
        0x398 /* ((0x73<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x399 /* ((0x73<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x39A /* ((0x73<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x39B /* ((0x73<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x39C /* ((0x73<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x39D /* ((0x73<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x39E /* ((0x73<<3)|6) */ => { c.AD=c.ror(c.AD as u8) as u16; SD!(pins, c.AD); c.adc(c.AD as u8); WR!(pins); /* c->AD=_m6502_ror(c,c->AD); _SD(c->AD); _m6502_adc(c,c->AD); _WR(); */ },
        0x39F /* ((0x73<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP zp,X (undoc) */
        0x3A0 /* ((0x74<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3A1 /* ((0x74<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x3A2 /* ((0x74<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x3A3 /* ((0x74<<3)|3) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ADC zp,X */
        0x3A8 /* ((0x75<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3A9 /* ((0x75<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x3AA /* ((0x75<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x3AB /* ((0x75<<3)|3) */ => { c.adc(GD!(pins)); /* _m6502_adc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ROR zp,X */
        0x3B0 /* ((0x76<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3B1 /* ((0x76<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x3B2 /* ((0x76<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x3B3 /* ((0x76<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x3B4 /* ((0x76<<3)|4) */ => { SD!(pins, c.ror(c.AD as u8)); WR!(pins); /* _SD(_m6502_ror(c,c->AD)); _WR(); */ },
        0x3B5 /* ((0x76<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RRA zp,X (undoc) */
        0x3B8 /* ((0x77<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3B9 /* ((0x77<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x3BA /* ((0x77<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x3BB /* ((0x77<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x3BC /* ((0x77<<3)|4) */ => { c.AD=c.ror(c.AD as u8) as u16; SD!(pins, c.AD); c.adc(c.AD as u8); WR!(pins); /* c->AD=_m6502_ror(c,c->AD); _SD(c->AD); _m6502_adc(c,c->AD); _WR(); */ },
        0x3BD /* ((0x77<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SEI  */
        0x3C0 /* ((0x78<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x3C1 /* ((0x78<<3)|1) */ => { c.P|=0x4; /* c->P|=0x4; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ADC abs,Y */
        0x3C8 /* ((0x79<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3C9 /* ((0x79<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x3CA /* ((0x79<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x3CB /* ((0x79<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x3CC /* ((0x79<<3)|4) */ => { c.adc(GD!(pins)); /* _m6502_adc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP  (undoc) */
        0x3D0 /* ((0x7A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x3D1 /* ((0x7A<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* RRA abs,Y (undoc) */
        0x3D8 /* ((0x7B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3D9 /* ((0x7B<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x3DA /* ((0x7B<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x3DB /* ((0x7B<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x3DC /* ((0x7B<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x3DD /* ((0x7B<<3)|5) */ => { c.AD=c.ror(c.AD as u8) as u16; SD!(pins, c.AD); c.adc(c.AD as u8); WR!(pins); /* c->AD=_m6502_ror(c,c->AD); _SD(c->AD); _m6502_adc(c,c->AD); _WR(); */ },
        0x3DE /* ((0x7B<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP abs,X (undoc) */
        0x3E0 /* ((0x7C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3E1 /* ((0x7C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x3E2 /* ((0x7C<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x3E3 /* ((0x7C<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x3E4 /* ((0x7C<<3)|4) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ADC abs,X */
        0x3E8 /* ((0x7D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3E9 /* ((0x7D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x3EA /* ((0x7D<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x3EB /* ((0x7D<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x3EC /* ((0x7D<<3)|4) */ => { c.adc(GD!(pins)); /* _m6502_adc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ROR abs,X */
        0x3F0 /* ((0x7E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3F1 /* ((0x7E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x3F2 /* ((0x7E<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x3F3 /* ((0x7E<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x3F4 /* ((0x7E<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x3F5 /* ((0x7E<<3)|5) */ => { SD!(pins, c.ror(c.AD as u8)); WR!(pins); /* _SD(_m6502_ror(c,c->AD)); _WR(); */ },
        0x3F6 /* ((0x7E<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* RRA abs,X (undoc) */
        0x3F8 /* ((0x7F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x3F9 /* ((0x7F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x3FA /* ((0x7F<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x3FB /* ((0x7F<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x3FC /* ((0x7F<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x3FD /* ((0x7F<<3)|5) */ => { c.AD=c.ror(c.AD as u8) as u16; SD!(pins, c.AD); c.adc(c.AD as u8); WR!(pins); /* c->AD=_m6502_ror(c,c->AD); _SD(c->AD); _m6502_adc(c,c->AD); _WR(); */ },
        0x3FE /* ((0x7F<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP # (undoc) */
        0x400 /* ((0x80<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x401 /* ((0x80<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* STA (zp,X) */
        0x408 /* ((0x81<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x409 /* ((0x81<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x40A /* ((0x81<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x40B /* ((0x81<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x40C /* ((0x81<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ SD!(pins, c.A); WR!(pins); /* _SD(c->A); _WR(); */ },
        0x40D /* ((0x81<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP # (undoc) */
        0x410 /* ((0x82<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x411 /* ((0x82<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SAX (zp,X) (undoc) */
        0x418 /* ((0x83<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x419 /* ((0x83<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x41A /* ((0x83<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x41B /* ((0x83<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x41C /* ((0x83<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ SD!(pins, c.A&c.X); WR!(pins); /* _SD(c->A&c->X); _WR(); */ },
        0x41D /* ((0x83<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* STY zp */
        0x420 /* ((0x84<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x421 /* ((0x84<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ SD!(pins, c.Y); WR!(pins); /* _SD(c->Y); _WR(); */ },
        0x422 /* ((0x84<<3)|2) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* STA zp */
        0x428 /* ((0x85<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x429 /* ((0x85<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ SD!(pins, c.A); WR!(pins); /* _SD(c->A); _WR(); */ },
        0x42A /* ((0x85<<3)|2) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* STX zp */
        0x430 /* ((0x86<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x431 /* ((0x86<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ SD!(pins, c.X); WR!(pins); /* _SD(c->X); _WR(); */ },
        0x432 /* ((0x86<<3)|2) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SAX zp (undoc) */
        0x438 /* ((0x87<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x439 /* ((0x87<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ SD!(pins, c.A&c.X); WR!(pins); /* _SD(c->A&c->X); _WR(); */ },
        0x43A /* ((0x87<<3)|2) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* DEY  */
        0x440 /* ((0x88<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x441 /* ((0x88<<3)|1) */ => { c.Y = c.Y.wrapping_sub(1); c.NZ(c.Y);  /* c->Y--; _NZ(c->Y); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP # (undoc) */
        0x448 /* ((0x89<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x449 /* ((0x89<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* TXA  */
        0x450 /* ((0x8A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x451 /* ((0x8A<<3)|1) */ => { c.A=c.X; c.NZ(c.A); /* c->A=c->X; _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ANE # (undoc) */
        0x458 /* ((0x8B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x459 /* ((0x8B<<3)|1) */ => { c.A=(c.A|0xEE)&c.X&GD!(pins); c.NZ(c.A); /* c->A=(c->A|0xEE)&c->X&_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* STY abs */
        0x460 /* ((0x8C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x461 /* ((0x8C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x462 /* ((0x8C<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ SD!(pins, c.Y); WR!(pins); /* _SD(c->Y); _WR(); */ },
        0x463 /* ((0x8C<<3)|3) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* STA abs */
        0x468 /* ((0x8D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x469 /* ((0x8D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x46A /* ((0x8D<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ SD!(pins, c.A); WR!(pins); /* _SD(c->A); _WR(); */ },
        0x46B /* ((0x8D<<3)|3) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* STX abs */
        0x470 /* ((0x8E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x471 /* ((0x8E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x472 /* ((0x8E<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ SD!(pins, c.X); WR!(pins); /* _SD(c->X); _WR(); */ },
        0x473 /* ((0x8E<<3)|3) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SAX abs (undoc) */
        0x478 /* ((0x8F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x479 /* ((0x8F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x47A /* ((0x8F<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ SD!(pins, c.A&c.X); WR!(pins); /* _SD(c->A&c->X); _WR(); */ },
        0x47B /* ((0x8F<<3)|3) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* BCC # */
        0x480 /* ((0x90<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x481 /* ((0x90<<3)|1) */ => { SA!(pins,c.PC); c.AD = addi8tou16(c.PC, GD!(pins)); if((c.P&0x1)!=0x0){FETCH!(pins,c); }; /* _SA(c->PC); c->AD=c->PC+(int8_t)_GD(); if((c->P&0x1)!=0x0){_FETCH(); }; */ },
        0x482 /* ((0x90<<3)|2) */ => { SA!(pins,(c.PC&0xFF00)|(c.AD&0x00FF)); if((c.AD&0xFF00)==(c.PC&0xFF00)){c.PC=c.AD; c.irq_pip>>=1; c.nmi_pip>>=1; FETCH!(pins,c); }; /* _SA((c->PC&0xFF00)|(c->AD&0x00FF)); if((c->AD&0xFF00)==(c->PC&0xFF00)){c->PC=c->AD; c->irq_pip>>=1; c->nmi_pip>>=1; _FETCH(); }; */ },
        0x483 /* ((0x90<<3)|3) */ => { c.PC=c.AD; /* c->PC=c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* STA (zp),Y */
        0x488 /* ((0x91<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x489 /* ((0x91<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x48A /* ((0x91<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x48B /* ((0x91<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x48C /* ((0x91<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ SD!(pins, c.A); WR!(pins); /* _SD(c->A); _WR(); */ },
        0x48D /* ((0x91<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x490 /* ((0x92<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x491 /* ((0x92<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* SHA (zp),Y (undoc) */
        0x498 /* ((0x93<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x499 /* ((0x93<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x49A /* ((0x93<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x49B /* ((0x93<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x49C /* ((0x93<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ SD!(pins, c.A&c.X&(((GA!(pins)>>8)+1) as u8)); WR!(pins); /* _SD(c->A&c->X&(uint8_t)((_GA()>>8)+1)); _WR(); */ },
        0x49D /* ((0x93<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* STY zp,X */
        0x4A0 /* ((0x94<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4A1 /* ((0x94<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x4A2 /* ((0x94<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ SD!(pins, c.Y); WR!(pins); /* _SD(c->Y); _WR(); */ },
        0x4A3 /* ((0x94<<3)|3) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* STA zp,X */
        0x4A8 /* ((0x95<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4A9 /* ((0x95<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x4AA /* ((0x95<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ SD!(pins, c.A); WR!(pins); /* _SD(c->A); _WR(); */ },
        0x4AB /* ((0x95<<3)|3) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* STX zp,Y */
        0x4B0 /* ((0x96<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4B1 /* ((0x96<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x4B2 /* ((0x96<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.Y as u16))&0x00FF); /* _SA((c->AD+c->Y)&0x00FF); */ SD!(pins, c.X); WR!(pins); /* _SD(c->X); _WR(); */ },
        0x4B3 /* ((0x96<<3)|3) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SAX zp,Y (undoc) */
        0x4B8 /* ((0x97<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4B9 /* ((0x97<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x4BA /* ((0x97<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.Y as u16))&0x00FF); /* _SA((c->AD+c->Y)&0x00FF); */ SD!(pins, c.A&c.X); WR!(pins); /* _SD(c->A&c->X); _WR(); */ },
        0x4BB /* ((0x97<<3)|3) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* TYA  */
        0x4C0 /* ((0x98<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x4C1 /* ((0x98<<3)|1) */ => { c.A=c.Y; c.NZ(c.A); /* c->A=c->Y; _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* STA abs,Y */
        0x4C8 /* ((0x99<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4C9 /* ((0x99<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x4CA /* ((0x99<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x4CB /* ((0x99<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ SD!(pins, c.A); WR!(pins); /* _SD(c->A); _WR(); */ },
        0x4CC /* ((0x99<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* TXS  */
        0x4D0 /* ((0x9A<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x4D1 /* ((0x9A<<3)|1) */ => { c.S=c.X; /* c->S=c->X; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SHS abs,Y (undoc) */
        0x4D8 /* ((0x9B<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4D9 /* ((0x9B<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x4DA /* ((0x9B<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x4DB /* ((0x9B<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ c.S=c.A&c.X; SD!(pins, c.S&(((GA!(pins)>>8)+1) as u8)); WR!(pins); /* c->S=c->A&c->X; _SD(c->S&(uint8_t)((_GA()>>8)+1)); _WR(); */ },
        0x4DC /* ((0x9B<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SHY abs,X (undoc) */
        0x4E0 /* ((0x9C<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4E1 /* ((0x9C<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x4E2 /* ((0x9C<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x4E3 /* ((0x9C<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ SD!(pins, c.Y&(((GA!(pins)>>8)+1) as u8)); WR!(pins); /* _SD(c->Y&(uint8_t)((_GA()>>8)+1)); _WR(); */ },
        0x4E4 /* ((0x9C<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* STA abs,X */
        0x4E8 /* ((0x9D<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4E9 /* ((0x9D<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x4EA /* ((0x9D<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x4EB /* ((0x9D<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ SD!(pins, c.A); WR!(pins); /* _SD(c->A); _WR(); */ },
        0x4EC /* ((0x9D<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SHX abs,Y (undoc) */
        0x4F0 /* ((0x9E<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4F1 /* ((0x9E<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x4F2 /* ((0x9E<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x4F3 /* ((0x9E<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ SD!(pins, c.X&(((GA!(pins)>>8)+1) as u8)); WR!(pins); /* _SD(c->X&(uint8_t)((_GA()>>8)+1)); _WR(); */ },
        0x4F4 /* ((0x9E<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SHA abs,Y (undoc) */
        0x4F8 /* ((0x9F<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x4F9 /* ((0x9F<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x4FA /* ((0x9F<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x4FB /* ((0x9F<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ SD!(pins, c.A&c.X&(((GA!(pins)>>8)+1) as u8)); WR!(pins); /* _SD(c->A&c->X&(uint8_t)((_GA()>>8)+1)); _WR(); */ },
        0x4FC /* ((0x9F<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* LDY # */
        0x500 /* ((0xA0<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x501 /* ((0xA0<<3)|1) */ => { c.Y=GD!(pins); c.NZ(c.Y); /* c->Y=_GD(); _NZ(c->Y); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDA (zp,X) */
        0x508 /* ((0xA1<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x509 /* ((0xA1<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x50A /* ((0xA1<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x50B /* ((0xA1<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x50C /* ((0xA1<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x50D /* ((0xA1<<3)|5) */ => { c.A=GD!(pins); c.NZ(c.A); /* c->A=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDX # */
        0x510 /* ((0xA2<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x511 /* ((0xA2<<3)|1) */ => { c.X=GD!(pins); c.NZ(c.X); /* c->X=_GD(); _NZ(c->X); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LAX (zp,X) (undoc) */
        0x518 /* ((0xA3<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x519 /* ((0xA3<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x51A /* ((0xA3<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x51B /* ((0xA3<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x51C /* ((0xA3<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x51D /* ((0xA3<<3)|5) */ => { c.X=GD!(pins); c.A=c.X; c.NZ(c.A); /* c->X=_GD(); c->A=c->X; _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDY zp */
        0x520 /* ((0xA4<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x521 /* ((0xA4<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x522 /* ((0xA4<<3)|2) */ => { c.Y=GD!(pins); c.NZ(c.Y); /* c->Y=_GD(); _NZ(c->Y); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDA zp */
        0x528 /* ((0xA5<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x529 /* ((0xA5<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x52A /* ((0xA5<<3)|2) */ => { c.A=GD!(pins); c.NZ(c.A); /* c->A=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDX zp */
        0x530 /* ((0xA6<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x531 /* ((0xA6<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x532 /* ((0xA6<<3)|2) */ => { c.X=GD!(pins); c.NZ(c.X); /* c->X=_GD(); _NZ(c->X); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LAX zp (undoc) */
        0x538 /* ((0xA7<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x539 /* ((0xA7<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x53A /* ((0xA7<<3)|2) */ => { c.X=GD!(pins); c.A=c.X; c.NZ(c.A); /* c->X=_GD(); c->A=c->X; _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* TAY  */
        0x540 /* ((0xA8<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x541 /* ((0xA8<<3)|1) */ => { c.Y=c.A; c.NZ(c.Y); /* c->Y=c->A; _NZ(c->Y); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDA # */
        0x548 /* ((0xA9<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x549 /* ((0xA9<<3)|1) */ => { c.A=GD!(pins); c.NZ(c.A); /* c->A=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* TAX  */
        0x550 /* ((0xAA<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x551 /* ((0xAA<<3)|1) */ => { c.X=c.A; c.NZ(c.X); /* c->X=c->A; _NZ(c->X); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LXA # (undoc) */
        0x558 /* ((0xAB<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x559 /* ((0xAB<<3)|1) */ => { c.X=(c.A|0xEE)&GD!(pins);  c.A=c.X;  c.NZ(c.A); /* c->X=(c->A|0xEE)&_GD();  c->A=c->X;  _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDY abs */
        0x560 /* ((0xAC<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x561 /* ((0xAC<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x562 /* ((0xAC<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x563 /* ((0xAC<<3)|3) */ => { c.Y=GD!(pins); c.NZ(c.Y); /* c->Y=_GD(); _NZ(c->Y); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDA abs */
        0x568 /* ((0xAD<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x569 /* ((0xAD<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x56A /* ((0xAD<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x56B /* ((0xAD<<3)|3) */ => { c.A=GD!(pins); c.NZ(c.A); /* c->A=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDX abs */
        0x570 /* ((0xAE<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x571 /* ((0xAE<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x572 /* ((0xAE<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x573 /* ((0xAE<<3)|3) */ => { c.X=GD!(pins); c.NZ(c.X); /* c->X=_GD(); _NZ(c->X); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LAX abs (undoc) */
        0x578 /* ((0xAF<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x579 /* ((0xAF<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x57A /* ((0xAF<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x57B /* ((0xAF<<3)|3) */ => { c.X=GD!(pins); c.A=c.X; c.NZ(c.A); /* c->X=_GD(); c->A=c->X; _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* BCS # */
        0x580 /* ((0xB0<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x581 /* ((0xB0<<3)|1) */ => { SA!(pins,c.PC); c.AD = addi8tou16(c.PC, GD!(pins)); if((c.P&0x1)!=0x1){FETCH!(pins,c); }; /* _SA(c->PC); c->AD=c->PC+(int8_t)_GD(); if((c->P&0x1)!=0x1){_FETCH(); }; */ },
        0x582 /* ((0xB0<<3)|2) */ => { SA!(pins,(c.PC&0xFF00)|(c.AD&0x00FF)); if((c.AD&0xFF00)==(c.PC&0xFF00)){c.PC=c.AD; c.irq_pip>>=1; c.nmi_pip>>=1; FETCH!(pins,c); }; /* _SA((c->PC&0xFF00)|(c->AD&0x00FF)); if((c->AD&0xFF00)==(c->PC&0xFF00)){c->PC=c->AD; c->irq_pip>>=1; c->nmi_pip>>=1; _FETCH(); }; */ },
        0x583 /* ((0xB0<<3)|3) */ => { c.PC=c.AD; /* c->PC=c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDA (zp),Y */
        0x588 /* ((0xB1<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x589 /* ((0xB1<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x58A /* ((0xB1<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x58B /* ((0xB1<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x58C /* ((0xB1<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x58D /* ((0xB1<<3)|5) */ => { c.A=GD!(pins); c.NZ(c.A); /* c->A=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x590 /* ((0xB2<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x591 /* ((0xB2<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* LAX (zp),Y (undoc) */
        0x598 /* ((0xB3<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x599 /* ((0xB3<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x59A /* ((0xB3<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x59B /* ((0xB3<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x59C /* ((0xB3<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x59D /* ((0xB3<<3)|5) */ => { c.X=GD!(pins); c.A=c.X; c.NZ(c.A); /* c->X=_GD(); c->A=c->X; _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDY zp,X */
        0x5A0 /* ((0xB4<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5A1 /* ((0xB4<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x5A2 /* ((0xB4<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x5A3 /* ((0xB4<<3)|3) */ => { c.Y=GD!(pins); c.NZ(c.Y); /* c->Y=_GD(); _NZ(c->Y); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDA zp,X */
        0x5A8 /* ((0xB5<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5A9 /* ((0xB5<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x5AA /* ((0xB5<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x5AB /* ((0xB5<<3)|3) */ => { c.A=GD!(pins); c.NZ(c.A); /* c->A=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDX zp,Y */
        0x5B0 /* ((0xB6<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5B1 /* ((0xB6<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x5B2 /* ((0xB6<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.Y as u16))&0x00FF); /* _SA((c->AD+c->Y)&0x00FF); */ },
        0x5B3 /* ((0xB6<<3)|3) */ => { c.X=GD!(pins); c.NZ(c.X); /* c->X=_GD(); _NZ(c->X); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LAX zp,Y (undoc) */
        0x5B8 /* ((0xB7<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5B9 /* ((0xB7<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x5BA /* ((0xB7<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.Y as u16))&0x00FF); /* _SA((c->AD+c->Y)&0x00FF); */ },
        0x5BB /* ((0xB7<<3)|3) */ => { c.X=GD!(pins); c.A=c.X; c.NZ(c.A); /* c->X=_GD(); c->A=c->X; _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CLV  */
        0x5C0 /* ((0xB8<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x5C1 /* ((0xB8<<3)|1) */ => { c.P&=!0x40; /* c->P&=~0x40; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDA abs,Y */
        0x5C8 /* ((0xB9<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5C9 /* ((0xB9<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x5CA /* ((0xB9<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x5CB /* ((0xB9<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x5CC /* ((0xB9<<3)|4) */ => { c.A=GD!(pins); c.NZ(c.A); /* c->A=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* TSX  */
        0x5D0 /* ((0xBA<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x5D1 /* ((0xBA<<3)|1) */ => { c.X=c.S; c.NZ(c.X); /* c->X=c->S; _NZ(c->X); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LAS abs,Y (undoc) */
        0x5D8 /* ((0xBB<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5D9 /* ((0xBB<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x5DA /* ((0xBB<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x5DB /* ((0xBB<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x5DC /* ((0xBB<<3)|4) */ => { c.S=GD!(pins)&c.S;  c.X=c.S;  c.A=c.X;  c.NZ(c.A); /* c->S=_GD()&c->S;  c->X=c->S;  c->A=c->X;  _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDY abs,X */
        0x5E0 /* ((0xBC<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5E1 /* ((0xBC<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x5E2 /* ((0xBC<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x5E3 /* ((0xBC<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x5E4 /* ((0xBC<<3)|4) */ => { c.Y=GD!(pins); c.NZ(c.Y); /* c->Y=_GD(); _NZ(c->Y); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDA abs,X */
        0x5E8 /* ((0xBD<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5E9 /* ((0xBD<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x5EA /* ((0xBD<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x5EB /* ((0xBD<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x5EC /* ((0xBD<<3)|4) */ => { c.A=GD!(pins); c.NZ(c.A); /* c->A=_GD(); _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LDX abs,Y */
        0x5F0 /* ((0xBE<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5F1 /* ((0xBE<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x5F2 /* ((0xBE<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x5F3 /* ((0xBE<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x5F4 /* ((0xBE<<3)|4) */ => { c.X=GD!(pins); c.NZ(c.X); /* c->X=_GD(); _NZ(c->X); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* LAX abs,Y (undoc) */
        0x5F8 /* ((0xBF<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x5F9 /* ((0xBF<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x5FA /* ((0xBF<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x5FB /* ((0xBF<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x5FC /* ((0xBF<<3)|4) */ => { c.X=GD!(pins); c.A=c.X; c.NZ(c.A); /* c->X=_GD(); c->A=c->X; _NZ(c->A); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CPY # */
        0x600 /* ((0xC0<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x601 /* ((0xC0<<3)|1) */ => { c.cmp( c.Y, GD!(pins)); /* _m6502_cmp(c, c->Y, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CMP (zp,X) */
        0x608 /* ((0xC1<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x609 /* ((0xC1<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x60A /* ((0xC1<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x60B /* ((0xC1<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x60C /* ((0xC1<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x60D /* ((0xC1<<3)|5) */ => { c.cmp( c.A, GD!(pins)); /* _m6502_cmp(c, c->A, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP # (undoc) */
        0x610 /* ((0xC2<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x611 /* ((0xC2<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* DCP (zp,X) (undoc) */
        0x618 /* ((0xC3<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x619 /* ((0xC3<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x61A /* ((0xC3<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x61B /* ((0xC3<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x61C /* ((0xC3<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x61D /* ((0xC3<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x61E /* ((0xC3<<3)|6) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); c.cmp( c.A, c.AD as u8); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _m6502_cmp(c, c->A, c->AD); _WR(); */ },
        0x61F /* ((0xC3<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* CPY zp */
        0x620 /* ((0xC4<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x621 /* ((0xC4<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x622 /* ((0xC4<<3)|2) */ => { c.cmp( c.Y, GD!(pins)); /* _m6502_cmp(c, c->Y, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CMP zp */
        0x628 /* ((0xC5<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x629 /* ((0xC5<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x62A /* ((0xC5<<3)|2) */ => { c.cmp( c.A, GD!(pins)); /* _m6502_cmp(c, c->A, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* DEC zp */
        0x630 /* ((0xC6<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x631 /* ((0xC6<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x632 /* ((0xC6<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x633 /* ((0xC6<<3)|3) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _WR(); */ },
        0x634 /* ((0xC6<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* DCP zp (undoc) */
        0x638 /* ((0xC7<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x639 /* ((0xC7<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x63A /* ((0xC7<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x63B /* ((0xC7<<3)|3) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); c.cmp( c.A, c.AD as u8); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _m6502_cmp(c, c->A, c->AD); _WR(); */ },
        0x63C /* ((0xC7<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* INY  */
        0x640 /* ((0xC8<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x641 /* ((0xC8<<3)|1) */ => { c.Y = c.Y.wrapping_add(1); c.NZ(c.Y);  /* c->Y++; _NZ(c->Y); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CMP # */
        0x648 /* ((0xC9<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x649 /* ((0xC9<<3)|1) */ => { c.cmp( c.A, GD!(pins)); /* _m6502_cmp(c, c->A, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* DEX  */
        0x650 /* ((0xCA<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x651 /* ((0xCA<<3)|1) */ => { c.X = c.X.wrapping_sub(1); c.NZ(c.X);  /* c->X--; _NZ(c->X); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBX # (undoc) */
        0x658 /* ((0xCB<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x659 /* ((0xCB<<3)|1) */ => { c.sbx( GD!(pins)); /* _m6502_sbx(c, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CPY abs */
        0x660 /* ((0xCC<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x661 /* ((0xCC<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x662 /* ((0xCC<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x663 /* ((0xCC<<3)|3) */ => { c.cmp( c.Y, GD!(pins)); /* _m6502_cmp(c, c->Y, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CMP abs */
        0x668 /* ((0xCD<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x669 /* ((0xCD<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x66A /* ((0xCD<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x66B /* ((0xCD<<3)|3) */ => { c.cmp( c.A, GD!(pins)); /* _m6502_cmp(c, c->A, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* DEC abs */
        0x670 /* ((0xCE<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x671 /* ((0xCE<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x672 /* ((0xCE<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x673 /* ((0xCE<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x674 /* ((0xCE<<3)|4) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _WR(); */ },
        0x675 /* ((0xCE<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* DCP abs (undoc) */
        0x678 /* ((0xCF<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x679 /* ((0xCF<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x67A /* ((0xCF<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x67B /* ((0xCF<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x67C /* ((0xCF<<3)|4) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); c.cmp( c.A, c.AD as u8); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _m6502_cmp(c, c->A, c->AD); _WR(); */ },
        0x67D /* ((0xCF<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* BNE # */
        0x680 /* ((0xD0<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x681 /* ((0xD0<<3)|1) */ => { SA!(pins,c.PC); c.AD = addi8tou16(c.PC, GD!(pins)); if((c.P&0x2)!=0x0){FETCH!(pins,c); }; /* _SA(c->PC); c->AD=c->PC+(int8_t)_GD(); if((c->P&0x2)!=0x0){_FETCH(); }; */ },
        0x682 /* ((0xD0<<3)|2) */ => { SA!(pins,(c.PC&0xFF00)|(c.AD&0x00FF)); if((c.AD&0xFF00)==(c.PC&0xFF00)){c.PC=c.AD; c.irq_pip>>=1; c.nmi_pip>>=1; FETCH!(pins,c); }; /* _SA((c->PC&0xFF00)|(c->AD&0x00FF)); if((c->AD&0xFF00)==(c->PC&0xFF00)){c->PC=c->AD; c->irq_pip>>=1; c->nmi_pip>>=1; _FETCH(); }; */ },
        0x683 /* ((0xD0<<3)|3) */ => { c.PC=c.AD; /* c->PC=c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CMP (zp),Y */
        0x688 /* ((0xD1<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x689 /* ((0xD1<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x68A /* ((0xD1<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x68B /* ((0xD1<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x68C /* ((0xD1<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x68D /* ((0xD1<<3)|5) */ => { c.cmp( c.A, GD!(pins)); /* _m6502_cmp(c, c->A, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x690 /* ((0xD2<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x691 /* ((0xD2<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* DCP (zp),Y (undoc) */
        0x698 /* ((0xD3<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x699 /* ((0xD3<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x69A /* ((0xD3<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x69B /* ((0xD3<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x69C /* ((0xD3<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x69D /* ((0xD3<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x69E /* ((0xD3<<3)|6) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); c.cmp( c.A, c.AD as u8); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _m6502_cmp(c, c->A, c->AD); _WR(); */ },
        0x69F /* ((0xD3<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP zp,X (undoc) */
        0x6A0 /* ((0xD4<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6A1 /* ((0xD4<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x6A2 /* ((0xD4<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x6A3 /* ((0xD4<<3)|3) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CMP zp,X */
        0x6A8 /* ((0xD5<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6A9 /* ((0xD5<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x6AA /* ((0xD5<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x6AB /* ((0xD5<<3)|3) */ => { c.cmp( c.A, GD!(pins)); /* _m6502_cmp(c, c->A, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* DEC zp,X */
        0x6B0 /* ((0xD6<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6B1 /* ((0xD6<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x6B2 /* ((0xD6<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x6B3 /* ((0xD6<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x6B4 /* ((0xD6<<3)|4) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _WR(); */ },
        0x6B5 /* ((0xD6<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* DCP zp,X (undoc) */
        0x6B8 /* ((0xD7<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6B9 /* ((0xD7<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x6BA /* ((0xD7<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x6BB /* ((0xD7<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x6BC /* ((0xD7<<3)|4) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); c.cmp( c.A, c.AD as u8); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _m6502_cmp(c, c->A, c->AD); _WR(); */ },
        0x6BD /* ((0xD7<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* CLD  */
        0x6C0 /* ((0xD8<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x6C1 /* ((0xD8<<3)|1) */ => { c.P&=!0x8; /* c->P&=~0x8; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CMP abs,Y */
        0x6C8 /* ((0xD9<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6C9 /* ((0xD9<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x6CA /* ((0xD9<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x6CB /* ((0xD9<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x6CC /* ((0xD9<<3)|4) */ => { c.cmp( c.A, GD!(pins)); /* _m6502_cmp(c, c->A, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP  (undoc) */
        0x6D0 /* ((0xDA<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x6D1 /* ((0xDA<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* DCP abs,Y (undoc) */
        0x6D8 /* ((0xDB<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6D9 /* ((0xDB<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x6DA /* ((0xDB<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x6DB /* ((0xDB<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x6DC /* ((0xDB<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x6DD /* ((0xDB<<3)|5) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); c.cmp( c.A, c.AD as u8); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _m6502_cmp(c, c->A, c->AD); _WR(); */ },
        0x6DE /* ((0xDB<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP abs,X (undoc) */
        0x6E0 /* ((0xDC<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6E1 /* ((0xDC<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x6E2 /* ((0xDC<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x6E3 /* ((0xDC<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x6E4 /* ((0xDC<<3)|4) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CMP abs,X */
        0x6E8 /* ((0xDD<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6E9 /* ((0xDD<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x6EA /* ((0xDD<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x6EB /* ((0xDD<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x6EC /* ((0xDD<<3)|4) */ => { c.cmp( c.A, GD!(pins)); /* _m6502_cmp(c, c->A, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* DEC abs,X */
        0x6F0 /* ((0xDE<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6F1 /* ((0xDE<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x6F2 /* ((0xDE<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x6F3 /* ((0xDE<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x6F4 /* ((0xDE<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x6F5 /* ((0xDE<<3)|5) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _WR(); */ },
        0x6F6 /* ((0xDE<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* DCP abs,X (undoc) */
        0x6F8 /* ((0xDF<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x6F9 /* ((0xDF<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x6FA /* ((0xDF<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x6FB /* ((0xDF<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x6FC /* ((0xDF<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x6FD /* ((0xDF<<3)|5) */ => { c.AD = c.AD.wrapping_sub(1); c.NZ(c.AD as u8); SD!(pins, c.AD); c.cmp( c.A, c.AD as u8); WR!(pins);  /* c->AD--; _NZ(c->AD); _SD(c->AD); _m6502_cmp(c, c->A, c->AD); _WR(); */ },
        0x6FE /* ((0xDF<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* CPX # */
        0x700 /* ((0xE0<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x701 /* ((0xE0<<3)|1) */ => { c.cmp( c.X, GD!(pins)); /* _m6502_cmp(c, c->X, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBC (zp,X) */
        0x708 /* ((0xE1<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x709 /* ((0xE1<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x70A /* ((0xE1<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x70B /* ((0xE1<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x70C /* ((0xE1<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x70D /* ((0xE1<<3)|5) */ => { c.sbc(GD!(pins)); /* _m6502_sbc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP # (undoc) */
        0x710 /* ((0xE2<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x711 /* ((0xE2<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ISB (zp,X) (undoc) */
        0x718 /* ((0xE3<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x719 /* ((0xE3<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x71A /* ((0xE3<<3)|2) */ => { c.AD=(c.AD.wrapping_add(c.X as u16))&0xFF; SA!(pins,c.AD); /* c->AD=(c->AD+c->X)&0xFF; _SA(c->AD); */ },
        0x71B /* ((0xE3<<3)|3) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x71C /* ((0xE3<<3)|4) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x71D /* ((0xE3<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x71E /* ((0xE3<<3)|6) */ => { c.AD = c.AD.wrapping_add(1); SD!(pins, c.AD); c.sbc(c.AD as u8); WR!(pins);  /* c->AD++; _SD(c->AD); _m6502_sbc(c,c->AD); _WR(); */ },
        0x71F /* ((0xE3<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* CPX zp */
        0x720 /* ((0xE4<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x721 /* ((0xE4<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x722 /* ((0xE4<<3)|2) */ => { c.cmp( c.X, GD!(pins)); /* _m6502_cmp(c, c->X, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBC zp */
        0x728 /* ((0xE5<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x729 /* ((0xE5<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x72A /* ((0xE5<<3)|2) */ => { c.sbc(GD!(pins)); /* _m6502_sbc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* INC zp */
        0x730 /* ((0xE6<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x731 /* ((0xE6<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x732 /* ((0xE6<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x733 /* ((0xE6<<3)|3) */ => { c.AD = c.AD.wrapping_add(1); c.NZ(c.AD as u8); SD!(pins, c.AD); WR!(pins);  /* c->AD++; _NZ(c->AD); _SD(c->AD); _WR(); */ },
        0x734 /* ((0xE6<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* ISB zp (undoc) */
        0x738 /* ((0xE7<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x739 /* ((0xE7<<3)|1) */ => { SA!(pins,GD!(pins)); /* _SA(_GD()); */ },
        0x73A /* ((0xE7<<3)|2) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x73B /* ((0xE7<<3)|3) */ => { c.AD = c.AD.wrapping_add(1); SD!(pins, c.AD); c.sbc(c.AD as u8); WR!(pins);  /* c->AD++; _SD(c->AD); _m6502_sbc(c,c->AD); _WR(); */ },
        0x73C /* ((0xE7<<3)|4) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* INX  */
        0x740 /* ((0xE8<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x741 /* ((0xE8<<3)|1) */ => { c.X = c.X.wrapping_add(1); c.NZ(c.X);  /* c->X++; _NZ(c->X); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBC # */
        0x748 /* ((0xE9<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x749 /* ((0xE9<<3)|1) */ => { c.sbc(GD!(pins)); /* _m6502_sbc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP  */
        0x750 /* ((0xEA<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x751 /* ((0xEA<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBC # (undoc) */
        0x758 /* ((0xEB<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x759 /* ((0xEB<<3)|1) */ => { c.sbc(GD!(pins)); /* _m6502_sbc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* CPX abs */
        0x760 /* ((0xEC<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x761 /* ((0xEC<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x762 /* ((0xEC<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x763 /* ((0xEC<<3)|3) */ => { c.cmp( c.X, GD!(pins)); /* _m6502_cmp(c, c->X, _GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBC abs */
        0x768 /* ((0xED<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x769 /* ((0xED<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x76A /* ((0xED<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x76B /* ((0xED<<3)|3) */ => { c.sbc(GD!(pins)); /* _m6502_sbc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* INC abs */
        0x770 /* ((0xEE<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x771 /* ((0xEE<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x772 /* ((0xEE<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x773 /* ((0xEE<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x774 /* ((0xEE<<3)|4) */ => { c.AD = c.AD.wrapping_add(1); c.NZ(c.AD as u8); SD!(pins, c.AD); WR!(pins);  /* c->AD++; _NZ(c->AD); _SD(c->AD); _WR(); */ },
        0x775 /* ((0xEE<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* ISB abs (undoc) */
        0x778 /* ((0xEF<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x779 /* ((0xEF<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x77A /* ((0xEF<<3)|2) */ => { SA!(pins,((GD!(pins) as u16)<<8)|c.AD); /* _SA((_GD()<<8)|c->AD); */ },
        0x77B /* ((0xEF<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x77C /* ((0xEF<<3)|4) */ => { c.AD = c.AD.wrapping_add(1); SD!(pins, c.AD); c.sbc(c.AD as u8); WR!(pins);  /* c->AD++; _SD(c->AD); _m6502_sbc(c,c->AD); _WR(); */ },
        0x77D /* ((0xEF<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* BEQ # */
        0x780 /* ((0xF0<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x781 /* ((0xF0<<3)|1) */ => { SA!(pins,c.PC); c.AD = addi8tou16(c.PC, GD!(pins)); if((c.P&0x2)!=0x2){FETCH!(pins,c); }; /* _SA(c->PC); c->AD=c->PC+(int8_t)_GD(); if((c->P&0x2)!=0x2){_FETCH(); }; */ },
        0x782 /* ((0xF0<<3)|2) */ => { SA!(pins,(c.PC&0xFF00)|(c.AD&0x00FF)); if((c.AD&0xFF00)==(c.PC&0xFF00)){c.PC=c.AD; c.irq_pip>>=1; c.nmi_pip>>=1; FETCH!(pins,c); }; /* _SA((c->PC&0xFF00)|(c->AD&0x00FF)); if((c->AD&0xFF00)==(c->PC&0xFF00)){c->PC=c->AD; c->irq_pip>>=1; c->nmi_pip>>=1; _FETCH(); }; */ },
        0x783 /* ((0xF0<<3)|3) */ => { c.PC=c.AD; /* c->PC=c->AD; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBC (zp),Y */
        0x788 /* ((0xF1<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x789 /* ((0xF1<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x78A /* ((0xF1<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x78B /* ((0xF1<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x78C /* ((0xF1<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x78D /* ((0xF1<<3)|5) */ => { c.sbc(GD!(pins)); /* _m6502_sbc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* JAM INVALID (undoc) */
        0x790 /* ((0xF2<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x791 /* ((0xF2<<3)|1) */ => { SAD!(pins,0xFFFF,0xFF); c.IR = c.IR.wrapping_sub(1);  /* _SAD(0xFFFF,0xFF); c->IR--; */ },
    /* ISB (zp),Y (undoc) */
        0x798 /* ((0xF3<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x799 /* ((0xF3<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x79A /* ((0xF3<<3)|2) */ => { SA!(pins,(c.AD+1)&0xFF); c.AD=GD!(pins) as u16; /* _SA((c->AD+1)&0xFF); c->AD=_GD(); */ },
        0x79B /* ((0xF3<<3)|3) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x79C /* ((0xF3<<3)|4) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x79D /* ((0xF3<<3)|5) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x79E /* ((0xF3<<3)|6) */ => { c.AD = c.AD.wrapping_add(1); SD!(pins, c.AD); c.sbc(c.AD as u8); WR!(pins);  /* c->AD++; _SD(c->AD); _m6502_sbc(c,c->AD); _WR(); */ },
        0x79F /* ((0xF3<<3)|7) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP zp,X (undoc) */
        0x7A0 /* ((0xF4<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7A1 /* ((0xF4<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x7A2 /* ((0xF4<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x7A3 /* ((0xF4<<3)|3) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBC zp,X */
        0x7A8 /* ((0xF5<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7A9 /* ((0xF5<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x7AA /* ((0xF5<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x7AB /* ((0xF5<<3)|3) */ => { c.sbc(GD!(pins)); /* _m6502_sbc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* INC zp,X */
        0x7B0 /* ((0xF6<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7B1 /* ((0xF6<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x7B2 /* ((0xF6<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x7B3 /* ((0xF6<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x7B4 /* ((0xF6<<3)|4) */ => { c.AD = c.AD.wrapping_add(1); c.NZ(c.AD as u8); SD!(pins, c.AD); WR!(pins);  /* c->AD++; _NZ(c->AD); _SD(c->AD); _WR(); */ },
        0x7B5 /* ((0xF6<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* ISB zp,X (undoc) */
        0x7B8 /* ((0xF7<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7B9 /* ((0xF7<<3)|1) */ => { c.AD=GD!(pins) as u16; SA!(pins,c.AD); /* c->AD=_GD(); _SA(c->AD); */ },
        0x7BA /* ((0xF7<<3)|2) */ => { SA!(pins,(c.AD.wrapping_add(c.X as u16))&0x00FF); /* _SA((c->AD+c->X)&0x00FF); */ },
        0x7BB /* ((0xF7<<3)|3) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x7BC /* ((0xF7<<3)|4) */ => { c.AD = c.AD.wrapping_add(1); SD!(pins, c.AD); c.sbc(c.AD as u8); WR!(pins);  /* c->AD++; _SD(c->AD); _m6502_sbc(c,c->AD); _WR(); */ },
        0x7BD /* ((0xF7<<3)|5) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* SED  */
        0x7C0 /* ((0xF8<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x7C1 /* ((0xF8<<3)|1) */ => { c.P|=0x8; /* c->P|=0x8; */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBC abs,Y */
        0x7C8 /* ((0xF9<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7C9 /* ((0xF9<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x7CA /* ((0xF9<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.Y as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->Y)>>8)))&1; */ },
        0x7CB /* ((0xF9<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x7CC /* ((0xF9<<3)|4) */ => { c.sbc(GD!(pins)); /* _m6502_sbc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP  (undoc) */
        0x7D0 /* ((0xFA<<3)|0) */ => { SA!(pins,c.PC); /* _SA(c->PC); */ },
        0x7D1 /* ((0xFA<<3)|1) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* ISB abs,Y (undoc) */
        0x7D8 /* ((0xFB<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7D9 /* ((0xFB<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x7DA /* ((0xFB<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.Y as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->Y)&0xFF)); */ },
        0x7DB /* ((0xFB<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.Y as u16)); /* _SA(c->AD+c->Y); */ },
        0x7DC /* ((0xFB<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x7DD /* ((0xFB<<3)|5) */ => { c.AD = c.AD.wrapping_add(1); SD!(pins, c.AD); c.sbc(c.AD as u8); WR!(pins);  /* c->AD++; _SD(c->AD); _m6502_sbc(c,c->AD); _WR(); */ },
        0x7DE /* ((0xFB<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* NOP abs,X (undoc) */
        0x7E0 /* ((0xFC<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7E1 /* ((0xFC<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x7E2 /* ((0xFC<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x7E3 /* ((0xFC<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x7E4 /* ((0xFC<<3)|4) */ => {  /*  */ FETCH!(pins,c); /* _FETCH(); */ },
    /* SBC abs,X */
        0x7E8 /* ((0xFD<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7E9 /* ((0xFD<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x7EA /* ((0xFD<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ c.IR+=(!((c.AD>>8).wrapping_sub((c.AD.wrapping_add(c.X as u16))>>8)))&1; /* c->IR+=(~((c->AD>>8)-((c->AD+c->X)>>8)))&1; */ },
        0x7EB /* ((0xFD<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x7EC /* ((0xFD<<3)|4) */ => { c.sbc(GD!(pins)); /* _m6502_sbc(c,_GD()); */ FETCH!(pins,c); /* _FETCH(); */ },
    /* INC abs,X */
        0x7F0 /* ((0xFE<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7F1 /* ((0xFE<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x7F2 /* ((0xFE<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x7F3 /* ((0xFE<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x7F4 /* ((0xFE<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x7F5 /* ((0xFE<<3)|5) */ => { c.AD = c.AD.wrapping_add(1); c.NZ(c.AD as u8); SD!(pins, c.AD); WR!(pins);  /* c->AD++; _NZ(c->AD); _SD(c->AD); _WR(); */ },
        0x7F6 /* ((0xFE<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },
    /* ISB abs,X (undoc) */
        0x7F8 /* ((0xFF<<3)|0) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1);  /* _SA(c->PC++); */ },
        0x7F9 /* ((0xFF<<3)|1) */ => { SA!(pins,c.PC); c.PC = c.PC.wrapping_add(1); c.AD=GD!(pins) as u16;  /* _SA(c->PC++); c->AD=_GD(); */ },
        0x7FA /* ((0xFF<<3)|2) */ => { c.AD |= (GD!(pins) as u16) << 8; SA!(pins,(c.AD&0xFF00)|((c.AD.wrapping_add(c.X as u16))&0xFF)); /* c->AD|=_GD()<<8; _SA((c->AD&0xFF00)|((c->AD+c->X)&0xFF)); */ },
        0x7FB /* ((0xFF<<3)|3) */ => { SA!(pins,c.AD.wrapping_add(c.X as u16)); /* _SA(c->AD+c->X); */ },
        0x7FC /* ((0xFF<<3)|4) */ => { c.AD=GD!(pins) as u16; WR!(pins); /* c->AD=_GD(); _WR(); */ },
        0x7FD /* ((0xFF<<3)|5) */ => { c.AD = c.AD.wrapping_add(1); SD!(pins, c.AD); c.sbc(c.AD as u8); WR!(pins);  /* c->AD++; _SD(c->AD); _m6502_sbc(c,c->AD); _WR(); */ },
        0x7FE /* ((0xFF<<3)|6) */ => { FETCH!(pins,c); /* _FETCH(); */ },

	    _ => panic!("Unrecognized IR {:04X} at PC:{:04X}", c.IR, c.PC)

	}

	c.IR = c.IR + 1;
	c.PINS = pins;
	c.irq_pip <<= 1;

	c.cycles += 1;
	return pins
    }

    pub fn start_cpu_at( &mut self, addr: u16, pins: u64, opcode: u8) -> u64{

	let mut pins_after: u64 = pins;

	self.PC = addr;
	// fetch :
	//  SA!($pins, $c.PC); // set address on pins
	//  ON!($pins, M6502_SYNC); // ask for sync
	SD!(pins_after, opcode);
	FETCH!(pins_after, self);

	// tick:
	// if (pins & M6502_SYNC != 0) {
        //     c.IR = (GD!(pins) as u16) <<3;

	pins_after
    }

    // return Cycle6502 {
    // 	IR: 0,        /* internal instruction register */
    // 	PC: addr.wrapping_sub(1),        /* internal program counter register */
    // 	AD: addr.wrapping_sub(1) & 0x00FF,        /* ADL/ADH internal register */
    // 	A: 0,  /* regular registers */
    // 	X: 0,  /* regular registers */
    // 	Y: 0,  /* regular registers */
    // 	S: 0,  /* regular registers */
    // 	P: M6502_ZF,  /* regular registers */
    // 	PINS: M6502_RW | M6502_SYNC | M6502_RES,      /* last stored pin state (do NOT modify) */
    // 	irq_pip: 0,
    // 	nmi_pip: 0,
    // 	brk_flags: 0,  /* M6502_BRK_* */
    // 	bcd_enabled: 1,
    // 	cycles: 0,
    // 	last_instr_pc: 0
    // };
}


pub fn reset_cpu() -> Cycle6502 {
    return Cycle6502 {
	IR: 0,        /* internal instruction register */
	PC: 0,        /* internal program counter register */
	AD: 0,        /* ADL/ADH internal register */
	A: 0,  /* regular registers */
	X: 0,  /* regular registers */
	Y: 0,  /* regular registers */
	S: 0,  /* regular registers */
	P: M6502_ZF,  /* regular registers */
	PINS: M6502_RW | M6502_SYNC | M6502_RES,      /* last stored pin state (do NOT modify) */
	irq_pip: 0,
	nmi_pip: 0,
	brk_flags: M6502_BRK_RESET, // RES was triggered  /* M6502_BRK_* */
	bcd_enabled: 1,
	cycles: 0,
	last_instr_pc: 0
    };
}


fn test(mut ram: Vec<u8>) {

    // let mut a = m6502_desc_t {
    // 	bcd_disabled: true,              /* set to true if BCD mode is disabled */
    // 	m6510_user_data: 0,          /* optional callback user data */
    // 	m6510_io_pullup: 0,        /* IO port bits that are 1 when reading */
    // 	m6510_io_floating: 0      /* unconnected IO port pins */
    // };

    // let mut cpu = Cycle6502 {
    // 	IR: 0,        /* internal instruction register */
    // 	PC: 0x3FF,        /* internal program counter register */
    // 	AD: 0xFF,        /* ADL/ADH internal register */
    // 	A: 0,  /* regular registers */
    // 	X: 0,  /* regular registers */
    // 	Y: 0,  /* regular registers */
    // 	S: 0,  /* regular registers */
    // 	P: M6502_ZF,  /* regular registers */
    // 	PINS: M6502_RW | M6502_SYNC | M6502_RES,      /* last stored pin state (do NOT modify) */
    // 	irq_pip: 0,
    // 	nmi_pip: 0,
    // 	brk_flags: 0,  /* M6502_BRK_* */
    // 	bcd_enabled: 1,
    // 	cycles: 0,
    // 	last_instr_pc: 0
    // };

    // let mut cpu = start_cpu_at( 0x400);
    // M6502_SET_DATA!(pins, ram[0x400]);
    // // Set pins :
    // ON!(pins, M6502_SYNC);

    let mut cpu = reset_cpu();
    ram[0xFFFF] = 0x04;
    ram[0xFFFE] = 0x00;
    ram[0xFFFD] = 0x04;
    ram[0xFFFC] = 0x00;

    let mut pins: u64 = cpu.PINS;

    let mut prev_instr_pc: u16 = 0;
    let mut disassembler = crate::rs6502::disassembler::Disassembler::new();

    const limit: i64 = 30; // 100_664_000;

    for i in 0..limit {

	pins = cpu.tick(pins);

	if cpu.cycles & 0x7FFFFF == 0 {
	    println!("{}",cpu.cycles)
	}

	// if i > limit - 100_000_000  {
	//     println!("jjj {} {} {}", i, limit - 100_000, i > limit - 100_000);
	// }

	if (true ||  prev_instr_pc != cpu.last_instr_pc) {
	    if true || i > limit - 5_000_000  {
		//println!("jjj {} {} {}", i, limit - 100_000, i > limit - 100_000);

		//if pins & M6502_SYNC {
		//let offset = prev_instr_pc as usize;
		//disassembler.code_offset = offset as u16;
		// let m:[u8; 4] = [ram[offset], ram[offset+1], ram[offset+2], ram[offset+3]];
		// let asm = disassembler.disassemble(&m);
		// println!("{: <31} SP=01{:02X} A={:02X}, X={:02X}, Y={:02X} flags:{}{}{}{}{}{}{}{}|{:08b}={:02X} IR: ${:04X}/{:}, PC: ${:04x} AD: ${:04x} | {} ",
		// 	 asm, cpu.S, cpu.A, cpu.X, cpu.Y,
		// 	 if cpu.P & M6502_NF != 0 {"N"} else {"-"},
		// 	 if cpu.P & M6502_VF != 0 {"V"} else {"-"},
		// 	 if cpu.P & M6502_XF != 0 {"X"} else {"-"},
		// 	 if cpu.P & M6502_BF != 0 {"B"} else {"-"},
		// 	 if cpu.P & M6502_DF != 0 {"D"} else {"-"},
		// 	 if cpu.P & M6502_IF != 0 {"I"} else {"-"},
		// 	 if cpu.P & M6502_ZF != 0 {"Z"} else {"-"},
		// 	 if cpu.P & M6502_CF != 0 {"C"} else {"-"},
		// 	 cpu.P, cpu.P,
		// 	 cpu.IR>>3, cpu.IR & 7, cpu.PC, cpu.AD, cpu.cycles);

	    }
	    prev_instr_pc = cpu.last_instr_pc
	}


	let addr: u16 = M6502_GET_ADDR!(pins);

	let read: bool = pins & M6502_RW != 0;
	//println!("pins addr = {:04x} {}",addr, if read {"RD"} else {"WT"});

	if (read) {
	    // Mem read
	    M6502_SET_DATA!(pins, ram[addr as usize]);
	    // println!("   Read : {:02X}",ram[addr as usize]);
	} else {
	    //println!("   Write: {:02X}",M6502_GET_DATA!(pins));
	    // a memory write
	    ram[addr as usize] = M6502_GET_DATA!(pins);
        }
    }
    //cpu.regs_to_str();
    // Should end at $346B
    println!("{} Cycles ran",cpu.cycles);
}



//use rs6502::assembler::Assembler;
use std::time::Duration;
use std::thread::sleep;
use std::time::Instant;

fn main() {
     let asm = "
        .ORG $800
        LDA #$DE
        LDA #$AD
        CLC
        ADC #$1
        SBC #$1
        ROL
        ROR
    ";

    let mut assembler = crate::rs6502::assembler::Assembler::new();
    let segments = assembler.assemble_string(asm, None).unwrap();

    let mut ram: Vec<u8> = vec![0; 65536];
    let seg = &segments[0];
    let addr: usize = seg.address as usize;

    ram[addr .. addr + seg.code.len()].copy_from_slice(&seg.code);
    ram[0xFFFF] = 0x08;
    ram[0xFFFE] = 0x00;

    /*

    Need to update clap, will do that later.

    let cli = App::new("Apple 2 Emulator")
        .version("alpha")
        .arg(Arg::with_name("ram")
             .short("ram")
             .long("ram")
             .value_name("FILE")
             .help("RAM file")
             .takes_value(true)
             .required(true))
             .get_matches();

    let mut file = BufReader::new(File::open(cli.value_of("ram").unwrap()).unwrap());
    file.read(&mut ram[0..65536]);
    // ram[0xFFFF] = 0x04;
    // ram[0xFFFE] = 0x00;

    let start_time = Instant::now();
    test(ram);
    println!("Elapsed: {}", start_time.elapsed().as_millis());
*/

}
//}
