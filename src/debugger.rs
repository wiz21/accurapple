use std::fmt::{Debug};
use std::io::{Stdout, self};
use std::time::{Duration, Instant};
use std::sync::mpsc::{self, Sender, Receiver};
use std::fs;
use std::path::{Path, PathBuf};
use std::collections::{HashMap, HashSet};

use crossterm::{
    event::{self, Event as CEvent, KeyCode, KeyEvent},
    terminal::{disable_raw_mode, enable_raw_mode},
};
use itertools::Itertools;
use tui::backend::CrosstermBackend;
use tui::layout::{Constraint, Direction, Layout};
use tui::text::{Span, Spans};
use tui::style::{Style, Color};
use tui::widgets::{Block, Borders, Paragraph, Widget};
use tui::Terminal;
use circular_queue::CircularQueue;
use regex::Regex;
//use rs6502::Disassembler;
use crate::rs6502::disassembler::{Disassembler};
use crate::a2::{ModeInfo, ON, OFF, CYCLES_PER_FRAME, MemoryBankType, FRAMES_PER_SECOND, GfxBaseMode};
use crate::generic_peripheral::{IRQRequest};
use crate::emulator::{Command,CpuDebugState};
use crate::m6502::Cycle6502;
use crate::gfx::cycle_to_crt_line;
use crate::mem::Mem;
use crate::mos6522::MockingBoard;

use lazy_static::lazy_static;

use log::{Record, Level, Metadata, error, info};
use std::sync::{Arc,Mutex};

#[derive(Clone)]
pub struct MyLogRecord {
    pub module: String,
    pub message: String,
    pub level: String
}

impl MyLogRecord {
    fn new(r: &Record) -> MyLogRecord {
	MyLogRecord {
	    module: r.module_path().unwrap().to_string(),
	    message: format!("{}", r.args()),
	    level : format!("{}",r.level())
	}
    }
}

pub struct SimpleLogger {
    //pub records : Arc<Mutex<CircularQueue<String>>>,
    pub records : Arc<Mutex<CircularQueue<MyLogRecord>>>
}

impl log::Log for SimpleLogger {

    // Don't forget that the maximum log level is set during log construction
    // with log::set_max_level(LevelFilter::...)

    fn enabled(&self, metadata: &Metadata) -> bool {
        true // metadata.level() <= Level::Trace
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
	    let mut rec = self.records.lock().expect("");
	    // Level::Warn <= Level::Info <= Level::Debug
	    if record.module_path().is_some() && record.level() <= Level::Debug {
		let module_path = record.module_path().unwrap();
		if module_path.starts_with("accurapple::") {
		    rec.push(MyLogRecord::new(record));
		}
	    }
	    //println!("{}", rec.last().unwrap());
        }
    }

    fn flush(&self) {}
}

// FIXME This used for active ram detection, so we
// must move that in the a2.rs module.

pub const BANKS_OF_INTEREST: [u16; 16]=[
	0x0000,0x200,0x400,0x800,0x2000,0x4000,
	0xC000,0xC100,0xC200,0xC300,0xC600,0xC700,0xC800,
	0xD000,0xE000,0xF000];

pub struct DebugFrame {
    pub trace: Vec<CpuDebugState>,
    pub slots_status: Vec<String>,
    pub cycles: u64,
    pub mem_copy: Option<Vec<u8>>,
    pub cpu: Cycle6502,
    pub planned_irqs: Vec<IRQRequest>,
    pub irq_vector:u16,
    pub last_irq_delay: usize,
    pub gfx_mode_info: ModeInfo,
	pub gfx_base_mode: GfxBaseMode,
    pub last_vblank_read: u64,
    pub vbl_bit: usize,
    pub active_memory_bank: MemoryBankType,
    pub floating_addr: Option<usize>,
	pub memory_banks: [MemoryBankType; 16],
	pub memory_banks_for_write: [MemoryBankType; 16],
	pub hramrd: bool
}

impl DebugFrame {
    pub fn new(cpu: &Cycle6502) -> DebugFrame {
	DebugFrame {
	    trace: Vec::new(),
	    slots_status: Vec::new(),
	    cycles: 0,
	    mem_copy: None,
	    cpu: cpu.clone(),
	    planned_irqs: Vec::new(),
	    irq_vector: 0,
	    last_irq_delay: 0,
	    gfx_mode_info: ModeInfo {
		col80: OFF,
		store80: OFF,
		page2: OFF,
		text: OFF,
		mixed: OFF,
		hires: OFF,
		dhires: OFF },
		gfx_base_mode: GfxBaseMode::Text40,
	    last_vblank_read: 0,
	    vbl_bit: 0,
	    active_memory_bank: MemoryBankType::Motherboard,
	    floating_addr: None,
		memory_banks: [MemoryBankType::Motherboard; 16],
		memory_banks_for_write: [MemoryBankType::Motherboard; 16],
		hramrd: false
	}
    }
}




// (Terminal<CrosstermBackend<Stdout>>,
pub fn init_debugger(control_channel: Sender<Command>,
		     debug_stream_rx: Receiver<DebugFrame>,
		     log_lines:Arc<Mutex<CircularQueue<MyLogRecord>>>,
		     symbols_file:Option<PathBuf>) -> std::thread::JoinHandle<()> {
    // control_channel: the control channel of the emulator
    // debuger_Stream_rx: where we'll get information from the emulator.

    let stdout = io::stdout();

    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend).unwrap();
    terminal
        .clear()
        .expect("Terminal initialisation");
    enable_raw_mode().expect("can run in raw mode");


    let mut last_shown_PC: u16 = 0;
    let mut check_visible_line_disasm = false;
    let mut mem_dump_offset: u16 = 0xD000;
    let mut disassembly_offset: u16 = 0xD000;
    let mut disassembly_offset_max: u16 = disassembly_offset;
    let mut disassembly_page_up_offset: u16 = 0;
    let mut disassembly_page_down_offset: u16 = 0;

    let mut debugger_active = false;
    let mut main_page = 1;

    let mut labels:HashMap<u16,String> = HashMap::new();

    if let Some(symfile) = symbols_file {
		let mut h = HashSet::new();
		load_symbols(&symfile, &mut labels, &mut h);
    }

    labels.insert(0x03FE, "IRQ_VECH".to_string());
    labels.insert(0x03FF, "IRQ_VECL".to_string());

    labels.insert(0xC050, "TEXT_OFF".to_string());
    labels.insert(0xC051, "TEXT_ON".to_string());
    labels.insert(0xC052, "MIXED_OFF".to_string());
    labels.insert(0xC053, "MIXED_ON".to_string());
    labels.insert(0xC054, "PAGE2_OFF".to_string());
    labels.insert(0xC055, "PAGE2_ON".to_string());
    labels.insert(0xC056, "HIRES_OFF".to_string());
    labels.insert(0xC057, "HIRES_ON".to_string());

    labels.insert(0xC006, "SLOTCXROM_OFF".to_string());
    labels.insert(0xC007, "SLOTCXROM_ON".to_string());
    labels.insert(0xC011, "KBD_STROBE".to_string());
    labels.insert(0xC012, "KBD_STROBE".to_string());
    labels.insert(0xC013, "RAMRD_READ".to_string());
    labels.insert(0xC014, "RAMWRT_READ".to_string());
    labels.insert(0xC015, "RDCXROM_READ".to_string());
    labels.insert(0xC016, "ALTZP_READ".to_string());
    labels.insert(0xC018, "STORE80_READ".to_string());
    labels.insert(0xC01C, "PAGE2_READ".to_string());
	labels.insert(0xC01E, "ALTCHARSET_RD".to_string());
	labels.insert(0xC01F, "COL80_RD".to_string());
	labels.insert(0xC08C, "BANK1_ON,RAMRD_OFF".to_string());





    labels.insert(0xFB36, "A2Rom: text mode".to_string());
    labels.insert(0xFB40, "A2Rom: GR mode".to_string());

    labels.insert(0xC019, "VBLANK".to_string());
    labels.insert(0xC400, "6522_ORB".to_string());
    labels.insert(0xC401, "6522_ORA".to_string());
    labels.insert(0xC402, "6522_DDRB".to_string());
    labels.insert(0xC403, "6522_DDRA".to_string());
    labels.insert(0xC404, "6522_T1CL".to_string());
    labels.insert(0xC405, "6522_T1CH".to_string());
    labels.insert(0xC40B, "6522_ACR".to_string());
    labels.insert(0xC40D, "6522_IFR".to_string());
    labels.insert(0xC40E, "6522_IER".to_string());

    labels.insert(0xC480, "6522'_ORB".to_string());
    labels.insert(0xC481, "6522'_ORA".to_string());
    labels.insert(0xC482, "6522'_DDRB".to_string());
    labels.insert(0xC483, "6522'_DDRA".to_string());
    labels.insert(0xC484, "6522'_T1CL".to_string());
    labels.insert(0xC485, "6522'_T1CH".to_string());
    labels.insert(0xC48B, "6522'_ACR".to_string());
    labels.insert(0xC48D, "6522'_IFR".to_string());
    labels.insert(0xC48E, "6522'_IER".to_string());


    labels.insert(0xC0E0, "#6 MAG 0 OFF".to_string());
    labels.insert(0xC0E2, "#6 MAG 1 OFF".to_string());
    labels.insert(0xC0E4, "#6 MAG 2 OFF".to_string());
    labels.insert(0xC0E6, "#6 MAG 3 OFF".to_string());

    labels.insert(0xC0E1, "#6 MAG 0 ON".to_string());
    labels.insert(0xC0E3, "#6 MAG 1 ON".to_string());
    labels.insert(0xC0E5, "#6 MAG 2 ON".to_string());
    labels.insert(0xC0E7, "#6 MAG 3 ON".to_string());

    labels.insert(0xC0EA, "#6 Select Drive 0".to_string());
    labels.insert(0xC0EB, "#6 Select Drive 1".to_string());

    labels.insert(0xC0E8, "#6 MOTOR OFF".to_string());
    labels.insert(0xC0E9, "#6 MOTOR ON".to_string());
    labels.insert(0xC0EC, "#6 RD_LSS".to_string());
    labels.insert(0xC0ED, "#6 RD_WRITE_PRO+reset LSS".to_string());
    labels.insert(0xC0EE, "#6 Select Read mode".to_string());

    labels.insert(0x3E, "Slot base".to_string());
    labels.insert(0xF925, "All magnets off".to_string());
    labels.insert(0xFB85, "Delay".to_string());
    labels.insert(0xF98A, "Magnet on/off".to_string());
	labels.insert(0xFB98, "RD sect address".to_string());
	labels.insert(0xFBFD, "RD sector data".to_string());


	labels.insert(0xFB72, "Dest HalfTrk".to_string());
	labels.insert(0xFB5A, "Current HalfTrk".to_string());
	labels.insert(0xFB6B, "Steps done".to_string());
	labels.insert(0xFB71, "Hold current halftrk".to_string());
	labels.insert(0xF983, "At destination".to_string());
	labels.insert(0xF955, "Move (halftrk) down".to_string());
	labels.insert(0xF967, "Turn on".to_string());
	labels.insert(0xFB73, "Mag on times".to_string());
	labels.insert(0xFB73, "Mag off times".to_string());


	labels.insert(0xFB6D, "SectAddr-ChkSum".to_string());
	labels.insert(0xFB6D+1, "SectAddr-Sector".to_string());
	labels.insert(0xFB6D+2, "SectAddr-Track".to_string());
	labels.insert(0xFB6D+3, "SectAddr-Vol".to_string());

	labels.insert(0xFB57, "Dest sector".to_string());

    /* Deater */

    labels.insert(0x6000, "Demo main start".to_string());
    labels.insert(0x6B8B, "mockingboard_init".to_string());
    labels.insert(0x6BE9, "mockingboard_setup_interrupt".to_string());
    labels.insert(0x6B9A, "reset_ay_both".to_string());

    labels.insert(0x6BDC, "clear_ay_both".to_string());
    labels.insert(0x6953, "pt3_init_song".to_string());

    /*
    // Accolade's Test Drive
    labels.insert(0x7F00, "Protection routine?".to_string());
    labels.insert(0x81ED, "Read track? some sector?".to_string());
    labels.insert(0x8156, "Read sector data".to_string());
    labels.insert(0x80ED, "Read sector header".to_string());
    labels.insert(0x8019, "Pause (A=duration * +/-101cy.)".to_string());
    labels.insert(0x7FF9, "Magnet move & pause (dir. A)".to_string());
    labels.insert(0x7F96, "Stop motor AND READ latch !".to_string());
    labels.insert(0x80B5, "Magnet set (no pause)".to_string());
    labels.insert(0x8030, "Move to track".to_string());
    labels.insert(0x80D5, "Move to track".to_string());
    labels.insert(0x807C, "Set magnet ".to_string());
    labels.insert(0x807F, "Set magnet_intra ".to_string());

    labels.insert(0x80F8, "/// Read sector mark : #$D5".to_string());
    labels.insert(0x8104, "/// Read sector mark : #$AA".to_string());
    labels.insert(0x8110, "/// Read sector mark : #$96".to_string());
    labels.insert(0x811C, "/// Read vol, trk, sect, chksum".to_string());
    labels.insert(0x8137, "/// Move (vol^trk^sect) ^ checksumCheck == 0 in Y and test Z flag".to_string());
    labels.insert(0x813A, "/// Check #$DE (end block)".to_string());
    labels.insert(0x8146, "/// Check #$AA (end block)".to_string());


    labels.insert(0x82D9, "M: Subtrack".to_string());
    for i in 0..16 {
	labels.insert((0x82EC+i) as u16, format!("M: sector {}",i));
	labels.insert((0x82EC+i) as u16, format!("M: sector2 {}",i));
    }
    labels.insert(0x808D, "Read several tracks?".to_string());
    labels.insert(0x7F9C, "Read sector header+data".to_string());
    labels.insert(0x7F72, "Strange loop".to_string());

    labels.insert(0x82D5, "M: I/O select".to_string());
    labels.insert(0x002E, "M: Track # from sector".to_string());
    labels.insert(0x002D, "M: Sector # from sector".to_string());
    labels.insert(0x0047, "M: #$D8 - last pause duration".to_string());
    labels.insert(0x82EA, "M: magnet index".to_string());
    labels.insert(0x82E2, "M: magnet + move tbl".to_string());
    labels.insert(0x82DA, "M: magnet - move tbl".to_string());
    */

    let mut _thread_join_handle: std::thread::JoinHandle<()>  =
	std::thread::spawn(move || {
            // let tick_rate = Duration::from_millis(200);
            // let mut last_tick = Instant::now();
	    // Use of lazy_static! as documented here :
	    // https://docs.rs/regex/latest/regex/
	    lazy_static! {
		static ref DEBUG_COMMAND_RE:Regex = Regex::new(r"^([[:alpha:]]+)( ([[:alnum:]]+))?$").unwrap();
	    }
	    let mut input_str = String::from("");
	    let mut last_valid_input_str = String::from("");

	    let mut dframe = None;
	    let mut cycle_base: u64 = 0;

            loop {

		// So we try to pick up the last of the received
		// debug frame, just in case there's some buffering
		// 'cos some thread is late...
		loop {
		    let f2 = debug_stream_rx.try_recv();
		    if f2.is_ok() {
			// ok() is a DebugFrame
			dframe = Some(f2.ok().unwrap());
		    } else {
			// No debug frame available, keep the old one
			break;
		    }
		}


		match main_page {
		    1 => {
			if dframe.is_some() {
			    render_trace(&mut terminal, dframe.as_ref().unwrap(), &input_str, &labels, cycle_base);
			}
		    },
		    2 => {
			if dframe.is_some() {
			    let d = dframe.as_ref().unwrap();
			    if d.mem_copy.is_some() {
				// Is the new PC visible on the last debugger assembler render screen ?
				// if check_visible_line_disasm &&
				//     (d.cpu.PC < disassembly_offset ||
				//      d.cpu.PC >= disassembly_offset_max ) {
				//     disassembly_offset = d.cpu.PC;
				// }
				// check_visible_line_disasm = false;

				if last_shown_PC != d.cpu.PC &&
				    (d.cpu.PC < disassembly_offset ||
				     d.cpu.PC >= disassembly_offset_max ) {
					disassembly_offset = d.cpu.PC;
					last_shown_PC = d.cpu.PC;
				    }

				let (ofs,max_ofs,pgup,pgdn) =
				    render_disassembler(&mut terminal,
							&d.mem_copy.as_ref().unwrap(),
							disassembly_offset,
							&d.cpu,
							&input_str,
							&labels,
							dframe.as_ref().unwrap(), cycle_base);
				disassembly_offset = ofs;
				disassembly_offset_max = max_ofs;
				disassembly_page_up_offset = pgup;
				disassembly_page_down_offset = pgdn;
			    }
			}
		    },
		    3 => {
			if dframe.is_some() {
			    render_mem(&mut terminal, dframe.as_ref().unwrap(), &input_str, cycle_base, mem_dump_offset);
			}
		    },
		    _ => {
			render_log(&mut terminal, &log_lines, &input_str, cycle_base);
		    }
		};


		if event::poll(Duration::from_millis(20) /*timeout*/).expect("poll works") {
                    if let CEvent::Key(key) = event::read().expect("can read events") {
			//tx.send(TuiEvent::Input(key)).expect("can send events");

			match key.code {
			    KeyCode::Backspace => {
				input_str.pop();
			    },
			    KeyCode::Char(v) => {
				input_str.push(v);
			    },
			    KeyCode::F(1) => /* F1 */ {
				// Switch between run and debug
				if debugger_active == false {
				    control_channel.send(Command::EnterDebugger).expect("Send should work");
				    debugger_active = true;
				} else {
				    control_channel.send(Command::Run).expect("Send should work");
				    debugger_active = false;
				}
			    },
			    KeyCode::F(2) => {
				// Switch debugger page
				main_page = (main_page + 1) % 4;
				if main_page == 1 || main_page == 2 {
				    if dframe.is_some() {
				 	disassembly_offset = dframe.as_ref().unwrap().cpu.PC
				    }
				}
			    },
			    KeyCode::F(3) /* F3 step into (F11 is taken by konsole)  */=> {
				check_visible_line_disasm = true;
				control_channel.send(Command::RunNInstr(1)).expect("Send should work");
			    },
			    KeyCode::F(10) /* F10 step over*/=> {
					if dframe.is_some() {
						check_visible_line_disasm = true;
						let d = dframe.as_ref().unwrap();
						let pc = d.cpu.PC;
						if let Some(mem) = d.mem_copy.as_ref() {
							if mem[pc as usize] == 0x20 {
								control_channel.send(Command::GoToPC(pc+3)).expect("Send should work");
							} else {
								control_channel.send(Command::RunNInstr(1)).expect("Send should work");
							}
						}
					}

					},
				KeyCode::PageUp => {
				if main_page == 2 {
				    disassembly_offset = disassembly_page_up_offset;
				}
				if main_page == 3 {
				    mem_dump_offset = mem_dump_offset.wrapping_sub(0x100)
				}
			    },
			    KeyCode::PageDown => {
				if main_page == 2 {
				    disassembly_offset = disassembly_page_down_offset;
				}
				if main_page == 3 {
				    mem_dump_offset = mem_dump_offset.wrapping_add(0x100);
				}
			    },
			    KeyCode::Enter|KeyCode::F(4) => {

				let command_str = if key.code == KeyCode::Enter {
				    &input_str
				} else {
				    &last_valid_input_str
				};

				let cap = DEBUG_COMMAND_RE.captures(command_str.as_str());

				if cap.is_some() {
				    let cap = cap.unwrap();
				    let cmd = &cap.get(1).unwrap().as_str();
				    let mut to_send = None;
				    let mut command_is_valid = true;

				    if cap.get(3).is_some() {

					// Commands with one argument

					let val_int: Option<usize> = usize::from_str_radix(
					    &cap.get(3).unwrap().as_str(),10).ok();
					let val_u16: Option<u16> = u16::from_str_radix(
					    &cap.get(3).unwrap().as_str(),
					    16).ok();

					to_send = match *cmd {
					    "g" => Some(Command::GoToPC(val_u16.expect("16bits hex number"))),
					    "c" => Some(Command::BreakAtCycle(val_int.expect("Int number") as u64)),
					    "p" => Some(Command::RunNInstr(val_int.expect("Int number"))),
					    "m" => Some(Command::BreakAtMemoryAccess(val_u16.expect("16bits hex number"))),
					    "u" => {
						disassembly_offset = val_u16.unwrap_or(0);
						main_page = 2;
						None
					    },
					    "md" => {
						mem_dump_offset = val_u16.unwrap_or(0);
						main_page = 3;
						None
					    },
						"bpft" => {
							let subtrack = val_int.expect("Subtrack number == track*4");
							info!("Beakpoint on floppy reaching subtrack (times 4) {}", subtrack);
							Some(Command::BreakAtTrack(subtrack))
						}
					    _ => {command_is_valid = false; None}
					};

				    } else if cap.get(1).is_some() {

					// Commands with zero argument

					to_send = match *cmd {
					    "p" => Some(Command::RunNInstr(1)),
					    "gg" => Some(Command::Run),
					    "rc" => {
						if dframe.is_some() {
						    cycle_base = dframe.as_ref().unwrap().cycles;
						}
						None
					    },
					    "S" => Some(Command::StoreMem),
						"bml" => {
							info!("Beakpoint on memory layout set");
							Some(Command::BreakAtMemoryLayout)
						}
						"bpf" => {
							info!("Beakpoint on floppy control set");
							Some(Command::BreakAtFloppy)
						}
					    _ => {command_is_valid = false; None}
					};
				    }

				    if to_send.is_some() {
					control_channel.send(to_send.unwrap()).expect("Send should work");
				    }

				    if command_is_valid {
					last_valid_input_str = command_str.clone();
					input_str.clear();
				    }
				}
			    },
			    _ => ()
			}
                    }
		}
            }
	});

    return  _thread_join_handle;
}

fn close_debugger(terminal: &mut Terminal<CrosstermBackend<Stdout>>) {
    disable_raw_mode().unwrap();
    terminal.show_cursor().unwrap();
    terminal.clear().unwrap();
}


fn render_log( terminal: &mut Terminal<CrosstermBackend<Stdout>>,
	       log_lines: &Arc<Mutex<CircularQueue<MyLogRecord>>>,
	       input_str: &String,
	       cycle_base: u64) {

    terminal.draw(|rect| {
	let tsize = rect.size().height as usize;
	let size = rect.size();
	let chunks = Layout::default()
	    .direction(Direction::Vertical)
	    .margin(0)
	    .constraints(
		[
		    Constraint::Min(20),
		    Constraint::Length(2),
		]
		    .as_ref(),
	    )
	    .split(size);

	let nb_shown_log_lines = tsize - 2;

	// FIXME rewrite this to lock less long
	// let v = log_lines.lock().expect("");
	// let lls: Vec<MyLogRecord> = v.iter().take(20).map(|x| x.clone()).collect();

	let v = log_lines.lock().expect("");
	// FIXME Rewrite this code with a simple for loop. Because
	// the rev() iterator is a pain to work with.

	// take(20) has not an exact size (it can take less than 20)
	// therefore rev() cannot reverse because it needs ExactSize it seems
	// So I first take out a copy of the first 20. It must be a copy
	// because I can't keep refs to elements of another vector.
	let lls: Vec<MyLogRecord> = v.iter().take(nb_shown_log_lines).map(|x| x.clone()).collect();
	let l: Vec<Spans> = lls.iter().rev()
            .map(|line| Spans::from(Span::raw(
		format!("{}:{}:{}", line.level, line.module, line.message))))
            .collect();

	// This code here is a bit tricky. It makes sure
	// we consume all debug messages coming in the
	// channel up to the last one. We do this because
	// sometimes the debugger doesn't consume messages
	// fast enough, leading to an ever growing channel
	// and so a memory leak.

	let block = Paragraph::new(l).block(
            Block::default()
	);
	rect.render_widget(block, chunks[0]);

	let block = Block::default()
	    .title(format!("]] {}",input_str));
	rect.render_widget(block, chunks[1]);
    }).ok();
}

use crate::m6502::{M6502_NF,M6502_VF,M6502_XF,M6502_BF,M6502_DF,M6502_IF,M6502_ZF,M6502_CF};


fn flags_to_str(f:u8) -> String {
    format!(
	"flags:{}{}{}{}{}{}{}{}",
	if f & M6502_NF != 0 {"N"} else {"-"},
	if f & M6502_VF != 0 {"V"} else {"-"},
	if f & M6502_XF != 0 {"X"} else {"-"},
	if f & M6502_BF != 0 {"B"} else {"-"},
	if f & M6502_DF != 0 {"D"} else {"-"},
	if f & M6502_IF != 0 {"I"} else {"-"},
	if f & M6502_ZF != 0 {"Z"} else {"-"},
	if f & M6502_CF != 0 {"C"} else {"-"})
}

fn render_trace( terminal: &mut Terminal<CrosstermBackend<Stdout>>,
		 dframe: &DebugFrame,
		 input_str: &String,
		 labels: &HashMap<u16,String>,
		 cycle_base:u64) {
    terminal.draw(|rect| {
	let size = rect.size();
	let chunks = Layout::default()
	    .direction(Direction::Vertical)
	    .margin(0)
	    .constraints(
		[
		    Constraint::Length(3),
		    Constraint::Min(20),
		    Constraint::Length(12),
		    Constraint::Length(2),
		]
		    .as_ref(),
	    )
	    .split(size);

	let block = Block::default()
	    .title(format!("Seconds:{:.1} F1:Stop/Start F2:Page F3:Step",
			   ((dframe.cycles - cycle_base) as f64)/ ((FRAMES_PER_SECOND * CYCLES_PER_FRAME) as f64)));
	rect.render_widget(block, chunks[0]);

	// let h:usize = dframe.disassembly.len().saturating_sub(chunks[1].height as usize);
	// let l: Vec<Spans> = dframe.disassembly.iter().take(h)
        //     .map(|line| Spans::from(Span::raw(line)))
        //     .rev().collect();

	let h:usize = dframe.trace.len().min((chunks[1].height - 2) as usize);
	let mut disassembler = Disassembler::new();

	let mut l: Vec<Spans> = Vec::new();
	// if dframe.trace.len() < h {
	// 	for _ in 0..(h - dframe.trace.len()) {
	// 		l.push(Spans::from(Span::raw("")));
	// 	}
	// }
	l.extend(
		 dframe.trace.iter().take(h)
            .map(|line| {
			disassembler.code_offset = line.pc;
			let asm = disassembler.disassemble_with_addresses(
				&line.instr_mem,
				&dframe.mem_copy,
				dframe.cpu.X, dframe.cpu.Y,
				Some(1), labels,
				&HashMap::new());

			Spans::from(Span::raw(
				format!("At ${:04X} {} A:${:02X} X:${:02X} Y:${:02X} Flags:{}",
					line.pc, asm[0].source, line.a, line.x, line.y,
					flags_to_str(line.flags))))
			})
            .rev());

	let block = Paragraph::new(l).block(
            Block::default()
	);
	rect.render_widget(block, chunks[1]);

	let block = general_status(dframe, cycle_base);
	rect.render_widget(block, chunks[2]);

	let block = Block::default()
	    .title(format!("]] {}",input_str));
	rect.render_widget(block, chunks[3]);
    }).ok();
}

fn general_status(dframe: &DebugFrame,
		 cycle_base:u64) -> Paragraph {
    // Slots status
    let mut l: Vec<Spans> = dframe.slots_status.iter()
        .map(|line| Spans::from(Span::raw(line)))
        .collect();

    let mut irq = format!("Cycle:{} IRQ: vector:${:04X} Delay:{:} D000:{} HRAMRD:{}",
			  dframe.cycles - cycle_base,
			  dframe.irq_vector, dframe.last_irq_delay,
			  dframe.active_memory_bank,
			  dframe.hramrd);
    if dframe.planned_irqs.len() >= 1 {
	for r in dframe.planned_irqs.iter() {
	    irq.push_str( format!(", C:{} Slot:{} SubSrc:${:X}", r.irq_cycle - dframe.cycles, r.source_slot, r.sub_source).as_str())
	}
    } else {
	irq.push_str(" No IRQ");
    }
    l.push(Spans::from(Span::raw(irq)));

    let g = dframe.gfx_mode_info;
    let mut video_mode = "".to_string();
    if g.col80 == ON {video_mode.push_str("Col80 ");}
    if g.store80 == ON {video_mode.push_str("Store80 ");}
    if g.page2 == ON {video_mode.push_str("Page2 ");} else {video_mode.push_str("Page1 ");}
    if g.text == ON {video_mode.push_str("Text ");}
    if g.mixed == ON {video_mode.push_str("Mixed ");}
    if g.hires == ON {video_mode.push_str("HiRes ");}
    if g.dhires == ON {video_mode.push_str("DblHiRes ");}
	video_mode.push_str(dframe.gfx_base_mode.to_string().as_str());

    let gfx = format!("{}; current CRT line: {} VBL:{} c_in_frame:{} last_vbl_rd:{} float:{:04X}", video_mode,
		      cycle_to_crt_line(dframe.cycles),
		      if dframe.vbl_bit == 1 {"drawing"} else {"vbl"},
		      dframe.cycles % (CYCLES_PER_FRAME as u64),
		      (dframe.last_vblank_read as isize) - (cycle_base as isize),
		      if let Some (a) = dframe.floating_addr {a} else {0xFFFF});

    l.push(Spans::from(Span::raw(gfx)));

    let p = Paragraph::new(l).block(
        Block::default().borders(Borders::TOP)
    );

    return p;
}

fn render_disassembler(
    terminal: &mut Terminal<CrosstermBackend<Stdout>>,
    mem: &Vec<u8>,
    mut offset: u16,
    cpu: &Cycle6502,
    input_str: &String,
    labels: &HashMap<u16,String>,
    dframe: &DebugFrame,
    cycle_base:u64) -> (u16,u16,u16,u16) {

    const STEP_PAGE_LINES: usize = 5;
    const BYTES_PER_INSTR: usize = 3;

    let mut max_cycle = 0; //u64::MAX;

    let mut trace_map: HashMap<u16,&CpuDebugState> = HashMap::new();
    for line in &dframe.trace {
	// Either we've never seen this PC value
	// either we see a more recent one (clock cycle wise)
	if !trace_map.contains_key(&line.pc) ||
	    (line.cycle > trace_map.get(&line.pc).unwrap().cycle) {
		trace_map.insert(line.pc,line);
		if line.cycle > max_cycle {
		    max_cycle = line.cycle;
		}
	    }
    }

    let mut page_down_offset = 0;
    let mut offset_max = 0;

    terminal.draw(|rect| {
		let size = rect.size();
		let chunks = Layout::default()
			.direction(Direction::Vertical)
			.margin(0)
			.constraints(
			[
				Constraint::Min(10),
				Constraint::Length(12),
				Constraint::Length(2),
			]
				.as_ref(),
			)
			.split(size);

		let content_layout = Layout::default()
			.direction(Direction::Horizontal)
			.margin(0)
			.constraints(
			[
				Constraint::Min(10),
				Constraint::Length(20),
			]
				.as_ref(),
			)
			.split(chunks[0]);

	// We disassemble from offset onwards.
	// offset is expected to be *before* or *at* cpu.PC
	// offset is the page start if you will.

	let disasm = Disassembler::with_offset(offset);
	let (_, dmem) = mem.split_at(offset as usize);
	let mut lines = disasm.disassemble_with_addresses(
	    dmem, &dframe.mem_copy,
	    dframe.cpu.X, dframe.cpu.Y,
	    Some(content_layout[0].height as usize), labels, &trace_map);

	offset_max = offset + lines[lines.len()-1].pc;

	// Does the current PC line appear on the screen ?
	/* let mut pc_line_drawn = false;
	for line in lines.iter() {
	    if line.pc + offset == cpu.PC {
		pc_line_drawn = true;
		break
	    }
	} */

	/* if !pc_line_drawn {
	    // Either the disassembly didn't go down to PC,
	    // either the disassembly offsets passed over PC.
	    // In both case, we reset the disassembly at PC.
	    offset = cpu.PC;
	    let disasm = Disassembler::with_offset(offset);
	    let (_, dmem) = mem.split_at(offset as usize);
	    lines = disasm.disassemble_with_addresses(
		dmem,
		&dframe.mem_copy,
		dframe.cpu.X, dframe.cpu.Y,
		Some(chunks[0].height as usize),
		labels, // ADD CLUEZ HERE !!!
		&trace_map);
	} */

	page_down_offset = offset + lines[STEP_PAGE_LINES].pc;

	let mut l: Vec<Spans> = vec![];

	for line_ndx in 0..lines.len() {
	    let line = &lines[line_ndx];

	    if labels.contains_key(&(line.pc+offset)) {
		let label = labels.get(&(line.pc+offset)).unwrap();
		l.push(
		    Spans::from(Span::styled(
			format!("{}:",label),
			Style::default().fg(Color::DarkGray))));
	    }

	    let background = if line.pc+offset == cpu.PC {
		// FIXME if disassembly is not compatible with the
		// offset, the equality will fail

		// FIXME If the memory displayed is not where these
		// the execution is (another bank), then we shouldn't
		// highlight
		Color::LightRed
	    } else {
		Color::Black
	    };

	    if trace_map.contains_key(&(line.pc+offset)) {
		let s = trace_map.get(&(line.pc+offset)).unwrap();
		l.push(
		    Spans::from(vec![
			Span::styled(format!("{: <42}", &line.source),
				     Style::default().bg(background)),
			Span::styled(
			    format!("A=${:02X} X=${:02X} Y=${:02X}{}, c:{:4}",
				    s.a, s.x, s.y,
				    if let Some(old_ram) = s.old_mem_value {
					// Write operation
					if s.mem_access.is_some() {
					    let (addr,byte) = s.mem_access.unwrap();

					    format!(" {:04X}={:02X}→{:02X} {}", addr, byte, old_ram,
						    if labels.contains_key(&addr) {
							labels.get(&addr).unwrap().to_string()
						    } else { "".to_string() } )
					} else  {
					    "write???".to_string()
					}
				    } else {
					// Read operation or nothing operation
					if let Some((addr,byte)) = s.mem_access {
					    format!(" {:04X}={:02X} {}", addr, byte,
						    if labels.contains_key(&addr) {
							labels.get(&addr).unwrap().to_string()
						    } else { "".to_string() })
					} else {
					    "".to_string()
					}
				    },
				    -((max_cycle - s.cycle) as isize)),
			    Style::default().bg(background)
			),
			/* Span::styled(format!("{:02X} {:02X} {:02X}",
					  s.instr_mem[0],
					  s.instr_mem[1],
					  s.instr_mem[2]),
				     Style::default().bg(background)) */

		    ]));
	    } else if line.pc+offset == cpu.PC {
		l.push(
		    Spans::from(vec![
			Span::styled(
			    format!("{: <42}", &line.source),
			    Style::default().bg(background)),
			Span::styled(
			    render_cpu_state(cpu),
			    Style::default().bg(background))
			]));
	    } else {
		l.push(
		    Spans::from(
			Span::styled(
			    &line.source,
			    Style::default().bg(background))));
	    }
	}

	let mut stack_trace: Vec<Spans> = vec![];
	stack_trace.push(Spans::from("Stack"));

	let mut ofs: usize =  0x1FF as usize;
	while ofs > 0x100 + (cpu.S as usize)  {
		stack_trace.push(
			Spans::from(
			Span::styled(
				format!("${:04X}",
					(mem[ofs] as u16)*256+(mem[ofs-1] as u16)),
				Style::default())));
		ofs -= 2;
	}

	for i in 0..BANKS_OF_INTEREST.len() {
		stack_trace.push(
			Spans::from(
				format!("${:04X}: {}-{}", BANKS_OF_INTEREST[i],
					dframe.memory_banks[i].short_str(),
					dframe.memory_banks_for_write[i].short_str())));
	}


	let block = Paragraph::new(l).block(
            Block::default()
	);
	rect.render_widget(block, content_layout[0]);


	let block = Paragraph::new(stack_trace).block(
		Block::default()
	);
	rect.render_widget(block, content_layout[1]);


	let block = general_status(dframe, cycle_base);
	rect.render_widget(block, chunks[1]);

	let block = Block::default()
	    .title(format!("]] {}",input_str));
	rect.render_widget(block, chunks[2]);
    }).ok();


    // ------------------------------------------------------------
    // We compute the PageUp action.
    // PageUp means we want to go up by some disassembly lines
    // *and* make sure that the current lline (offset) still
    // disassembles correctly.

    let mut base_offset = if offset > 0 { offset-1 } else {offset};

    // if base_offset > cpu.PC {
    // 	base_offset = cpu.PC
    // }

    loop {
	let disasm = Disassembler::with_offset(base_offset);
	let (_, dmem) = mem.split_at(base_offset as usize);
	let lines = disasm.disassemble_with_addresses(
	    dmem, &dframe.mem_copy,
	    dframe.cpu.X, dframe.cpu.Y,
	    Some(3*BYTES_PER_INSTR*STEP_PAGE_LINES), labels,
	    &trace_map);

	// Disassemble up to the current offset (ie the current first
	// line on the screen)
	let mut i = 0;
	while base_offset+lines[i].pc < offset && i < lines.len() - 1 {
	    i += 1
	}

	if (base_offset+lines[i].pc == offset && i >= STEP_PAGE_LINES) /* line found and offset large enough*/
	    || base_offset == 0 /* beginning of memory */ {
	    break
	}

	// Up by a byte
	base_offset -= 1;

	// Sometimes going up goes too far, dunno why FIXME
	if (offset - base_offset) as usize > STEP_PAGE_LINES*BYTES_PER_INSTR*3 {
	    // In that case we go up to the last line that
	    // was correctly diassembling down to the offset-line.
	    break
	}
    }

    let page_up_offset = base_offset;


    (offset,offset_max,page_up_offset,page_down_offset)
}


// use lazy_static::lazy_static;
// use regex::Regex;
use std::io::BufReader;
use std::io::prelude::*;
use std::fs::File;

// fn load_symbols(filename: PathBuf) -> HashMap<u16,String> {

//     info!("Reading symbol from '{}'", filename.as_os_str().to_string_lossy());
//     lazy_static! {
// 	static ref SYMBOL_RE:Regex = Regex::new(r"\s*(\S+).*=.*\$([0-9A-Fa-f]+).*").unwrap();
//     }

//     let file = File::open(filename).unwrap();
//     let reader = BufReader::new(file);

//     let mut labels:HashMap<u16,String> = HashMap::new();

//     // Read the file line by line using the lines() iterator from std::io::BufRead.
//     for (index, line) in reader.lines().enumerate() {
//         let line = line.unwrap(); // Ignore errors.
// 	let cap = SYMBOL_RE.captures(&line);
// 	if cap.is_some() {
// 	    let cap = cap.unwrap();
// 	    let symbol = &cap.get(1).unwrap().as_str();
// 	    let value:u16 = u16::from_str_radix(
// 		&cap.get(2).unwrap().as_str(), 16).expect("Expecting a U16 in alphanumeric representation");

// 	    info!("Symbol: '{}' = ${:X}", symbol, value);
// 	    labels.insert(value, symbol.to_string());
// 	}
//     }

//     return labels;
// }

fn parse_addresses_string(contents: &str, labels: &mut HashMap<u16,String>,
	tracked: &mut HashSet<u16>) {


    // "Address Symbol"
    let re = Regex::new(r"^\s*([0-9A-Fa-f]{0,4})\s+([^:]+)\s*(:V)?.*$").unwrap();
    // "Symbol = address"
    let SYMBOL_RE:Regex = Regex::new(r"\s*(\S+).*=.*\$([0-9A-Fa-f]+)\\s*(:V)?.*").unwrap();
    for (ndx, line) in contents.lines().enumerate() {

        let mut success = false;

	if let Some(c) = re.captures(&line) {
            if let Some(addr) = u16::from_str_radix(&c[1], 16).ok() {
                info!("${:04X}: '{}'",addr,&c[2]);
                success = true;
				labels.insert(addr, c[2].to_string());

				if let Some(v) = c.get(3) {
				    println!("V={} ${}",v.as_str(), addr);
					tracked.insert(addr);
				}
	    	}
        } else if let Some(cap) = SYMBOL_RE.captures(&line) {
	    let symbol = &cap.get(1).unwrap().as_str();
	    let value:u16 = u16::from_str_radix(
		&cap.get(2).unwrap().as_str(), 16).expect("Expecting a U16 in alphanumeric representation");

	    info!("Symbol: '{}' = ${:X}", symbol, value);
	    labels.insert(value, symbol.to_string());
	}

        if !success {
            if line.trim().len() > 0 && !line.trim().starts_with("#"){
                error!("Could not parse line {}: '{}'",ndx+1,line)
            }
        }
    }
}

pub fn load_symbols(file_path: &PathBuf, labels: &mut HashMap<u16,String>,
	tracked: &mut HashSet<u16>)  {
    if file_path.is_file() {

        let contents = fs::read_to_string(file_path)
            .expect("Should have been able to read the file",);

        return parse_addresses_string(&contents, labels, tracked);

    } else {
        error!("{} not found", file_path.display());
    }
}

pub fn render_cpu_state(cpu: &Cycle6502) -> String {
    format!(
	"SP=01{:02X} A={:02X}, X={:02X}, Y={:02X} flags:{}{}{}{}{}{}{}{}",
	cpu.S, cpu.A, cpu.X, cpu.Y,
	if cpu.P & M6502_NF != 0 {"N"} else {"-"},
	if cpu.P & M6502_VF != 0 {"V"} else {"-"},
	if cpu.P & M6502_XF != 0 {"X"} else {"-"},
	if cpu.P & M6502_BF != 0 {"B"} else {"-"},
	if cpu.P & M6502_DF != 0 {"D"} else {"-"},
	if cpu.P & M6502_IF != 0 {"I"} else {"-"},
	if cpu.P & M6502_ZF != 0 {"Z"} else {"-"},
	if cpu.P & M6502_CF != 0 {"C"} else {"-"})
}








fn render_mem( terminal: &mut Terminal<CrosstermBackend<Stdout>>,
	       dframe: &DebugFrame,
	       input_str: &String,
	       cycle_base: u64,
	       mem_dump_offset: u16) {

    terminal.draw(|rect| {
	let tsize = rect.size().height as usize;
	let size = rect.size();
	let chunks = Layout::default()
	    .direction(Direction::Vertical)
	    .margin(0)
	    .constraints(
		[
		    Constraint::Min(20),
		    Constraint::Length(2),
		]
		    .as_ref(),
	    )
	    .split(size);

	let nb_visible_lines = tsize - 2;

	let mut i: usize = 65536;
	while i > ((rect.size().width as usize - 10 )/3) {
		i = i / 2;
	}
	let bytes_per_line = i;


	let mut l: Vec<Spans> = Vec::new();
	if let Some(mem) = dframe.mem_copy.as_ref() {

	    use std::fmt::Write;

	    // In line offsets
	    l.push(Spans::from(Span::raw(
		(0..bytes_per_line).fold("      ".to_string(),
			     |mut res, x| {
				 write!(&mut res, "{:02X} ",x).unwrap();
				 res
			     }))));

	    // Separator
	    l.push(Spans::from(Span::raw(
		(0..bytes_per_line).fold("------".to_string(),
			     |mut res, x| {
				 write!(&mut res, "---").unwrap();
				 res
			     }))));

	    for line in 0..nb_visible_lines.min(nb_visible_lines) {
			let ofs = mem_dump_offset as usize+ line*bytes_per_line;
			let mut s: String = format!("{:04X}: ",ofs);
			for column in 0..bytes_per_line {
				if ofs + column < mem.len() {
					s.push_str(format!("{:02X} ",mem[ofs+column]).as_str())
				}
			}
			l.push(Spans::from(Span::raw(s)));
	    }
	}

	let block = Paragraph::new(l).block(
            Block::default()
	);
	rect.render_widget(block, chunks[0]);

	let block = Block::default()
	    .title(format!("]] {}",input_str));
	rect.render_widget(block, chunks[1]);
    }).ok();
}
