// https://users.rust-lang.org/t/rust-performance-help-convolution/44075

fn re_re_conv(sample: &[f32], coeff: &[f32]) -> Vec<f32> {
    sample
        .windows(coeff.len())
        .map(|window| {
            window
                .iter()
                .zip(coeff.iter())
                .map(|(sample, coeff)| sample * coeff)
                .sum::<f32>()
        })
        .collect()
}

const SAMPLELEN: usize = 5_000;
const COEFFLEN: usize = 32;

use std::time::{Instant, Duration};

fn main() {
    let mut sample = Vec::with_capacity(SAMPLELEN);
    for i in 0..SAMPLELEN {
	sample.push((i % 31) as f32);
    }

    let mut coeff = Vec::with_capacity(COEFFLEN);
    for i in 0..COEFFLEN {
	coeff.push(((1 + (i % 97)) as f32) / 7.0);
    }

    let coeff2 = Vec::from([1.0, 1.0, 1.0, 1.0]);

    let mut result: f32 = 0.0;
    for i in 0..100 {
	let now = Instant::now();
	let zorg = re_re_conv(&sample, &coeff);
	result += re_re_conv(&zorg, &coeff2)[0];
	println!("{}", now.elapsed().as_nanos());
    }
    println!("{}", result);
}
