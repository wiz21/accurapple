use log::{debug, error, trace};
use std::path::{Path, PathBuf};

use crate::generic_peripheral::{BreakPointTriggerRef, GenericPeripheral, IRQAction, IRQRequest, IrqChange};
use crate::wozfile;
use crate::wozfile::WozFile;
use crate::wozpack::{subtrack_str, Controller};

const NUM_DRIVES: usize = 2;
pub const STOP_DRIVE1: u16 = 0xC0E8;
pub const START_DRIVE1: u16 = 0xC0E9;

struct Drive {
    //woz: WozFile,
    ctrl: Controller,
}

pub struct DiskController {
    drives: [Drive; NUM_DRIVES],
    selected: u8, // selected drive (0 or 1)
    motor: bool,  // is motor on?
    breakpoint: Option<BreakPointTriggerRef>,
}

impl DiskController {
    pub fn new() -> DiskController {
        DiskController {
            drives: [Drive::new(), Drive::new()],
            selected: 0,
            motor: false,
            breakpoint: None,
        }
    }

    pub fn save_log(&mut self) {
        let default = PathBuf::from("stepper.log");
        self.drives[0].ctrl.stepper.stop_logging(&default)
    }

    pub fn start_stepper_logging(&mut self) {
        self.drives[0].ctrl.stepper.start_logging();
    }

    pub fn stop_stepper_logging(&mut self, path: &PathBuf) {
        self.drives[0].ctrl.stepper.stop_logging( path);
    }

    pub fn remove_disk(&mut self, disknum: usize) {
        self.drives[disknum] = Drive::new();
        self.breakpoint = None;
    }

    pub fn load_disk(&mut self, disknum: usize, imagefilename: &Path) -> u32 {
        self.drives[disknum] = Drive::new();
        //self.drives[disknum].woz = wozpack::load_disk(imagefilename);
        self.drives[disknum]
            .ctrl
            .set_woz(wozfile::load_disk(imagefilename));
        assert!(self.has_disk(disknum));
        self.drives[disknum].ctrl.woz_file.checksum().unwrap()
    }

    pub fn woz(&self, disknum: usize) -> &WozFile {
        &self.drives[disknum].ctrl.woz_file
    }

    pub fn has_disk(&self, _disknum: usize) -> bool {
        true
    }

    pub fn disk_file_path(&self, disknum: usize) -> Option<PathBuf> {
        self.drives[disknum].ctrl.woz_file.source_file.clone()
    }


    pub fn current_track(&self) -> Option<usize> {
        self.drives[0].ctrl.current_subtrack()
    }

    pub fn reset(&mut self) {
        self.drives[0].ctrl.reset()
    }

    pub fn lss_debug(&self) -> String {
        self.drives[0].ctrl.lss_debug()
    }

}

impl Drive {
    pub fn new() -> Drive {
        Drive {
            //woz: WozFile::new(),
            ctrl: Controller::new(),
        }
    }

    fn read_latch(&mut self, _cycle: u64) -> u8 {
        //return self.ctrl.data_register(cycle);
        self.ctrl.data_register()
    }
}

impl GenericPeripheral for DiskController {

    fn ack_irq(&mut self, _req: IRQRequest, cycle: u64) -> (Option<Vec<IRQRequest>>, Option<IrqChange>) {
        debug!("ack_irq(): cycle:{}", cycle);
        let slot = self.get_slot();
        let drive = &mut self.drives[self.selected as usize];

        drive.ctrl.complete_current_stepper_eval(cycle);

		if let Some(bp) = &self.breakpoint {
			if let Some(track) = drive.ctrl.current_subtrack() {
				//println!("Track: {}",track);
				bp.borrow_mut().break_on_floppy_track(track);
			}
		}

        debug!(
            "ack_irq: (cycle {}) (track {:02.2})",
            cycle,
            subtrack_str(drive.ctrl.current_subtrack())
        );

        let next_stop = drive.ctrl.prepare_next_stepper_eval(cycle);
        let v = vec![IRQRequest::new(
            next_stop,
            slot,
            0,
            false,
            IRQAction::Replace,
        )];
        (Some(v), None)
    }

    fn cycle_tick(&mut self, data_on_bus: u8) {
        // Tick the controller
        let drive = &mut self.drives[0];
        drive.ctrl.tick_lss_one_microsecond(data_on_bus);
    }

    fn get_name(&self) -> String {
        String::from("Disk ][")
    }

    fn get_slot(&self) -> usize {
        6 // FIXME : that's slot 7, but somehow slot 5 doesn't work !!!
    }

    fn status_str(&self, _cycle: u64) -> String {
        let d = &self.drives[self.selected as usize];

        format!(
            "Drive:{} Motor:{} {}",
            self.selected + 1,
            if self.motor { "on " } else { "off" },
            d.ctrl.status_str()
        )
    }

    fn write_io(&mut self, _addr: u16, _data: u8, _cycle: u64) -> (Option<Vec<IRQRequest>>, Option<IrqChange>) {
        // Following a write to memory, this transfers the
        // written data to the peripheral.
        error!("write not supported");
        //self.read_io(addr, data, cycle);
        (None, None)
    }

    fn read_io(&mut self, addr: u16, _mem_byte: u8, cycle: u64) -> (u8, Option<Vec<IRQRequest>>, Option<IrqChange>) {
        //fn doIO(&mut self, addr: u16, val: u8) -> u8
        let slot = self.get_slot();
        let drive = &mut self.drives[self.selected as usize];

        match addr & 0xf {
            0..=7 => {
                let charge = match addr & 0x7 {
                    0 | 2 | 4 | 6 => false,
                    1 | 3 | 5 | 7 => true,
                    _ => false // rustc doesn't catch I have all the possibilities
                };
                let phase = ((addr >> 1) & 0b11) as usize;

                debug!(
                    "FLOPPY ${:04X}: (cycle {}) Stepper phase {:} set to {} (track {:02.2})",
                    addr,
                    cycle,
                    phase,
                    if charge {"on"} else {"off"},
                    subtrack_str(drive.ctrl.current_subtrack())
                );

                drive.ctrl.complete_current_stepper_eval(cycle);
                drive.ctrl.set_magnet(phase, charge, cycle);
                let v = vec![IRQRequest::new(
                    drive.ctrl.prepare_next_stepper_eval(cycle),
                    slot,
                    0,
                    false,
                    IRQAction::Replace,
                )];
                return (drive.read_latch(cycle), Some(v), None);
            }
            /*
             * Turn drive motor off.
             */
            8 => {
                trace!("FLOPPY: Turning drive motor off");
                self.motor = false;

                if let Some(bpt) = &self.breakpoint {
                    bpt.borrow_mut().break_on_floppy();
                }
            }
            /*
             * Turn drive motor on.
             */
            9 => {
                trace!("FLOPPY: (${:X})Turning drive motor ON", addr);
                self.motor = true;
                if let Some(bpt) = &self.breakpoint {
                    bpt.borrow_mut().break_on_floppy();
                }
            }
            /*
             * Select drive 1.
             */
            0xa => {
                trace!("FLOPPY: Selecting drive #0");

                self.selected = 0;
            }
            /*
             * Select drive 2.
             */
            0xb => {
                trace!("FLOPPY: Selecting drive #1");

                self.selected = 1;
            }
            /*
             * Read a disk byte if read mode is active.
             */
            0xc => drive.ctrl.set_shift_load_switch(0),
            0xd => {
                // WOZ Spec: Another nuance that needs to be
                // implemented is that reading $C08D,X will reset the
                // sequencer and clear the contents of the data
                // latch. This is used to great effect in the E7
                // protection scheme to resynchronize the nibble
                // stream to make timing bits become valid data bits.

                debug!("Reset LSS at cycle {}", cycle);
                drive.ctrl.reset_lss();

                // This will fill the data register with 0xFF so
                // that the CPU has time to read it (else the LSS
                // will be ticked and the value will disappear
                // before being accessed)

                // See Sather write protection detection code (page 9-17):
                // LDA $C08D,X => load the data register with the write protect info
                // LDA $C08E,X => read the data register
                // BMI floppy_write_protected

                drive.ctrl.set_shift_load_switch(1);
            }
            /*
             * Select read mode and read the write protect status.
             */
            0xe => {
                // Not sure if it's the right way to do it.
                // See Sather page 9-17. Also, AppleWin shows
                // that there are other way to check for write
                // protection (see disk.cpp code)

                // Sather 9-14: A3: READ/WRITE C08E,X/C08F,X
                // => I guess READ is set when C08E,X; write when C08F,X
                drive.ctrl.set_read_write_switch(0);
            }
            /*
             * Select write mode.
             */
            0xf => drive.ctrl.set_read_write_switch(1),
            _ => {}
        }

        (
            if addr & 1 == 1 {
                trace!("Floating bus");
                0
            } else {
                let l = drive.read_latch(cycle);
                trace!("data register {:X}", l);
                l
            },
            None,
            None
        )
    }

    fn set_debugger(&mut self, breakpoint: BreakPointTriggerRef) {
        self.breakpoint = Some(breakpoint);
    }

}
