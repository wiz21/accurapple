// Vertex shader bindings

struct VertexOutput {
    @location(0) tex_coord: vec2<f32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn vs_main(
    @location(0) position: vec2<f32>,
) -> VertexOutput {
    var out: VertexOutput;
    out.tex_coord = fma(position, vec2<f32>(0.5, -0.5), vec2<f32>(0.5, 0.5));
    out.position = vec4<f32>(position, 0.0, 1.0);
    return out;
}

// Fragment shader bindings

@group(0) @binding(0) var r_tex_color: texture_2d<f32>;
@group(0) @binding(1) var r_tex_sampler: sampler;
struct Locals {
    time: f32,
}
@group(0) @binding(2) var<uniform> r_locals: Locals;


// let a: vec3<f32> = vec3<f32>(1.0,0.956, 0.621);
// let b: vec3<f32> = vec3<f32>(1.0,-0.272,-0.647);
// let c: vec3<f32> = vec3<f32>(1.0,-1.106,1.703);

const a: vec3<f32> = vec3<f32>(1.000, 1.000, 1.000);
const b: vec3<f32> = vec3<f32>(0.956,-0.272,-1.106);
const c: vec3<f32> = vec3<f32>(0.621,-0.647, 1.703);
const YIQ2RGB: mat3x3<f32> = mat3x3<f32>( a,   b,  c);

const size: vec2<f32> = vec2<f32>(640.0, 240.0);
const TAU:f32 = 6.28318530717958647693;

fn srgb2lin(  cs: vec3<f32> ) -> vec3<f32>
{
	let c_lo: vec3<f32> = cs / 12.92;
	let c_hi: vec3<f32>  = pow( (cs + 0.055) / 1.055, vec3(2.4) );
	let s: vec3<f32>  = step(vec3(0.04045), cs);
	return mix( c_lo, c_hi, s );
}

@fragment
fn fs_main(@location(0) tex_coord: vec2<f32>,
           @builtin(position) position: vec4<f32>
) -> @location(0) vec4<f32> {

    // https://github.com/gfx-rs/wgpu-rs/issues/912

    let size: vec2<f32> = vec2<f32>(textureDimensions(r_tex_color).xy);
    let uv:vec2<f32> = position.xy / size;

    // Is the color burst "on" for the current line ?
    let colorburst:f32 = textureSample(r_tex_color, r_tex_sampler, vec2<f32>(1.0, tex_coord.y)).r;
    // This mask will be used to change the behaviour of the
    // filter to better render parts without colour burst.
    // This means:removing the IQ components and just keep the Y.

    let colorburst_mask: vec3<f32> = vec3<f32>(1.0, colorburst, colorburst);

    var res_color_org = vec3<f32>(textureSample(r_tex_color, r_tex_sampler, tex_coord).rgb);
    var res_color: vec3<f32> = vec3(0.0,0.0,0.0);

    var YIQ:vec3<f32>  = vec3<f32>(0.0,0.0,0.0);
    for(var n: i32=-2; n<2; n++) {
        let pos: vec2<f32> = uv + vec2<f32>(f32(n) /size.x, 0.0);
        let phase:f32 = (position.x + f32(n)) * TAU / 4.0;
        YIQ += textureSample(r_tex_color, r_tex_sampler, pos).rgb * vec3(1.0, cos(phase), sin(phase));
    }

    // Multiply by 0.25 to average the sum above.
    YIQ = YIQ * colorburst_mask * 0.25;
    res_color = srgb2lin(YIQ2RGB * YIQ);

    if (position.x>=560.0) {
        res_color = res_color_org;
    }

    // Render pixels like on a CRT
    //res_color = res_color_org;

    return vec4<f32>(res_color.rgb, 1.0);
}
