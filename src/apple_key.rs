use std::{collections::{HashMap, HashSet}, fmt};
use itertools::Itertools;
use strum_macros::Display;
use std::hash::Hash;
use log::{debug, warn, info, trace, error};

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash)]
pub(crate) enum Key {
    Character(char),
    Delete,
    Tab,
    Left,
    Right,
    Up,
    Down,
    Return,
    Escape,
	Reset,
	Space
}

impl fmt::Display for Key {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
       match self {
           Key::Character(c) => write!(f, "{}", c),
           Key::Delete => write!(f, "Del"),
           Key::Tab => write!(f, "Tab"),
           Key::Left => write!(f, "Left"),
		   Key::Right => write!(f, "Right"),
		   Key::Up => write!(f, "Up"),
		   Key::Down => write!(f, "Down"),
		   Key::Return => write!(f, "Return"),
		   Key::Escape => write!(f, "Escape"),
		   Key::Reset => write!(f, "Reset"),
		   Key::Space => write!(f, "Space")
       }
    }
}

pub fn key_to_kbd_code(key: &Key) -> u8 {
    match key {
        Key::Delete => 0x7F,
        Key::Tab => 0x09,
        Key::Left => 0x8,
        Key::Right => 0x15,
        Key::Up => 0xB,
        Key::Down => 0xA,
        Key::Return => 0xd,
        Key::Escape => 0x1b,
        Key::Reset => 0x00, // FIXME Dirty !
        Key::Space => 0x20,
        Key::Character(c) =>
            match c {
            // See table 2-2 in Apple2E Ref Manual
            // Normal column
            '.' => 0x2E,
            '/' => 0x2F,
            '0'..='9' => ((*c as u32) - ('0' as u32) + 0x30) as u8,
            ';' => 0x3B,
            '=' => 0x3D,
            '[' => 0x5B,
            '\\' => 0x5C,
            ']' => 0x5D,
            '`' => 0x60,
            'a'..='z' => ((*c as u32) - ('a' as u32) + 0x61) as u8,

            // Shift column
            ' ' => 0x20,
            '"' => 0x22,
            '<' => 0x3C,
            '_' => 0x5f,
            '>' => 0x3e,
            '?' => 0x3f,
            ')' => 0x29,
            '!' => 0x21,
            '@' => 0x40,
            '#' => 0x23,
            '$' => 0x24,
            '%' => 0x25,
            '^' => 0x5E,
            '&' => 0x26,
            '\'' => 0x27,
            '*' => 0x2A,
            '(' => 0x28,
            ':' => 0x3A,
            '+' => 0x2B,
            ',' => 0x2C,
            '-'|'\u{2212}' => 0x2D,
            '{' => 0x7B,
            '|' => 0x7C,
            '}' => 0x7D,
            '~' => 0x7E,
            'A'..='Z' => ((*c as u32) - ('A' as u32) + 0x41) as u8,
            _ => {
                error!("Unsupported key \\u'{:X}' or {}", *c as u32, c);
                0x07 // Bell
            }
	    }
    }
}


#[derive(PartialEq)]
pub enum KeyAction {
    KeyPressed,
    KeyReleased
}

impl fmt::Display for KeyAction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
       match self {
           KeyAction::KeyPressed => write!(f, "KeyPressed"),
           KeyAction::KeyReleased => write!(f, "KeyReleased")
       }
    }
}


pub struct KeyEvent {
    pub(crate) action: KeyAction,
    pub cycle: u64,
    pub key: Key
}

pub struct KeyboardQueue {
    key_events: Vec<KeyEvent>
}

enum Location {
    AtEnd,
    Inside(usize)
}

impl KeyboardQueue {
    pub fn new() -> KeyboardQueue {
        KeyboardQueue {
            key_events : Vec::new()
        }
    }

    fn locate(&self, cycle: u64) -> Location {

        if self.key_events.is_empty() {
            return Location::Inside(0)
        } else {
            let mut i = self.key_events.len()-1;
            while i > 0 && self.key_events[i].cycle > cycle {
                i -= 1;
            }
            if i == self.key_events.len()-1 {
                return Location::AtEnd
            } else {
                Location::Inside(i)
            }
        }
    }

    pub fn clear_up_to(&mut self, cycle: u64) {
        if !self.key_events.is_empty() {
            let mut p = 0;
            let mut clearable: HashMap<Key, usize> = HashMap::new();
            let mut to_delete: Vec<usize> = Vec::new();

            while p < self.key_events.len() && self.key_events[p].cycle < cycle {
                let ke = &self.key_events[p];
                match ke.action {
                    KeyAction::KeyPressed => {
                        clearable.insert(ke.key, p);
                    }
                    KeyAction::KeyReleased => {
                        if clearable.contains_key(&ke.key) {
                            let p_pressed = *clearable.get(&ke.key).unwrap();
                            to_delete.push(p_pressed);
                            to_delete.push(p);

                            // Get ready for the next pressed/released pair
                            clearable.remove(&ke.key);
                        }
                    }
                }
                p += 1;
            }

            // I *do* need to sort :-)
            for ndx in to_delete.iter().sorted().rev() {
                self.key_events.remove(*ndx);
            }
        }
    }

    pub fn insert(&mut self, key_event: KeyEvent) {

        if !self.key_events.is_empty() {
            assert!(self.key_events.last().unwrap().cycle <= key_event.cycle);
        }

        match self.locate(key_event.cycle) {
            Location::AtEnd => {
                self.key_events.push(key_event);
            }
            Location::Inside(p) => {
                self.key_events.insert(p, key_event);
            }
        }

        // for ke in self.key_events.iter() {
        //     println!("KQ: {} {} {}", ke.cycle, ke.key, ke.action);
        // }
    }

    pub fn last_cycle(&self) -> u64 {
        if let Some(last) = self.key_events.last() {
            return last.cycle
        } else {
            return 0
        }
    }

    pub fn last_key_press(&self, cycle: u64) -> Option<&KeyEvent> {
        if self.key_events.is_empty() {
            return None;
        } else {
            for ke in self.key_events.iter().rev() {
                if cycle < ke.cycle {
                    return None
                } else if ke.action == KeyAction::KeyPressed {
                    return Some(ke)
                }
            }
            return None;
        }
    }

    fn print(&self) {
        for ke in self.key_events.iter() {
            println!("key: {} {} {}", ke.cycle, ke.action, ke.key);
        }
    }

    pub fn key_currently_pressed_at(&self, cycle: u64) -> Option<&KeyEvent> {
        //println!("Active key at {} ?", cycle);
        if self.key_events.is_empty() {
            return None;
        }

        let mut p =  match self.locate(cycle) {
            Location::AtEnd => {
                self.key_events.len() - 1
            }
            Location::Inside(ip) => {
                ip
            }
        };

        let mut ignore: HashSet<Key> = HashSet::new();

        //println!("location: {}",p);
        //p = self.key_events.len() - 1;
        loop {
            if self.key_events[p].cycle <= cycle && self.key_events[p].action == KeyAction::KeyPressed && !ignore.contains(&self.key_events[p].key) {
                //println!("key_currently_pressed_at @cycle {}: key cycle: {} key={} {}", cycle, self.key_events[p].cycle, self.key_events[p].action,self.key_events[p].key);
                return Some(&self.key_events[p])
            } else {
                if self.key_events[p].action == KeyAction::KeyReleased {
                    //println!("key_currently_pressed_at @cycle {}: key released at key cycle: {} key={} {}", cycle, self.key_events[p].cycle, self.key_events[p].action,self.key_events[p].key);
                    ignore.insert(self.key_events[p].key);
                }
                if p == 0 {
                    break
                } else {
                    p -= 1
                }
            }
        }

        return None;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn my_test() {
        let mut kq = KeyboardQueue::new();
        kq.insert(KeyEvent {action: KeyAction::KeyPressed, cycle:123, key:Key::Escape});
        kq.insert(KeyEvent {action: KeyAction::KeyPressed, cycle:300, key:Key::Left});
        kq.insert(KeyEvent {action: KeyAction::KeyPressed, cycle:100, key:Key::Right});

        for i in 1..kq.key_events.len() {
            assert!(kq.key_events[i-1].cycle <=  kq.key_events[i-1].cycle)
        }

        let k = kq.key_currently_pressed_at(200).unwrap();
        assert!(k.key == Key::Escape, "Got {}", k.key);
        let k = kq.key_currently_pressed_at(400).unwrap();
        assert!(k.key == Key::Left, "Got {}", k.key);
        assert!(kq.key_currently_pressed_at(0).is_none());

        // 100 right pressed
        // 123 escape pressed
        // 300 left pressed
        kq.insert(KeyEvent {action: KeyAction::KeyReleased, cycle:400, key:Key::Right});

        // 100 right pressed
        // 123 escape pressed
        // 300 left pressed
        // 400 right released

        assert!( kq.key_currently_pressed_at(500).unwrap().key == Key::Left);
        kq.insert(KeyEvent {action: KeyAction::KeyReleased, cycle:500, key:Key::Left});

        println!("----- Test 3");
        // 100 right pressed
        // 123 escape pressed
        // 300 left pressed
        // 400 right released
        // 500 left released
        assert!( kq.key_currently_pressed_at(550).unwrap().key == Key::Escape);
        println!("----- Test 4");
        assert!( kq.key_currently_pressed_at(450).unwrap().key == Key::Left);
        println!("----- Test 5");
        assert!( kq.key_currently_pressed_at(350).unwrap().key == Key::Left);
        println!("----- Test 6");
        assert!( kq.key_currently_pressed_at(150).unwrap().key == Key::Escape);
        println!("----- Test 7");
        assert!( kq.key_currently_pressed_at(110).unwrap().key == Key::Right);
        println!("----- Test 8");
        assert!( kq.key_currently_pressed_at(50).is_none());

    }

    #[test]
    fn test_clear() {
        let mut kq = KeyboardQueue::new();
        kq.insert(KeyEvent {action: KeyAction::KeyPressed, cycle:10, key:Key::Escape});
        kq.insert(KeyEvent {action: KeyAction::KeyPressed, cycle:20, key:Key::Left});
        kq.insert(KeyEvent {action: KeyAction::KeyReleased, cycle:30, key:Key::Left});
        kq.insert(KeyEvent {action: KeyAction::KeyReleased, cycle:40, key:Key::Escape});

        kq.clear_up_to(35);
        kq.print();
        assert!(kq.key_events.len() == 2);
        assert!(kq.key_events[0].key == Key::Escape);
        assert!(kq.key_events[1].key == Key::Escape);

        kq.clear_up_to(0);
        assert!(kq.key_events.len() == 2, "Should have done nothing");

        kq.clear_up_to(100);
        assert!(kq.key_events.len() == 0, "Should have deleted everything");

   }
}