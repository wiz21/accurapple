use core::default::Default;
use std::borrow::BorrowMut;
use accurashader::wgpu_render;
use crate::a2::{hgr_address, EMULATOR_SHORT_DESCRIPTION};
use crate::wozpack::subtrack_str;
use egui::text::LayoutJob;
use egui::{Button, ColorImage, Context, Stroke, TextBuffer};
use emath::{Pos2, Vec2b};
use egui::{
    self, Color32,  FontFamily, FontId, FontSelection, Rect, RichText, TextEdit,
    TextFormat, TextStyle, Ui, WidgetText,
};
//use egui::context::Context;
//use egui_file::{FileDialog, Filter};
use egui_file_dialog::{FileDialog, DialogMode};

const BASE_SYMBOLS: &str = include_str!("../additional/default_symbols.txt");



#[derive(PartialEq, Clone)]
pub enum CRTColourBurstInterpretation { FullScreenMono, LineByLine}



struct FileDialogfilter {
    extension: String,
    filter: Arc<dyn Fn(&Path) -> bool + Send + Sync>
}


struct MyFileDialog {
    dialog: FileDialog,
    is_open: bool,
    current_path: Option<PathBuf>
}

impl MyFileDialog {
    fn new( accepted_extensions: Vec<FileDialogfilter>) -> MyFileDialog {

        let mut d = FileDialog::new()
            .show_devices(true)
            .show_places(false)
            .show_left_panel(true)
            .default_pos(Pos2::from((100.0, 100.0)));

        for ext in accepted_extensions {
            d = d.add_file_filter( &ext.extension, ext.filter);
        };

        MyFileDialog {
            dialog : d,
            is_open: false,
            current_path: None
        }
    }

    fn activate(&mut self, _ctx: &Context) {
        if let Some(path) = &self.current_path {
            if let Some(dir) = path.parent() {
                self.dialog.config_mut().initial_directory = dir.to_path_buf();
                //self.dialog = self.dialog.initial_directory(dir.to_path_buf());
            }
        }
        let _ = self.dialog.open(DialogMode::SelectFile, true, None);
        self.is_open = true;
    }

    fn ui(&mut self, ctx: &egui::Context) -> Option<&Path> {

        let r = self.dialog.update(ctx).selected();

        if r.is_some() {
            self.is_open = false;
            self.current_path = Some(r.unwrap().to_path_buf());
        }

        r
    }
}


use image::{DynamicImage};
use indexmap::{IndexMap};
use itertools::Itertools;
use lazy_static::lazy_static;
use log::{debug, error, info, trace, warn, Level, Metadata, Record};
use serde_json::{Value};
use thousands::Separable;
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::{fs};
use std::str::FromStr;

use crate::a2::{CYCLES_PER_FRAME, CYCLES_PER_LINE};
use crate::wgpu_render::{CompositeToRGBUniform, RAMToCompositeUniform};
use crate::filtering::{filters, SIGNAL_14M_FREQ, NTSC_CARRIER_FREQ};
use crate::configuration::{Configuration};

lazy_static! {
    static ref MOBY_CRC_TO_GAME_ID: HashMap<u32, u32> = {
        let mut m = HashMap::new();
        let a = include_bytes!("../data/moby_keys.bin");
        for i in (0..a.len()).step_by(2 * 4) {
            let dsk_crc32 = u32::from_le_bytes(a[i..i + 4].try_into().unwrap());
            let moby_game_id: u32 = u32::from_le_bytes(a[i + 4..i + 8].try_into().unwrap());
            m.insert(dsk_crc32, moby_game_id);
        }
        m
    };
}

type MobyIdToUrlType = HashMap<u32, String>;
lazy_static! {
    static ref MOBY_GAME_ID_TO_COVER_URL: MobyIdToUrlType = {
        const JSON: &str = include_str!("../data/covers_url.json");
        serde_json::from_str(JSON).unwrap()
    };
}


fn download_image(url_str: &str) -> Option<DynamicImage> {
    info!("Downloading {}", url_str);
    //let url_str = image.as_str().unwrap();
    let r = reqwest::blocking::get(url_str);
    if let Ok(reply) = r {
        //println!("{}", reply.bytes().unwrap().len());
        use image::io::Reader;
        use std::io::Cursor;

        let reader =
            Reader::new(Cursor::new(reply.bytes().ok().unwrap()))
                .with_guessed_format()
                .expect("Cursor io never fails");
        // if let Some(fmt) = reader.format() {
        //     for s in fmt.extensions_str() {
        //         println!("Extension: {}", s);
        //     }
        // }
        if let Ok(imgbytes) = reader.decode() {
            info!("Success");
            return Some(imgbytes.resize(imgbytes.width()/3, imgbytes.height()/3, image::imageops::FilterType::Nearest));
        } else {
            error!("Can't decode Moby's cover picture data into an image");
            return None;
        }
    } else {
        error!("Can't open URL {}", url_str);
        return None;
    }
}

#[derive(PartialEq)]
pub enum ScreenLabels {
    Log,
    Debugger,
    Memory,
    GraphicsMemory,
    Presentation,
    Documentation,
    Control,
}

#[derive(Clone, Eq, Hash, PartialEq)]
pub enum SymbolFlags {
    Jump,
    ByteWatch,
    WordWatch,
}

pub struct MyImage {
    texture: Option<egui::TextureHandle>,
    pixels: Option<Vec<u8>>,
    size: [usize; 2],
}

impl MyImage {
    pub fn new() -> MyImage {
        MyImage {
            texture: None,
            pixels: None,
            size: [0_usize, 0_usize],
        }
    }

    pub fn clear_image(&mut self) {
        self.size = [0, 0];
        self.pixels = None;
        self.texture = None;
    }

    pub fn set_hgr_image(&mut self, pixels:&[u8]) {
        self.size = [560, 192*2];
        let mut image = vec![0; 560*192*2*4];

        for y in 0..192 {
            let addr = hgr_address(y);
            let dest = y*560*2*4;
            for x in 0..40 {
                let mut hgr_byte = pixels[addr+x];
                for i in 0..7 {
                    let dest_addr = dest+(x*7+i)*4*2;
                    image[dest_addr ] = (hgr_byte & 1)*255;
                    image[dest_addr + 1 ] = (hgr_byte & 1)*255;
                    image[dest_addr + 2 ] = (hgr_byte & 1)*255;
                    image[dest_addr + 3 ] = (hgr_byte & 1)*255;
                    image[dest_addr + 4 ] = image[dest_addr ];
                    image[dest_addr + 5 ] = image[dest_addr + 1 ];
                    image[dest_addr + 6 ] = image[dest_addr + 2 ];
                    image[dest_addr + 7 ] = image[dest_addr + 3 ];
                    hgr_byte >>= 1;
                }
            }

            image.copy_within(dest..dest+560*4, dest+560*4);
        }

        self.pixels = Some(image);
    }


    pub fn set_dhgr_image(&mut self, ram:&[u8], aux:&[u8]) {
        self.size = [560, 192*2];
        let mut image = vec![0u8; 560*192*2*4];

        for y in 0..192 {
            let addr = hgr_address(y);
            let line_ofs = y*560*2*4;

            for x in 0..40 {
                let mut aux_byte = aux[addr+x];
                for i in 0..7 {
                    let dest_addr = line_ofs+(x*14+i)*4;
                    image[dest_addr ] = (aux_byte & 1)*255;
                    image[dest_addr + 1 ] = (aux_byte & 1)*255;
                    image[dest_addr + 2 ] = (aux_byte & 1)*255;
                    image[dest_addr + 3 ] = (aux_byte & 1)*255;
                    aux_byte >>= 1;
                }

                let mut ram_byte = ram[addr+x];
                for i in 0..7 {
                    let dest_addr = line_ofs+(x*14+7+i)*4;
                    image[dest_addr ] += (ram_byte & 1)*255;
                    image[dest_addr + 1 ] += (ram_byte & 1)*255;
                    image[dest_addr + 2 ] += (ram_byte & 1)*255;
                    image[dest_addr + 3 ] += (ram_byte & 1)*255;
                    ram_byte >>= 1;
                }
            }

            image.copy_within(line_ofs..line_ofs+560*4, line_ofs+560*4);
        }

        self.pixels = Some(image);
    }


    pub fn set_image(&mut self, img: DynamicImage) {
        self.size = [img.width() as usize, img.height() as usize];
        let image_buffer = img.to_rgba8();
        let buf = image_buffer.into_vec();
        assert_eq!(self.size[0] * self.size[1] * 4, buf.len());
        self.pixels = Some(buf);
        /* let pixels: Vec<_> = pixels
        .chunks_ex


        act(4)
        .map(|p| egui::Color32::from_rgba_unmultiplied(p[0], p[1], p[2], p[3]))
        .collect(); */
    }

    fn ui(&mut self, ui: &mut egui::Ui) {
        if let Some(pix) = self.pixels.take() {
            self.texture = Some(ui.ctx().load_texture(
                "moby",
                ColorImage::from_rgba_unmultiplied(self.size, &pix),
                Default::default(),
            ));
        }

        if let Some(texture) = self.texture.as_ref() {
            // Show the image:
            ui.image(texture);
        }
    }
}

#[derive(Clone)]
pub struct Symbol {
    pub symbol: String,
    pub address: u16,
    pub flags: HashSet<SymbolFlags>,
    pub old_value: usize,
    highlighted: bool,
}

impl Symbol {
    fn read_from(&self, mem: &Vec<u8>) -> Option<usize> {
        if self.flags.contains(&SymbolFlags::WordWatch) {
            Some(
                (mem[(self.address + 1) as usize] as usize) * 256
                    + mem[self.address as usize] as usize,
            )
        } else if self.flags.contains(&SymbolFlags::ByteWatch) {
            Some(mem[self.address as usize] as usize)
        } else {
            None
        }
    }

    fn update_value(&mut self, mem: &Vec<u8>, highlight: bool) {
        if let Some(v) = self.read_from(mem) {
            if highlight {
                self.highlighted = v != self.old_value;
            }
            self.old_value = v;
        }
    }
}


use crate::a2::{GfxBaseMode, MemoryBankType, ModeInfo, BANKS_OF_INTEREST, OFF};
use crate::emulator::{Command, CpuDebugState, CpuType, JumpTrace, MonoColourSwitch};
use crate::generic_peripheral::IRQRequest;
use circular_queue::CircularQueue;
use std::path::{Path, PathBuf};
use std::sync::mpsc::{Receiver, Sender};
use std::sync::{Arc, Mutex};
//use crate::debugger::{DebugFrame,MyLogRecord,render_cpu_state,BANKS_OF_INTEREST};
use crate::all6502::{CpuSnaphsot, CpuEmulator};
use crate::m6502::{
    OPCODE_JSR,
};
use crate::rs6502::disassembler::Disassembler;
use crate::emulator::Timers;

pub struct DebugFrame {
    pub floppy_disk_name: Option<PathBuf>,
    pub floppy_disk_checksum: Option<u32>,
    pub floppy_track: Option<usize>,
    pub stack_markers: Vec<bool>,
    pub jsr_stack: Vec<JumpTrace>,
    pub trace: Vec<CpuDebugState>,
    pub slots_status: Vec<String>,
    pub lss_debug: String,
    pub cycles: u64,
    pub active_mem_copy: Option<Vec<u8>>,
    pub ram_copy: Option<Vec<u8>>,
    pub auxram_copy: Option<Vec<u8>>,
    pub cpu_snapshot: CpuSnaphsot,
    pub cpu_emulator: CpuEmulator,
    pub planned_irqs: Vec<IRQRequest>,
    pub irq_vector: u16,
    pub last_irq_delay: usize,
    pub gfx_mode_info: ModeInfo,
    pub gfx_base_mode: GfxBaseMode,
    pub last_vblank_read: u64,
    pub vbl_bit: usize,
	pub cycle_in_frame: usize,
    pub active_memory_bank: MemoryBankType,
    pub floating_addr: Option<usize>,
    pub memory_banks: [MemoryBankType; 16],
    pub memory_banks_for_write: [MemoryBankType; 16],
    pub hramrd: bool,
    pub break_point_triggered: bool,
    pub breakpoints: String,
    pub memory_flags: String,
    // Force the debugger view to appear on the debugger view
    // (used when one is doing a pause on the emulator).
    pub present_debugger: bool,
    pub timers: Timers,
    pub take_screenshot: bool,
    pub gfx_debug_line: Option<u32>
}

impl DebugFrame {
    pub fn new(cpu: &CpuSnaphsot) -> DebugFrame {
        DebugFrame {
            floppy_disk_name: None,
            floppy_disk_checksum: None,
            floppy_track: None,
            jsr_stack: Vec::new(),
            trace: Vec::new(),
            stack_markers: Vec::new(),
            slots_status: Vec::new(),
            lss_debug: "".to_string(),
            cycles: 0,
            active_mem_copy: None,
            ram_copy: None,
            auxram_copy: None,
            cpu_snapshot: *cpu,
            cpu_emulator: CpuEmulator::None,
            planned_irqs: Vec::new(),
            irq_vector: 0,
            last_irq_delay: 0,
            gfx_mode_info: ModeInfo {
                col80: OFF,
                store80: OFF,
                page2: OFF,
                text: OFF,
                mixed: OFF,
                hires: OFF,
                dhires: OFF,
            },
            gfx_base_mode: GfxBaseMode::Text40,
            last_vblank_read: 0,
            vbl_bit: 0,
			cycle_in_frame: 0,
            active_memory_bank: MemoryBankType::Motherboard,
            floating_addr: None,
            memory_banks: [MemoryBankType::Motherboard; 16],
            memory_banks_for_write: [MemoryBankType::Motherboard; 16],
            hramrd: false,
            break_point_triggered: false,
            breakpoints: String::new(),
            memory_flags: String::new(),
            present_debugger: false,
            timers: Timers::new(),
            take_screenshot: false,
            gfx_debug_line: None
        }
    }
}

// pub fn render_cpu_state(cpu: &CpuSnaphsot) -> String {
//     format!(
//         "SP=01{:02X} A={:02X}, X={:02X}, Y={:02X} flags:{}{}{}{}{}{}{}{}",
//         cpu.sp,
//         cpu.a,
//         cpu.x,
//         cpu.y,
//         if cpu.flags & M6502_NF != 0 { "N" } else { "-" },
//         if cpu.flags & M6502_VF != 0 { "V" } else { "-" },
//         if cpu.flags & M6502_XF != 0 { "X" } else { "-" },
//         if cpu.flags & M6502_BF != 0 { "B" } else { "-" },
//         if cpu.flags & M6502_DF != 0 { "D" } else { "-" },
//         if cpu.flags & M6502_IF != 0 { "I" } else { "-" },
//         if cpu.flags & M6502_ZF != 0 { "Z" } else { "-" },
//         if cpu.flags & M6502_CF != 0 { "C" } else { "-" }
//     )
// }

use regex::Regex;

fn parse_addresses_string(
    contents: &str,
    symbols: &mut IndexMap<u16, Symbol>,
) {
    // "Address Symbol"
    let re = Regex::new(       r"^\s*([0-9A-Fa-f]{1,4})\s+([^:]+)\s*(:V?T?W?)?.*$").unwrap();
    // "Symbol = $address"
    let SYMBOL_RE: Regex = Regex::new(r"^\s*(\S+)\s*=\s*\$?([0-9A-Fa-f]{1,4})\s*(:V?T?W?)?.*$").unwrap();
    for (ndx, line) in contents.lines().enumerate() {

		if line.trim_start().starts_with(';') || line.trim_start().starts_with('#') {
			continue
		}

        let (mut symbol, flags_str) = if let Some(cap) = re.captures(line) {
            (
                Some(Symbol {
                    symbol: cap.get(2).unwrap().as_str().trim().to_string(),
                    address: u16::from_str_radix(&cap[1], 16)
                        .expect("Expecting a U16 in alphanumeric representation"),
                    flags: HashSet::new(),
                    old_value: 0,
                    highlighted: false,
                }),
                cap.get(3),
            )
        } else if let Some(cap) = SYMBOL_RE.captures(line) {
            (
                Some(Symbol {
                    symbol: cap.get(1).unwrap().as_str().trim().to_string(),
                    address: u16::from_str_radix(cap.get(2).unwrap().as_str(), 16)
                        .expect("Expecting a U16 in alphanumeric representation"),
                    flags: HashSet::new(),
                    old_value: 0,
                    highlighted: false,
                }),
                cap.get(3),
            )
        } else {
            (None, None)
        };

        if let Some(s) = &mut symbol {
            //println!("${:04X}: '{}'",s.address, s.symbol);

            if let Some(flags) = flags_str {
                if flags.as_str().contains('V') {
                    s.flags.insert(SymbolFlags::ByteWatch);
                }
                if flags.as_str().contains('W') {
                    s.flags.insert(SymbolFlags::WordWatch);
                }
                if flags.as_str().contains('T') {
                    s.flags.insert(SymbolFlags::Jump);
                }
            }
            debug!("Adding symbol {}={}",s.symbol,s.address);
            symbols.insert(s.address, s.clone()); // FIXME This clone is ugly.
        } else if !line.trim().is_empty() && !line.trim().starts_with('#') {
            error!(
                "Could not parse line {}: '{}' in symbols file",
                ndx + 1,
                line
            )
        }
    }
}


pub fn load_symbols(file_path: &PathBuf, symbols: &mut IndexMap<u16, Symbol>) {
    if file_path.is_file() {
        info!("Loading symbols file {}", file_path.to_string_lossy());
        let contents =
            fs::read_to_string(file_path).expect("Should have been able to read the file");

        symbols.clear();
        parse_addresses_string(BASE_SYMBOLS, symbols);
        parse_addresses_string(&contents, symbols);
        info!("Loaded symbol file {}", file_path.as_os_str().to_string_lossy());
    } else {
        error!("{} not found", file_path.display());
    }
}

#[derive(Clone)]
pub struct MyLogRecord {
    pub module: String,
    pub message: String,
    pub level: Level,
}

impl MyLogRecord {
    fn new(r: &Record) -> MyLogRecord {
        MyLogRecord {
            module: r.module_path().unwrap().to_string(),
            message: format!("{}", r.args()),
            level: r.level(),
        }
    }
}

pub struct AccurappleSimpleLogger {
    //pub records : Arc<Mutex<CircularQueue<String>>>,
    pub records: Arc<Mutex<CircularQueue<MyLogRecord>>>,
}

impl log::Log for AccurappleSimpleLogger {
    // Don't forget that the maximum log level is set during log construction
    // with log::set_max_level(LevelFilter::...)

    fn enabled(&self, _metadata: &Metadata) -> bool {
        true // metadata.level() <= Level::Trace
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let mut rec = self.records.lock().expect("");
            // Level::Warn <= Level::Info <= Level::Debug
            if record.module_path().is_some() && record.level() <= Level::Debug {
                let module_path = record.module_path().unwrap();
                //println!("{}", module_path);
                if module_path.starts_with("accurapple") {
                    rec.push(MyLogRecord::new(record));
                }
            }
        }
    }

    fn flush(&self) {}
}

pub struct Debugger {
    moby_cover: RefCell<MyImage>,
    hgr1: RefCell<MyImage>,
    hgr2: RefCell<MyImage>,
    dhgr1: RefCell<MyImage>,
    dhgr2: RefCell<MyImage>,
    floppy1_file: RefCell<Option<PathBuf>>,
    floppy1_checksum: Option<u32>,
    symbols_file: RefCell<Option<PathBuf>>,
    file_dialog: RefCell<Option<MyFileDialog>>,
    sym_file_dialog: RefCell<Option<MyFileDialog>>,
    control_channel: Sender<Command>,
    debug_stream_rx: Receiver<DebugFrame>,
    log_lines: Arc<Mutex<CircularQueue<MyLogRecord>>>,
    user_input: String,
    command_result: String,
    last_valid_user_input: String,
    current_debug_frame: Option<DebugFrame>,
    debugger_active: bool,
    pub current_screen: ScreenLabels,
    available_lines: usize,
    available_columns: usize,
    available_regular_lines: usize,
    mem_dump_offset: u16,
    log_back_lines: isize,
    disassembly_offset: u16,
    page_up_offset: u16,
    page_down_offset: u16,
    labels: RefCell<IndexMap<u16, Symbol>>,
    memview_offset: isize,
    bytes_per_line: usize,
    last_address: u16,
    mono_em_space: f32,
    mono_font_height: f32,
    regular_font_height: f32,
    cycle_base: u64,
    frame_count: usize,
    test: RefCell<IndexMap<u16, Symbol>>,
    recenter_disassembly_if_needed: bool,
    plan_recenter_disassembly_if_needed: bool,
    skip_watches_highlight_once: bool,
    resync_disassembler: bool,
    pub sw_clock_modulo: f32,
    pub phase0_offset: f32,
    luma_freq: f32,
    chroma_freq: f32,
    pub flash: f32,
    pub filters: Vec<f32>,
    pub take_screenshot: bool,
    pub take_screenshot_crt: bool,
    pub composite_to_rgb_uniform: CompositeToRGBUniform,
    pub ram_to_composite_uniform: RAMToCompositeUniform,
    pub rgb_to_host_uniform: wgpu_render::RGBToHostShaderConstants,
    pub crt_colourburst_interpretation: CRTColourBurstInterpretation,
    motherboard_mono_colour_switch: MonoColourSwitch,
    frame_durations: Vec<f32>,
    emu_frame_durations: Vec<f32>,
    irq_lines_enabled: bool,
    configuration: Configuration,
    power_debug: bool
}

fn addr_to_label(labels_map: &RefCell<IndexMap<u16, Symbol>>, addr: &u16) -> String {
    if let Some(label) = labels_map.borrow().get(addr) {
        label.symbol.clone()
    } else {
        format!("${addr:04X}")
    }
}

fn is_address_symbol(labels_map: &RefCell<IndexMap<u16, Symbol>>, address: &u16) -> bool {
    let labels = labels_map.borrow();
    let pre_address = address.saturating_sub(1);

    let is_word = if let Some(sym) = labels.get(&pre_address) {
        sym.flags.contains(&SymbolFlags::WordWatch)
    } else {
        false
    };

    labels.contains_key(address) || (labels.contains_key(&pre_address) && is_word)
}

impl Debugger {
    pub fn new(
        control_channel: Sender<Command>,
        debug_stream_rx: Receiver<DebugFrame>,
        log_lines: Arc<Mutex<CircularQueue<MyLogRecord>>>,
        symbols_file: Option<PathBuf>,
        configuration: Configuration,
        power_debug: bool
    ) -> Debugger {
        let _filter = Box::new(|f: &Path| {
            if let Some(ext) = f.extension() {
                    ext.eq_ignore_ascii_case("woz")
                    || ext.eq_ignore_ascii_case("dsk")
                    || ext.eq_ignore_ascii_case("po")
                    || ext.eq_ignore_ascii_case("do")
            } else {
                false
            }
        });
        // let path = PathBuf::from_str(".").ok().unwrap();
        // let mut dialog = FileDialog::open_file(Some(path))
        //     .show_files_filter(filter)
        //     .show_rename(false)
        //     .show_new_folder(false);
        // dialog.open();


        let mut dbg = Debugger {
            moby_cover: RefCell::new(MyImage::new()),
            hgr1: RefCell::new(MyImage::new()),
            hgr2: RefCell::new(MyImage::new()),
            dhgr1: RefCell::new(MyImage::new()),
            dhgr2: RefCell::new(MyImage::new()),
            floppy1_file: RefCell::new(None),
            floppy1_checksum: None,
            file_dialog: RefCell::new(Some(MyFileDialog::new( vec![
                FileDialogfilter {
                    extension: "Floppy disks".to_string(),
                    filter: Arc::new(|p:&Path| {
                        let ext = p.extension().unwrap_or_default();
                        ext.eq_ignore_ascii_case("woz") || ext.eq_ignore_ascii_case("dsk") || ext.eq_ignore_ascii_case("do")
                    })
                }
            ] ))),
            sym_file_dialog: RefCell::new(Some(MyFileDialog::new(vec![
                FileDialogfilter {
                    extension: "Symbols files".to_string(),
                    filter: Arc::new(|p:&Path| {
                        let ext = p.extension().unwrap_or_default();
                        ext.eq_ignore_ascii_case("txt")
                    })
                }
            ]))),
            control_channel,
            debug_stream_rx,
            log_lines,
            user_input: "".to_string(),
            command_result: "".to_string(),
            last_valid_user_input: "".to_string(),
            current_debug_frame: None,
            debugger_active: false,
            current_screen: ScreenLabels::Presentation,
            available_lines: 10,
            available_columns: 16,
            available_regular_lines: 10,
            mem_dump_offset: 0,
            log_back_lines: 0,
            disassembly_offset: 0x800,
            page_up_offset: 0x800,
            page_down_offset: 0x800,
            labels: RefCell::new(IndexMap::new()),
            memview_offset: 0,
            bytes_per_line: 16,
            last_address: 0xF000,
            mono_em_space: 10.0,
            mono_font_height: 14.0,
            regular_font_height: 14.0,
            cycle_base: 0,
            frame_count: 0,
            test: RefCell::new(IndexMap::new()),
            recenter_disassembly_if_needed: true,
            plan_recenter_disassembly_if_needed: false,
            skip_watches_highlight_once: false,
            resync_disassembler: true,
            symbols_file: RefCell::new(symbols_file.clone()),
            sw_clock_modulo: 13.0,
            phase0_offset: 12.0,
            luma_freq: NTSC_CARRIER_FREQ,
            chroma_freq: 350000.0f32,
            flash: 0.0,
            filters: vec![0.0; 256*4],
            take_screenshot: false,
            take_screenshot_crt: false,
            composite_to_rgb_uniform : CompositeToRGBUniform::new(),
            ram_to_composite_uniform : RAMToCompositeUniform::new(),
            rgb_to_host_uniform: wgpu_render::RGBToHostShaderConstants::new(),
            crt_colourburst_interpretation: CRTColourBurstInterpretation::FullScreenMono,
            motherboard_mono_colour_switch: MonoColourSwitch::Colour,
            frame_durations: Vec::new(),
            emu_frame_durations: Vec::new(),
            irq_lines_enabled: false,
            configuration,
            power_debug
        };

        let configuration = Configuration::load();
        if let Some(composite) = configuration.display_composite {
            dbg.composite_to_rgb_uniform.composite = composite as i32;
        }
        if let Some(phase) = configuration.display_phase {
            dbg.composite_to_rgb_uniform.phase = phase;
        }

        dbg.filters[0] = 1.0;
        dbg.filters[1] = 1.0;
        dbg.filters[2] = 1.0;
        dbg.filters[3] = 1.0;

        filters(SIGNAL_14M_FREQ, dbg.luma_freq, dbg.chroma_freq, dbg.composite_to_rgb_uniform.luma_filter_size, &mut dbg.filters);

        if let Some(symfile) = symbols_file {
            load_symbols(&symfile, &mut dbg.labels.borrow_mut());
        } else {
            debug!("Parsing default symbols");
            parse_addresses_string(BASE_SYMBOLS, &mut dbg.labels.borrow_mut());
        }

        dbg
    }

    // #[allow(unused_variables)]
    // pub fn input(&mut self, event: &WindowEvent) -> bool {
    //     match event {
    //         WindowEvent::KeyboardInput {
    //             device_id,
    //             event,
    //             is_synthetic,
    //         } => {

    //             if event.state == ElementState::Pressed {
    //                 match event.logical_key {
    //                     Key::Named(NamedKey::PageUp) => {
    //                         match self.current_screen {
    //                             ScreenLabels::Debugger => self.scroll_disassembly_view(-1),
    //                             ScreenLabels::Memory => self.scroll_mem_view(-1),
    //                             ScreenLabels::Log => self.scroll_log_view(-1),
    //                             _ => (),
    //                         }
    //                     },
    //                     Key::Named(NamedKey::PageDown) => {
    //                         match self.current_screen {
    //                             ScreenLabels::Debugger => self.scroll_disassembly_view(1),
    //                             ScreenLabels::Memory => self.scroll_mem_view(1),
    //                             ScreenLabels::Log => self.scroll_log_view(1),
    //                             _ => (),
    //                         }
    //                     },
    //                     Key::Named(NamedKey::F10) => {
    //                         if let Some(d) = self.current_debug_frame.as_ref() {
    //                             //check_visible_line_disasm = true;
    //                             let pc = d.cpu_snapshot.ad;
    //                             if let Some(mem) = d.mem_copy.as_ref() {
    //                                 if mem[pc as usize] == OPCODE_JSR {
    //                                     self.control_channel
    //                                         .send(Command::BreakAtPC(pc + 3))
    //                                         .expect("Send should work");
    //                                 } else {
    //                                     self.control_channel
    //                                         .send(Command::RunNInstr(1))
    //                                         .expect("Send should work");
    //                                 }
    //                             }
    //                         }
    //                     }
    //                     Key::Named(NamedKey::F1) => {
    //                         if self.debugger_active == false {
    //                             self.control_channel
    //                                 .send(Command::EnterDebugger)
    //                                 .expect("Send should work");
    //                             self.debugger_active = true;

    //                             self.resync_disassembler = true;

    //                             if let Some(df) = self.current_debug_frame.as_ref() {
    //                                 self.disassembly_offset = 0;
    //                                 self.page_up_offset = df.cpu_snapshot.ad;
    //                                 self.page_down_offset = df.cpu_snapshot.ad;
    //                                 self.plan_recenter_disassembly_if_needed = true;
    //                                 self.skip_watches_highlight_once = true;

    //                                 //println!("self.disassembly_offset {:X}", self.disassembly_offset);
    //                             }

    //                             if self.current_screen != ScreenLabels::Memory {
    //                                 self.current_screen = ScreenLabels::Debugger;
    //                             }
    //                         } else {
    //                             self.current_screen = ScreenLabels::Log;
    //                             self.control_channel
    //                                 .send(Command::Run)
    //                                 .expect("Send should work");
    //                             self.debugger_active = false;
    //                         }
    //                     }
    //                     Key::Named(NamedKey::F3) => {
    //                         //check_visible_line_disasm = true;
    //                         self.control_channel
    //                             .send(Command::RunNInstr(1))
    //                             .expect("Send should work");
    //                         self.plan_recenter_disassembly_if_needed = true;
    //                     }
    //                     _ => ()
    //                 };
    //             }
    //         },
    //         _ => ()
    //     }
    //     true
    // }

    fn is_debugger_active(&self) -> bool {
        if let Some(df) = self.current_debug_frame.as_ref() {
            df.active_mem_copy.is_some()
        } else {
            false
        }
    }

    pub fn current_cycle(&self) -> u64 {
        if let Some(df) = self.current_debug_frame.as_ref() {
            df.cycles
        } else {
            0
        }
    }

    pub fn consume_available_debug_frames(&mut self) {
        loop {
            let f2 = self.debug_stream_rx.try_recv();
            if f2.is_ok() {
                trace!("Debug frame received");
                let frame = Some(f2.ok().unwrap());


                self.current_debug_frame = frame;
                self.frame_count += 1;
                let df = self.current_debug_frame.as_ref().unwrap();

                if let Some(line) = df.gfx_debug_line {
                    self.rgb_to_host_uniform.line_to_debug = line;
                }

                let moby_api_key = &self.configuration.moby_api_key;

                if let Some(new_checksum) = df.floppy_disk_checksum {
                    if let Some(old_checksum) = self.floppy1_checksum {
                        if new_checksum != old_checksum {
                            //println!("{:X} {:X}",self.floppy1_checksum, df.floppy_disk_checksum);
                            self.reload_moby_cover(new_checksum, moby_api_key);
                            self.floppy1_checksum = Some(new_checksum);
                        }
                    } else {
                        self.reload_moby_cover(new_checksum, moby_api_key);
                        self.floppy1_checksum = Some(new_checksum);
                    }
                } else {
                    self.floppy1_checksum = None;
                    self.moby_cover.borrow_mut().clear_image();
                }

                // Resync occurs when we enter the debugger in an
                // unscheduled manner. For example when a breakpoint
                // occur.
                if let Some(mem) = df.active_mem_copy.as_ref() {
                    self.debugger_active = true;
                    if df.break_point_triggered || df.present_debugger {
                        self.current_screen = ScreenLabels::Debugger;
                        trace!("Forcing current screen");
                    }
                    self.recenter_disassembly_if_needed = true;

                    for (_addr, symbol) in (*self.labels.borrow_mut()).iter_mut() {
                        symbol.update_value(mem, !self.skip_watches_highlight_once);
                    }
                    self.skip_watches_highlight_once = false;

                }
                if let Some(ram) = df.ram_copy.as_ref() {
                    self.hgr1.borrow_mut().set_hgr_image(&ram[0x2000..0x4000]);
                    self.hgr2.borrow_mut().set_hgr_image(&ram[0x4000..0x6000]);

                    if let Some(aux_ram) = df.auxram_copy.as_ref() {
                        self.dhgr1.borrow_mut().set_dhgr_image(&ram[0x2000..0x4000], &aux_ram[0x2000..0x4000]);
                        self.dhgr2.borrow_mut().set_dhgr_image(&ram[0x4000..0x6000], &aux_ram[0x4000..0x6000]);
                    }
                }

            } else {
                break;
            }
        }
    }

    fn render_log(&mut self, ui: &mut Ui) {
        let v = self.log_lines.lock().expect("");

        let al: usize = self.available_regular_lines;
        let lb = if v.len() <= al {
            // We can fit all the log lines on the screen
            self.log_back_lines = 0;
            self.log_back_lines as usize
        } else if v.len() - self.log_back_lines as usize >= al {
            // If I remove self.log_back_lines from the tail of the
            // log, then the rest fits in the screen
            self.log_back_lines as usize
        } else {
            self.log_back_lines = (v.len() - al) as isize;
            self.log_back_lines as usize
        };
        //println!("al={} lb={} v.len()={}", al, lb, v.len());

        //println!("al:{} lb:{} vlen={}", al, lb, v.len());
        let iter = v.iter().skip(lb);
        let lls: Vec<MyLogRecord> = iter.take(al).cloned().collect();
        let mono_id = TextStyle::Body.resolve(&ui.ctx().style());

        for line in lls.iter().rev() {
            let mut job = LayoutJob::default();
            job.append(
                line.level.as_str(),
                0.0,
                TextFormat {
                    color: match line.level {
                        Level::Error => Color32::RED,
                        Level::Warn => Color32::YELLOW,
                        _ => Color32::GRAY,
                    },
                    font_id: mono_id.clone(),
                    ..Default::default()
                },
            );
            job.append(
                line.module.as_str(),
                4.0, // Some horizontal space
                TextFormat {
                    ..Default::default()
                },
            );
            job.append(
                line.message.as_str(),
                4.0,
                TextFormat {
                    color: Color32::WHITE,
                    ..Default::default()
                },
            );

            ui.add(egui::Label::new(job));
        }

        /*let l: Vec<Spans> = lls.iter().rev()
            .map(|line| Spans::from(Span::raw(
        format!("{}:{}:{}", line.level, line.module, line.message))))
            .collect();*/
    }

    fn render_slots(&self, ui: &mut Ui) {
        self.render_slots_as_text(ui)

        /* egui::Grid::new("some_unique_id").show(ui, |ui| {
            ui.vertical(|ui| {
                ui.set_width_range(500.0..=1000.0);
                self.render_slots_as_text(ui)
            });
            ui.horizontal(|ui| { ui.label("Mockingboar") });
            ui.end_row();
        }); */
    }

    fn render_slots_as_text(&self, ui: &mut Ui) {
        if let Some(df) = self.current_debug_frame.as_ref() {
            for s in &df.slots_status {
                ui.add(egui::Label::new(s).wrap());
            }
            ui.label(&df.breakpoints);
            ui.label(format!(
                "GfxMode:{} MemFlags:{}",
                df.gfx_base_mode,
                df.memory_flags
            ));

            ui.label(format!(
                "Cycle in frame: frame {} line {} offset {}, cycles left: {} vbl_bit:{}",
                df.cycles / (CYCLES_PER_FRAME as u64),
                df.cycle_in_frame / CYCLES_PER_LINE,
                df.cycle_in_frame % CYCLES_PER_LINE,
                CYCLES_PER_FRAME - df.cycle_in_frame,
				if df.vbl_bit == 0 {
					"Blanking"
				} else {
					"Redraw"
				}
            ));
            ui.label(format!(
                "IRQ: {} planned, {}",
                df.planned_irqs.len(),
                if !df.planned_irqs.is_empty() {
                    df.planned_irqs
                        .iter()
                        .map(|pi| {
                            format!(
                                "Slot #{}/{:x} in +{} c. ({})",
                                pi.source_slot, pi.sub_source,
                                pi.irq_cycle - df.cycles,
                                if pi.trigger_cpu {
                                    "IRQ"
                                } else {
                                    "Service"
                                }
                            )
                        })
                        .join(", ")
                } else {
                    String::from_str("none").unwrap()
                }
            ));


            ui.label(format!("[{}] SP: ${:04X}", df.cpu_emulator, df.cpu_snapshot.sp));
        }
    }

    fn read_mouse_scroll_delta(&self, ui: &Ui, rect: Rect) -> isize {
        let mut wheel_delta = 0;
        if ui.rect_contains_pointer(rect) {
            ui.ctx().input(|ctx| {
                for event in &ctx.events {
                    match event {
                        egui::Event::MouseWheel { unit: _, delta, modifiers: _ } => {
                            //println!("MouseWheel");
                            if delta[1] > 0.0 {
                                wheel_delta = 1;
                            } else if delta[1] < 0.0 {
                                wheel_delta = -1;
                            };
                        }
                        _ => ()
                    }
                }
            })
        };

        wheel_delta
    }

    fn read_mouse_scroll(&self, ui: &Ui, rect: Rect, value: isize) -> isize {
        let d = self.read_mouse_scroll_delta(ui, rect);
        if (d < 0 && value > 0) || (d > 0) {
            value + d
        } else {
            value
        }
    }

    pub fn handle_keyboard(&mut self, key: &egui::Key)  {
        match key {
            egui::Key::PageUp => {
                match self.current_screen {
                    ScreenLabels::Debugger => self.scroll_disassembly_view(1),
                    ScreenLabels::Memory => self.scroll_mem_view(1),
                    ScreenLabels::Log => self.scroll_log_view(1),
                    _ => (),
                }
            },
            egui::Key::PageDown => {
                match self.current_screen {
                    ScreenLabels::Debugger => self.scroll_disassembly_view(-1),
                    ScreenLabels::Memory => self.scroll_mem_view(-1),
                    ScreenLabels::Log => self.scroll_log_view(-1),
                    _ => (),
                }
            },
            egui::Key::F5 => {
                if !self.is_debugger_active() {
                    self.control_channel
                        .send(Command::RunNInstr(1))
                        .expect("Send should work");
                    self.plan_recenter_disassembly_if_needed = true;
                } else {
                    self.control_channel
                        .send(Command::Run)
                        .expect("Send should work");
                }

            },
            egui::Key::F3 => {
                self.control_channel
                    .send(Command::RunNInstr(1))
                    .expect("Send should work");
                self.plan_recenter_disassembly_if_needed = true;
            },
            egui::Key::F4 => {
                // Repeat last debug command
                self.user_input.clone_from(&self.last_valid_user_input);
                //println!("Repeat {}",self.user_input);
                self.handle_command_string();
            },
            egui::Key::F10 => {
                if let Some(d) = self.current_debug_frame.as_ref() {
                    //check_visible_line_disasm = true;
                    let pc = d.cpu_snapshot.ad;
                    if let Some(mem) = d.active_mem_copy.as_ref() {
                        if mem[pc as usize] == OPCODE_JSR {
                            self.control_channel
                                .send(Command::BreakAtPC(pc + 3))
                                .expect("Send should work");
                        } else {
                            self.control_channel
                                .send(Command::RunNInstr(1))
                                .expect("Send should work");
                        }
                    }
                }
            }
            egui::Key::F1 => {
                if !self.debugger_active {
                    self.control_channel
                        .send(Command::EnterDebugger)
                        .expect("Send should work");
                    self.debugger_active = true;

                    self.resync_disassembler = true;

                    if let Some(df) = self.current_debug_frame.as_ref() {
                        self.disassembly_offset = 0;
                        self.page_up_offset = df.cpu_snapshot.ad;
                        self.page_down_offset = df.cpu_snapshot.ad;
                        self.plan_recenter_disassembly_if_needed = true;
                        self.skip_watches_highlight_once = true;

                        //println!("self.disassembly_offset {:X}", self.disassembly_offset);
                    }

                    if self.current_screen != ScreenLabels::Memory {
                        self.current_screen = ScreenLabels::Debugger;
                    }
                } else {
                    self.current_screen = ScreenLabels::Log;
                    self.control_channel
                        .send(Command::Run)
                        .expect("Send should work");
                    self.debugger_active = false;
                }
            },
            _ => ()
        };
    }

    pub fn render_to_egui(&mut self, ui: &mut Ui, ctx: &Context) {
        let mut y_top_down = 0.0;

        self.mono_em_space = WidgetText::from("MM").into_galley(ui,Some(egui::TextWrapMode::Extend),14.0,FontSelection::Style(TextStyle::Monospace)).size().x - WidgetText::from("M").into_galley(ui,Some(egui::TextWrapMode::Extend),14.0,FontSelection::Style(TextStyle::Monospace)).size().x;
        self.mono_font_height = ui.text_style_height(&TextStyle::Monospace) + ctx.style().as_ref().spacing.item_spacing.y;
        self.regular_font_height = ui.text_style_height(&TextStyle::Body) + ctx.style().as_ref().spacing.item_spacing.y;



		let _resp = ui.with_layout(egui::Layout::top_down(egui::Align::TOP), |ui| {
            ui.visuals_mut().dark_mode = true;
            ui.visuals_mut().window_fill = egui::Color32::RED;

		    // Tricky stuff to memorize, I'll leave it here for reference:
		    //let mut fonts = FontDefinitions::default();
		    // if let Some(f) = fonts.families.get(&FontFamily::Monospace) {
		    // 	ctx.fonts(|fonts| {
		    // 	    //let font_id: &FontId = ui.style().text_styles.get(&TextStyle::Monospace).unwrap();
		    // 	    ui.text_style_height(&TextStyle::Monospace );
		    // 	})
		    // };

		    self.mono_em_space = WidgetText::from("MM").into_galley(ui,Some(egui::TextWrapMode::Extend),14.0,FontSelection::Style(TextStyle::Monospace)).size().x
                               - WidgetText::from("M").into_galley(ui,Some(egui::TextWrapMode::Extend),14.0,FontSelection::Style(TextStyle::Monospace)).size().x;

		    self.mono_font_height = ui.text_style_height(&TextStyle::Monospace) + ctx.style().as_ref().spacing.item_spacing.y;
		    self.regular_font_height = ui.text_style_height(&TextStyle::Body) + ctx.style().as_ref().spacing.item_spacing.y;

		    //won't do : ctx.fonts(|&fonts| { fonts.families().get(&FontFamily::Monospace) } );

		    //println!("w:{} h:{} pts", self.mono_em_space, self.mono_font_height);

		    // Intended for separators only but may influence others FIXME !
		    ui.visuals_mut().widgets.noninteractive.bg_stroke.color = Color32::WHITE;
		    ui.visuals_mut().widgets.noninteractive.bg_stroke.width = 1.0;


			//ui.selectable_value(&mut my_enum, ScreenLabels::Status, "Status");
		    //ui.selectable_value(&mut my_enum, ScreenLabels::Debugger, "Debugger");
		    //let resp = ui.heading("AccurApple");
		    //ui.label(RichText::new("accurapple").color(Color32::WHITE).heading());
		    // if resp.hovered() {
		    // 	println!("Hovered")
		    // }


		    //ui.separator();

			if let Some(_df) = self.current_debug_frame.as_ref() {
			}

		    ui.horizontal(|ui| {

			ui.label(RichText::new("Menu>").color(Color32::GREEN).text_style(TextStyle::Monospace));

			if ui.selectable_label(self.current_screen == ScreenLabels::Control, "Control").clicked() {
			    self.current_screen = ScreenLabels::Control
			}

			// if self.debugger_active {
            if ui.selectable_label(self.current_screen == ScreenLabels::Debugger, "Debugger").clicked() {
				self.current_screen = ScreenLabels::Debugger
            };
            if ui.selectable_label(self.current_screen == ScreenLabels::Memory, "Memory").clicked() {
				self.current_screen = ScreenLabels::Memory
            };
            if ui.selectable_label(self.current_screen == ScreenLabels::GraphicsMemory, "Graphics").clicked() {
				self.current_screen = ScreenLabels::GraphicsMemory
            };
			//}

			if ui.selectable_label(self.current_screen == ScreenLabels::Documentation, "Documentation").clicked() {
			    self.current_screen = ScreenLabels::Documentation
			}

			if ui.selectable_label(self.current_screen == ScreenLabels::Log, "Log").clicked() {
			    self.current_screen = ScreenLabels::Log
			}

			if ui.selectable_label(self.current_screen == ScreenLabels::Presentation, "Credits").clicked() {
			    self.current_screen = ScreenLabels::Presentation
			}


		    }); // horizontal ui




		    if self.current_screen != ScreenLabels::Presentation {
		    ui.separator();
		    ui.horizontal(|ui| {
		    //ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
			let mut job = LayoutJob::default();



			if let Some(df) = self.current_debug_frame.as_ref() {

                if self.frame_durations.len() < 100 {
                    self.frame_durations.push(df.timers.frame_duration.as_secs_f32());
                } else {
                    self.frame_durations[self.frame_count % 100] = df.timers.frame_duration.as_secs_f32();
                }

                if self.emu_frame_durations.len() < 100 {
                    self.emu_frame_durations.push(df.timers.emu_frame_duration.as_secs_f32());
                } else {
                    self.emu_frame_durations[self.frame_count % 100] = df.timers.emu_frame_duration.as_secs_f32();
                }

                use egui_plot::{Line, Plot, PlotPoints};

                let mean: f32 = self.frame_durations.iter().sum::<f32>() / (self.frame_durations.len() as f32);
                let line =Line::new(
                    PlotPoints::new(
                    (0..self.frame_durations.len()).map(
                        |i|
                        [i as f64, self.frame_durations[i] as f64]
                    ).collect()));

                let mean2: f32 = self.emu_frame_durations.iter().sum::<f32>() / (self.frame_durations.len() as f32);
                let line2 =Line::new(
                    PlotPoints::new(
                    (0..self.frame_durations.len()).map(
                        |i|
                        [i as f64, self.emu_frame_durations[i] as f64]
                    ).collect()));

                Plot::new("Perf").reset().view_aspect(1.0).width(300.0).height(50.0).auto_bounds(
                    Vec2b::new(false, false)).show_axes(false).include_x(0.0).include_x(100.0)
                    .show_x(false).show_y(false)
                    .include_y(mean*1.5)
                    .include_y(0.0).include_y(mean2*1.5)
                    .show(ui, |plot_ui| { plot_ui.line(line); plot_ui.line(line2)});


			    job.append(
                    format!("Cycles {}, frame: {:.1}ms {:.1}ms\nFloppy track:{}",
                    (df.cycles - self.cycle_base).separate_with_underscores(),
                    mean * 1000.0,
                    mean2 * 1000.0,
                    subtrack_str(df.floppy_track)).as_str(),
                    0.0,
                    TextFormat {
                        font_id: FontId::new(14.0, FontFamily::Monospace),
                        ..Default::default()
                    },
			    );
			}
			ui.label(job);

			ui.separator();

			if self.current_screen == ScreenLabels::Debugger {
                ui.vertical(|ui| {
                    ui.horizontal(|ui| {
        			    ui.label(RichText::new("CMD>").color(Color32::GREEN).text_style(TextStyle::Monospace));
                        let w = TextEdit::singleline(&mut self.user_input).hint_text("Enter debugger command");
                        let w = w.id("entry".into());
                        let resp = ui.add_sized(ui.available_size_before_wrap(),w);

                        // Right now, KeyPad Enter is not detected by egui
                        // See https://github.com/emilk/egui/issues/1987
                        let execute_command =  resp.lost_focus() && ui.input(|i| i.key_pressed(egui::Key::Enter));
                        if execute_command {
                            self.handle_command_string()
                        }
                        resp.request_focus();
                    });
                    ui.horizontal(|ui| {
                        ui.label(RichText::new("RESULT>").color(Color32::GREEN).text_style(TextStyle::Monospace));
                        ui.label( &self.command_result);
                    });
                });

			}


		    }); // horizontal ui
		    }



		    // Force the focus on the entry line
		    // ui.memory_mut(|m| {m.request_focus("entry".into())});

		    //let label_response = ui.label(job);
		    //let label_height = label_response.rect.height();
		    //ui.label(format!("height={}",label_height));

		    ui.style_mut().override_text_style = Some(egui::TextStyle::Monospace);
		    let _resp = ui.separator();

		    if let Some(df) = self.current_debug_frame.as_ref() {

				if self.recenter_disassembly_if_needed{
				if let Some(_mem) = df.active_mem_copy.as_ref() {

					/* for (ndx, mark) in df.stack_markers.iter().enumerate() {
						if *mark {
							let rts_return_addr = (mem[ndx+1+1+0x100] as u16)*256+(mem[ndx+1+0x100]as u16)+1;
							println!("{}={:04X}", ndx, rts_return_addr);
						}
					} */
				}
			}


		    match self.current_screen {
			ScreenLabels::Debugger => {
			    self.render_slots(ui);
			    let resp = ui.separator();

			    y_top_down = resp.rect.max.y;

			    let cycle_base = 0;
			    if df.active_mem_copy.is_some() && self.available_lines >= 2 {
                    // Ugly but the borrow checker pushes me there (or I didn't get something right)
                    let mut offset = 0;
                    let mut offset_max = 0;
                    let mut page_up_offset = 0;
                    let mut page_down_offset = 0;
                    let mut disassembly_jump = None;

                    egui::SidePanel::left("my_left_panel").show_inside(ui, |ui| {
                        ui.with_layout(
                            egui::Layout::top_down(egui::Align::TOP), |ui| {
                                self.render_watches(ui);
                                ui.label("");
                                disassembly_jump = disassembly_jump.or(self.render_bookmarks(ui))
                            });
                        ui.separator();
                    });

                    let resp = egui::CentralPanel::default().show_inside(ui, |ui| {
                        ui.with_layout(
                            egui::Layout::top_down(egui::Align::TOP), |ui| {
                            (offset,offset_max,page_up_offset, page_down_offset) =
                                self.render_disassembler(
                                ui,
                                self.disassembly_offset,
                                df,
                                cycle_base);
                            });
                    });

                    // egui's doc says one must add the central panel last. But if
                    // I do that, the central panel takes all the room and the right
                    // panel disappears (it's pushed out of the window I thing). That's
                    // strange because the central panel should take the remaining
                    // space (according to the doc. too).

                    egui::SidePanel::right("my_right_panel").show_inside(ui, |ui| {
                        ui.with_layout(
                            egui::Layout::top_down(egui::Align::TOP), |ui| {
                                disassembly_jump = disassembly_jump.or( self.render_stack(ui, df))
                            }
                        );
                    });


                    // We do these here (right after the call to the disassembler) because BC wants us to.
                    self.disassembly_offset = offset;
                    self.page_up_offset = page_up_offset;
                    self.page_down_offset = page_down_offset;
                    self.recenter_disassembly_if_needed = false;

                    if let Some(addr) = disassembly_jump {
                        self.disassembly_offset = addr;
                    }

                    let d = self.read_mouse_scroll_delta(ui, resp.response.rect);
                    self.scroll_disassembly_view(d);
                } else {
                    ui.label(RichText::new("Hit F5 to enter/leave the debugger").color(Color32::RED));
                }
            },
			ScreenLabels::Documentation => {
                self.render_documentation(ui);
			},
			ScreenLabels::Presentation => {
			    let _resp = ui.with_layout(
				egui::Layout::top_down(egui::Align::TOP), |ui| {
                    let ts = TextStyle::Name("Heading1".into());
				    let _resp = ui.with_layout(
					egui::Layout::top_down(egui::Align::Center), |ui| {
					    ui.label(RichText::new("accurapple").color(Color32::WHITE).font(
                            ui.style().text_styles.get(&ts).unwrap().clone()));
                        ui.label(EMULATOR_SHORT_DESCRIPTION);
					    ui.label(format!("Version {}, © Stéphane Champailler", env!("CARGO_PKG_VERSION")));
					});

				    ui.separator();
				    ui.add(egui::Label::new(RichText::new("\nUnless specified otherwise, code is licensed under the General Affero Public License (GPL) version 3.

Thanks to all who have helped at #Apple II Touch and #AppleInfinitum ! In no particular order: fenarinarsa for making cool demos and being really cool, XtoF, Jansky for being so cool and helping me fix my Apples, JF for fixing my Disk ][, Aldo Reset for unconsciously putting pressure, xot for some mind bending composite decoding, JBrooksBSI for insights, qkumba for insights and great code, Kris Kennaway for its incredible sound routine, Ian Brumby for talking about CrossRunner, Kent Dickey for x-ray insights, Parsifal & BusError for being good company, Utashi for listening to me, Richard31 for testing, stivo for insights !!!

Special thanks to β-seed who provided a life-changing Salae Pro logic analyzer. Without it, guessing the mode switches glitches would have been impossible.

Special thanks to Obscoz who provided a full 65C02 emulation.

Special thanks to grouik for sharing incredible code and doing tests on his PAL CRT's !

None of this wouldn't happen without Jansky's Apple2 Festival Paris. It gives me such a motivation!

We're hosted at https://gitlab.com/wiz21/accurapple
We use many rust packages, see cargo.toml for credits.
We use the PrintChar21 font by Kreative Korps and Motter Tektura Neue by Enzo Bicudo Pepi.
").text_style(TextStyle::Body)).wrap());
				});
			},
			ScreenLabels::Log => {
			    self.render_slots(ui);
			    let resp = ui.separator();
			    y_top_down = resp.rect.max.y;

			    let resp = ui.with_layout(
				egui::Layout::top_down(egui::Align::TOP), |ui| {
				    self.render_log(ui);
				});
			    let d = self.read_mouse_scroll_delta(
				ui,
				Rect::everything_below(resp.response.rect.min.y));
			    self.scroll_log_view(d);
			},
			ScreenLabels::Memory => {
			    self.render_slots(ui);
			    let resp = ui.separator();
			    y_top_down = resp.rect.max.y;

			    let mut bytes_per_line = 0;
			    let mut last_address = 0;


			    let resp = ui.with_layout(
				egui::Layout::left_to_right(egui::Align::LEFT), |ui| {

                    // Render memory banks
				    let resp = ui.with_layout(egui::Layout::top_down(egui::Align::TOP), |ui| {
						if let Some(bank_to_go) = self.render_banks(ui) {
							self.mem_dump_offset = bank_to_go;
						}
				    });

                    //println!("screen_w: {} - resp.w: {} / mono_size: {}",ctx.screen_rect().width(), resp.response.rect.width(), self.mono_em_space);
				    self.available_columns = ((ctx.screen_rect().width() - resp.response.rect.width()) / self.mono_em_space) as usize;

                    // Vertical separator
				    ui.separator();

				    let resp = ui.with_layout(
                        egui::Layout::top_down(egui::Align::TOP), |ui| {
                            (bytes_per_line, last_address) = self.render_memory(ui);
                        });

				    self.bytes_per_line = bytes_per_line;
				    self.last_address = last_address;

				    if resp.response.contains_pointer() {
                        resp.response.ctx.set_cursor_icon(egui::CursorIcon::Crosshair);
                        let r = resp.response;
						if let Some(hpos) = r.ctx.pointer_latest_pos() {
							let x = (((hpos.x - r.rect.left()) / self.mono_em_space) as usize - 6)/3;
							let y = ((hpos.y - r.rect.top()) / self.mono_font_height) as usize - 2;
							//println!("{} {}",x,y);
							let offset = self.mem_dump_offset + (y*bytes_per_line + x) as u16;
							let offset_min_one = offset.saturating_sub(1);

							if let Some(symbol) = self.labels.borrow().get(&offset) {
								if symbol.flags.contains(&SymbolFlags::WordWatch) {
									if let Some(mem) = self.current_debug_frame.as_ref().unwrap().active_mem_copy.as_ref() {
										let v = symbol.read_from(mem).unwrap();
										egui::show_tooltip(ui.ctx(), ui.layer_id(), egui::Id::new("my_tooltip"), |ui| {
                                            //ui.set_min_width(10000.0);
                                            ui.set_max_width(ui.spacing().tooltip_width);
											ui.label(
											//RichText::new(
												format!("${:04X}\n{}={}, ${:04X}",
												symbol.address,symbol.symbol, v, v)) /*.color(Color32::LIGHT_BLUE))*/;
										});
									}
								} else {
									egui::show_tooltip(ui.ctx(), ui.layer_id(), egui::Id::new("my_tooltip"), |ui| {
                                        //ui.set_min_width(10000.0);
                                        //ui.set_max_width(40000.0);
                                        ui.set_max_width(ui.spacing().tooltip_width);
                                    ui.label(
										RichText::new(
											format!("${:04X}\n{}",symbol.address,symbol.symbol)).color(Color32::LIGHT_BLUE));
									});
								}
							} else if let Some(symbol) = self.labels.borrow().get(&offset_min_one) {
								if symbol.flags.contains(&SymbolFlags::WordWatch) {
									if let Some(mem) = self.current_debug_frame.as_ref().unwrap().active_mem_copy.as_ref() {
										let v = symbol.read_from(mem).unwrap();
										egui::show_tooltip(ui.ctx(), ui.layer_id(), egui::Id::new("my_tooltip"), |ui| {
                                            //ui.set_min_width(10000.0);
                                            //ui.set_max_width(40000.0);
                                            ui.set_max_width(ui.spacing().tooltip_width);
											ui.label(
											RichText::new(
												format!("${:04X}+1\n{}={}, ${:04X}",
												symbol.address,symbol.symbol, v, v)).color(Color32::WHITE));
										});
									}
								}
							}
						}
					}
			    });

				//println!("Last {:04X}", last_address);

			    let d = self.read_mouse_scroll_delta(ui, resp.response.rect);
			    self.scroll_mem_view(d);

			},

			ScreenLabels::Control => {
			    self.render_control(ui);
			},

			ScreenLabels::GraphicsMemory => {

			    if df.active_mem_copy.is_some() {
                    self.render_graphics_memory(ui);
                } else {
                    ui.label(RichText::new("The debugger must be active. Hit F5 to enter the debugger !").color(Color32::RED));
                }
		    }

            }
        }


		}).response; // top down

        // The font height and width are hardcoded at the moment
	    // They correspond to what is set up in the font configuration
	    // for font height, and manually estimated for font width.
	    // FIXME Compute those based on the font data.

        let r = ui.clip_rect();

	    self.available_lines = (( (r.max.y - r.min.y) - y_top_down) / (14.0+ctx.style().as_ref().spacing.item_spacing.y)) as usize;
	    //self.available_colums = ((r.max.x - r.min.x) / 12.0 )as usize;
	    self.available_regular_lines = (( (r.max.y - r.min.y) - y_top_down) / self.regular_font_height) as usize;

    }

    pub fn render(&mut self, ctx: &egui::Context /* , raw_input: RawInput*/) /* -> FullOutput */ {
        self.consume_available_debug_frames();

        let y_top_down = 0.0;

        // ctx.run(raw_input, |ctx| {
	    debug!("ctx run");

	    let _resp_panel = egui::CentralPanel::default().show(ctx, |_ui| {

	    }).response; // central panel

	    let r = ctx.screen_rect();

	    // The font height and width are hardcoded at the moment
	    // They correspond to what is set up in the font configuration
	    // for font height, and manually estimated for font width.
	    // FIXME Compute those based on the font data.
	    self.available_lines = (( (r.max.y - r.min.y) - y_top_down) / (14.0+ctx.style().as_ref().spacing.item_spacing.y)) as usize;
	    //self.available_colums = ((r.max.x - r.min.x) / 12.0 )as usize;
	    self.available_regular_lines = (( (r.max.y - r.min.y) - y_top_down) / self.regular_font_height) as usize;

	    //println!("Panel: r_min_y:{} r_max_y:{} available_regular_lines:{}",r.min.y, r.max.y, self.available_regular_lines);

	//}) // ctx.run
    }

    fn scroll_log_view(&mut self, d: isize) {
        if d > 0 {
            self.log_back_lines += 1
        } else if d < 0 && self.log_back_lines > 0 {
            self.log_back_lines -= 1
        }
    }

    fn scroll_disassembly_view(&mut self, d: isize) {
        if d > 0 {
            self.disassembly_offset = self.page_up_offset;
        } else if d < 0 {
            self.disassembly_offset = self.page_down_offset;
        }
    }

    fn scroll_mem_view(&mut self, d: isize) {
        let mem_jump = (8 * self.bytes_per_line) as u16;
        if d > 0 && self.mem_dump_offset >= mem_jump {
            self.mem_dump_offset -= mem_jump;
        } else if d > 0 && self.mem_dump_offset < mem_jump {
            self.mem_dump_offset = 0;
        } else if d < 0 && self.mem_dump_offset + mem_jump <= self.last_address {
            self.mem_dump_offset += mem_jump;
        } else if d < 0 && self.mem_dump_offset + mem_jump > self.last_address {
            self.mem_dump_offset = self.last_address;
        }
    }

    fn center_disassembler_offset(&self, mut offset: u16, dframe: &DebugFrame) -> u16 {
        let labels: IndexMap<u16, Symbol> = IndexMap::new();
        let trace_map: HashMap<u16, &CpuDebugState> = HashMap::new();

        // Crude approximation of "a good number of lines above".
        offset = dframe
            .cpu_snapshot
            .ad
            .saturating_sub((self.available_lines / 2) as u16);

        // We go up line by line until we reached a good spot.
        loop {
            if offset == 0 {
                break;
            }

            let disasm = Disassembler::with_offset(offset);
            let (_, dmem) = dframe.active_mem_copy.as_ref().unwrap().split_at(offset as usize);
            let lines = disasm.disassemble_with_addresses(
                dmem,
                &dframe.active_mem_copy,
                dframe.cpu_snapshot.x,
                dframe.cpu_snapshot.y,
                Some(self.available_lines),
                &labels,
                &trace_map,
            );

            let mut line_ndx = 0;
            let mut current_pc_on_screen = false;
            while line_ndx < lines.len() && !current_pc_on_screen {
                let line = &lines[line_ndx];
                if line.pc + offset == dframe.cpu_snapshot.ad {
                    current_pc_on_screen = true;
                } else {
                    line_ndx += 1;
                }
            }

            if current_pc_on_screen {
                break;
            } else {
                offset -= 1
            }
        }

        offset
    }

    fn render_disassembler(
        &self,
        ui: &mut Ui,
        mut offset: u16,
        dframe: &DebugFrame,
        _cycle_base: u64,
    ) -> (u16, u16, u16, u16) {
        //println!("render_disassembler. offset:{:04X} PC:{:04X}", offset, dframe.cpu.PC);

        let labels = &*self.labels.borrow();

        const STEP_PAGE_LINES: usize = 3;
        const BYTES_PER_INSTR: usize = 3;

        let mut max_cycle = 0; //u64::MAX;

        let mut trace_map: HashMap<u16, &CpuDebugState> = HashMap::new();
        for line in &dframe.trace {
            // Either we've never seen this PC value
            // either we see a more recent one (clock cycle wise)
            if !trace_map.contains_key(&line.pc)
                || (line.cycle > trace_map.get(&line.pc).unwrap().cycle)
            {
                trace_map.insert(line.pc, line);
                if line.cycle > max_cycle {
                    max_cycle = line.cycle;
                }
            }
        }

        if offset == 0 {
            offset = dframe.cpu_snapshot.pc;
        }

        let mut offset_max = 0;

        // We disassemble from offset onwards.
        // offset is expected to be *before* or *at* cpu.PC
        // offset is the page start if you will.

        let disasm = Disassembler::with_offset(offset);
        let (_, dmem) = dframe.active_mem_copy.as_ref().unwrap().split_at(offset as usize);
        let mut lines = disasm.disassemble_with_addresses(
            dmem,
            &dframe.active_mem_copy,
            dframe.cpu_snapshot.x,
            dframe.cpu_snapshot.y,
            Some(self.available_lines),
            labels,
            &trace_map,
        );

        if self.recenter_disassembly_if_needed {
            //println!("Recenter is checked");
            if dframe.cpu_snapshot.ad < lines[0].pc + offset
                || lines[lines.len() - 1].pc + offset < dframe.cpu_snapshot.ad
            {
                //println!("Recenter is done");
                offset = self.center_disassembler_offset(offset, dframe);

                // FIXME It'd be nice to reuse the last disassembly of the
                // `center_disassembler_offset` call above.

                lines = disasm.disassemble_with_addresses(
                    dmem,
                    &dframe.active_mem_copy,
                    dframe.cpu_snapshot.x,
                    dframe.cpu_snapshot.y,
                    Some(self.available_lines),
                    labels,
                    &trace_map,
                );
            } else {
                /* println!("Recenter is skipped : first line:{:04X} PC={:04X} last line:{:04X}",
                lines[0].pc + offset,
                dframe.cpu.PC,
                lines[lines.len()-1].pc + offset); */
            }
        }

        offset_max = offset + lines[lines.len() - 1].pc;
        //println!("self.available_lines= {}",self.available_lines);
        let page_down_offset = offset + lines[STEP_PAGE_LINES.min(self.available_lines - 1)].pc;

        for line_ndx in 0..lines.len() {
            let mut job = LayoutJob::default();
            let line = &lines[line_ndx];

            if labels.contains_key(&(line.pc + offset)) {
                let label = labels.get(&(line.pc + offset)).unwrap();
                job.append(
                    format!("{}:\n", label.symbol).as_str(),
                    0.0,
                    TextFormat {
                        color: Color32::YELLOW,
                        font_id: FontId::new(14.0, FontFamily::Monospace),
                        ..Default::default()
                    },
                );
            }

            //println!("{} {:X} {:X} {:X}",line_ndx, line.pc, line.pc+offset, dframe.cpu.PC);
            // Adapt the color if the cursor is on the line.
            let background = if line.pc + offset == dframe.cpu_snapshot.ad {
                // FIXME if disassembly is not compatible with the
                // offset, the equality will fail

                // FIXME If the memory displayed is not where these
                // the execution is (another bank), then we shouldn't
                // highlight
                Color32::DARK_RED
            } else {
                ui.ctx().style().noninteractive().bg_fill
            };

            job.append(
                line.source[0..16].to_string().as_str(),
                0.0,
                TextFormat {
                    background,
                    font_id: FontId::new(14.0, FontFamily::Monospace),
                    ..Default::default()
                },
            );

            job.append(
                line.source[16..19].to_string().as_str(),
                0.0,
                TextFormat {
                    color: Color32::LIGHT_BLUE,
                    background,
                    font_id: FontId::new(14.0, FontFamily::Monospace),
                    ..Default::default()
                },
            );

            job.append(
                format!("{: <23}", &line.source[19..line.source.len()]).as_str(),
                0.0,
                TextFormat {
                    background,
                    font_id: FontId::new(14.0, FontFamily::Monospace),
                    ..Default::default()
                },
            );

            if trace_map.contains_key(&(line.pc + offset)) {
                let s = trace_map.get(&(line.pc + offset)).unwrap();
                job.append(
                    format!(
                        "A=${:02X} X=${:02X} Y=${:02X}{}, c:{:4}",
                        s.a,
                        s.x,
                        s.y,
                        if let Some(old_ram) = s.old_mem_value {
                            // Write operation
                            if s.mem_access.is_some() {
                                let (addr, byte) = s.mem_access.unwrap();

                                if let Some(sym) = labels.get(&addr) {
                                    format!(
                                        " {:04X}={:02X}→{:02X} {}",
                                        addr, byte, old_ram, sym.symbol
                                    )
                                } else {
                                    format!(" {:04X}={:02X}→{:02X}", addr, byte, old_ram)
                                }
                            } else {
                                "write???".to_string()
                            }
                        } else {
                            // Read operation or nothing operation
                            if let Some((addr, byte)) = s.mem_access {
                                if let Some(sym) = labels.get(&addr) {
                                    format!(" {:04X}={:02X} {}", addr, byte, sym.symbol)
                                } else {
                                    format!(" {:04X}={:02X}", addr, byte)
                                }
                            } else {
                                "".to_string()
                            }
                        },
                        -((max_cycle - s.cycle) as isize)
                    )
                    .as_str(),
                    0.0,
                    TextFormat {
                        background,
                        font_id: FontId::new(14.0, FontFamily::Monospace),
                        ..Default::default()
                    },
                );
            } else if line.pc + offset == dframe.cpu_snapshot.ad {
                job.append(
                    &dframe.cpu_snapshot.to_string(),
                    0.0,
                    TextFormat {
                        background,
                        font_id: FontId::new(14.0, FontFamily::Monospace),
                        ..Default::default()
                    },
                );
            }

            ui.add(egui::Label::new(job));
        } // for

        // ------------------------------------------------------------
        // We compute the PageUp action.
        // PageUp means we want to go up by some disassembly lines
        // *and* make sure that the current lline (offset) still
        // disassembles correctly.

        // What we do is going up on byte then disassembling
        // and checking we reach the current position. We do
        // that until we have gon enough instructions up.
        // We do that to have a disaasembly that "connects"
        // to the current view, instruction wise.
        let mut base_offset = if offset > 0 { offset - 1 } else { offset };

        // if base_offset > cpu.PC {
        // 	base_offset = cpu.PC
        // }

        loop {
            let disasm = Disassembler::with_offset(base_offset);
            let (_, dmem) = dframe
                .active_mem_copy
                .as_ref()
                .unwrap()
                .split_at(base_offset as usize);
            let lines = disasm.disassemble_with_addresses(
                dmem,
                &dframe.active_mem_copy,
                dframe.cpu_snapshot.x,
                dframe.cpu_snapshot.y,
                // We go from the top of the page up to top - one scroll delta.
                Some(3 * BYTES_PER_INSTR * STEP_PAGE_LINES),
                labels,
                &trace_map,
            );

            // Disassemble up to the current offset (ie the current first
            // line on the screen)
            let mut i = 0;
            // FIXME for some reason, we can go past lines.len() (and therefore
            // crashed when accessing lines[i] afterwards
            while i < lines.len() - 1 && base_offset + lines[i].pc < offset {
                i += 1
            }

            if (base_offset+lines[i].pc == offset && i >= STEP_PAGE_LINES && !lines[0].source.contains("???")) /* line found and offset large enough*/
		|| base_offset == 0
            /* beginning of memory */
            {
                break;
            }

            // Up by a byte
            base_offset -= 1;

            // Sometimes going up goes too far, dunno why FIXME
            if (offset - base_offset) as usize > STEP_PAGE_LINES * BYTES_PER_INSTR * 3 {
                // In that case we go up to the last line that
                // was correctly diassembling down to the offset-line.
                break;
            }
        }

        let page_up_offset = base_offset;

        (offset, offset_max, page_up_offset, page_down_offset)
    }

    // , map: RefCell<IndexMap<u16,Symbol>>
    fn render_watches(&self, ui: &mut Ui) {
        let _k = 0x801u16;
        //self.test.borrow_mut().get_mut(&k).unwrap().old_value = 0x12u8;

        if let Some(df) = self.current_debug_frame.as_ref() {
            if let Some(mem) = df.active_mem_copy.as_ref() {
                ui.with_layout(egui::Layout::top_down(egui::Align::TOP), |ui| {
                    let mut labels = self.labels.borrow_mut();

                    if let Some(symfile) = self.symbols_file.borrow().as_ref() {
                        let b = ui.button("Reload symbols");
                        if b.clicked() {
                            load_symbols(symfile, &mut labels);
                            for (_addr, symbol) in labels.iter_mut() {
                                symbol.update_value(mem, true);
                            }
                        }
                    }

                    ui.label(
                        RichText::new("Watches")
                            .color(Color32::GREEN)
                            .text_style(TextStyle::Monospace),
                    );

                    for symbol in labels.values_mut() {
                        let color = if symbol.highlighted {
                            Color32::YELLOW
                        } else {
                            Color32::GRAY
                        };

                        if symbol.flags.contains(&SymbolFlags::ByteWatch) {
                            ui.add(
                                egui::Label::new(
                                    RichText::new(format!(
                                        "${:04X}:{} = ${:02X}",
                                        symbol.address, symbol.symbol, symbol.old_value
                                    ))
                                    .color(color),
                                )
                                ,
                            );
                        } else if symbol.flags.contains(&SymbolFlags::WordWatch) {
                            ui.add(
                                egui::Label::new(
                                    RichText::new(format!(
                                        "${:04X}:{} = ${:04X}",
                                        symbol.address, symbol.symbol, symbol.old_value
                                    ))
                                    .color(color),
                                )
                                ,
                            );
                        }
                    }
                });
            }
        }
    }

    fn render_bookmarks(&self, ui: &mut Ui) -> Option<u16> {
        let mut address_to_go: Option<u16> = None;
        let labels = self.labels.borrow();

        if let Some(df) = self.current_debug_frame.as_ref() {
            if let Some(_mem) = df.active_mem_copy.as_ref() {
                ui.with_layout(egui::Layout::top_down(egui::Align::TOP), |ui| {
                    ui.label(
                        RichText::new("Bookmarks>")
                            .color(Color32::GREEN)
                            .text_style(TextStyle::Monospace),
                    );

                    let b = ui.button(format!("${:04X} PC", df.cpu_snapshot.ad));
                    if b.clicked() {
                        address_to_go = Some(df.cpu_snapshot.ad);
                    }
                    for symbol in labels.values() {
                        if symbol.flags.contains(&SymbolFlags::Jump) {
                            let b = ui.button(format!("${:04X} {}", symbol.address, symbol.symbol));
                            if b.clicked() {
                                address_to_go = Some(symbol.address);
                            }
                        }
                    }
                });
            }
        }

        address_to_go
    }

    fn render_memory(&self, ui: &mut Ui) -> (usize, u16) {
        // Wikipedia charsets : https://en.wikipedia.org/wiki/Apple_II_character_set
        //const CHARSET: &str = &" !\"#$%&'()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[\\]^_ ╵╴╷╶┘┐┌└─│┴┤┬├┼◤◥▒▘▝▀▖▗▚▌  ←↑→↓`ABCDEFGHIJKLMNOPQRSTUVWXYZ{|}~ ";

        //const CHARSET: &str = &"@ ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_!\"#$%&'()*+,-./0123456789:;<=>?";

        // From my own ROM
        // const CHARSET: &str = &"@ABCDEFGHIJKLMNO
        //                         PQRSTUVWXYZ[\\]^_
        //                          !\"#$%&'()*+,-./0123456789:;<=>?                                `abcdefghijklmnopqrstuvwxyz{|}~  ";

        //let vc: Vec<char> = CHARSET.chars().collect(); // FIXME Make this static

        lazy_static! {
            static ref CHARSET : Vec<char> = {
            let mut vc: Vec<char> = Vec::new();

            for c in 0xE140..0xE160u32 {
                vc.push( char::from_u32(c).unwrap());
            }
            for c in 0xE120..0xE140u32 {
                vc.push( char::from_u32(c).unwrap());
            }
            for c in 0xE080..0xE0A0u32 {
                vc.push( char::from_u32(c).unwrap());
            }
            // Mouse
            for c in 0xE160..0xE180u32 {
                vc.push( char::from_u32(c).unwrap());
            }
            // Normal char
            for c in 0xE040..0xE060u32 {
                vc.push( char::from_u32(c).unwrap());
            }
            for c in 0xE020..0xE040u32 {
                vc.push( char::from_u32(c).unwrap());
            }
            for c in 0xE040..0xE060u32 {
                vc.push( char::from_u32(c).unwrap());
            }
            for c in 0xE060..0xE080u32 {
                vc.push( char::from_u32(c).unwrap());
            }
            vc
            };
        }

        let _labels = self.labels.borrow();
        let mut nb_visible_lines = self.available_lines - 2; //-2 for headers
        let mut i: usize = 1024;
        const ADDR_HEADER_LEN: usize = 6;
        //println!("available columns: {}", self.available_columns);
        while i > ((self.available_columns - ADDR_HEADER_LEN - 1) / 4) && i > 16 {
            i /= 2;
        }
        let bytes_per_line = i;

        if nb_visible_lines * bytes_per_line + self.mem_dump_offset as usize >= 65536 {
            nb_visible_lines = (65536 - self.mem_dump_offset as usize) / bytes_per_line;
        }

        //let bytes_per_line = 16;

        if let Some(mem) = self.current_debug_frame.as_ref().unwrap().active_mem_copy.as_ref() {
            use std::fmt::Write;

            // In line offsets
            let s = (0..bytes_per_line).fold("      ".to_string(), |mut res, x| {
                write!(&mut res, "{:02X} ", x).unwrap();
                res
            });
            ui.add(egui::Label::new(s));

            // Separator

            let s = (0..bytes_per_line).fold("------".to_string(), |mut res, _x| {
                write!(&mut res, "---").unwrap();
                res
            });
            ui.add(egui::Label::new(s));

            for line in 0..nb_visible_lines.min(nb_visible_lines) {
                let ofs = self.mem_dump_offset as usize + line * bytes_per_line;
                let _s: String = format!("{:04X}: ", ofs);

                // for column in 0..bytes_per_line {
                //     if ofs + column < mem.len() {
                // 		s.push_str(format!("{:02X} ",mem[ofs+column]).as_str())
                //     }
                // }
                // ui.add(egui::Label::new(s).wrap(false));

                let mono_id = TextStyle::Monospace.resolve(&ui.ctx().style());
                let mut job = LayoutJob::default();
                job.append(
                    format!("{:04X}: ", ofs).as_str(),
                    0.0,
                    TextFormat {
                        font_id: mono_id.clone(),
                        ..Default::default()
                    },
                );
                for column in 0..bytes_per_line {
                    let mut fmt = TextFormat {
                        font_id: mono_id.clone(),
                        ..Default::default()
                    };


                    let address = (ofs + column) as u16;
                    if is_address_symbol(&self.labels, &address) {
                        fmt.color = Color32::YELLOW;
                    }
                    job.append(format!("{:02X} ", mem[address as usize]).as_str(), 0.0, fmt);
                }

                let mut char_view = String::new();
                for column in 0..bytes_per_line {
                    char_view.push(CHARSET[(mem[ofs + column]) as usize]);
                }
                job.append(
                    char_view.as_str(),
                    0.0,
                    TextFormat {
                        font_id: mono_id.clone(),
                        ..Default::default()
                    },
                );
                ui.add_enabled(true, egui::Label::new(job));
            }
        }

        (
            bytes_per_line,
            (65536 - (bytes_per_line * nb_visible_lines)) as u16,
        )
    }

    fn render_banks(&self, ui: &mut Ui) -> Option<u16> {
        let mut bank_to_go = None;
        if let Some(dframe) = self.current_debug_frame.as_ref() {
            ui.label(
                RichText::new("Bookmarks>")
                    .color(Color32::GREEN)
                    .text_style(TextStyle::Monospace),
            );



            for i in 0..BANKS_OF_INTEREST.len() {
                let b = ui.button(format!(
                    "${:04X} r:{: <6} w:{: <6}",
                    BANKS_OF_INTEREST[i],
                    dframe.memory_banks[i].short_str(),
                    dframe.memory_banks_for_write[i].short_str()
                ));
                if b.clicked() {
                    bank_to_go = Some(BANKS_OF_INTEREST[i]);
                }
            }
        }
        bank_to_go
    }

    fn render_stack(&self, ui: &mut Ui, dframe: &DebugFrame) -> Option<u16> {
        ui.label(
            RichText::new("JSR Trace (from/to)")
                .color(Color32::GREEN)
                .text_style(TextStyle::Monospace),
        );
        let mut address_to_go = None;
        for (_ndx, jsr_dest) in dframe.jsr_stack.iter().enumerate() {
            ui.with_layout(egui::Layout::left_to_right(egui::Align::LEFT), |ui| {
                let button = Button::new(addr_to_label(&self.labels, &jsr_dest.from));
                let button_dest = Button::new(addr_to_label(&self.labels, &jsr_dest.to));

                let b = ui.add(button);
                ui.add(egui::Label::new("\u{2192}"));
                let b_dest = ui.add(button_dest);

                if b.clicked() {
                    address_to_go = Some(jsr_dest.from);
                }
                if b_dest.clicked() {
                    address_to_go = Some(jsr_dest.to);
                }
            });
        }

        address_to_go
    }

    fn render_control(&mut self, ui: &mut Ui) {

        ui.label(
            RichText::new("Floppies>")
                .color(Color32::GREEN)
                .text_style(TextStyle::Monospace),
        );

        egui::Frame::none().rounding(5.0).inner_margin(10.0).stroke( egui::Stroke::new( 1.0, egui::Color32::GRAY)).show(ui, |ui| {
            //let mut phase_shift = 0.0;



            ui.vertical(|ui| {

                ui.label(format!(
                    "Floppy1: {}",
                    if let Some(df) = self.current_debug_frame.as_ref() {
                        if let Some(pb) = df.floppy_disk_name.as_ref() {
                            pb.file_name().unwrap().to_string_lossy().to_string()
                        } else {
                            "No disk!".to_string()
                        }
                    } else {
                        "No disk!".to_string()
                    }
                ));


                let b = ui.button("Load dsk/woz/po/do");
                {
                    let mut a = self.file_dialog.borrow_mut();
                    let fd = a.as_mut().unwrap();

                    if b.clicked() {
                        fd.activate(ui.ctx());
                    }

                    if fd.is_open {
                        if let Some(file) = fd.ui(ui.ctx()) {
                            self.floppy1_file.borrow_mut().replace(file.to_path_buf());
                            let configuration = &mut self.configuration;
                            configuration.last_floppy_file = Some(file.to_path_buf());
                            configuration.save();

                            self.control_channel
                                .send(Command::LoadFloppy(file.to_path_buf()))
                                .expect("Send should work");
                        }
                    }

                }


                let b = ui.button("Empty floppy");
                if b.clicked() {
                    self.control_channel
                        .send(Command::RemoveFloppy)
                        .expect("Send should work");
                    self.floppy1_file.borrow_mut().take();
                    self.moby_cover.borrow_mut().clear_image();
                }

                ui.horizontal(|ui| {
                    let configuration = &mut self.configuration;
                    let mut moby_key = if let Some(k) = &configuration.moby_api_key {k.clone()} else {"".to_string()};
                        ui.label("Moby API Key:");
                    let b = ui.text_edit_singleline(&mut moby_key);
                    if b.changed() {
                        configuration.moby_api_key = Some(moby_key);
                        configuration.save();
                        if let Some(old_checksum) = self.floppy1_checksum {
                            let k = &configuration.moby_api_key.clone();
                            self.reload_moby_cover(old_checksum, k);
                        }

                    }
                });

                self.moby_cover.borrow_mut().ui(ui);
                ui.add(egui::Hyperlink::from_label_and_url("Cover from Moby Games", "https://mobygames.com"));

            })
        });


        ui.label(
            RichText::new("AccurApple>")
                .color(Color32::GREEN)
                .text_style(TextStyle::Monospace),
        );
        egui::Frame::none().rounding(5.0).inner_margin(10.0).stroke( egui::Stroke::new( 1.0, egui::Color32::GRAY)).show(ui, |ui| {
            //let mut phase_shift = 0.0;
            ui.vertical(|ui| {

                let b = ui.button("Cold Reboot");
                if b.clicked() {
                    self.control_channel
                        .send(Command::Reset)
                        .expect("Send should work");
                }
                let b = ui.button("Turbo On/off");
                if b.clicked() {
                    self.control_channel
                        .send(Command::TurboSwap)
                        .expect("Send should work");
                }


                ui.label("");



                {
                    let configuration = &mut self.configuration;

                    let mut z = configuration.altcharset.unwrap_or(false);
                    ui.add(egui::Checkbox::new(&mut z, "AltCharset"));
                    if z != configuration.altcharset.unwrap_or(false) {
                        configuration.altcharset = Some(z);
                        configuration.save();
                        self.control_channel
                            .send(Command::AltCharset(configuration.altcharset.unwrap()))
                            .expect("Send should work");
                    }


                    let mut selected: CpuType = configuration.cpu.clone();
                    egui::ComboBox::from_label("CPU")
                        .selected_text(format!("{:?}", selected))
                        .show_ui(ui, |ui| {
                            ui.selectable_value(&mut selected, CpuType::Floooh6502, "Floooh 6502");
                            ui.selectable_value(&mut selected, CpuType::Obscoz6502, "Obscoz 6502");
                            ui.selectable_value(&mut selected, CpuType::Obscoz65C02, "Obscoz 65C02");
                        }
                    );

                    if selected != configuration.cpu {
                        configuration.cpu = selected;
                        configuration.save();
                        self.control_channel
                            .send(Command::ChangeCPUAndReset(configuration.cpu.clone()))
                            .expect("Send should work");
                    }
                }


                ui.label(format!(
                    "Symbol file: {}",
                    if let Some(p) = self.symbols_file.borrow().as_ref() {
                        //
                        p.to_string_lossy().to_string()
                    } else {
                        "No symbol file!".to_string()
                    }
                ));
                let b_load_sym = ui.button("Open symbols");

                {
                    let mut a = self.sym_file_dialog.borrow_mut();
                    let fd = a.as_mut().unwrap();

                    if b_load_sym.clicked() {
                        fd.activate(ui.ctx());
                    }

                    if fd.is_open {
                        if let Some(file) = fd.ui(ui.ctx()) {
                            let mut labels = self.labels.borrow_mut();
                            load_symbols( &file.to_path_buf(), &mut labels);
                            self.symbols_file.borrow_mut().replace(file.to_path_buf());
                        }
                    }

                }
            })
        });

        ui.label(
            RichText::new("Cathode Ray Tube>")
                .color(Color32::GREEN)
                .text_style(TextStyle::Monospace),
        );

        ui.horizontal(|ui| {
            egui::Frame::none().rounding(5.0).inner_margin(10.0).stroke( egui::Stroke::new( 1.0, egui::Color32::GRAY)).show(ui, |ui| {
                //let mut phase_shift = 0.0;
                ui.vertical(|ui| {
                    ui.spacing_mut().slider_width = 300.0;

                    if ui.add(egui::Slider::new(&mut self.composite_to_rgb_uniform.phase, 0.0..=1.0).text("Phase")).changed() {
                        let configuration = &mut self.configuration;
                        configuration.display_phase = Some(self.composite_to_rgb_uniform.phase);
                        configuration.save();
                    }


                    let mut filter_order = self.composite_to_rgb_uniform.luma_filter_size;
                    ui.add(egui::Slider::new(&mut filter_order, 1..=21).text("Luma filter order"));
                    self.composite_to_rgb_uniform.luma_filter_size = filter_order | 1; // Must be odd
                    ui.add(egui::Slider::new(&mut self.luma_freq, 100000.0..=3_800_000.0).text("Luma Frequency"));
                    ui.add(egui::Slider::new(&mut self.chroma_freq, 100000.0..=3_800_000.0).text("Chroma Frequency"));
                    ui.add(egui::Slider::new(&mut self.composite_to_rgb_uniform.luma_threshold, 0.0..=1.0).text("Black threshold"));
                    ui.add(egui::Slider::new(&mut self.composite_to_rgb_uniform.hsv_value[0], 0.5..=1.5).text("HSV Hue"));
                    ui.add(egui::Slider::new(&mut self.composite_to_rgb_uniform.hsv_value[1], 0.5..=1.5).text("HSV Saturation"));
                    ui.add(egui::Slider::new(&mut self.composite_to_rgb_uniform.hsv_value[2], 0.5..=1.5).text("HSV Value"));

                    let mut comb = self.composite_to_rgb_uniform.comb_filter == 1;
                    ui.add(egui::Checkbox::new(&mut comb, "Comb Filter"));
                    if comb {
                        self.composite_to_rgb_uniform.comb_filter = 1;
                    } else {
                        self.composite_to_rgb_uniform.comb_filter = 0;
                    }

                    let old_value = self.motherboard_mono_colour_switch.clone();
                    ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
                        ui.label("Mono/Colour mother board switch: ").on_hover_ui(|ui| {
                            ui.label("This switch is located on the motherboard. It controls the emission of the colour burst in the PAL video signal.");
                        });
                        ui.radio_value(&mut self.motherboard_mono_colour_switch, MonoColourSwitch::Mono, "Mono");
                        ui.radio_value(&mut self.motherboard_mono_colour_switch, MonoColourSwitch::Colour, "Colour");
                    });

                    if old_value != self.motherboard_mono_colour_switch {
                        self.composite_to_rgb_uniform.motherboard_mono_color_switch =
                            match self.motherboard_mono_colour_switch {
                                MonoColourSwitch::Colour => 1,
                                MonoColourSwitch::Mono => 0
                            };
                    }

                    let old_value = self.crt_colourburst_interpretation.clone();
                    ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
                        ui.label("CRT bias: ").on_hover_ui(|ui| {
                            ui.label("Some CRT monitors will switch to full screen monochrome if they see some text lines. Some will apply colour burst on each line.");
                        });
                        ui.radio_value(&mut self.crt_colourburst_interpretation, CRTColourBurstInterpretation::FullScreenMono, "Fullscreen");
                        ui.radio_value(&mut self.crt_colourburst_interpretation, CRTColourBurstInterpretation::LineByLine, "Line by line");
                    });

                    // if old_value != self.crt_colourburst_interpretation {
                    //     self.crt_colourburst_interpretation = old_value;
                    // }


                    let mut composite = self.composite_to_rgb_uniform.composite == 1;
                    if ui.checkbox(&mut composite, "Composite").on_hover_ui(|ui| {
                        ui.label("Display the composite signal as output by the Apple 2 (without colour bursts)");
                    }).changed() {
                        let configuration = &mut self.configuration;
                        configuration.display_composite = Some(composite);
                        configuration.save();
                    }

                    if composite {
                        self.composite_to_rgb_uniform.composite = 1;
                    } else {
                        self.composite_to_rgb_uniform.composite = 0;
                    }

                    if self.power_debug {
                        let mut dbg_line = self.rgb_to_host_uniform.line_to_debug;
                        ui.add(egui::Slider::new(&mut dbg_line, 0u32..=193u32).text("Debug Line"));
                        if dbg_line != self.rgb_to_host_uniform.line_to_debug {
                            self.control_channel
                            .send(Command::SetGfxDebugLine(dbg_line))
                            .expect("Send should work");
                        }

                        ui.add(egui::Slider::new(&mut self.ram_to_composite_uniform.dummy1, -14f32..=14f32).text("Dummy1"));
                        ui.add(egui::Slider::new(&mut self.ram_to_composite_uniform.dummy2, -14f32..=14f32).text("Dummy2"));
                        self.ram_to_composite_uniform.line_to_debug = self.rgb_to_host_uniform.line_to_debug;
                        ui.add(egui::Checkbox::new(&mut self.irq_lines_enabled, "IRQ lines"));
                        if ui.button("Record debug line").clicked() {

                            self.control_channel
                                .send(Command::StoreMem(PathBuf::from_str("allmem.bin").unwrap()))
                                .expect("Send should work");

                            self.control_channel
                                .send(Command::SaveGfxDebug(PathBuf::from_str("gfx_debug.bin").unwrap()))
                                .expect("Send should work");

                            self.control_channel
                                .send(Command::GlScreenshot(PathBuf::from_str("scanner.bin").unwrap()))
                                .expect("Send should work");

                            //SAVE_ALL_MEM allmem.bin,  WAIT_UNTIL 21, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin
                        }
                    }

                    filters(SIGNAL_14M_FREQ, self.luma_freq, self.chroma_freq, self.composite_to_rgb_uniform.luma_filter_size, &mut self.filters);

                    {
                        let _nb_lines = CYCLES_PER_FRAME / CYCLES_PER_LINE;
                        self.rgb_to_host_uniform.irq_line.fill(-1);
                        if let Some(df) = self.current_debug_frame.as_ref() {
                            if self.irq_lines_enabled {

                                for &req in df.planned_irqs.iter() {
                                    // We don't draw service requests
                                    if req.trigger_cpu {
                                        let line = ((req.irq_cycle % (CYCLES_PER_FRAME as u64)) / (CYCLES_PER_LINE as u64)) as i32;

                                        let line_num = match req.sub_source {
                                            1..=2 => req.sub_source - 1,
                                            0x81..=0x83 => (req.sub_source - 1) & (0b1 + 0b10),
                                            _ => 0
                                        };

                                        self.rgb_to_host_uniform.irq_line[line_num] = line;// (l.to_f32().unwrap() / nb_lines.to_f32().unwrap() * 192.0) as i32;
                                    }
                                }
                            }

                            self.rgb_to_host_uniform.frame = (df.cycles as usize / CYCLES_PER_FRAME) as u32;
                        }
                    }

                });
            });

            egui::Frame::none().rounding(5.0).inner_margin(10.0).stroke( egui::Stroke::new( 1.0, egui::Color32::GRAY)).show(ui, |ui| {
                ui.vertical(|ui| {

                    if ui.button("Composite screenshot").clicked() {
                        self.control_channel
                            .send(Command::ScreenshotComposite(PathBuf::from("screenshot_composite.png")))
                            .expect("Send should work");
                    }

                    if ui.button("CRT screenshot").clicked() {
                        self.control_channel
                            .send(Command::ScreenshotCrt(PathBuf::from("screenshot_crt.png")))
                            .expect("Send should work");
                    }

                    if ui.button("Start CRT video recording").clicked() {
                        self.control_channel
                            .send(Command::RecordVideoStart(PathBuf::from("video_crt.png")))
                            .expect("Send should work");
                    }

                    if ui.button("Stop video recording").clicked() {
                        self.control_channel
                            .send(Command::RecordVideoStop)
                            .expect("Send should work");
                    }

                })});
        });

    }

    fn render_graphics_memory(&self, ui: &mut Ui) {
        ui.label(RichText::new("HGR>").color(Color32::GREEN).text_style(TextStyle::Monospace));
        ui.horizontal(|ui| {
            egui::Frame::default().stroke(Stroke::new(1.0, Color32::GRAY)).show(ui, |ui| self.hgr1.borrow_mut().ui(ui));
            egui::Frame::default().stroke(Stroke::new(1.0, Color32::GRAY)).show(ui, |ui| self.hgr2.borrow_mut().ui(ui));
        });

        ui.label(RichText::new("DHGR>").color(Color32::GREEN).text_style(TextStyle::Monospace));
        ui.horizontal(|ui| {
            egui::Frame::default().stroke(Stroke::new(1.0, Color32::GRAY)).show(ui, |ui| self.dhgr1.borrow_mut().ui(ui));
            egui::Frame::default().stroke(Stroke::new(1.0, Color32::GRAY)).show(ui, |ui| self.dhgr2.borrow_mut().ui(ui));
        });

    }

    fn render_documentation(&self, ui: &mut Ui) {
        let _resp = ui.with_layout(
            egui::Layout::top_down(egui::Align::TOP), |ui| {
                egui::ScrollArea::vertical().show(ui, |ui| {
                    const DOC_TEXT: &str = include_str!("../data/documentation.txt") ;

                    lazy_static! {
                        static ref DOC_VEC : Vec<&'static str> = DOC_TEXT.split('\n').collect();
                    };

                let EMPHASIS_RE:Regex = Regex::new(r".*(\[.+?]).*").unwrap();

                for s in DOC_VEC.iter() {
                    if (*s).starts_with('*') {
                    let ss = String::from(*s);
                    //ui.heading(&ss[1..ss.len()]);
                    ui.label("");
                    ui.label(RichText::new(&ss[1..ss.len()]).color(Color32::GREEN).text_style(TextStyle::Monospace));
                    } else {
                    let mut job = LayoutJob::default();
                    if let Some(c) = EMPHASIS_RE.captures(s) {
                        let start = c.get(1).unwrap().start();
                        let end = c.get(1).unwrap().end();
                        let ss = s.to_string();
                        job.append( &ss[0..start],
                            0.0, TextFormat { font_id: FontId::new(14.0, FontFamily::Proportional), ..Default::default() });
                        job.append( &ss[start+1 .. end-1],
                            0.0, TextFormat { font_id: FontId::new(12.0, FontFamily::Monospace), color:Color32::YELLOW, ..Default::default() });
                        job.append( &ss[end..],
                            0.0, TextFormat { font_id: FontId::new(14.0, FontFamily::Proportional), ..Default::default() });

                    } else {
                        job.append( s,
                            0.0, TextFormat { font_id: FontId::new(14.0, FontFamily::Proportional), ..Default::default() });
                    }
                    ui.label(job);
                    }
                }

                });

            });

    }

    fn handle_command_string(&mut self) {
        lazy_static! {
            static ref DEBUG_COMMAND_RE: Regex =
                Regex::new(r"^([[:alpha:]]+)( ([[:alnum:]_]+))?$").unwrap();
        }

        let command_str = &self.user_input;

        let cap = DEBUG_COMMAND_RE.captures(command_str.as_str());

        if cap.is_some() {
            let cap = cap.unwrap();
            let cmd = &cap.get(1).unwrap().as_str();
            let mut to_send = None;
            let mut command_is_valid = true;

            if cap.get(3).is_some() {
                // Commands with one argument

                let val_int: Option<usize> =
                    usize::from_str_radix(cap.get(3).unwrap().as_str(), 10).ok();
                let val_u16: Option<u16> =
                    u16::from_str_radix(cap.get(3).unwrap().as_str(), 16).ok();

                to_send = match *cmd {
                    "g" => {
                        if let Some(hexa) = val_u16 {
                            Some(Command::GoToPC(hexa))
                        } else {
                            let user_sym = cap.get(3).unwrap().as_str();
                            let symbols = self.labels.borrow();

                            let mut res = None;
                            for (_addr, sym) in symbols.iter() {
                                println!("{} {}",sym.symbol, user_sym);
                                if sym.symbol.eq_ignore_ascii_case(user_sym) {
                                    res = Some(Command::GoToPC(sym.address));
                                    break
                                }
                            }
                            res
                        }
                    },
                    "c" => Some(Command::BreakAtCycle(val_int.expect("Int number") as u64 + self.cycle_base)),
                    "p" => Some(Command::RunNInstr(val_int.expect("Number of instructions"))),
                    "pt" => Some(Command::RunNCycles(val_int.expect("Number of cycles"))),
                    "u" => {
                        self.disassembly_offset = val_u16.unwrap_or(0);
                        self.current_screen = ScreenLabels::Debugger;
                        None
                    }
                    "md" => {
                        self.mem_dump_offset = val_u16.unwrap_or(0);
                        self.current_screen = ScreenLabels::Memory;
                        None
                    }
                    "bpmw" => Some(Command::BreakAtMemoryWrite(Some(
                        val_u16.expect("16bits hex number"),
                    ))),
                    "bpmr" => Some(Command::BreakAtMemoryRead(Some(
                        val_u16.expect("16bits hex number"),
                    ))),
                    "bpft" => {
                        let subtrack = val_int.expect("Subtrack number == track*4");
                        info!(
                            "Beakpoint on floppy reaching subtrack (times 4) {}",
                            subtrack
                        );
                        Some(Command::BreakAtTrack(Some(subtrack)))
                    }
                    _ => {
                        command_is_valid = false;
                        self.command_result = "Unrecgnized command".to_string();
                        None
                    }
                };
            } else if cap.get(1).is_some() {
                // Commands with zero argument

                to_send = match *cmd {
                    "bpmw" => Some(Command::BreakAtMemoryWrite(None)), // Clear the breakpoint
                    "bpmr" => Some(Command::BreakAtMemoryRead(None)),
                    "bpft" => Some(Command::BreakAtTrack(None)),
                    "bpml" => {
                        info!("Beakpoint on memory layout set");
                        Some(Command::BreakAtMemoryLayout)
                    }
                    "bpf" => {
                        info!("Beakpoint on floppy control set");
                        Some(Command::BreakAtFloppy)
                    }
                    "p" => Some(Command::RunNInstr(1)),
                    "gg" => {
                        //self.current_screen = ScreenLabels::Log;
                        self.debugger_active = false;
                        //println!("gg");
                        Some(Command::Run)
                    }
                    "rc" => {
                        if let Some(df) = self.current_debug_frame.as_ref() {
                            self.cycle_base = df.cycles;
                        }
                        None
                    }
                    "S" => Some(Command::StoreMem(PathBuf::from_str("mem.bin").ok().unwrap())),
                    _ => {
                        command_is_valid = false;
                        None
                    }
                };
            }

            if to_send.is_some() {
                self.control_channel
                    .send(to_send.unwrap())
                    .expect("Send should work");
            }
            if command_is_valid {
                self.command_result = "OK".to_string();
                //last_valid_input_str = command_str.clone();
                self.last_valid_user_input.clone_from(&self.user_input);
                self.user_input.clear();
            } else {
                self.command_result = "Error".to_string();
            }
        } else {
            let mut ctx = calc::Context::<i64>::default();
            let parser = calc::ast::parser::ExprParser::new();
            let modern = self.user_input.replace('$', "0x");
            if let Ok(_parsed) = parser.parse(&modern) {
                if let Ok(re) = ctx.evaluate( &modern) {
                    if re.abs() < 256 {
                        self.command_result = format!("{} = ${:02X} = {}", self.user_input, re & 0xFFFF, re);
                    } else {
                        self.command_result = format!("{} = ${:04X} = {}", self.user_input, re & 0xFFFF, re);
                    }
                } else {
                    error!("Cannot evaluate {}", modern);
                }
            } else {
                error!("Cannot parse {}", modern);
            }
        }
    }

    fn reload_moby_cover(&self, crc: u32, moby_api_key_in: &Option<String>) {
        if let Some(moby_api_key) = moby_api_key_in {
            if let Some(game_id) = MOBY_CRC_TO_GAME_ID.get(&crc) {
                info!("{:08X} known in my Moby game database, it corresponds to {}", crc, game_id);

                if let Some(url) = MOBY_GAME_ID_TO_COVER_URL.get(game_id) {

                    if let Some(image) = download_image(url) {
                        self.moby_cover.borrow_mut().set_image(image);
                    } else {
                        self.moby_cover.borrow_mut().clear_image();
                    }
                    return;
                } else {
                    info!("Game ID {} not known in my covers database", game_id);
                }

                // let r = reqwest::blocking::get(format!(
                //     "https://api.mobygames.com/v1/games/{}?api_key={}",
                //     game_id, moby_api_key
                // ));

                // if r.is_ok() {
                //     let reply = r.ok().unwrap();

                //     if reply.status().is_success() {

                //         if let Ok(json) = reply.json::<Value>() {
                //             if let Some(sample_cover) = json.get("sample_cover") {
                //                 if let Some(image) = sample_cover.get("thumbnail_image") {
                //                     //println!("{}", image.to_string());
                //                     let url_str = image.as_str().unwrap();
                //                     let r = reqwest::blocking::get(url_str);
                //                     if let Ok(reply) = r {
                //                         //println!("{}", reply.bytes().unwrap().len());
                //                         use image::io::Reader;
                //                         use std::io::Cursor;

                //                         let reader =
                //                             Reader::new(Cursor::new(reply.bytes().ok().unwrap()))
                //                                 .with_guessed_format()
                //                                 .expect("Cursor io never fails");
                //                         // if let Some(fmt) = reader.format() {
                //                         //     for s in fmt.extensions_str() {
                //                         //         println!("Extension: {}", s);
                //                         //     }
                //                         // }
                //                         if let Ok(imgbytes) = reader.decode() {
                //                             self.moby_cover.borrow_mut().set_image(imgbytes.resize(imgbytes.width()*3/4, imgbytes.height()*3/4, image::imageops::FilterType::Nearest));
                //                             return;
                //                         } else {
                //                             error!("Can't decode Moby's cover picture data into an image");
                //                         }
                //                     } else {
                //                         error!("Unable to read thumbnail_image from Moby's API");
                //                     }
                //                 } else {
                //                     error!("Unable to load game cover");
                //                 }
                //             } else {
                //                 error!("Unable to load 'sample_cover' json value");
                //             }
                //         } else {
                //             error!("Unable to load json reply from Moby's API");
                //         }

                //     } else {
                //         error!("Moby Game API error {}: {}", reply.status().as_str(), reply.text().unwrap());
                //     }
                // } else {
                //     error!("MobyGames couldn't answer our query {}",
                //         if let Some(status_code) = r.err().unwrap().status() {
                //             status_code.as_str().to_string()
                //         } else {"Unknonw error".to_string()});
                // }
            } else {
                warn!("{:08X} unknown in my Moby game database", crc);
            }

            self.moby_cover.borrow_mut().clear_image();
        } else {
            warn!("No Moby API key so I can't retrieve games' cover");
        }
    }
}
