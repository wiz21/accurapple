use std::cell::RefCell;
use std::collections::VecDeque;
use std::fs;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::ops::DerefMut;
use std::path::{Path, PathBuf};
use std::rc::Rc;
use std::str::FromStr;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, Mutex};
use std::thread::sleep;
use std::time::{Duration, Instant};
use std::{thread, u64, usize};

use circular_queue::CircularQueue;
use lazy_static::__Deref;
use log::{debug, error, info, trace, warn};

use crate::all6502::{Cpu6502, CpuSnaphsot, Floooh6502, Obscoz6502, Obscoz65C02, IrqLineState};
use crate::rs6502::disassembler::Disassembler;

use crate::a2::{
    ALTCHARSETSelect, AppleII, BANKS_OF_INTEREST, CYCLES_PER_FRAME, CYCLES_PER_SECOND, FRAMES_PER_SECOND, NB_SLOTS
};

use crate::apple_key::Key;
use crate::diskii::DiskController;
use crate::m6502::{
    OPCODE_JSR, OPCODE_RTS, OPCODE_RTI, OPCODE_BRK
};

use crate::mos6522::MockingBoard;
//use crate::gfx::{render_one_frame,GfxRenderState};
//use crate::gfx_tv::{render_one_frame,GfxRenderState,Scanner};
use crate::debug2::DebugFrame;
use crate::generic_peripheral::{BreakPointTrigger, BreakPointTriggerRef, IrqChange};
//use crate::gfx_tv::Scanner;
use crate::gfx_tv3::{render_one_frame2, GfxRenderState};
use crate::mem::Mem; // FIXME For storeb, deprecate that
use crate::sound::{SamplesFrame, SoundRenderer};
use crate::record_iou_debug_information;

use gilrs::{Axis, Button, Event, EventType, Gilrs};

use std::default::Default;

const SCREEN_WIDTH: usize = 280 * 2;
const SCREEN_HEIGHT: usize = 312;

use serde::{Deserialize, Serialize};
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Display)]
pub enum CpuType {
    Floooh6502,
    Obscoz6502,
    Obscoz65C02,
}

// RGBA screen
pub struct Screen {
    // FIXME remove pub, provide accessor to the [u8] array directly
    pub ptr: Box<[u8; SCREEN_WIDTH * SCREEN_HEIGHT * 4]>, //Vec<u8>;
}
impl Screen {
    fn new() -> Screen {
        Screen {
            ptr: Box::new([0; SCREEN_WIDTH * SCREEN_HEIGHT * 4]),
        }
    }
}

// FIXME This is required for take() to work.
// rustc complains about some stuff about
// the fact there's not default() for arrays
// According to what I'v read, rustc can't
// do that at the moment.
// I tried to implmement default() directly
// over a Box<[u8;...]> but it didn't work either.
// So I finally wrapped everything inside a new struct.

impl Default for Screen {
    fn default() -> Screen {
        Screen::new()
    }
}

pub struct OutputQueues {
    // Note that contrary to the screen render which operates
    // on a frame basis, the sound rendering is much more fluid.
    pub sound: VecDeque<(i16, i16)>,
    gfx_wip_frame: Screen,

    // When the display has consumed the ram/softswitches to display one frame.
    // It sets consumed to true. When false, the display knows a new frame is
    // available.
    pub consumed: bool,

    pub last_time_displayed: Instant,

    // All the softswitches at any time.
    pub rendered_soft_switches: Vec<u8>,

    // Scanned RAM
    pub scanned_ram: Vec<u8>,

    // Colour burst flag at each of the 192 lines
    pub colour_bursts: Vec<u8>,
    pub done_frames: usize,

    // These screenshots require an access to wgpu to get the data. Therefore
    // it's not possible to do it inside the "emulator" part, only in the "main"
    // part (which knows the wgpu. To ensure the commands are all seen by the
    // emulator (and handled in one place), the emulator receive the commands
    // and forwards them to the "main". The main will know which screenshot to
    // make based on the fact that the following fields are None or not.

    pub screenshot: Option<PathBuf>,
    pub screenshot_composite: Option<PathBuf>,
    pub screenshot_debug_timing: Option<PathBuf>,
    pub line_to_debug: u32
}

impl Default for OutputQueues {
    fn default() -> Self {
        Self::new()
    }
}

impl OutputQueues {
    // Make it private again
    pub fn new() -> OutputQueues {
        OutputQueues {
            //gfx : VecDeque::new(),
            sound: VecDeque::new(),
            gfx_wip_frame: Screen::new(),
            consumed: false,
            last_time_displayed: Instant::now(),
            rendered_soft_switches: vec![0; 192*65],
            scanned_ram: vec![0; 192*40*2],
            colour_bursts: vec![0; 192],
            done_frames : 0,
            screenshot: None,
            screenshot_composite: None,
            screenshot_debug_timing: None,
            line_to_debug: 0
        }
    }

    pub fn get_gfx_wip_frame(&mut self) -> &mut Screen {
        &mut self.gfx_wip_frame
    }

    // fn frame_done(& mut self) {
    // 	// Black magic ownership transfer. See https://stackoverflow.com/questions/71354545/transfer-ownership-of-vec-to-vecdeque/71355210#71355210
    // 	self.gfx.push_front(take(&mut self.gfx_wip_frame));
    // }
}

#[derive(Clone)]
struct ScriptCommand {
    start_cycle: u64,
    command: Command,
}


#[derive(Clone,Copy)]
pub struct Timers {
    pub frame_duration : Duration,
    pub emu_frame_duration : Duration,
    pub frame_start : std::time::Instant
}

impl Default for Timers {
    fn default() -> Self {
        Self::new()
    }
}

impl Timers {
    pub fn new() -> Timers {
        Timers {
            frame_duration: Duration::from_secs(0),
            emu_frame_duration: Duration::from_secs(0),
            frame_start: std::time::Instant::now()
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct ForcedPaddle {
    pos0 : Option<isize>,
    pos1 : Option<isize>,
    button0: Option<bool>,
    button1 : Option<bool>
}


struct EmulatorState {
    // https://ricardomartins.cc/2016/06/25/interior-mutability-thread-safety

    // The general idea, I guess, is that although I protect
    // the instance with a Mutex, one cannot make guarantees
    // on the other structs connected to the present instance.
    // So Rust forces me to make sure everything is safe down to
    // each descendant struct...

    // Arc : even if I have only one thread, I think rust thinks
    //       I may have several threads having a reference to
    //       the mocking board => Arc instead of Rc
    // RwLock: like RefCell but thread safe

    // RefCell : Sometimes it is required to have multiple references to an object and yet mutate it.
    // My line is that I want several parties to have access
    // to an object => Rc, but at least one of them will
    // mutate that object => Cell. But since I don't want to
    // copy values in and out, just use reference : RefCell.
    // => Rc<RefCell<T>>
    mockingboard: Rc<RefCell<MockingBoard>>,
    disk_controller: Rc<RefCell<DiskController>>,
    //cpu: Cycle6502,
    cpu: Box<dyn Cpu6502>,
    a2: AppleII,
    disassembler: Disassembler,
    debugger: CpuDebugger,
    send_debugging_info: bool,
    //pins: u64,
    mode: ExecutionMode,
    gfx_render_state: GfxRenderState,
    //gfx_scanner: Scanner,
    sample_rate: usize,
    samples_per_frame: usize,
    record_video_frame: Option<usize>,
    record_video_path: Option<PathBuf>,
    turbo_mode: bool,
    video_frames: Vec<Vec<u8>>,
    sound_record: Vec<i16>,
    active_sound_record: Option<PathBuf>,
    active_speaker_record: Option<PathBuf>,
    record_stepper_file: Option<PathBuf>,
    output_queue_sound_size: usize,
    script: Vec<ScriptCommand>,
    device_breakpoints: BreakPointTriggerRef,
    break_point_triggered: bool,
    // The moment where the emulator session has started.
    // Used to pace the emulator. It is possible to reset
    // that time in flight.
    session_begin_time: Instant,

    // The base cycle used to count the time passed in the
    // emulator (see session_begin_time).
    session_begin_cycle: u64,

    // The emulator has quit. If true, none of the emulator
    // works anymore.
    has_quit: bool,

    timers: Timers,
    sound_production: bool,
    take_screenshot: bool,
    video_recording: bool,
    forced_paddle: Option<ForcedPaddle>,
    gfx_debug_line: Option<u32>,
}
use std::string::ToString;
use strum_macros::Display;

#[derive(Display, PartialEq, Clone, Debug)]
pub enum MonoColourSwitch { Mono, Colour}


#[derive(Display, Debug, Clone, PartialEq)]
pub enum Signals {
    Quit,
    FrameAvailable,
    RecordVideoStart(PathBuf),
    TakeScreenshot,
    EmulationPaused
}

#[derive(Display, Debug, Clone)]
pub enum Command {
    LoadFloppy(PathBuf),
    RemoveFloppy,
    ReadRom(PathBuf), // FIXME Move to PathBuf for file path
    ReadCharsetRom(PathBuf),
    KeyUp(Key, Option<u64>),
    KeyDown(Key, Option<u64>),
    Run,
    Quit,
    // Start debugging. From then on, the emulator will send debug frames to the debugger.
    EnterDebugger,
    StartDebugStream,
    GoToPC(u16),
    BreakAtMemoryRead(Option<u16>),
    BreakAtMemoryWrite(Option<u16>),
    BreakAtCycle(u64),
    BreakAtTrack(Option<usize>),
    BreakAtPC(u16),
    BreakAtMemoryLayout,
    BreakAtFloppy,
    RecordDiskStepperStart(PathBuf),
    RecordDiskStepperStop,
    RecordSpeakerStart(PathBuf),
    RecordSpeakerStop,
    RecordSoundStart(PathBuf),
    RecordSoundStop,
    RecordVideoStart(PathBuf),
    RecordVideoStop,
    RecordExecutionTraceStart,
    RecordExecutionTraceStop,
    ScreenshotCrt(PathBuf),
    ScreenshotComposite(PathBuf),
    GlScreenshot(PathBuf),
    SaveGfxDebug(PathBuf),
    SetGfxDebugLine(u32),
    TurboStart,
    TurboStop,
    TurboSwap,
    RunNInstr(usize), // Run n instructions then pause
    RunNCycles(usize), // Run n cycles then pause
    SetScript(String),
    // #[strum(serialize = "LoadMem")]
    StoreMem(PathBuf),         // Dumps all the memory
    LoadMem(PathBuf, u16),
    SaveMem(PathBuf, u16, u16),
    SetPc(u16),
    Pause,
    Reset,
    ControlReset,
    SoundStop,
    ChangeCPUAndReset(CpuType),
    ForcePaddle(Option<ForcedPaddle>),
    Wait(u64),
    AltCharset(bool),
    MotherboardBoardMonoSwitch(MonoColourSwitch)
}

#[derive(PartialEq, Clone, Display, Copy)]
enum ExecutionMode {
    Paused,
    // These two modes are temporarily disabled
    //StepWise, // Run one cycle at a time, will pause right after.
    //FrameWise, // Run one frame at a time, will pause right afterwards
    Run, // Run normally: one frame at a time, at frame rate (50fps/60fps), don't pause if not asked to
    RunTurbo, // Run one frame a time as fast as it can.
}

impl EmulatorState {
    // sample_rate: the rate at which the sound generators
    // must render sound.
    fn new(output_queues: Arc<Mutex<OutputQueues>>, sample_rate: usize) -> EmulatorState {
        //cpu: Box::new(Obscoz6502::new()),
        //cpu: Box::new(Obscoz65C02::new()),
        let cpu = Box::new(Floooh6502::new());
        let base_cycle = cpu.cycles();
        //compute_gfx_mode
        let a2 = AppleII::new(sample_rate, output_queues);
        let init_gfx_mode = a2.compute_gfx_mode();
        EmulatorState {
            mockingboard: Rc::new(RefCell::new(MockingBoard::new(sample_rate))),
            disk_controller: Rc::new(RefCell::new(DiskController::new())),
            cpu,
            a2,
            disassembler: Disassembler::with_verbose_output(),
            debugger: CpuDebugger::new(),
            send_debugging_info: false,
            mode: ExecutionMode::Paused,
            gfx_render_state: GfxRenderState::new(init_gfx_mode),
            //gfx_scanner: Scanner::new(init_gfx_mode),
            sample_rate,
            samples_per_frame: sample_rate / FRAMES_PER_SECOND,
            record_video_frame: None,
            turbo_mode: false,
            video_frames: Vec::new(),
            record_video_path: None,
            sound_record: Vec::new(),
            active_sound_record: None,
            active_speaker_record: None,
            record_stepper_file: None,
            output_queue_sound_size: 0,
            script: Vec::new(),
            device_breakpoints: Rc::new(RefCell::new(BreakPointTrigger::new())),
            break_point_triggered: false,
            session_begin_time: Instant::now(),
            session_begin_cycle: base_cycle,
            has_quit: false,
            timers: Timers::new(),
            sound_production: true,
            take_screenshot: false,
            video_recording: false,
            forced_paddle: None,
            gfx_debug_line: None,
        }
    }

    fn cycles(&self) -> u64 {
        self.cpu.cycles()
        //return self.cpu.cycles
    }

    fn parse_script(&mut self, s: &String) {
        //load_symbols("additional/symbols");

        let mut cycle: u64 = 0;

        info!("Script: '{}'", s);
        for cmd_rough in s.split(',') {
            info!("Parsing: {}", cmd_rough);

            let cmd: Vec<&str> = cmd_rough.split_ascii_whitespace().collect();

            match cmd[0].to_uppercase().as_str() {
                "WAIT_UNTIL" => {
                    let d = cmd[1].parse::<f32>().expect(
                        "A number of seconds (possibly float) since beginning of execution",
                    );
                    // Seconds to cycles
                    let seconds_as_cycles = (d * (CYCLES_PER_SECOND as f32)) as u64;

                    if seconds_as_cycles >= cycle {
                        info!("Wait until {}", cmd[1]);

                        // This will have the effect of delaying the next script
                        // commands.
                        cycle = seconds_as_cycles;
                    } else {
                        error!("The moment you wait up to is already passed");
                    }
                }
                "WAIT" => {
                    let d = cmd[1].parse::<f32>().expect(
                        "A number of seconds (possibly float) since beginning of execution",
                    );
                    // Seconds to cycles
                    let seconds_as_cycles = (d * (CYCLES_PER_SECOND as f32)) as u64;
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::Wait(seconds_as_cycles),
                    });
                }
                "BP_CYCLE" => {
                    let d = usize::from_str_radix(cmd[1], 10)
                        .expect("A number of cycles since beginning of execution");
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::BreakAtCycle(d as u64),
                    });
                }
                "BP_PC" => {
                    let pc = u16::from_str_radix(cmd[1], 16).expect("A hex offset");
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::BreakAtPC(pc),
                    });
                }
                "BP_TRACK" => {
                    // FIXME I should decide once for all of how
                    // I represent tracks : usize*4 or float...

                    let trk = cmd[1].parse::<f64>().unwrap();

                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::BreakAtTrack(Some((trk * 4.0) as usize)),
                    });
                }
                "SPEAKER_REC_START" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordSpeakerStart(PathBuf::from(cmd[1])),
                    });
                    info!("SPEAKER_REC_START")
                }
                "SPEAKER_REC_STOP" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordSpeakerStop,
                    });

                    info!("SPEAKER_REC_START")
                }
                "SOUND_REC_START" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordSoundStart(PathBuf::from(cmd[1])),
                    });
                    info!("SOUND_REC_START")
                }
                "SOUND_REC_STOP" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordSoundStop,
                    });
                    info!("SOUND_REC_STOP")
                }
                "VIDEO_REC_START" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordVideoStart(PathBuf::from(cmd[1])),
                    });
                    info!("VIDEO_REC_START")
                }
                "VIDEO_REC_STOP" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordVideoStop,
                    });
                    info!("VIDEO_REC_STOP")
                }
                "SCREENSHOT" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::ScreenshotCrt(PathBuf::from(cmd[1])),
                    });
                }
                "IOU_GFX_INPUT" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::GlScreenshot(PathBuf::from(cmd[1])),
                    });
                }
                "SET_GFX_DEBUG_LINE" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::SetGfxDebugLine(
                            cmd[1].parse::<u32>().unwrap()
                        ),
                    });
                }
                "SAVE_GFX_DEBUG" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::SaveGfxDebug(PathBuf::from(cmd[1])),
                    });
                }
                "KEYS" => {
                    // command is: "KEY <sequence of keys>"
                    let mut add_cycle = 0;
                    for k in (*cmd[1]).chars() {
                        debug!("key: {}", k);
                        let key = match k {
                            '~' => {
                                // Trying to pass it through \n but cargo messes up somewhere
                                Key::Character(' ')
                            }
                            _ => Key::Character(k),
                        };

                        self.script.push(ScriptCommand {
                            start_cycle: cycle + add_cycle,
                            command: Command::KeyDown(key, None),
                        });
                        add_cycle += 100000;
                        self.script.push(ScriptCommand {
                            start_cycle: cycle + add_cycle,
                            command: Command::KeyUp(key, None),
                        });
                        add_cycle += 100000;
                    }
                    cycle += add_cycle + 100000;
                }
                "KEY_RETURN" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::KeyDown(Key::Return, None),
                    });
                    self.script.push(ScriptCommand {
                        start_cycle: cycle + 100000,
                        command: Command::KeyUp(Key::Return, None),
                    });
                    cycle += 100000;
                }
                "KEY_RIGHT" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::KeyDown(Key::Right, None),
                    });
                    self.script.push(ScriptCommand {
                        start_cycle: cycle + 100000,
                        command: Command::KeyUp(Key::Right, None),
                    });
                    cycle += 100000;
                }
                "KEY_SPACE" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::KeyDown(Key::Space, None),
                    });
                    self.script.push(ScriptCommand {
                        start_cycle: cycle + 100000,
                        command: Command::KeyUp(Key::Space, None),
                    });
                    cycle += 100000;
                }
                "QUIT" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::Quit,
                    });
                }
                "TURBO_START" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::TurboStart,
                    });
                }
                "TURBO_STOP" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::TurboStop,
                    });
                }
                "PAUSE" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::Pause,
                    });
                }
                "RUN" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::Run,
                    });
                }
                "RESET" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::Reset,
                    });
                    cycle = 0;
                }
                "SET_PC" => {
                    let pc = u16::from_str_radix(cmd[1], 16).expect("A hex offset");
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::SetPc(pc),
                    });
                }
                "LOAD_MEM" => {
                    info!("LOAD_MEM:  path:'{}' address:$'{}'", cmd[1], cmd[2]);
                    let addr = u16::from_str_radix(cmd[2], 16)
                        .unwrap_or_else(|_| panic!("Require hex offset instead of '{}'.", cmd[2]));

                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::LoadMem(PathBuf::from(cmd[1]), addr),
                    });
                }
                "SAVE_ALL_MEM" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::StoreMem(PathBuf::from(cmd[1])),
                    });
                }
                "SAVE_MEM" => {
                    info!(
                        "SAVE_MEM:  path:'{}' address:$'{}' length:$'{}'",
                        cmd[1], cmd[2], cmd[3]
                    );
                    let addr = u16::from_str_radix(cmd[2], 16)
                        .unwrap_or_else(|_| panic!("Require hex offset instead of '{}'.", cmd[2]));

                    let len = u16::from_str_radix(cmd[3], 16)
                        .unwrap_or_else(|_| panic!("Require hex offset instead of '{}'.", cmd[3]));

                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::SaveMem(PathBuf::from(cmd[1]), addr, len),
                    });
                }
                "D2_STEPPER_REC_START" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordDiskStepperStart(PathBuf::from(cmd[1])),
                    });
                }
                "D2_STEPPER_REC_STOP" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordDiskStepperStop,
                    });
                }
                "EXEC_TRACE_START" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordExecutionTraceStart,
                    });
                }
                "EXEC_TRACE_STOP" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::RecordExecutionTraceStop,
                    });
                },
                "JOYSTICK" => {
                    fn parse_pos(v: &str) -> Option<isize> {
                        if v.eq_ignore_ascii_case("none") {
                            None
                        } else if let Ok(integer) = v.parse::<isize>() {
                            return Some(integer);
                        } else if let Ok(float) = v.parse::<f32>() {
                            return Some((float*127.0) as isize);
                        } else {
                            error!("Can't parse joystick position {}", v);
                            return None;
                        }
                    }

                    fn parse_button(v: &str) -> Option<bool> {
                        if v.eq_ignore_ascii_case("none") {
                            None
                        } else if v.eq_ignore_ascii_case("1") || v.eq_ignore_ascii_case("p") || v.eq_ignore_ascii_case("pushed") {
                            return Some(true)
                        } else if v.eq_ignore_ascii_case("0") || v.eq_ignore_ascii_case("r") || v.eq_ignore_ascii_case("released") {
                            return Some(false)
                        } else {
                            error!("Can't parse joystick button state {}", v);
                            return None;
                        }
                    }

                    if cmd[1].eq_ignore_ascii_case("stop") {
                        self.script.push(ScriptCommand {
                            start_cycle: cycle,
                            command: Command::ForcePaddle(
                                None),
                            });
                    } else {
                        self.script.push(ScriptCommand {
                            start_cycle: cycle,
                            command: Command::ForcePaddle(
                                Some(ForcedPaddle {
                                    pos0: parse_pos(cmd[1]),
                                    pos1: parse_pos(cmd[2]),
                                    button0: parse_button(cmd[3]),
                                    button1: parse_button(cmd[4]),
                                })),
                            });
                    }
                }
                "LOAD_FLOPPY" => {
                    self.script.push(ScriptCommand {
                        start_cycle: cycle,
                        command: Command::LoadFloppy(PathBuf::from(cmd[1]))
                    });
                }
                _ => error!("Unrecognized command \"{}\"", cmd_rough.to_string().trim()),
            }

            //cycle += 1; // CYCLES_PER_FRAME as u64;
        }
    }
}

fn power_up_reset(em_state: &mut EmulatorState) {
    em_state.a2.power_up_reset(em_state.cycles());
    em_state.cpu.pull_reset_pin_down();

    // Reset the cycle counter to zero
    em_state.cpu.reset_cycles();

    debug!("CPU.S = {:02X}", em_state.cpu.s());

    // https://www.pagetable.com/?p=410
    // "Cycle 0: When a 6502 is turned on, the stack pointer is initialized with zero."
    // em_state.cpu.S = 0;
    // This was guessed :-/
    //em_state.pins |= M6502_SYNC | M6502_RES;

    //let b= em_state.a2.speaker.borrow_mut().get_mut();
    em_state.a2.speaker.borrow_mut().reset_ticks();
    em_state.disk_controller.borrow_mut().reset();

    for i in 0..NB_SLOTS {
        em_state.a2.reset_slot(i);
    }
}

use crate::speaker2::SpeakerRenderState;
use std::cell::RefMut;

fn save_speaker_recording(
    mut record_borrow: RefMut<SpeakerRenderState>,
    speaker_record_name: &PathBuf,
) {
    //let path = Path::new(&speaker_record_name);
    let path = speaker_record_name.with_extension("ticks");
    //let path: &Path = Path::new("sound.bin");
    let s = record_borrow.recorded_ticks.as_slice();
    info!(
        "Saving ticks in {}, {} ticks",
        path.to_string_lossy(),
        s.len()
    );

    if true {
        // Record as sequence of speaker tick cycles
        let output: Vec<u8> = s.iter().flat_map(|val| val.to_be_bytes()).collect();
        fs::write(path.as_path(), output).unwrap();
    } else {
        // record as a sequence of 0/1 on each cycle
        let mut v: Vec<u8> = Vec::new();
        let mut l = 0;
        for ofs in s {
            for _j in 0..((*ofs as usize) - v.len()) {
                v.push(l)
            }
            if l == 0 {
                l = 255;
            } else {
                l = 0;
            }
        }
        fs::write(path.as_path(), v).unwrap();
    }
    info!(
        "Wrote sound buffer with {} records in {}",
        record_borrow.recorded_ticks.len(),
        path.as_path().to_string_lossy()
    );
    record_borrow.recorded_ticks.clear();
}

fn save_screenshot(path: &Path, screen: &Screen) {
    let file = File::create(path).unwrap();
    let w = &mut BufWriter::new(file);
    let width = SCREEN_WIDTH + 2;

    let mut encoder = png::Encoder::new(w, width as u32, 192);
    encoder.set_color(png::ColorType::Rgba);
    encoder.set_depth(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();

    writer
        .write_image_data(&screen.ptr.deref()[60 * width * 4..(60 + 192) * width * 4])
        .unwrap(); // Save
}

pub fn save_i16_to_wav_stereo(bytes: Vec<i16>, path: &Path, sample_rate: usize) {
    let mut path = path.to_path_buf();
    path.set_extension("wav");
    info!(
        "Saving sound in {}, {} bytes",
        path.to_string_lossy(),
        bytes.len()
    );
    let mut out_file = File::create(path.as_path()).ok().unwrap();
    wav::write(
        wav::Header::new(
            wav::header::WAV_FORMAT_PCM,
            2, /* channels */
            sample_rate as u32,
            16, /* bits */
        ),
        &wav::BitDepth::Sixteen(bytes),
        &mut out_file,
    )
    .ok();
}

fn save_sound(em_state: &mut EmulatorState, path: &PathBuf) {
    let bytes = em_state.sound_record.to_owned();

    let corrected_path = path.with_extension("wav");
    info!(
        "Saving sound in {}, {} bytes",
        corrected_path.to_string_lossy(),
        bytes.len()
    );
    let mut out_file = File::create(corrected_path.as_path()).ok().unwrap();
    wav::write(
        wav::Header::new(
            wav::header::WAV_FORMAT_PCM,
            2, /* channels */
            em_state.sample_rate as u32,
            16, /* bits */
        ),
        &wav::BitDepth::Sixteen(bytes),
        &mut out_file,
    )
    .ok();
    em_state.sound_record.clear();
}


fn reset_session_timings(em_state: &mut EmulatorState) {
    em_state.session_begin_time = Instant::now();
    em_state.session_begin_cycle = em_state.cycles();
}


fn make_key_cycle(mut key_cycle_emu: u64, last_key_cycle: u64, frame_start_cycle: u64) -> u64 {
    const CYCLES_PER_FRAME_U64: u64 = CYCLES_PER_FRAME as u64;

    let mut key_cycle = key_cycle_emu;

    // The problem of jitter is that I have no idea of the offset
    // there is between the main thread and the emulator.

    // Handle jitter between keyboard events times and emulator's inner time
    while key_cycle > frame_start_cycle + CYCLES_PER_FRAME_U64  {
        key_cycle -= CYCLES_PER_FRAME_U64
    }

    while key_cycle < frame_start_cycle   {
        key_cycle += CYCLES_PER_FRAME_U64
    }

    // make sure the key event chronology is maintained
    let r = key_cycle.max(last_key_cycle);

    //println!("make_key_cycle: {} becomes {}, frame_start_cycle={} ", key_cycle_emu, r, frame_start_cycle);
    return r;
}


fn process_command(command: &Option<Command>, em_state: &mut EmulatorState,
    thread_oq: &Arc<Mutex<OutputQueues>>,
    quit_tx: &Sender<Signals>,
    mut keys_cycle: u64) -> (bool, u64) {
    // Return true if the debugger must be informed of the event
    let mut inform_ui_of_debugger_status_change = false;
    if command.is_some() {
        match command.as_ref().unwrap() {
            Command::Run => {
                //println!("Run command");
                em_state.mode = ExecutionMode::Run;
                em_state.debugger.proceed_instructions = None;
                em_state.debugger.proceed_ticks = None;
                // em_state.debugger.breakpoint = None;
                // em_state.debugger.breakpoint_mem = None;
                // em_state.debugger.cycle_breakpoint = None;
                reset_session_timings(em_state);
            }
            Command::KeyDown(key, cycle) => {
                if em_state.mode != ExecutionMode::Paused {

                    if let Some(key_cycle) = cycle {
                        let kc = make_key_cycle(*key_cycle, em_state.a2.keyboard_queue.last_cycle(), em_state.cycles());
                        //println!("emulator.KeyDown: {} host_cycle:{} emu_cycle:{}", key, key_cycle, kc);
                        em_state.a2.keyboard_pressed( kc, *key);
                    } else {
                        em_state.a2.record_key_event(keys_cycle, *key);
                        em_state.a2.keyboard_pressed( cycle.unwrap_or(keys_cycle), *key);
                        keys_cycle += 100000;
                    }
                }
            }
            Command::KeyUp(key, cycle) => {
                if em_state.mode != ExecutionMode::Paused {
                    if let Some(key_cycle) = cycle {
                        let kc = make_key_cycle(*key_cycle, em_state.a2.keyboard_queue.last_cycle(), em_state.cycles());
                        //println!("emulator.KeyUp: {} host_cycle:{} emu_cycle:{}", key, key_cycle, kc);
                        em_state.a2.keyboard_released( kc, *key);
                    } else {
                        em_state.a2.keyboard_released( cycle.unwrap_or(keys_cycle), *key);
                        keys_cycle += 100000;
                    }
                }
            }
            Command::RemoveFloppy => {
                em_state.disk_controller.borrow_mut().remove_disk(0);
            }
            Command::LoadFloppy(path) => {
                if path.exists() {
                    let mut controller = em_state.disk_controller.borrow_mut();
                    controller.load_disk(0, path);
                    info!("Loaded floppy {}", path.as_os_str().to_string_lossy());

                    if let Some(checksum) = controller.woz(0).checksum() {
                        info!("Disk checksum {:08X}", checksum);

                        match checksum {
                            0x3C96677D => error!("Falcon emulation has keyboard problems"),
                            0xC153B8A5 => error!("Obscoz's Mario needs 65C02 opcodes !"),
                            0x5E98EE17 => error!("Golombeck's Minesweeper requires 65C02"),
                            _ => (),
                        }
                    }
                    inform_ui_of_debugger_status_change = true;
                    // power_up_reset(&mut em_state);
                    // begin_time = Instant::now();
                    // begin_cycle = em_state.cycles();

                } else {
                    error!("DSK file not found : {}", path.to_string_lossy());
                }
            }
            Command::Reset => {
                power_up_reset(em_state);
                reset_session_timings(em_state);
            }
            Command::ControlReset => {}
            Command::ReadCharsetRom(path) => {
                em_state.a2.read_charset_rom(path).expect("load ok");
                info!("Loaded charset ROM {}", path.as_os_str().to_string_lossy());
            }
            Command::ReadRom(path) => {
                em_state.a2.read_roms(path);
                info!("Loaded ROM {}", path.as_os_str().to_string_lossy());
            }
            Command::Quit => {
                info!("Quitting. {} cycles ran", em_state.cycles());
                em_state.has_quit = true;
            }
            Command::EnterDebugger => {
                // This will trigger an immediate break point.
                //em_state.debugger.breakpoint_pc = Some(em_state.cpu.last_instr_pc);
                em_state.debugger.trace = true;
            }
            Command::StartDebugStream => {
                em_state.send_debugging_info = true;
            }
            Command::GoToPC(addr) => {
                trace!("Command::GoToPC {:04X}", *addr);
                em_state.debugger.breakpoint_pc = Some(*addr);

                em_state.debugger.trace = true;
                // Same code as ::Run command. Make sure to duplicate (yirks!)
                em_state.mode = ExecutionMode::Run;
                em_state.debugger.proceed_instructions = None;
                reset_session_timings(em_state);
                inform_ui_of_debugger_status_change = true;
            }
            Command::BreakAtPC(addr) => {
                em_state.debugger.breakpoint_pc = Some(*addr);
                em_state.debugger.proceed_instructions = None;
                em_state.debugger.trace = true;
                // Same code as ::Run command. Make sure to duplicate (yirks!)
                em_state.mode = ExecutionMode::Run;
                em_state.debugger.proceed_instructions = None;
                inform_ui_of_debugger_status_change = true;
            }
            Command::BreakAtMemoryWrite(addr) => {
                inform_ui_of_debugger_status_change = true;
                em_state.debugger.breakpoint_mem_write = *addr;
                em_state.debugger.proceed_instructions = None;
            }
            Command::BreakAtMemoryRead(addr) => {
                inform_ui_of_debugger_status_change = true;
                //println!("BreakAtMemoryRead");
                em_state.debugger.breakpoint_mem_read = *addr;
                em_state.debugger.proceed_instructions = None;
            }
            Command::BreakAtCycle(cycle) => {
                inform_ui_of_debugger_status_change = true;
                em_state.debugger.breakpoint_cycle = Some(*cycle);
                em_state.mode = ExecutionMode::Run;
                em_state.debugger.proceed_instructions = None;
                reset_session_timings(em_state);
            }
            Command::BreakAtTrack(trk) => {
                inform_ui_of_debugger_status_change = true;
                em_state
                    .device_breakpoints
                    .borrow_mut()
                    .enable_break_on_floppy_track(*trk);
                em_state.debugger.proceed_instructions = None;
            }
            Command::BreakAtFloppy => {
                inform_ui_of_debugger_status_change = true;
                em_state
                    .device_breakpoints
                    .borrow_mut()
                    .enable_break_on_floppy();
                em_state.debugger.proceed_instructions = None;
            }
            Command::BreakAtMemoryLayout => {
                inform_ui_of_debugger_status_change = true;
                em_state.debugger.breakpoint_memory_layout = true;
            }
            Command::RunNInstr(n) => {
                //println!("Run {} instruction(s)", n);
                em_state.mode = ExecutionMode::Run;
                em_state.debugger.proceed_instructions = Some(*n);
                em_state.debugger.trace = true;
                inform_ui_of_debugger_status_change = true;
            }
            Command::RunNCycles(n) => {
                //println!("Run {} instruction(s)", n);
                em_state.mode = ExecutionMode::Run;
                em_state.debugger.proceed_ticks = Some(*n);
                em_state.debugger.trace = true;
                inform_ui_of_debugger_status_change = true;
            }
            Command::RecordSpeakerStart(path) => {
                em_state.active_speaker_record = Some(path.clone());
                let mut speaker = em_state.a2.speaker.borrow_mut();
                speaker.start_recording();
                info!("Speaker recording has started");
            }
            Command::RecordSpeakerStop => {
                if em_state.active_speaker_record.is_some() {
                    let mut speaker = em_state.a2.speaker.borrow_mut();
                    speaker.stop_recording();
                    save_speaker_recording(speaker, em_state.active_speaker_record.as_ref().unwrap());
                    info!("Speaker recording complete");
                } else {
                    error!("Stopping a speaker ticks recording but you didn't start it");
                }
            }
            Command::RecordSoundStart(path) => {
                em_state.active_sound_record = Some(path.clone());
                info!("Sound recording started");
            }
            Command::RecordSoundStop => {
                if em_state.active_sound_record.is_some() {
                    let p = em_state.active_sound_record.as_ref().unwrap().clone();
                    save_sound(em_state, &p);
                    em_state.active_sound_record = None;
                    info!("Sound recording complete");
                } else {
                    error!("Stopping a sound recording but you didn't start it");
                }
            }
            Command::RecordVideoStart(name) => {
                quit_tx.send(Signals::RecordVideoStart(name.clone())).ok();
                em_state.record_video_frame = Some(1);
                em_state.record_video_path = Some(name.clone());
                info!("Video recording start with base file name {}", name.to_string_lossy());
            }
            Command::RecordVideoStop => {
                if em_state.record_video_frame.is_some() {
                    em_state.record_video_frame = None;
                    em_state.record_video_path = None;
                    info!("Video recording stopped");
                } else {
                    error!("Stopping a video recording but you didn't start it");
                }
            }
            Command::ScreenshotCrt(name) => {
                let mut dest = PathBuf::new();
                dest.set_file_name(name);
                dest.set_extension("png");
                info!("Requesting a CRT screenshot to the host rendered to save into {}", dest.as_path().to_string_lossy());
                //quit_tx.send(Signals::TakeScreenshot).ok();
                let mut oq = thread_oq.lock().expect("locking output queues");
                oq.screenshot = Some(dest);
            }
            Command::ScreenshotComposite(name) => {
                let mut dest = PathBuf::new();
                dest.set_file_name(name);
                dest.set_extension("png");
                info!("Requesting a composite screenshot to the host rendered to save into {}", dest.as_path().to_string_lossy());
                let mut oq = thread_oq.lock().expect("locking output queues");
                oq.screenshot_composite = Some(dest);
            }
            Command::GlScreenshot(name) => {
                let oq = thread_oq.lock().expect("locking output queues");
                let path: &Path = Path::new(name);
                info!("Writing graphics generation data dump to {}", path.to_string_lossy());
                let mut f = File::create(path).unwrap();
                info!("Soft switches are {} bytes", oq.rendered_soft_switches.len());
                f.write_all(oq.rendered_soft_switches.as_slice()).ok();
                info!("Scanned RAM is {} bytes", oq.scanned_ram.len());
                f.write_all(oq.scanned_ram.as_slice()).ok();
            }
            Command::SetGfxDebugLine(line) => {
                let mut oq = thread_oq.lock().expect("locking output queues");
                println!("Setting debug line {}", *line);
                oq.line_to_debug = *line;
                em_state.gfx_debug_line = Some(*line);
            }
            Command::SaveGfxDebug(name) => {
                let mut oq = thread_oq.lock().expect("locking output queues");
                let path: &Path = Path::new(name);
                oq.screenshot_debug_timing = Some(PathBuf::from(path));
            }
            Command::StoreMem(p) => {

                let path = p.with_extension("bin");
                info!("Writing memory dump to {}", path.to_string_lossy());
                let mut f = File::create(path).unwrap();

                info!("Writing 65536 bytes of RAM (at 0x0)");
                f.write_all(em_state.a2.ram()).ok(); // 64K
                info!("Writing  4096 bytes of BANK2 (at 0x10000)");
                f.write_all(em_state.a2.ram_bank2()).ok(); // 4K D000:E000
                info!("Writing 65536 bytes of AUXRAM (at 0x11000)");
                f.write_all(em_state.a2.aux_ram()).ok(); // 64K
                info!("Writing  4096 bytes of AUX BANK2 (at 0x21000)");
                f.write_all(em_state.a2.aux_ram_bank2()).ok(); // 4K D000:E000
                f.flush().ok();
            }
            Command::TurboStart => {
                info!("Script: Turbo start");
                em_state.turbo_mode = true
            }
            Command::TurboStop => {
                info!("Script: Turbo stop");
                em_state.turbo_mode = false;
            }
            Command::TurboSwap => {
                //println!("turbo swap {}",em_state.turbo_mode);
                em_state.turbo_mode = !em_state.turbo_mode;
            }
            Command::SetScript(s) => {
                em_state.parse_script(s);
            }
            Command::LoadMem(path, addr) => {
                em_state
                    .a2
                    .load_ram(path, *addr as usize, em_state.cycles() + 1000);
            }
            Command::SaveMem(path, addr, len) => {
                em_state.a2.save_ram(path, *addr as usize, *len as usize);
            }
            Command::SetPc(pc) => {
                info!("Setting PC to ${:04X}", pc);
                let cycle = em_state.cycles();
                let cpu = em_state.cpu.as_mut();
                let machine = &mut em_state.a2;
                let (b, irq_change) = machine.readb_cycle(*pc, false, cycle);
                assert!(irq_change.is_none(), "I said 'no side effect'");
                cpu.set_pc(*pc, b);
                handle_bus(cpu, machine);

                fn short_tick(cpu: &mut dyn Cpu6502, pc: u16, machine: &mut AppleII, debugger: &mut CpuDebugger) {
                    cpu.tick();
                    track_calls(cpu, machine, debugger);
                    if cpu.address() == 0xFFFF {
                        cpu.set_data( (pc >> 8) as u8);
                    } else if cpu.address() == 0xFFFE {
                        cpu.set_data( (pc & 0xFF) as u8);
                    } else if cpu.is_reading() {
                        let (b, _irq_change) = machine.readb_cycle(cpu.address(), true, cpu.cycles());
                        cpu.set_data(b);
                    } else if cpu.is_writing() {
                        let (_, _irq_change) = machine.storeb(cpu.address(), cpu.data(), cpu.cycles());
                    }
                }

                // Make sure we're not currently executing a break
                while cpu.ir() == 0 {
                    short_tick(cpu, *pc, machine, &mut em_state.debugger);
                }

                // Feed BRK in the CPU until it recognizes it
                while cpu.ir() != 0 {
                    //trace!("at {}, ad={:04X}",cpu.cycles(), cpu.address());
                    cpu.set_data(0x00);
                    cpu.tick();
                }

                // Proceed until the PC is reached by the BRK procedure.
                for _i in 0..6 {
                    //trace!("2> at {}, ad={:04X}",cpu.cycles(), cpu.address());
                    short_tick(cpu, *pc, machine, &mut em_state.debugger);
                }

                em_state.mode = ExecutionMode::Paused;
                inform_ui_of_debugger_status_change = true;
            }
            Command::Pause => {
                debug!("Entering pause mode from {}",em_state.mode);
                em_state.mode = ExecutionMode::Paused;
                // In a pause, I find it convenient to have the debugger to show up
                // FIXME this is not super right because the
                inform_ui_of_debugger_status_change = true;
            }
            Command::RecordDiskStepperStart(path) => {
                em_state.record_stepper_file = Some(path.clone());
                em_state
                    .disk_controller
                    .borrow_mut()
                    .start_stepper_logging();
                info!("Disk stepper recording started to {}", path.to_string_lossy());
            }
            Command::RecordDiskStepperStop => {
                if em_state.record_stepper_file.is_some() {
                    em_state
                        .disk_controller
                        .borrow_mut()
                        .stop_stepper_logging(em_state.record_stepper_file.as_ref().unwrap());
                    em_state.record_stepper_file = None;
                    info!("Disk stepper recording stopped");
                } else {
                    error!("Stopping a disk stepper recording but you didn't start it");
                }
            }
            Command::RecordExecutionTraceStart => {
                info!("Starting recording of execution trace");
                em_state.debugger.execution_trace = Some(Vec::new());
            }
            Command::RecordExecutionTraceStop => {
                if let Some(exec_trace) = em_state.debugger.execution_trace.as_ref() {
                    let f = File::create("trace.txt").expect("Unable to create file");
                    let mut f = BufWriter::new(f);
                    let _numbers: Result<Vec<_>, _> = exec_trace
                        .iter()
                        .map(|snap| {
                            writeln!(
                                f,
                                "{:9} PC:{:04X} A:{:02X} X:{:02X}",
                                snap.cycle, snap.pc, snap.a, snap.x
                            )
                        })
                        .collect();
                    em_state.debugger.execution_trace = None;
                    info!("Stopped recording of execution trace");
                } else {
                    warn!("You stopped the execution trace recording but you didn't started it in the first place.")
                }

                //em_state.debugger.execution_trace
            },
            Command::SoundStop => em_state.sound_production = false,
            Command::ChangeCPUAndReset(cpu_type) => {
                em_state.cpu = match cpu_type {
                    CpuType::Floooh6502 =>  Box::new(Floooh6502::new()),
                    CpuType::Obscoz6502 => Box::new(Obscoz6502::new()),
                    CpuType::Obscoz65C02 => Box::new(Obscoz65C02::new())
                };
                power_up_reset(em_state);
                reset_session_timings(em_state);
            },
            Command::ForcePaddle(forced) => {
                if let Some(f) = forced {
                    //println!("Command::ForcePaddle");
                    em_state.forced_paddle = Some(*f);
                } else {
                    em_state.forced_paddle = None;
                }
            },
            Command::Wait(cycles) => {
                panic!("Not implemented yet !!! Remember that WAIT_UNTIL clears the cycle counter...");
            },
            Command::AltCharset(v) => {
                //println!("sel althcarset {}", *v);
                if *v {
                    em_state.a2.altcharset_select = ALTCHARSETSelect::On;
                } else {
                    em_state.a2.altcharset_select = ALTCHARSETSelect::Off;
                }
                let z = em_state.a2.gfx_mode_change_clock_event(em_state.cycles());
				em_state.a2.clock_events.push_front(z);
            },
            Command::MotherboardBoardMonoSwitch(switch) => {
                em_state.a2.set_motherboard_mono_colour_switch(switch.clone());
            }

        }
    }

    (inform_ui_of_debugger_status_change, keys_cycle)
}


/// Starts the emulator in pause mode.
///
/// Returns a ref. to a mutex to the [`OutputQueues`] needed to
/// get the data to transfer to the host hardware (gfx, sound).
/// Returns a control channel where one can send commands to
/// control the emulator
pub fn start_emulator_thread(
    sample_rate: usize,
    cpu_type: CpuType,
    quit_tx: Sender<Signals>,
    mut initial_commands:  Vec<Command>
) -> (
    Arc<Mutex<OutputQueues>>,
    Sender<Command>,
    Receiver<DebugFrame>,
    thread::JoinHandle<()>,
) {
    let (tx, rx): (Sender<Command>, Receiver<Command>) = channel();
    let (debug_stream_tx, debug_stream_rx): (Sender<DebugFrame>, Receiver<DebugFrame>) = channel();

    let output_queues: Arc<Mutex<OutputQueues>> = Arc::new(Mutex::new(OutputQueues::new()));
    let thread_oq = output_queues.clone();
    let emstate_oq = output_queues.clone(); // Will go to AppleII and ultimately to sound generators

    // FIXME There's unsafe code there
    crate::rs6502::opcodes::init_addressing_modes();

    //let handle = thread::spawn(move || {
    let emulator_thread_handle = thread::Builder::new()
        .name("AccurApple".to_string())
        .spawn(move || {
            let mut em_state = EmulatorState::new(emstate_oq, sample_rate);

            em_state.cpu = match cpu_type {
                CpuType::Floooh6502 =>  Box::new(Floooh6502::new()),
                CpuType::Obscoz6502 => Box::new(Obscoz6502::new()),
                CpuType::Obscoz65C02 => Box::new(Obscoz65C02::new())
            };


            //em_state.debugger.breakpoint = Some(0x800);

            em_state.a2.add_peripheral(em_state.mockingboard.clone());
            em_state.a2.add_peripheral(em_state.disk_controller.clone());

            //let bpt = Rc::new( RefCell::new( BreakPointTrigger::new()));
            /*{
                em_state
                    .device_breakpoints
                    .borrow_mut()
                    .break_on_floppy_track(21 * 4);
            };*/
            em_state.a2.set_breakpoints(em_state.device_breakpoints.clone());

            while !initial_commands.is_empty() {
                em_state.script.push(
                    ScriptCommand {
                        start_cycle: 0,
                        command: initial_commands.pop().unwrap()
                    });
                    debug!("Preloading command {}", em_state.script.last().unwrap().command);
            }

            // One loop per video frame. So conceptually, we
            // run one frame at a time. Everything is hardwired
            // to that end.

            let mut gilrs = Gilrs::new().unwrap();

	    for (_id, gamepad) in gilrs.gamepads() {
		    info!("GamePad: {} ID={:02X?} is {:?}", gamepad.name(), gamepad.uuid(), gamepad.power_info());
	    }


            loop { // Thread loop
                if em_state.has_quit {
                    info!("Quitting emulator thread");
                    break;
                }

                let current_mode = em_state.mode;
                let current_turbo = em_state.turbo_mode;

                let mut inform_ui_of_debugger_status_change = false;
                let mut keys_cycle = em_state.cycles();

                // Consume all available commands before the next frame.

                loop {
                    let r = rx.try_recv();
                    let command = if r.is_ok() {
                        // Interaction coming from the user thread
                        r.ok()
                    } else if let Some(cmd) = em_state.script.first() {
                        // OR it may come from a script
                        // Each command coming from a script has a time (in cycles)
                        // where it must start. We ignore the commands which are in the future
                        // (and will process them at the right moment).
                        if cmd.start_cycle <= em_state.cycles() {
                            Some(em_state.script.remove(0).command.clone())
                            //Some(cmd.command.clone())
                        } else {
                            break;
                        }
                    } else {
                        break;
                    };

                    info!("At cycle {}, PC={:04X}, {} processing command {}", em_state.cycles(),  em_state.cpu.pc(),
                        em_state.mode, command.as_ref().unwrap());

                    let (inform_debugger, nks) = process_command(
                        &command, &mut em_state,
                        &thread_oq,  &quit_tx,
                        keys_cycle);

                    inform_ui_of_debugger_status_change |= inform_debugger;
                    keys_cycle = nks;
                }


                if let Some(ref paddle) = em_state.forced_paddle {
                    if let Some(pos0) = paddle.pos0.as_ref() {
                        em_state.a2.set_analog_input(0, (128 + *pos0) as usize)
                    }
                    if let Some(pos1) = paddle.pos1.as_ref() {
                        em_state.a2.set_analog_input(1, (128 + *pos1) as usize)
                    }
                    if let Some(button0) = paddle.button0.as_ref() {
                        em_state.a2.set_switch_input(0, *button0)
                    }
                    if let Some(button1) = paddle.button1.as_ref() {
                        em_state.a2.set_switch_input(1, *button1)
                    }

                } else {

                    while let Some(Event { id: _, event, time: _, .. }) = gilrs.next_event() {
                        //println!("{:?} New event from {}: {:?}", time, id, event);
                        match event {
                    // https://stackoverflow.com/questions/13211595/how-can-i-convert-coordinates-on-a-circle-to-coordinates-on-a-square
                            EventType::AxisChanged(Axis::LeftStickX, value, _) => em_state
                                .a2
                                .set_analog_input(0, (125.0 + value * 125.0) as usize),
                            EventType::AxisChanged(Axis::LeftStickY, value, _) => em_state
                                .a2
                                .set_analog_input(1, (125.0 - value * 125.0) as usize),
                            EventType::ButtonPressed(Button::West, _)
                            | EventType::ButtonPressed(Button::North, _) => {
                    //println!("Button west/north");
                                em_state.a2.set_switch_input(1, true);
                            }
                            EventType::ButtonReleased(Button::West, _)
                            | EventType::ButtonReleased(Button::North, _) => {
                                em_state.a2.set_switch_input(1, false);
                            }
                            EventType::ButtonPressed(Button::South, _)
                            | EventType::ButtonPressed(Button::East, _) => {
                    //println!("Button south/east");
                                em_state.a2.set_switch_input(0, true);
                            }
                            EventType::ButtonReleased(Button::South, _)
                            | EventType::ButtonReleased(Button::East, _) => {
                                em_state.a2.set_switch_input(0, false);
                            }
                            EventType::ButtonPressed( _, _ ) => {
                                warn!("I don't recognize your joystick button, only button 0 will work!");
                                em_state.a2.set_switch_input(0, true);
                            }
                            EventType::ButtonReleased( _, _) => { // RightThumb, DPaddown
                                em_state.a2.set_switch_input(0, false);
                            }
                                    _ => (),
                                }
                    }
                }
                //em_state.forced_paddle = None;
                if em_state.mode != ExecutionMode::Paused {
                    // Rememebr. We can have the follwoing sequence: Emulator is
                    // pause. Thn it does a command RunNInstr : Emulator runs.
                    // But right after the command, the emulator is back at
                    // pause. So, in effect, at this point in the code, you
                    // can't see the emulator was run.

                    let start = Instant::now();
                    run_one_frame(&mut em_state, &thread_oq);
                    quit_tx.send(Signals::FrameAvailable).ok();
                    em_state.timers.emu_frame_duration = start.elapsed();
                    em_state.timers.frame_duration = em_state.timers.frame_start.elapsed();
                    em_state.timers.frame_start = Instant::now();
                }

                let this_frame_leaves_turbo_mode = current_turbo && !em_state.turbo_mode;


                let pause_mode_entered = em_state.mode == ExecutionMode::Paused &&  em_state.mode != current_mode;

                if pause_mode_entered {
                    //println!("paused");
                    quit_tx.send(Signals::EmulationPaused).ok();
                }

                // When running, the emulator will send short debuf frames
                // When entering a pause, the emulator will send full debug frame
                // While in pause, the emulator sends nothing except  when a
                // breakpoint is changed, we send a debug frame
                // so that the debugger UI can present updated breakpoints
                // state to the user.

                let send_debug_frame = em_state.send_debugging_info &&
                    (inform_ui_of_debugger_status_change || pause_mode_entered || em_state.mode == ExecutionMode::Run);

                //println!("{} {} {} {}", em_state.send_debugging_info,inform_ui_of_debugger_status_change, pause_mode_entered, em_state.mode);

                if send_debug_frame {
                    trace!("send debug frame: inform_ui_of_debugger_status_change={} pause_mode_entered={} mode={} ",inform_ui_of_debugger_status_change, pause_mode_entered, em_state.mode);
                    // At this point the emulator can be running or paused
                    // (depending on the trigger of some breakpoints)
                    let mut dframe = make_debug_frame(&em_state);
                    em_state.gfx_debug_line = None;

                    if pause_mode_entered {
                        dframe.present_debugger = true;
                    }

                    if debug_stream_tx.send(dframe).is_err() {
                        error!("Debugger disappeared ?")
                    }
                }


		/*
                if em_state.record_video_frame.is_some() {
		    // This will record the frames as they are before
		    // being fed into the composite pipeline
                    let n = em_state.record_video_frame.unwrap();
                    let mut oq = thread_oq.lock().expect("locking output queues");
                    let p = std::env::temp_dir().join(Path::new(&format!("vidrec{:05}.png", n)));
                    let out_file = p.as_path();
                    save_screenshot(out_file, oq.get_gfx_wip_frame());
                    em_state.record_video_frame = Some(n + 1);
                }
		*/


                    // ----------------------------------------------------------------
                    // Pacing the emulator

                    // When sleep()ing, don't forget https://blat-blatnik.github.io/computerBear/making-accurate-sleep-function/

                    if em_state.mode == ExecutionMode::Paused {
                        // Sleep, but not too long as the debugger must still
                        // be responsive
                        sleep(Duration::from_millis(20));
                    } else if em_state.mode == ExecutionMode::Run {
                        // We don't wait when in turbo mode
                        // Pace at the right speed
                        // How much frames have we completed since the beginning
                        // of the emulation session (frames are counted in cycles,
                        // no in real clock time).
                        let nb_frames =
                            ((em_state.cycles() - em_state.session_begin_cycle) as usize) / CYCLES_PER_FRAME;

                        if nb_frames % 100 == 0 {
                            //println!("frameq {}", nb_frames);
                        }

                        // Real clock time that these frames should have taken
                        // to be rendered to the user.
                        let elapsed_ideal = Duration::from_secs_f32(
                            (nb_frames as f32) / (FRAMES_PER_SECOND as f32),
                        ); // equivalent to: nb_frames * frame_duration

                        // How much time since the beginning of the emulation session ?
                        let elapsed = em_state.session_begin_time.elapsed();

                        if elapsed < elapsed_ideal {
                            // We're going faster than the Apple2 speed
                            // debug!("nb frames: {} ideal:{} us, real: {} us",nb_frames, elapsed_ideal.as_micros(), elapsed.as_micros());
                            // debug!("sleeping {}",(elapsed_ideal - elapsed).as_secs_f32());
                            if !em_state.turbo_mode && !this_frame_leaves_turbo_mode {
                                //info!("Sleeping {:.3}-{:.3}={:.3} sec.",elapsed_ideal.as_secs_f32(), elapsed.as_secs_f32(), (elapsed_ideal - elapsed).as_secs_f32());
                                sleep(elapsed_ideal - elapsed);
                            } else if this_frame_leaves_turbo_mode {
                                info!("Leaving turbo mode");
                                em_state.session_begin_cycle = em_state.cycles();
                                em_state.session_begin_time = Instant::now();
                            }
                        } else {
                            // We're going slower than the Apple2 speed
                            // 1. This must never happen because it means the host CPU is too slow
                            // 2. Therefore it means we have been put in background for a while (on Linux!)
                            // FIXME This is a bit too strong 'cos the emulator
                            // could go slow for a frame or two then come back to
                            // normal speed... OTOH here we don't do much wrong
                            // since we just reset some basis for counters...

                            // warn!("Resetting time base");
                            // begin_time = Instant::now();
                            // begin_cycle = em_state.cycles();
                        }
                    };
                }; // loop
            info!("Quitting");
            // Signal the calling thread that we're done (since
            // it can't "join" properly).
            quit_tx.send(Signals::Quit).ok();
        })
        .expect("Couldn't spawn the AccurApple thread");

    (output_queues.clone(), tx, debug_stream_rx, emulator_thread_handle)
}

fn make_debug_frame_for_commands(em_state: &EmulatorState) -> DebugFrame {
    let mut dframe = DebugFrame::new(&em_state.cpu.snapshot());
    dframe.cycles = em_state.cycles();
    dframe.take_screenshot = em_state.take_screenshot;
    dframe
}

fn make_debug_frame(em_state: &EmulatorState) -> DebugFrame {
    let mut dframe = DebugFrame::new(&em_state.cpu.snapshot());

    trace!("Debug frame : replacing PC: {:04X} --> {:04X}",dframe.cpu_snapshot.pc, em_state.debugger.last_instruction_start);
    dframe.cpu_snapshot.pc = em_state.debugger.last_instruction_start;
    dframe.cpu_emulator = em_state.cpu.emulator();

    dframe.floppy_disk_name = em_state.disk_controller.borrow().disk_file_path(0);
    dframe.floppy_disk_checksum = em_state.disk_controller.borrow().woz(0).checksum();
    dframe.lss_debug = em_state.disk_controller.borrow().lss_debug();
    dframe.cycles = em_state.cycles();
    dframe.irq_vector = em_state.a2.irq_vector();
    dframe.last_irq_delay = em_state.a2.last_irq_delay;
    dframe.gfx_mode_info = em_state.a2.gfx_mode_info();
    dframe.gfx_base_mode = em_state.a2.compute_gfx_mode().mode;
    dframe.last_vblank_read = em_state.a2.last_vblank_read;
    dframe.vbl_bit = em_state.a2.vbl_bit(em_state.cycles());
    dframe.cycle_in_frame = (em_state.cpu.cycles() % (CYCLES_PER_FRAME as u64)) as usize;
    //dframe.floating_addr = em_state.a2.floating_addr(em_state.cycles());

    for (i, address) in BANKS_OF_INTEREST.iter().enumerate() {
        dframe.memory_banks[i] = em_state.a2.which_memory_bank(*address, false);
        dframe.memory_banks_for_write[i] = em_state.a2.which_memory_bank(*address, true);
    }
    dframe.active_memory_bank = em_state.a2.which_memory_bank(0xD000, false);
    dframe.hramrd = em_state.a2.hramrd;

    dframe.memory_flags = em_state.a2.log_bank_switched_config(0);

    for s in em_state.a2.slots_status(em_state.cycles()) {
        dframe.slots_status.push(s);
    }

    for s in em_state.a2.planned_irq.iter() {
        dframe.planned_irqs.push(*s);
    }
    dframe.gfx_debug_line = em_state.gfx_debug_line;

    dframe.floppy_track = em_state.disk_controller.borrow().current_track();

    let mut bp_s = "Breakpoints:".to_string();
    if em_state.debugger.breakpoint_memory_layout {
        bp_s.push_str(" MemLayout")
    }
    if let Some(addr) = em_state.debugger.breakpoint_mem_read {
        bp_s.push_str(format!(" MemRead ${:04X}", addr).as_str())
    }
    if let Some(addr) = em_state.debugger.breakpoint_mem_write {
        bp_s.push_str(format!(" MemWrite ${:04X}", addr).as_str())
    }
    if let Some(addr) = em_state.debugger.breakpoint_pc {
        bp_s.push_str(format!(" PC ${:04X}", addr).as_str())
    }
    if let Some(track) = em_state.debugger.breakpoint_track {
        bp_s.push_str(format!(" DriveTrack {}", track).as_str())
    }
    if em_state.debugger.breakpoint_floppy_command {
        bp_s.push_str(" DriveCommand")
    }

    dframe.breakpoints = bp_s;

    dframe.break_point_triggered = em_state.break_point_triggered;

    if em_state.mode == ExecutionMode::Paused {

        // Iterate from last history (most recent) to first history (oldest)
        for s in em_state.debugger.history2.iter() {
            dframe.trace.push(s.clone())
        }

        //println!("ram copy");
        // When paused, we assume we're debugging so we're
        // interested in memory information (and we don't
        // care about the performance cost of copying the whole RAM :-) )

        let m = em_state.a2.active_ram();
        dframe.active_mem_copy = Some(m);
        dframe.ram_copy = Some(em_state.a2.ram().to_vec());
        dframe.auxram_copy = Some(em_state.a2.aux_ram().to_vec());

        /* for s in em_state.debugger.stack_markers.iter() {
            dframe.stack_markers.push(s.clone())
        } */

        trace!("Copying JSR trace");
        for s in em_state.debugger.jsr_stack.iter() {
            dframe.jsr_stack.push(s.clone())
        }
    } else {
        //println!("NO ram copy");
        dframe.active_mem_copy = None;
        dframe.ram_copy = None;
        dframe.auxram_copy = None;
    }

    dframe.slots_status.push(format!(
        "Sound buffer {}, {}Hz",
        em_state.output_queue_sound_size, em_state.sample_rate
    ));
    //debug!("Sending debug frame");

    dframe.timers = em_state.timers;
    dframe
}

pub fn make_logo() -> Vec<u8> {
    // From https://combineoverwiki.net/images/thumb/2/26/Apple_logo.svg/500px-Apple_logo.svg.png
    const COLORS: [u8; 6 * 4] = [
        0, 158, 221, 255, // BLUE
        151, 59, 152, 255, // Mauve
        225, 55, 60, 255, // Red
        245, 131, 25, 255, // Organge
        253, 185, 34, 255, //Yellow
        97, 188, 68, 255, // Green
    ];
    const BORDER: [u8; 4] = [0, 0, 0, 255];

    const H: usize = 32 - 2; // -2 == - top and bottom border
    let mut icon: Vec<u8> = Vec::new();

    const ICON_SIZE: usize = 32;

    for _i in 0..32 {
        for j in 0..4 {
            icon.push(BORDER[j]);
        }
    }

    for c in 0..6 {
        let h = (c + 1) * H / 6 - c * H / 6;
        for _pixel in 0..(h * ICON_SIZE) {
            for i in 0..4 {
                icon.push(COLORS[(5 - c) * 4 + i]);
            }
        }
    }

    for _i in 0..32 {
        for j in 0..4 {
            icon.push(BORDER[j]);
        }
    }

    for i in 0..32 {
        for j in 0..4 {
            icon[32 * i * 4 + j] = BORDER[j];
            icon[(32 * i + 31) * 4 + j] = BORDER[j];
        }
    }

    icon
}

fn run_one_frame(em_state: &mut EmulatorState, output_queues: &Arc<Mutex<OutputQueues>>) {
    assert!(
        em_state.mode != ExecutionMode::Paused,
        "A paused emulator should not do anything"
    );

    trace!(
        "Emulating one frame PC=${:04X}, Mode:{}",
        em_state.cpu.pc(),
        em_state.mode
    );

    // FIXME Apple simulation should go elsewhere,
    // this function should only be concerned with
    // filling the output queues so that we just
    // lock() it the least possible

    //let freeze_execution = false;
    let freeze_execution = run_ticks_as_needed(
        em_state.cpu.as_mut(),
        &mut em_state.a2,
        &mut em_state.debugger,
        &em_state.device_breakpoints,
    );

    // Break point on floppy disk reaching a track
    // let bp_track_triggered = if let Some(bp_track) = em_state.debugger.breakpoint_track {
    // if let Some(track) = em_state.disk_controller.borrow().current_track() {
    //     if track == bp_track {
    // 	em_state.debugger.breakpoint_track = None;
    // 	true
    //     } else {false}
    // } else {false}
    // } else {false};

    if freeze_execution {
        // The execution needs to be frozen because
        // the CPU execution has reached a stop point
        // such as a debugger breakpoint.

        em_state.break_point_triggered = true;
        em_state.mode = ExecutionMode::Paused;
    } else {
        em_state.break_point_triggered = false;
    }

    // FIXME The code hereafter locks and unlocks
    // the mutex. My hope is that by doing so I
    // leave some room for the sound stream to
    // be able to get the lock as well to play
    // sound. But what we should do instead
    // is to lock the rendered sound and the played
    // sound with *different* locks. This way they
    // wouldn't get in the way.

    if em_state.record_video_path.is_some() {
        // FIXME This is very bad... It's done in a way to ensure
        // the rendering thread and this thread are in lock steps.
        // But that's not the way to do it. What one should do is
        // to queue all generated frames, just as we queue sound-frames.
        // and let the consuming thread take whatever it can.
        loop {
            if let Ok(oq) = output_queues.lock() {
                if oq.consumed {
                    break
                }
            }
            sleep(Duration::from_millis(1));
        }
    }


    {
        // This code block is here to make sure we lock the
        // output_queues as less as possible AND release
        // the lock for others to use.

        let mut oq = output_queues.lock().expect("locking output queues");
        let c = em_state.cycles(); // Avoid borrow checker

        // https://stackoverflow.com/questions/60253791/why-can-i-not-mutably-borrow-separate-fields-from-a-mutex-guard
        let oq_mut = oq.deref_mut();
        let wip_frame = oq_mut.gfx_wip_frame.ptr.deref_mut();

        render_one_frame2(
            &mut em_state.a2,
            c,
            wip_frame,
            &mut em_state.gfx_render_state,
        );

        em_state.a2.keyboard_queue.clear_up_to(c);

        oq_mut.scanned_ram.copy_from_slice(em_state.gfx_render_state.scanned_bytes.as_slice());
        oq_mut.rendered_soft_switches.copy_from_slice(em_state.gfx_render_state.soft_switches.as_slice());

        oq_mut.colour_bursts.copy_from_slice(
            bytemuck::cast_slice(
                em_state.gfx_render_state.colour_bursts.as_slice()
            ));

        oq.done_frames += 1;
        oq.last_time_displayed = Instant::now();
        oq.consumed = false;

        if em_state.record_video_frame.is_some() {
            let recorded_frame_num = em_state.record_video_frame.unwrap();
            em_state.record_video_frame = Some(recorded_frame_num + 1);
            oq.screenshot = Some( PathBuf::from(format!("{}_{:05}.png", em_state.record_video_path.as_ref().unwrap().file_stem().unwrap().to_string_lossy(), recorded_frame_num)));
        }
    }

    // If the emulator is frozen, then no sound is produced.
    // Therefore, the sound production must be stopped as
    // well (else it will be desynchronized with the rest
    // of the emulation)

    if em_state.mode != ExecutionMode::Paused {
        let spf = em_state.samples_per_frame;
        let speaker_sound: SamplesFrame = em_state.a2.speaker.borrow_mut().process_frame();
        let mockingboard_sound: SamplesFrame = em_state.mockingboard.borrow_mut().process_frame();

        assert!(
            speaker_sound.len() == 0
                || mockingboard_sound.len() == 0
                || speaker_sound.len() == spf
                || mockingboard_sound.len() == spf,
            "Buffer size incorrect: speaker_sound:{} mockingboard_sound:{}",
            speaker_sound.len(),
            mockingboard_sound.len()
        );

        // Now that we have processed the sound for this frame,
        // we mix MB and speaker output into the output queue
        // where it will be read by the CPAL thread.

        let mut oq = output_queues.lock().expect("locking output queues");

        // If turbo mode, let the audio thread drain the buffer before adding sound
        // FIXME Doesn't explain why there's a big pause once we buffer
        // too much sound => there's a BUG somewhere. BUG is still there.

        if em_state.sound_production && (!em_state.turbo_mode || oq.sound.is_empty()) {
            //println!("Sound OQ");
            // The goal here is to avoid spending time rendering
            // silence.

            if speaker_sound.len() > 0 && mockingboard_sound.len() > 0 {
                //println!("sound1");
                for i in 0..speaker_sound.len() {
                    // Poor man's sound clipping.
                    let left = speaker_sound.left[i].saturating_add(mockingboard_sound.left[i]);
                    let right = speaker_sound.right[i].saturating_add(mockingboard_sound.right[i]);
                    oq.sound.push_front((left, right));
                    if em_state.active_sound_record.is_some() {
                        em_state.sound_record.push(left);
                        em_state.sound_record.push(right);
                    }
                }
            } else if speaker_sound.len() == 0 && mockingboard_sound.len() > 0 {
                //println!("sound2");

                for i in 0..mockingboard_sound.len() {
                    let left = mockingboard_sound.left[i];
                    let right = mockingboard_sound.right[i];
                    oq.sound.push_front((left, right));
                    if em_state.active_sound_record.is_some() {
                        em_state.sound_record.push(left);
                        em_state.sound_record.push(right);
                    }
                }
            } else if speaker_sound.len() > 0 && mockingboard_sound.len() == 0 {
                //println!("sound");
                for i in 0..speaker_sound.len() {
                    let left = speaker_sound.left[i];
                    let right = speaker_sound.right[i];
                    oq.sound.push_front((left, right));
                    if em_state.active_sound_record.is_some() {
                        em_state.sound_record.push(left);
                        em_state.sound_record.push(right);
                    }
                }
            } else {
                //println!("sound3");

                // Silence
                for _i in 0..spf {
                    let left = cpal::Sample::EQUILIBRIUM;
                    let right = cpal::Sample::EQUILIBRIUM;
                    oq.sound.push_front((left, right));
                    if em_state.active_sound_record.is_some() {
                        em_state.sound_record.push(left);
                        em_state.sound_record.push(right);
                    }
                }
            }

            // Record some info for the debugger.
            // I could put that in the main loop to keep the code
            // clean but then I'd have to lock the queue more
            // often...
            em_state.output_queue_sound_size = oq.sound.len();
        }
    } else {
        //debug!("Frozen audio");
    }
}


#[derive(Clone)]
pub struct CpuDebugState {
    // Registers
    pub pc: u16,
    pub a: u8,
    pub x: u8,
    pub y: u8,
    pub flags: u8,
    // We copy the memory of the instrucion bytes to
    // be able to remember self modifying code
    pub instr_mem: [u8; 4],
    pub cycle: u64,
    pub old_mem_value: Option<u8>,
    pub mem_access: Option<(u16, u8)>,
    pub irq_line_state: IrqLineState
}

impl CpuDebugState {
    pub fn new(
        cpu: &dyn Cpu6502,
        instr_mem: [u8; 4],
        old_mem_value: Option<u8>,
        mem_access: Option<(u16, u8)>,
    ) -> CpuDebugState {

        CpuDebugState {
            pc: cpu.pc(),
            a: cpu.a(),
            x: cpu.x(),
            y: cpu.y(),
            flags: cpu.flags(),
            instr_mem,
            cycle: cpu.cycles(),
            old_mem_value,
            mem_access,
            irq_line_state: cpu.irq_line_state()
        }
    }
}

#[derive(Clone)]
pub struct JumpTrace {
    pub from: u16,
    pub to: u16,
}

struct CpuDebugger {
    breakpoint_pc: Option<u16>,
    breakpoint_mem_write: Option<u16>,
    breakpoint_mem_read: Option<u16>,
    breakpoint_cycle: Option<u64>,
    breakpoint_track: Option<usize>,
    breakpoint_floppy_command: bool,
    breakpoint_memory_layout: bool,
    breakpoint_irq: bool,
    stack_markers: Vec<bool>,

    // This one is used to store a bit of history from the
    // exceution, to look at it in the debugger
    history2: CircularQueue<CpuDebugState>,
    proceed_instructions: Option<usize>, // How many instructions to proceed
    // If true, the debugger will record the emulator execution.
    proceed_ticks: Option<usize>, // How many cycles to proceed
    trace: bool,
    jsr_stack: Vec<JumpTrace>,

    // This one is used to store a full execution stack
    // trace.
    execution_trace: Option<Vec<CpuSnaphsot>>,

    // Address of the last instruction began by the CPU
    // This is useful to put the debugger on the right line.
    last_instruction_start: u16,

    last_brk_cycle: u64
}

const ASM_HISTORY_SIZE: usize = 5000;

impl CpuDebugger {
    fn new() -> CpuDebugger {
        CpuDebugger {
            breakpoint_pc: None,        // Some(0xD195),
            breakpoint_mem_write: None, // Some(0xD195),
            breakpoint_mem_read: None,
            breakpoint_cycle: None, //Some(6422520), // CHP.dsk mockingboard slot 4 detection
            history2: CircularQueue::<CpuDebugState>::with_capacity(ASM_HISTORY_SIZE),
            proceed_instructions: None,
            proceed_ticks: None,
            trace: false,
            breakpoint_track: None,
            breakpoint_floppy_command: false,
            breakpoint_memory_layout: false,
            breakpoint_irq: false,
            stack_markers: vec![false; 256],
            jsr_stack: Vec::new(),
            execution_trace: None,
            last_instruction_start: 0,
            last_brk_cycle: 0
        }
    }

    fn mark_stack(&mut self, stack_ptr: u8) {
        self.stack_markers[stack_ptr as usize] = true;
    }

    fn unmark_stack(&mut self, stack_ptr: u8) {
        self.stack_markers[stack_ptr as usize] = false;
    }

    fn push_jsr(&mut self, jsr_from: u16, jsr_dest: u16) {
        self.jsr_stack.push(JumpTrace {
            from: jsr_from,
            to: jsr_dest,
        });
    }

    fn pop_jsr(&mut self) -> Option<JumpTrace> {
        self.jsr_stack.pop()
    }

    fn last_jsr_from_address(&self) -> Option<u16> {
        self.jsr_stack.last().map(|last| last.from)
    }

    fn last_jsr_to_address(&self) -> Option<u16> {
        self.jsr_stack.last().map(|last| last.to)
    }
}


fn cpu_consumes_irq(cpu: &mut dyn Cpu6502, irq_change: Option<IrqChange>) -> bool {
    // Returns true if an IRQ was accepted.
    match irq_change {
        Some(IrqChange::PullDown) => {
            let will_irq = cpu.accepts_irq();
            trace!("CPU will accept pull down ? {}", will_irq);
            cpu.pull_irq_pin_down();
            will_irq
        },
        Some(IrqChange::PullUp) => {
            let will_irq = cpu.accepts_irq();
            trace!("CPU will accept pull up ? {}", will_irq);
            cpu.pull_irq_pin_up();
            false
        },
        _ => false,
    }
}

fn handle_bus(cpu: &mut dyn Cpu6502, machine: &mut AppleII) -> (Option<u8>, bool) {
    // We process memory reads and writes.
    if cpu.is_reading() {
        let (b, irq_change) = machine.readb_cycle(cpu.address(), true, cpu.cycles());
        cpu.set_data(b);
        let irq_started = cpu_consumes_irq(cpu, irq_change);
        (None, irq_started)
    } else if cpu.is_writing() {
        // The value in memory before the write operation. Used for logging/debugging.
        let old_mem_value = cpu.data(); // FIXME Most likely incorrect.
        let (_, irq_change) = machine.storeb(cpu.address(), old_mem_value, cpu.cycles());
        let irq_started= cpu_consumes_irq(cpu, irq_change);
        return (Some(old_mem_value), irq_started);
    } else {
        panic!("The 6502 is always either reading or writing")
    }
}

fn handle_planned_irqs(cpu: &mut dyn Cpu6502, machine: &mut AppleII) -> bool {
    // Process planned IRQ (some of them may actually interrupt the CPU, some
    // of them are for housekeeping of the devices plugged in the slots (or any
    // other thing).

    while !machine.planned_irq.is_empty() {
        let irq_req = machine.planned_irq.first().unwrap();
        let next_irq_cycle = irq_req.irq_cycle;

        assert!(next_irq_cycle >= cpu.cycles(),
            "The next IRQ to process is not in the future next_irq_cycle:{} < cpu.cycles:{}. Source:{} / {}",
            next_irq_cycle, cpu.cycles(), irq_req.source_slot, irq_req.sub_source);

        if next_irq_cycle == cpu.cycles() {
            // Note that if interrupt are disabled (CLI/SEI), the 6502 will not clear
            // the IRQ flag *but* the IRQ processing in the device will
            // occur anyway...

            //trace!("{}",cpu_trace(cpu, &machine, disassembler));
            trace!(
                "IRQ triggered at cycle {}. {} irq's in the pipe",
                cpu.cycles(),
                machine.planned_irq.len()
            );

            let c = cpu.cycles();
            cpu_consumes_irq(cpu, machine.handle_first_available_irq(c));

        } else {
            break;
        }
    }

    false
}

fn just_one_tick(cpu: &mut dyn Cpu6502, machine: &mut AppleII) -> Option<u8> {

    let _irq_will_start = handle_planned_irqs(cpu, machine);

    // Tick and bus handling must go together (or at least IRQ must not
    // happen between them).
    cpu.tick();

    // if cpu.ir() == OPCODE_CLI {
    //     for ndx in 0..7 {
    //         if machine.slot(ndx).irq_line_level() == IrqLineLevel::Low {
    //             cpu.pull_irq_pin_down()
    //         }
    //     }
    // }

    // Remember that bus handling may trigger memory reads/writes which in turn
    // may trigger I/O which in turn may trigger IRQ requests !
    let (old_mem, _irq_will_start2) = handle_bus(cpu, machine);
    // Floppy disk actually needs half-cycle emulation. We
    // do two half cycles at once here.

    machine.tick_floppy(old_mem.unwrap_or(0));

    old_mem
    //return (pins_after, old_mem_value);
}

fn track_calls(cpu: &dyn Cpu6502, machine: &AppleII, debugger: &mut CpuDebugger) {
    // This code is here because the ir() will be set to BRK for the whole
    // duration of the IRQ processing by the 6502. Moreover, it allows
    // for reentrant IRQ.
    if cpu.ir() == OPCODE_BRK && cpu.cycles() > debugger.last_brk_cycle + 7 {
        trace!("OPCODE_BRK");
        // We have entered a software irq or a hardware one.
        let dest = machine.loadw_no_side_effect(0xFFFE);

        debugger.push_jsr(cpu.pc(), dest);
        debugger.last_brk_cycle = cpu.cycles();

    } else if let Some(new_instr_beginning) = cpu.begins_instruction() {
        let instr_beginning = debugger.last_instruction_start;
        debugger.last_instruction_start = new_instr_beginning;

        let opcode = machine.loadb_no_side_effect(instr_beginning);
        match opcode {
            OPCODE_JSR => {
                // JSR
                // At this point the PC has been pushed on the stack
                let jump_addr = instr_beginning + 1;
                let dest = machine.loadw_no_side_effect(jump_addr);
                trace!(
                    "mark jump dest address={:04X} jump dest={:04X}",
                    jump_addr,
                    dest
                );
                //debugger.mark_stack(cpu.S);
                debugger.push_jsr(instr_beginning, dest);
            }
            OPCODE_RTS|OPCODE_RTI => {
                if let Some(last_track) = debugger.last_jsr_from_address() {
                    let stack_addr = cpu.s()+1-2;
                    let jsr_addr = if  opcode == OPCODE_RTS {
                        machine.loadw_no_side_effect(stack_addr) - 2
                    } else {
                        machine.loadw_no_side_effect(stack_addr)
                    };

                    trace!("unmark SP:{:03X} stack={:04X}",
                        stack_addr,
                        jsr_addr);
                    if last_track == jsr_addr {
                        let jt_opt = debugger.pop_jsr();
                        // At this point the PC has not yet been pulled off the stack
                        trace!("unmark SP:{:03X} stack={:04X} at address={:04X}, out from {}",
                            stack_addr,
                            jsr_addr,
                            instr_beginning,
                            if let Some(jt) = jt_opt {
                                format!("{:04X} -> {:04X}", jt.from, jt.to)
                            } else {
                                "/".to_string()
                            });
                        //debugger.unmark_stack(cpu.S)
                    }
                }
            }
            _ => (),
        }
    }
}

fn tick_and_trace(cpu_obscoz: &mut dyn Cpu6502, machine: &mut AppleII, debugger: &mut CpuDebugger) {
    // FIXME We assume there's only ONE BYTE written per instruction.
    // This is not correct for JSR (the stack is update'd with 2 bytes)
    let temp_old_mem_value = just_one_tick(cpu_obscoz, machine);

    // Recording
    if let Some(exec_trace) = debugger.execution_trace.as_mut() {
        if let Some(_pc) = cpu_obscoz.begins_instruction() {
            let mut snap = cpu_obscoz.snapshot_begin_instruction();
            if !exec_trace.is_empty() {
                snap.cycle -= exec_trace[0].cycle;
            }
            exec_trace.push(snap);
        }
    }

    // if old_mem_value.is_none() && temp_old_mem_value.is_some() {
    //     old_mem_value = temp_old_mem_value;
    // }
    if temp_old_mem_value.is_some() {
        let mut iter = debugger.history2.iter_mut();
        if let Some(prev) = iter.next() {
            prev.old_mem_value = temp_old_mem_value;
        }
    }

    track_calls(cpu_obscoz, machine, debugger);

    if let Some(instr_beginning) = cpu_obscoz.begins_instruction() {
        // We're a the beginning of the execution of the new instruction.
        // We start fetchng a new instructions

        // let mut iter = debugger.history2.iter_mut();
        // if let Some(prev) = iter.next() {
        // 	prev.old_mem_value = old_mem_value;
        // }

        let m: [u8; 4] = [
            machine.loadb_no_side_effect(instr_beginning),
            machine.loadb_no_side_effect(instr_beginning + 1),
            machine.loadb_no_side_effect(instr_beginning + 2),
            machine.loadb_no_side_effect(instr_beginning + 3),
        ];
        let data_read = crate::rs6502::disassembler::decode_address(
            &m,
            |addr: u16| machine.loadb_no_side_effect(addr),
            cpu_obscoz.x(),
            cpu_obscoz.y(),
        );
        debugger
            .history2
            .push(CpuDebugState::new(cpu_obscoz, m, None, data_read));

        //old_mem_value = None;
    }


}

fn run_ticks_as_needed(
    cpu_obscoz: &mut dyn Cpu6502,
    machine: &mut AppleII,
    debugger: &mut CpuDebugger,
    breakpoints: &BreakPointTriggerRef,
) -> bool {
    // bool is true if execution has stopped (for example on a break point)

    if let Some(nb_cycles) = debugger.proceed_ticks {

        let mut buffer = File::create("accurapple_trace.txt").ok();

        if let Some(file) = buffer.as_mut() {
            let mut buffered_file = BufWriter::new(file);
            info!("Starting recording at cycle {}", cpu_obscoz.cycles());

            for _i in 0..nb_cycles {
                let _temp_old_mem_value = just_one_tick(cpu_obscoz, machine);
                track_calls(cpu_obscoz, machine, debugger);
                //info!("CPU:{}", cpu_obscoz.snapshot());
                if _i % 100_000 == 0{
                    info!("At {}", _i);
                }

                writeln!(buffered_file, "{} CPU:{} 6522:{}",
                    cpu_obscoz.cycles(),
                    cpu_obscoz.snapshot(),
                    machine.slot(4).status_str(cpu_obscoz.cycles())).expect("can't write ?!");
            }
            info!("Finished recording at cycle {}", cpu_obscoz.cycles());
        } else {
            error!("Can't create output file for trace");
        }
        debugger.proceed_ticks = None;
        true
    } else if let Some(nb_instr) = debugger.proceed_instructions {
        // Debugger "proceed n instructions" command

        debug!("Proceed with {} instructions", nb_instr);
        for _i in 0..nb_instr {
            // Ticks until next instruction
            loop {
                tick_and_trace(cpu_obscoz, machine, debugger);
                trace!("Proceed PC={:04X} IR={:02X}", cpu_obscoz.pc(), cpu_obscoz.ir());
                if cpu_obscoz.begins_instruction().is_some() {
                    // We're a the beginning of the execution of the new instruction.
                    // We start fetchng a new instructions
                    break;
                }
            }
        }
        debugger.proceed_instructions = None;
        return true;
    } else {
        let frame_ofs = (cpu_obscoz.cycles() % (CYCLES_PER_FRAME as u64)) as usize;
        let cycles_to_do = if frame_ofs > 0 {
            CYCLES_PER_FRAME - frame_ofs
        } else {
            CYCLES_PER_FRAME
        };

        trace!("Running {} cycles", cycles_to_do);
        for _i in 0..cycles_to_do {
            // end bound exclusive !

            // Design principle: a break point remains active
            // after it has triggered. EXCEPTION: PC breakpoints are
            // disabled after triggering (see note below)
            if debugger.breakpoint_cycle.is_some()
                && cpu_obscoz.cycles() >= debugger.breakpoint_cycle.unwrap()
            {
                debugger.breakpoint_cycle = None;
                return true;
            } else if debugger.breakpoint_irq {
                // FIXME Check an IRQ will occur
                // return true;
            }

            tick_and_trace(cpu_obscoz, machine, debugger);
            //let read: bool = pins & M6502_RW != 0; // || true;
            //let write: bool = !read;

            if let Some(addr) = debugger.breakpoint_mem_write {
                if cpu_obscoz.is_writing() && cpu_obscoz.address() == addr {
                    return true;
                }
            } else if let Some(addr) = debugger.breakpoint_mem_read {
                if cpu_obscoz.is_reading() && cpu_obscoz.address() == addr {
                    return true;
                }
            } else if debugger.breakpoint_memory_layout && machine.has_mem_layout_changed() {
                return true;
            } else if breakpoints.borrow_mut().has_triggered() {
                return true;
            } else if let Some(pc) = debugger.breakpoint_pc {
                // trace!("Checking PC breakpoint at {:04X}", pc);
                if let Some(addr) = cpu_obscoz.begins_instruction() {
                    // trace!("Checking PC breakpoint at {:04X}, address is {:04X}", pc, addr);
                    if addr == pc {
                        // The PC breakpoint is a bit different: it is used most
                        // of the time to run up to an address, not to stop each
                        // time this address is reached. So I disable
                        // automatically it when I reachit.

                        // FIXME I could arrange for that but it makes code more complex.
                        debugger.breakpoint_pc = None;
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

// fn cpu_trace(cpu: &Cycle6502, machine: &AppleII, disassembler: &mut Disassembler) -> String {
//     //let offset: usize = cpu.last_instr_pc as usize;
//     let offset: u16 = cpu.PC;

//     disassembler.code_offset = offset;
//     let m: [u8; 4] = [
//         machine.loadb_no_side_effect(offset),
//         machine.loadb_no_side_effect(offset + 1),
//         machine.loadb_no_side_effect(offset + 2),
//         machine.loadb_no_side_effect(offset + 3),
//     ];

//     let mut labels:HashMap<u16,String> = HashMap::new();
//     labels.insert(0x802, "loop".to_string());
//     labels.insert(0x400, "counter_A".to_string());
//     labels.insert(0x800, "counter_B".to_string());

//     let asm = disassembler.disassemble_with_addresses(&m, Some(1), &labels);

//     format!(
// 	"{: <31} SP=01{:02X} A={:02X}, X={:02X}, Y={:02X} flags:{}{}{}{}{}{}{}{}| PC:${:04X} | {}",
// 	&asm.first().unwrap().source, cpu.S, cpu.A, cpu.X, cpu.Y,
// 	if cpu.P & M6502_NF != 0 {"N"} else {"-"},
// 	if cpu.P & M6502_VF != 0 {"V"} else {"-"},
// 	if cpu.P & M6502_XF != 0 {"X"} else {"-"},
// 	if cpu.P & M6502_BF != 0 {"B"} else {"-"},
// 	if cpu.P & M6502_DF != 0 {"D"} else {"-"},
// 	if cpu.P & M6502_IF != 0 {"I"} else {"-"},
// 	if cpu.P & M6502_ZF != 0 {"Z"} else {"-"},
// 	if cpu.P & M6502_CF != 0 {"C"} else {"-"},
// 	// pinIRQ:{} SYN:{} pip:{:04X} IR: ${:04X}/{:}, PC:${:04X} AD: ${:04x}
// 	/*if cpu.PINS & M6502_IRQ != 0 {1} else {0},
// 	if cpu.PINS & M6502_SYNC != 0 {1} else {0},
// 	cpu.irq_pip,
// 	cpu.IR>>3,
// 	cpu.IR & 7,*/
// 	cpu.PC,
// 	/*cpu.AD, */
// 	cpu.cycles)
// }

// fn hgr_address(y: usize) -> usize {
//     let ofs = match y {
//         0..=63 => 0,
//         64..=127 => 0x28,
//         128..=191 => 0x50,
//         _ => panic!(),
//     };

//     let i = (y % 64) >> 3; // div by 8
//     let j = (y % 64) % 8;

//     return ofs + 0x80 * i + 0x400 * j;
// }

// fn monochrome_hgr_to_png(png_path: &str, bytes: &[u8]) {
//     let mut rgba = [0 as u8; 192 * (40 * 7) * 4]; // 215040 bytes
//     let mut rgb_i = 0;

//     for y in 0..191 {
//         let ofs = hgr_address(y);
//         for i in ofs..ofs + 40 {
//             let mut byte = bytes[i];
//             for bit in 0..7 {
//                 let c: u8 = (byte & 1) * 0xFF;
//                 rgba[rgb_i] = c;
//                 rgba[rgb_i + 1] = c;
//                 rgba[rgb_i + 2] = c;
//                 rgba[rgb_i + 3] = 0xFF;
//                 byte = byte >> 1;
//                 rgb_i += 4;
//             }
//         }
//     }

//     let path = Path::new(r"image.png");
//     let file = File::create(png_path).unwrap();
//     let ref mut w = BufWriter::new(file);

//     let mut encoder = png::Encoder::new(w, SCREEN_WIDTH as u32, 192); // Width is 2 pixels and height is 1.
//     encoder.set_color(png::ColorType::Rgba);
//     encoder.set_depth(png::BitDepth::Eight);
//     let mut writer = encoder.write_header().unwrap();

//     writer.write_image_data(&rgba).unwrap(); // Save
// }
