import math
from math import cos, sin
from matplotlib import pyplot as plt
from matplotlib import lines
from matplotlib.patches import Arc

def dist(a,b):
    # shortest distance on a modulo ring
    u = (a - b) % 360
    v = (b - a) % 360
    if u < v:
        return u
    else:
        return v

def direction(a,b):
    """
                          a
                         /
                        /
                       /   zone 3
      -----zone 1-----+--------------
                     /
                    / zone 2
                   /
                  /
    """


    a = a % 360
    b = b % 360

    if a == b:
        return 0

    if a < b  and (b - a) < 180:
        # zone 1 : there's not crossing around zero degree; b is on the left of a.
        return +1
    elif a > 180 and a < b:
        # zone 2: a > 180 and still no crossing of the zero degree angle.
        return +1
    elif a > 180 and (360-a) + b < 180:
        # zone 3: a crossing occur
        return +1
    else:
        return -1

assert direction(0,0) == 0
assert direction(10,20) == +1 # Zone 1
assert direction(20,10) == -1
assert direction(210,220) == +1 # Zone 2
assert direction(220,210) == -1
assert direction(10,-10) == -1 # Zone 3
assert direction(-10,10) == +1
assert direction(350,10) == +1
assert direction(10,350) == -1
assert direction(170,190) == +1 # Just to be sure around 180°
assert direction(190,170) == -1


def middle(a,b):
    # Center of the smallest interval on a modulo ring between a and b
    u = (a - b) % 360
    v = (b - a) % 360
    if u < v:
        return b % 360 + u/2
    else:
        return a % 360 + v/2


SPEED = 8

# EQUAL_ZONE = 3
# ZONES = [((0 - EQUAL_ZONE) % 360, (0 + EQUAL_ZONE) % 360),
#          (0 + EQUAL_ZONE, 45 - EQUAL_ZONE),
#          (0 + EQUAL_ZONE, 45 - EQUAL_ZONE),
#          ]


def move_subtrack(magnet_states, angle, subtrack, time_to_go):

    MAGNET_ANGLES = [0,90,180,270]

    mod_angle = int(angle) % 360

    # Given our current position, what are the magnets
    # that can contribute to move the head ?
    mag1, mag2 = None, None

    if mod_angle % 90 == 0:
        tm_center = mod_angle // 90
        tm_right = (tm_center - 1) % 4
        tm_left = (tm_center + 1) % 4

        if magnet_states[tm_center] and magnet_states[tm_right]:
            mag1, mag2 = tm_center, tm_right
        elif magnet_states[tm_center] and magnet_states[tm_left]:
            mag1, mag2 = tm_center, tm_left
        elif magnet_states[tm_center]:
            mag1, mag2 = tm_center, tm_center
        elif magnet_states[tm_left]:
            mag1, mag2 = tm_left, tm_left
        elif magnet_states[tm_right]:
            mag1, mag2 = tm_right, tm_right

    else:
        # Not on right angles
        mag1 = mod_angle // 90
        mag2 = (mag1 + 1) % 4

    print(f"angle={angle}, mag1:{mag1}, mag2:{mag2}")
    # Are those magnets turned on ? If so, what
    # "target" give they to the stepper ?
    if mag1 is None and mag2 is None:
        dest = angle
    elif magnet_states[mag1] and magnet_states[mag2]:
        dest = middle(MAGNET_ANGLES[mag1], MAGNET_ANGLES[mag2])
    elif magnet_states[mag1]:
        dest = MAGNET_ANGLES[mag1]
    elif magnet_states[mag2]:
        dest = MAGNET_ANGLES[mag2]
    else:
        # No magnet contributes
        dest = angle

    print(f"angle={angle}, mag1:{mag1}, mag2:{mag2}, dest:{dest}, dir:{direction(angle, dest)}")

    # Will be zero if we're on target. +/- 1 else
    d = direction(angle, dest)
    distance_to_target = dist(angle, dest)
    angle += d * min(distance_to_target, time_to_go)

    if angle < 0:
        angle = 0

    RELAX = 5 # Degrees
    # Closest unit part
    h_angle = round(angle / 45) * 45
    between_tracks = abs(h_angle - angle) > RELAX
    if not between_tracks:
        subtrack = (h_angle // 180) + ((h_angle % 180) // 45)*0.25

    if d != 0:
        # We're moving
        # We look a the next magnet in the direction the rotor is moving
        # Don't forget angle can be much bigger than 360°

        if d > 0:
            next_mag_angle = (math.ceil(angle/45)*45)
        else:
            next_mag_angle = (math.floor(angle/45)*45)

        print(f"angle:{angle} next_mag_angle:{next_mag_angle}")
        # Are we already close enough to that next magnet ?
        if    (d > 0 and angle < next_mag_angle - RELAX) \
           or (d < 0 and angle > next_mag_angle + RELAX):
            dist_to_next_boundary = dist(angle, next_mag_angle)
        else:
            dist_to_next_boundary = None
            next_mag_angle = None
    else:
        next_mag_angle = None
        dist_to_next_boundary = None


    print(f"--> angle={angle}, subtrack:{subtrack}")
    return angle, subtrack, between_tracks, next_mag_angle, dist_to_next_boundary


stepper_angle = 720
subtrack = 0
stepper_position = 0
previous_stepper_position = 0
between_track = False
next_mag_angle = None
dist_to_next_boundary = None

def animate(t):
    global stepper_angle, subtrack, stepper_position, previous_stepper_position, axes, between_track, next_mag_angle, dist_to_next_boundary
    axes.clear()

    print(t)
    STEP = 10
    i = t // STEP
    j = t % STEP

    magnet_states = [False, False, False, False]

    if 10 < t < 120:
        magnet_states[(i//2)%4] = True
        magnet_states[(1+i//2)%4] = True
    elif t >= 120:
        #if i % 2 == 0:
        magnet_states[(i//2)%4] = True

    if t > 80:
        magnet_states = list(reversed(magnet_states))


    axes.set_aspect( 1 )
    cc = plt.Circle(( 0.5 , 0.5 ), 0.5, fill=None)
    axes.add_artist( cc )

    for apos in range(0,360,45):
        arc = Arc(( 0.5 , 0.5 ), 0.8,0.8, theta1=apos-5, theta2=apos+5, color="green")
        axes.add_artist( arc )

    for k in range(4):
        if magnet_states[k]:
            angle = [0, math.pi/2, math.pi, (3/2)*math.pi][k]
            cc = plt.Circle(( 0.5 + 0.4*cos(angle) , 0.5 + 0.4*sin(angle)), 0.1, fill=None)
            axes.add_artist( cc )

    dx = 0.4*cos(stepper_angle * (2*math.pi/360))
    dy = 0.4*sin(stepper_angle * (2*math.pi/360))
    if between_track:
        c = "red"
    else:
        c = "green"
    axes.add_artist( lines.Line2D( (0.5,0.5+dx), (0.5,0.5+dy), c=c, linewidth=5) )

    if next_mag_angle is not None:
        dx = 0.4*cos(next_mag_angle * (2*math.pi/360))
        dy = 0.4*sin(next_mag_angle * (2*math.pi/360))
        axes.add_artist( lines.Line2D( (0.5,0.5+dx), (0.5,0.5+dy), c="black", linewidth=5) )

    axes.text(0,0.5,f"Step {t} Track: {subtrack}")

    stepper_angle, subtrack, between_track, next_mag_angle, dist_to_next_boundary = move_subtrack(magnet_states, stepper_angle, subtrack, time_to_go=3)

global axes

if False:
    for t in range(100):
        figure, axes = plt.subplots()
        animate(t)
        plt.show()
else:
    import matplotlib.animation as animation
    from matplotlib.animation import FuncAnimation
    figure, axes = plt.subplots()
    ani = animation.FuncAnimation(figure, animate, frames=300, blit = False)
    fname = 'test.gif'
    ani.save(fname, writer='pillow', fps=20)
    print(f"Wrote {fname}")
