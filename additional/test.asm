;;; acme -o additional/test additional/test.asm ; mv symbols additional
;;; acme -o test test.asm
	!sl "symbols"
	* = $800
hgr = 8192

	LDA #00
main_loop:
	ADC #1
	STA hgr
	STA $4000
	STA $400
	STA $800

test_mb1:
	MB_Base = $F0
	LDA #$c4
	STA $F1
	LDA #$00
	STA $F0


	LDY #4			; 2 cycles
	LDA (MB_Base),Y 	; 5 cycles; read MOCK_6522_T1CH
	sta target		; (*) 4 cycles
	INY			; (*) 2 cycles
	LDA (MB_Base),Y		; (*) 5 cycles; read MOCK_6522_T1CL
	sta target+1		; 4 cycles

	JMP main_loop
text1:	!byte 40,12
target:	!word 0
