s = """00 BRK “Sig" 7 *SP := PCH; SP -= 1
*SP := PCL; SP -= 1
*SP := P; SP -= 1; PBR.CLR
PCL := *fCP; DPL += 1
PCH := *fDP; SETF(SEI/CLD)
IR := *PC; PC += 1; END.INT
DPL := B := *PC; PC += 1"""

TRANS = {
    "DPL := B := *PC": "self.b = self.pc; self.dpl = self.b",
    "*SP := PCL;": "self.memset(sp, self.pcl)",
    "IR := *PC": "self.ir = self.mem(self.pc)",
    "*SP := PCL":  "self.memset[sp] := self.pcl",
    "PCL := *fCP": "self.pcl = self.mem( 0xFF00 + self.which_interrupt())",
    "PCH := *fDP": "self.pch = self.mem( 0xFF00 + self.dpl)",
    "DPL += 1": "self.dpl.wrapping_add(1)",
    "SETF(SEI/CLD)": "self.set_clear_flags(FLAG_I, FLAG_D)",
    "DPL := B := *PC": "self.b = self.mem(self.pc); self.dpl = self.b",
    "PC += 1": "self.pc.wrapping_add(1)",
    "*SP := PCH" : "self.memset(sp, self.pch)",
    "SP -= 1": "self.sp.wrapping_sub(1)",
    "*SP := PCL" : "self.memset(sp, self.pcl)",
    "*SP := P": "self.memset(sp, self.p)",
    "END.INT": "self.end_irq_sequence()"
    #"PBR.CLR" : None # 65816
}

def parse_micro(m):
    if m in TRANS:
        return TRANS[m]
    else:
        return "???"




a = s.split("\n")
opcode_nr = 0
print("   /* BRK */")
for ndx, line in enumerate(a):
    micro = line.split(";")

    print(f"   {(opcode_nr << 3) + ndx:03X} => {{ {'; '.join([parse_micro(m.strip()) for m in micro])} }},")
