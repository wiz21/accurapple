import numpy as np
from PIL import Image

APPLE_YRES=192

COLORS = { 0: np.array((0, 0, 0)),  # Black
           8: np.array((148, 12, 125)),  # Magenta
           4: np.array((99, 77, 0)),  # Brown
           12: np.array((249, 86, 29)),  # Orange
           2: np.array((51, 111, 0)),  # Dark green
           10: np.array((126, 126, 126)),  # Grey2
           6: np.array((67, 200, 0)),  # Green
           14: np.array((221, 206, 23)),  # Yellow
           1: np.array((32, 54, 212)),  # Dark blue
           9: np.array((188, 55, 255)),  # Violet
           5: np.array((126, 126, 126)),  # Grey1
           13: np.array((255, 129, 236)),  # Pink
           3: np.array((7, 168, 225)),  # Med blue
           11: np.array((158, 172, 255)),  # Light blue
           7: np.array((93, 248, 133)),  # Aqua
           15: np.array((255, 255, 255)),  # White
          }


def hgr_address( y):
    #assert page == 0x2000 or page == 0x4000, "I'll work only for legal pages"
    assert 0 <= y < APPLE_YRES, "You're outside Apple's veritcal resolution"

    if 0 <= y < 64:
        ofs = 0
    elif 64 <= y < 128:
        ofs = 0x28
    else:
        ofs = 0x50

    i = (y % 64) // 8
    j = (y % 64) % 8

    return ofs + 0x80*i + 0x400*j

with open("mem.bin","rb") as file_in:
    main_ram = file_in.read(0x10000)
    main_bank2_ram = file_in.read(0x1000)
    aux_ram = file_in.read(0x10000)
    aux_bank2_ram = file_in.read(0x1000)

    print(len(main_ram))
    print(len(main_bank2_ram))
    print(len(aux_ram))
    print(len(aux_bank2_ram))

image2 = np.ones((192,140,3),dtype="uint8")

for y in range(192):
    ofs = hgr_address(y) + 0x2000

    px = 0

    for x in range(140//7):

        # From the Apple IIC Technical reference manual, table 5-6.

        # ?BBBAAAA ?DDCCCCB ?FEEEEDD ?GGGGFFF
        # Aux N    Main N   Aux N+1  Main N+1  (N even)

        pA = (aux_ram[ofs]     &  0b0001111)
        pB = ((aux_ram[ofs]    &  0b1110000) >> 4) | ((main_ram[ofs] & 0b1) << 3)
        pC = ((main_ram[ofs]   &  0b0011110) >> 1)
        pD = ((main_ram[ofs]   &  0b1100000) >> 5) | ((aux_ram[ofs+1] & 0b11) << 2)
        pE = ((aux_ram[ofs+1]  &  0b0111100) >> 2)
        pF = ((aux_ram[ofs+1]  &  0b1000000) >> 6) | ((main_ram[ofs+1] & 0b111) << 1)
        pG = ((main_ram[ofs+1] &  0b1111000) >> 3)
        ofs += 2


        image2[y,px+0,:] = COLORS[pA]
        image2[y,px+1,:] = COLORS[pB]
        image2[y,px+2,:] = COLORS[pC]
        image2[y,px+3,:] = COLORS[pD]
        image2[y,px+4,:] = COLORS[pE]
        image2[y,px+5,:] = COLORS[pF]
        image2[y,px+6,:] = COLORS[pG]

        px += 7

Image.fromarray(image2,"RGB").resize((140*4, 192*2), Image.NEAREST).show()
