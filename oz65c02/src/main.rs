use std::fs::read;
use std::env;
use std::time::Instant;

//mod wdc65c02;
mod n6502;

fn load_ram(filename: &str, addr: u16) -> Vec<u8> {
    let mut ram: Vec<u8> = vec![0; 65536];
    let bytes = read(filename).expect("File not found");
    ram[addr as usize..addr as usize + bytes.len()].copy_from_slice(bytes.as_slice());
    ram
}

fn parse_u16(s: &String) -> u16 {
    let no_prefix = s.strip_prefix("0x").expect("No prefix 0x");
    return u16::from_str_radix(no_prefix, 16).expect("Not a valide hex");
}


fn test_irq() {
    let mut ram = [0 as u8; 65536];

    // Starting at $C00
    ram[0xfffc] = 0x00;
    ram[0xfffd] = 0x0C;

    // IRQ handler at 0xD00
    ram[0xfffe] = 0x00;
    ram[0xffff] = 0x0D;

    const NOP: u8 = 0xEA;
    const CLI: u8 = 0x58;
    const RTI: u8 = 0x40;
    const SEI: u8 = 0x78;

    // $C00: CLI # Enable interrupts
    // $C01: NOP
    // $C02: NOP
    // ..: NOP
    ram[0xC00] = SEI;
    for i in 0xC01..0xD20 {
        ram[i] = NOP;
    }
    ram[0xC0A] = CLI; // Enable interrupts after about 0xA*2 cycles
    // $D00: NOP # Dummy IRQ handler
    // $D01: RTI
    ram[0xD01] = RTI;

    let mut cpu = oz65c02::wdc65c02::Cpu::new();
    //let mut cpu = n6502::Cpu::new();
    cpu.res_pull_down();

    let mut cycles: u64 = 0;

    while cycles < 62 {

        if cycles == 12 {
            println!("*************** IRQ Pull down");
            cpu.irq_pull_down();
        } /* else if cycles == 13 {
            println!("*************** IRQ Pull up");
            cpu.irq_pull_up();
        } */ else if cycles == 45 {
            println!("*************** IRQ Pull down (2)");
            cpu.irq_pull_down();
        } else if cycles == 48 {
            println!("*************** IRQ Pull up (2)");
            cpu.irq_pull_up();
        }


        let ad: u16 = cpu.ad.into();
        if cpu.rw != 0 { // read
            cpu.d = ram[ad as usize];
        } else { // write
            ram[ad as usize] = cpu.d;
        }

        let ad: u16 = cpu.ad.into();
        let pc: u16 = cpu.pc.into();
        let cgl: u16 = cpu.cgl.into();
        let ir: u16 = cpu.ir.into();
        let p: u64 = cpu.p.into();

        println!("Cycle:{cycles} IR={:#04x} tick={} A={:#04x} X={:#04x} Y={:#04x} NV1BDIZC={:#018x} SP={:#04x} PC={:#06x} CGL={:#06x} Q={}",
            ir, cycles, cpu.a, cpu.x, cpu.y, p, cpu.sp.l, pc, cgl, cpu.q);
        println!("{} ad={:#06x} v={:#04x} sync={} ",
            ["Write", "Read "][cpu.rw as usize], ad, [cpu.d, ram[ad as usize]][cpu.rw as usize], cpu.sync());
        println!("microinstr={} n_int_pnd={} n_irq_pnd={}",
            8 * cpu.ir as usize + cpu.q as usize, cpu.n_int_pnd, cpu.n_irq_pnd);
        cpu.cycle();
        println!("");

        cycles = cycles + 1;
    }

    println!("////////////////// IRQ TEST COMPLETE");
}

pub fn main() {
    test_irq();
    return;

    let args: Vec<String> = env::args().collect();
    let args = [
        "".to_string(),
        "c:/Users/StephaneC/works/accurapple/speaker/noise.o".to_string(),
        "0x0C00".to_string(),
        "0x0E00".to_string(),
        "0x0C00".to_string(),];

    if args.len() != 4 && args.len() != 5 {
        println!("Usage: {} filename base_addr stop_addr [start_addr]\n", args[0]);
        return;
    }

    let filename = &args[1];
    let base_addr: u16 = parse_u16(&args[2]);
    println!("File: {}\nBase address: {:04x}", filename, base_addr);
    let stop_addr: u16 = parse_u16(&args[3]);
    println!("Stop address: {:04x}", stop_addr);

    let mut ram = load_ram(filename, base_addr);

    if args.len() == 5 {
        let start_addr: u16 = parse_u16(&args[4]);
        println!("Start address: {:04x}", start_addr);
        ram[0xbffc] = 0x00; // for interrupts test
        //let start: wdc65c02::U8Pair = start_addr.into();
        let start: n6502::U8Pair = start_addr.into();
        ram[0xfffc] = start.l;
        ram[0xfffd] = start.h;
    }


    //let mut cpu = wdc65c02::Cpu::new();
    let mut cpu = n6502::Cpu::new();
    cpu.res_pull_down();

    let now = Instant::now();
    let mut cycles: u64 = 0;
    // while stop_addr != cpu.pc.into() {
    while cycles < 20 {
        cpu.cycle();

            if cycles == 10 {
                cpu.irq_pull_down();
            }

            if cycles == 10 {
                cpu.irq_pull_down();
            }

        let ad: u16 = cpu.ad.into();
        let sp: u16 = cpu.sp.into();
        let dp: u16 = cpu.dp.into();
        let pc: u16 = cpu.pc.into();
        let cgl: u16 = cpu.cgl.into();
        let ir: u16 = cpu.ir.into();

        let p: u64 = cpu.p.into();

       if cycles < 20 {
           println!("IR={:#04x} tick={} A={:#04x} X={:#04x} Y={:#04x} NV1BDIZC={:#018x} SP={:#04x} PC={:#06x} CGL={:#06x} Q={}",
                    ir, cycles, cpu.a, cpu.x, cpu.y, p, cpu.sp.l, pc, cgl, cpu.q);
       println!("{} ad={:#06x} v={:#04x} sync={}",
       ["Write", "Read "][cpu.rw as usize], ad, [cpu.d, ram[ad as usize]][cpu.rw as usize], cpu.sync());
           println!("");
       }

        let p: u8 = cpu.p.into();
//        if cycles > 84030549 && cycles < 84031000 {
//            println!("IR={:02x} tick={} A={:02x} X={:02x} Y={:02x} NV1BDIZC={:02x} SP={:04x} DP={:04x} PC={:04x} CGL={:04x} Q={}",
//                     ir, cycles, cpu.a, cpu.x, cpu.y, p, sp, dp, pc, cgl, cpu.q);
//        println!("{} {:04x} {:02x}", ["W", "R"][cpu.rw as usize], ad, [cpu.d, ram[ad as usize]][cpu.rw as usize]);
//            println!("");
//        }

	  if cpu.rw != 0 { // read
            cpu.d = ram[ad as usize];
        } else { // write
            // if ad == 0xa000 {
            //     println!("Test case {} (cycle {})", cpu.d, cycles);
            // }

            // // feedback register for interrupt test
            // if ad == 0xbffc {
            //     let diff = cpu.d ^ ram[ad as usize];
            //     if diff & 0x01 != 0 {
            //         if cpu.d & 0x01 != 0 {
            //             cpu.irq_pull_down();
            //         } else {
            //             cpu.irq_pull_up();
            //         }
            //     }
            //     if diff & 0x02 != 0 && cpu.d & 0x02 != 0 {
            //         cpu.nmi_pull_down();
            //     }
            // }

            ram[ad as usize] = cpu.d;
        }
        cycles = cycles + 1;
    }

    let duration = now.elapsed().as_nanos() as f64;
    let frequency = 1000.0 / (duration / cycles as f64);
    println!("");
    println!("Cycles: {}", cycles);
    println!("Time: {} ns", duration);
    println!("Frequency: {} MHz", frequency);
    println!("Test OK\n");
}
