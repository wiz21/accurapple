v1.7.1, January 2025
- Fixed MacOS build. Now Accurapple really works on MacOS :-)

v1.7.0, January 2025
- Improved graphics emulation system. We are now very close to 100% accurate
  as far as RAM-to-composite conversion is concerned
- F5 now starts/stops the debugger
- Added "softwarelibrary_apple_woz_games" collection to games DB. We now have
  about 9500 titles (but there are still many non games in that and many
  games don't necessarily have a cover)
- Added script commands SET_GFX_DEBUG_LINE, SAVE_GFX_DEBUG, IOU_GFX_INPUT
- Added LOAD_FLOPPY script command
- Added Turbo On/Off button
- Added support for write protected WOZ (fixes Wizardry)
- "Composite" and "phase" options for display are now memorized across different
  executions
- Fixed floating bus calculations (this improves Crazy Cycles, Ansi Story, Mad
  Effects, and several others)
- Fixed ALTCHARSET soft switch
- Fixed broken --charset CLI parameter
- Fixed keyboard input once mode
- Fixed default symbols loading
- Migrated to egui 29.0
- Replaced Motter-Tektura font by a font with Open Font License. See
  https://fontstruct.com/fontstructions/show/2463449/motter-tektura-neue

v1.6.3, August 2024
- Fixed $C011 (thanks Cédric!)
- Fixed very dumb bug in WOZ flux construction

v1.6.2, August 2024
- Fixed 6522 second timer behaviour
- Fixed 6522 IER/IFR handling

v1.6.1, August 2024
- Fixed some IRQ handling (now Latecomer works)
- Fixed some a2audit warnings

v1.6.0, July 2024
- Added scripting command JOYSTICK
- Added scripting command D2_STEPPER_REC_START/STOP
- Added preview of HGR/DHGR memory in debugger
- Floppies are now writable (but still not saved to host)
- Fixed 6502 emulator selection
- Fixed 80STORE and PAGE2 interaction (fixes Test Drive)
- Fixed broken space key
- Improved game database (roughly up to names starting with letter E)

v1.5.1b, June 2024
- Fixed missing frames in video recording

v1.5.0, June 2024
- added --log-stdout to log to STDOUT
- fixed reset when MockingBoard is playing
- fixed delay in sound when audio thread is late to the party
- fixed woz's random number generator
- clippy fixes

v1.4.0, June 2024
- Many improvements to the debugger
- Added more scripting commands
- Video recording
- Screenshot of composite signal
- Sound recording
- Speaker's "ticks" recording
- New graphics architecture: we simulate the all the timing signals of
  the IOU. But there are still bugs.
- The drive stepper is now simulated with a simple harmonic
  oscillator. It's a bit bugged but hopefully, this will allow to
  reproduce several edge behaviours.
- Moby games covers are now displayed for games DSK files coming from
  ftp.asimov.net (last checked about a year ago). Result is so-so
  because Moby's database is far from complete and it's a bit tough to
  match the DSK to the actual games.
- Moved to webgpu/shader rendering
- Added hue/saturation/value filtering
- Added some controls over the composite decoding
- Added experimental 6502/65c02 by Obscoz ! Merci JN!

v1.3.1, May 2023
- Fixed memory bug (Bug attack)
- Fixed file dialog

v1.3.0, May 2023
- Added DGR graphics mode (was kind of lost in last release :-( )

v1.2.0, May 2023
- All graphics modes
- New UI
- Debugger in egui (much nicer and powerful)
- Improved speaker's rendering with simple harmonic oscillator
- Tons of fixes and small improvements
- Moved to OpenGL for rendering and using shaders for PAL conversion
- Support for some of the HGR/GR transition video glitches
- Woz flux
- Better floppy drive stepper
- Floating bus
- AN3 support

v1.1.0, June 2022
- Basic scripting
- Mockingboard and 6522
- Video recording
- Small debugger
- Joystick
- Woz support
- Better keyboard

v1.0.0, April 2022
- Basic emulation
