!to "scott.bin", plain
* =   $6000

CYCLES_PER_FRAME = 20280
				; ** BIOS locations **
A1L       =   $3C
A1H       =   $3D
A2L       =   $3E
A2H       =   $3F
PAGE = $FF

NXTA1     =   $FCB4         ; routine to increment (A1) and compare (A2)
;
HBASL     =   $26           ; hires base address (low byte)
HPOSN     =   $F411         ; set HBASL to base address of HGR line in A
HGRPAGE   =   $E6           ;base address ($20 or $40)
MONCH     =   $24           ;cursor horizontal position


RAMRW_D000B1  =      $c08b           ; read and write RAM in $d000-$ffff
SET80MODE     =      $c00d           ; turn on 80 columns
RAMRDOFF      =      $c002           ; turn off RAMRD
RAMRDON       =      $c003           ; turn on RAMRD
RAMWRTOFF     =      $c004           ; turn off RAMWRT
RAMWRTON      =      $c005           ; turn on RAMWRT
VERTBLANK     =      $c019           ; vertical blank
CLR80COL      =      $c000           ; disable 80 column store
KBD           =      $c000           ; read keyboard
KBDSTRB       =      $c010           ; clear keyboard strobe
VBLINTON      =      $c05b           ; enable VBL interrupt
VSYNCIRF      =      $c070           ; reset VSYNC IRF (Interrupt Flag)
IOUOFF        =      $c078           ; disable IOU access
IOUON         =      $c079           ; enable IOU access

; ** Screen soft switches **
TEXTOFF   =   $C050
TEXTON    =   $C051
MIXEDOFF  =   $C052
MIXEDON   =   $C053
PAGE1     =   $C054
PAGE2     =   $C055
LORES     =   $C056
HIRES     =   $C057
AN3ON     =   $c05e           ; enable double-width graphics
AN3OFF    =   $c05f           ; disable double-width graphics


!macro wait_cycles .N {

	LDA # (.N - 34) >> 8
	LDX # (.N - 34) & 255
	JSR waiter

}


INIT:
	JSR enable_vbl

	SEI
	JSR hgr_page_copy
	;; JSR   HISCRUB		; start by scrubbing the HGR screen
	;; JSR LOPAGERND

				;
FINDSYNC  LDA   HIRES         ;Choose hi resolution mode
          LDA   PAGE1         ;Default to page 1
          LDA   MIXEDOFF      ;text window off until sync acquired
          LDA   TEXTOFF       ; scan must occur in graphics mode
;
;* Various find loops
;* 1. FIND HIGH BIT for graphics without half-dot-shift
;
FINDLOOP  BIT   TEXTOFF
          BPL   FINDLOOP      ; keep searching if high bit isn't set

VERTBLANK   	= $C019

	JMP skipper

fine_wait:
	N = CYCLES_PER_FRAME - 1 - 6 - 3
	LDA # (N - 34) >> 8	; 2
	LDX # (N - 34) & 255	; 2
	JMP waiter		; 3
skipper:

	;; The general idea is this:
	;; Detectct the start of the drawing area. When done
	;; a few cycles have passed so we're inside the drawing area.
	;; Now, we wait a little less than the duration of a full
	;; frame so that we're a bit "higher" on the screen, we do
	;; that with a big step then with 1 cycle steps.
	;; We continue until we're back in the VBL area, just above the
	;; drawing area. Doing it like that allows us to be on the
	;; very cycle where the frame draw start.

	JSR switch_display
	;; Returning from JSR is 6 cycles
	N = CYCLES_PER_FRAME - 6 - 4
	LDA # (N - 34) >> 8	; 2
	LDX # (N - 34) & 255	; 2
	JSR waiter		; 3

	;; at this point we're in the drawing zone

	!for X,1,16 {
	;; If no branch, This will have the total effect of waiting
	;; CYCLES_PER_FRAME minus one cycle.
	lda     VERTBLANK       ; 4
	;; A=$80=Drawing; A=0=VBL
        bpl     zed		; 2 or 3 branch if in the VBL part
	JSR fine_wait
	}

	;; -------------------------------------------------------------
vbl1:
	JSR switch_display
zed:


	;; At this point <we're at the beginning of the frame.
	;; on the very first line to draw.
	;; I wait to get into the visible zone.

	INC $2000		; 6
	NOP			; 2

	LDX #64			; 2
	LDY #64			; 2

SYNCLOOP:
	CMP   HIRES       ;Cols 00 01 02 03
        CMP   LORES       ;     04 05 06 07
        CMP   HIRES       ;     08 09 10 11
          ;; CMP   LORES       ;     12 13 14 15
	LDA $FF
        CMP   LORES       ;     16 17 18 19
        CMP   LORES       ;     16 17 18 19
	CMP   HIRES       ;     20 21 22 23
          ;; CMP   LORES       ;     24 25 26 27
	NOP
        CMP   LORES       ;     28 29 30 31
        CMP   LORES       ;     32 33 34 35
        CMP   LORES       ;     36 37 38 39
        CMP   HIRES       ;     44 45 46 47
        CMP   HIRES       ;     48 49 50 51
        CMP   HIRES       ;     52 53 54 55   --- [4] + 34 = 38
	LDA $FF

	LDA   $C010
	DEX
        BNE   SYNCLOOP      ;[3] + 3E = 41

	NOP			; -1 + 3 = 2
	LDA $C010		; 14*4=56 -> 58
	!for outer,1,13 {
	LDA $C010		; 14*4=56 -> 58
	}
	NOP			; 2 -> 60
	LDA $FF			; 3 -> 63

	;; here, we are one cycle too short

SYNCLOOP2  CMP   HIRES       ;Cols 00 01 02 03
          CMP   LORES       ;     04 05 06 07
          CMP   HIRES       ;     08 09 10 11
          CMP   LORES       ;     12 13 14 15
          CMP   LORES       ;     16 17 18 19
          CMP   HIRES       ;     20 21 22 23
          CMP   LORES       ;     24 25 26 27
          CMP   LORES       ;     28 29 30 31
          CMP   LORES       ;     32 33 34 35
          CMP   HIRES       ;     36 37 38 39

          CMP   HIRES       ;     .. .. .. ..
          CMP   HIRES       ;     HBL
          CMP   HIRES       ;     HBL
          CMP   HIRES         ;[4] + 34 = 38
          ;; NOP                 ;[2] + 38 = 3A
          ;; LDA   $C000         ;[4] + 3A = 3E
	NOP
	NOP
	DEY
          BPL   SYNCLOOP2      ;[3] + 3E = 41

	;; ---
	NOP
	;; ---

	;; Wait 2 cycles
	;; NOP

	;; Wait 3 cycles
	 LDA $FF

	;; Wait 4 cycles
	;; NOP
	;; NOP

	;; Wait 5 cycles
	;; NOP
	;; LDA $FF


	LDA $C000
	BMI skip_cycle
skip_cycle:

	JMP vbl1



;
;* Scrub high bits out of hires memory
;
HISCRUB   LDA   #$20
          STA   HGRPAGE       ;Set HGR page address
          STA   A1H           ;Set (A1) to start address
          LDA   #$40
          STA   A2H           ;Set (a2) to end address
          LDY   #0
          STY   A1L
          STY   A2L
MASKLOOP  LDA   (A1L),Y       ; read next byte
          AND   #$7F          ; remove high bit
          STA   (A1L),Y       ; store it back
          JSR   NXTA1
          BCC   MASKLOOP
;
;* Now add a high bit to 64 bytes in a column starting at line 64
;
          LDA   #64           ; start at line 64
          STA   A1L           ; current line
          STA   A1H           ; current count
DEMASKLOOP LDA  A1L
          JSR   HPOSN
          LDY   #0
          LDA   (HBASL),Y
          ORA   #$80          ; add high bit
          STA   (HBASL),Y
          INC   A1L
          DEC   A1H
          BNE   DEMASKLOOP
          RTS

LOPAGERND LDA #$04
	STA A1H
	LDA #$00
	STA A1L
loop1
	LDY #$0
loop0	STA (A1L),Y
	INY
	BNE loop0
	INC A1H
	LDA A1H
	CMP #$0C
	BNE loop1
	RTS

hgr_page_copy:
	;; LDA $C054		; Page 1
	;; LDA $C057
	;; LDA $C050 ; display graphics; last for cleaner mode change (according to Apple doc)

	LDA #screen_hgr >> 8
	LDY #$20
	LDX #$20
	JSR page_copy

	LDA #screen_gr >> 8
	LDY #$04
	LDX #$4
	JSR page_copy

	RTS



page_copy:
	;; A = start page
	;; Y = page dest
	;; X = nb pages to copy
	STA page_source
	STY page_dest
	LDY #0
copy_loop_page:
copy_loop:
	page_source = * + 2
	LDA $AA00,Y
	page_dest = * + 2
	STA $AA00,Y
	INY
	BNE copy_loop
	INC page_source
	INC page_dest
	DEX
	BNE copy_loop_page
	RTS




; ----------------------------------------------------------------------------
; enable_vbl
; ----------------------------------------------------------------------------
enable_vbl:
        sei                     ; disable interrupts
        bit     IOUON
        bit     VBLINTON
        bit     IOUOFF
        lda     VSYNCIRF
        rts


; ----------------------------------------------------------------------------
; switch_display
; ----------------------------------------------------------------------------
switch_display:
sd1:	lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
        bpl     sd1              ;   IIe : wait for VBLANK start
        lda     VSYNCIRF        ; IIc : reset VSYNC IRF
sd2:	lda     VERTBLANK       ; IIe : wait for VBLANK end
        bmi     sd2
sd3:	lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
        bpl     sd3              ;   IIe : wait for VBLANK start

	;; once here, we're close to the drawn area of the screen
        rts




waiter !zone {
	;;;;;;;;;;;;;;;;;;;;;;;;
	;; Delays A:X clocks+overhead
	;; Time: 256*A+X+33 clocks (including JSR)
	;; Clobbers A. Preserves X,Y. Has relocations.
	;;;;;;;;;;;;;;;;;;;;;;;;
	;;  https://www.nesdev.org/wiki/Delay_code#256%C3%97A_+_X_+_33_cycles_of_delay,_clobbers_A,_Z&N,_C,_V

	sec
wloop1:	; 5 cycles done, do 256-5 more.
	sbc #1			; 2 cycles - Carry was set from cmp
	pha                     ; 3
	lda #(256-27 - 16)     ; 2
	jsr delay_a_27_clocks  ; 240
	pla                     ; 4
delay_256a_x_33_clocks:
	cmp #1			; +2
	bcs wloop1			; +3
	; 0-255 cycles remain, overhead = 4
	txa 			; -1+2; 6; +27 = 33

delay_a_27_clocks:
        sec
L:     	sbc #5
        bcs L  ;  6 6 6 6 6  FB FC FD FE FF
        adc #3  ;  2 2 2 2 2  FE FF 00 01 02
        bcc L4  ;  3 3 2 2 2  FE FF 00 01 02
        lsr     ;  - - 2 2 2  -- -- 00 00 01
        beq L5  ;  - - 3 3 2  -- -- 00 00 01
L4:     lsr     ;  2 2 - - 2  7F 7F -- -- 00
L5:     bcs L6  ;  2 3 2 3 2  7F 7F 00 00 00
L6:     rts     ;
}



	!align 255,0
screen_hgr:
!binary "screen.hgr"
	!align 255,0
screen_gr:
!binary "screen.gr"
