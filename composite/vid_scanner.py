EXPECTED_TEST16 = """
Line 16

00 17 00 10 00 11 00 12 00 57 00 50 00
51 00 52 00 00 97 00 90 00 91 00 92 00
D7 00 D0 00 D1 00 D2 01 17 01 10 01 11
"""

"""
Line 16: HBL:2168 HBL':2100
Line 17: HBL:2568 HBL':2500
Line 18: HBL:2968 HBL':2900
Line 19: HBL:2D68 HBL':2D00

"""


"""
Counter:
bit 8: V5
bit 7: V4
bit 6: V3
bit 5: V2
bit 4: V1
bit 3: V0
bit 2: VC
bit 1: VB
bit 0: VA

H5
...
H0

According to Sather (IIUC):

a line is: 12 cycles HBL-40cycles visible-13 cycles HBL.

first visible byte of screen is on line 0, clock 0

0-39 (40 visible); 40-64 (25 HBL)
65-104;            105-129

according to my crt beam:
line 0: cycle 0..12 (HBL, colour burst) cycle=13..39 (visible) CK = 53..65 (HBL + SYNC)

line 0:

according to Sather:
line 0: CK=53..64 (HBL, colour burst) CK=65 + 0..39 (visible) CK = 65 + 40..52 (HBL + SYNC)



"""

"""
On line 1: VA == 1, Vx == 0

"""

FIRST_VISIBLE_CRT_LINE=50
FIRST_VISIBLE_BYTE_ON_CRT_LINE = 12

def compute_V50CBA(line):
    if line <= 255:
        return 0b100_000_000 + line
    else:
        return 0b011_001_000 + (line - 256)

def compute_H05(cycle_on_line):
    # 0 == first HBL' (visible) byte
    # 40 = first HBL byte
    if cycle_on_line == 40:
        return 0
    elif 41 <= cycle_on_line <= 65:
        return cycle_on_line - 41
    elif 0 <= cycle_on_line <= 39:
        return cycle_on_line + 0b011_000
    else:
        raise Exception(f"H05 bad: {cycle_on_line}")

def compute_H05_v2(cycle_on_line, line_addr):
    """
    UtA2e 5-10:
    HBL scanned memory begins $18 bytes before display scanned memory. The HBL
    base address can be computed from the displayed base address using this
    Applesoft program sequence:

    10 HBL= BASE-24
    20 IF INTCHBL/128)<>INT(BASE/128) THEN HBL = HBL+128
    """


    if cycle_on_line <= 25:
        return cycle_on_line
    elif 41 <= cycle_on_line <= 65:
        return cycle_on_line - 41
    elif 0 <= cycle_on_line <= 39:
        return cycle_on_line + 0b011_000
    else:
        raise Exception(f"H05 bad: {cycle_on_line}")


def compute_both_scanner_counters(cycle_in_frame):

    line = cycle_in_frame // 65
    hpos = cycle_in_frame % 65

    # print(f"line={line} hpos={hpos}")

    if line < FIRST_VISIBLE_CRT_LINE:
        v = compute_V50CBA(line + 268)
    else:
        v = compute_V50CBA(line - FIRST_VISIBLE_CRT_LINE)

    #h = compute_H05(53 + hpos)

    # if 0 <= hpos < FIRST_VISIBLE_BYTE_ON_CRT_LINE:
    #     h = compute_H05(53 + hpos)
    # elif FIRST_VISIBLE_BYTE_ON_CRT_LINE <= hpos < FIRST_VISIBLE_BYTE_ON_CRT_LINE + 40 :
    #     h = compute_H05(hpos - FIRST_VISIBLE_BYTE_ON_CRT_LINE)
    # elif FIRST_VISIBLE_BYTE_ON_CRT_LINE + 40 <= hpos < FIRST_VISIBLE_BYTE_ON_CRT_LINE + 40 + 13:
    #     h = compute_H05(hpos - FIRST_VISIBLE_BYTE_ON_CRT_LINE)
    # else:
    #     raise Exception(f"bad hpos {hpos}")

    #h = compute_H05(hpos)

    return v, h

def compute_scanner_mem_addr(V50CBA, H05):
    V4_V3 = (V50CBA & 0b11000_000) >> 6
    V012  = (V50CBA & 0b00111_000) >> 3
    VABC  = V50CBA & 0b111

    H543 = (H05 & 0b111_000) >> 3
    H012 = H05 & 0b111
    SUMS = (0b1101 + H543 + (V4_V3 | (V4_V3 << 2))) & 0b1111

    scan = H012 | (SUMS << 3) | (V012 << 7)

    if False:
        # LORES page 1
        scan |= 0x400
    else:
        # HIRES
        scan |= VABC << 10
        scan |= 0x2000 # hires page 1

    return scan

if __name__ == "__main__":

    mem = [0]*65536

    c = 0
    i = 0
    while i < 0x2000:
        for _ in range(8):
            mem[0x2000 + i ] = c % 256
            i += 1
        for _ in range(8):
            mem[0x2000 + i ] = c // 256
            i += 1
        c += 1

    for base in [0x2100, 0x2500, 0x2900, 0x2D00]:
        print( f"${base+0x68:04X}: " + "".join(map(lambda n:f"{n:02X} ", mem[base+0x68: base + 0x68 + 25])), end="")
        print( f" | ${base:04X}: ", "".join(map(lambda n:f"{n:02X} ", mem[base: base + 40])), end="")
        print()
    print()

    """
    5-10: "The first address of HBL is always addressed twice consecutively,
    because H0—H5 is in the all zero state for two consecutive scans."
    """

    if False:
        line = 16
        for hpos in range(65):

            if hpos >= 1:
                hpos -= 1
            a = compute_scanner_mem_addr(compute_V50CBA(line), hpos)
            print(f"line:{line}, hpos:{hpos} addr= ${a:04X}")
        exit()

    k = 0
    start_line = 288 + 60

    # This +2 offset was set empirically. I would have expected this to be +4 as
    # it corresponds to the cycle at which the LDA is actually reading the
    # (floating) bus. I guess there are small issues with PHI0/PHI1 I didn't
    # get. If LDA is 4 cycles, maybe they are: 0,1,2,3 and somehow I'm just off
    # by one so instead of 3 I have to be at 2...
    # This was compared to my video scanner assembler test for a full scan.

    for cycle in range(65*start_line+2, 65*start_line+2078, 8):

        if cycle >= 60*65:
            cycle -= 60*65
        else:
            cycle += 192*65

        # line 0 == first displayed line.
        # line 192 == first VBL line.
        line = cycle // 65
        hpos = cycle % 65

        # Here, the 25 first scanned bytes are part of the HBL.
        # The next 40 are obviously the HBL' (visible part).
        if hpos >= 1:
            nhpos = hpos - 1
        else:
            nhpos = hpos
        a = compute_scanner_mem_addr(compute_V50CBA(line), nhpos)

        if False:
            print(f"line:{line:3}, hpos:{hpos:2} nhpos:{nhpos:2} addr= ${a:04X} mem={mem[a]:02X}")
        else:
            # Same display as the vidscan assembler test code.
            print(f"{mem[a]:02X} ", end="")
            if k % 13 == 12:
                print()
            k += 1
    print(f"last line: {line} (max is: 192+120=312)")
    exit()

    match 2:
        case 1:
            # First visible byte on screen
            first_line, hcycle = FIRST_VISIBLE_CRT_LINE, FIRST_VISIBLE_BYTE_ON_CRT_LINE
        case 2:
            # Test line 16
            # FIRST_VISIBLE_BYTE_ON_CRT_LINE-25 is what I see on the apple 2 (its scanner
            # first scans the 25 cycles of HBL). But I have to add 65 because it's a wrapping...

            first_line, hcycle = FIRST_VISIBLE_CRT_LINE+16, 52

    nb_lines = 1

    for line in range(first_line, first_line + nb_lines):

        vals = []
        for z in range(13*3):
            # The first LDA loads after 4 cycles and after that LDA/STA takes 8 cycles.
            ofs = hcycle + 4 + z*8
            cycle = line * 65 + ofs
            V50CBA, H05 = compute_both_scanner_counters(cycle)
            scan = compute_scanner_mem_addr( V50CBA, H05)
            #print(f"{scan:04X}")
            vals.append(mem[scan])

            print(f"{mem[scan]:02X} ", end="")
            if z % 13 == 12:
                print()

        print(EXPECTED_TEST16)

        V50CBA, H05 = compute_both_scanner_counters(line * 65 + hcycle)
        scan = compute_scanner_mem_addr( V50CBA, H05)


        if line % 8 == 0:
            print()
        v = f"{V50CBA:09b}"
        v2 = "".join(map(lambda n:f"{n:02X} ", vals))
        print(f"Line: {line:3} hgr line: {line-FIRST_VISIBLE_CRT_LINE:3} V543210CBA:{v[0:3]} {v[3:6]} {v[6:9]} H05:{H05:06b} addr: {scan:04X} {v2}")
