from enum import Enum
from tqdm import tqdm
from PIL import Image
from PIL.Image import Resampling
from matplotlib import pyplot as plt
import numpy as np
from scanner import qam_demodulation_kaiser_line, fix_range, OVERSAMPLING, M, post_process_color, side_by_side, recompute_sine

from vidutils import FlipFlop, SoftSwitches, Shifter, rom_address, is_rising,ShiftAction,load_video_rom,test_colour_screen_of_death, hgr_address, gr_address, test_choplifter
from vidutils import SoftSwitchMask



def set_flip_flop(ff: FlipFlop, d):
    ff.set(True, d)

def bnot(b):
    return 1 - b

def read_soft_switch_byte(y, byte_offset):
    return SoftSwitches(text=False, hires=True).as_byte()

def read_ram(y, byte_offset, soft_switch_byte):
    hires = (soft_switch_byte >> SoftSwitchMask.SOFT_SWITCH_BIT_POS_HIRES.value) & 1
    text = (soft_switch_byte >> SoftSwitchMask.SOFT_SWITCH_BIT_POS_TEXT.value) & 1
    col80 = (soft_switch_byte >> SoftSwitchMask.SOFT_SWITCH_BIT_POS_COL80.value) & 1

    if hires == 1:
        ofs = 0x2000 + hgr_address(y)
    else:
        ofs = 0x400 + gr_address(y)

    return RAM[ofs]


def gen1(N, soft_switches, y):
    SOFT_SWITCH_BIT_POS_TEXT = 3
    SOFT_SWITCH_BIT_POS_COL80 = 4
    SOFT_SWITCH_BIT_POS_HIRES = 5
    SOFT_SWITCH_BIT_POS_ALT_CHARSET = 6
    SOFT_SWITCH_BIT_PAGE2 = 7

    PHASE_0 = FlipFlop(0)
    PHASE_1 = FlipFlop(0)
    RASp = FlipFlop(0)
    AX = FlipFlop(0)
    Q3p = FlipFlop(0)
    VID7M = FlipFlop(0)
    LDPSp = FlipFlop(0)
    for i in range(1,N):
        s14M = (i-1) % 2
        s7M = ((i-1) >> 1) % 2
        CREF = ((i-1) >> 2) % 2
        H0 = ((i-1) // 28) % 2
        byte_offset = i//28
        soft_switches = read_soft_switch_byte(y, byte_offset)
        GRp = (soft_switches >> SOFT_SWITCH_BIT_POS_TEXT) & 1
        SEGB = bnot((soft_switches >> SOFT_SWITCH_BIT_POS_HIRES) & 1)
        s80COLp = bnot((soft_switches >> SOFT_SWITCH_BIT_POS_COL80) & 1)
        VID7 = read_ram(y, byte_offset, soft_switches) >> 7
        new_RASp = bnot((Q3p.d) | (s7M & bnot(RASp.d) & bnot(PHASE_0.d)) | (bnot(s7M) & bnot(AX.d) & PHASE_0.d) | (s7M & CREF & AX.d & H0 & PHASE_0.d) | (bnot(s7M) & bnot(CREF) & AX.d & H0 & PHASE_0.d))
        new_AX = bnot((bnot(RASp.d) & Q3p.d))
        new_Q3p = bnot((bnot(s7M) & bnot(AX.d) & bnot(PHASE_0.d)) | (bnot(RASp.d) & bnot(Q3p.d)) | (s7M & bnot(AX.d) & PHASE_0.d))
        new_PHASE_0 = bnot((RASp.d & bnot(Q3p.d) & PHASE_0.d) | (Q3p.d & bnot(PHASE_0.d)) | (bnot(RASp.d) & bnot(PHASE_0.d)))
        new_PHASE_1 = bnot((RASp.d & bnot(Q3p.d) & bnot(PHASE_0.d)) | (Q3p.d & PHASE_0.d) | (bnot(RASp.d) & PHASE_0.d))
        new_bump = (bnot(Q3p.d) & bnot(AX.d) & PHASE_0.d)
        new_VID7M = bnot((s7M & GRp) | (SEGB & bnot(GRp)) | (GRp & bnot(s80COLp)) | (PHASE_0.d & bnot(SEGB) & bnot(GRp) & VID7M.d) | (Q3p.d & bnot(SEGB) & bnot(GRp) & VID7M.d) | (AX.d & bnot(SEGB) & bnot(GRp) & VID7M.d) | (bnot(AX.d) & bnot(VID7) & bnot(Q3p.d) & bnot(SEGB) & bnot(PHASE_0.d) & bnot(GRp)) | (CREF & bnot(AX.d) & bnot(H0) & bnot(Q3p.d) & bnot(SEGB) & bnot(PHASE_0.d) & bnot(GRp)))
        new_LDPSp = bnot((s7M & bnot(RASp.d) & bnot(VID7) & bnot(Q3p.d) & bnot(SEGB) & bnot(PHASE_0.d) & bnot(GRp)) | (bnot(s7M) & bnot(RASp.d) & VID7 & bnot(Q3p.d) & bnot(SEGB) & bnot(PHASE_0.d) & bnot(GRp)) | (bnot(AX.d) & bnot(Q3p.d) & SEGB & bnot(PHASE_0.d) & bnot(GRp)) | (bnot(AX.d) & bnot(Q3p.d) & bnot(PHASE_0.d) & GRp) | (bnot(AX.d) & bnot(Q3p.d) & PHASE_0.d & GRp & bnot(s80COLp)) | (CREF & bnot(AX.d) & bnot(H0) & bnot(Q3p.d) & bnot(SEGB) & bnot(PHASE_0.d) & bnot(GRp)))
        if s14M:
            set_flip_flop(PHASE_0, new_PHASE_0)
            set_flip_flop(PHASE_1, new_PHASE_1)
            set_flip_flop(RASp, new_RASp)
            set_flip_flop(AX, new_AX)
            set_flip_flop(Q3p, new_Q3p)
            set_flip_flop(VID7M, new_VID7M)
            set_flip_flop(LDPSp, new_LDPSp)

# These come from http://www.applelogic.org/files/3410170A%20(ABEL).txt
equations = {

    "/RAS'": """
        Q3'
        +7M*/RAS'*/PHASE_0
        +/7M*/AX*PHASE_0
        +7M*CREF*AX*H0*PHASE_0
        +/7M*/CREF*AX*H0*PHASE_0""",

    "/AX" : "/RAS'*Q3'",

    "/Q3'": """
        /7M*/AX*/PHASE_0
        +/RAS'*/Q3'
        +7M*/AX*PHASE_0
    """,

    "/PHASE_0" : """
        RAS'*/Q3'*PHASE_0
       +Q3'*/PHASE_0
       +/RAS'*/PHASE_0
    """,

    "/PHASE_1" : """
        RAS'*/Q3'*/PHASE_0
        +Q3'*PHASE_0
        +/RAS'*PHASE_0
    """,

    "bump" : "/Q3' * /AX * PHASE_0", # /AX*/H0*

    "/VID7M" : """
    7M*GR'
    +SEGB*/GR'
    +GR'*/80COL'
    +PHASE_0 */SEGB*/GR'*VID7M
    +Q3'     */SEGB*/GR'*VID7M
    +AX      */SEGB*/GR'*VID7M
    +/AX*/VID7*/Q3'*/SEGB*/PHASE_0*/GR'
    +CREF*/AX*/H0*/Q3'*/SEGB*/PHASE_0*/GR'
    """,

    "/LDPS'" : """
    7M*/RAS'*/VID7*/Q3'*/SEGB*/PHASE_0*/GR'
    +/7M*/RAS'* VID7*/Q3'*/SEGB*/PHASE_0*/GR'
    +/AX*/Q3'*SEGB*/PHASE_0*/GR'
    +/AX*/Q3'*/PHASE_0*GR'
    +/AX*/Q3'*PHASE_0*GR'*/80COL'
    +CREF*/AX*/H0*/Q3'*/SEGB*/PHASE_0*/GR'
    """
    }

FLIP_FLOPS = ["PHASE_0","PHASE_1","RAS'","AX","Q3'","VID7M","LDPS'"]

SOFT_SWITCHES = ["GR'", "VID7", "SEGB", "80COL'"]

CONSTANTS = {
    # wgsl doesn't want "i-1" in expressions, I don't know why. I have
    # to stick i-1 in another variable to make it accept...
    "prev" : "i-1",
    "14M"  : "prev % 2",
    "7M"   : "(prev >> 1) % 2",
    "CREF" : "(prev >> 2) % 2",
    "H0"   : "(prev // 28) % 2"
}
# "H0" : [0] + ([0]*28+[1]*28)*(N//28),

def term_to_array_access(term, array_access="[i-1]"):
    if term[0] in "0123456789":
        term = "s"+term

    return term.strip().replace("/","not_").replace("'","p") + array_access

def symbol_name(term):
    term = term.strip().replace("/","")
    if term[0] in "0123456789":
        term = "s"+term
    return term.replace("'","p")

def ram_address(y, soft_switch_byte):
    hires = (soft_switch_byte >> SoftSwitchMask.SOFT_SWITCH_BIT_POS_HIRES.value) & 1
    text = (soft_switch_byte >> SoftSwitchMask.SOFT_SWITCH_BIT_POS_TEXT.value) & 1
    col80 = (soft_switch_byte >> SoftSwitchMask.SOFT_SWITCH_BIT_POS_COL80.value) & 1

    if hires == 1:
        ofs = 0x2000 + hgr_address(y)
    else:
        ofs = 0x400 + gr_address(y)

    return ofs


class Language(Enum):
    WGSL = 1
    PYTHON = 2

class PythonGen:
    def __init__(self):
        self.lines_out = []
        self._tab = "    "

    def _indent(self):
        self._tab += "    "

    def _deindent(self):
        self._tab = self._tab[0:-4]

    def line(self, line):
        self.lines_out.append( self._tab + line)

    def assignment(self, sym, expr):
        self.line(f"{sym}={expr}")

    def constant_u32(self, sym, value):
        self.line(f"{sym} = {value}")

    def for_begin(self, sym, start, end):
        self.line(f"for {sym} in range({start},{end}):")
        self._indent()

    def for_end(self):
        self._deindent()

    def if_begin(self, expr):
        self.line(f"if {expr}:")
        self._indent()

    def if_end(self):
        self._deindent()

    def create_flip_flop(self, name):
        self.line(f"{name} = FlipFlop()")

    def set_flip_flop(self, name, value):
        self.line(f"{name}.set(True, {value})")

    def any(self, expr):
        self.line(f"{expr}")

    def generate(self):
        with open("output.py","w") as fout:
            fout.write("\n".join(self.lines_out))


class WGSLGen:
    def __init__(self):
        self.lines_out = []
        self._tab = "    "

    def _indent(self):
        self._tab += "    "

    def _deindent(self):
        self._tab = self._tab[0:-4]

    def _rewrite_expr(self, expr):
        import re
        shift_re = re.compile(r".*>> *([0-9]+)")
        m = shift_re.match(expr)
        if m:
            print(expr)
            expr = f"{expr[:m.span(1)[0]]}{m.group(1)}u{expr[m.span(1)[1]:]}"
            print(expr)


        return expr.replace('//','/').replace("-1", "+i32(-1)")


    def line(self, line):
        self.lines_out.append( self._tab + line)

    def assignment(self, sym, expr, cast=None):
        if cast:
            self.line(f"let {sym}={cast}({self._rewrite_expr(expr)});")
        else:
            self.line(f"let {sym}={self._rewrite_expr(expr)};")

    def constant_u32(self, sym, value):
        self.line(f"const {sym}: u32={value}u;")

    def for_begin(self, sym, start, end):
        self.line(f"for( var {sym}: i32={start}; {sym}<{end}; {sym}++) {{")
        self._indent()

    def for_end(self):
        self._deindent()
        self.line("}")

    def if_begin(self, expr):
        self.line(f"if {expr} {{")
        self._indent()

    def if_end(self):
        self._deindent()
        self.line("}")

    def create_flip_flop(self, name):
        self.line(f"var {name}: FlipFlop = FlipFlop(0);")

    def set_flip_flop(self, name, value):
        self.line(f"{name}.d = {value};")

    def any(self, expr):
        self.line(f"{expr}")

    def generate(self):
        with open("output.wgsl","w") as fout:
            fout.write("\n".join(self.lines_out))


def generate_code(lang=Language.WGSL):

    gen = WGSLGen()

    lines_out = []

    for ssm in SoftSwitchMask:
        gen.constant_u32(ssm.name, ssm.value)

    for ff in FLIP_FLOPS:
        gen.create_flip_flop(symbol_name(ff))

    gen.for_begin("i", 0, "N")

    for var, eqn in CONSTANTS.items():
        gen.assignment(symbol_name(var),eqn)

    gen.assignment("byte_offset", "i//28", cast="u32") # Casting for WGSL
    gen.assignment("soft_switches", "read_soft_switch_byte(y, byte_offset)")
    gen.assignment("GRp", "(soft_switches >> SOFT_SWITCH_BIT_POS_TEXT) & 1")
    gen.assignment("SEGB", "bnot((soft_switches >> SOFT_SWITCH_BIT_POS_HIRES) & 1)")
    gen.assignment("s80COLp", "bnot((soft_switches >> SOFT_SWITCH_BIT_POS_COL80) & 1)")
    gen.assignment("VID7", "read_ram(y, byte_offset, soft_switches) >> 7", cast="i32")

    for var, eqn in equations.items():
        or_stmts = eqn.split("+")

        and_stmts = []
        for or_stmt in or_stmts:

            terms = []
            for term in map(lambda s:s.strip(), or_stmt.split("*")):
                base_term_name = term.replace("/","")
                sym = symbol_name(term)

                term_name = base_term_name.replace("'","p")
                if term_name[0] in "0123456789":
                    term_name = "s"+term_name

                if base_term_name in FLIP_FLOPS:
                    term_value = f"{term_name}.d"
                elif base_term_name in CONSTANTS:
                    term_value = sym
                else:
                    #print(f"{term_name} {rscan}/{len(vars[term_name])}")
                    term_value = f"{term_name}"

                if term.startswith("/"):
                    term_value = f"bnot({term_value})"

                terms.append(term_value)

            and_stmts.append(" & ".join(terms))

        if var.startswith("/"):
            gen.assignment( f"new_{symbol_name(var[1:])}", f"bnot(" + " | ".join(map("({})".format, and_stmts)) + ")")
        else:
            gen.assignment( f"new_{symbol_name(var)}", f"bnot(" + " | ".join(map("({})".format, and_stmts)) + ")")

    # wgsl understands boolean expression only.
    gen.if_begin("s14M != 0")
    for ff in FLIP_FLOPS:
        gen.set_flip_flop(symbol_name(ff), f"new_{symbol_name(ff)}")
    gen.if_end()

    gen.for_end()

    gen.generate()

# generate_code()
# exit()

def signals_for_line(memory, soft_switches: list[SoftSwitches], RAM, ROM, line_number):

    N = 14*40
    vars = {
        "14M" : [0,1]*N,
        "7M" : [0,0,1,1]*(N//2),
        "CREF" : [0,0,0,0,1,1,1,1]*(N//4),
        "H0" : [0] + ([0]*28+[1]*28)*(N//28),
        "GR'"  : [0]*(N*2),
        "SEGB" : [0] + [0]*(N*2),
        "80COL'" : [0] + [0]*(N*2),
        "VID7" : [1] + [1]*(N*2),
        "PHASE_0" : [0],
        "PHASE_1" : [0],
        "RAS'" : [0],
        "AX" : [0],
        "Q3'" : [0],
        "VID7M" : [0],
        "LDPS'": [0],
        "bump" : [0],
        "bump2" : [0],
        }

    # Apply a "zoom" factor
    for k in vars.keys():
        if len(vars[k]) > 1:
            vars[k] = np.repeat(vars[k], 8)
            #print(f"{k}: {len(vars[k])}")

    #memory = np.pad(memory, (0,2)) # Add two seroz at the end to prevent index out of bound in sloppy code

    mem_bus = [0] * len(vars["H0"])
    mem_scan_pos = [0] * len(vars["H0"])
    soft_switches_scanned: list[SoftSwitches] = [None] * len(vars["H0"])

    j = 0
    for i in range(len(vars["H0"])):
        mem = RAM[soft_switches[j].ram_scanner(line_number,j)] # memory[j]
        vars["VID7"][i] = (mem & 128) > 0
        vars["SEGB"][i] = bool(soft_switches[j].segb)
        vars["80COL'"][i] = not bool(soft_switches[j].col80)
        soft_switches_scanned[i] = soft_switches[j]

        mem_bus[i] = mem
        mem_scan_pos[i] = j
        if i >= 1 and vars["H0"][i-1] != vars["H0"][i]:
            j += 1

    flip_flops = {
        "PHASE_0" : FlipFlop(),
        "PHASE_1" : FlipFlop(),
        "RAS'" : FlipFlop(),
        "AX" : FlipFlop(),
        "Q3'" : FlipFlop(),
        "VID7M" : FlipFlop(),
        "LDPS'" : FlipFlop(),
    }


    for rscan in range(1,len(vars["14M"])-1):

        for var_eq, eq in equations.items():

            var_name = var_eq.replace("/","")

            eq = eq.split("\n")
            eq = [line for line in eq if not line.strip().startswith("#")]
            eq = " ".join(eq)

            or_terms = list(map(lambda s: s.strip(), eq.split("+")))
            # If one "+" is dangling in front of the eq, just ignore it.
            if not or_terms[0]:
                or_terms = or_terms[1:]
            #print(or_terms)

            disjunction = False
            for or_term in or_terms:

                terms = list(map(lambda s: s.strip(), or_term.split("*")))
                #print(terms)
                conjunction = True
                for term in terms:

                    term_name = term.replace("/","")

                    if term_name in flip_flops:
                        term_value = flip_flops[term_name].d
                    else:
                        #print(f"{term_name} {rscan}/{len(vars[term_name])}")
                        term_value = bool(vars[term_name][rscan-1])

                    if term.startswith("/"):
                        term_value = not term_value

                    #print(f"{term_name} = {term_value}")

                    conjunction = conjunction and term_value

                disjunction = disjunction or conjunction

            if var_eq.startswith("/"):
                vars[var_name].append(not disjunction)
            else:
                vars[var_name].append(disjunction)

        set_ff = bool(is_rising(vars["14M"][rscan], vars["14M"][rscan-1]))
        for ff_name, ff in flip_flops.items():
            ff.set(set_ff, vars[ff_name][-1])

    #print(f"{var_name} := {vars[var_name][scan]}")
    return mem_bus, mem_scan_pos, soft_switches_scanned, vars


def build_video_out(mem_bus, mem_scan_pos, soft_switches, vars, line_of_interest):
    shifter = Shifter()
    video_out = []
    vars["bump2"] = [0] * len(vars["14M"])
    big_count = 1
    ldps_loads = {}

    for rscan in range(1,len(vars["14M"])-1):
        byte = mem_bus[rscan]
        #byte = RAM[soft_switches.ram_scanner(line_of_interest, mem_scan_pos[rscan])]
        ra = rom_address( soft_switches[rscan], mem_scan_pos[rscan], line_of_interest, byte)
        #print(mem_scan_pos[rscan])

        # FIXME Check in ref that we use /VID7M as the input to the shifter
        # (and not VID7M)
        clock = is_rising(vars["14M"][rscan-1], vars["14M"][rscan]) and not vars["VID7M"][rscan]

        res = shifter.trigger(
            clock,
            vars["LDPS'"][rscan],
            ROM[ra])

        # if res == ShiftAction.SHIFT:
        #     print(f"{big_count%14:02d} {shifter.register:08b}")
        # elif res == ShiftAction.LOAD:
        #     print(f"{big_count%14:02d} {shifter.register:08b} <- {byte:08b} (mem_scan_pos={mem_scan_pos[rscan]})")

        if clock:
            vars["bump"][rscan] = vars["LDPS'"][rscan] - 0.5
            video_out.append(shifter.d)
            if not vars["LDPS'"][rscan]:
                ldps_loads[rscan] = byte # ROM[ra] # byte
        else:
            vars["bump"][rscan] = 0

        if res != ShiftAction.NONE:
            big_count += 1

        vars["bump2"][rscan] = shifter.d
    return ldps_loads, video_out

ROM = load_video_rom()
RAM, soft_switches_lines = test_choplifter()
lines_of_interest = range(12,25)
#RAM, soft_switches_lines = test_colour_screen_of_death()
#lines_of_interest = range(192)

if True:
    # Just one line
    video_out_final = []
    image = []
    for line_of_interest in tqdm(lines_of_interest):
        memory = None # RAM[0x2000+hgr_address(line_of_interest):0x2000+hgr_address(line_of_interest)+40]

        mem_bus, mem_scan_pos, soft_switches, vars = signals_for_line(memory, soft_switches_lines[line_of_interest], RAM, ROM, line_of_interest)
        ldps_loads, video_out = build_video_out(mem_bus, mem_scan_pos, soft_switches, vars, line_of_interest)

        video_out_final.append( vars["bump2"][8::16])

    video_out_final = np.array(video_out_final)
    np.save("video_out", video_out_final)

    image = []
    recompute_sine((270+33)/360*2*np.pi)
    for decimated in tqdm(video_out_final):
        # decimated in 4x oversampled
        # FIXME I need this to get the proper colours
        SHIFT_FACTOR = 3
        shifted = decimated
        # shifted = np.roll(decimated, SHIFT_FACTOR)
        # shifted[0:SHIFT_FACTOR] = 0
        # 12.5*180.0/8.0 = 281 = 270+11
        image.append( qam_demodulation_kaiser_line(shifted ,blur=int(M), iq_rotation=0))

    image = fix_range(np.log(1+np.array(image)))


    #plt.imshow(np.asarray(image))

    vars["VIDOUT"] = video_out
    print("Plotting")


    fig = plt.figure(figsize=(8, 8), layout='constrained')
    #fig, (ax1, ax2) = plt.subplots(2, 1, gridspec_kw = {'wspace':0, 'hspace':0})
    outer_grid = fig.add_gridspec(2, 1, wspace=0.0, hspace=0.0, height_ratios=[10, 1])
    ax1, ax2 = outer_grid.subplots()

    for i, nv in enumerate(vars.items()):
        name, values = nv
        if name in ("VID7M"):
            name, values = "/"+name, np.logical_not(values)

        base = len(vars) - i*4
        #sig_plot(name, values)
        ax1.plot(np.arange(len(values)), base + np.array(values), label=name)
        ax1.axhline(base,color="grey",linewidth="0.5")
        #plt.text(1,base+0.25,name,backgroundcolor="white")


    for i, item in enumerate(ldps_loads.items()):
        ck, val = item
        ax1.text(x=ck, y=len(vars) - 15*4 + [0.5,-1.5][i%2], s=f"{val&127:07b}"[::-1], fontsize="x-small")

    #ax1.set_title("Apple 2 Timing Signals Generation")
    ax1.set_yticks([len(vars) - i*4 for i, name in enumerate(vars.keys())],
            [name for i, name in enumerate(vars.keys())])
    ax1.set_xlim(0,560*16)
    ax1.set_xticklabels([])
    ax1.set_xticks([])

    ax2.imshow(image, aspect='auto')
    ax2.set_xticklabels([])
    ax2.set_xticks([])
    ax2.set_yticklabels([])
    ax2.set_yticks([])
    #plt.margins(y=0)
    #plt.subplots_adjust(wspace=0, hspace=0)
    #plt.tight_layout()
    plt.show()

# Reload previous step data (because we might comute it once because it's very slow)
video_out_final = np.load("video_out.npy") # .astype(np.uint8)*255
print(f"Input shape : {video_out_final.shape} {video_out_final.dtype}")
Image.fromarray(video_out_final,"L").save("csod_accura.png")

# Make a mosaic BROKEN :-(
if False:
    big_image = Image.new("RGB", (560*4, 192*2*4))
    images = []
    phase = np.pi
    for i in range(4):
        for j in range(4):
            image = []
            recompute_sine(phase)
            phase += np.pi/16
            for decimated in tqdm(video_out_final):
                # decimated in 4x oversampled
                # FIXME I need this to get the proper colours
                SHIFT_FACTOR = 3
                shifted = decimated
                # shifted = np.roll(decimated, SHIFT_FACTOR)
                # shifted[0:SHIFT_FACTOR] = 0
                # 12.5*180.0/8.0 = 281 = 270+11
                image.append( qam_demodulation_kaiser_line(shifted ,blur=int(M), iq_rotation=0))

            image = fix_range(np.log(1+np.array(image)))
            print(image.shape)
            image = Image.fromarray((image*255).astype(np.byte), "RGB").resize((560,192*2), Resampling.NEAREST)
            images.append(image)

            big_image.paste(image, (j*560, i*192*2))

    # best = pi + 11/16*pi = 27/16*pi ~= 270°+33°

    big_image.show()


if True:
    image = []
    recompute_sine((270+33)/360*2*np.pi)
    for vidline in tqdm(video_out_final):
        image.append( qam_demodulation_kaiser_line(vidline, blur=int(M), iq_rotation=0))

    image = fix_range(np.log(1+np.array(image)))
    zoom = 2
    image = Image.fromarray((image*255).astype(np.byte), "RGB").resize((560*zoom,192*2*zoom), Resampling.NEAREST)
    ref_image = Image.open("colored_screen_of_death.JPG")
    ref_image = ref_image.resize( (ref_image.width//4, ref_image.height//4) )
    side_by_side(image, ref_image).show()
