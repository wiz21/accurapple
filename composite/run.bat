del NEW.DSK
copy BLANK_PRODOS2.DSK NEW.DSK
REM c:\users\stephanec\opt\acme hgr_text.s
python make_tests_screens.py
c:\users\stephanec\opt\acme scott.s
c:\users\stephanec\opt\acme test_dhgr.s
c:\users\stephanec\opt\acme -r test_dhgr2.lst test_dhgr2.s
c:\users\stephanec\opt\acme test_dhgr3.s
c:\users\stephanec\opt\acme hgr_text.s
c:\users\stephanec\opt\acme test_hgr_simple.s
c:\users\stephanec\opt\acme hgr_text_scan.s
java -jar AppleCommander-1.3.5.jar -p NEW.DSK GRHGR BIN 0x6000 < scott.bin
java -jar AppleCommander-1.3.5.jar -p NEW.DSK DHGR BIN 0x6000 < test_dhgr.bin
java -jar AppleCommander-1.3.5.jar -p NEW.DSK ALLMODES BIN 0x6000 < test_dhgr3.bin
java -jar AppleCommander-1.3.5.jar -p NEW.DSK DHGR2 BIN 0x6000 < test_dhgr2.bin
java -jar AppleCommander-1.3.5.jar -p NEW.DSK HGRTEXT BIN 0x6000 < hgrtxt.bin
java -jar AppleCommander-1.3.5.jar -p NEW.DSK COMB BIN 0x6000 < HGR_SIMPLE.bin
java -jar AppleCommander-1.3.5.jar -p NEW.DSK HGRTXTSCN BIN 0x6000 < HGRTXTSCN.bin
REM Get-Content scott.bin | java -jar AppleCommander-1.3.5.jar -p NEW.DSK START BIN 0x6000
REM ..\target\release\accurapple.exe --cpu F6502 --floppy1 NEW.DSK
set RUST_LOG=accurapple=info,%RUST_LOG%
REM 0 key right = GR/HGR
REM 3 key right = ALLMODES
REM 4 key right = DHGR and TXT80 mixed
REM
REM ----- All modes -----
REM cargo run --release -- --cpu 6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT,KEY_RETURN, WAIT_UNTIL 20, GL_SCREENSHOT dhgr.bin, SAVE_ALL_MEM"
REM ----- DHGR -----
REM cargo run --release -- --cpu 6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RETURN, WAIT_UNTIL 20, GL_SCREENSHOT dhgr.bin, SAVE_ALL_MEM"
REM ----- GR and HGR -----
REM cargo run --release -- --cpu 6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RETURN, WAIT_UNTIL 20, GL_SCREENSHOT dhgr.bin, SCREENSHOT test.png, SAVE_ALL_MEM"
REM ----- TXT40 -----
cargo run --release -- --cpu 6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, GL_SCREENSHOT dhgr.bin, SAVE_ALL_MEM"
REM ----- DHGR2 -----
REM cargo run --release -- --cpu 6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT,KEY_RIGHT,KEY_RETURN, WAIT_UNTIL 20, GL_SCREENSHOT dhgr.bin, SAVE_ALL_MEM, TURBO_STOP"
REM ----- COMB -----
REM cargo run --release -- --cpu 6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT,KEY_RIGHT,KEY_RETURN, WAIT_UNTIL 20, GL_SCREENSHOT dhgr.bin, SAVE_ALL_MEM, TURBO_STOP"
REM ----- HGRTXTSCAN -----
REM cargo run --release -- --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT,KEY_RIGHT,KEY_RETURN, WAIT_UNTIL 20, GL_SCREENSHOT dhgr.bin, SAVE_ALL_MEM, TURBO_STOP"
REM cargo run --release -- --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT,KEY_RIGHT,KEY_RETURN, BP_PC 61CA"

REM --script "TURBO_START, WAIT_UNTIL 10, KEY_RETURN, WAIT_UNTIL 20, GL_SCREENSHOT dhgr.bin, SAVE_ALL_MEM, TURBO_STOP"
REM --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RETURN, WAIT_UNTIL 20, GL_SCREENSHOT dhgr.bin, SAVE_ALL_MEM, TURBO_STOP"
REM --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RETURN, WAIT_UNTIL 20, GL_SCREENSHOT dhgr.bin, TURBO_STOP"
REM --script "WAIT_UNTIL 13, KEY_RETURN"
REM cargo run --release -- --floppy1 NEW.DSK --script "WAIT_UNTIL 12, KEY_RETURN"

REM cargo run --release -- --floppy1 NEW.DSK --script "LOAD_MEM scott.bin $6000, SET_PC $6000"
REM python view_gl_signal.py .