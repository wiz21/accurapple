;;; acme.exe -r test_dhgr.lst .\test_dhgr.s
	!to "test_dhgr2.bin", plain

	* =   $6000
	!source "equs.s"
	;;  http://www.1000bit.it/support/manuali/apple/technotes/aiie/tn.aiie.03.html

	;; STA $C00A		; active the ROM so we have access to AUXMOVE
	;; STA $C000

	;; LDX #>screen_page1_hgr
	;; LDY #>screen_page1_hgr + $20
	;; LDA #$20
	;; STX A1H 		; start page
	;; STY A2H			; end page
	;; STA A4H			; destination page
	;; LDA #0
	;; STA A1L
	;; STA A4L
	;; LDA #$FF
	;; STA A2L
	;; ;;  SEC = main -> aux
	;; ;;  CLC = aux -> main
	;; SEC
	;; JSR AUXMOVE

	;; Now copying the DHGR image

	JSR load_gr_image
	JSR load_dhgr_image
	JSR set_dhgr

	JSR enable_vbl
	SEI

	JMP skipper
	!align 255,0		; Make sure we don't have extra cycle due to page crossing.

fine_wait:
	N = CYCLES_PER_FRAME - 1 - 6 - 3
	LDA # (N - 34) >> 8	; 2
	LDX # (N - 34) & 255	; 2
	JMP waiter		; 3
skipper:

	;; The general idea is this:
	;; Detectct the start of the drawing area. When done
	;; a few cycles have passed so we're inside the drawing area.
	;; Now, we wait a little less than the duration of a full
	;; frame so that we're a bit "higher" on the screen, we do
	;; that with a big step then with 1 cycle steps.
	;; We continue until we're back in the VBL area, just above the
	;; drawing area. Doing it like that allows us to be on the
	;; very cycle where the frame draw start.

	JSR switch_display
	;; Returning from JSR is 6 cycles
	N = CYCLES_PER_FRAME - 6 - 4
	LDA # (N - 34) >> 8	; 2
	LDX # (N - 34) & 255	; 2
	JSR waiter		; 3

	;; at this point we're in the drawing zone

	!for X,1,16 {
	;; If no branch, This will have the total effect of waiting
	;; CYCLES_PER_FRAME minus one cycle.
	lda     VERTBLANK       ; 4
	;; A=$80=Drawing; A=0=VBL
        bpl     zed		; 2 or 3 branch if in the VBL part
	JSR fine_wait
	}
	;; -------------------------------------------------------------

vbl1:
	JSR switch_display
zed:

	;; At this point we're at the beginning of the frame.
	;; on the very first line to draw, somewhere in the HBL

	LDX #64			; 2
	JSR wait650

SYNCLOOP2:

	;; --------------------------------------------------
	;; I wait to get into the visible zone
	;; This block must be 10 cycles
	NOP			; 2
	LDA $2000
	;; /// This block must be 10 cycles
	;; --------------------------------------------------

    ;; Now we're in the display area
	LDA $2000
	LDA $2000
	LDA TEXTOFF
	LDA $2000
	LDA $2000
	LDA $2000
	LDA $2000
	LDA $2000
	LDA LORES

	LDA HIRES
	CMP TEXTON		; 4
	CMP TEXTON
	CMP TEXTON
	NOP
	DEX ; 2
	BNE SYNCLOOP2 ; 3

	LDA TEXTOFF
	LDX #5
SYNCLOOP3:
	JSR wait650
	DEX
	BNE SYNCLOOP3

	LDA TEXTOFF ;; STA STORE80ON


    ;NOP ; 2 cycles
    ADC $80 ; 3 cycles
    ;CMP $2000 ; 4

    ;NOP
    ;ADC $FF ; 5 cycles

    ;NOP
    ;CMP $2000

    ;ADC $FF
    ;CMP $2000

    ;CMP $2000
    ;CMP $2000

	JMP vbl1
dummy_rts:
	RTS

;; freeze:
;; 	inc $2000
;; 	inc $200F
;; 	inc $4000
;; 	inc $4001

;; 	inc $400
;; 	inc $800
;; 	jmp freeze


page_copy !zone {
	;; A = start page
	;; Y = page dest
	;; X = nb pages to copy
	STA page_source
	STY page_dest
	LDY #0
.copy_loop_page
.copy_loop
	page_source = * + 2
	LDA $AA00,Y
	page_dest = * + 2
	STA $AA00,Y
	INY
	BNE .copy_loop
	INC page_source
	INC page_dest
	DEX
	BNE .copy_loop_page
	RTS
}

set_dhgr !zone {
	;;  Switch to DHGR mode
	;; See sather p. 820

	LDA TEXTOFF
	LDA MIXEDOFF
	LDA HIRES
	LDA PAGE1
	LDA AN3OFF
	STA COL80ON		; c00d
	;; STA STORE80ON
	RTS
}

load_gr_image !zone {
	;; The AUXRAM part
	STA STORE80ON		; 80STORE set
	LDA HIRES		; HIRES set
	LDA PAGE1		; selects RAM

	LDA #screen_page1_gr >> 8
	LDY #$04
	LDX #$4
	JSR page_copy

	LDA PAGE2		; selects AUXRAM

	LDA #screen_page1_gr >> 8
	LDY #$04
	LDX #$4
	JSR page_copy

	RTS
}

load_dhgr_image !zone {
	STA STORE80ON		; 80STORE set
	LDA HIRES		; HIRES set
	LDA PAGE1		; selects RAM

	;; The RAM part
	LDA #>screen_page1_hgr	; source page
	LDY #$20		; destination page
	LDX #$20		; number of pages
	JSR page_copy

	;; The AUXRAM part
	LDA PAGE2		; selects AUXRAM

	LDA #>screen_page1_aux_hgr
	LDY #$20		; destination page
	LDX #$20
	JSR page_copy
	RTS
}

	!align 255,0
wait650 !zone {
	;; Calling this is JSR+RTS = 12
	LDY #37			; 2
.loop:
	JSR .my_rts		; 6 + 6 for rts == 12
	DEY			; 2
	BNE .loop		; 3 => loop == 12+2+3=17 cycles * 37 = 629 - 1 for last branch not taken

	;; 12 + 2 + 628
	NOP
	NOP
	NOP
	NOP
	;; 12 + 2 + 628 + 8 == 650

.my_rts:
	RTS
}
wait50 !zone {
	;; Calling this is 12
	JSR .my_rts		; 12
	JSR .my_rts		; 12
	JSR .my_rts		; 12
	NOP		; 2
.my_rts:
	RTS
}
wait40 !zone {
	;; Calling this is 12
	JSR .my_rts		; 12
	JSR .my_rts		; 12
	NOP		; 2
	NOP
.my_rts:
	RTS
}

	!align 255,0
screen_page1_hgr:
!binary "screen_page1.hgr"
screen_page1_aux_hgr:
!binary "screen_page1_aux.hgr"
screen_page1_gr:
!binary "screen.gr"
