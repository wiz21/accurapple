import subprocess
from pathlib import Path
from utils import side_by_side, side_by_sidev
from PIL import Image
from matplotlib import pyplot as plt
import numpy as np

PRJDIR=Path(__file__).parent.parent
ACCURAPPLE_PATH=Path(__file__).parent.parent / "target" / "release" / "accurapple.exe"
print(ACCURAPPLE_PATH)

#  --floppy1 NEW.DSK --no-sound  --charset "%ACCURAPPLE_DIR%/data/Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin" --log-stdout --cpu F6502 --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT, KEY_RIGHT, KEY_RETURN, WAIT_UNTIL 14, SAVE_ALL_MEM allmem.bin,  SET_GFX_DEBUG_LINE 36, WAIT_UNTIL 15, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"

script = ["TURBO_START"] + \
    ["WAIT_UNTIL 10", "KEY_RETURN", "WAIT_UNTIL 15", "SCREENSHOT txt4080.png", "RESET"] + \
    ["WAIT_UNTIL 10", "KEY_RIGHT", "KEY_RIGHT", "KEY_RETURN", "WAIT_UNTIL 14", "KEY_RETURN", "WAIT_UNTIL 15", "SCREENSHOT allmodes.png", "RESET"] + \
    ["WAIT_UNTIL 10", "KEY_RIGHT", "KEY_RIGHT", "KEY_RIGHT", "KEY_RETURN", "WAIT_UNTIL 15", "SCREENSHOT grdgr.png", "RESET"] + \
    ["WAIT_UNTIL 10", "KEY_RIGHT", "KEY_RIGHT", "KEY_RIGHT","KEY_RIGHT", "KEY_RETURN", "WAIT_UNTIL 15", "SCREENSHOT grtxt.png", "RESET"] + \
    ["WAIT_UNTIL 1", "QUIT"]

script = ", ".join(script)

# SHADER = f"{PRJDIR}/accurashader/src/shaders/apple_to_composite_simplified.wgsl"
SHADER = f"{PRJDIR}/accurashader/src/shaders/apple_to_composite_hal.wgsl"

subprocess.run([ACCURAPPLE_PATH,
                "--a2c-shader", SHADER,
                "--charset", f"{PRJDIR}/data/Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin",
                "--floppy1", f"{PRJDIR}/composite/tests/NEW.DSK", "--no-sound", "--cpu", "F6502", "--log-stdout",
                "--script", script
                ])

script = ["TURBO_START"] + \
    ["WAIT_UNTIL 10", "KEY_RETURN", "WAIT_UNTIL 17", "SCREENSHOT grhgr.png", "RESET"] + \
    ["WAIT_UNTIL 10", "KEY_RIGHT","KEY_RIGHT","KEY_RETURN", "WAIT_UNTIL 17", "SCREENSHOT dhgrtxt.png", "RESET"] + \
    ["WAIT_UNTIL 1", "QUIT"]

script = ", ".join(script)

subprocess.run([ACCURAPPLE_PATH,
                "--a2c-shader", SHADER,
                "--charset", f"{PRJDIR}/data/Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin",
                "--floppy1", f"{PRJDIR}/composite/NEW.DSK", "--no-sound", "--cpu", "F6502", "--log-stdout",
                "--script", script
                ])


mosaic = side_by_sidev(
            side_by_side( side_by_side(Image.open("txt4080.png"),Image.open("allmodes.png")), Image.open("grhgr.png")),
            side_by_side( side_by_side( Image.open("grdgr.png"), Image.open("grtxt.png")),Image.open("dhgrtxt.png")))

mosaic.save("all_screenshots.png")

fig = plt.figure()
plt.imshow(np.asarray(mosaic))
fig.tight_layout()
plt.show()
