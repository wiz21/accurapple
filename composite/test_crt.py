from PIL import ImageEnhance
import numpy as np
from PIL import Image, ImageFilter
from PIL.Image import Resampling
from scipy.ndimage import gaussian_filter
from scipy.signal import convolve2d

def np_gaussian_filter(kernel_size, sigma=1, muu=0):

    # Initializing value of x,y as grid of kernel size
    # in the range of kernel size

    x, y = np.meshgrid(np.linspace(-1, 1, kernel_size),
                       np.linspace(-1, 1, kernel_size))
    dst = np.sqrt(x**2+y**2)

    # lower normal part of gaussian
    normal = 1.0/(2.0 * np.pi * sigma**2)

    # Calculating Gaussian filter
    gauss = np.exp(-((dst-muu)**2 / (2.0 * sigma**2))) * normal
    return gauss

def rgb_to_crt(hgr_image: Image.Image, gauss=3, lightness_factor=1):
    PIXEL_HEIGHT = 3
    PIXEL_SPACE = 1
    SCAN_LINE=1
    PIXEL_WIDTH = 3 + PIXEL_SPACE
    hgr = np.asarray(hgr_image).astype(np.float32)
    assert hgr.shape == (192,280, 3), f"{hgr.shape}"
    #hgr = np.asarray( Image.open("../additional/ntsc.png")).astype(np.float32)
    print(hgr.shape)

    npi = np.ones( (192*(PIXEL_HEIGHT+SCAN_LINE),280*PIXEL_WIDTH,3), dtype=np.float32 )

    for line in range(hgr.shape[0]):
        y = line * (PIXEL_HEIGHT+SCAN_LINE)
        for i in range(1,hgr.shape[1]-1):
            s = i%2
            s = 0
            ss = np.sum(hgr[line,i,:])/3*0.12
            ss = 0
            npi[y+s:y+PIXEL_HEIGHT+s, i*PIXEL_WIDTH, 0]   += hgr[line,i,0] + ss
            npi[y+s:y+PIXEL_HEIGHT+s, i*PIXEL_WIDTH+1, 1] += hgr[line,i,1] + ss
            npi[y+s:y+PIXEL_HEIGHT+s, i*PIXEL_WIDTH+2, 2] += hgr[line,i,2] + ss

            BLEED=0.15
            npi[y+s:y+PIXEL_HEIGHT+s, i*PIXEL_WIDTH, 0]   += (hgr[line,i+1,0] + hgr[line,i-1,0])*BLEED
            npi[y+s:y+PIXEL_HEIGHT+s, i*PIXEL_WIDTH+1, 1] += (hgr[line,i+1,1] + hgr[line,i-1,1])*BLEED
            npi[y+s:y+PIXEL_HEIGHT+s, i*PIXEL_WIDTH+2, 2] += (hgr[line,i+1,2] + hgr[line,i-1,2])*BLEED

    npi1 = np.ones_like(npi)
    gauss_kernel = np_gaussian_filter(gauss)
    gauss_kernel = gauss_kernel / np.sum(gauss_kernel)
    #gauss = gauss + gauss*1j
    for i in range(3):
        npi1[:,:,i] = convolve2d(npi[:,:,i], gauss_kernel, mode="same")

    npi = npi1 # *npi2

    # Gamma correction makes it much closer to my souvenir
    npi = (npi-np.min(npi))
    npi /= np.max(npi)
    #npi = np.power(npi,2.2)
    #npi = np.log(npi)
    #npi /= np.max(npi)
    #npi = npi*2

    import cv2
    hsl = cv2.cvtColor(npi, cv2.COLOR_RGB2HLS)
    hsl = np.array(hsl)
    print(f"{hsl.shape} Lightness max: {np.max(hsl[:,:,1])}")
    hsl[:,:,1] *= lightness_factor
    hsl[:,:,1] = np.clip(hsl[:,:,1],0,1)
    npi = cv2.cvtColor(hsl, cv2.COLOR_HLS2RGB)

    return Image.fromarray((np.clip(npi,0,1)*255).astype(np.uint8),mode="RGB")


if __name__ == "__main__":
    import argparse

    cli_args_parser = argparse.ArgumentParser()
    cli_args_parser.add_argument("filename", help="RGB image")
    #cli_args_parser.add_argument("-o", default=None, help="Output image")
    cli_args = cli_args_parser.parse_args()
    hgr =  Image.open(cli_args.filename )
    rgb_img = rgb_to_crt(hgr)
    rgb_img.save("crt_render.png")
    rgb_img.show()
