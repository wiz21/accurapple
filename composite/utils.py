from PIL import Image, ImageShow, ImageColor
from PIL import ImageEnhance, ImageOps
from PIL.Image import Resampling
from PIL import ImageFont, ImageDraw


APPLE_YRES = 64*3 # 192

def hgr_address( y):
    #assert page == 0x2000 or page == 0x4000, "I'll work only for legal pages"
    assert 0 <= y < APPLE_YRES, "You're outside Apple's veritcal resolution"

    if 0 <= y < 64:
        ofs = 0
    elif 64 <= y < 128:
        ofs = 0x28
    else:
        ofs = 0x50

    i = (y % 64) // 8 # 64 // 8 == 3 bits
    j = (y % 64) % 8 # 8 == 3 bits

    return ofs + 0x80*i + 0x400*j

def text_address(display_line):
    y = display_line // 8

    if 0 <= y <= 7:
        ofs = 0
    elif 8 <= y <= 15:
        ofs = 0x28
    elif 16 <= y <= 23:
        ofs = 0x50
    else:
        raise Exception(f"Wrong line ? {display_line} should be < 192")

    i = y % 8
    return ofs + 0x80 * i

gr_address = text_address


def rol(b):
    # 8 bits ROL operation
    return ((b << 1) & 0xFF) | (b >> 7)

assert rol(0x80+0x40) == 0x81

def ror(b):
    # 8 bits ROR operation
    return (b >> 1) | ((b & 1) << 7)



def side_by_side(a,b):
    separator = 10
    h = max(a.height, b.height)
    w = a.width + b.width
    combined_image = Image.new("RGB", (w+separator, h))
    combined_image.paste(a, (0, 0))
    combined_image.paste(b, (a.width+separator, 0))
    # combined_image.save("ntsc.png")
    return combined_image

def side_by_sidev(a,b):
    separator = 10
    h = a.height + b.height
    w = max(a.width, b.width)
    if a.width > b.width:
        a_pos = 0
        b_pos = (a.width - b.width)//2
    else:
        a_pos = (b.width - a.width)//2
        b_pos = 0
    combined_image = Image.new("RGB", (w, h+separator))
    combined_image.paste(a, (a_pos, 0))
    combined_image.paste(b, (b_pos, a.height + separator))
    # combined_image.save("ntsc.png")
    return combined_image
