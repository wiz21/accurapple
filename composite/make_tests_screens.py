from pathlib import Path
import numpy as np
from vidutils import gr_address, hgr_address

def make_gr(path: Path):
    gr = np.zeros( (0x400,), dtype=np.byte)

    for y in range(0,192,8):
        base_addr = gr_address(y)

        for x in range(0,40):
            if (x+y//8) % 3 == 0:
                c = 0xFF
            else:
                c = (y // 8) % 8
                c = c + 16*(c+8)

            gr[base_addr+x] = c

        gr[base_addr + 0] = 0xFF
        gr[base_addr + 39] = 0xFF

    gr.tofile(path)

def make_gr_and_80col(base_name):
    # Eight lines of GR modes
    # Followed by eight lines of 80COL

    gr_ram = np.zeros( (0x400,), dtype=np.byte)
    gr_auxram = np.zeros( (0x400,), dtype=np.byte)

    for y in range(0,8*8,8):
        base_addr = gr_address(y)

        for x in range(0,40):
            if (x+y//8) % 3 == 0:
                c = 0xFF
            else:
                c = (y // 8) % 8
                c = c + 16*(c+8)

            gr_ram[base_addr+x] = c

        gr_ram[base_addr + 0] = 0xFF
        gr_ram[base_addr + 39] = 0xFF


    offset = 0
    for y in range(8*8,16*8,8):
        base_addr = gr_address(y)

        for x in range(0,40):
            gr_ram[base_addr+x] = 128+x*2 + offset
            gr_auxram[base_addr+x] = 128+x*2 + offset + 1

        offset += 1

    gr_ram.tofile(f"{base_name}_ram.bin")
    gr_auxram.tofile(f"{base_name}_auxram.bin")



def make_hgr(path: Path, memory = None, start_y=0):
    if memory is None:
        memory = np.zeros((0x2000,), dtype=np.byte)

    for y in range(start_y,192):
        base_addr = hgr_address(y)

        bc = (y >> 2) & 0b11
        c = bc
        c = c + (bc << 2)
        c = c + (bc << 4)
        c = c + (bc << 6)
        c = c + (bc << 8)
        c = c + (bc << 10)
        c = c + (bc << 12)
        c = c + (bc << 14)

        delay = ((y >> 4) & 1) << 7

        #print(f"{c:16b}")
        #print(delay)
        for x in range(0,40,2):
            memory[base_addr + x + 1] = delay + (c & 0b01111111)
            memory[base_addr + x ] = delay + ((c >> 7) & 0b01111111)

    if path is not None:
        memory.tofile(path)
    else:
        return memory

def make_hgr_color(c):
    assert 0 <= c <= 7

    bits = c & 0b11
    all_bits = 0
    for i in range(7):
        all_bits += bits << (i*2)

    low = all_bits & 0b1111111
    high = (all_bits >> 7) & 0b1111111

    low = low | ((c >> 2) << 7)
    high = high | ((c >> 2) << 7)

    return low, high

def make_hgr_patterns_for_comb(path: Path, memory = None):
    if memory is None:
        memory = np.zeros((0x2000,), dtype=np.byte)

    TILE_H = 16
    TILE_W = 4
    for i in range(8):
        for j in range(8):
            for y in range(TILE_H):
                for x in range(TILE_W):
                    if y % 2 == 0:
                        low, high = make_hgr_color(i)
                    else:
                        low, high = make_hgr_color(j)
                    base_addr = hgr_address(j*(TILE_H+3)+y)
                    offset = base_addr + i*TILE_W + x

                    if x % 2 == 0:
                        memory[offset] = low
                    else:
                        memory[offset] = high

    memory.tofile(path)

def make_hgr_full_test(path: Path):
    memory = make_hgr(None, start_y=150)

    for c in range(8):
        delay_bit = (c & 4) << 5
        color = c & 3

        # Make one pixel

        y = 0
        memory[hgr_address(y) + c*2] = c | delay_bit

        # Then two pixels

        memory[hgr_address(y+2) + c*2] = (c + (c << 2))  | delay_bit

        # Then three pixels

        memory[hgr_address(y+4) + c*2] = (c + (c << 2) + (c << 4))  | delay_bit

    memory.tofile(path)

if __name__ == "__main__":
    make_gr(Path("screen_gr.bin"))

    make_gr_and_80col("gr_and_80col")

    # make_hgr(
    #     Path("screen_test_main_hgr.npy"),
    #     memory=np.fromfile("venise_dhgr_main.bin", dtype=np.byte),
    #     start_y=150
    # )

    make_hgr_patterns_for_comb(Path("comb_pattern_hgr.bin"))
    make_hgr_full_test(Path("full_test_hgr.bin"))
