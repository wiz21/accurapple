from enum import Enum
from typing import Union
import numpy as np
from utils import gr_address, hgr_address

def is_low(s):
    return not s

def is_rising(s0, s1):
    return (not s0) and s1

class ShiftAction(Enum):
    SHIFT = 1
    LOAD = 2
    NONE = 3

class Shifter:
    def __init__(self):
        self.register = 0

    def load(self, r):
        assert 0 <= r <= 255
        self.register = r

    def shift(self):
        self.register = (self.register >> 1) + ((self.register & 1) << 7)

    def trigger(self, clock: bool, action: bool, data: int) -> ShiftAction:
        """
        action: False == LOAD, True == SHIFT.
        """
        if clock:
            if action:
                self.shift()
                return ShiftAction.SHIFT
            else:
                self.load(data)
                return ShiftAction.LOAD
        else:
            return ShiftAction.NONE

    @property
    def d(self):
        return self.register & 1


class FlipFlop:
    def __init__(self, d:Union[bool,int] = False):
        self.d = bool(d)

    def set(self, clk, d):
        if clk:
            self.d = bool(d)

    @property
    def d_p(self):
        return not self.d


class SoftSwitchMask(Enum):
    SOFT_SWITCH_BIT_POS_TEXT = 3
    SOFT_SWITCH_BIT_POS_COL80 = 4
    SOFT_SWITCH_BIT_POS_HIRES = 5
    SOFT_SWITCH_BIT_POS_ALT_CHARSET = 6

class SoftSwitches:
    def __init__(self, text:bool, hires:bool):
        self.text = text
        self.hires = hires
        self.col80 = False
        self.alt_charset = 0
        self.flash = 0

        if self.hires:
            self._ram_scan_func = hgr_address
            self._ram_base = 0x2000
        else:
            self._ram_scan_func = gr_address
            self._ram_base = 0x400

    def set_hires(self):
        self.text = False
        self.hires = True
        self.col80 = False
        self._ram_scan_func = hgr_address
        self._ram_base = 0x2000

    @property
    def segb(self):
        return self.hires == False

    @property
    def gr(self):
        return self.text == False

    def ram_scanner(self,scanline,x):
        """RAM address for the current `scanline` and `x` (horizontal
        position, measured in bytes).
        """

        return self._ram_base + self._ram_scan_func(scanline) + x

    def as_byte(self):
        return (int(self.text) << SoftSwitchMask.SOFT_SWITCH_BIT_POS_TEXT.value) \
            + (int(self.col80) << SoftSwitchMask.SOFT_SWITCH_BIT_POS_COL80.value) \
                + (int(self.hires) << SoftSwitchMask.SOFT_SWITCH_BIT_POS_HIRES.value) \
                    + (int(self.alt_charset) << SoftSwitchMask.SOFT_SWITCH_BIT_POS_ALT_CHARSET.value)


def load_video_rom():
    with open("../data/Apple IIe Video - Unenhanced - 342-0133-A - 2732.bin","rb") as fin:

        #ROM = fin.read()
        ROM = np.frombuffer(fin.read(), np.uint8).copy()

        for b in range(len(ROM)):
            ROM[b] = ~ROM[b]
    return ROM


def rom_address( switches: SoftSwitches, horizontal_count: int, vertical_count: int, byte: int):
    vid05 = byte & 0b0011_1111
    vid6 = (byte & 0b0100_0000) >> 6
    vid7 = (byte & 0b1000_0000) >> 7

    va = vertical_count & 1
    vb = (vertical_count & 2) >> 1
    vc = (vertical_count & 4) >> 2
    h0 = horizontal_count & 1

    ra9 = vid6 & (vid7 | int(switches.alt_charset) | int(switches.gr))
    ra10 = vid7 | ( int(not switches.gr) & vid6 & int(switches.flash) & (1-switches.alt_charset))
    ra11 = int(switches.gr)

    # "select" component
    if not switches.gr:
        sega = va
        segb = vb
    else:
        sega = h0
        segb = int(not switches.hires)
    segc = vc
    segc = int(segc)

    addr = sega + (segb << 1) + (segc << 2)
    addr += vid05 << 3
    addr += (ra9 << 9) + (ra10 << 10) + (ra11 << 11)
    return addr


def test_choplifter():
    RAM = np.zeros( (65536,), dtype=np.uint8)
    with open("choplifter.hgr","rb") as fin:
        gr = np.frombuffer(fin.read(), np.uint8)
        RAM[0x2000:0x6000] = gr

    soft_switches_lines = []
    for i in range(192):
        soft_switches_lines.append( [SoftSwitches(text=False, hires=True)]*41 )

    return RAM, soft_switches_lines


def test_colour_screen_of_death():
    RAM = np.zeros( (65536,), dtype=np.uint8)

    with open("screen.hgr","rb") as fin:
        gr = np.frombuffer(fin.read(), np.uint8)
        RAM[0x2000:0x4000] = gr

    with open("screen.gr","rb") as fin:
        gr = np.frombuffer(fin.read(), np.uint8)
        RAM[0x400:0x800] = gr

    soft_switches_lines = []
    for i in range(192):
        if i < 64:
            soft_switches_lines.append( [SoftSwitches(text=False, hires=True)] * 4
                                       + [SoftSwitches(text=False, hires=False)] * 4
                                       + [SoftSwitches(text=False, hires=True)] * 7
                                       + [SoftSwitches(text=False, hires=False)] * 8
                                       + [SoftSwitches(text=False, hires=True)] * 6
                                        + [SoftSwitches(text=False, hires=False)] * 12  )
        elif i < 128:
            soft_switches_lines.append( [SoftSwitches(text=False, hires=True)] * 1
                                       + [SoftSwitches(text=False, hires=False)] * 4
                                       + [SoftSwitches(text=False, hires=True)] * 4
                                       + [SoftSwitches(text=False, hires=False)] * 8
                                       + [SoftSwitches(text=False, hires=True)] * 4
                                        + [SoftSwitches(text=False, hires=False)] * 12
                                        + [SoftSwitches(text=False, hires=True)] * 8)
        else:
            soft_switches_lines.append( [SoftSwitches(text=False, hires=True)] * 41)

    return RAM, soft_switches_lines


def sig_plot(title, a):
    if isinstance(a, np.ndarray):
        a = a.tolist()

    lines = [[],[]]

    for i in range(len(a)-1):
        if a[i:i+2] == [0,0]:
            lines[0].append("   ")
            lines[1].append("___")
        if a[i:i+2] == [1,1]:
            lines[0].append("___")
            lines[1].append("   ")
        if a[i:i+2] == [0,1]:
            lines[0].append("  _")
            lines[1].append("_! ")
        if a[i:i+2] == [1,0]:
            lines[0].append("_  ")
            lines[1].append(" !_")

    top = "".join(lines[0])
    bot = "".join(lines[1])
    print(top)
    print(title + bot[len(title):])
