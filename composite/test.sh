#!/bin/bash

if rustc scanner.rs --test ; then

   ./scanner  --nocapture test_video_scanner_png
   /usr/bin/python3 scanner.py
   feh ntsc.png -F -Z
fi
