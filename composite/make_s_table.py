def bool2str(x):
    if x:
        return "Y"
    else:
        return "."

for time in [1,2]:
    lines = []
    for i in range (8):
        LO_HI = (i & 1) > 0 # 0 == LORES, 1=HIRES
        COL80 = (i & 2) > 0
        AN3 = (i & 4) > 0

        if time == 1:
            # Graphics time
            TEXT_GR = False
            GR = AN3
            SEGB = not LO_HI
        else:
            # Text time
            TEXT_GR = True
            GR = False

        if TEXT_GR and not COL80:
            mode = "TXT40"
        elif TEXT_GR and COL80:
            mode = "TXT80"
        elif not TEXT_GR and not LO_HI and AN3:
            mode = "GR40"
        elif not TEXT_GR and not LO_HI and COL80 and not AN3:
            mode = "GR80"
        elif not TEXT_GR and LO_HI and not COL80 and AN3:
            mode = "HGR40"
        elif not TEXT_GR and LO_HI and not COL80 and not AN3:
            mode = "HGR40 dly inhib"
        elif not TEXT_GR and LO_HI and COL80 and not AN3:
            mode = "HGR80"
        elif not TEXT_GR and not LO_HI and not COL80 and not AN3:
            mode = "GR 7MHz"
        else:
            mode = "HGR40 ???"

        S1 = COL80 and not GR
        S2 = not GR
        S3 = SEGB

        VID7M_S1 = GR and SEGB
        VID7M_S2 = not GR and COL80

        if not GR:
            VID7M_S3 = "7M"
        else:
            VID7M_S3 = bool2str(False)

        lines.append(f"{mode:15}|   {bool2str(TEXT_GR):6} {bool2str(LO_HI):5} {bool2str(COL80):4} {bool2str(AN3):3}| {bool2str(GR):3}| {bool2str(S1):3} {bool2str(S2):3} {bool2str(S3):3}| {bool2str(VID7M_S1):3} {bool2str(VID7M_S2):3} {VID7M_S3}")

    # print("\n-----------------------------------------------------------------------")
    print("\n")
    if time == 1:
        print("Graphics Time")
    else:
        print("Text Time")

    print("---------------+------------------------+----+------------+------------")
    print("               | Soft switches          |    | LDPS       | VID7M")
    print("Mode           | TXT/GR HI/LO COL80 AN3 |GR+2| S1  S2  S3 | S1  S2  S3")
    print("---------------+------------------------+----+------------+------------")
    for line in sorted(lines):
        print(line)
