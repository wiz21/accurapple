from pathlib import Path
import numpy as np
from matplotlib import pyplot as plt
import sys
from utils import gr_address, hgr_address

SOFT_SWITCH_BIT_GR = 0
SOFT_SWITCH_BIT_MODEMIX = 1
SOFT_SWITCH_BIT_AN3 = 2
SOFT_SWITCH_BIT_POS_TEXT = 3
SOFT_SWITCH_BIT_POS_COL80 = 4
SOFT_SWITCH_BIT_POS_HIRES = 5
SOFT_SWITCH_BIT_POS_ALT_CHARSET = 6
#const SOFT_SWITCH_BIT_PAGE2 = 7;

def bits_to_sw_flags(sw):
    f = []
    f.append(["gr", "GR"][sw & (1 << SOFT_SWITCH_BIT_GR) > 0])
    f.append(["mix", "MIX"][sw & (1 << SOFT_SWITCH_BIT_MODEMIX) > 0])
    f.append(["an3", "AN3"][sw & (1 << SOFT_SWITCH_BIT_AN3) > 0])
    f.append(["txt", "TXT"][sw & (1 << SOFT_SWITCH_BIT_POS_TEXT) > 0])
    f.append(["40", "COL80"][sw & (1 << SOFT_SWITCH_BIT_POS_COL80) > 0])
    f.append(["lo", "HI"][sw & (1 << SOFT_SWITCH_BIT_POS_HIRES) > 0])
    f.append(["alt", "ALT"][sw & (1 << SOFT_SWITCH_BIT_POS_ALT_CHARSET) > 0])
    return " ".join(reversed(f))

class MemDump:
    def __init__(self, fname=Path("mem.bin")):
        mem = np.fromfile(str(fname), dtype=np.uint8)

        self.gr_page1 = mem[0x400:0x800]
        self.hgr_page1 = mem[0x2000:0x4000]
        self.gr_page2 = mem[0x800:0xC00]
        self.hgr_page2 = mem[0x4000:0x6000]

        aux_mem = mem[0x10000 + 0x1000:]
        self.aux_gr_page1 = aux_mem[0x400:0x800]
        self.aux_hgr_page1 = aux_mem[0x2000:0x4000]
        self.aux_gr_page2 = aux_mem[0x800:0xC00]
        self.aux_hgr_page2 = aux_mem[0x4000:0x6000]

def hex_dump(a):
    return " ".join([f"{x:02X}" for x in a])

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(
                    prog='ProgramName',
                    description='What the program does',
                    epilog='Text at the bottom of help')
    parser.add_argument('basepath')           # positional argument
    parser.add_argument('-p', '--period', default="0,-1", help="Period expressed in dots*2 so in [0,560*2+some padding]")
    args = parser.parse_args()


    base_path = Path(args.basepath)

    period = [int(x) for x in args.period.split(",")]

    mem = MemDump(base_path / "allmem.bin")
    #y=68 # gr
    # y=185
    #y = 170 # hgr
    #y = 100 #DHGR on all_modes
    #y = 64 #DHGR on all_modes
    #y = 186 # hgr
    #y = 170 # =192- (4*5+2) HGR non delayed
    # y = 32
    #y = 5*8+4 GR
    #y = 63 # Test GR on DHGR scenario
    # y = 63-16 # Test GR on DHGR scenario
    #y = 64 # Test 80COL/HGR on DHGR scenario

    a = np.fromfile( base_path / "gfx_debug.bin", dtype=np.int32)
    y = a[len(a)-1]
    print(f"Y of captured line = {y}")

    a = np.fromfile( base_path / "gfx_debug.bin", dtype=np.float32)
    a = a[0:-1]
    pixels_per_line = 573*2+54
    if period[1] == -1:
        period[1] = pixels_per_line
    assert a.size % pixels_per_line == 0
    lines = a.size // pixels_per_line
    print(f"Available debug lines: {lines}")
    a = a.reshape((lines, pixels_per_line))

    addr = gr_address(y)
    print(y)
    print(addr)
    print("Apple's RAM:")
    print(f"GR      {y}:",hex_dump(mem.gr_page1[addr: addr+40]))
    print(f"AUX GR  {y}:", hex_dump(mem.aux_gr_page1[addr : addr + 40]))
    addr = hgr_address(y)
    print(f"HGR     {y}:", hex_dump(mem.hgr_page1[addr : addr + 40]))
    print(f"AUX HGR {y}:", hex_dump(mem.aux_hgr_page1[addr : addr + 40]))
    print()


    mem = np.fromfile(str(base_path/"scanner.bin"), dtype=np.uint8)
    soft_switches = mem[0:12480].reshape((192, 65))
    scanned_ram = mem[12480:].reshape( (192,80) )
    print("Rust scan:")
    print(f"SSW:    {y}:",
          " ".join([f"{x:02X}" for x in soft_switches[y, :25]]),
          "|",
          " ".join([f"{x:02X}" for x in soft_switches[y, 25:]]))
    print(f"MBD:    {y}:", " ".join([f"{x:02X}" for x in scanned_ram[y, 0::2]]))
    print(f"AUX:    {y}:", " ".join([f"{x:02X}" for x in scanned_ram[y, 1::2]]))

    for v in set(soft_switches[y, :].tolist()):
        print(hex(v).upper(), bits_to_sw_flags(v))
    #exit()

    print(f"Shader's line ={a[0][0]}")

    # plt.imshow(a)
    # plt.show()

    vars = dict()
    for ndx, name in enumerate(
        [
            "!14M",
            "!7M",
            "!RAS'",
            "!PH0 rising",
            "!AX",
            "!Q3",
            "PHASE0",
            "H0",
            "CLR_REF",
            "RASRISE1",
            "7M",
            "LOAD_SOFT_SWITCHES",
            "LDPS_HGR_CHECK",
            "!skip",
            "VID7",
            "SEGB",

            "LDPS_CHECK",
            "LDPS_S1",
            "LDPS_S2",
            "LDPS_S3",
            "LDPS_S4",
            "LDPS_S5",
            "LDPS_S6",
            "VID7M_S1",
            "VID7M_S2",
            "VID7M_S3",
            "VID7M_S4",
            "VID7M_S5",
            "VID7M_T123",
            "GR/Text time",
            "GR2p",
            "VID7M",
            "LSS166-OUT",
            "LDPS",
            "SoftSwitches_b",
            "BYTE_OFS_i",
            "RAM_BYTE_h",

            "ROM_BYTE_h",
            "ROM_ADDR_b",
            "SH/LD",
            "!zero",
            "SHIFT_REG_b",
            "DOT_SPEED",
            "MEM_SCAN_SPEED",
            "SHIFT_SPD_CHCK",
            "MEM_SPD_CHCK",
            "SEG_ABC_GR1",
            "SEG_ABC_GR2",
            "SEG_ABC_SEGB",
            "SEG_ABC_SEGA1",
            "SEG_ABC_SEGB1",
            "DelayHGR",
            "Is HGR?",
            "Dly progrs?",
            "!LDPS check t?",
            "SEGA"
        ]
    ):
        if "!" not in name:
            vars[name] = a[ndx, :]

    print("Plotting")


    fig = plt.figure(figsize=(8, 8), layout="constrained")
    # fig, (ax1, ax2) = plt.subplots(2, 1, gridspec_kw = {'wspace':0, 'hspace':0})
    # outer_grid = fig.add_gridspec(2, 1, wspace=0.0, hspace=0.0, height_ratios=[10, 1])
    # ax1, ax2 = outer_grid.subplots()

    ax1 = fig.subplots(1,1)
    ax1.set_title(f"line y={y}")

    for ck in range( (period[0]//28)*28, (period[1]//28)*28):
        if ck % 28 == 0:
            ax1.axvline(ck, color="black", linewidth="1")
        elif ck % 14 == 0:
            ax1.axvline(ck, color="black", linewidth="0.5")

    for i, nv in enumerate(vars.items()):
        name, values = nv

        # if name == "SH/LD":
        #     print(values.tolist())
        #values=values[0:560]
        base = (len(vars) - i)*4


        if name == "PHASE0":
            for i in range(20):
                pos = i*14 + 3
                if period[1] >= pos >= period[0]:
                    ax1.text(
                        x=pos,
                        y=base,
                        s=f"{['MBD','AUX'][int(values[i*14])]}",  # [::-1],
                        fontsize="x-small"
                    )
                    ax1.text(
                        x=pos,
                        y=base - 1,
                        s=f"{['EVEN','ODD'][((i)//2)%2]}",  # [::-1],
                        fontsize="x-small"
                    )

        name:str
        if name.endswith("_b"):
            alter = 0
            for ck in range(0, len(values)):
                if period[1] >= ck >= period[0]:
                    if ck == 0 or (values[ck] != values[ck-1] and ("ROM_ADDR" not in name or values[ck] > 0)):
                        if "ROM_ADDR" not in name:
                            ax1.text(
                                x=ck,
                                y=base, #+ [0.5, -1.5][alter],  # - 15 * 4 + [0.5, -1.5][i % 2],
                                s=f"{int(values[ck]):08b}",  # [::-1],
                                fontsize="x-small", rotation=45
                            )
                        else:
                            b = int(values[ck])
                            ax1.text(
                                x=ck,
                                y=base, #+ [0.5, -1.5][alter],  # - 15 * 4 + [0.5, -1.5][i % 2],
                                s=f"{(b >> 9) & 0b111:03b}-{(b >> 3) & 0b111111:06b}-{b & 0b111:03b}",  # [::-1],
                                fontsize="x-small", rotation=45
                            )

                        alter = 1 - alter
                        ax1.axvline(ck, color="grey", linewidth="0.5")
        elif name.endswith("_i"):
            alter = 0
            for ck in range(0, len(values)):
                if period[1] >= ck >= period[0]:
                    if ck == 0 or values[ck] != values[ck - 1]:
                        ax1.text(
                            x=ck,
                            y=base,  # + [0.5, -1.5][alter],  # - 15 * 4 + [0.5, -1.5][i % 2],
                            s=f"{int(values[ck])}",  # [::-1],
                            fontsize="x-small",
                        )
                        alter = 1 - alter
                        ax1.axvline(ck, color="blue", linewidth="0.5")

                        # if name == "BYTE_OFS_i":
                        #     mem_ofs = int(values[ck])
                        #     ax1.text(
                        #         x=ck,
                        #         y=base-1.0,  # + [0.5, -1.5][alter],  # - 15 * 4 + [0.5, -1.5][i % 2],
                        #         s=f"MBD:{scanned_ram[y,mem_ofs*2+0]:02X}, AUX:{scanned_ram[y,mem_ofs*2+1]:02X}",  # [::-1],
                        #         fontsize="x-small",
                        #     )


        elif name.endswith("_h"):
            alter = 0
            for ck in range(0, len(values)):
                if period[1] >= ck >= period[0]:
                    if ck == 0 or values[ck] != values[ck - 1]:
                        ax1.text(
                            x=ck,
                            y=base,  # + [0.5, -1.5][alter],  # - 15 * 4 + [0.5, -1.5][i % 2],
                            s=f"{int(values[ck]):X}",  # [::-1],
                            fontsize="x-small",
                            rotation=45,
                        )
                        alter = 1 - alter
                        ax1.axvline(ck, color="blue", linewidth="0.5")
        else:
            #ax1.step(np.arange(len(values)), base + np.array(values), where="post", label=name)
            ax1.step(np.arange(len(values)), base + np.array(values), where="mid", label=name)
            #ax1.scatter(np.arange(len(values)), base + np.array(values), label=name)
            ax1.axhline(base, color="grey", linewidth="0.5")


    # ax1.set_title("Apple 2 Timing Signals Generation")
    ax1.set_yticks(
        [(len(vars) - i) * 4 for i, name in enumerate(vars.keys())] + [0.3, 1.3, 2.3],
        [name.replace("_b","") for i, name in enumerate(vars.keys())] + ["scnd SW", "scnd AUX", "scnd MBD"],

    )

    TICK_START = 0
    ax1.set_xlim(period[0], period[1])
    ax1.set_xticklabels(np.arange(period[0], period[1], 28))
    ax1.set_xticks(np.arange(period[0], period[1], 28))

    base = TICK_START // 28
    for x in range(20):
        if period[1] >= x*28 >= period[0]:
            ax1.text(
                x=x * 28,
                y=2.3,  # + [0.5, -1.5][alter],  # - 15 * 4 + [0.5, -1.5][i % 2],
                s=f"{scanned_ram[y,base*2+x*2+0]:02X}",  # [::-1],
                fontsize="x-small",
            )
            ax1.text(
                x=x * 28 + 14,
                y=1.3,  # + [0.5, -1.5][alter],  # - 15 * 4 + [0.5, -1.5][i % 2],
                s=f"{scanned_ram[y,base*2+x*2+1]:02X}",  # [::-1],
                fontsize="x-small",
            )
            ax1.text(
                x=x * 28,
                y=0.6,  # + [0.5, -1.5][alter],  # - 15 * 4 + [0.5, -1.5][i % 2],
                s=f"{soft_switches[y, base+x]:02X}",  # [::-1],
                fontsize="x-small",
            )
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    ax1.text(
        0.8,
        0.95,
        """0: GR;
1: MIX
2: AN3
3: TEXT
4 0x10: COL80
5 0x20: HIRES
6 0x40: ALT
7 0x80: PAGE2
PH0: 0=RAM,1=AUX""",
        transform=ax1.transAxes,
        fontsize=14,
        verticalalignment="top",
        bbox=props,
    )

    # plt.imshow(a)
    ax1.format_coord = lambda x, y: 'x={:g}, y={:g}'.format(int(x), int(y))
    figManager = plt.get_current_fig_manager()
    #figManager.window.showMaximized() # For Tcl/Tk
    #figManager.window.state("zoomed") # For Qt
    plt.show()
