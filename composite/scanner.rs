// eglot
use std::fs;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::{Path, PathBuf};

struct HorizontalScanner {
    counter: usize,
    clock_number: usize,
}

impl HorizontalScanner {
    fn new() -> HorizontalScanner {
        HorizontalScanner {
            counter: 0b1_011_000,
            clock_number: 0
        }
    }

    fn tick(&mut self) -> bool {
        self.clock_number += 1;
        // Returns true if the vertical scanner must be updated
        if self.hpe_prime() == 0 {
            self.counter = 0b1_000_000
        } else {
            self.counter = (self.counter + 1) & 0b0111_1111
        }

        if self.counter == 0b1_011_000 {
            self.clock_number = 0;
        }

        return self.counter == 0;
    }

    fn counter(&self) -> usize {
        self.counter
    }

    fn clock_number(&self) -> usize {
        return self.clock_number;
    }

    fn hpe_prime(&self) -> u8 {
        if self.counter & 0b1_000_000 > 0 { 1 } else { 0 }
    }

    fn hbl(&self) -> u8 {
        if self.counter >= 0b1_011_000 && self.counter <= 0b1_111_111 {0} else {1}
    }

    fn burst(&self) -> u8 {
        if self.counter >= 0b1_001_100 && self.counter < 0b1_010_000 {1} else {0}
    }

    fn sync(&self) -> u8 {
        if self.counter >= 0b1_001_000 && self.counter <= 0b1_001_011 {1} else {0}
    }

    fn h543(&self) -> usize {
        ((self.counter & 0b111000) >> 3) as usize
    }

    fn h012(&self) -> usize {
        (self.counter & 0b111) as usize
    }

    fn h0(&self) -> usize {
        (self.counter & 0b1) as usize
    }

    fn status_str(&self) -> String {
        format!("{:09b} CK_NO:{:>3} BURST:{} HBL:{} SYNC:{}", self.counter(), self.clock_number(), self.burst(), self.hbl(), self.sync())
    }
}

struct VerticalScanner {
    counter: usize,
    line: usize
}

impl VerticalScanner {
    fn new() -> VerticalScanner {
        VerticalScanner { counter : 0b100_000_000, line : 0 }
    }

    fn counter(&self) -> usize {
        return self.counter;
    }

    fn display_line(&self) -> usize {
        return self.line;
    }

    fn tick(&mut self) {
        self.counter = (self.counter + 1) & 0b1_1111_1111;
        self.line += 1;
        if self.counter == 0 {
            // overflow
            self.counter = 0b011_111_010;
        }

        if self.counter == 0b100_000_000 {
            self.line = 0;
        }
    }

    fn gr(&self) -> u8 {
        if (self.counter >= 0b100_000_000 && self.counter < 0b110_100_000)
            || (self.counter >= 0b111_000_000 && self.counter < 0b111_100_000) {
            return 1;
        } else {
            return 0;
        }
    }

    fn sync(&self) -> u8 {
        if self.counter == 0b111_100_000 {1} else {0}
    }

    fn vbl(&self) -> u8 {
        if self.counter >= 0b111_000_000 { 1 } else { 0 }
    }

    fn text(&self) -> u8 {
        if self.counter >= 0b110_100_000 && self.counter < 0b111_000_000 { 1 } else { 0 }
    }

    fn v4343(&self) -> usize {
        let v43 = ((self.counter & 0b0_11_000_000) >> 6) as usize;
        return v43 | (v43 << 2);
    }

    fn v012(&self) -> usize {
        ((self.counter & 0b111_000) >> 3) as usize
    }

    fn vabc(&self) -> usize {
        (self.counter & 0b111) as usize
    }

    fn vc(&self) -> usize {
        ((self.counter & 0b100) >> 2) as usize
    }

    fn status_str(&self) -> String {
        format!("{:09b} line:{:>3} GR:{} TEXT:{} VBL:{} SYNC:{}", self.counter(), self.display_line(), self.gr(), self.text(), self.vbl(), self.sync())
    }
}

struct VideoScanner {
    horiz_scanner: HorizontalScanner,
    verti_scanner: VerticalScanner,
}

impl VideoScanner {
    fn new() -> VideoScanner {
        VideoScanner {
            horiz_scanner: HorizontalScanner::new(),
            verti_scanner: VerticalScanner::new(),
        }
    }

    fn tick(&mut self) {
        if self.horiz_scanner.tick() {
            self.verti_scanner.tick()
        }
    }

    fn sum_a6543(&self) -> usize {
        (0b1101 + self.horiz_scanner.h543() + self.verti_scanner.v4343()) & 0b1111
    }

    fn mpu_address(&self, text_switch: bool, hires_switch: bool, page2: bool, store80: bool) -> u16 {
        // Tabte 5.1  MPU/Scanner Equivalent Address Bits.

        let p = page2 & !store80;
        let pp = ((p as usize) << 1) + (!p as usize);
        let text_lores = text_switch || !hires_switch;
        if text_lores {
            (self.horiz_scanner.h012() + (self.sum_a6543() << 3) + (self.verti_scanner.v012() << 7)
             + (pp << 10)) as u16
        } else if hires_switch {
            (self.horiz_scanner.h012() + (self.sum_a6543() << 3) + (self.verti_scanner.v012() << 7)
             + (self.verti_scanner.vabc() << 10) + ((pp << 13) as usize)) as u16
        } else {
            panic!("unexpected");
        }
    }
}


#[test]
fn test_vertical_scanner() {
    let mut vs = VerticalScanner::new();
    while vs.display_line() < 10 {
        println!("{}", vs.status_str());
        vs.tick();
    }
    while vs.display_line() < 152 {
        vs.tick();
    }
    while vs.display_line() < 261 {
        println!("{}", vs.status_str());
        vs.tick();
    }

    for i in 0..3 {
        println!("{}", vs.status_str());
        vs.tick();
    }
}


#[test]
fn test_horizontal_scanner() {
    let mut hs = HorizontalScanner::new();
    for i in 0..70 {
        println!("{}", hs.status_str());
        hs.tick();
    }
}


#[test]
fn test_video_scanner() {
    let mut vidscan = VideoScanner::new();
    for i in 0..65*262 {
        vidscan.tick();
    }

    println!("{}\n{}", vidscan.horiz_scanner.status_str(), vidscan.verti_scanner.status_str());
}

#[test]
fn test_video_scanner_first_line() {
    let mut vidscan = VideoScanner::new();

    let text_switch = false;
    let hires_switch = true;
    let page2 = false;
    let store80 = false;

    for line in 0..200 {
        print!("displine:{:>3} Addr:{:04X} VC:{:09b}", vidscan.verti_scanner.display_line(),
            vidscan.mpu_address(text_switch, hires_switch, page2, store80),
            vidscan.verti_scanner.counter());
        print!(" {:03b} {:04b} v012:{:b}", vidscan.horiz_scanner.h543(),  vidscan.verti_scanner.v4343(),
            vidscan.verti_scanner.v012());
        for i in 0..65 {
            vidscan.tick();
        }
        println!("");
    }

    println!("\n{}\n{}", vidscan.horiz_scanner.status_str(), vidscan.verti_scanner.status_str());
}

fn save_screenshot(path: &Path, screen: &Vec<u8>) {
    // let file = File::create(path).unwrap();
    // let ref mut w = BufWriter::new(file);
    // let width = 280;

    // let mut encoder = png::Encoder::new(w, width as u32, 192);
    // encoder.set_color(png::ColorType::Rgba);
    // encoder.set_depth(png::BitDepth::Eight);
    // let mut writer = encoder.write_header().unwrap();

    // writer
    //     .write_image_data(&screen[..])
    //     .unwrap(); // Save

    fs::write(path, screen).unwrap();
}

fn segabc(gr_plus_1: bool, vscan: &VideoScanner, hires: bool) -> usize {
    if gr_plus_1 {
        return vscan.horiz_scanner.h0() + ((!hires as usize) << 1) + (vscan.verti_scanner.vc() << 2);
    } else {
        return vscan.verti_scanner.vabc();
    }
}

#[test]
fn test_video_scanner_png() {
    let mut vidscan = VideoScanner::new();

    let text_switch = false;
    let hires_switch = false;
    let page2 = false;
    let store80 = false;

    let mut charset_rom:Vec<u8> = vec![];
    charset_rom.extend_from_slice(include_bytes!("../data/3410265A.bin"));

    //let mut ram:Vec<u8> = vec![]; //0 as u8; 0x2000];
    //ram.extend_from_slice(include_bytes!("miner.ram"));
//    ram.extend_from_slice(include_bytes!("madef.ram"));

    let mut ram:Vec<u8> = vec![0 as u8; 0x400];
    ram.extend_from_slice(include_bytes!("screen.gr"));

    println!("RAM ${:04X} bytes", ram.len());
    let mut image: Vec<u8> = vec![0 as u8; 40*7*2*192];
    for line in 0..192 {
        for i in 0..40 {
            let addr = vidscan.mpu_address(text_switch, hires_switch, page2, store80);
            if i == 1 {
                print!("{:04X} ",addr);
            }

            let mut vid07 = ram[addr as usize] as usize;
            let gr_2 = true;
            let rom_a = (vid07 << 3) + segabc(true, &vidscan, hires_switch) + ((gr_2 as usize) << 11);

            //let mut ram_byte = ram[addr as usize];
            let mut ram_byte: u8 = charset_rom[rom_a];

            // 7 MHz
            // for sub in 0..7 {
            //     image[line*40*14 + i*14 + 2*sub] = (!ram_byte & 1) * 0xFF;
            //     image[line*40*14 + i*14 + 2*sub+1] = (!ram_byte & 1) * 0xFF;
            //     ram_byte = ram_byte >> 1;
            // }

            // 14 Mhz
            for sub in 0..14 {
                image[line*40*14 + i*14 + sub] = (!ram_byte & 1) * 0xFF;
                ram_byte = ram_byte.rotate_right(1);
            }

            vidscan.tick();
        }
        for i in 0..25 {
            vidscan.tick();
        }
    }

    println!("\n{}\n{}", vidscan.horiz_scanner.status_str(), vidscan.verti_scanner.status_str());
    save_screenshot(Path::new("test.bin"), &image);
}



fn main() {
    println!("Hello, world!");
}
