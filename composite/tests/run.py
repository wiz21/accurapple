import sys
import shutil
import subprocess
from pathlib import Path

ACCURAPPLE_DIR=Path(__file__).parent.parent.parent

if sys.platform == "win32":
    ACME=r"c:/users/stephanec/opt/acme"
else:
    ACME="acme"

APPLE_COMMANDER = ["java", "-jar",  "AppleCommander-1.3.5.13-ac.jar"]
ACCURAPPLE_EXE=ACCURAPPLE_DIR / "target" / "release" / "accurapple"

sys.path.append(str(ACCURAPPLE_DIR / "utils"))
import make_convtab
import print_date
from dsk2nic import main as run_dsk2nic

def assemble( fname):
    cmd = [ACME, "-I", str(ACCURAPPLE_DIR / "utils"), fname]
    print(" ".join(cmd))
    ret = subprocess.run(cmd)
    assert ret.returncode == 0

def apple_commander(dos_name, fname):
    with open(fname, "rb") as pin:
        input = pin.read()

    cmd = APPLE_COMMANDER + ["-p", "NEW.DSK", dos_name, "BIN", "0x6000", fname]
    subprocess.run(cmd, input=input)

def run_accurapple(args):
    cmd = [ ACCURAPPLE_EXE ] + args
    subprocess.run(cmd)

def cargo_build():
    cmd = ["cargo", "build", "--release"]
    subprocess.run(cmd)

if __name__ == "__main__":
    assemble("txt_40_80/txt4080.s")
    # assemble("txt_40_80/allmodes.s")
    assemble("logic_ana/lagr/la_gr.s")
    assemble("logic_ana/lagr/bad_burst.s")
    assemble("logic_ana/vidscan.s")


    #assemble("txt_40_80/txt4080.s")
    #assemble("txt_40_80/allmodes.s")
    assemble("txt_40_80/gr_dgr.s")
    assemble("txt_40_80/gr_txt.s")
    assemble("txt_40_80/hgr_txt80.s")
    assemble("logic_ana/la_gr_txt.s")

    Path("NEW.DSK").unlink(missing_ok=True)
    shutil.copy("BLANK_PRODOS2.DSK", "NEW.DSK")

    apple_commander("TXT40.80.SWITCH", "TXT4080.bin")
    apple_commander("MODE.SWITCHES", "LAGR.bin")
    apple_commander("COLOUR.BURST", "BAD_BURST.bin")
    apple_commander("VIDEO.SCANNER", "VID_SCAN.bin")

    apple_commander("STRANGE.MODE", "DHGRDGR.bin")
    apple_commander("GRTXT", "GRTXT.bin")
    apple_commander("HGRTXT80","HGRTXT80.bin")
    apple_commander("LAGRTXT", "LAGRTXT.bin")

    run_dsk2nic(["python", "dsk2nic.py", "--source=NEW.DSK", "--target=gfxtest.nic"])

    cargo_build()
    run_accurapple(["--floppy1", "NEW.DSK",
                    "--charset", ACCURAPPLE_DIR / "data" / "Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin",
                    "--script", "TURBO_START"])
