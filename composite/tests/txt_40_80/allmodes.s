!to "ALLMODES.bin", plain
!convtab "apple2c.ct"

* =   $6000

!source "equs.s"



    ;lda #$11
    ;jsr COUT
	LDA #3
	OUTPORT = $FE95
	JSR OUTPORT
    +print_one_text_at " ", 0, 0


    ;STA TEXTOFF
    ;STA LORES
    ;STA COL80ON
    ;STA AN3OFF

    STA STORE80ON		; 80STORE set
    LDA HIRES
    BIT PAGE1
    LDA #160
    LDY #$04
    LDX #4
    JSR page_fill
    JSR clear_half_gr

    BIT PAGE2
    LDA #160
    LDY #$04
    LDX #4
    JSR page_fill
    JSR clear_half_gr

    BIT PAGE1
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

    BIT PAGE2
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

	show_text = $F0
	LDA #0
	STA show_text

    STA STORE80OFF
    JSR plot_gr
    JSR plot_dgr
    JSR plot_hgr
    JSR plot_dhgr
    JSR plot_txt40
    JSR plot_txt80
    +print_one_text_at "You see GR, DGR, HGR, DHGR, 40, 80", 1, 18
    +print_one_text_at "HGR:pink,blue,orange,green,white", 1, 19

	;PR#3: set 80 columns and activate firmware (for COUT to work)


    STA TEXTOFF
    STA HIRES
    STA COL80OFF
    STA AN3ON
    LDA PAGE1

	JSR enable_vbl


	!align 255, 0
	JMP skipper

fine_wait:
	N = CYCLES_PER_FRAME - 1 - 6 - 3 - 1 - 2
	LDA # (N - 34) >> 8	; 2
	LDX # (N - 34) & 255	; 2
	JMP delay_256a_x_33_clocks
skipper:

	;; The general idea is this:
	;; Detectct the start of the drawing area. When done
	;; a few cycles have passed so we're inside the drawing area.
	;; Now, we wait a little less than the duration of a full
	;; frame so that we end up a bit "higher" on the screen, we do
	;; that with a big step then with 1 cycle steps.
	;; We continue until we're back in the VBL area, just above the
	;; drawing area. Doing it like that allows us to be on the
	;; very cycle where the frame draw start.

	JSR switch_display
	;; Returning from JSR is 6 cycles
	N2 = CYCLES_PER_FRAME - 6 - 4
	LDA # (N2 - 34) >> 8	; 2
	LDX # (N2 - 34) & 255	; 2
	JSR delay_256a_x_33_clocks		; 3

	;; at this point we're in the drawing zone

	!for X,1,16 {
        ;; If no branch, This will have the total effect of waiting
        ;; CYCLES_PER_FRAME minus one cycle.
        lda     VERTBLANK       ; 4
        ;; A=$80=Drawing; A=0=VBL
        bpl     zed		; 2 or 3 branch if in the VBL part
    	JSR fine_wait
	}
zed:


freeze:
    ; GR
    STA TEXTOFF ; 4C
    STA LORES   ; 4
    STA COL80OFF
    STA AN3ON
    LDA PAGE1

    +short_wait 65 - 20 + 65*15

    ; DGR
    STA TEXTOFF ; 4C
    STA LORES   ; 4
    STA COL80ON
    STA AN3OFF
    LDA PAGE1

    +short_wait 65 - 20 + 65*15

    ; HGR
    STA TEXTOFF ; 4C
    STA HIRES   ; 4
    STA COL80OFF
    STA AN3ON
    LDA PAGE1

    +short_wait 65 - 20 + 65*15

    ; DHGR
    STA TEXTOFF ; 4C
    STA HIRES   ; 4
    STA COL80ON
    STA AN3OFF
    LDA PAGE1

    +short_wait 65 - 20 + 65*15

	KBD_CHECK_CYCLES = 21
	LDA $C000		; 4c
	BIT $C010		; 4c
	AND #$80		; 2c 128 == key pressed
	CLC			; 2c
	ADC show_text		; 3c
	STA show_text		; 4c
	BEQ no_key_pressed	; 2 or 3 => total = 12 or 13

    ; TXT40
    STA TEXTON ; 4C
    STA LORES   ; 4
    STA COL80OFF
    STA AN3ON
    LDA PAGE1

    +short_wait 65 - 20 + 65*15	; 16 lines are 1040 cycles

    ; TXT80
    STA TEXTON ; 4C
    STA LORES   ; 4
    STA COL80ON
    STA AN3OFF
    LDA PAGE1

    +short_wait 65 - 20 + 65*15

	+short_wait 20280 - 3 - 65*(16+16+16+16+16+16) - (KBD_CHECK_CYCLES-1)
    JMP freeze

no_key_pressed:
    +short_wait 20280 - 3 - 65*(16+16+16+16) - (KBD_CHECK_CYCLES+2-1)
    JMP freeze


clear_half_gr !zone {
    .POINTER = $FE
    LDA #0
    STA .line

.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA txt_addr,X
    STA .POINTER
    INX
    LDA txt_addr,X
    STA .POINTER+1

    LDA #0
    STA .counter
.loop:
    LDY .counter
    LDA #0
    STA (.POINTER),Y
    INC .counter
    LDA .counter
    CMP #40
    BMI .loop

    INC .line
    LDA .line
    CMP #10
    BMI .loop_line
    RTS

.line !byte 0
.counter !byte 0
}

plot_gr !zone {
    LDA #0
    STA .line
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA txt_addr,x
    STA .smc1 + 1
    INX
    LDA txt_addr,x
    STA .smc1 + 2

    LDA #0
    STA .counter
.loop_fill_line:
    LDA .counter
    CLC
    ADC #2
    ROR
    AND #15
    STA .masked_counter
    CLC
    ROL
    ROL
    ROL
    ROL
    ORA .masked_counter

    LDX .counter
.smc1:
    STA $400,X

    INC .counter
    LDA .counter
    CMP #32
    BNE .loop_fill_line

    INC .line
    LDA .line
    CMP #1
    BNE .loop_line
    RTS

.counter !byte 0
.line !byte 0
.masked_counter !byte 0
}

;; ---------------------------------------------------------------------------

plot_dgr !zone {
    POINTER = $FE
    STY STORE80ON		; 80STORE set
    LDA LORES

    LDA #2
    STA .line
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA txt_addr,x
    STA POINTER
    INX
    LDA txt_addr,x
    STA POINTER + 1

    LDA #0
    STA .counter
    LDA #1
    STA .color_counter
.loop_fill_line:
    LDA .color_counter
    AND #15
    STA .color
    ASL
    ASL
    ASL
    ASL
    ORA .color
    +RROR
    STA .color
    INC .color_counter

    LDX PAGE2           ; Selects AUX
    LDY .counter
    STA (POINTER),Y

    LDX PAGE1           ; Selects RAM
    PHA
    ROL
    PLA
    ROL
    STA (POINTER),Y

    INC .counter
    LDX PAGE2           ; Selects RAM
    LDY .counter
    LDA .color
    STA (POINTER),Y
    LDX PAGE1           ; Selects AUX
    LDA #255
    STA (POINTER),Y

    INC .counter


    LDA .counter
    CMP #32
    BMI .loop_fill_line

    INC .line
    LDA .line
    CMP #3
    BNE .loop_line

    STX STORE80OFF
    LDY PAGE1
    RTS

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}


plot_hgr !zone {
    POINTER = $FE
    LDA #32
    STA .line
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA hgr_addr,x
    STA POINTER
    INX
    LDA hgr_addr,x
    STA POINTER + 1

    LDA #0
    STA .counter
    LDA #1
    STA .color_counter
.loop_fill_line:
    LDA .color_counter
    INC .color_counter
    AND #7
    TAX

    LDA .color_pos, X
    TAY
    LDA .colors, X
    INC .counter
    STA (POINTER),Y


    LDA .rotated_colors, X
    INY
    INC .counter
    STA (POINTER),Y

    LDA .counter
    CMP #16
    BMI .loop_fill_line

    INC .line
    LDA .line
    CMP #40
    BNE .loop_line

    RTS

// Purple, green, white, black, blue, red, white
.color_pos !byte 0,4,22,0, 0, 10, 12+4, 28
.colors !byte %00000000, %01010101, %00101010, %01111111
        !byte %10000000, %11010101, %10101010, %11111111
.rotated_colors:
        !byte %00000000, %00101010, %01010101, %01111111
        !byte %10000000, %10101010, %11010101, %11111111

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}



plot_dhgr !zone {
    POINTER = $FE
    STA STORE80ON		; 80STORE set
    LDA HIRES

    LDA #48
    STA .line
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA hgr_addr,x
    STA POINTER
    INX
    LDA hgr_addr,x
    STA POINTER + 1

    LDA #0
    STA .counter
    LDA #1
    STA .color_counter
.loop_fill_line:
    LDA .color_counter
    AND #15
    STA .color
    ASL
    ASL
    ASL
    ASL
    ORA .color
    +RROR
    STA .color
    INC .color_counter

    LDX PAGE2           ; Selects AUX
    LDY .counter
    STA (POINTER),Y
    +RROL
    LDX PAGE1           ; Selects RAM
    STA (POINTER),Y

    INC .counter

    LDX PAGE2           ; Selects RAM
    LDY .counter
    LDA .color
    +RROL
    +RROL
    STA (POINTER),Y
    LDX PAGE1           ; Selects AUX
    LDA #255
    STA (POINTER),Y

    INC .counter


    LDA .counter
    CMP #32
    BMI .loop_fill_line

    INC .line
    LDA .line
    CMP #48+8
    BNE .loop_line

    STX STORE80OFF
    LDY PAGE1
    RTS

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}



plot_txt40 !zone {
    .POINTER = $FC
    .POINTER2 = $FE
    LDX #8 * 2
    LDA txt_addr,x
    STA .POINTER
    INX
    LDA txt_addr,x
    STA .POINTER + 1

    LDX #9 * 2
    LDA txt_addr,x
    STA .POINTER2
    INX
    LDA txt_addr,x
    STA .POINTER2 + 1

    LDA #0
    STA .counter
    LDA .letter_A
    STA .color_counter
.loop_fill_line:
    LDA .color_counter
    INC .color_counter
    LDY .counter
    STA (.POINTER),Y

    LDA .letter_SPACE
    STA (.POINTER2),Y

    INC .counter
    LDA .counter
    CMP #26
    BMI .loop_fill_line

    LDA .letter_A
    STA .color_counter
.loop_fill_line2:
    LDA .color_counter
    INC .color_counter
    LDY .counter
    STA (.POINTER),Y

    LDA .letter_SPACE
    STA (.POINTER2),Y

    INC .counter
    LDA .counter
    CMP #40
    BMI .loop_fill_line2

    RTS

.letter_A !text "A"
.letter_SPACE !text " "
.counter !byte 0
.color_counter !byte 0
}


plot_txt80 !zone {
    .POINTER = $FC
    .POINTER2 = $FE
    STA STORE80ON		; 80STORE set
    LDA HIRES
    LDX #10 * 2
    LDA txt_addr,x
    STA .POINTER
    INX
    LDA txt_addr,x
    STA .POINTER + 1

    LDX #11 * 2
    LDA txt_addr,x
    STA .POINTER2
    INX
    LDA txt_addr,x
    STA .POINTER2 + 1

    LDA #0
    STA .counter
    LDA .letter_A
    STA .color_counter
.loop_fill_line:
    BIT PAGE2
    LDA .color_counter
    INC .color_counter
    LDY .counter
    STA (.POINTER),Y

    BIT PAGE1
    LDA .color_counter
    INC .color_counter
    LDY .counter
    STA (.POINTER),Y


    LDA .letter_SPACE
    BIT PAGE2
    STA (.POINTER2),Y
    BIT PAGE1
    STA (.POINTER2),Y

    INC .counter
    LDA .counter
    CMP #26
    BMI .loop_fill_line

    LDA .letter_A
    STA .color_counter
.loop_fill_line2:
    BIT PAGE2
    LDA .color_counter
    INC .color_counter
    LDY .counter
    STA (.POINTER),Y

    BIT PAGE1
    LDA .color_counter
    INC .color_counter
    LDY .counter
    STA (.POINTER),Y


    LDA .letter_SPACE
    BIT PAGE2
    STA (.POINTER2),Y
    BIT PAGE1
    STA (.POINTER2),Y

    INC .counter
    LDA .counter
    CMP #40
    BMI .loop_fill_line2

    RTS

.letter_A !text "A"
.letter_SPACE !text " "
.counter !byte 0
.color_counter !byte 0
}




txt_addr:
	!word $0400, $0480, $0500, $0580, $0600, $0680, $0700, $0780, $0428, $04A8, $0528, $05A8, $0628, $06A8, $0728, $07A8, $0450, $04D0, $0550, $05D0, $0650, $06D0, $0750, $07D0

hgr_addr:
	!word $2000, $2400, $2800, $2C00, $3000, $3400, $3800, $3C00, $2080, $2480, $2880, $2C80, $3080, $3480, $3880, $3C80, $2100, $2500, $2900, $2D00, $3100, $3500, $3900, $3D00, $2180, $2580, $2980, $2D80, $3180, $3580, $3980, $3D80, $2200, $2600, $2A00, $2E00, $3200, $3600, $3A00, $3E00, $2280, $2680, $2A80, $2E80, $3280, $3680, $3A80, $3E80, $2300, $2700, $2B00, $2F00, $3300, $3700, $3B00, $3F00, $2380, $2780, $2B80, $2F80, $3380, $3780, $3B80, $3F80, $2028, $2428, $2828, $2C28, $3028, $3428, $3828, $3C28, $20A8, $24A8, $28A8, $2CA8, $30A8, $34A8, $38A8, $3CA8, $2128, $2528, $2928, $2D28, $3128, $3528, $3928, $3D28, $21A8, $25A8, $29A8, $2DA8, $31A8, $35A8, $39A8, $3DA8, $2228, $2628, $2A28, $2E28, $3228, $3628, $3A28, $3E28, $22A8, $26A8, $2AA8, $2EA8, $32A8, $36A8, $3AA8, $3EA8, $2328, $2728, $2B28, $2F28, $3328, $3728, $3B28, $3F28, $23A8, $27A8, $2BA8, $2FA8, $33A8, $37A8, $3BA8, $3FA8, $2050, $2450, $2850, $2C50, $3050, $3450, $3850, $3C50, $20D0, $24D0, $28D0, $2CD0, $30D0, $34D0, $38D0, $3CD0, $2150, $2550, $2950, $2D50, $3150, $3550, $3950, $3D50, $21D0, $25D0, $29D0, $2DD0, $31D0, $35D0, $39D0, $3DD0, $2250, $2650, $2A50, $2E50, $3250, $3650, $3A50, $3E50, $22D0, $26D0, $2AD0, $2ED0, $32D0, $36D0, $3AD0, $3ED0, $2350, $2750, $2B50, $2F50, $3350, $3750, $3B50, $3F50, $23D0, $27D0, $2BD0, $2FD0, $33D0, $37D0, $3BD0, $3FD0

explanation !text "You see GR, DGR, HGR, DHGR, 40, 80"
            !text "HGR:pink,blue,orange,green,white"
            !byte 0
