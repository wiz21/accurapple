!to "GRTXT.bin", plain
!convtab "apple2c.ct"

* =   $6000

!source "equs.s"


BASCALC = $FBC1
BASL = $28
BASH = $29
HEXPRINT = $FDDA ; Apple2e ROM ? A = byte to display
COUT = $FDED
COUT1 = $FDF0
;; See http://www.applelogic.org/files/AIIMP.pdf (A2 monitor peeled)
COUTZ = $FDF6
HOME = $FC58
LINEFEED= $FC66
SCROLL_UP = $FC70
VTAB=$FC22
PRBYTE = $FDDA
PRBL2 = $F94A ;X=how many spaces




INIT:
    STA STORE80ON		; 80STORE set
    LDA HIRES
    BIT PAGE1
    LDA #0
    LDY #$04
    LDX #4
    JSR page_fill
    LDA #0
    LDY #$08
    LDX #4
    JSR page_fill

    BIT PAGE2
    LDA #0
    LDY #$04
    LDX #4
    JSR page_fill
    LDA #0
    LDY #$08
    LDX #4
    JSR page_fill

    BIT PAGE1
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

    BIT PAGE2
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

    BIT PAGE1
	JSR enable_vbl
	SEI

	+print_text_at screen_80col, 0, 20


	STA ALTCHARSETOFF
	LDA MIXEDON
	JSR fill_text_screen


	;;  GR
	LDA TEXTOFF ; 4C
	LDA LORES   ; 4
	STA COL80OFF
	LDA AN3ON
    LDA PAGE1


	+wait_redraw


	;; -------------------------------------------------------------
vbl1:
	;; At this point we're at the beginning of the frame.
	;; on the very first line to draw.
	;; I wait to get into the visible zone.

	; Display some progress and prepare line test. This costs 1 scan line
	; Then wait a few lines so that we can clearly see what happens in the
	; visible parts of the "£" and "_" characters.

	+short_wait 65-6 + 15*65
	; Give some visual feed back that this program is looping correctly
	INC $7D0+39		; 6


	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines
	JSR eight_lines

	; Compensate for the missing cycles in the "eight_line" with offset calls.
	+short_wait 15

	;; ---------------------------------------------------------------

	; -3 for the JMP
	; +1 to add the missing cycle of the last line of the loop above
	; -4 for the STA for mixed mode
	+wait_cycles2  20280 - (16+15*8)*65 - 3

	JMP vbl1 ; 3 cycles

!align 255, 0
eight_lines !zone {

	; Apply visual pattern to eight successive lines MINUS ONE CYCLE.
	; Minus one cycle because it allows the visual pattern to be shifted
	; by 14 pixels when this routine is called several times in a row

	; -2 for the LDX
	; 12 for the JSR (6c) /RTS (6c)
	; +1 for the last BNE not taken
	; - 1 for shifting LEFT
	+wait_cycles2 65-12-2+1 - 1
	LDX #7

.SYNCLOOP:
	; These 25 cycles are in the HBL
	BIT   TEXTON       ;     24 25 26 27
	+short_wait 29

	; Now the visible cycles
	BIT   TEXTOFF       ;     24 25 26 27
	+short_wait 4
	BIT   TEXTON

	+short_wait 4
	BIT   TEXTOFF

	+short_wait 65-33-20-5
	DEX 			   ; 60 61
	BNE   .SYNCLOOP     ; 62 63 64

	RTS
}


hgr_skip !zone {
	;; coming from JSR we're 6 cycles in the line
	LDA $A000	; 4 c
	LDA $A000	; 4 c
	+short_wait 65-16 + 6*65
	LDA $A000	; 4 c
	LDA $A000	; 4 c
	; 12 for the JSR (6c) /RTS (6c)
	+wait_cycles2 65-12
	RTS
}



waiter2 !zone {
	!align 255, 0
	;;;;;;;;;;;;;;;;;;;;;;;;
	;; Delays A:X clocks+overhead
	;; Time: 256*A+X+33 clocks (including JSR)
	;; Clobbers A. Preserves X,Y. Has relocations.
	;;;;;;;;;;;;;;;;;;;;;;;;
	;;  https://www.nesdev.org/wiki/Delay_code#256%C3%97A_+_X_+_33_cycles_of_delay,_clobbers_A,_Z&N,_C,_V

.wloop1:	; 5 cycles done, do 256-5 more.
	sbc #1			; 2 cycles - Carry was set from cmp
	pha                     ; 3
	lda #(256-27 - 16)     ; 2
	jsr .delay_a_27_clocks  ; 240
	pla                     ; 4
delay_256a_x_33_clocks2:
	cmp #1			; +2
	bcs .wloop1			; +3
	; 0-255 cycles remain, overhead = 4
	txa 			; -1+2; 6; +27 = 33

.delay_a_27_clocks:
        sec
.L:     	sbc #5
        bcs .L  ;  6 6 6 6 6  FB FC FD FE FF
        adc #3  ;  2 2 2 2 2  FE FF 00 01 02
        bcc .L4  ;  3 3 2 2 2  FE FF 00 01 02
        lsr     ;  - - 2 2 2  -- -- 00 00 01
        beq .L5  ;  - - 3 3 2  -- -- 00 00 01
.L4:     lsr     ;  2 2 - - 2  7F 7F -- -- 00
.L5:     bcs .L6  ;  2 3 2 3 2  7F 7F 00 00 00
.L6:     rts     ;
}




fill_text_screen !zone {
	LDA #1
	STA CV

	LDA #0
	STA CH

.new_line:
	LDA CV
	CLC
	ROL
	TAY
	LDA text_lines_ofs,Y
	STA .smc1+1
	INY
	LDA text_lines_ofs,Y
	STA .smc1+1+1

    LDA CV
    CLC
    AND #15
    STA .color
    +RROR
    +RROR
    +RROR
    +RROR
    ORA .color
    STA .color

.write_line:
    LDY CH
    LDA .color
.smc1	STA $0400,Y

	INC CH
	LDA CH
	CMP #40
	BNE .write_line
	LDA #0
	STA CH
.no_wrap_ch:

	INC CV
	LDA CV
	CMP #19
	BEQ .done_fill
	JMP .new_line
.done_fill:
	RTS

.color  !byte 0
}


	!align 255,0
screen_80colb:
	!source "thedate.txt"
screen_80col:
	!text "[This test shows GR and TXT modes      ]"
    !text "[                                      ]"
    !text "0123456789012345678901234567890123456789"
    !text "[                                      "
	!byte 0
