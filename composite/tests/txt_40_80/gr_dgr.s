!to "DHGRDGR.bin", plain
!convtab "apple2c.ct"

* =   $6000

!source "equs.s"


BASCALC = $FBC1
BASL = $28
BASH = $29
HEXPRINT = $FDDA ; Apple2e ROM ? A = byte to display
COUT = $FDED
COUT1 = $FDF0
;; See http://www.applelogic.org/files/AIIMP.pdf (A2 monitor peeled)
COUTZ = $FDF6
HOME = $FC58
LINEFEED= $FC66
SCROLL_UP = $FC70
VTAB=$FC22
PRBYTE = $FDDA
PRBL2 = $F94A ;X=how many spaces


INIT:
    STA STORE80ON		; 80STORE set
    LDA HIRES
    BIT PAGE1
    LDA #0
    LDY #$04
    LDX #4
    JSR page_fill
    LDA #0
    LDY #$08
    LDX #4
    JSR page_fill

    BIT PAGE2
    LDA #0
    LDY #$04
    LDX #4
    JSR page_fill
    LDA #0
    LDY #$08
    LDX #4
    JSR page_fill

    BIT PAGE1
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

    BIT PAGE2
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

    BIT PAGE1
	JSR enable_vbl
	SEI

	;PR#3: set 80 columns and activate firmware (for COUT to work)
	;LDA #3
	;OUTPORT = $FE95
	;JSR OUTPORT

	JSR $C300 ; PR#3
	+print_text_at screen_80col, 0, 20

	STA ALTCHARSETOFF
	LDA MIXEDON
	JSR fill_hgr_screen
	JSR fill_text_screen


	;;  GR
	LDA TEXTOFF ; 4C
	LDA LORES   ; 4
	STA COL80OFF
	LDA AN3OFF

				; Back to 40 columns
	 ;LDA #17
	 ;JSR COUT
	;LDA #21
	;JSR COUT
	;STA STORE80OFF		; 80STORE set

	+wait_redraw

	;; -------------------------------------------------------------
vbl1:
	;; At this point we're at the beginning of the frame.
	;; on the very first line to draw.
	;; I wait to get into the visible zone.

	; Display some progress and prepare line test. This costs 1 scan line
	; Then wait a few lines so that we can clearly see what happens in the
	; visible parts of the "£" and "_" characters.

	+short_wait 65-6 + 15*65
	; Give some visual feed back that this program is looping correctly
	INC $7D0+39		; 6


	JSR hgr_skip
	JSR eight_lines
	JSR hgr_skip
	JSR eight_lines
	JSR hgr_skip
	JSR eight_lines

	; Compensate for the missing cycles in the "eight_line" with offset calls.
	+short_wait 3

	; Setting up costs a line.
	; Not the last BNE was not taken, so the fiddling on the last
	; line lasted 64 vyvles, not 65. So we have to wait one more
	; to have a full line.
	+wait_cycles2 65-2
	LDX #40

	;; ---------------------------------------------------------------

	; -3 for the JMP
	; +1 to add the missing cycle of the last line of the loop above
	; -4 for the STA for mixed mode
	+wait_cycles2  20280 - (1+15+6*8+1)*65 - 3

	JMP vbl1 ; 3 cycles

!align 255, 0
eight_lines !zone {

	; Apply visual pattern to eight successive lines MINUS ONE CYCLE.
	; Minus one cycle because it allows the visual pattern to be shifted
	; by 14 pixels when this routine is called several times in a row

	; -2 for the LDX
	; 12 for the JSR (6c) /RTS (6c)
	; +1 for the last BNE not taken
	; - 1 for shifting LEFT
	+wait_cycles2 65-12-2+1 - 1
	LDX #7

.SYNCLOOP:
	; These 25 cycles are in the HBL
	STA   COL80OFF       ;     24 25 26 27
	+short_wait 29

	; Now the visible cycles
	STA   COL80ON       ;     24 25 26 27
	+short_wait 4
	STA   COL80OFF

	+short_wait 4
	STA   COL80ON

	+short_wait 65-33-20-5
	DEX 			   ; 60 61
	BNE   .SYNCLOOP     ; 62 63 64

	RTS
}


hgr_skip !zone {
	;; coming from JSR we're 6 cycles in the line
	LDA TEXTOFF	; 4 c
	LDA HIRES	; 4 c
	+short_wait 65-16 + 6*65
	LDA TEXTOFF	; 4 c
	LDA LORES	; 4 c
	; 12 for the JSR (6c) /RTS (6c)
	+wait_cycles2 65-12
	RTS
}



waiter2 !zone {
	!align 255, 0
	;;;;;;;;;;;;;;;;;;;;;;;;
	;; Delays A:X clocks+overhead
	;; Time: 256*A+X+33 clocks (including JSR)
	;; Clobbers A. Preserves X,Y. Has relocations.
	;;;;;;;;;;;;;;;;;;;;;;;;
	;;  https://www.nesdev.org/wiki/Delay_code#256%C3%97A_+_X_+_33_cycles_of_delay,_clobbers_A,_Z&N,_C,_V

.wloop1:	; 5 cycles done, do 256-5 more.
	sbc #1			; 2 cycles - Carry was set from cmp
	pha                     ; 3
	lda #(256-27 - 16)     ; 2
	jsr .delay_a_27_clocks  ; 240
	pla                     ; 4
delay_256a_x_33_clocks2:
	cmp #1			; +2
	bcs .wloop1			; +3
	; 0-255 cycles remain, overhead = 4
	txa 			; -1+2; 6; +27 = 33

.delay_a_27_clocks:
        sec
.L:     	sbc #5
        bcs .L  ;  6 6 6 6 6  FB FC FD FE FF
        adc #3  ;  2 2 2 2 2  FE FF 00 01 02
        bcc .L4  ;  3 3 2 2 2  FE FF 00 01 02
        lsr     ;  - - 2 2 2  -- -- 00 00 01
        beq .L5  ;  - - 3 3 2  -- -- 00 00 01
.L4:     lsr     ;  2 2 - - 2  7F 7F -- -- 00
.L5:     bcs .L6  ;  2 3 2 3 2  7F 7F 00 00 00
.L6:     rts     ;
}




fill_hgr_screen !zone {

.fill_page:
	INC .smc+1
	BNE .load_a
	INC .smc+1+1
	LDA .smc+1+1
	CMP #$40
	BNE .load_a
	RTS
.load_a:
	LDA .smc+1
	AND #1
	TAX
	LDA .patterns,X
.smc:
	STA $2000
	JMP .fill_page
;.patterns	!byte %00100011, %00010101
.patterns	!byte %00000001, %00000001
}


fill_text_screen !zone {
	LDA #1
	STA CV

	LDA #0
	STA CH
.new_line:
	LDA CV
	CLC
	ROL
	TAY
	LDA text_lines_ofs,Y
	STA .smc1+1
	INY
	LDA text_lines_ofs,Y
	STA .smc1+1+1

.write_line:
	;; Set the page based on CH odd or even
	LDA CH
	AND #1
	TAY
	LDA PAGE1, Y

	;;  Prepare offset to line position table
	LDA CH
	CLC
	ROR
	TAY

	;; Prepare the caracter we will display
	CLC
	LDA CV
	ROR
	AND #1
	CLC
	ADC CH
	AND #1
	BNE .underscore
	LDA #$23		; pound sign = $A3 (white on black)
	BNE .smc1
.underscore:
	LDA #$9F		; 9F underscore, 1F underscore inverse
.smc1:
	STA $0400,Y

	INC CH
	LDA CH
	CMP #80
	BNE .write_line
	LDA #0
	STA CH
.no_wrap_ch:

	INC CV
	INC CV
	LDA CV
	CMP #19
	BEQ .done_fill
	BPL .done_fill
	JMP .new_line
.done_fill:
	RTS

}


	!align 255,0
screen_80colb:
	!text "[ "
	!source "thedate.txt"
	!text " - This line is regular 80 columns"
	!text "                           ]"
	!for i,1,19 {
	!text "                                                                                "
	}
screen_80col:
	!text "[This test shows GR and DGR modes as well as the special 7MHz LORES mode.      ]"
	!text "                                                                               "
	!byte 0
