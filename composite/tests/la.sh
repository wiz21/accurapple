#!/bin/bash
ADIR=../..
APPLE_COMMANDER="java -jar AppleCommander-1.3.5.13-ac.jar"

rm NEW.DSK
cp BLANK_PRODOS2.DSK NEW.DSK
chmod +w NEW.DSK

python3 $ADIR/utils/generate_text_gr.py
python3 $ADIR/utils/make_convtab.py
python3 $ADIR/utils/print_date.py

# acme scott.s
# acme test_dhgr.s
# acme -r hgr_text.lst hgr_text.s
acme -I $ADIR/utils txt_40_80/gr_txt.s
# acme -r grtxt_mono.lst good_tests/grtxt_mono.s
acme -I $ADIR/utils logic_ana/la_gr_txt.s
acme -I $ADIR/utils logic_ana/lagr/la_gr.s
acme -I $ADIR/utils logic_ana/lagr/dhgr_txt.s
acme -I $ADIR/utils logic_ana/lagr/bad_burst.s


$APPLE_COMMANDER -p NEW.DSK LAGRTXT  BIN 0x6000 < LAGRTXT.bin
$APPLE_COMMANDER -p NEW.DSK LAGR     BIN 0x6000 < LAGR.bin
$APPLE_COMMANDER -p NEW.DSK GRTXT    BIN 0x6000 < GRTXT.bin
$APPLE_COMMANDER -p NEW.DSK DHGRTXT BIN 0x6000 < DHGR_TXT.bin
$APPLE_COMMANDER -p NEW.DSK BADBURST BIN 0x6000 < BAD_BURST.bin
python3 $ADIR/utils/dsk2nic.py --source NEW.DSK --target gfxtest.nic

export RUST_LOG=accurapple=info
export RUST_LOG=accurapple::a2=info,$RUST_LOG

# TXT 40 80
# $ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_simplified.wgsl" --charset "$ADIR/data/Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin"  --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RETURN, WAIT_UNTIL 14"

# ALL MODES
#$ADIR/target/release/accurapple --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --rom "$ADIR/data/apple2e-enhanced.rom" --log-stdout --cpu 6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP"

# DGR GR
# $ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_simplified.wgsl" --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,  KEY_RIGHT,KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP, SAVE_ALL_MEM allmem.bin,  SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"

# HGR TXT80
# $ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_simplified.wgsl" --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,  KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP, SAVE_ALL_MEM allmem.bin,  SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"


# L.A. GR-TXT
#$ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_simplified.wgsl" --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10,  KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP, SAVE_ALL_MEM allmem.bin,  SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"
# apple_to_composite_simplified.wgsl

#$ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_hal.wgsl" --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "SET_GFX_DEBUG_LINE 66, TURBO_START, WAIT_UNTIL 10, KEY_RIGHT,KEY_RIGHT,KEY_RIGHT,KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 14, KEY_RETURN, WAIT_UNTIL 20, SAVE_ALL_MEM allmem.bin,  WAIT_UNTIL 21, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin, TURBO_STOP"

$ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_hal.wgsl" --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --log-stdout --floppy1 NEW.DSK --script "TURBO_START"

#--script "SET_GFX_DEBUG_LINE 66, TURBO_START, WAIT_UNTIL 10, KEY_RIGHT,KEY_RIGHT,KEY_RIGHT,KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 14, KEY_RETURN, WAIT_UNTIL 20, SAVE_ALL_MEM allmem.bin,  WAIT_UNTIL 21, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin, TURBO_STOP"

# $ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_hal.wgsl" --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --log-stdout --cpu floooh6502 --floppy1 ../../additional/CC.dsk --power-debug --script "SET_GFX_DEBUG_LINE 66, WAIT_UNTIL 11.5, SAVE_ALL_MEM allmem.bin,  WAIT_UNTIL 21, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin, TURBO_STOP"
