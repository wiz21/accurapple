!to "DHGR_TXT.bin", plain
!convtab "apple2c.ct"

* =   $6000

!source "equs.s"


BASCALC = $FBC1
BASL = $28
BASH = $29
HEXPRINT = $FDDA ; Apple2e ROM ? A = byte to display
COUT = $FDED
COUT1 = $FDF0
;; See http://www.applelogic.org/files/AIIMP.pdf (A2 monitor peeled)
COUTZ = $FDF6
HOME = $FC58
LINEFEED= $FC66
SCROLL_UP = $FC70
VTAB=$FC22
PRBYTE = $FDDA
PRBL2 = $F94A ;X=how many spaces


INIT:
	+switch_dhgr
	JSR clear_video_ram
	JSR enable_vbl
	SEI
	LDA #0
	LDX #192
	JSR plot_dhgr
	+set_cursor_at 0, 0
	lda #<screen_80colb
	ldy #>screen_80colb
	JSR print_80col

	+set_cursor_at 1, 2
	lda #<screen_80colb
	ldy #>screen_80colb
	JSR print_80col

	+set_cursor_at 2, 4
	lda #<screen_80colb
	ldy #>screen_80colb
	JSR print_80col

	LDA MIXEDOFF

	!align 255, 0
	JMP skipper

fine_wait:
	N = CYCLES_PER_FRAME - 1 - 6 - 3 - 1 - 2
	LDA # (N - 34) >> 8	; 2
	LDX # (N - 34) & 255	; 2
	JMP delay_256a_x_33_clocks
skipper:

	;; The general idea is this:
	;; Detectct the start of the drawing area. When done
	;; a few cycles have passed so we're inside the drawing area.
	;; Now, we wait a little less than the duration of a full
	;; frame so that we end up a bit "higher" on the screen, we do
	;; that with a big step then with 1 cycle steps.
	;; We continue until we're back in the VBL area, just above the
	;; drawing area. Doing it like that allows us to be on the
	;; very cycle where the frame draw start.

	JSR switch_display
	;; Returning from JSR is 6 cycles
	N2 = CYCLES_PER_FRAME - 6 - 4
	LDA # (N2 - 34) >> 8	; 2
	LDX # (N2 - 34) & 255	; 2
	JSR delay_256a_x_33_clocks		; 3

	;; at this point we're in the drawing zone

	!for X,1,16 {
	;; If no branch, This will have the total effect of waiting
	;; CYCLES_PER_FRAME minus one cycle.
	lda     VERTBLANK       ; 4
	;; A=$80=Drawing; A=0=VBL
        bpl     zed		; 2 or 3 branch if in the VBL part
	JSR fine_wait
	}

zed:
	+wait_cycles2 20280-3


	;; -------------------------------------------------------------
vbl1:
	;; At this point we're at the beginning of the frame.
	;; on the very first line to draw.
	;; I wait to get into the visible zone.

 	;; --- TEST 1 ------------------------------------------
	;; 65 * 4 lines = 260 µsec

        LDA $8000 ; trigger logic analyzer ($C000 = $8000+$4000) 4c
	+switch_dhgr		; 16c
	+short_wait 65-16-4

	+short_wait 65*25

	;; +switch_dhgr
	+short_wait 65 - 4	; -16
	LDX #<TEXTOFF		;2c
	LDY #<TEXTON		;2c

	JSR six_lines
	;; 	+switch_dhgr
	LDA TEXTOFF
	+short_wait 65 - 4 + 65*4

	+short_wait 65*80

	;; +switch_dhgr
	+short_wait 65 - 4	; -16
	LDX #<TEXTOFF		;2c
	LDY #<TEXTON		;2c

	JSR six_lines
	;; +switch_dhgr
	STA TEXTOFF
	+short_wait 65 - 4 + 65*4

	LDA $C000
	BMI key_pressed
	+short_wait 65-6

	; -3 for the JMP
	;
	+wait_cycles2  20280 - (65*(1+8+80+8+25+1) + 3)

	JMP vbl1 ; 3 cycles
key_pressed:
	+switch_txt40
	JMP $C600

!align 255, 0
six_lines !zone {

	; These 25 cycles are in the HBL
	+short_wait 25-6    ; -6 for JSR
	; Now the visible cycles
	STA $C000,Y		;5
	;; NOP			;2
	;; STA $C000,Y		;5
	;; STA $C000,Y		;5 X
	;; STA $C000,Y		;5
	+short_wait 65-25-5

	;; let it go until the end of the HBL
	+short_wait 25
	STA $C000,X		;5

	;; +short_wait 25
	;; +short_wait 23
	+short_wait 65 - 30 - 6 ; 6 = RTS


	RTS
}



fill_text_screen !zone {
	LDA #1
	STA CV

	LDA #0
	STA CH

.new_line:
	LDA CV
	CLC
	ROL
	TAY
	LDA text_lines_ofs,Y
	STA .smc1+1
	INY
	LDA text_lines_ofs,Y
	STA .smc1+1+1

    LDA CV
    CLC
    AND #15
    STA .color
    +RROR
    +RROR
    +RROR
    +RROR
    ORA .color
    STA .color

.write_line:
    LDY CH
    LDA .color
.smc1	STA $0400,Y

	INC CH
	LDA CH
	CMP #40
	BNE .write_line
	LDA #0
	STA CH
.no_wrap_ch:

	INC CV
	LDA CV
	CMP #19
	BEQ .done_fill
	JMP .new_line
.done_fill:
	RTS

.color  !byte 0
}
GRHGR_GR_LINE_ADDR = $06A8
GRHGR_HGR_LINE_ADDR = $26A8
GRHGR_GR_LINE_ADDR2 = $06A8
GRHGR_HGR_LINE_ADDR2 = $2AA8

hgrgr_bytes_copy !zone {
	LDX #0
.loop:
	LDA grhgr_gr,X
	STA GRHGR_GR_LINE_ADDR,X
	STA GRHGR_GR_LINE_ADDR2,X
	LDA grhgr_hgr,X
	STA GRHGR_HGR_LINE_ADDR,X
	STA GRHGR_HGR_LINE_ADDR2,X
	INX
	CPX #40
	BNE .loop
	RTS
}

	!align 255,0
screen_80colb:
	!source "thedate.txt"
	!text "MAMAMAMAMAMAMAMAMAMAMAM"
	!text "0123456789012345678901234567890123456789"
	!byte 0

screen_80col:
	!text "[This test shows GR and TXT modes      ]"
    !text "[                                      ]"
    !text "0123456789012345678901234567890123456789"
    !text "[                                      "
	!byte 0

screen_txt_hgr:
    !for i,0,39 {
    !byte $40 + i
    }
	!byte 0

grhgr_gr !byte $66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$00,$98,$79,$98,$AA,$98,$BB,$98,$CC,$98,$DD,$98,$EE,$98,$FF,$A9,$16
grhgr_hgr !byte $49,$88,$88,$89,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00


plot_gr_rainbow !zone {
	;; A = start_line
	;; X = end line
	STA .line
	STX .end_line_smc + 1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA text_lines_ofs,x
    STA .smc1 + 1
    INX
    LDA text_lines_ofs,x
    STA .smc1 + 2

    LDA #0
    STA .counter
.loop_fill_line:
    LDA .counter
    CLC
    ADC #2
    ROR
    AND #15
    STA .masked_counter
    CLC
    ROL
    ROL
    ROL
    ROL
    ORA .masked_counter

    LDX .counter
.smc1:
    STA $400,X

    INC .counter
    LDA .counter
    CMP #32
    BNE .loop_fill_line

    INC .line
    LDA .line
.end_line_smc:
    CMP #1
    BNE .loop_line
    RTS

.counter !byte 0
.line !byte 0
.masked_counter !byte 0
}


plot_gr_monochrome !zone {
	;; A = start_line
	;; X = end line
	;; Y = color
	STA .line
	STX .end_line_smc + 1
	STY .smc_color + 1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA text_lines_ofs,x
    STA .smc1 + 1
    INX
    LDA text_lines_ofs,x
    STA .smc1 + 2

    LDX #0
.smc_color	LDA #0
.loop_fill_line:
.smc1 STA $400,X
    INX
    CPX #39
    BNE .loop_fill_line

    INC .line
    LDA .line
.end_line_smc CMP #1
    BNE .loop_line
    RTS

.counter !byte 0
.line !byte 0
}


plot_dgr !zone {
    .POINTER = $FE
	.FIRST_LINE = 1
	.LAST_LINE = .FIRST_LINE + 2

    STA .line
	STX .last_line_smc+1

    STY STORE80ON		; 80STORE set
    LDA LORES
	LDX PAGE1

.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA text_lines_ofs,x
    STA .POINTER
    INX
    LDA text_lines_ofs,x
    STA .POINTER + 1

    LDA #0
    STA .counter
    LDA #1
    STA .color_counter
.loop_fill_line:
    LDA .color_counter
    AND #15
    STA .color
    ASL
    ASL
    ASL
    ASL
    ORA .color
    +RROR
    STA .color
    INC .color_counter

    LDX PAGE2           ; Selects AUX
    LDY .counter
    STA (.POINTER),Y

    LDX PAGE1           ; Selects RAM
    PHA
    ROL
    PLA
    ROL
    STA (.POINTER),Y

    INC .counter
    LDX PAGE2           ; Selects RAM
    LDY .counter
    LDA .color
    STA (.POINTER),Y
    LDX PAGE1           ; Selects AUX
    LDA #0
    STA (.POINTER),Y

    INC .counter


    LDA .counter
    CMP #32
    BMI .loop_fill_line

    INC .line
    LDA .line
.last_line_smc:
    CMP #.LAST_LINE
    BNE .loop_line

    STX STORE80OFF
    LDY PAGE1
    RTS

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}



plot_hgr !zone {
    POINTER = $FE
	.FIRST_LINE = 3
	.LAST_LINE = .FIRST_LINE + 7
    STA .line
	STX .last_line_smc+1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA hgr_addr,x
    STA POINTER
    INX
    LDA hgr_addr,x
    STA POINTER + 1

    LDA #0
    STA .counter
    LDA #1
    STA .color_counter
.loop_fill_line:
    LDA .color_counter
    INC .color_counter
    AND #7
    TAX

    LDA .color_pos, X
    TAY
    LDA .colors, X
    INC .counter
    STA (POINTER),Y


    LDA .rotated_colors, X
    INY
    INC .counter
    STA (POINTER),Y

    LDA .counter
    CMP #16
    BMI .loop_fill_line

    INC .line
    LDA .line
.last_line_smc:
	CMP #0
    BNE .loop_line

    RTS

// Purple, green, white, black, blue, red, white
.color_pos !byte 0,4,22,0, 0, 10, 12+4, 28
.colors !byte %00000000, %01010101, %00101010, %01111111
        !byte %10000000, %11010101, %10101010, %11111111
.rotated_colors:
        !byte %00000000, %00101010, %01010101, %01111111
        !byte %10000000, %10101010, %11010101, %11111111

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}


plot_hgr2 !zone {
    POINTER = $FE
    STA .line
	STX .last_line_smc+1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA hgr_addr,x
    STA POINTER
    INX
    LDA hgr_addr,x
    STA POINTER + 1

    LDA #0
    STA .counter
.loop_fill_line:
	LDX .counter
	LDY .counter
    LDA .colors_line, X
    STA (POINTER),Y
    INC .counter

	LDA .counter
    CMP #32
    BMI .loop_fill_line

    INC .line
    LDA .line
.last_line_smc:
	CMP #0
    BNE .loop_line

    RTS

// Purple, green, white, black, blue, red, white
.color_pos !byte 0,4,5,22,23,0, 0, 10, 11,12+4,12+4+1, 28,29
.colors !byte %00000000, %01010101, %00101010, %01111111
        !byte %10000000, %11010101, %10101010, %11111111
.rotated_colors:
        !byte %00000000, %00101010, %01010101, %01111111
        !byte %10000000, %10101010, %11010101, %11111111

.colors_line:
	!byte %11010101, %10101010
	!byte %11010101, %10101010

	!byte %10101010, %11010101
	!byte %10101010, %11010101

	!byte %11010101, %10101010
	!byte %11010101, %10101010

	!byte %10101010, %11010101
	!byte %10101010, %11010101

	!byte %11010101, %10101010
	!byte %11010101, %10101010

	!byte %10101010, %11010101
	!byte %10101010, %11010101

	!byte %11010101, %10101010
	!byte %11010101, %10101010

	!byte %10101010, %11010101
	!byte %10101010, %11010101

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}


plot_hgr3 !zone {
    POINTER = $FE
    STA .line
	STX .last_line_smc+1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA hgr_addr,x
    STA POINTER
    INX
    LDA hgr_addr,x
    STA POINTER + 1

    LDA #0
    STA .counter
.loop_fill_line:
	LDA .counter
	AND #$0F
	CLC
	ADC #$40
	LDY .counter
	STA (POINTER),Y
	INC .counter

	LDA .counter
	CMP #32
	BMI .loop_fill_line

    INC .line
    LDA .line
.last_line_smc:
	CMP #0
    BNE .loop_line

    RTS

.counter !byte 0
.line !byte 0
}
