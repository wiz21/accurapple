!to "BAD_BURST.bin", plain
!convtab "apple2c.ct"

* =   $6000

!source "equs.s"

	+switch_dhgr
	JSR clear_video_ram
	JSR clear_text_ram
	JSR enable_vbl
	SEI
	LDA #0
	LDX #192
	JSR plot_dhgr

	+set_cursor_at 0, 21
	lda #<screen_40cold
	ldy #>screen_40cold
	JSR print_para

	+set_cursor_at 0, 0
	lda #<screen_80colb
	ldy #>screen_80colb
	JSR print_80col

	+set_cursor_at 1, 2
	lda #<screen_80colb
	ldy #>screen_80colb
	JSR print_80col

	+set_cursor_at 2, 4
	lda #<screen_80colb
	ldy #>screen_80colb
	JSR print_80col

	+set_cursor_at 0, 20
	lda #<screen_80colc
	ldy #>screen_80colc
	JSR print_80col



	LDA MIXEDON

run_frame:
	+wait_redraw

	;; -------------------------------------------------------------
vbl1:
	;; At this point we're at the beginning of the frame.
	;; on the very first line to draw. We're in the HBL though,
	;; So we will have to wait 25 cycles before being in the
	;; visible area.

        LDA $8000 ; trigger logic analyzer ($C000 = $8000+$4000) 4c
	+short_wait 25
	+switch_dhgr		; 16c
	+short_wait 65-16-4-25

	;; Wait 1 line plus 35 + switch_offset cycles

vertical_wait_smc1_hi = * + 1
vertical_wait_smc1_lo = * + 3
	+wait_cycles2 1*65+35	; 1 line (out of 12)

	;; At the beginning of the HBL...

	;; Here we jump to several wait routine (we will SMC
	;; the JSR to one of these routines)
	;; In the wait routine we temporarily to text mode
	;; and back to dhgr mode.
smc_jsr:
	JSR wait_14		; wait 1 line and to a TXT/GR switch after +/- 10 cycles

vertical_wait_smc2_hi = * + 1
vertical_wait_smc2_lo = * + 3
	+wait_cycles2 12*65-(1*65+35)	; 11 lines (out of 12)

	;; +short_wait 65

	+switch_dhgr		; 16c
	+short_wait 65-16	; 1 line

	;; Wait a line of TXT. This will zap the colour burst.
	;; Seems like it is enough for the CRT to decide to go full
	;; black and white.
	;; LDA TEXTON		; 4c
	;; +short_wait 65-8	; 1 line
	;; LDA TEXTOFF		; 4c

	+short_wait 65*148-16
	+switch_txt40

	+short_wait 65*16-16
	+switch_txt80

	;; Wait a bit before starting the mode switch

	LDA $C000		; 4c
	BMI key_pressed		; 2c
	+short_wait 65-6	; 1 line


	+short_wait 20280 - 65*(12+4) - 3 - 65*148 - 65*16
	JMP vbl1 ; 3 cycles

key_pressed:
	AND #127		; clear key pressed indicator
	CMP #$15		; Key right
	BNE not_key_right
	JSR handle_key_right
	JMP done_keys
not_key_right:

	CMP #$8		; Key left
	BNE not_key_left
	DEC wait_select_smc + 1
	LDA wait_select_smc + 1
	CMP #$FF
	BNE done_keys
	LDA #0
	STA wait_select_smc + 1

	JMP done_keys
not_key_left:
	CMP #$A		; Key down
	BNE not_key_down

	LDY #255
	LDX #-65
	JSR move_mode_switch_by_YX

	JMP done_keys
not_key_down:


	CMP #$B		; Key up
	BNE not_key_up
	LDY #0
	LDX #65
	JSR move_mode_switch_by_YX
	JMP done_keys
not_key_up:

	CMP #$49		; I
	BNE not_I_key
	JSR handle_i_key
	JMP done_keys
not_I_key:

	CMP #$55		; U
	BNE not_U_key
	JSR handle_j_key
	JMP done_keys
not_U_key:

	CMP #$1B		; Key up
	BNE not_esc
	JMP $C600
not_esc:

done_keys:

wait_select_smc:
	LDA #0
	CLC
	ROL
	TAX
	LDA wait_routines,x
	STA smc_jsr + 1
	INX
	LDA wait_routines,x
	STA smc_jsr + 2
	INX

	LDX wait_select_smc + 1
	+print_byte $6D0+5
	LDX switch_offset
	+print_byte $6D0+16

	inc $2000
	inc $400
	LDA $c010		; Clear the keyboard
	JMP run_frame

handle_key_right:
	INC wait_select_smc + 1
	LDA wait_select_smc + 1
	CMP #NB_WAITS
	BMI no_limit
	LDA #0
	STA wait_select_smc + 1
no_limit:
	RTS

handle_i_key:
	INC switch_offset
	LDY #$FF
	LDX #$FF
	JSR move_mode_switch_by_YX
	RTS

handle_j_key:
	DEC switch_offset
	LDY #0
	LDX #1
	JSR move_mode_switch_by_YX
	RTS


move_mode_switch_by_YX !zone {
	;;  Y:X
	CLC
	TXA
	ADC vertical_wait_smc2_lo
	STA vertical_wait_smc2_lo
	TYA
	ADC vertical_wait_smc2_hi
	STA vertical_wait_smc2_hi

	SEC
	LDA vertical_wait_smc1_lo
	STX .smc1
.smc1 = * + 1
	SBC #65
	STA vertical_wait_smc1_lo
	LDA vertical_wait_smc1_hi
	STY .smc2
.smc2 = * + 1
	SBC #0
	STA vertical_wait_smc1_hi
	RTS
}

!align 255, 0
six_lines !zone {

	; These 25 cycles are in the HBL
	+short_wait 25-6    ; -6 for JSR
	; Now the visible cycles
	STA $C000,X		;5
	NOP			;2
	STA $C000,Y		;5
	STA $C000,X		;5
	STA $C000,Y		;5
	+short_wait 65-25-22 ; wait until the end of the line.

	;; ; These 25 cycles are in the HBL
	;; +short_wait 25
	;; ; Now the visible cycles
	;; STA $C000,X		;5
	;; NOP
	;; STA $C000,Y		;5
	;; STA $C000,X		;5
	;; STA $C000,Y		;5
	;; +short_wait 65-25-22

	;; ; These 25 cycles are in the HBL
	;; +short_wait 25
	;; ; Now the visible cycles
	;; STA $C000,X		;5
	;; STA $C000,Y		;5
	;; NOP
	;; STA $C000,X		;5
	;; STA $C000,Y		;5
	;; +short_wait 65-25-22

	;; ; These 25 cycles are in the HBL
	;; +short_wait 25
	;; ; Now the visible cycles
	;; STA $C000,X		;5
	;; LDA $55
	;; STA $C000,Y		;5
	;; STA $C000,X		;5
	;; STA $C000,Y		;5
	;; +short_wait 65-25-23

	;; ; These 25 cycles are in the HBL
	;; +short_wait 25
	;; ; Now the visible cycles
	;; STA $C000,X		;5
	;; LDA $55
	;; STA $C000,Y		;5
	;; STA $C000,X		;5
	;; STA $C000,Y		;5
	;; +short_wait 65-25-23


	; These 25 cycles are in the HBL
	+short_wait 25
	; Now the visible cycles
	STA $C000,X		;5
	LDA $55
	STA $C000,Y		;5
	STA $C000,X		;5
	STA $C000,Y		;5
	+short_wait 65-25-23 - 6 ; 6 = RTS


	RTS
}



fill_text_screen !zone {
	LDA #1
	STA CV

	LDA #0
	STA CH

.new_line:
	LDA CV
	CLC
	ROL
	TAY
	LDA text_lines_ofs,Y
	STA .smc1+1
	INY
	LDA text_lines_ofs,Y
	STA .smc1+1+1

    LDA CV
    CLC
    AND #15
    STA .color
    +RROR
    +RROR
    +RROR
    +RROR
    ORA .color
    STA .color

.write_line:
    LDY CH
    LDA .color
.smc1	STA $0400,Y

	INC CH
	LDA CH
	CMP #40
	BNE .write_line
	LDA #0
	STA CH
.no_wrap_ch:

	INC CV
	LDA CV
	CMP #19
	BEQ .done_fill
	JMP .new_line
.done_fill:
	RTS

.color  !byte 0
}
GRHGR_GR_LINE_ADDR = $06A8
GRHGR_HGR_LINE_ADDR = $26A8
GRHGR_GR_LINE_ADDR2 = $06A8
GRHGR_HGR_LINE_ADDR2 = $2AA8

hgrgr_bytes_copy !zone {
	LDX #0
.loop:
	LDA grhgr_gr,X
	STA GRHGR_GR_LINE_ADDR,X
	STA GRHGR_GR_LINE_ADDR2,X
	LDA grhgr_hgr,X
	STA GRHGR_HGR_LINE_ADDR,X
	STA GRHGR_HGR_LINE_ADDR2,X
	INX
	CPX #40
	BNE .loop
	RTS
}

	!align 255,0
screen_80colb:
	!source "thedate.txt"
	!text "MAMAMAMAMAMAMAMAMAMAMAM"
	!text "0123456789012345678901234567890123456789"
	!byte 0

screen_80colc:
	!text "------------------------------------------------------------------------"
	!byte 0
screen_40cold:
	!text "SIZE:    OFFSET:   "
	!source "thedate.txt"
	!byte 0
screen_80col:
	!text "[This test shows GR and TXT modes      ]"
	!text "[                                      ]"
	!text "0123456789012345678901234567890123456789"
	!text "[                                      "
	!byte 0

screen_txt_hgr:
    !for i,0,39 {
    !byte $40 + i
    }
	!byte 0

grhgr_gr !byte $66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$00,$98,$79,$98,$AA,$98,$BB,$98,$CC,$98,$DD,$98,$EE,$98,$FF,$A9,$16
grhgr_hgr !byte $49,$88,$88,$89,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00


plot_gr_rainbow !zone {
	;; A = start_line
	;; X = end line
	STA .line
	STX .end_line_smc + 1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA text_lines_ofs,x
    STA .smc1 + 1
    INX
    LDA text_lines_ofs,x
    STA .smc1 + 2

    LDA #0
    STA .counter
.loop_fill_line:
    LDA .counter
    CLC
    ADC #2
    ROR
    AND #15
    STA .masked_counter
    CLC
    ROL
    ROL
    ROL
    ROL
    ORA .masked_counter

    LDX .counter
.smc1:
    STA $400,X

    INC .counter
    LDA .counter
    CMP #32
    BNE .loop_fill_line

    INC .line
    LDA .line
.end_line_smc:
    CMP #1
    BNE .loop_line
    RTS

.counter !byte 0
.line !byte 0
.masked_counter !byte 0
}


plot_gr_monochrome !zone {
	;; A = start_line
	;; X = end line
	;; Y = color
	STA .line
	STX .end_line_smc + 1
	STY .smc_color + 1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA text_lines_ofs,x
    STA .smc1 + 1
    INX
    LDA text_lines_ofs,x
    STA .smc1 + 2

    LDX #0
.smc_color	LDA #0
.loop_fill_line:
.smc1 STA $400,X
    INX
    CPX #39
    BNE .loop_fill_line

    INC .line
    LDA .line
.end_line_smc CMP #1
    BNE .loop_line
    RTS

.counter !byte 0
.line !byte 0
}


plot_dgr !zone {
    .POINTER = $FE
	.FIRST_LINE = 1
	.LAST_LINE = .FIRST_LINE + 2

    STA .line
	STX .last_line_smc+1

    STY STORE80ON		; 80STORE set
    LDA LORES
	LDX PAGE1

.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA text_lines_ofs,x
    STA .POINTER
    INX
    LDA text_lines_ofs,x
    STA .POINTER + 1

    LDA #0
    STA .counter
    LDA #1
    STA .color_counter
.loop_fill_line:
    LDA .color_counter
    AND #15
    STA .color
    ASL
    ASL
    ASL
    ASL
    ORA .color
    +RROR
    STA .color
    INC .color_counter

    LDX PAGE2           ; Selects AUX
    LDY .counter
    STA (.POINTER),Y

    LDX PAGE1           ; Selects RAM
    PHA
    ROL
    PLA
    ROL
    STA (.POINTER),Y

    INC .counter
    LDX PAGE2           ; Selects RAM
    LDY .counter
    LDA .color
    STA (.POINTER),Y
    LDX PAGE1           ; Selects AUX
    LDA #0
    STA (.POINTER),Y

    INC .counter


    LDA .counter
    CMP #32
    BMI .loop_fill_line

    INC .line
    LDA .line
.last_line_smc:
    CMP #.LAST_LINE
    BNE .loop_line

    STX STORE80OFF
    LDY PAGE1
    RTS

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}



plot_hgr !zone {
    POINTER = $FE
	.FIRST_LINE = 3
	.LAST_LINE = .FIRST_LINE + 7
    STA .line
	STX .last_line_smc+1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA hgr_addr,x
    STA POINTER
    INX
    LDA hgr_addr,x
    STA POINTER + 1

    LDA #0
    STA .counter
    LDA #1
    STA .color_counter
.loop_fill_line:
    LDA .color_counter
    INC .color_counter
    AND #7
    TAX

    LDA .color_pos, X
    TAY
    LDA .colors, X
    INC .counter
    STA (POINTER),Y


    LDA .rotated_colors, X
    INY
    INC .counter
    STA (POINTER),Y

    LDA .counter
    CMP #16
    BMI .loop_fill_line

    INC .line
    LDA .line
.last_line_smc:
	CMP #0
    BNE .loop_line

    RTS

// Purple, green, white, black, blue, red, white
.color_pos !byte 0,4,22,0, 0, 10, 12+4, 28
.colors !byte %00000000, %01010101, %00101010, %01111111
        !byte %10000000, %11010101, %10101010, %11111111
.rotated_colors:
        !byte %00000000, %00101010, %01010101, %01111111
        !byte %10000000, %10101010, %11010101, %11111111

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}


plot_hgr2 !zone {
    POINTER = $FE
    STA .line
	STX .last_line_smc+1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA hgr_addr,x
    STA POINTER
    INX
    LDA hgr_addr,x
    STA POINTER + 1

    LDA #0
    STA .counter
.loop_fill_line:
	LDX .counter
	LDY .counter
    LDA .colors_line, X
    STA (POINTER),Y
    INC .counter

	LDA .counter
    CMP #32
    BMI .loop_fill_line

    INC .line
    LDA .line
.last_line_smc:
	CMP #0
    BNE .loop_line

    RTS

// Purple, green, white, black, blue, red, white
.color_pos !byte 0,4,5,22,23,0, 0, 10, 11,12+4,12+4+1, 28,29
.colors !byte %00000000, %01010101, %00101010, %01111111
        !byte %10000000, %11010101, %10101010, %11111111
.rotated_colors:
        !byte %00000000, %00101010, %01010101, %01111111
        !byte %10000000, %10101010, %11010101, %11111111

.colors_line:
	!byte %11010101, %10101010
	!byte %11010101, %10101010

	!byte %10101010, %11010101
	!byte %10101010, %11010101

	!byte %11010101, %10101010
	!byte %11010101, %10101010

	!byte %10101010, %11010101
	!byte %10101010, %11010101

	!byte %11010101, %10101010
	!byte %11010101, %10101010

	!byte %10101010, %11010101
	!byte %10101010, %11010101

	!byte %11010101, %10101010
	!byte %11010101, %10101010

	!byte %10101010, %11010101
	!byte %10101010, %11010101

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}


plot_hgr3 !zone {
    POINTER = $FE
    STA .line
	STX .last_line_smc+1
.loop_line:
    LDA .line
    CLC
    ROL
    TAX
    LDA hgr_addr,x
    STA POINTER
    INX
    LDA hgr_addr,x
    STA POINTER + 1

    LDA #0
    STA .counter
.loop_fill_line:
	LDA .counter
	AND #$0F
	CLC
	ADC #$40
	LDY .counter
	STA (POINTER),Y
	INC .counter

	LDA .counter
	CMP #32
	BMI .loop_fill_line

    INC .line
    LDA .line
.last_line_smc:
	CMP #0
    BNE .loop_line

    RTS

.counter !byte 0
.line !byte 0
}

wait_routines	!word wait_14, wait_15, wait_16, wait_17, wait_18, wait_19, wait_20, wait_21, wait_22, wait_23
	!word wait_24, wait_25, wait_26, wait_27, wait_28, wait_29, wait_30, wait_31
NB_WAITS = (* - wait_routines)/2

!macro waiter_colour_burst .N {
	;; TEXTON occurs 6 + 4 cycles after the call to this subroutine
	!if .N <= 1 {
	STA TEXTON		; 4c
	STA TEXTOFF		; 4c
	+short_wait 65 - 12 - 8
	}
	!if .N >= 2 {
	STA TEXTON		; 4c
	+short_wait .N
	STA TEXTOFF		; 4c
	+short_wait 65 - .N - 12 - 8
	}
	RTS			; 6c
}

wait_14:	+waiter_colour_burst 0
wait_15:	+waiter_colour_burst 1
wait_16:	+waiter_colour_burst 2
wait_17:	+waiter_colour_burst 3
wait_18:	+waiter_colour_burst 4
wait_19:	+waiter_colour_burst 5
wait_20:	+waiter_colour_burst 6
wait_21:	+waiter_colour_burst 7
wait_22:	+waiter_colour_burst 8
wait_23:	+waiter_colour_burst 9
wait_24:	+waiter_colour_burst 10
wait_25:	+waiter_colour_burst 11
wait_26:	+waiter_colour_burst 12
wait_27:	+waiter_colour_burst 13
wait_28:	+waiter_colour_burst 14
wait_29:	+waiter_colour_burst 15
wait_30:	+waiter_colour_burst 16
wait_31:	+waiter_colour_burst 17
switch_offset !byte 0
