from matplotlib import pyplot as plt
import numpy as np

N=1000
s = np.zeros(N)
BW = 7
for i in range(BW,200,2*BW):
    s = s + np.sin(np.arange(N, dtype=np.float32)*i*3.14/2500)/i
plt.plot(s)
plt.show()
exit()

def append_value(series, tag, value):
    if tag not in series:
        series[tag] = []
    series[tag].append(value)

def plot_series(series, experiment = 0):

    color, offset, pattern = [ ["red",0,"dashed"], ["blue",0.01,"solid"] ][experiment]
    SPACE = 1.5
    for serie_ndx, serie in enumerate(reversed(series.items())):
        name, data = serie
        plt.step(
            np.arange(len(data)) + offset,
            np.array(data) + serie_ndx*SPACE + offset,
            where="post", color=color, linestyle=pattern)

    plt.yticks(0.5 + np.arange(len(series))*SPACE, reversed(series.keys()))
    plt.ylim(-1, (len(series))*SPACE)


class Delay:
    def __init__(self, value, time, future_time):
        #print(f"At {time} Delaying {value} until {future_time}")
        self.value = value
        self.future_time = future_time

    def __str__(self):
        return f"@{self.future_time}: {self.value}"

def compute_segb(gr, vb, lohi):
    """
    :param gr: graphics time (1 == graphics time, 0 == text time)
    :param lohi: hires (1 == hires, 0 == lores)
    """
    print(f"compute_segb(gr={gr}, vb={vb}, lohi={lohi})")
    gr_p = not gr
    if gr_p:
        print(f"gr'={not gr} => segb=vb {vb}")
        return vb
    else:
        print(f"gr'={not gr} => segb=lohi {lohi}")
        # HIRES' == 1 == HIRES = 0 == lores mode
        # HIRES' == 0 == HIRES = 1 == hires mode
        hires_p = lohi == 0 # lohi == 0 means lores
        return hires_p


def compute(ssw_gr, ssw_lohi, ssw_col80, vb):

    current_gr_txt = ssw_gr[0]
    current_lohi = ssw_lohi[0]
    current_col80 = ssw_col80[0]
    current_segb = compute_segb(1-current_gr_txt, vb, current_lohi)
    delayed_gr_txt = Delay(current_gr_txt, 0, 0)
    delayed_segb = Delay(current_segb, 0, 0)
    delayed_lohi = Delay(current_lohi, 0, 0)
    delayed_col80 = Delay(current_col80, 0, 0)

    segb = []
    series= dict()

    VID7M = False
    VID7M_S1 = False
    VID7M_S2 = False
    VID7M_S3 = False
    GR2 = False
    for i in range(len(ssw_gr)-1):
        ssw_gr_new = ssw_gr[i]
        ssw_gr_old = ssw_gr[i-1]
        if ssw_gr_new == 1 and ssw_gr_old == 0:
            # Rising GR/TXT (we go from graphics to text mode)
            print("Rising GRTXT => GR+2' rises to one")
            DELAY_SEGB = 2*14+6
            DELAY_GRTXT_RISING=DELAY_SEGB+1
            delayed_gr_txt = Delay(ssw_gr_new, i, i+(DELAY_GRTXT_RISING)*2)
            delayed_segb = Delay(compute_segb(1-ssw_gr_new, vb, current_lohi), i, i+DELAY_SEGB*2)
        elif ssw_gr_new == 0 and ssw_gr_old == 1:
            # Falling GR/TXT (we go from text to graphics mode)
            print("Falling GRTXT: from text mode to graphics mode => GR+2' falls to zero")
            DELAY_GRTXT_FALLING =2*14+6.5
            DELAY_SEGB = DELAY_GRTXT_FALLING
            delayed_gr_txt = Delay(ssw_gr_new, i, i+DELAY_GRTXT_FALLING*2)
            delayed_segb = Delay(compute_segb(1-ssw_gr_new, vb, current_lohi), i, i+DELAY_SEGB*2)

        if ssw_lohi[i] == 1 and ssw_lohi[i-1] == 0:
            # Rising LOHI
            print("Rising LOHI")
            DELAY_SEGB=1*14+6
            delayed_segb = Delay(compute_segb(1-current_gr_txt, vb, ssw_lohi[i]), i, i+DELAY_SEGB*2)
        elif ssw_lohi[i] == 0 and ssw_lohi[i-1] == 1:
            # Falling LOHI
            print("Falling LOHI")
            DELAY_SEGB=1*14+6.5
            delayed_segb = Delay(compute_segb(1-current_gr_txt, vb, ssw_lohi[i]), i, i+DELAY_SEGB*2)


        if ssw_col80[i] == 1 and ssw_col80[i-1] == 0:
            # Rising col80
            print("Rising col80")
            DELAY_COL80_RISING=11.5
            delayed_col80 = Delay(ssw_col80[i], i, i+DELAY_COL80_RISING*2)
        elif ssw_col80[i] == 0 and ssw_col80[i-1] == 1:
            # Falling col80
            print("Falling col80")
            DELAY_COL80_FALLING=11
            delayed_col80 = Delay(ssw_col80[i], i, i+DELAY_COL80_FALLING*2)


        print(f"Step {i/2} 14M: current_gr_txt={current_gr_txt}, delayed GR/TXT={delayed_gr_txt}, delayed SEGB={delayed_segb}, delayed LO/HI={delayed_lohi}")

        if i >= delayed_gr_txt.future_time:
            current_gr_txt = delayed_gr_txt.value

        if i >= delayed_lohi.future_time:
            current_lohi = delayed_lohi.value

        if i >= delayed_segb.future_time:
            current_segb = delayed_segb.value

        if i >= delayed_col80.future_time:
            current_col80 = delayed_col80.value

        # PAL/HAL inputs (pay attention to the inverted ones)
        SIG_7M = ((i//2) % 2) == 1
        SIG_14M = (i+1)%2
        # GR = not TEXT = not 1
        # => GRp = TEXT = 1
        GR2p = current_gr_txt == 1
        SEGB = current_segb
        COL80p = not (current_col80 == 1)


        append_value( series, "GR/TXT", ssw_gr[i])
        append_value( series, "LO/HI", ssw_lohi[i])
        append_value( series, "COL80", ssw_col80[i])
        append_value( series, "COL80'", COL80p)
        append_value( series, "GR+2'", GR2p)
        append_value( series, "7M", SIG_7M)
        append_value( series, "14M", SIG_14M)
        append_value( series, "SEGB", SEGB)
        append_value( series, "VID7M_S1", VID7M_S1)
        append_value( series, "VID7M_S2", VID7M_S2)
        append_value( series, "VID7M_S3", VID7M_S3)
        append_value( series, "VID7M", 1-VID7M)

        if i % 2 == 1:
            VID7M_S1 = not GR2p and SEGB
            VID7M_S2 = GR2p and not (COL80p)
            VID7M_S3 = GR2p and SIG_7M
            VID7M_T123 = not VID7M
            VID7M = VID7M_S1 or VID7M_S2 or VID7M_S3 or VID7M_T123


    return series

xlimits = None

for scenario in [3]:
    print("-"*80)
    ssw_txt_gr = np.array([0]*10)
    ssw_lohi = np.array([0]*10)
    ssw_col80 = np.array([0]*10)

    ssw_txt_gr = np.repeat(ssw_txt_gr, 14*2)
    ssw_lohi = np.repeat(ssw_lohi, 14*2)
    ssw_col80 = np.repeat(ssw_col80, 14*2)

    """
    GR/TXT changes from zero (GR) to one (TXT)
    GR+1, GR+2 will go from one to zero.
    """
    match scenario:
        case 0 | 1:
            # GR+2' falls 1 --> 0, VB=1 and HIRES' = 1 => SEGB becomes HIRES' = 1
            # To simulate this:
            # 1/ Set the delay over SEGB so that SEGB changes at the right time
            # 2/ Set GR+2 delay to the same as SEGB so that VID7M reacts as meaured
            #  (the idea is that GR+2' and SEGB rise/fall time are slightly
            # different in reality and accounting for the is difficult. The easiest
            # way is just to choose SEGB as the "true" delay and fiddle with GR+2
            # to repdocuce the behaviour of VID7M --- without trying to get the
            # delay fof GR+2' right, which is too difficult).
            ssw_txt_gr[:] = 1
            ssw_txt_gr[14*2+1:70*2+1] = 0
            ssw_lohi[:] = 0

            vb = 1 - scenario
            color= scenario

        # Switch HIRES/LORES
        case 2:
            ssw_txt_gr[:] = 0
            ssw_lohi[:] = 1
            ssw_lohi[14*2*2+1:70*2+1] = 0
            vb = 0
            color=1

        # Switch COL80
        case 3:
            ssw_txt_gr[:] = 1
            ssw_col80[14*2*2+1:70*2+1] = 1
            vb = 0
            color=1

    series = compute(ssw_txt_gr, ssw_lohi, ssw_col80, vb)
    plot_series(series, experiment=color)
if xlimits is not None:
    plt.xlim(xlimits)
plt.xticks(range(0,len(ssw_txt_gr),14*2), range(0,len(ssw_txt_gr)//2,14))
plt.grid()
plt.show()
