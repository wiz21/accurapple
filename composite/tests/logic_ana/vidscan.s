!to "VID_SCAN.bin", plain
!convtab "apple2c.ct"

* =   $6000
!source "equs.s"
	FLOATING_BUS = $C070
	hgr_ofs_base = $F0
	counter1 = $EF
	counter2 = $EE
	floating_bus_reads_ptr = $F0
	FIRST_LINE = 16
	NB_DISPLAYED_LINES = 20
run_frame:
	JSR clear_video_ram
	LDA #LETTER_SPACE
	LDY #$04
	LDX #4
	JSR page_fill

	+switch_hgr

	LDA #0
	STA counter
	STA counter+1

	LDA #<$2000
	STA hgr_ofs_base
	LDA #>$2000
	STA hgr_ofs_base+1
fill:
	LDA counter
	LDY #0
!for i,0,7 {
	STA (hgr_ofs_base),Y
	INY
}
	LDA counter+1
!for i,0,7 {
	STA (hgr_ofs_base),Y
	INY
}

	+inc16 counter
	+add_const16 hgr_ofs_base, 16
	LDA hgr_ofs_base + 1
	CMP #$40
	BNE fill



	+wait_redraw
smc_pause_hi = * + 1
smc_pause_lo = * + 3
	+wait_cycles2 65*FIRST_LINE

	VALUES_PER_LINE=13

!for i,0,VALUES_PER_LINE*NB_DISPLAYED_LINES-1 {
	LDA FLOATING_BUS	; 4c
	STA floating_bus_reads + i 	; 4c
}

;;; ===========================================================================
;;; Display results
;;; ===========================================================================

	+switch_txt40


	LDA #<floating_bus_reads
	STA smc_display
	LDA #>floating_bus_reads
	STA smc_display + 1
	LDA #0
	STA counter2

print_all_lines:
	LDA counter2
	CLC
	ROL
	TAX
	LDA text_lines_ofs,X
	STA $FC
	INX
	LDA text_lines_ofs,X
	STA $FD

	LDA #VALUES_PER_LINE
	STA counter1

print_line:
smc_display = * + 1
	LDX $1234
	JSR print_byte_in_X
	+add_const16 $FC, 3
	+inc16 smc_display
	DEC counter1
	BNE print_line

	INC counter2
	LDA counter2
	CMP #NB_DISPLAYED_LINES
	BNE print_all_lines


!for i,0,VALUES_PER_LINE-1 {
	LDY #i
	LDA (floating_bus_reads_ptr), Y
	TAX
}

	+print_one_text_at "LINE:   <-/-> TO MOVE THE START LINE", 0, 23
	;; LDX current_line + 1
	;; +print_byte $7D0 + 6
	;; LDX current_line
	;; +print_byte $7D0 + 7

	NUMBER = $E0
	LDA current_line
	STA NUMBER
	LDA current_line+1
	STA NUMBER+1
	LDA #0
	STA NUMBER+2
	STA NUMBER+3

	LDX #NUMBER
	LDY #0
	LDA #<$7D5
	STA PRINTNUM_SMC
	LDA #>$7D5
	STA PRINTNUM_SMC+1
	JSR PRINTDEC


	LDA $C010		; Clear the keyboard
wait_key:
	LDA $C000		; 4c
	BPL wait_key		; 2c
	AND #127

	CMP #KEY_LEFT
	BEQ key_left
	CMP #KEY_RIGHT
	BEQ key_right
	JMP run_frame

	STEP_LINES = 16
	STEP_CYCLES = 65*STEP_LINES
key_left:
	SEC
	LDA smc_pause_lo
	SBC #<STEP_CYCLES
	STA smc_pause_lo
	LDA smc_pause_hi
	SBC #>STEP_CYCLES
	STA smc_pause_hi
	+sub_const16 current_line, STEP_LINES
	JMP run_frame
key_right:
	CLC
	LDA smc_pause_lo
	ADC #<STEP_CYCLES
	STA smc_pause_lo
	LDA smc_pause_hi
	ADC #>STEP_CYCLES
	STA smc_pause_hi
	+add_const16 current_line, STEP_LINES
	JMP run_frame

floating_bus_reads:
	!fill VALUES_PER_LINE*NB_DISPLAYED_LINES, 0
counter	!word 0
current_line !word FIRST_LINE



;; * Print up to 32-bit unsigned decimal number
;; ********************************************
;; * See forum.6502.org/viewtopic.php?f=2&t=4894
;; * and groups.google.com/g/comp.sys.apple2/c/_y27d_TxDHA
;; *
;; * On entry:
;; *  X=>base of four-byte zero page locations
;; *  Y= number of digits to pad to, 0 for no padding
;; * Can print fewer than 32 bits by setting higher bytes
;; * to zero and setting Y appropriately
;; *
;; * On exit:
;; *  The four bytes at 0,X to 3,X are set to zero
;; *  X=preserved
;; *  A,Y corrupted
;; *
;; * Needs OSPAD  = to hold pad count
;; *       OSTEMP = bit counter
;; *       OSWRCH = routine to display a character
;; *

	OSPAD = $FC
	OSTEMP = $FD

PRINTDEC    sty   OSPAD      ; Number of padding+digits
            ldy   #0         ; Digit counter
PRDECDIGIT  lda   #32        ; 32-bit divide
            sta   OSTEMP
            lda   #0         ; Remainder=0
            clv              ; V=0 means div result = 0
PRDECDIV10  cmp   #10/2      ; Calculate OSNUM/10
            bcc   PRDEC10
            sbc   #10/2+$80  ; Remove digit & set V=1 to show div result > 0
            sec              ; Shift 1 into div result
PRDEC10     rol   0,x        ; Shift /10 result into OSNUM
            rol   1,x
            rol   2,x
            rol   3,x
            rol             ; Shift bits of input into acc (input mod 10)
            dec   OSTEMP
            bne   PRDECDIV10 ; Continue 32-bit divide
            ora   #48
            pha              ; Push low digit 0-9 to print
            iny
            bvs   PRDECDIGIT ; If V=1, result of /10 was > 0 & do next digit
            lda   #32
PRDECLP1    cpy   OSPAD
            bcs   PRDECLP2   ; Enough padding pushed
            pha              ; Push leading space characters
            iny
            bne   PRDECLP1
PRDECLP2    pla              ; Pop character left to right
            jsr   OSWRCH     ; Print it
            dey
            bne   PRDECLP2
            rts
OSWRCH:
PRINTNUM_SMC = * + 1
	STA $400
	+inc16 PRINTNUM_SMC
	RTS
