!to "LAGRTXT.bin", plain
!convtab "apple2c.ct"

* =   $6000

!source "equs.s"


BASCALC = $FBC1
BASL = $28
BASH = $29
HEXPRINT = $FDDA ; Apple2e ROM ? A = byte to display
COUT = $FDED
COUT1 = $FDF0
;; See http://www.applelogic.org/files/AIIMP.pdf (A2 monitor peeled)
COUTZ = $FDF6
HOME = $FC58
LINEFEED= $FC66
SCROLL_UP = $FC70
VTAB=$FC22
PRBYTE = $FDDA
PRBL2 = $F94A ;X=how many spaces


INIT:
    STA STORE80ON		; 80STORE set
    LDA HIRES
    BIT PAGE1
    LDA #0
    LDY #$04
    LDX #4
    JSR page_fill
    LDA #0
    LDY #$08
    LDX #4
    JSR page_fill

    BIT PAGE2
    LDA #0
    LDY #$04
    LDX #4
    JSR page_fill
    LDA #0
    LDY #$08
    LDX #4
    JSR page_fill

    BIT PAGE1
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

    BIT PAGE2
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

    BIT PAGE1
	JSR enable_vbl
	SEI

	+print_text_at screen_80col, 0, 20


	STA ALTCHARSETOFF
	LDA MIXEDON
	JSR fill_text_screen
	JSR hgrgr_bytes_copy


	;;  GR
	LDA TEXTOFF ; 4C
	LDA LORES   ; 4
	STA COL80OFF
	LDA AN3ON
	LDA PAGE1

	;;  TXT
	LDA TEXTON ; 4C
	LDA LORES   ; 4
	STA COL80OFF
	LDA AN3ON
	LDA PAGE1

	+wait_redraw

	;; -------------------------------------------------------------
vbl1:
	;; At this point we're at the beginning of the frame.
	;; on the very first line to draw.
	;; I wait to get into the visible zone.

	; Display some progress and prepare line test. This costs 1 scan line
	; Then wait a few lines so that we can clearly see what happens in the
	; visible parts of the "£" and "_" characters.

	;; I wait this line because it is a nice pattern in GR (1100)
	;; and it is a nice pattern on both text lines (because the carh is L)
	;; and the L works on any charset.
	FIRST_GOOD_LINE = 12*8
	+short_wait FIRST_GOOD_LINE*65
	; Give some visual feed back that this program is looping correctly

	;; We trigger the LA on A11 = 1 becaue $C000 = $8000 + $4000
	;; and because $6000 = $4000 + $2000 (so no $8000)
	JSR eight_lines	; 65*2
	JSR eight_lines	; 65*2
	+wait_cycles2  65*4
	JSR eight_lines2	; 65*4
	JSR eight_lines3	; 65*4

	;; ---------------------------------------------------------------

	; -3 for the JMP
	;
	+wait_cycles2  20280 - FIRST_GOOD_LINE*65 - 2*2*65 - 4*65 - 4*65 -4*65 - 3

	JMP vbl1 ; 3 cycles

!align 255, 0
eight_lines !zone {

	; Apply visual pattern to eight successive lines MINUS ONE CYCLE.
	; Minus one cycle because it allows the visual pattern to be shifted
	; by 14 pixels when this routine is called several times in a row

	;;  -6 for the JSR
	+short_wait 65 - 6

.SYNCLOOP:
	; These 25 cycles are in the HBL
	BIT   TEXTON       ;     24 25 26 27
	+short_wait 30


	; Now the visible cycles
	BIT   TEXTOFF       ;     24 25 26 27
	+short_wait 4
	BIT   TEXTON

	+short_wait 4
	BIT   TEXTOFF

	+short_wait 65-54 - 6

	RTS
}

eight_lines2 !zone {
	;;  -6 for the JSR
	+short_wait 65 - 6

	!for ndx, 1, 2 {

	; These 25 cycles are in the HBL
	BIT LORES       ;     24 25 26 27
	BIT HIRES
	+short_wait 18

	; Now the visible cycles
	BIT   LORES       ;     24 25 26 27
	BIT   HIRES
	+short_wait 16

	BIT   LORES
	+short_wait 11
}

	+short_wait 65 - 6

	RTS
}

!align 255, 0
eight_lines3 !zone {
	;;  -6 for the JSR
	+short_wait 65 - 6 - 4
	BIT   TEXTON       ;     24 25 26 27

	!for ndx, 1, 2 {

	; These 25 cycles are in the HBL
	STA COL80OFF       ;     24 25 26 27
	+short_wait 45

	; Now the visible cycles
	STA COL80ON       ;     24 25 26 27
	STA COL80OFF
	+short_wait 8
}
	BIT   TEXTOFF       ;     24 25 26 27

	+short_wait 65 - 6 - 4

	RTS
}


fill_text_screen !zone {
	LDA #1
	STA CV

	LDA #0
	STA CH

.new_line:
	LDA CV
	CLC
	ROL
	TAY
	LDA text_lines_ofs,Y
	STA .smc1+1
	INY
	LDA text_lines_ofs,Y
	STA .smc1+1+1

    LDA CV
    CLC
    AND #15
    STA .color
    +RROR
    +RROR
    +RROR
    +RROR
    ORA .color
    STA .color

.write_line:
    LDY CH
    LDA .color
.smc1	STA $0400,Y

	INC CH
	LDA CH
	CMP #40
	BNE .write_line
	LDA #0
	STA CH
.no_wrap_ch:

	INC CV
	LDA CV
	CMP #19
	BEQ .done_fill
	JMP .new_line
.done_fill:
	RTS

.color  !byte 0
}
GRHGR_GR_LINE_ADDR = $06A8
GRHGR_HGR_LINE_ADDR = $26A8
GRHGR_GR_LINE_ADDR2 = $06A8
GRHGR_HGR_LINE_ADDR2 = $2AA8

hgrgr_bytes_copy !zone {
	LDX #0
.loop:
	LDA grhgr_gr,X
	STA GRHGR_GR_LINE_ADDR,X
	STA GRHGR_GR_LINE_ADDR2,X
	LDA grhgr_hgr,X
	STA GRHGR_HGR_LINE_ADDR,X
	STA GRHGR_HGR_LINE_ADDR2,X
	INX
	CPX #40
	BNE .loop
	RTS
}

	!align 255,0
screen_80colb:
	!source "thedate.txt"
screen_80col:
	!text "[This test shows GR and TXT modes      ]"
    !text "[                                      ]"
    !text "0123456789012345678901234567890123456789"
    !text "[                                      "
	!byte 0

grhgr_gr !byte $66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$FF,$66,$66,$00,$98,$79,$98,$AA,$98,$BB,$98,$CC,$98,$DD,$98,$EE,$98,$FF,$A9,$16
grhgr_hgr !byte $49,$88,$88,$89,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00,$88,$00,$D9,$00,$88,$00
