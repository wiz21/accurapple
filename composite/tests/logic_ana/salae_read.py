from pathlib import Path
import array
import struct
import sys
from collections import namedtuple
import numpy as np

# For Siglent CSV
TCOL = "Time [s]"

class Scenario:
    def __init__(self, charts, charts_titles, cursors=[], renames={}, measures=[]):
        self.charts = charts
        self.charts_titles = charts_titles
        self.cursors = cursors
        self.measures = measures
        self.columns = None
        self.renames = renames

TYPE_DIGITAL = 0
TYPE_ANALOG = 1
expected_version = 0

DigitalData = namedtuple('DigitalData', ('initial_state', 'begin_time', 'end_time', 'num_transitions', 'transition_times'))

def parse_digital(f):
    # Parse header
    identifier = f.read(8)
    if identifier != b"<SALEAE>":
        raise Exception("Not a saleae file")

    version, datatype = struct.unpack('=ii', f.read(8))

    if version != expected_version or datatype != TYPE_DIGITAL:
        raise Exception("Unexpected data type: {}".format(datatype))

    # Parse digital-specific data
    initial_state, begin_time, end_time, num_transitions = struct.unpack('=iddq', f.read(28))

    # Parse transition times
    transition_times = array.array('d')
    transition_times.fromfile(f, num_transitions)

    return DigitalData(initial_state, begin_time, end_time, num_transitions, transition_times)

class CloseTo:
    """ Memorize a point that is close to a given data value.
    The nearest actual value will be found automatically.
    This allows one to be a bit flacky when choosing a coordinate.
    """
    def __init__(self, col: str, t: float, delta: float=0):
        """
        :param col: Name of the column to look the time in
        :param t: The time to look for
        :param delta:
        """
        self.column = col
        self.time = t
        self.delta = delta

def find_nearest(df, col, value):
    r = df.iloc[(df[col]-value).abs().argsort()[:1]]["Time [s]"].values[0]
    #print(value, r)
    return r

def find_nearest_transition(df, col, time):
    #print(df)
    #print(df.at[df.index.min(),col])
    tx = np.diff(df[col].values, prepend=df.at[df.index.min(),col])
    times = df["Time [s]"].values[tx != 0]
    if len(times) ==0:
        print("Empty?")
        return None
    idx = (np.abs(times - time)).argmin()
    r = times[idx]
    #print(f"Close to {col}: {time} is {r:.4f}") # in {times.tolist()}")
    return r


def cut(df, col, time, time2):
    """ Extract [time, time2[ (in `col`) rows from dataframe.
    The `col` column is assumed to be sorted. And the index must be
    ordered like `col`.
    """
    #print(time, time2)
    assert time < time2, f"{time} < ? {time2}"
    times = df[col].values

    ndx = df[col].values.searchsorted(time)
    # plt.plot(times)
    # plt.plot(df.index)
    # plt.show()
    # print(ndx)
    # print(df.iloc[ndx-3:ndx+3])
    # print()
    df = df.drop(df.index[:ndx-1])
    assert time >= df.at[ndx-1, col], f"Cut position is too far ahead {time} >= {df.at[ndx-1, col]}"
    df.at[ndx-1, col] = time
    # print(df)

    # print("-"*80)
    # print(time2)
    assert time2 < df.at[df.index.max(), col], "Unsupported edge case"
    ndx = df[col].values.searchsorted(time2)
    # print(ndx)
    # print(df.iloc[ndx-3:ndx+3])
    # print()
    df = df.drop(df.index[ndx+1:])
    assert time2 >= df.at[df.index.max()-1, col], "Cut position is too far ahead"
    df.at[df.index.max(), col] = time2
    # print(df)
    return df


def read_csvs(files):
    for f in files:
        print(f)

    csvs = [pd.read_csv(f) for f in files]

    tcol = set()
    for csv in csvs:
        tcol.add(csv.columns[0])

    assert len(tcol) == 1, f"The first column of all CSV must have the same name in all CSVs... I get {tcol}"
    tcol = tcol.pop()
    print(f"Time column is \"{tcol}\"")

    r = pd.concat(csvs, ignore_index=True)
    columns=list(sorted(r.columns))
    del columns[columns.index(tcol)]
    print(f"columns are: { ','.join(columns)}")

    d = dict()
    d["files"] = files
    d["time_column"] = tcol
    d["columns"] = columns

    import json
    print(json.dumps(d, indent=True))


if __name__ == '__main__':
    import argparse
    import pandas as pd
    from matplotlib import pyplot as plt
    pd.set_option('display.float_format', '{:.10f}'.format)

    parser = argparse.ArgumentParser(
                    prog='ProgramName',
                    description='What the program does',
                    epilog='Text at the bottom of help')
    parser.add_argument("filename", nargs="*")
    parser.add_argument("--all", action="store_true")
    parser.add_argument("-s", type=int, default=1, help="Scenario")
    parser.add_argument("--init-prj", action="store_true")
    parser.add_argument("--plot", action="store_true")

    args = parser.parse_args()

    if args.init_prj:
        read_csvs(args.filename)
        exit()

    if args.plot:
        print(f"loading {args.filename}")

        def load_siglent_csv(filename) -> pd.DataFrame:
            r = pd.read_csv(filename, index_col=0)

            COLUMNS=list(sorted(r.columns))
            print("Available columns:", ", ".join(COLUMNS))
            #COLUMNS.remove(TCOL)

            # From seconds to milliseconds
            r.index *= 1_000_000
            # r = r.sort_values(by=TCOL)
            # for c in COLUMNS:
            #     r[c] = r[c].ffill()
            # # MAke sure the index follow the time column
            # r.reset_index(inplace=True)
            #r.set_index(TCOL, inplace=True)
            #print(r)
            return r

        r = load_siglent_csv(args.filename[0])

        fig, axes = plt.subplots(nrows=len(r.columns), ncols=1)
        for ndx, column in enumerate(r.columns):
            axes[ndx].step(r.index, r[column], where="post", label=column)
            axes[ndx].set_xlabel(f"µ-sec")
            axes[ndx].legend(loc="upper right")
        plt.suptitle(args.filename[0])
        plt.show()
        exit()



    FREQ = 14.238 # MegaHertz
    COLORS = ["blue","red","green"]
    PATTERNS = ["solid","dashed"]
    SPACE=1.5
    SHIFT_SCALE=0.95


    BASE = Path(".") / "lagr"
    FILES = [BASE / r"ls166.csv", BASE / r"palhal.csv", BASE / r"palhal2.csv", BASE / r"vidrom.csv"]

    ALL_SCENARIOS = [
        Scenario(
            [[("GR regular", CloseTo(TCOL, 0 , 0), CloseTo(TCOL, 65, 0))]],
            ["Regular GR - Around line 2"]),
        Scenario(
            [[("HGR regular", CloseTo(TCOL, 270 , 0), CloseTo(TCOL, 270, 65))]],
            ["Regular HGR - Around line 4"],
            cursors=[("First dot", CloseTo("LS166-OUT",283.82))]),
        Scenario(
            [[("HGR undelayed", CloseTo(TCOL, 475 , 0), CloseTo(TCOL, 502, 0))]],
            ["HGR Undelayed - Around line 8"],
            cursors=[("First dot", CloseTo("LS166-OUT",475.8))],
            renames={"PAL/HAL-COL80":"PAL/HAL-80COL'"}),
        Scenario(
            [[("HGR <-> GR", CloseTo(TCOL, 1110 , 0), CloseTo(TCOL, 1145, 0))]],
            ["HGR <-> GR - Around line 17"],
            cursors=[("First dot", CloseTo("LS166-OUT",1113.918))]),
        Scenario(
            [[("HGR regular <-> HGR undelayed", CloseTo(TCOL, 1607 , 0), CloseTo(TCOL, 1671, 0))]],
            ["HGR regular <-> HGR undelayed - Around line 15"],
            cursors=[("First dot", CloseTo("LS166-OUT",1623.9))],
            measures=[(CloseTo("6502-A15",1624.5), CloseTo("PAL/HAL-GR+2'", 1625.3))]
        ),
        Scenario(
            [[("HGR <-> TXT40", CloseTo(TCOL, 3090 , 0), CloseTo(TCOL, 3130, 0))]],
            ["HGR <-> TXT40 - Line 48-51"],
            cursors=[("GR change", CloseTo("PAL/HAL-GR+2'",3099))],
            measures=[(CloseTo("6502-A15",3096.5), CloseTo("PAL/HAL-GR+2'", 3099))]
        )
    ]

    print("\nAvailable scenarios")
    print("-------------------\n")
    for ndx, current_scenario in enumerate(ALL_SCENARIOS):
        print(f"[{ndx+1: 2}] {current_scenario.charts_titles}")
    print()


    # FILES = ["palhal.csv", "palhal2.csv", "ls166.csv", "vidrom.csv"]
    # COLUMNS = ["6502 A15", "PAL/HAL-14M", "PAL/HAL-7M", "PAL/HAL-PHASE0","PAL/HAL-H0", "PAL/HAL-GR+2'", "PAL/HAL-SEGB", "PAL/HAL-VID7M",
    #            "PAL/HAL-LDPS", "PAL/HAL-COL80'", "LS166-LDPS", "VIDROM-GR2", "VIDROM-SEGB", "VIDROM-SEGA"]

    if args.filename:
        FILES = args.filename

    print(f"Reading files {', '.join([str(f) for f in FILES])}")
    TRIGGER_COL="6502-A15"
    COLUMNS = ["6502-A14","6502-A15","VIDROM-RA9","VIDROM-GR","VIDROM-A10","VIDROM-SEGA","VIDROM-SEGB","VIDROM-SEGC"]

    CHARTS = None
    MEASURES =[]

    if False:
        CHARTS = [
            [("Line1", CloseTo(TRIGGER_COL, 1516, -0.1), CloseTo(TRIGGER_COL, 1792, 0))],
        ]
        CHARTS_TITLES = ["HGR <-> HGR undelayed"]
        MEASURES =[
            [ (CloseTo(TRIGGER_COL, 44.44), CloseTo("PAL/HAL-GR+2'", 45.25)),
            (CloseTo(TRIGGER_COL, 51.32), CloseTo("PAL/HAL-GR+2'", 52.134))]
            ]

    if False:
        CHARTS = [
            [("HGR undelayed", CloseTo(TRIGGER_COL, 384 , -0.1), CloseTo(TRIGGER_COL, 640, 0))],
        ]
        CHARTS_TITLES = ["HGR undelayed - Around line 7"]
        MEASURES =[]

    if False:
        CHARTS = [
            [("GR regular", CloseTo(TCOL, 0 , 0), CloseTo(TCOL, 65, 0))]
        ]

        CHARTS_TITLES = ["GR - Around line 2"]
        MEASURES =[]
        COLUMNS = None

    if args.s:
        current_scenario = ALL_SCENARIOS[args.s-1]
        COLUMNS=current_scenario.columns
        CHARTS = current_scenario.charts
        CHARTS_TITLES = current_scenario.charts_titles
        MEASURES = current_scenario.measures
    else:
        current_scenario = None


    if args.all:
        COLUMNS=None
        CHARTS = None
        MEASURES =None

    INVERTED = []


    r = pd.concat([pd.read_csv(f) for f in FILES], ignore_index=True)

    if current_scenario is not None and current_scenario.renames:
        print(f"Renames {current_scenario.renames}")
        print(r.columns)
        r.rename(columns=current_scenario.renames, inplace=True)

    if not COLUMNS:
        COLUMNS=list(sorted(r.columns))
        print("Available columns:", ", ".join(COLUMNS))
        COLUMNS.remove(TCOL)



    r[TCOL] *= 1_000_000
    r = r.sort_values(by=TCOL)
    for c in COLUMNS:
        r[c] = r[c].ffill()
        if c in INVERTED :
            r[c] = 1-r[c]
    # MAke sure the index follow the time column
    r.reset_index(inplace=True)


    if False:
        CHARTS = [
            [("Line1", CloseTo(TRIGGER_COL, 511, -0.1), CloseTo(TRIGGER_COL, 511, 63)),
             ("Line2", CloseTo(TRIGGER_COL, 576, -0.1), CloseTo(TRIGGER_COL, 576, 63))],
            [("Line1", -0.1, 63),
             ("Line2", CloseTo(TRIGGER_COL, 128, -0.1), CloseTo(TRIGGER_COL, 128, 63))],
            [("Line1", CloseTo(TRIGGER_COL, 767, -0.1), CloseTo(TRIGGER_COL, 767, 63)),
             ("Line2", CloseTo(TRIGGER_COL, 829, -0.1), CloseTo(TRIGGER_COL, 829, 63))],
        ]
        CHARTS_TITLES = ["Switch HIRES/LORES","Switch GR/TXT","Switch COL80"]
        # Times are relative to charts, not absolute time of measurements, so
        # it's easier to locate them.
        MEASURES = [
            [ (CloseTo(TRIGGER_COL, 25.643), CloseTo("PAL/HAL-SEGB", 27.086)),
              (CloseTo(TRIGGER_COL, 29.58), CloseTo("PAL/HAL-SEGB", 31.07))],
            [ (CloseTo(TRIGGER_COL, 33.5), CloseTo("PAL/HAL-SEGB", 35.926)),
              (CloseTo(TRIGGER_COL, 41.364), CloseTo("PAL/HAL-SEGB", 43.744))],
            [ (CloseTo(TRIGGER_COL, 48.24), CloseTo("PAL/HAL-COL80'", 49.04)),
              (CloseTo(TRIGGER_COL, 52.13), CloseTo("PAL/HAL-COL80'", 52.96))
             ],
        ]



    if False:
        CHARTS = None
        MEASURES = None

    DRAW_GRID=False

    if not CHARTS:
        CHARTS = [
            [("Line1", CloseTo(TRIGGER_COL, 0, -0.1), CloseTo(TRIGGER_COL, r[TCOL].max(), 3))]
        ]
        CHARTS_TITLES = ["All data"]
        DRAW_GRID=False


    fig, axes = plt.subplots(nrows=len(CHARTS), ncols=1)
    for chart_ndx, chart_parts in enumerate(CHARTS):
        if len(CHARTS) == 1:
            ax = axes
        else:
            ax = axes[chart_ndx]

        length = None
        drawn_parts = []
        for ndx_part, chart_part in enumerate(chart_parts):
            part_label, part_begin, part_end = chart_part

            if isinstance(part_begin, CloseTo):
                part_begin = find_nearest_transition(r, part_begin.column, part_begin.time) + part_begin.delta
            if isinstance(part_end, CloseTo):
                part_end = find_nearest_transition(r, part_end.column, part_end.time) + part_end.delta

            part = cut(r.copy(), TCOL, part_begin, part_end)
            #part[TCOL] -= part[TCOL].min()
            drawn_parts.append(part)

            if length is None:
                length = part[TCOL].max()
            else:
                length = max(length, part[TCOL].max())

            for ndx_col, column in enumerate(COLUMNS):
                ax.step(part[TCOL], ndx_col*SPACE + part[column],
                        where="post", color=COLORS[ndx_part], label=part_label,
                        alpha=0.7, linestyle=PATTERNS[ndx_part])

        if MEASURES:
            for measure in MEASURES:
                for part in drawn_parts:
                    start, stop = measure
                    ta = find_nearest_transition(part, start.column, start.time)
                    tb = find_nearest_transition(part, stop.column, stop.time)
                    if ta is not None and tb is not None:
                        y = COLUMNS.index(start.column) * SPACE + 0.5
                        y2 = COLUMNS.index(stop.column) * SPACE + 0.5
                        ax.plot( (ta,tb),(y,y2), color="black")
                        d = round((tb-ta)*FREQ)
                        d_str = f"{int(d/FREQ)} c.-{d%FREQ:.1f}"
                        ax.text( tb, y2, f"{d_str}" )
                        break

        ax.set_yticks(0.5 + np.arange(len(COLUMNS))*SPACE, COLUMNS)
        ax.set_xlabel(f"µ-sec")
        ax.set_title(f"{CHARTS_TITLES[chart_ndx]}")

        if DRAW_GRID:
            m = round(length / FREQ)*FREQ
            minor_ticks = np.linspace(0, m, int(m/(1/FREQ)))
            ax.set_xticks(minor_ticks, minor=True)

    if current_scenario is not None:
        for clabel, cx in current_scenario.cursors:
            if isinstance(cx, CloseTo):
                cx = find_nearest_transition(r, cx.column, cx.time)
            plt.axvline(cx, color="red")
            plt.text(cx+0.1, 0.0, clabel, rotation=90, va='center')

    # if len(CHARTS) >= 2:
    #     axes[0].set_xlim(28.47,28.47+5)
    #     axes[1].set_xlim(32.4,32.4+5)

    #plt.tight_layout()
    def on_resize(event):
        fig.tight_layout()
        fig.canvas.draw()

    cid = fig.canvas.mpl_connect('resize_event', on_resize)
    plt.show()
