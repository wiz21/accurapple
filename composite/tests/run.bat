@echo off
set APPLE_COMMANDER=java -jar AppleCommander-1.3.5.13-ac.jar
set ACCURAPPLE_DIR=..\..
del NEW.DSK
copy BLANK_PRODOS2.DSK NEW.DSK
REM c:\users\stephanec\opt\acme hgr_text.s
REM python ..\..\generate_text_gr.py
python ..\..\utils\make_convtab.py

python %ACCURAPPLE_DIR%\utils\print_date.py
REM GOTO :done

REM for /f "delims=" %%k in ('python -c "from datetime import datetime; print(datetime.now().strftime('%%Y-%%m-%%d %%H:%%M'))"') do (
REM     echo !text "%%k" > thedate.txt
REM )
REM c:\users\stephanec\opt\acme scott.s
REM c:\users\stephanec\opt\acme test_dhgr.s
REM c:\users\stephanec\opt\acme -r hgr_text.lst hgr_text.s
REM c:\users\stephanec\opt\acme -r grtxt.lst good_tests/grtxt.s
REM c:\users\stephanec\opt\acme -r grtxt_mono.lst good_tests/grtxt_mono.s

c:\users\stephanec\opt\acme -I %ACCURAPPLE_DIR%\utils -r text_40_80.lst  txt_40_80\txt4080.s
c:\users\stephanec\opt\acme -I %ACCURAPPLE_DIR%\utils txt_40_80\allmodes.s
c:\users\stephanec\opt\acme -I %ACCURAPPLE_DIR%\utils txt_40_80\gr_dgr.s
c:\users\stephanec\opt\acme -I %ACCURAPPLE_DIR%\utils txt_40_80\gr_txt.s
c:\users\stephanec\opt\acme -I %ACCURAPPLE_DIR%\utils txt_40_80/hgr_txt80.s
c:\users\stephanec\opt\acme -I %ACCURAPPLE_DIR%\utils logic_ana/la_gr_txt.s
c:\users\stephanec\opt\acme -I %ACCURAPPLE_DIR%\utils logic_ana/lagr/la_gr.s

REM DEL thedate.txt

@REM %APPLE_COMMANDER% -p NEW.DSK GRHGR BIN 0x6000 < scott.bin
@REM %APPLE_COMMANDER% -p NEW.DSK DHGR BIN 0x6000 < test_dhgr.bin
@REM %APPLE_COMMANDER% -p NEW.DSK HGRTEXT BIN 0x6000 < hgrtxt.bin
@REM %APPLE_COMMANDER% -p NEW.DSK GRTXTMONO BIN 0x6000 < GRTXTMON.bin
@REM %APPLE_COMMANDER% -p NEW.DSK GRTXT BIN 0x6000 < GRTXT.bin

REM New tests
@REM %APPLE_COMMANDER% -p NEW.DSK TXT4080 BIN 0x6000 < TXT4080.bin
%APPLE_COMMANDER% -p NEW.DSK ALLMODES BIN 0x6000 < ALLMODES.bin
@REM %APPLE_COMMANDER% -p NEW.DSK GRDGR BIN 0x6000 < DHGRDGR.bin
%APPLE_COMMANDER% -p NEW.DSK GRTXT BIN 0x6000 < GRTXT.bin
@REM %APPLE_COMMANDER% -p NEW.DSK HGRTXT80 BIN 0x6000 < HGRTXT80.bin
@REM %APPLE_COMMANDER% -p NEW.DSK LAGRTXT BIN 0x6000 < LAGRTXT.bin
%APPLE_COMMANDER% -p NEW.DSK LAGR BIN 0x6000 < LAGR.bin



REM GOTO :done

REM Get-Content scott.bin | java -jar AppleCommander-1.3.5.jar -p NEW.DSK START BIN 0x6000

set RUST_LOG=accurapple=info
set RUST_LOG=accurapple::a2=info,%RUST_LOG%
REM $env:RUST_LOG = '%RUST_LOG%'
REM Apple IIe Video - Enhanced - 342-0265-A - 2732.bin
REM Apple IIe Video - Unenhanced - 342-0133-A - 2732.bin
REM Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin

set FRENCH=--charset "%ACCURAPPLE_DIR%/data/Apple IIe Video - Unenhanced - 342-0133-A - 2732.bin"

REM SET SIMPLE_SHADER=--a2c-shader "%ACCURAPPLE_DIR%/accurashader/src/shaders/apple_to_composite_simplified.wgsl"
SET SIMPLE_SHADER=--a2c-shader "%ACCURAPPLE_DIR%/accurashader/src/shaders/apple_to_composite_hal.wgsl"

REM %ACCURAPPLE_DIR%\target\release\accurapple.exe --floppy1 NEW.DSK --a2c-shader "%ACCURAPPLE_DIR%/accurashader/src/shaders/apple_to_composite_simplified.wgsl" --charset "%ACCURAPPLE_DIR%/data/Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin" --log-stdout --cpu F6502  --script "TURBO_START, WAIT_UNTIL 10, KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP"

REM %ACCURAPPLE_DIR%\target\release\accurapple.exe %SIMPLE_SHADER% --log-stdout --cpu F6502


REM Old tests
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe %SIMPLE_SHADER%   --floppy1 ../NEW.DSK --no-sound  %FRENCH% --log-stdout --cpu F6502 --script "SET_GFX_DEBUG_LINE 66, TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,KEY_RIGHT,KEY_RETURN, WAIT_UNTIL 14, KEY_RETURN, WAIT_UNTIL 20, SAVE_ALL_MEM allmem.bin,  WAIT_UNTIL 21, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin, TURBO_STOP"

REM All modes
REM HGR is pink blue, orange, green white
REM 16+4 = DGR line, 48+4= DHGR, 36=HGR
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe   --power-debug --floppy1 NEW.DSK --no-sound  %FRENCH% --log-stdout --cpu floooh6502 --script "SET_GFX_DEBUG_LINE 92, TURBO_START, WAIT_UNTIL 10, KEY_RIGHT,KEY_RIGHT,KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 14, KEY_RETURN, WAIT_UNTIL 20, SAVE_ALL_MEM allmem.bin,  WAIT_UNTIL 21, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin, TURBO_STOP"


rem cargo run --release --  --power-debug --floppy1 ../../additional/CC.dsk --no-sound  %FRENCH% --log-stdout --cpu floooh6502 --script "SET_GFX_DEBUG_LINE 66, TURBO_STOP, WAIT_UNTIL 13, KEY_SPACE, WAIT_UNTIL 46, TURBO_STOP, SAVE_ALL_MEM allmem.bin, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin, TURBO_STOP"

cargo run --release -- --cpu obscoz65c02  --floppy1 "..\..\additional\TRIBU.dsk"

REM TXT40 - TXT80
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe %SIMPLE_SHADER%  --floppy1 NEW.DSK --no-sound  --charset "%ACCURAPPLE_DIR%/data/Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin" --log-stdout --cpu F6502 --script "SET_GFX_DEBUG_LINE 44, TURBO_START, WAIT_UNTIL 10,  KEY_RETURN, WAIT_UNTIL 14, SAVE_ALL_MEM allmem.bin,  WAIT_UNTIL 15, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"

REM GR-DGR
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe --a2c-shader "%ACCURAPPLE_DIR%/accurashader/src/shaders/apple_to_composite.wgsl"  --floppy1 NEW.DSK --no-sound  --charset "%ACCURAPPLE_DIR%/data/Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin" --log-stdout --cpu F6502 --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,KEY_RIGHT,KEY_RIGHT,KEY_RETURN, WAIT_UNTIL 14, SAVE_ALL_MEM allmem.bin,  SET_GFX_DEBUG_LINE 30, WAIT_UNTIL 15, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe %SIMPLE_SHADER%  --floppy1 NEW.DSK --no-sound  --charset "%ACCURAPPLE_DIR%/data/Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin" --log-stdout --cpu F6502 --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,KEY_RIGHT,KEY_RIGHT,KEY_RETURN, WAIT_UNTIL 14, SAVE_ALL_MEM allmem.bin,  SET_GFX_DEBUG_LINE 30, WAIT_UNTIL 15, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"

REM CRAZY_CYCLES
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe %SIMPLE_SHADER%  --floppy1 %ACCURAPPLE_DIR%/additional/CC.DSK --no-sound   --log-stdout --cpu F6502 --script "SET_GFX_DEBUG_LINE 7, KEY_RIGHT,WAIT_UNTIL 4, TURBO_STOP, SAVE_ALL_MEM allmem.bin, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"

REM GR-HGR
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe %SIMPLE_SHADER%  --floppy1 ../NEW.DSK --no-sound   --log-stdout --cpu F6502 --script "SET_GFX_DEBUG_LINE 44,TURBO_START, WAIT_UNTIL 10,   KEY_RETURN, WAIT_UNTIL 20, SAVE_ALL_MEM allmem.bin, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"

REM GR-TXT
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe %FRENCH% %SIMPLE_SHADER%  --floppy1 NEW.DSK --no-sound   --log-stdout --cpu F6502 --script "SET_GFX_DEBUG_LINE 28, TURBO_START, WAIT_UNTIL 10, KEY_RIGHT,KEY_RIGHT,KEY_RIGHT,KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 20, SAVE_ALL_MEM allmem.bin, SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"

REM HGR-TXT80
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe %FRENCH% %SIMPLE_SHADER% --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,  KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP, SAVE_ALL_MEM allmem.bin,  SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"


REM Logic.Ana. GR-TXT
REM 97
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe %FRENCH% %SIMPLE_SHADER% --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "SET_GFX_DEBUG_LINE 101, TURBO_START, WAIT_UNTIL 10,  KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP, SAVE_ALL_MEM allmem.bin,  SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"


REM HGRSCNTXT, see expected accurapple\accurashader\experiments\good_tests\hgrscntxt\HGRTXTSCN.JPG
REM Avec ce mode, je vois les changements d'accès mémoire, TACRE.
REM Donc ça permet de voir si j'interprète le signal GR/TXT au bon moment.
REM %ACCURAPPLE_DIR%\target\release\accurapple.exe --a2c-shader "%ACCURAPPLE_DIR%/accurashader/src/shaders/apple_to_composite_simplified.wgsl"  --floppy1 ../NEW.DSK --no-sound   --log-stdout --cpu F6502 --script "SET_GFX_DEBUG_LINE 12, TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RETURN, WAIT_UNTIL 20, SAVE_ALL_MEM allmem.bin,  SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"

@ECHO OFF
REM --a2c-shader "%ACCURAPPLE_DIR%/accurashader/src/shaders/apple_to_composite_simplified.wgsl"
REM --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,  KEY_RIGHT,  KEY_RIGHT, KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP"
rem --floppy1 "%ACCURAPPLE_DIR%\accurashader\experiments\good_tests\allmodes\NEW.DSK"
REM --floppy1 NEW.DSK
REM ..\..\target\release\accurapple.exe --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "BP_PC 6000, TURBO_START, WAIT_UNTIL 10,  KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP"
REM --script "WAIT_UNTIL 13, KEY_RETURN"

REM cargo run --release -- --floppy1 NEW.DSK --script "WAIT_UNTIL 12, KEY_RETURN"

REM cargo run --release -- --floppy1 NEW.DSK --script "LOAD_MEM scott.bin $6000, SET_PC $6000"
:done