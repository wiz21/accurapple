#!/bin/bash
ADIR=../..
APPLE_COMMANDER="java -jar AppleCommander-1.3.5.13-ac.jar"

rm NEW.DSK
cp BLANK_PRODOS2.DSK NEW.DSK
chmod +w NEW.DSK

python3 $ADIR/utils/generate_text_gr.py
python3 $ADIR/utils/make_convtab.py
python3 $ADIR/utils/print_date.py

# acme scott.s
# acme test_dhgr.s
# acme -r hgr_text.lst hgr_text.s
# acme -r grtxt.lst good_tests/grtxt.s
# acme -r grtxt_mono.lst good_tests/grtxt_mono.s

acme -I $ADIR/utils txt_40_80/txt4080.s
acme -I $ADIR/utils txt_40_80/allmodes.s
acme -I $ADIR/utils txt_40_80/gr_dgr.s
acme -I $ADIR/utils txt_40_80/gr_txt.s
acme -I $ADIR/utils txt_40_80/hgr_txt80.s
acme -I $ADIR/utils logic_ana/la_gr_txt.s


# $APPLE_COMMANDER -p NEW.DSK GRHGR BIN 0x6000 < scott.bin
# $APPLE_COMMANDER -p NEW.DSK DHGR BIN 0x6000 < test_dhgr.bin
# $APPLE_COMMANDER -p NEW.DSK HGRTEXT BIN 0x6000 < hgrtxt.bin
# $APPLE_COMMANDER -p NEW.DSK GRTXTMONO BIN 0x6000 < GRTXTMON.bin
# $APPLE_COMMANDER -p NEW.DSK GRTXT BIN 0x6000 < GRTXT.bin
$APPLE_COMMANDER -p NEW.DSK TXT4080 BIN 0x6000 < TXT4080.bin
$APPLE_COMMANDER -p NEW.DSK ALLMODES BIN 0x6000 < ALLMODES.bin
$APPLE_COMMANDER -p NEW.DSK GRDGR BIN 0x6000 < DHGRDGR.bin
$APPLE_COMMANDER -p NEW.DSK GRTXT BIN 0x6000 < GRTXT.bin
$APPLE_COMMANDER -p NEW.DSK HGRTXT80 BIN 0x6000 < HGRTXT80.bin
$APPLE_COMMANDER -p NEW.DSK LAGRTXT BIN 0x6000 < LAGRTXT.bin
python3 $ADIR/utils/dsk2nic.py --source NEW.DSK --target gfxtest.nic

export RUST_LOG=accurapple=error
export RUST_LOG=accurapple::a2=info,$RUST_LOG

# TXT 40 80
$ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_simplified.wgsl" --charset "$ADIR/data/Apple IIe Video French Canadian - Unenhanced - 341-0168-A - 2732.bin"  --log-stdout --cpu floooh6502 --floppy1 NEW.DSK # --script "TURBO_START, WAIT_UNTIL 10, KEY_RETURN, WAIT_UNTIL 14"

# ALL MODES
#$ADIR/target/release/accurapple --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --rom "$ADIR/data/apple2e-enhanced.rom" --log-stdout --cpu 6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10, KEY_RIGHT, KEY_RIGHT, KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP"

# DGR GR
# $ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_simplified.wgsl" --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,  KEY_RIGHT,KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP, SAVE_ALL_MEM allmem.bin,  SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"

# HGR TXT80
# $ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_simplified.wgsl" --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,  KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP, SAVE_ALL_MEM allmem.bin,  SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"


# L.A. GR-TXT
# $ADIR/target/release/accurapple --a2c-shader "$ADIR/accurashader/src/shaders/apple_to_composite_simplified.wgsl" --charset "$ADIR/data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin" --log-stdout --cpu F6502 --floppy1 NEW.DSK --script "TURBO_START, WAIT_UNTIL 10,  KEY_RIGHT,  KEY_RIGHT,  KEY_RIGHT, KEY_RIGHT, KEY_RIGHT, KEY_RIGHT,  KEY_RETURN, WAIT_UNTIL 14, TURBO_STOP, SAVE_ALL_MEM allmem.bin,  SAVE_GFX_DEBUG gfx_debug.bin, IOU_GFX_INPUT scanner.bin"
