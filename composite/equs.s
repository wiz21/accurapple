CYCLES_PER_FRAME = 20280

	AUXMOVE = $C311
	A1L = $3C
	A1H = $3D
	A2L = $3E
	A2H = $3F
	A4L = $42
	A4H = $43

	RAMRW_D000B1  =      $c08b           ; read and write RAM in $d000-$ffff
STORE80ON     =      $C001
RAMRDOFF      =      $c002           ; turn off RAMRD
RAMRDON       =      $c003           ; turn on RAMRD
RAMWRTOFF     =      $c004           ; turn off RAMWRT
RAMWRTON      =      $c005           ; turn on RAMWRT
VERTBLANK     =      $c019           ; vertical blank
CLR80COL      =      $c000           ; disable 80 column store
KBD           =      $c000           ; read keyboard
KBDSTRB       =      $c010           ; clear keyboard strobe
VBLINTON      =      $c05b           ; enable VBL interrupt
VSYNCIRF      =      $c070           ; reset VSYNC IRF (Interrupt Flag)
IOUOFF        =      $c078           ; disable IOU access
IOUON         =      $c079           ; enable IOU access
ALTCHARSETOFF    =   $c00e	     ; Write
ALTCHARSETON    =    $c00f	     ; Write
; ** Screen soft switches **
TEXTOFF   =   $C050
TEXTON    =   $C051
MIXEDOFF  =   $C052
MIXEDON   =   $C053
PAGE1     =   $C054
PAGE2     =   $C055
LORES     =   $C056
HIRES     =   $C057
COL80ON   =   $c00d           ; turn on 80 columns
COL80OFF  =   $c00c

AN3OFF     =   $c05e           ; enable double-width graphics
AN3ON    =   $c05f           ; disable double-width graphics

	JMP skip_utils

!macro wait_cycles .N {
	LDA # (.N - 36) >> 8
	LDX # (.N - 36) & 255
	JSR delay_256a_x_33_clocks
}


; ----------------------------------------------------------------------------
; enable_vbl
; ----------------------------------------------------------------------------
enable_vbl:
        sei                     ; disable interrupts
        bit     IOUON
        bit     VBLINTON
        bit     IOUOFF
        lda     VSYNCIRF
        rts


; ----------------------------------------------------------------------------
; switch_display
; ----------------------------------------------------------------------------
switch_display:
sd1:	lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
        bpl     sd1              ;   IIe : wait for VBLANK start
        lda     VSYNCIRF        ; IIc : reset VSYNC IRF
sd2:	lda     VERTBLANK       ; IIe : wait for VBLANK end
        bmi     sd2
sd3:	lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
        bpl     sd3              ;   IIe : wait for VBLANK start

	;; once here, we're close to the drawn area of the screen
        rts




waiter !zone {
	!align 255, 0
	;;;;;;;;;;;;;;;;;;;;;;;;
	;; Delays A:X clocks+overhead
	;; Time: 256*A+X+33 clocks (including JSR)
	;; Clobbers A. Preserves X,Y. Has relocations.
	;;;;;;;;;;;;;;;;;;;;;;;;
	;;  https://www.nesdev.org/wiki/Delay_code#256%C3%97A_+_X_+_33_cycles_of_delay,_clobbers_A,_Z&N,_C,_V

.wloop1:	; 5 cycles done, do 256-5 more.
	sbc #1			; 2 cycles - Carry was set from cmp
	pha                     ; 3
	lda #(256-27 - 16)     ; 2
	jsr .delay_a_27_clocks  ; 240
	pla                     ; 4
delay_256a_x_33_clocks:
	cmp #1			; +2
	bcs .wloop1			; +3
	; 0-255 cycles remain, overhead = 4
	txa 			; -1+2; 6; +27 = 33

.delay_a_27_clocks:
        sec
.L:     	sbc #5
        bcs .L  ;  6 6 6 6 6  FB FC FD FE FF
        adc #3  ;  2 2 2 2 2  FE FF 00 01 02
        bcc .L4  ;  3 3 2 2 2  FE FF 00 01 02
        lsr     ;  - - 2 2 2  -- -- 00 00 01
        beq .L5  ;  - - 3 3 2  -- -- 00 00 01
.L4:     lsr     ;  2 2 - - 2  7F 7F -- -- 00
.L5:     bcs .L6  ;  2 3 2 3 2  7F 7F 00 00 00
.L6:     rts     ;
}
skip_utils:
