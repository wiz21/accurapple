import tqdm
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from scanner import qam_demodulation_kaiser_line, fix_range, OVERSAMPLING, M, post_process_color, side_by_side
from utils import gr_address, hgr_address

ZOOM = 8

def is_low(s):
    return not s

def is_rising(s0, s1):
    return (not s0) and s1

class FlipFlop:
    def __init__(self):
        self.d = False

    def set(self, clk, d):
        if clk:
            self.d = bool(d)

    @property
    def d_p(self):
        return not self.d



class SoftSwitches:
    def __init__(self, text:bool, hires:bool):
        self.text = text
        self.hires = hires
        self.col80 = False
        self.alt_charset = 0
        self.flash = 0

        if self.hires:
            self._ram_scan_func = hgr_address
            self._ram_base = 0x2000
        else:
            self._ram_scan_func = gr_address
            self._ram_base = 0x400

    @property
    def segb(self) -> bool:
        return self.hires == False

    @property
    def gr(self) -> bool:
        return self.text == False

    def ram_scanner(self,scanline,x):
        return self._ram_base + self._ram_scan_func(scanline) + x



def rom_address( switches: SoftSwitches, horizontal_count: int, vertical_count: int, byte: int):
    vid05 = byte & 0b0011_1111
    vid6 = (byte & 0b0100_0000) >> 6
    vid7 = (byte & 0b1000_0000) >> 7

    va = vertical_count & 1
    vb = (vertical_count & 2) >> 1
    vc = (vertical_count & 4) >> 2
    h0 = horizontal_count & 1

    ra9 = vid6 & (vid7 | int(switches.alt_charset) | int(switches.gr))
    ra10 = vid7 | ( int(not switches.gr) & vid6 & int(switches.flash) & (1-switches.alt_charset))
    ra11 = int(switches.gr)

    # "select" component
    if not switches.gr:
        sega = va
        segb = vb
    else:
        sega = h0
        segb = int(not switches.hires)
    segc = vc
    segc = int(segc)

    addr = sega + (segb << 1) + (segc << 2)
    addr += vid05 << 3
    addr += (ra9 << 9) + (ra10 << 10) + (ra11 << 11)
    return addr

def generate_base_signals(memory: np.ndarray, soft_switches: list[SoftSwitches], nb_steps: int, vertical_count: int):
    nb_steps *= ZOOM

    sig_14M = np.zeros((nb_steps,), dtype=bool)
    sig_7M = np.zeros((nb_steps,), dtype=bool)
    sig_COLOR_REF = np.zeros((nb_steps,), dtype=bool)
    sig_Q3 = np.zeros((nb_steps,), dtype=bool)
    sig_RAS_not = np.zeros((nb_steps,), dtype=bool)
    sig_AX = np.zeros((nb_steps,), dtype=bool)
    sig_PHI0 = np.zeros((nb_steps,), dtype=bool)
    sig_PHI1 = np.zeros((nb_steps,), dtype=bool)
    sig_VID7M = np.zeros((nb_steps,), dtype=bool)
    sig_VID7 = np.zeros((nb_steps,), dtype=bool)
    sig_LDPS = np.zeros((nb_steps,), dtype=bool)
    sig_H0 = np.zeros((nb_steps,), dtype=bool)
    load_shift = np.zeros((nb_steps,), dtype=bool)
    vid_out = np.zeros((nb_steps,), dtype=bool)
    debug1 = np.zeros((nb_steps,), dtype=bool)

    sig_SCAN_CK = np.zeros((nb_steps,), dtype=bool)
    scanner_h = 0
    shifter = 0

    ff_Q3 = FlipFlop()
    ff_RAS_p = FlipFlop()
    ff_AX = FlipFlop()
    ff_PHI0 = FlipFlop()
    ff_PHI1 = FlipFlop()
    ff_VID7M = FlipFlop()
    ff_LDPS = FlipFlop()

    for SIG28 in range(nb_steps):
        sig_14M[SIG28] = ((SIG28 // ZOOM) % 2) == 1
        sig_7M[SIG28] = (SIG28 // (ZOOM*2)) % 2 == 1
        sig_COLOR_REF[SIG28] = (SIG28 // (ZOOM*4)) % 2 == 1
        # sig_H0[SIG28] = ((SIG28+14) // (ZOOM*28)) % 2 == 1

    for SIG28 in range(nb_steps-1):
        RAS_p  = not ff_RAS_p.d
        RAS_pp = not ff_RAS_p.d_p
        Q3     = not ff_Q3.d
        Q3_p   = not ff_Q3.d_p
        Q3_pp  = not Q3_p
        AX     = not ff_AX.d
        AX_p   = not ff_AX.d_p
        PHI0   = not ff_PHI0.d
        PHI0_p = not ff_PHI0.d_p
        PHI1   = not ff_PHI1.d

        M7      = sig_7M[SIG28]
        M7_p    = not M7
        CLR_REF = sig_COLOR_REF[SIG28]
        H0      = scanner_h & 1

        new_RAS_p = Q3 or (RAS_pp and AX_p) \
            or (RAS_pp and CLR_REF and H0 and PHI0) \
            or (RAS_pp and M7_p and H0 and PHI0)

        new_AX = (RAS_pp and Q3) or (AX_p and Q3)

        new_Q3 = (
            (AX_p and PHI1 and M7_p) or (AX_p and PHI0 and M7) or (Q3_p and RAS_pp)
        )
        new_PHI0 = (
            (PHI0 and RAS_p and Q3_p) or (PHI0_p and RAS_pp) or (PHI0_p and Q3)
        )
        new_PHI1 = (
            (PHI0_p and RAS_p and Q3_p) or (PHI0 and RAS_pp) or (PHI0 and Q3)
        )

        SEGB = soft_switches[scanner_h].segb
        COL80 = soft_switches[scanner_h].col80
        GR = soft_switches[scanner_h].gr

        #VID07 = memory[scanner_h]
        MEM = memory[soft_switches[scanner_h].ram_scanner(vertical_count, scanner_h)]
        if scanner_h <= 1:
            MEM = 0
        VID7 = bool(MEM >> 7)
        VID7_p = not VID7


        # In HGR, the ROM always returns 0 for the 8th bit :-)
        # I think it is importnat for the shifter. Because if I put the
        # MEM (with the 8th bit) then strange things happen...
        VID07 = ROM[rom_address( soft_switches[scanner_h], scanner_h, vertical_count, MEM)]
        # if MEM != VID07:
        #     print(f"{MEM:8b} {VID07:8b}")
        #VID07 = MEM

        GR_p = not GR
        GR_pp = not GR_p
        SEGB_p = not SEGB
        COL80_pp = COL80
        H0_p = not H0

        VID7Mp = ff_VID7M.d_p

        new_VID7M = (
            # (GR_pp and SEGB)
            # or (GR_p and COL80_pp)
            # or (GR_p and M7)
            (VID7_p           and PHI1 and Q3_p and AX_p)
            #(H0_p and CLR_REF and PHI1 and Q3_p and AX_p)
            or (VID7Mp and PHI0)
            or (VID7Mp and Q3)
            or (VID7Mp and AX)
        )

        new_LDPS = (
               (Q3_p and AX_p and COL80_pp and GR_p)
            or (Q3_p and AX_p and PHI1 and GR_p)
            or (Q3_p and AX_p and PHI1 and SEGB)
            or (Q3_p and AX_p and PHI1 and VID7_p)
            # or (Q3_p and AX_p and PHI1 and CLR_REF and H0_p)
            or (Q3_p and AX   and RAS_pp and PHI1 and VID7 and SEGB_p and GR_pp)
        )

        # FIXME For some reason I have to invert these three...
        sig_RAS_not[SIG28] = ff_RAS_p.d_p
        sig_AX[SIG28]      = ff_AX.d_p
        sig_Q3[SIG28]      = ff_Q3.d_p

        sig_PHI0[SIG28]    = ff_PHI0.d
        sig_PHI1[SIG28]    = ff_PHI1.d
        sig_VID7M[SIG28]   = ff_VID7M.d_p
        sig_LDPS[SIG28]    = ff_LDPS.d_p

        sig_H0[SIG28]      = H0
        sig_SCAN_CK[SIG28] = (
            sig_RAS_not[SIG28] and (not sig_PHI0[SIG28]) and (not sig_Q3[SIG28])
        )
        sig_VID7[SIG28]    = VID7
        debug1[SIG28]      = PHI1 and Q3_p and AX_p

        if is_rising(sig_SCAN_CK[SIG28-1], sig_SCAN_CK[SIG28]):
            scanner_h += 1

        # When to act (LDPS == what to do)
        load_shift[SIG28] = is_low(sig_VID7M[SIG28]) and is_rising(sig_14M[SIG28-1],sig_14M[SIG28])

        if is_rising(load_shift[SIG28-1], load_shift[SIG28]):
            #print(" load/shift")
            if not sig_LDPS[SIG28]:
                #print("  load")
                # That is a load.
                #old = shifter
                shifter = VID07

                #print(f"{shifter:08b}")
                #print(f"At {SIG28} load shifter (old={old:08b}) {VID07:08b} from {scanner_h}")
                vid_out[SIG28] = shifter & 1
            else:
                #print("  shift")
                # That is a shift.
                shifter = (shifter >> 1) + ((shifter & 1) << 7)
                vid_out[SIG28] = shifter & 1
        else:
            vid_out[SIG28] = shifter & 1

        rising_14M = is_rising(sig_14M[SIG28-1], sig_14M[SIG28])
        ff_RAS_p.set( rising_14M, new_RAS_p)
        ff_AX.set(    rising_14M, new_AX)
        ff_Q3.set(    rising_14M, new_Q3)
        ff_PHI0.set(  rising_14M, new_PHI0)
        ff_PHI1.set(  rising_14M, new_PHI1)
        ff_VID7M.set( rising_14M, new_VID7M)
        ff_LDPS.set(  rising_14M, new_LDPS)



    signals = {
        "14M": sig_14M,
        "RAS'": sig_RAS_not,
        "AX": sig_AX,
        "Q3": sig_Q3,
        "PHI0": sig_PHI0,
        "PHI1": sig_PHI1,
        "VID7": sig_VID7,
        "debug1": debug1,
        "VID7M": sig_VID7M,
        "L/S when": load_shift,
        "LDPS": sig_LDPS,
        "7M": sig_7M,
        "CLR_REF": sig_COLOR_REF,
        "H0": sig_H0,
        "SCAN_CK":sig_SCAN_CK,
        "VID_OUT": np.roll(vid_out,shift=ZOOM*6),
    }

    return signals


def decoder( memory: np.ndarray, soft_switches: list[SoftSwitches], nb_steps: int, vertical_count: int):

    sig_14M = np.zeros((nb_steps,), dtype=bool)
    sig_7M = np.zeros((nb_steps,), dtype=bool)
    sig_COLOR_REF = np.zeros((nb_steps,), dtype=bool)
    sig_Q3 = np.zeros((nb_steps,), dtype=bool)
    sig_RAS_not = np.zeros((nb_steps,), dtype=bool)
    sig_AX = np.zeros((nb_steps,), dtype=bool)
    sig_PHI0 = np.zeros((nb_steps,), dtype=bool)
    sig_PHI1 = np.zeros((nb_steps,), dtype=bool)
    sig_VID7M = np.zeros((nb_steps,), dtype=bool)
    sig_VID7 = np.zeros((nb_steps,), dtype=bool)
    sig_LDPSp = np.zeros((nb_steps,), dtype=bool)
    sig_SCAN_CK = np.zeros((nb_steps,), dtype=bool)
    sig_H0 = np.zeros((nb_steps,), dtype=bool)
    load_shift = np.zeros((nb_steps,), dtype=bool)
    vid_out = np.zeros((nb_steps,), dtype=bool)
    vid_out = []
    ldps_dbg1 = np.zeros((nb_steps,), dtype=bool)
    ldps_dbg2 = np.zeros((nb_steps,), dtype=bool)
    ldps_dbg3 = np.zeros((nb_steps,), dtype=bool)

    sig_Q3[0] = True
    sig_PHI1[0] = True

    scanner_h = 0
    shifter = 0

    cpu_cycle_start = False
    cycle = 0
    for SIG28 in range(nb_steps):
        sig_14M[SIG28] = (SIG28 % 2) == 1
        sig_7M[SIG28] = (SIG28 // 2) % 2
        sig_COLOR_REF[SIG28] = (SIG28 // 4) % 2

        if SIG28 >= 1:
            prev = SIG28 - 1

            Q3 = sig_Q3[prev]
            Q3_p = not Q3
            AX = sig_AX[prev]
            AX_p = not sig_AX[prev]
            RAS_p = sig_RAS_not[prev]
            RAS_pp = not RAS_p
            PHI0 = sig_PHI0[prev]
            PHI0_p = not PHI0
            PHI1 = sig_PHI1[prev]
            PHI1_p = not PHI1
            M14 = sig_14M[prev]
            M7 = sig_7M[prev]
            M7_p = not M7
            COLOR_REF = sig_COLOR_REF[prev]
            VID7M = sig_VID7M[prev]
            LDPS = sig_LDPSp[prev]

            H0 = scanner_h & 1
            H0_p = not H0

            SEGB = soft_switches[scanner_h].segb
            COL80 = soft_switches[scanner_h].col80
            GR = soft_switches[scanner_h].gr

            #VID07 = memory[scanner_h]
            MEM = memory[soft_switches[scanner_h].ram_scanner(vertical_count, scanner_h)]
            VID07 = ROM[rom_address( soft_switches[scanner_h], scanner_h, vertical_count, MEM)]

            SEGB_p = not SEGB
            COL80_p = not COL80
            COL80_pp = not (not COL80)
            GR_p = not GR
            GR_pp = not(not GR)

            VID7 = (MEM & 0b1_000_0000) > 0
            if (not soft_switches[scanner_h-1].hires) and soft_switches[scanner_h].hires:
                VID7 = 0
                vid_out.append(0)
            VID7_p = not VID7
            sig_VID7[SIG28] = not VID7_p

            # Equations from UTAe page 3-22

            sig_RAS_not[SIG28] = Q3 or (RAS_pp and AX_p)
            sig_AX[SIG28] = (RAS_pp and Q3) or (AX_p and Q3)
            sig_Q3[SIG28] = (
                (AX_p and PHI1 and M7_p) or (AX_p and PHI0 and M7) or (Q3_p and RAS_pp)
            )
            sig_PHI0[SIG28] = (
                (PHI0 and RAS_p and Q3_p) or (PHI0_p and RAS_pp) or (PHI0_p and Q3)
            )
            sig_PHI1[SIG28] = (
                (PHI0_p and RAS_p and Q3_p) or (PHI0 and RAS_pp) or (PHI0 and Q3)
            )

            sig_VID7M[SIG28] = (
                (GR_pp and SEGB)
                or (GR_p and COL80_pp)
                or (GR_p and M7)
                or (VID7_p and PHI1 and Q3_p and AX_p)
                or (H0_p and COLOR_REF and PHI1 and Q3_p and AX_p)
                or (VID7M and AX)
                or (VID7M and PHI0)
                or (VID7M and Q3)
            )

            # UTA 3-20: "The SEGB, gated GR+2', VID7, and 80COL' inputs to the PAL
            # are used only in generation of LDPS' and VID7M. Also, none of the
            # other HAL outputs are affected by LDPS' and VID7M. LDPS' and VID7M
            # generation is discussed in Chapter 8."
            # => FIXME GR' est en fait GR+2'

            sig_LDPSp[SIG28] = (
                (Q3_p and AX_p and COL80_pp and GR_p)
                or (Q3_p and AX_p and PHI1 and GR_p)
                or (Q3_p and AX_p and PHI1 and SEGB)
                or (Q3_p and AX_p and PHI1 and VID7_p)
                or (Q3_p and AX_p and PHI1 and COLOR_REF and H0_p)
                or (Q3_p and AX   and RAS_pp and PHI1 and VID7 and SEGB_p and GR_pp)
            )

            ldps_dbg2[SIG28] = SEGB_p
            ldps_dbg3[SIG28] = Q3_p and AX_p and PHI1 and VID7_p

            # Hires delayed(long cycle)
            # SEGB_p like HIRES
            # GR_pp = graphics
            ldps_dbg1[SIG28] = Q3_p and AX   and RAS_pp and PHI1 and VID7 and SEGB_p and GR_pp

            # UTAE p. 3-7 : "1) RAS', CAS', Q3, PHASE 0, PHASE 1, VID7M, 7M, and
            # COLOR REFERENCE are all clocked by the rising edge of 14M."

            # Handle D flip flop (see fig 3.10 UA2e, right side)
            if is_rising(M14, sig_14M[SIG28]):
                sig_RAS_not[SIG28] = not sig_RAS_not[SIG28]
                sig_AX[SIG28] = not sig_AX[SIG28]
                sig_Q3[SIG28] = not sig_Q3[SIG28]
                sig_PHI0[SIG28] = not sig_PHI0[SIG28]
                sig_PHI1[SIG28] = not sig_PHI1[SIG28]
                sig_VID7M[SIG28] = not sig_VID7M[SIG28]

                # FIXME In the UTA2e, there is no inversion for LDPS but I need it
                # to reproduce the graphics ????
                sig_LDPSp[SIG28] = not sig_LDPSp[SIG28]
            else:
                sig_RAS_not[SIG28] = sig_RAS_not[SIG28-1]
                sig_AX[SIG28] = sig_AX[SIG28-1]
                sig_Q3[SIG28] =  sig_Q3[SIG28-1]
                sig_PHI0[SIG28] =  sig_PHI0[SIG28-1]
                sig_PHI1[SIG28] =  sig_PHI1[SIG28-1]
                sig_VID7M[SIG28] =  sig_VID7M[SIG28-1]
                sig_LDPSp[SIG28] =  sig_LDPSp[SIG28-1]

            sig_H0[SIG28] =  not H0_p


            # UTAE p 3-7: "The video scanner in the IOU is clocked by the rising
            # edge of RAS' during PHASE 1".

            sig_SCAN_CK[SIG28] = (
                sig_RAS_not[SIG28] and (not sig_PHI0[SIG28]) and (not sig_Q3[SIG28])
            )

            if is_rising(sig_SCAN_CK[SIG28-1], sig_SCAN_CK[SIG28]):
                scanner_h += 1

            # UTAE p. 3-4: "LDPS' (LoaD Parallel in/Serial out register) is a video
            # timing term that defines a video cycle. Picture patterns are loaded
            # while LDPS' is low and shifted out to the VIDEO output line when LDPS'
            # is high. LDPS' occurs once every 6502 cycle in SINGLE-RES display
            # modes and twice every 6502 cycle in DOUBLE-RES display modes."

            # UTAE p. 3-7 : "1) RAS', CAS', Q3, PHASE 0, PHASE 1, VID7M, 7M, and
            # COLOR REFERENCE are all clocked by the rising edge of 14M."

            # UTAE page 8-14: "The output of the video ROM is connected to a 74LS166
            # load/shift register that loads the dot patterns and shifts them out,
            # The patterns are loaded when LDPS' drops low near the end of PHASE 0
            # (if DOUBLE-RES) and near the end of PHASE 1 (always). When the 8-bit
            # load/shift register is not loading data, it is rotating the data in an
            # end around shift. The rotation is only important in LORES40 mode,
            # because new data is always loaded before the old data is completely
            # rotated in the other modes."

            load_shift[SIG28] = is_low(sig_VID7M[SIG28]) and is_rising(sig_14M[max(0,SIG28-1)], sig_14M[SIG28])

            if is_rising(load_shift[SIG28-1], load_shift[SIG28]):
                #print("rising")
                if not sig_LDPSp[SIG28]:
                    # That is a load.
                    #old = shifter
                    shifter = VID07
                    #print(f"At {SIG28} load shifter (old={old:08b}) {VID07:08b} from {scanner_h}")
                    vid_out.append( shifter & 1)
                else:
                    # That is a shift.
                    shifter = (shifter >> 1) + ((shifter & 1) << 7)
                    vid_out.append( shifter & 1)
            else:
                if len(vid_out) > 1:
                    vid_out.append(vid_out[-1])


    signals = {
        "14M": sig_14M,
        "RAS'": sig_RAS_not,
        "AX": sig_AX,
        "Q3": sig_Q3,
        "PHI0": sig_PHI0,
        "PHI1": sig_PHI1,
        "CLR_REF": sig_COLOR_REF,
        "7M": sig_7M,
        "VID7M": sig_VID7M,
        "VID7": sig_VID7,
        "LDPS'": sig_LDPSp,
        "SCAN_CK": sig_SCAN_CK,
        "H0": sig_H0,
        "Load/Shift":load_shift,
        "Video Out" : np.pad( np.array(vid_out), (0,sig_H0.shape[0]))[0:sig_H0.shape[0]],
        # "db": ldps_dbg1,
        # "db2": ldps_dbg2,
        # "db3": ldps_dbg3,
    }

    return signals

def plot_signals(signals: dict, nb_steps):

    height = (len(signals) + 1) * 3

    #t = np.arange(nb_steps)
    # for i in range(nb_steps // 2):
    #     plt.vlines(i * 2, 0, height, color="grey", alpha=0.5)

    # for i in range(nb_steps):
    #     cpu_cycle_start = sig_PHI0[i] and (not sig_Q3[i]) and sig_RAS_not[i]
    #     if cpu_cycle_start:
    #         plt.vlines(i, 0, height, color="black")

    # plt.stairs( sig_14M + 21, baseline = 21, label="14M")
    plt.yticks(np.arange(len(signals)) * 3 + 0.5 + 3, labels=list(reversed(signals.keys())))

    for i, label_data in enumerate(signals.items()):
        label, data = label_data
        base = (len(signals) - i) * 3
        # plt.stairs(
        #     (data + base)[:nb_steps], edges=np.arange(nb_steps + 1) - 0.5, baseline=base, label=label
        # )
        plt.plot(
            (data + base)[:nb_steps], label=label
        )
        #plt.scatter(np.arange(nb_steps), data + base, s=4)

        # if label == "Load/Shift":
        #     for j in range(nb_steps):
        #         if load_shift[j]:
        #             shifter = shifter >> 1
        #             plt.text(j, base, str(shifter & 1), ha="center", va="top")


    plt.title("Apple 2 Timing Signals Generation")
    plt.show()


def test_colour_screen_of_death():
    RAM = np.zeros( (65536,), dtype=np.uint8)

    with open("screen.hgr","rb") as fin:
        gr = np.frombuffer(fin.read(), np.uint8)
        RAM[0x2000:0x4000] = gr

    with open("screen.gr","rb") as fin:
        gr = np.frombuffer(fin.read(), np.uint8)
        RAM[0x400:0x800] = gr

    soft_switches_lines = []
    for i in range(192):
        if i < 64:
            soft_switches_lines.append( [SoftSwitches(text=False, hires=True)] * 4
                                       + [SoftSwitches(text=False, hires=False)] * 4
                                       + [SoftSwitches(text=False, hires=True)] * 7
                                       + [SoftSwitches(text=False, hires=False)] * 8
                                       + [SoftSwitches(text=False, hires=True)] * 6
                                        + [SoftSwitches(text=False, hires=False)] * 12  )
        elif i < 128:
            soft_switches_lines.append( [SoftSwitches(text=False, hires=True)] * 1
                                       + [SoftSwitches(text=False, hires=False)] * 4
                                       + [SoftSwitches(text=False, hires=True)] * 4
                                       + [SoftSwitches(text=False, hires=False)] * 8
                                       + [SoftSwitches(text=False, hires=True)] * 4
                                        + [SoftSwitches(text=False, hires=False)] * 12
                                        + [SoftSwitches(text=False, hires=True)] * 8)
        else:
            soft_switches_lines.append( [SoftSwitches(text=False, hires=True)] * 41)

    return RAM, soft_switches_lines

def test_choplifter():
    RAM = np.zeros( (65536,), dtype=np.uint8)
    with open("choplifter.hgr","rb") as fin:
        gr = np.frombuffer(fin.read(), np.uint8)
        RAM[0x2000:0x6000] = gr

    soft_switches_lines = []
    for i in range(192):
        soft_switches_lines.append( [SoftSwitches(text=False, hires=True)]*41 )

    return RAM, soft_switches_lines

if __name__ == "__main__":

    #with open("../data/3410265A.bin","rb") as fin:
    #with open("../data/Apple IIe Video - Unenhanced - 342-0133-A - 2732.bin","rb") as fin:
    with open("../data/Apple IIe Video - Enhanced - 342-0265-A - 2732.bin","rb") as fin:

        #ROM = fin.read()
        ROM = np.frombuffer(fin.read(), np.uint8).copy()

        for b in range(len(ROM)):
            ROM[b] = ~ROM[b]


    with open("miner.ram","rb") as fin:
        RAM = np.frombuffer(fin.read(), np.uint8).copy()



    #RAM, soft_switches_lines = test_pattern()
    RAM, soft_switches_lines = test_choplifter()


    NSTEPS = 40*28
    NB_LINES = 192
    image = []
    for y in tqdm.tqdm(range(NB_LINES)):
        signals = generate_base_signals(RAM, soft_switches_lines[y], NSTEPS, y)
        video_out = signals["VID_OUT"].astype(float)
        # print(video_out.shape)
        # print(video_out.shape[0] / 560)
        if y == 5:
            plot_signals(signals, NSTEPS*ZOOM*28)

        # plt.plot(video_out)
        # plt.scatter( np.arange(560)*64, video_out[::64])
        # plt.show()

        image.append( qam_demodulation_kaiser_line(video_out[::ZOOM*2],blur=int(M)))
    image = fix_range(np.log(1+np.array(image)))
    ref_image = Image.open("colored_screen_of_death.JPG")
    ref_image = ref_image.resize( (ref_image.width//4, ref_image.height//4) )
    side_by_side(
        Image.fromarray((image*255).astype(np.byte), "RGB").resize((560*2,NB_LINES*4)),  ref_image).show()

    exit()

    NB_STEPS = 40
    image = []
    for y in range(192):
        signals = decoder(RAM, soft_switches_lines[y], NB_STEPS*28, y)
        video_out = signals["Video Out"].astype(float)
        #video_out[0:1120:2] += video_out[1:1120:2]
        #video_out = np.convolve([1,1,1], video_out, mode="same")[0:1120]
        video_out = np.append([0,0,0], video_out)
        # plot_signals(signals, NB_STEPS*28)
        # print(len(video_out))
        # print(video_out[1::2].tolist())

        #deqam = qam_demodulation_kaiser_line(np.append(video_out[3:1122:2], 0),blur=0)
        # 0 ne montre rien => y'a alternance noir-aut'chose-noir-...
        deqam = qam_demodulation_kaiser_line(video_out[0:1120:2],blur=int(M*0.9))

        if y == 5*8+4:
            plot_signals(signals, NB_STEPS*28)
            # plt.plot(np.repeat(video_out,OVERSAMPLING//2))
            # plt.plot(deqam)
            # plt.show()
        image.append(deqam)


    image = fix_range(np.log(1+np.array(image)))
    #image = fix_range(np.array(image))
    print(image.shape)
    print(image.shape[1] / OVERSAMPLING)

    ref_image = Image.open("colored_screen_of_death.JPG")
    ref_image = ref_image.resize( (ref_image.width//4, ref_image.height//4) )
    side_by_side(
        post_process_color(
            Image.fromarray((image*255).astype(np.byte), "RGB"),
            color=2, contrast=1, brightness=1).resize((560*2,192*4)),
            ref_image).show()
