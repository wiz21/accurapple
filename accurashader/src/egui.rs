use std::{path::PathBuf, sync::{Arc, Mutex}};

use eframe::egui;
use egui_wgpu::ScreenDescriptor;
use wgpu::{Device, Queue};

use crate::wgpu_render::{CompositeToRGBUniform, Pipeline, RAMToCompositeUniform};



pub struct AccurappleImageCallback {
    pub apple_ram_bytes: Arc<Mutex<Box<[u8; 15360]>>>,
    pub soft_switches_bytes: Arc<Mutex<Box<[u8; 7680]>>>
}

// Documentation is here:
// https://github.com/emilk/egui/blob/master/crates/egui-wgpu/src/renderer.rs
impl egui_wgpu::CallbackTrait for AccurappleImageCallback {
    fn prepare(
        &self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        screen_descriptor: &ScreenDescriptor,
        _egui_encoder: &mut wgpu::CommandEncoder,
        resources: &mut egui_wgpu::CallbackResources,
    ) -> Vec<wgpu::CommandBuffer> {
        let pipeline: &Pipeline = resources.get().unwrap();

        {

            println!("lkjlkj");
            let buffer_slice = pipeline.ram_to_composite_pipeline.composite_copy_buffer.slice(..);

            let (tx, rx) = futures_intrusive::channel::shared::oneshot_channel();
            buffer_slice.map_async(wgpu::MapMode::Read, move |result| {
                tx.send(result).unwrap();
            });
            device.poll(wgpu::Maintain::Wait);

            use futures::executor;
            executor::block_on({
                rx.receive()
            }).unwrap().unwrap();

            let data = buffer_slice.get_mapped_range();


            let d2: &[u8] = &data[..];

            use std::io::Write;
            let mut f1 = std::fs::File::create("my_file.dat").unwrap();
            f1.write_all(d2).expect("write must succeed");

            // let s:&[f32] = bytemuck::cast_slice( d2);
            // println!("{}", s.len());


            // use image::{ImageBuffer, Rgba};
            // let buffer =
            //     ImageBuffer::<Rgba<u8>, _>::from_raw(560, 192, data).unwrap();
            // buffer.save("image.png").unwrap();
        }
        pipeline.ram_to_composite_pipeline.composite_copy_buffer.unmap();


        let apple_ram = self.apple_ram_bytes.lock().unwrap();
        let soft_switches = self.soft_switches_bytes.lock().unwrap();
        let composite_to_rgb_uniform = CompositeToRGBUniform::new();
        let ram_to_composite_uniform = RAMToCompositeUniform::new();

        pipeline.prepare(queue, _egui_encoder,
            apple_ram.as_ref(),
            soft_switches.as_ref(),
            egui::Pos2 { x: 0.0, y: 0.0 },
            &ram_to_composite_uniform,
            &vec![],
            &composite_to_rgb_uniform);
        Vec::new()


    }

    fn paint(
        &self,
        _info: egui::PaintCallbackInfo,
        render_pass: &mut wgpu::RenderPass<'static>,
        resources: &egui_wgpu::CallbackResources,
    ) {
        let pipeline: &Pipeline = resources.get().unwrap();
        pipeline.paint(render_pass);
    }

}


pub fn init_accurapple_render_pipeline(device: &Arc<Device>, queue: &Queue, rom_bytes: &[u8; 4096],
    path_apple_to_composite_shader: Option<PathBuf>) -> Pipeline {
    let pipeline = Pipeline::new(device,
            queue, rom_bytes, path_apple_to_composite_shader);
    return pipeline;
}


pub fn insert_accurapple_render_pipeline_in_egui(cc: &eframe::CreationContext, pipeline: Pipeline) {
        // Get the WGPU render state from the eframe creation context. This can also be retrieved
        // from `eframe::Frame` when you don't have a `CreationContext` available.
        let wgpu_render_state = cc.wgpu_render_state.as_ref().unwrap();

        // Because the graphics pipeline must have the same lifetime as the egui render pass,
        // instead of storing the pipeline in our `Custom3D` struct, we insert it into the
        // `paint_callback_resources` type map, which is stored alongside the render pass.
        wgpu_render_state
            .renderer
            .write()
            .callback_resources
            .insert( pipeline);
}
