/// #![deny(elided_lifetimes_in_paths)]

use std::f32;
use std::mem::size_of;
use std::path::PathBuf;

use bytemuck::cast_slice;
use eframe::egui;
use wgpu::util::DeviceExt; // some traits
use wgpu::{BindGroup, BindGroupLayout, Buffer, BufferSize, ComputePipeline, /* PipelineCompilationOptions, */ RenderPipeline, ShaderModuleDescriptor, TextureFormat};

const BYTES_PER_F32: usize = size_of::<f32>();

pub const COMPUTED_COMPOSITE_WIDTH: usize = 560+28;
const COMPOSITE_BUFFER_SIZE_IN_BYTES: usize = 192*COMPUTED_COMPOSITE_WIDTH*BYTES_PER_F32;
//const COMPOSITE_DEBUG_BUFFER_SIZE_IN_BYTES: usize = 50*2*COMPUTED_COMPOSITE_WIDTH*BYTES_PER_F32;
const COMPOSITE_DEBUG_BUFFER_SIZE_IN_BYTES: usize = 100*(573*2+54)*BYTES_PER_F32;






pub struct RAMToCompositePipeline {
    ram_buffer: Buffer,
    soft_switches_buffer: Buffer,
    composite_buffer: Buffer,
    compute_bind_group_layout: BindGroupLayout,
    compute_bind_group: wgpu::BindGroup,
    compute_pipeline: ComputePipeline,
    // The staging buffer is a copy of the composite buffer. This copy
    // is used as a transfer zone when we're doing a screenshot
    // and need to asynchronously access the GPU.
    // That copy is done in the rendering pipeline (not in the shaders).
    // FIXME That's what I think but I have never tested if this
    // is actually useful :-/
    pub composite_copy_buffer: Buffer,
    pub debug_buffer: Buffer,
    pub uniforms_buffer: Buffer,
}

#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
// We need this for Rust to store our data correctly for the shaders
#[repr(C)]
pub struct RAMToCompositeUniform {
    pub line_to_debug: u32,
    pub sw_clock_modulo: f32,
    pub phase0_offset: f32,
    pub flash: f32,
    pub dummy1: f32,
    pub dummy2: f32,
    pub _pad: [i32; 2]
}

impl RAMToCompositeUniform {
    pub fn new() -> RAMToCompositeUniform {
        RAMToCompositeUniform {
            line_to_debug: 200, // Outside the screen
            sw_clock_modulo: 0f32,
            phase0_offset: 0f32,
            flash: 0f32,
            dummy1: 0f32,
            dummy2: 0f32,
            _pad: [0; 2]
        }
    }

}

fn create_compute_shader_apple_ram_to_composite(device: &wgpu::Device, queue: &wgpu::Queue, rom_bytes: &[u8],
    path_apple_to_composite_shader: Option<PathBuf>) -> RAMToCompositePipeline {

    // ""

    let rom_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: Some("Buffer holding the graphics ROM"),
        usage: wgpu::BufferUsages::STORAGE
                | wgpu::BufferUsages::COPY_DST,
        size: 4096, // in bytes. I don't store the whole memory, just what's needed for text/GR/HGR.
        mapped_at_creation: false,
    });

    queue.write_buffer(&rom_buffer, 0, &rom_bytes);

    let ram_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: Some("The scanned RAM"),
        usage: wgpu::BufferUsages::STORAGE
             | wgpu::BufferUsages::COPY_SRC
             | wgpu::BufferUsages::COPY_DST,
        // Alternating values of RAM/AUXRAM (so, 2 bytes). 192 lines, 40 cycles per line.
        size: 192*40*2,
        mapped_at_creation: false,
    });


    let soft_switches_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: Some("soft_switches_buffer"),
        usage: wgpu::BufferUsages::STORAGE
            | wgpu::BufferUsages::COPY_SRC
            | wgpu::BufferUsages::COPY_DST,
        size: 192*65, // bytes
        mapped_at_creation: false,
    });


    let composite_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: Some("Composite Buffer"),
        usage: wgpu::BufferUsages::STORAGE
            | wgpu::BufferUsages::COPY_DST
            | wgpu::BufferUsages::COPY_SRC,
        size: COMPOSITE_BUFFER_SIZE_IN_BYTES as u64,
        mapped_at_creation: false,
    });


    let debug_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: Some("Debug Buffer"),
        usage: wgpu::BufferUsages::STORAGE
            | wgpu::BufferUsages::COPY_DST
            | wgpu::BufferUsages::COPY_SRC,
        size: COMPOSITE_DEBUG_BUFFER_SIZE_IN_BYTES as u64, // in bytes, texture format is r32float (=> 4 bytes)
        mapped_at_creation: false,
    });


    // Instantiates buffer without data.
    // `usage` of buffer specifies how it can be used:
    //   `BufferUsage::MAP_READ` allows it to be read (outside the shader).
    //   `BufferUsage::COPY_DST` allows it to be the destination of the copy.
    let composite_copy_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: None,
        size: COMPOSITE_BUFFER_SIZE_IN_BYTES as u64,
        usage:  wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::COPY_SRC,
        mapped_at_creation: false,
    });

    // let decoded_composite_texture = device.create_texture(
    //     &wgpu::TextureDescriptor {
    //         // All textures are stored as 3D, we represent our 2D texture
    //         // by setting depth to 1.
    //         size: wgpu::Extent3d {
    //             width: 560,
    //             height: 192,
    //             depth_or_array_layers: 1,
    //         },
    //         mip_level_count: 1, // We'll talk about this a little later
    //         sample_count: 1,
    //         dimension: wgpu::TextureDimension::D2,
    //         // Most images are stored using sRGB, so we need to reflect that here.
    //         format: wgpu::TextureFormat::Rgba8Unorm,
    //         // TEXTURE_BINDING tells wgpu that we want to use this texture in shaders
    //         // COPY_DST means that we want to copy data to this texture
    //         usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST
    //             | wgpu::TextureUsages::COPY_SRC | wgpu::TextureUsages::STORAGE_BINDING,
    //         label: Some("Final_screen"),
    //         // This is the same as with the SurfaceConfig. It
    //         // specifies what texture formats can be used to
    //         // create TextureViews for this texture. The base
    //         // texture format (Rgba8UnormSrgb in this case) is
    //         // always supported. Note that using a different
    //         // texture format is not supported on the WebGL2
    //         // backend.
    //         view_formats: &[],
    //     }
    // );

    let ram_to_composite_buffer = RAMToCompositeUniform::new();

    let uniform_buffer = device.create_buffer_init(
        &wgpu::util::BufferInitDescriptor {
            label: Some("Composite to RGB uniform buffer"),
            contents: bytemuck::cast_slice(&[ram_to_composite_buffer]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        }
    );


    let compute_module = device.create_shader_module(ShaderModuleDescriptor {
        label: Some("RAM to composite"),
        source: wgpu::ShaderSource::Wgsl(
            if let Some(p) = path_apple_to_composite_shader {
                if p.is_file() && p.exists() {
                    std::fs::read_to_string(p.as_path()).unwrap().into()
                } else {
                    panic!("The shader path is not found or is not a file: {}", p.to_string_lossy())
                }
            } else {
                include_str!("shaders/apple_to_composite.wgsl").into()
            })
    });


    let compute_bind_group_layout =
        device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("RAM to composite"),
            entries: &[
                // Apple RAM
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: true },
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(ram_buffer.size()),
                    },
                    count: None,
                },
                // input Composite signal
                wgpu::BindGroupLayoutEntry {
                    binding: 1,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: false },
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(composite_buffer.size()),
                    },
                    count: None,
                },
                // Apple SoftSwitches
                wgpu::BindGroupLayoutEntry {
                    binding: 2,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: true },
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(soft_switches_buffer.size()),
                    },
                    count: None,
                },
                // Apple Video ROM
                wgpu::BindGroupLayoutEntry {
                    binding: 3,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: true },
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(rom_buffer.size()),
                    },
                    count: None,
                },
                // The buffer of uniforms
                wgpu::BindGroupLayoutEntry {
                    binding: 4,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        //min_binding_size: core::num::NonZeroU64::new(std::mem::size_of::<f32>() as u64)
                        min_binding_size: wgpu::BufferSize::new(uniform_buffer.size())
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 5,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: false },
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(debug_buffer.size()),
                    },
                    count: None,
                },

                ]});

    // let decoded_composite_texture_view = decoded_composite_texture.create_view(
    //     &wgpu::TextureViewDescriptor::default());

    let compute_bind_group = device.create_bind_group(
        &wgpu::BindGroupDescriptor {
            layout: &compute_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: ram_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: composite_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: soft_switches_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 3,
                    resource: rom_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 4,
                    resource: uniform_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 5,
                    resource: debug_buffer.as_entire_binding(),
                }
            ],
            label: Some("RAM to composite"),
        }
    );

    let compute_pipeline_layout =
        device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("Compute Pipeline Layout"),
            bind_group_layouts: &[&compute_bind_group_layout],
            push_constant_ranges: &[],
        });

    let compute_pipeline = device.create_compute_pipeline(
        &wgpu::ComputePipelineDescriptor {
            label: Some("ram_to_composite"),
            layout: Some(&compute_pipeline_layout),
            module: &compute_module,
            entry_point: "main",
            compilation_options: wgpu::PipelineCompilationOptions::default(),
            cache: None,
        });



    return RAMToCompositePipeline {
        ram_buffer,
        soft_switches_buffer,
        composite_buffer,
        compute_bind_group_layout,
        compute_bind_group,
        compute_pipeline,
        composite_copy_buffer,
        uniforms_buffer: uniform_buffer,
        debug_buffer
    };
}

pub struct CompositeToRGBPipeline {
    compute_pipeline: ComputePipeline,
    pub apple_rgb_pixels_buffer: Buffer,
    uniforms_buffer: Buffer,
    filters_buffer: Buffer,
    bind_group : BindGroup
}

// This is so we can store this in a buffer
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
// We need this for Rust to store our data correctly for the shaders
#[repr(C)]
pub struct CompositeToRGBUniform {
    pub phase: f32,
    pub luma_filter_size: i32,
    pub luma_threshold: f32,
    pub comb_filter: i32, // Boolean flag: activate comb filtering
    pub composite: i32, // Boolean flag: activate composite only view

    /// What the CRT decides to do over the whole screen.
    /// 0 = black and white; 1 == colour
    pub colour_burst_crt_decision: i32,
    pub crt_reads_color_burst: i32,
    /// 1  =colour
    pub motherboard_mono_color_switch: i32,

    //pub _pad: [i32; 1],
    pub hsv_value: [f32; 4], // Padding seems necessary because it seems array are 16 bytes aligned in wgpu...
    pub colour_burst: [i32; 192],
}

impl CompositeToRGBUniform {
    pub fn new() -> CompositeToRGBUniform {
        CompositeToRGBUniform {
            phase: 0.636,
            comb_filter: 0,
            luma_filter_size: 21,
            luma_threshold: 0.1,
            composite: 0,
            colour_burst_crt_decision: 0,
            crt_reads_color_burst: 1,
            motherboard_mono_color_switch: 1, // 1  =colour
            hsv_value: [1.0, 1.130, 1.3, 1.0],
            colour_burst: [0; 192],
            //_pad: [0; 1],
        }
    }
}



fn create_compute_shader_composite_to_rgb(device: &wgpu::Device, queue: &wgpu::Queue,
    composite_buffer : &Buffer,
    soft_switches_buffer: &Buffer) -> CompositeToRGBPipeline {



    // let filters_texture_size = wgpu::Extent3d {
    //     width: 2241,
    //     height: 5,
    //     depth_or_array_layers: 1,
    // };

    // let filters_texture = device.create_texture(
    //     &wgpu::TextureDescriptor {
    //         // All textures are stored as 3D, we represent our 2D texture
    //         // by setting depth to 1.
    //         size: filters_texture_size,
    //         mip_level_count: 1, // We'll talk about this a little later
    //         sample_count: 1,
    //         dimension: wgpu::TextureDimension::D2,
    //         // Most images are stored using sRGB, so we need to reflect that here.
    //         format: wgpu::TextureFormat::R32Float,
    //         // TEXTURE_BINDING tells wgpu that we want to use this texture in shaders
    //         // COPY_DST means that we want to copy data to this texture
    //         usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST
    //             | wgpu::TextureUsages::COPY_SRC,
    //         label: Some("Final_screen"),
    //         // This is the same as with the SurfaceConfig. It
    //         // specifies what texture formats can be used to
    //         // create TextureViews for this texture. The base
    //         // texture format (Rgba8UnormSrgb in this case) is
    //         // always supported. Note that using a different
    //         // texture format is not supported on the WebGL2
    //         // backend.
    //         view_formats: &[],
    //     }
    // );

    // queue.write_texture(
    //     // Tells wgpu where to copy the pixel data
    //     wgpu::ImageCopyTexture {
    //         texture: &filters_texture,
    //         mip_level: 0,
    //         origin: wgpu::Origin3d::ZERO,
    //         aspect: wgpu::TextureAspect::All,
    //     },
    //     // The actual pixel data
    //     bytemuck::cast_slice( &filters_texture_data),
    //     // The layout of the texture
    //     wgpu::ImageDataLayout {
    //         offset: 0,
    //         bytes_per_row: Some(filters_texture_size.width * (BYTES_PER_F32 as u32)),
    //         rows_per_image: Some(filters_texture_size.height),
    //     },
    //     filters_texture_size,
    // );

    // let filters_texture_view = filters_texture.create_view(
    //     &wgpu::TextureViewDescriptor::default());


    let compute_module = device.create_shader_module(ShaderModuleDescriptor {
        label: Some("shader"),
        source: wgpu::ShaderSource::Wgsl(include_str!("shaders/composite_to_rgb.wgsl").into())
    });

    let apple_rgb_pixels_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: Some("Storage Buffer"),
        usage: wgpu::BufferUsages::STORAGE
            | wgpu::BufferUsages::COPY_DST
            | wgpu::BufferUsages::COPY_SRC,
        size: (COMPUTED_COMPOSITE_WIDTH*(4*4)*192) as u64, // in bytes, texture format is rgba32float (=> 4*4 bytes)
        mapped_at_creation: false,
    });

    let composite_to_rgb_uniform = CompositeToRGBUniform::new();

    let filters_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: Some("Filters buffer"),
        usage: wgpu::BufferUsages::STORAGE
            | wgpu::BufferUsages::COPY_SRC
            | wgpu::BufferUsages::COPY_DST,
        size: 256*4*4,
        mapped_at_creation: false,
    });

    let composite_to_rgb_uniform_buffer = device.create_buffer_init(
        &wgpu::util::BufferInitDescriptor {
            label: Some("Composite to RGB uniform buffer"),
            contents: bytemuck::cast_slice(&[composite_to_rgb_uniform]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        }
    );


    let compute_bind_group_layout =
        device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("Compute Pipeline textures"),
            entries: &[
                // output RGB signal
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: true },
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(filters_buffer.size()),
                    },
                    count: None,
                },
                // input Composite signal
                wgpu::BindGroupLayoutEntry {
                    binding: 1,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: true },
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(composite_buffer.size()),
                    },
                    count: None,
                },
                // output RGB signal
                wgpu::BindGroupLayoutEntry {
                    binding: 2,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: false },
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(apple_rgb_pixels_buffer.size()),
                    },
                    count: None,
                },
                // soft switches
                wgpu::BindGroupLayoutEntry {
                    binding: 3,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: true },
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(soft_switches_buffer.size()),
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 4,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None // BufferSize::new(composite_to_rgb_uniform_buffer.size())
                    },
                    count: None,
                },

            ]});


    let compute_bind_group = device.create_bind_group(
        &wgpu::BindGroupDescriptor {
            layout: &compute_bind_group_layout,
            entries: &[
                // wgpu::BindGroupEntry {
                //     binding: 0,
                //     resource: wgpu::BindingResource::TextureView(&filters_texture_view),
                // },
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: filters_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: composite_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: apple_rgb_pixels_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 3,
                    resource: soft_switches_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 4,
                    resource: composite_to_rgb_uniform_buffer.as_entire_binding(),
                }
            ],
            label: Some("compute_bind_group"),
        }
    );

    let compute_pipeline_layout =
        device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("Composite to RGB"),
            bind_group_layouts: &[&compute_bind_group_layout],
            push_constant_ranges: &[],
        });

    let compute_pipeline = device.create_compute_pipeline(
        &wgpu::ComputePipelineDescriptor {
            label: Some("composite_to_rgb"),
            layout: Some(&compute_pipeline_layout),
            module: &compute_module,
            entry_point: "main",
            compilation_options: wgpu::PipelineCompilationOptions::default(),
            cache: None
        });

    return CompositeToRGBPipeline {
        compute_pipeline,
        apple_rgb_pixels_buffer,
        uniforms_buffer: composite_to_rgb_uniform_buffer,
        bind_group : compute_bind_group,
        filters_buffer: filters_buffer
    }
}

pub struct RGBToHostPipeline {
    const_buffer : Buffer,
    bind_group_layout: BindGroupLayout,
    rgb_to_host_pipeline: RenderPipeline,
    rgb_to_host_bind_group: wgpu::BindGroup
}


fn create_compute_shader_rgb_to_host(device: &wgpu::Device, queue: &wgpu::Queue,
    apple_rgb_pixels_buffer : &Buffer,
    target_format: TextureFormat) -> RGBToHostPipeline {

        let rgb_to_hosts_consts = RGBToHostShaderConstants {
            top_left : [0.0, 0.0],
            irq_line: [-1; 4],
            window_size : [COMPUTED_COMPOSITE_WIDTH as f32 * 2.0, 192.0*4.0],
            line_to_debug: 500,
            frame: 0u32,
            _pad: [0; 2],
        };

        let const_buffer = device.create_buffer_init(
            &wgpu::util::BufferInitDescriptor {
                label: Some("RGB to host constants buffer"),
                contents: bytemuck::cast_slice(&[rgb_to_hosts_consts]),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            }
        );

        let rgb_to_host_texture_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Storage { read_only: true },
                            has_dynamic_offset: false,
                            min_binding_size:
                                wgpu::BufferSize::new(apple_rgb_pixels_buffer.size()),
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Uniform,
                            has_dynamic_offset: false,
                            min_binding_size:
                                wgpu::BufferSize::new(const_buffer.size()),
                        },
                        count: None,
                    },

                ],
                label: Some("texture_bind_group_layout"),
            });

        let rgb_to_host_bind_group = device.create_bind_group(
            &wgpu::BindGroupDescriptor {
                layout: &rgb_to_host_texture_bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: apple_rgb_pixels_buffer.as_entire_binding()
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: const_buffer.as_entire_binding()
                    }
                ],
                label: Some("RGB to host bing group"),
            }
        );


        let rgb_to_host_module = device.create_shader_module(ShaderModuleDescriptor {
            label: Some("shader"),
            source: wgpu::ShaderSource::Wgsl(include_str!("shaders/rgb_to_host.wgsl").into())
        });


        let rgb_to_host_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("RGB to host Pipeline Layout"),
                bind_group_layouts: &[&rgb_to_host_texture_bind_group_layout],
                push_constant_ranges: &[],
            });


        let rgb_to_host_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("RGB to host  Pipeline"),
            layout: Some(&rgb_to_host_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &rgb_to_host_module,
                entry_point: "vs_main", // 1.
                buffers: &[], // 2.
                compilation_options: wgpu::PipelineCompilationOptions::default()
            },
            fragment: Some(wgpu::FragmentState { // 3.
                module: &rgb_to_host_module,
                entry_point: "fs_main",
                targets: &[Some(wgpu::ColorTargetState { // 4.
                    format: target_format,
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrites::ALL,
                })],
                compilation_options: wgpu::PipelineCompilationOptions::default()
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList, // 1.
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw, // 2.
                cull_mode: Some(wgpu::Face::Back),
                // Setting this to anything other than Fill requires Features::NON_FILL_POLYGON_MODE
                polygon_mode: wgpu::PolygonMode::Fill,
                // Requires Features::DEPTH_CLIP_CONTROL
                unclipped_depth: false,
                // Requires Features::CONSERVATIVE_RASTERIZATION
                conservative: false,
            },
            depth_stencil: None, // 1.
            multisample: wgpu::MultisampleState {
                count: 1, // 2.
                mask: !0, // 3.
                alpha_to_coverage_enabled: false, // 4.
            },
            multiview: None,
            cache: None, // 5.
        });


        RGBToHostPipeline {
            const_buffer,
            bind_group_layout: rgb_to_host_texture_bind_group_layout,
            rgb_to_host_pipeline,
            rgb_to_host_bind_group,
        }
}


pub struct Pipeline {
    pub ram_to_composite_pipeline: RAMToCompositePipeline,
    pub composite_to_rgb_pipeline : CompositeToRGBPipeline,
    compute_composite_bind_group_layout: Option<BindGroupLayout>,
    compute_composite_bind_group: Option<wgpu::BindGroup>,
    compute_composite_to_rgb: Option<ComputePipeline>,
    rgb_to_host_pipeline: RGBToHostPipeline
}


#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
#[repr(C)]
pub struct RGBToHostShaderConstants {
    top_left: [f32; 2],
    window_size : [f32; 2],
    pub irq_line: [i32; 4],
    pub line_to_debug: u32,
    pub frame: u32,
    pub _pad: [i32; 2],
}

impl RGBToHostShaderConstants {
    pub fn new() -> RGBToHostShaderConstants {
        RGBToHostShaderConstants {
            top_left : [0.0, 0.0],
            irq_line: [-1; 4],
            window_size : [COMPUTED_COMPOSITE_WIDTH as f32 * 2.0, 192.0*4.0],
            line_to_debug: 555,
            frame:0u32,
            _pad: [0; 2],
        }
    }
}

impl Pipeline {
    /* My idea:

    First shader: RAM to composite signal: one thread per *line*, compute shader
    Second shader: composite signal to RGB : one thread per pixel, compute shader
    Third shader: RGB to screen, fragment shader.
    */




    pub fn new(device: &wgpu::Device, queue: &wgpu::Queue,
        rom_bytes: &[u8],
        path_apple_to_composite_shader: Option<PathBuf>) -> Pipeline {

        let ram_to_composite_pipeline = create_compute_shader_apple_ram_to_composite(
            device, queue,
            rom_bytes,
            path_apple_to_composite_shader);

        let composite_to_rgb_pipeline = create_compute_shader_composite_to_rgb(
            device, queue,
            &ram_to_composite_pipeline.composite_buffer,
            &ram_to_composite_pipeline.soft_switches_buffer);

        let rgb_to_host_pipeline = create_compute_shader_rgb_to_host(
            device, queue,
            &composite_to_rgb_pipeline.apple_rgb_pixels_buffer,
            /* TextureFormat::Bgra8UnormSrgb */
            TextureFormat::Bgra8Unorm
        );



        Pipeline {
            ram_to_composite_pipeline,
            composite_to_rgb_pipeline,
            rgb_to_host_pipeline,
            compute_composite_bind_group_layout: None,
            compute_composite_bind_group: None,
            compute_composite_to_rgb: None,
        }
    }


    pub fn prepare(&self,
        wgpu_queue: &wgpu::Queue,
        encoder: &mut wgpu::CommandEncoder,
        apple_ram_bytes: &[u8],
        soft_switches_bytes: &[u8],
        left_top_corner: egui::emath::Pos2,
        ram_to_composite_uniforms: &RAMToCompositeUniform,
        // sw_clock_modulo: f32,
        // phase0_offset: f32,
        // flash: f32,
        // line_to_debug: u32,
        filters: &Vec<f32>,
        composite_to_rgb_uniform: &CompositeToRGBUniform) {

        wgpu_queue.write_buffer(&self.ram_to_composite_pipeline.ram_buffer, 0, &apple_ram_bytes);
        wgpu_queue.write_buffer(&self.ram_to_composite_pipeline.soft_switches_buffer, 0, &soft_switches_bytes);
        wgpu_queue.write_buffer(&self.ram_to_composite_pipeline.uniforms_buffer, 0, bytemuck::bytes_of(ram_to_composite_uniforms));

        {
            let mut cpass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor {
                label: Some("RAM to composite"),
                timestamp_writes: None
            });
            cpass.set_pipeline(&self.ram_to_composite_pipeline.compute_pipeline);
            cpass.set_bind_group(0, &self.ram_to_composite_pipeline.compute_bind_group, &[]);
            cpass.insert_debug_marker("RAM to composite");
            cpass.dispatch_workgroups(1, 1, 1);
        }

        // let composite_to_rgb_uniform = CompositeToRGBUniform {
        //     phase : phase,
        //     luma_filter_size: composite_to_rgb_uniform.luma_filter_size,
        //     pad:[0.0; 2],
        //     hsv_value: composite_to_rgb_uniform.hsv_value
        // };
        wgpu_queue.write_buffer(&self.composite_to_rgb_pipeline.uniforms_buffer, 0, bytemuck::bytes_of(composite_to_rgb_uniform));
        wgpu_queue.write_buffer(&self.composite_to_rgb_pipeline.filters_buffer, 0, bytemuck::cast_slice(filters));
        // for f in filters.split_at(10).0 {
        //     print!("{:.3} ",f);
        // }
        // println!(" -- {}", composite_to_rgb_uniform.luma_filter_size);

        {
            let mut cpass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor {
                label: Some("Composite to RGB"),
                timestamp_writes: None
            });
            cpass.set_pipeline(&self.composite_to_rgb_pipeline.compute_pipeline);
            cpass.set_bind_group(0, &self.composite_to_rgb_pipeline.bind_group, &[]);
            cpass.insert_debug_marker("Composite to RGB");
            cpass.dispatch_workgroups((COMPUTED_COMPOSITE_WIDTH/14) as u32, 192/8, 1); // 41*14 == 560+14
        }

        // Plan a copy of the composite buffer. The composite buffer will be
        // used in later rendering shaders. The copy of the composite buffer
        // will be used while making screenshots and other tasks which are not
        // directly parts of the emulation rendering.
        encoder.copy_buffer_to_buffer(
            &self.ram_to_composite_pipeline.composite_buffer, 0,
            &self.ram_to_composite_pipeline.composite_copy_buffer, 0,
            (COMPUTED_COMPOSITE_WIDTH*192*4) as u64);
    }

    pub fn prepare_window_size(&self,
        queue: &wgpu::Queue,
        left_top_corner: egui::emath::Pos2,
        uniforms: &RGBToHostShaderConstants) {

        let mut rgb_to_hosts_consts = RGBToHostShaderConstants::clone(uniforms);
        rgb_to_hosts_consts.top_left = [left_top_corner.x, left_top_corner.y];
        queue.write_buffer(&self.rgb_to_host_pipeline.const_buffer, 0, bytemuck::bytes_of(&rgb_to_hosts_consts));
    }

    pub fn paint<'a>(&'a self, render_pass: &mut wgpu::RenderPass) {
        // When in egui, this one will be used.
        render_pass.set_pipeline(&self.rgb_to_host_pipeline.rgb_to_host_pipeline); // 2.
        render_pass.set_bind_group(0, &self.rgb_to_host_pipeline.rgb_to_host_bind_group, &[]); // NEW!)
        render_pass.draw(0..3, 0..1); // 3.
    }


    pub fn render(&self, surface: &wgpu::Surface, device: &wgpu::Device, queue: &mut wgpu::Queue) -> Result<(), wgpu::SurfaceError> {
        let output = surface.get_current_texture()?;
        let view = output.texture.create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("Render Encoder"),
        });

        {
        let mut _render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("RGB to host"),
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color {
                        r: 0.1,
                        g: 0.2,
                        b: 0.3,
                        a: 1.0,
                    }),
                    store: wgpu::StoreOp::Store,
                },
            })],
            depth_stencil_attachment: None,
            occlusion_query_set: None,
            timestamp_writes: None,
        });

        self.paint(&mut _render_pass);
        }

        // submit will accept anything that implements IntoIter
        let cmd_buf = encoder.finish();
        queue.submit(std::iter::once(cmd_buf));
        output.present();

        Ok(())
    }

}
