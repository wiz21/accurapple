const DEBUGGER: bool = true;

const SOFT_SWITCH_BIT_GR_DELAYED_BY_ONE_CYCLE:u32 = 0u; // AKA SOFT_SWITCH_BIT_GR
const SOFT_SWITCH_BIT_MODEMIX:u32 = 1u;
const SOFT_SWITCH_BIT_POS_AN3:u32 = 2u;
const SOFT_SWITCH_BIT_POS_TEXT:u32 = 3u;
const SOFT_SWITCH_BIT_POS_COL80:u32 = 4u;
const SOFT_SWITCH_BIT_POS_HIRES:u32 = 5u;
const SOFT_SWITCH_BIT_POS_ALT_CHARSET:u32 = 6u;
const SOFT_SWITCH_BIT_PAGE2:u32 = 7u;

const SLOW_SPEED: i32 = 1;
const FAST_SPEED: i32 = 2;

const COMPUTED_COMPOSITE_WIDTH: u32 = 560u+28u;

@group(0) @binding(0)
var<storage, read> apple_ram: array<u32>;

@group(0) @binding(1)
var<storage, read_write> composite_signal_out: array<f32, 115200>; //115200 == 600*192

@group(0) @binding(2)
var<storage, read> apple_soft_switches: array<u32, 1920>; // 1920 == 40*192 / 4

@group(0) @binding(3)
var<storage, read> apple_video_rom: array<u32>;

struct ParametersUniform {
    line_to_debug: u32,
    sw_clock_modulo : f32,
    phase0_offset : f32,
    flash: f32,
};
@group(0) @binding(4)
var<uniform> uniforms: ParametersUniform;

@group(0) @binding(5)
var<storage, read_write> debug_buffer: array<f32, 115200>; //115200 == 600*192


struct SegABC {
    hires: i32,
    hires1: i32,
    gr: i32,
    gr1: i32,
    gr2: i32,
    sega: i32,
    segb: i32,
    segc: i32,
    sega1: i32,
    segb1: i32,
    sega2: i32,
    segb2: i32
}

fn ror(x: u32) -> u32 {
    // SN74LS166AN is an 8-bit shift register (8, not 7)
    // To make it a rotate register, its output is connected to its input.
    return ((x >> 1u) & 127u) | ((x & 1u) << 7u);
}

fn shifter_data_out(shifter_reg_in: u32) -> u32 {
    return shifter_reg_in & 1u;
}

fn bnot(b: i32) -> i32 {
    return 1 - b;
}

fn bool2int(b: bool) -> i32 {
    if (b) {
        return 1;
    } else {
        return 0;
    }
}


fn read_scanned_ram(y: u32, byte_offset: i32, phase0: i32) -> u32 {
    // PHASE0 will tell if we read from RAM or AUXRAM
    // PHASE0: 0 == Motherboard, 1 == Aux;  (see Sather)

    // Moreover :

    // Scanner:   Mbd   Aux   Mbd   Aux   Mbd   Aux
    // Phase0:          111111000000111111000000111111000000
    // Mem:             Aux   Mbd   Aux   Mbd   Aux   Mbd
    // MemAddr:         [-- ODD  --][-- EVEN --][-- ODD ---] (you can see that on the H0 signal)

    if byte_offset >= 0 && (byte_offset < 40) {
        let addr = (y * 40u + u32(byte_offset)) * 2u + u32(phase0);
        let four_bytes: u32 = apple_ram[addr >> 2u];
        //let four_bytes = 0x90919293u; // PQRS
        return (four_bytes >> u32((addr & 3u)*8u)) & 0xFFu;
    } else if byte_offset == 40 {
        // FIXME This is not correct as 0 will only be interpreted
        // as black in LO/HI-RES modes. It will be interpreted as
        // an inversed @ in text modes...
        return 0u;
    } else {
        return 0u;
    }
}

fn rom_address( switches: i32, current_byte: u32, seg_abc: SegABC) -> i32 {

    // Inputs to ROM are:
    // - some soft switches
    // - SEGA,B,C
    // - GR+2
    // the RAM byte

    let ssw_alt_charset = (switches >> SOFT_SWITCH_BIT_POS_ALT_CHARSET) & 1;
    //let ssw_alt_charset = 1;
    let ssw_flash = i32(uniforms.flash);

    let byte = i32(current_byte);
    let vid05 = byte & 63; // 0b00111111
    let vid6 = (byte & 64) >> 6u;
    let vid7 = (byte & 128) >> 7u;


    /* Page 8-11 :"GR+1 and GR+2 are the GR signal, delayed by
    one and two scanner clocks respectively.
    ...
    GR+2 identifies GRAPHICS time in RA9 and RA10
    generation and is the GRAPHICS time output of the IOU.
    As an addressing input, GR+2 divides the video ROM into
    GRAPHICS patterns and TEXT pattern"
    */

    // If in graphics mode (ssw_gr2 == 1):
    //    ra9 = vid6; ra10 = vid7
    // If not in graphics mode:
    //    ra9 = vid6 & (vid7 | ssw_alt_charset)
    //    ra10 = vid7 | ( vid6 & ssw_flash & bnot(ssw_alt_charset));

    let GR2 = seg_abc.gr2;
    let ra9 = vid6 & (vid7 | GR2 | ssw_alt_charset);
    let ra10 = vid7 | ( bnot(GR2) & vid6 & ssw_flash & bnot(ssw_alt_charset));
    let ra11 = GR2;

    /* What happens when we go from LORES40 to TXT40 ?

    --------------------------------------------------------------------------------------------------------------------------------------
    // SEGA_A,B,C select on GR+1, ROM(GR+2, SEGB1), HAL(GR+1, SEGB)
                                                                            HAL         ROM
                                                                            ----------- -------
    Cycle1 : (ssw=lores40) GR=1,                 SEGB=lores    SEGB1=lores  dotspeed=14 lores
    Cycle2 : (ssw=txt40)   GR=0, GR+1=1;         SEGB=lores    SEGB1=lores  dotspeed=14 lores
    Cycle3 : (ssw=txt40)   GR=0, GR+1=0; GR+2=1, SEGB=VB       SEGB1=lores  dotspeed=7  lores  <--- What I need
    Cycle4 : (ssw=txt40)   GR=0, GR+1=0; GR+2=0, SEGB=VB       SEGB1=VB     dotspeed=7  text40
    Cycle5 : (ssw=txt40)   GR=0, GR+1=0; GR+2=0, SEGB=VB       SEGB1=VB     dotspeed=7  text40

    --------------------------------------------------------------------------------------------------------------------------------------
    // SEGA_A,B,C select on GR+1, ROM(GR+2, SEGB1), HAL(GR+2, SEGB1) (as described by Sather)
    // According to Apple schematics https://downloads.reactivemicro.com/Apple%20II%20Items/Hardware/IIe/Schematic/Apple%20IIe%20Schematic%20-%203.jpg
    // the IOU produces the GR and SEGB which are used in *both* HAL and ROM (what is not said is what is GR exactly, GR+1, +2 ?)
    // This is the same as Sather => so it must be correct, except it isn't :-(

                                                                            HAL         ROM
                                                                            ----------- -------
    Cycle1 : (ssw=lores40) GR=1,                 SEGB=lores    SEGB1=lores  dotspeed=14 lores
    Cycle2 : (ssw=txt40)   GR=0, GR+1=1;         SEGB=lores    SEGB1=lores  dotspeed=14 lores
    Cycle3 : (ssw=txt40)   GR=0, GR+1=0; GR+2=1, SEGB=VB       SEGB1=lores  dotspeed=14 lores
    Cycle4 : (ssw=txt40)   GR=0, GR+1=0; GR+2=0, SEGB=VB       SEGB1=VB     dotspeed=7  txt40
    Cycle5 : (ssw=txt40)   GR=0, GR+1=0; GR+2=0, SEGB=VB       SEGB1=VB     dotspeed=7  txt40

    This doesn't help because LORES rom is always tied to 14MHz
    Maybe the ROM access or shifter load introduces yet another delay ? (then it would mean that LD is set at the right time but there's another clock that delays it ?)
    Maybe when the inputs to the HAL asks for a 7MHz dot speed, the HAL needs a bit of time to react
    and then the LD operation
    The ROM access time seems to be 450nanosec... Thatt's half a cycle.
    --------------------------------------------------------------------------------------------------------------------------------------



    => Le problème ici, c'est que si on prend le SEGB non retardé (donc VB) pour la ROM alors on prend aussi le SEGA(=VA) (puisqu'on dirait
    qu'ils sont évalués simultanément). => J'ai SEGA qui vient faire une alternance aussi => je devrais avoir des patterns su 4 lignes...
    => faut tester ce que font exactement les franekn ROM accesses
    --------------------------------------------------------------------------------------------------------------------------------------

    Si c'était le dot speed qui induisait le décalage, alors le décalage serait le même à toutes les lignes
    => ça ne peut être que le ROM access qui crée le décalage.


    If SEGB is *not* delayed one cycle before reaching HAL, you read SEGB (which is wired to GR+1) and GR+2.
    On cycle 3, SEGB talks about text mode (its value is VB) but GR+2 still talks about graphics mode...
    => if SEGB is not delayed by one cycle before HAL, it seems to explain the artefacts.

    If SEGB is delayed one cycle before reaching HAL, then we have to look
    at GR+2 and SEGB+1. And those are in sync at all times => it can't explain the LORES40 -> TXT40 artifacts.

    Tracking SEGB gives us a way to decide when to activate 7MHz or 14MHz mode
    It tells us also how to compute the ROM address.
    */


    // ra11 is the 12th bit (=2048)
    let addr = (seg_abc.sega1 + (seg_abc.segb1 << 1u) + (seg_abc.segc << 2u))
        | (vid05 << 3u)
        | ((ra9 << 9u) + (ra10 << 10u) + (ra11 << 11u));
    return addr;
}



fn read_rom_byte(soft_switches: i32, seg_abc: SegABC, current_byte: u32) -> u32 {
    let rom_addr = u32(rom_address(i32(soft_switches), current_byte, seg_abc));
    let four_rom_bytes = apple_video_rom[rom_addr >> 2u];
    var translated_byte = (four_rom_bytes >> u32((rom_addr & 3u)*8u)) & 0xFFu ;
    return translated_byte;
}

fn is_gr(switches: i32, y: u32) -> bool {
    var on_mix_line: i32 = 0;
    if y >= 160u {
        on_mix_line = 1;
    }

    let ssw_gr = bnot((switches >> SOFT_SWITCH_BIT_POS_TEXT) & 1) & bnot(on_mix_line & (switches >> SOFT_SWITCH_BIT_MODEMIX) & 1);
    return ssw_gr == 1;
}


fn compute_gr1_gr2(switches: i32, h0: i32, vertical_count: i32, current: SegABC) -> SegABC {
    var new_segabc = SegABC(0,0,0,0,0,0,0,0,0,0,0,0);
    new_segabc.hires = current.hires;
    new_segabc.hires1 = current.hires1;
    new_segabc.sega = current.sega;
    new_segabc.segb = current.segb;
    new_segabc.segc = current.segc;
    new_segabc.sega1 = current.sega1;
    new_segabc.segb1 = current.segb1;
    new_segabc.sega2 = current.sega2;
    new_segabc.segb2 = current.segb2;


    var on_mix_line: i32 = 0;
    if vertical_count >= 160 {
        on_mix_line = 1;
    }

    let ssw_gr = bnot((switches >> SOFT_SWITCH_BIT_POS_TEXT) & 1)
        & bnot(on_mix_line & (switches >> SOFT_SWITCH_BIT_MODEMIX) & 1);

    new_segabc.gr = ssw_gr;

    // One cycle delay
    // LGR_TXT_N <= PGR_TXT_N_INT;  -- Called GR+2 in "Understanding the Apple IIe" by Jim Sather
    new_segabc.gr1 = current.gr;

    // Two cycles delay
    new_segabc.gr2 = current.gr1;

    return new_segabc;
}

fn compute_segabc(switches: i32, h0: i32, vertical_count: i32, current: SegABC) -> SegABC {
    /*
    SEG_ABC are the result of a flip-flop which is updated on SCANCLOCK
    */

    // HIRESEN_N <= PGR_TXT_N nand HIRES == NOT(PGR_TXT_N and HIRES)
    // FIXME PGR_TXT_N has timing ???

    let ssw_hires = (switches >> SOFT_SWITCH_BIT_POS_HIRES) & 1;

    // UtA2e: "The GR signal in Figure 8.5 is not the reset state of the
    // TEXT/GRAPHICS soft switch. Rather it represents GRAPHICS time, which is
    // all of the time in GRAPHICS NOMIX mode and all times except V4•V2 of
    // GRAPHICS MIXED mode. V4•V2 identifies TEXT time in MIXED mode, and it is
    // true during the last 32 horizontal periods of VBL' and during the last 38
    // undisplayed horizontal periods of VBL."

    var on_mix_line: i32 = 0;
    if vertical_count >= 160 {
        on_mix_line = 1;
    }

    let va = vertical_count & 1;
    let vb = (vertical_count & 2) >> 1u;
    let vc = (vertical_count & 4) >> 2u;

    // UTA2e p 8.12 : table 8.1 : "Selected signals are delayed one scanner
    // clock before being output from the IOU". (when output from IOU they end
    // up on the ROM address input). And on the chart they (fig. 8-5) SEGA/B/C
    // are built with GR+1 So we compute them once with GR1 and delay them by
    // one additional scanner clock.

    // See N6 at IOU1-row B col 2
    var new_segabc = SegABC(0,0,0,0,0,0,0,0,0,0,0,0);

    new_segabc.gr = current.gr;
    new_segabc.gr1 = current.gr1;
    new_segabc.gr2 = current.gr2;


    new_segabc.sega1 = current.sega;
    new_segabc.segb1 = current.segb;
    new_segabc.sega2 = current.sega1;
    new_segabc.segb2 = current.segb1;

    // SEGA/B/C are built with GR+1 (Table 8.1 in UtA2e)
    if new_segabc.gr1 == 0 {
        // Text mode. SEGA/B are used to choose the line of the character
        // to draw.
        new_segabc.sega = va;
        new_segabc.segb = vb;
    } else {
        // Graphics mode
        new_segabc.sega = h0;
        new_segabc.segb = bnot(ssw_hires);
    }
    new_segabc.segc = vc;

    return new_segabc;
}


fn read_soft_switch_byte(y: u32, byte_offset: i32) -> i32 {
    // byte offset is given in "screen coordinates": 0 to 39.
    // But the soft switches "scan" is in "real coordinates" where the HBL is there
    // So we add +25 to skip the HBL part.
    let addr = y*65u + u32(byte_offset) + 25u;
    let four_bytes = apple_soft_switches[addr >> 2u];
    let soft_switches = i32( (four_bytes >> u32((addr & 3u)*8u)) & 0xFFu );
    return soft_switches;
}

fn read_soft_switch(y: u32, time: i32, mask: u32) -> bool {
    return ((read_soft_switch_byte(y, time / 28) >> mask) & 1) == 1;
}

fn read_soft_switches(y: u32, time: i32) -> i32 {
    return read_soft_switch_byte(y, time / 28);
}


fn render_composite2(y: u32) {

    // Video RAM from AUX is available during PHASE0
    // Video RAM from MBD is available during PHASE1

    // From UtA2e: "Clockpulse action takes place when the 6502 PHASE0
    // clockpulse input line switches from high to low or low to high. These
    // transitions trigger actions inside the 6502 which will be discussed in
    // greater detail in the next chapter. "

    // From UtA2e: "The PHASE1 sequence controls the video scanner access to
    // RAM, and the PHASE0 sequence controls the MPU access to RAM."

    // From UtA2e: "A high to low transition of PHASE0 causes the 6502 to begin
    // a new machine cycle after a short delay."

    // Conclusion: CPU accesses memory while PHASE0 is low.
    // CPU triggers softswitches during a full cycle (made of PHASE0 and PHASE1), but I don't know when exactly.

    // H0 increases when PHASE0 goes from low to high
    // LDPS LOAD from RAM occurs at the end of PHASE1 high.
    // LDPS LOAD from AUX occurs at the end of PHASE1 high.

    var seg_abc = SegABC(0,0,0,0,0,0,0,0,0,0,0,0);
    var delay_hgr_countdown = 0;

    // Pump up seg_abc.
    // seg abc is update on GR.
    // GR
    var soft_switches = read_soft_switch_byte(y, 0);
    seg_abc = compute_gr1_gr2(soft_switches,0, i32(y), seg_abc);
    seg_abc = compute_segabc(soft_switches, 0, i32(y), seg_abc);
    seg_abc = compute_gr1_gr2(soft_switches,0, i32(y), seg_abc);
    seg_abc = compute_segabc(soft_switches, 0, i32(y), seg_abc);
    seg_abc = compute_gr1_gr2(soft_switches,1, i32(y), seg_abc);
    seg_abc = compute_segabc(soft_switches, 1, i32(y), seg_abc);

    var byte_offset = 0;


    var old_dot_shift_speed = 0;

    // The first LDPS load in TXT40/GR/HGR will occur after 7 dots. We need to show
    // black until the load occurs.
    var shift_register = 255u;
    var dot_shift_speed = 0;
    var mem_scan_speed = 0;
    var delay_in_progress = false;

    // Put outside the loop so they remain constant over 1/28MHZ periods.
    var debug_rom_addr = 0u;
    var debug_rom = 0u;
    var debug_load = 0;
    var debug_delay_hgr = false;
    var vid7m_extended = false;
    var old_VID7M_T123 = false;
    var was_delayed = false;
    var ldps_load_time = false;

    var ldps_load_planned = 88888;
    var next_shift_register = 0u;
    var rom_byte_hgr = 0u;

    var i = 0;

    // VID7M is 14M divided by two.
    var VID7M = true;

    var rom_value_delayed = 0u;

    while i < (560+7+4)*2 {

        composite_signal_out[y*COMPUTED_COMPOSITE_WIDTH + u32(i >> 1u)] = f32(1u-shifter_data_out(shift_register));

        // We start by the 7 AUX dots.

        // Figure 5.2 shows that the video RAM latching is done alternating
        // between PH0 and PH1. Figure 5.1 show that the latched data are
        // available when PHASE0 is rising (I guess for AUX ram it's PHASE1
        // rising, not shown on the figure).
        let byte_offset = i / 28;

        // PHASE0 is used to decide between RAM and AUX when scanning memory.
        // PHASE0 == 1 means AUX and we must start with AUX (because we need
        // to start correctly in 80COL)

        // During the first 7 dots, we're reading from AUX.
        // PHASE0 must be 1 when we're in AUX.
        let phase0 = 1 - (i / 14) % 2;
        let SIG_7M = (i % 4) == 2; // 7M is 1 at the beginning of PHASE0 low.

        // See figure 8.5. This signal triggers GR+1, GR+2 updates.
        let RASRISE1 = (i % 28) == 26; // RAS' & !PHASE0 & !Q3, one 14M before PHASE0 rises.
        let RASRISE1_p = ((i) % 28) == 26;
        let PHASE2 = ((i+0) % 28) == 26;
        let LOAD_SOFT_SWITCHES = i % 28 == 6*2; // PH0 & RAS'' * Q3' = 2 14M before PH0 fall
        let CLR_REF = (i / 4) % 2 == 0; // FIXME == 1 or == 0 ?


        /*
        Notes on scanner update (H0)
        ----------------------------

        On figure 3.6 (p56) it is clear that the video scanner increment is
        done on RASRISE1, one 14M before the rising edge of PHASE0 (+/- ==
        PHASE2, see figure 4.5). However, on figure 3.2, H0 has its value
        during PHASE0... So, I guess it means that H0 needs one 14M to
        reach its new value.

        - Contradiction ? Sather figure 3.2: H0 suit PHASE1 exactement. Mais figure 3.8, H0 est mis à jour sur RASRISE1...
        - According to https://github.com/frozen-signal/Apple_IIe_MMU_IOU/blob/master/IOU/IOU_TIMINGS.vhdl :
          "P_PHI_2 is HIGH for one 14M clk before and one 14M clock after a PHI_0 rising edge" ()
          => H0 est mis à jour un peu avant le rising edge de PHASE0, so it follows PHASE1.
          And this is not RASRISE1 and not PHASE0
        - According to Apple schematics of IOU, PHI2 contrôle le scanner (clock du counter)

        So scanner counter gets updated either on PHASE0 rising, PHASE2 or RASRISE1...Which one then ?
        */

        let H0 = (i / 28) % 2 == 1;

        // Now, ROM access only depends on SEGB (so it is timed on GR+2, which
        // itself is timed on RASRISE1). LDPS/VID7M depend on SEGB (timed on
        // GR+2, thus RASRISE1) and COL80 (RAS'' & Q3' & PHASE0)
        //

        // We start at EVEN address, so H0=0.
        // The first memory read is AUX
        // So we read: addr 0:AUX, addr 0:MBD, addr 1:AUX, addr 1:MBD,...
        // H0 is constant over on [AUX, MBD]


        let ram_byte = read_scanned_ram(y, byte_offset, phase0);

        if LOAD_SOFT_SWITCHES {
            // According to figure 7.1, all soft switches are updated on RAS'' & Q3'
            // & PHASE0 rising edge, that is, three 14M before PHASE0 falls.

            // (i+16)/28 is the first one to work when I study
            // TXT40 <-> TXT80 transition. The next ones up
            // to (i+42)/28.

            // (i+44)/28 makes LORES<->TXT40 somehow work...
            soft_switches  = read_soft_switch_byte(y, (i+16+30)/28);
        }

        let ssw80COL = ((soft_switches >> SOFT_SWITCH_BIT_POS_COL80) & 1) == 1;


        if RASRISE1 {
            seg_abc = compute_gr1_gr2(soft_switches, i32(H0), i32(y), seg_abc);
        }

        if RASRISE1 {
            seg_abc = compute_segabc(soft_switches, i32(H0), i32(y), seg_abc);
        }

        // http://www.applelogic.org/files/3410170A%20(ABEL).txt
        // /VID7M:=7M*GR'
        //     +/SEGB*PHASE_0*/GR'*VID7M
        //     +Q3'*/SEGB*/GR'*VID7M
        //     +AX*/SEGB*/GR'*VID7M
        //     +/AX*/VID7*/Q3'*/SEGB*/PHASE_0*/GR'
        //     +CREF*/AX*/H0*/Q3'*/SEGB*/PHASE_0*/GR'
        //     +SEGB*/GR'
        //     +GR'*/80COL'

        // /LDPS':=7M*/RAS'*/VID7*/Q3'*/SEGB*/PHASE_0*/GR'
        //     +/7M*/RAS'*VID7*/Q3'*/SEGB*/PHASE_0*/GR'
        //     +/AX*/Q3'*SEGB*/PHASE_0*/GR'
        //     +/AX*/Q3'*/PHASE_0*GR'
        //     +/AX*/Q3'*PHASE_0*GR'*/80COL'
        //     +CREF*/AX*/H0*/Q3'*/SEGB*/PHASE_0*/GR'


        // According to schematics, the *same* SEGB and GR+2 signals
        // are given to the ROM inputs *and* HAL inputs.
        let GR2 = seg_abc.gr2 == 1 & ((soft_switches >> SOFT_SWITCH_BIT_POS_AN3)  & 1) == 1;
        let SEGB = seg_abc.segb1 == 1; // SEGB1 is SEGB based on GR+1 delayed by one cycle
        let VID7 = (ram_byte & 128u) > 0u;

        let d = 2;
        let LDPS_CHECK = ((i+d) % 28) == 22; // ϕ1 & Q3' & AX'
        let LDPS_CHECK_PHASE_0_AND_1 = ((i+d) % 28) == 22 || ((i+d) % 28) == 8;
        let LDPS_HGR_CHECK = ((i+d) % 28) == (14-2)*2; //  Q3' & AX & RAS'' & ϕ1 = 2 befire phase0 rises

        let LDPS_S1 = LDPS_CHECK_PHASE_0_AND_1 & ssw80COL & !GR2;
        let LDPS_S2 = LDPS_CHECK & !GR2;
        let LDPS_S3 = LDPS_CHECK & SEGB;

        // Si pas VID7, alors on load de suite, sans delay.
        let LDPS_S4 = LDPS_CHECK & !VID7;

        let LDPS_S5 = LDPS_CHECK & CLR_REF & !H0;

        // Si VID7 et hires, alors on charge maintenant, donc avec un delay
        let LDPS_S6 = LDPS_HGR_CHECK & VID7 & !SEGB & GR2;

        // https://github.com/frozen-signal/Apple_IIe_TIMING_HAL
        // /VID7M:=
        //     +SEGB*/GR'    => comme Sather
        //     +GR'*/80COL'  => ssw80COL inversé
        //     +7M*GR'       => comme Sather
        //     +/PHASE_0*/Q3'*/AX * /VID7 * /SEGB*/GR' => Q3' inversé, AX inversé+ HGR check explicite
        //     +/PHASE_0*/Q3'*/AX * CREF */H0 */SEGB*/GR'
        //     +PHASE_0 * /SEGB*/GR'*VID7M => comme Sather
        //     +Q3'     * /SEGB*/GR'*VID7M => Q3' inversé
        //     +AX      * /SEGB*/GR'*VID7M => comme Sather


        // Ces 3 là demandent VID7M high tout le temps, donc demandent le 14MHz
        // Toujours bien évaluer Si à la leur des Si qui le précédent
        let VID7M_S1 = GR2 & SEGB;
        let VID7M_S2 = !GR2 & ssw80COL;
        let VID7M_S3 = !GR2 & SIG_7M;

        // Si VID7 == 0 : pas de délai
        // donc, au moment du check delay, on repasse à 1.
        // Si VID7 == 1 : delai
        // on laisse VID7M courir encore un 14M.
        let VID7M_S4 = LDPS_CHECK & !VID7; //  VID7' & ϕ1 & Q3' & AX'

        let VID7M_S5 = LDPS_CHECK & CLR_REF & !H0;

        // 7MHz alternating VID7M
        // T1 = VID7M & AX
        // T2 = VID7M & PH0
        // T3 = VID7M & Q3
        //let VID7M_T123 = (i % 28) != (7+4)*2; // 1 all time but 0 during one 14M tick after four 14M-ticks counted from PHASE0 falling

        // Ici on fait alterner VID7M. Sauf au moment de LDSP_CHECK.
        // Lorsque VID7=0, S4 inhibe ce "saut" d'alternance.
        // Si VID7=1, alors ce saut d'alternance se produit.
        // La valeur de VID7M est alors étirée d'un 14M cycle.
        // Ca permet de décaler VID7M par rapport au signal couleur.
        // Pour être bien aligné sur ce VID7M décalé, LDPS est aussi
        // décalé.
        // Si au prochain memscan, VID7 est maintenu à 1, alors if
        // faut maintenir le décalage.
        let VID7M_T123 = !LDPS_CHECK & !VID7M;

        ldps_load_time = false;

        if i % 2 == 0 {

            VID7M = VID7M_S1 | VID7M_S2 | VID7M_S3 | VID7M_S4 | VID7M_S5 | VID7M_T123;

            if LDPS_S1 | LDPS_S2 | LDPS_S3 | LDPS_S4 | LDPS_S5 | LDPS_S6 {
                ldps_load_time = true;
                if  byte_offset < 40 {
                    rom_byte_hgr = read_rom_byte(soft_switches, seg_abc, ram_byte);
                } else {
                    rom_byte_hgr = 255u;
                }
                shift_register = rom_byte_hgr;
            } else if VID7M {
                shift_register = ror(shift_register);
            }

            //composite_signal_out[y*COMPUTED_COMPOSITE_WIDTH + u32(i >> 1u)] = f32(1u-shifter_data_out(shift_register));

        }

        old_VID7M_T123 = VID7M_T123;

        if DEBUGGER && uniforms.line_to_debug <= 191 && y == uniforms.line_to_debug {
            let DEBUG_ROW_WIDTH = 573u*2u+54u;
            let b: u32 = u32(i);

            debug_buffer[0u * DEBUG_ROW_WIDTH + b] = f32(uniforms.line_to_debug);

            var k = 6u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(phase0);           k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(H0);           k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(CLR_REF);           k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(RASRISE1);           k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(SIG_7M);           k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(LOAD_SOFT_SWITCHES);           k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(LDPS_HGR_CHECK);           k += 1u;


            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(0.0); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = 0.0; k += 1u; //  VID7
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(seg_abc.segb1); k += 1u;

            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(LDPS_CHECK); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(LDPS_S1); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(LDPS_S2); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(LDPS_S3); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(LDPS_S4); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(LDPS_S5); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(LDPS_S6); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(VID7M_S1); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(VID7M_S2); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(VID7M_S3); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(VID7M_S4); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(VID7M_S5); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(VID7M_T123); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(GR2); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(VID7M); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(ldps_load_time); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(0); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(soft_switches); k += 1u; // soft_switches
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(byte_offset); k += 1u; // byte_offset
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(ram_byte); k += 1u; // at the beginning of this line, k==20

            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(debug_rom); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(debug_rom_addr); k += 1u; // was ROM_ADDR
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(debug_load); k += 1u; // SH/LD
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = 0.0; k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(shift_register); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(dot_shift_speed); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(mem_scan_speed); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(0); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(0); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(seg_abc.gr1); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(seg_abc.gr2); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(seg_abc.segb); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(seg_abc.sega1); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(seg_abc.segb1); k += 1u;
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(debug_delay_hgr); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(0); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(was_delayed); k += 1u; //delay_in_progress
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(0); k += 1u; //
            debug_buffer[k * DEBUG_ROW_WIDTH + b] = f32(seg_abc.sega); k += 1u; //
        }

        i += 1;
    } // while
}



@compute
@workgroup_size(1, 192, 1)
fn main(@builtin(global_invocation_id) global_id: vec3<u32>,
        @builtin(local_invocation_id) local_id: vec3<u32>) {
    render_composite2(local_id.y);
}
