const COMPUTED_WIDTH: i32 = 588;

struct Constants {
    top_left_corner : vec2<f32>, // width, height
    window_size : vec2<f32>, // width, height
    irq_line: vec4<i32>,
    line_to_debug: u32,
    frame: u32,
}

@group(0) @binding(0)
var<storage, read> rgb_signal: array<vec4<f32>, 112896>;

@group(0) @binding(1)
var<uniform> constants: Constants;

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
};



@vertex
fn vs_main(
    @builtin(vertex_index) in_vertex_index: u32,
) -> VertexOutput {
    var out: VertexOutput;
    let x = f32(1 - i32(in_vertex_index)) * 1.5 * 100.0;
    let y = f32(i32(in_vertex_index & 1u) * 2 - 1) * 1.5 * 100.0;
    out.clip_position = vec4<f32>(x, y, 0.0, 1.0);
    return out;
}


@fragment
fn fs_main(@builtin(position) pixelPosition: vec4f) -> @location(0) vec4<f32> {

    var rgb = vec4<f32>(0.0,0.0,0.0,0.0);

    // @builtin(position) reports coordinates in the coordinate system of the emulator
    // windows (that is, the one that has the *debugger* in it). I use this transform
    // to bring the coordinates into the emulated-content window.

    let a = vec2<f32>(
        f32(pixelPosition.x - constants.top_left_corner.x)/constants.window_size.x*f32(COMPUTED_WIDTH),
        f32(pixelPosition.y - constants.top_left_corner.y)/constants.window_size.y*312.0);

    if (constants.irq_line[0] >= 0 || constants.irq_line[1] >= 0) {

        if (60.0 <= a.y && a.y <= 60.0+192.0) {
            let rgb_apple = rgb_signal[i32(a.y - 60.0)*COMPUTED_WIDTH + i32(a.x)];
            rgb = vec4<f32>(rgb_apple.x, rgb_apple.y, rgb_apple.z, 0.0);
        }

        if i32(a.y) == constants.irq_line[0] {
            rgb = rgb + vec4<f32>(0,0.3,1.0,0.0);
        }
        if i32(a.y) == constants.irq_line[1] {
            rgb = rgb + vec4<f32>(1.0,0.0,0.0,0.0);
        }
    } else {
        let a = vec2<f32>(
            f32(pixelPosition.x - constants.top_left_corner.x)*0.5, // FIXME
            f32(pixelPosition.y - constants.top_left_corner.y)/constants.window_size.y*192.0);
        let rgb_apple = rgb_signal[i32(a.y)*COMPUTED_WIDTH + i32(a.x)];
        rgb = vec4<f32>(rgb_apple.x, rgb_apple.y, rgb_apple.z, 0.0);
    }

    let y_pos = u32(f32(pixelPosition.y - constants.top_left_corner.y)/constants.window_size.y*192.0);
    if y_pos == constants.line_to_debug {
        let s = (1.0 + sin(f32(constants.frame)/12.0)) / 2.0;
        rgb = rgb + vec4<f32>(0.7,7.0*s,s,0.0);
    }

    // rgb = vec4<f32>(rgb.x, f32((i32(pixelPosition.x) >> 1u) & 1), 0.0, 0.0);

    return rgb ;
}
