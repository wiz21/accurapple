mod wgpu_render;
mod wgpu_state;

use std::{fs::File, io::{BufReader, Read}, sync::{Arc, Mutex}, time::Instant};
use egui_wgpu::RenderState;
use wgpu::{Queue, Device};
use winit::event::{WindowEvent, Event};

use wgpu_render::Pipeline;
use wgpu_state::WgpuState;
use eframe::egui::{self, ImageSource, Context};



// Heavily based on
// https://stackoverflow.com/questions/72855505/what-is-the-best-way-to-update-app-fields-from-another-thread-in-egui-in-rust
fn slow_process2(state_clone: Arc<Mutex<State>>) {
    loop {
        let duration = 1000;
        //println!("going to sleep for {}ms", duration);
        std::thread::sleep(std::time::Duration::from_millis(duration));
        state_clone.lock().unwrap().duration = duration;
        let ctx = &state_clone.lock().unwrap().ctx;
        match ctx {
            Some(x) => x.request_repaint(),
            None => panic!("error in Option<>"),
        }
    }
}

struct State {
    duration: u64,
    ctx: Option<egui::Context>,
}

impl State {
    pub fn new() -> Self {
        Self {
            duration: 0,
            ctx: None,
        }
    }
}

pub struct App {
    state: Arc<Mutex<State>>,
}

impl App {
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        let state = Arc::new(Mutex::new(State::new()));
        state.lock().unwrap().ctx = Some(cc.egui_ctx.clone());
        let state_clone = state.clone();
        std::thread::spawn(move || {
            slow_process2(state_clone);
        });
        Self {
            state,
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.label(format!("woke up after {}ms", self.state.lock().unwrap().duration));
        });
        println!(".");
    }
}

fn stackoverflow() {

    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "eframe template",
        native_options,
        Box::new(|cc| Ok(Box::new(App::new(cc)))),
    );
}










use eframe::egui_wgpu::{self, wgpu};
use accurashader::egui::{AccurappleImageCallback, init_accurapple_render_pipeline, insert_accurapple_render_pipeline_in_egui};

fn slow_process(state_clone: Arc<Mutex<Context>>) {
    loop {
        let duration = 1000u64;
        //println!("going to sleep for {}ms", duration);
        std::thread::sleep(std::time::Duration::from_millis(duration));
        let ctx = state_clone.lock().ok();
        match ctx {
            Some(x) => x.request_repaint(),
            None => panic!("error in Option<>"),
        }
    }
}

pub struct DebugApp {
    angle: f32,
    apple_ram_bytes: Arc<Mutex<Box<[u8; 15360]>>>,
    soft_switches_bytes: Arc<Mutex<Box<[u8; 7680]>>>
}



impl DebugApp {
    pub fn new(cc: &eframe::CreationContext) -> Self {
        // Get the WGPU render state from the eframe creation context. This can also be retrieved
        // from `eframe::Frame` when you don't have a `CreationContext` available.
        let wgpu_render_state = cc.wgpu_render_state.as_ref().unwrap();
        let rom_bytes = include_bytes!("Apple IIe Video - Unenhanced - 342-0133-A - 2732.bin");
        let pipeline = init_accurapple_render_pipeline(&wgpu_render_state.device, wgpu_render_state.queue.as_ref(), rom_bytes, None);

        insert_accurapple_render_pipeline_in_egui(cc, pipeline);


        let r = Self { angle: 0.0,
            apple_ram_bytes: Arc::new( Mutex::new (Box::new([0u8; 15360]))),
            soft_switches_bytes: Arc::new( Mutex::new (Box::new([0u8; 7680])))
        };

        let f = File::open("dhgr.bin").unwrap();
        let mut reader = BufReader::new(f);
        let mut buffer: Vec<u8> = Vec::new();
        reader.read_to_end(&mut buffer);
        r.apple_ram_bytes.lock().as_deref_mut().unwrap().copy_from_slice(&buffer[7680..15360+7680]);
        r.soft_switches_bytes.lock().as_deref_mut().unwrap().copy_from_slice(&buffer[0..7680]);

        let state = Arc::new(Mutex::new(cc.egui_ctx.clone()));
        std::thread::spawn(move || {
            slow_process(state);
        });

        return r;
    }

    fn paint_accurapple_image(&mut self, ui: &mut egui::Ui) {
        let (rect, _response) =
            ui.allocate_exact_size(egui::vec2(560.0*2.0, 192.0*4.0), egui::Sense::drag());

        ui.painter().add(egui_wgpu::Callback::new_paint_callback(
            rect,
            AccurappleImageCallback {
                apple_ram_bytes: self.apple_ram_bytes.clone(),
                soft_switches_bytes: self.soft_switches_bytes.clone()
            }
        ));
    }
}



impl eframe::App for DebugApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::both()
                .auto_shrink(false)
                .show(ui, |ui| {
                    ui.horizontal(|ui| {
                        ui.spacing_mut().item_spacing.x = 0.0;
                        ui.label("The triangle is being painted using ");
                        ui.hyperlink_to("WGPU", "https://wgpu.rs");
                        ui.label(" (Portable Rust graphics API awesomeness)");
                    });
                    ui.label("It's not a very impressive demo, but it shows you can embed 3D inside of egui.");

                    egui::Frame::canvas(ui.style()).show(ui, |ui| {
                        self.paint_accurapple_image(ui);
                    });

                    ui.label(format!("Drag to rotate! {}", self.angle));
                    //ui.add(egui_demo_lib::egui_github_link_file!());
                });
        });
        //println!("update");
        self.angle += 1.0;
    }
}




// https://github.com/emilk/egui/blob/master/crates/egui_demo_app/src/apps/custom3d_wgpu.rs
fn egui_run() {
    //let native_options = eframe::NativeOptions::default();
    let native_options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default()
            .with_inner_size([1280.0, 1024.0])
            .with_drag_and_drop(true),

        renderer: eframe::Renderer::Wgpu,

        ..Default::default()
    };
    //eframe::run_native("My egui App", native_options, Box::new(|cc| Box::new(MyEguiApp::new(cc))));
    eframe::run_native("My egui App", native_options,
        Box::new(|cc|
            Ok(Box::new(
                DebugApp::new(cc))))).expect("should run");
}


#[cfg_attr(target_arch = "wasm32", wasm_bindgen(start))]
pub fn run() {
    // Window setup...
    let rom_bytes = include_bytes!("Apple IIe Video - Unenhanced - 342-0133-A - 2732.bin");
    //let filters_texture_data = fs::read("src/filters.bin").expect("can't read filters");
    let filters_texture_data = include_bytes!("filters.bin");
    let csod_ram_bytes = include_bytes!("csod_ram.bin");
    let csod_soft_switches_bytes = include_bytes!("csod_soft_switches.bin");

    let event_loop = winit::event_loop::EventLoop::new().unwrap();
    let window = winit::window::WindowBuilder::new().with_inner_size(winit::dpi::PhysicalSize::new(560.0*2.0, 192.0*2.0*2.0)).build(&event_loop).unwrap();
    let mut state = WgpuState::new(&window);
    let pipeline = Pipeline::new(&state.device, &state.queue, rom_bytes, None);
    let mut nb_frames = 0;

    let mut chrono = Instant::now();
    println!("Event loop starts");
    event_loop.run(move |event, elwt| {
        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                println!("The close button was pressed; stopping");
                elwt.exit();
            },
            Event::WindowEvent {
                event: WindowEvent::RedrawRequested,
                ..
            } => {
                state.update();

                match pipeline.render(&state.surface, &state.device,  &mut state.queue) {
                    Ok(_) => {}
                    // Reconfigure the surface if lost
                    Err(wgpu::SurfaceError::Lost) => state.resize(state.size),
                    // The system is out of memory, we should probably quit
                    Err(wgpu::SurfaceError::OutOfMemory) => elwt.exit(),
                    // All other errors (Outdated, Timeout) should be resolved by the next frame
                    Err(e) => eprintln!("{:?}", e),
                }

                nb_frames += 1;
                if nb_frames == 200 {
                    print!("\rFrames per sec {:.2}", (nb_frames as f32) / chrono.elapsed().as_secs_f32());
                    // use std::io;
                    use std::io::Write;
                    let _ = std::io::stdout().flush();
                    nb_frames = 0;
                    chrono = Instant::now();
                }
            },
            Event::AboutToWait => {
                // RedrawRequested will only trigger once unless we manually
                // request it.
                state.window().request_redraw();
            }
            _ => {}
        }
    }).ok();
}


fn main() {
    println!("Hello, world!");

    //accurashader::test::run();
    //run();
    egui_run();
    //stackoverflow();

     println!("Done!");

}
