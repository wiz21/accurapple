from tkinter import *
import time
from time import sleep, perf_counter
from math import sin, cos, pi
import numpy as np
from numpy.linalg import norm

def sign(a):
    if a > 0:
        return 1
    elif a < 0:
        return -1
    else:
        return 0


window = Tk()

WIDTH = 500
HEIGHT = 500


angle = 0
angle2 = 0.1
angle_speed = 0
mag = np.array([1,0,0])
f = np.array([-1,0.1,0])
t = 0
canvas = Canvas(window,width=WIDTH,height=HEIGHT)

WINDINGS_SEQUENCE = [
    ( 1, 0,1,0,0),
    ( 1, 1,0,0,0) ] * 30 + [ \
    (   4, 0,0,0,0),
    (  4.8, 0,1,1,0),
    (  4.4, 0,0,1,0),
    (    4, 0,0,1,1),
    (  3.9, 0,0,0,1),
    (  3.6, 1,0,0,1),
    (  3.5, 1,0,0,0),
    (  3.2, 1,1,0,0),
    (  3.1, 0,1,0,0),
    (  3.1, 0,1,1,0),
    (    3, 0,0,1,0),
    (  2.9, 0,0,1,1),
    (    3, 0,0,0,1) ] + \
    [(  2.8, 1,0,0,1),
    (  2.9, 1,0,0,0),
    (  2.8, 1,1,0,0),
    (  2.9, 0,1,0,0),
    (  2.8, 0,1,1,0),
    (  2.9, 0,0,1,0),
    (  2.8, 0,0,1,1), # 14 is good for a full head movement (36 tracks)
    (  2.9, 0,0,0,1)]*1 + \
    [(    3, 1,0,0,1),
    (  2.9, 1,0,0,0),
    (    3, 1,1,0,0),
    (  3.1, 0,1,0,0),
    (  3.2, 0,1,1,0),
    (  3.2, 0,0,1,0),
    (  3.6, 0,0,1,1),
    (  3.4, 0,0,0,1),
    (  4.1, 1,0,0,1),
    (  3.8, 1,0,0,0),
    (  4.8, 1,1,0,0),
    (  4.5, 0,1,0,0),
    (  0.1, 0,1,1,0),
    ( 36.6, 0,0,1,0),
]

for i in range(len(WINDINGS_SEQUENCE)):
    a = list(WINDINGS_SEQUENCE[i])
    a[0] /= 1000
    WINDINGS_SEQUENCE[i] = a


#WINDINGS_SEQUENCE = WINDINGS_SEQUENCE + list(reversed(WINDINGS_SEQUENCE))
mag1, mag2, mag3, mag4 = WINDINGS_SEQUENCE[0][1:]

seq_ndx = 0
old_t_in_ms = 0

first_pause = True

class Stepper:
    def __init__(self):
        self.angle_speed = 0
        self.angle = 0
        self._torque = 0
        self.set_mags(0,0,0,0)

        # Smooth
        #torque_factor = 200000 # norm of the torque vector
        #friction_factor = 600
        #self.max_dt = 0.001

        # Stiff
        self.torque_factor = 2000000
        self.friction_factor = 2200
        self.max_dt = 0.0001

    def set_mags(self, mag1, mag2, mag3, mag4):
        assert mag1 in (0,1)
        self.mag1, self.mag2, self.mag3, self.mag4 = mag1, mag2, mag3, mag4
        f = self.mag1*np.array([1,0]) # ->
        f += self.mag2*np.array([0,1]) # ^
        f += self.mag3*np.array([-1,0]) # <-
        f += self.mag4*np.array([0,-1]) # v
        self.mag_force = f

    def quarter_track(self):
        return round(self.angle/(2*pi)*8)

    def next_step_time(self):
        # How much time the stepper thinks it needs
        # to continue its movement. It is an estimation.
        # Returns 0 if the stepper is at rest.

        mag = np.array([cos(self.angle),sin(self.angle),0])
        if norm(self.mag_force) > 0:
            torque = abs(np.cross(mag, self.mag_force)[2])
        else:
            torque = 0

        if abs(self.angle_speed) > 1e-7 or abs(torque) > 1e-7:
            return self.max_dt
        else:
            #print(f"Stopped {self.angle_speed} {self._torque}")
            return 0

    def tick(self, dt):
        # dt: expressed in seconds

        assert dt > 0, "I got nothing to do"
        assert dt <= self.max_dt, "Precision will suffer"

        mag = np.array([cos(self.angle),sin(self.angle),0])
        if norm(self.mag_force) > 0:
            torque = self.torque_factor * np.cross(mag, self.mag_force)
            # norm(self.mag_force) is not always one !!!
            # The formula is: torque_factor * |r|/(|F|*|v|)
            # - |r| = torque vector norm
            # - F = torque vector direction
            # - mag = position vector
            self._torque = sign(torque[2]) * norm(torque) / (norm(self.mag_force)*norm(mag))
        else:
            self._torque = 0

        # This is an Euler step. It mens that we're doing
        # an approximation on the speed as big as dt.
        # With dt small enough (<0.001s) this seems just fine.
        friction = -self.friction_factor*self.angle_speed
        self.angle_speed += (self._torque + friction)*dt
        self.angle += self.angle_speed*dt

        if self.angle < 0:
            # Bumping against physical limits !
            self.angle = 0
            self.angle_speed = self.angle_speed*-0.1

        #print(f"{torque:.2f} {self.angle:.2f}")

def do_sequence():
    stepper = Stepper()
    next_time = WINDINGS_SEQUENCE[0][0]
    stepper.set_mags(*WINDINGS_SEQUENCE[0][1:5])
    seq_ndx = 0

    t = 0
    angles = []

    next_dt = 1
    while True:
        if t >= next_time:
            assert seq_ndx >= len(WINDINGS_SEQUENCE)-2 or t <= next_time + WINDINGS_SEQUENCE[seq_ndx+1][0] +0.1e-5, \
                f"{t} <= {next_time} + {WINDINGS_SEQUENCE[seq_ndx+1][0]}"
            if seq_ndx < len(WINDINGS_SEQUENCE)-1:
                seq_ndx += 1
                #print(f"{seq_ndx}/{len(WINDINGS_SEQUENCE)}")
            else:
                return angles

            next_time += WINDINGS_SEQUENCE[seq_ndx][0]
            stepper.set_mags(*WINDINGS_SEQUENCE[seq_ndx][1:5])

        dt = min(next_time - t, stepper.next_step_time())
        if dt > 0:
            stepper.tick(dt)
            t += dt
        else:
            t += min( next_time - t, 0.1)

        angles.append([t, stepper.angle, WINDINGS_SEQUENCE[seq_ndx][1:5], stepper.mag_force, stepper.quarter_track(),
                       WINDINGS_SEQUENCE[seq_ndx][5]])

    return angles

def read_stepper_log():
    seq = []
    last_t = None
    with open("stepper.log") as log:
        for line in log.readlines()[1092:]:
            a = line.split(",")
            d = { "0": 0, "1":1, "2":1, "3":1, "4":1}
            t = int(a[0])/(50*20280)
            expected = float(a[7])+1
            mags = list(map(d.get, a[2:6]))

            if len(seq) == 0 or mags != seq[-1][1:5]:
                l = [t] + mags + [expected]
                seq.append(l)
                last_t = t
            else:
                seq[-1][5] = expected


    for i in range(len(seq)-1):
        seq[i][0] = seq[i+1][0] - seq[i][0]
        print(seq[i])
    return seq

WINDINGS_SEQUENCE = read_stepper_log()




t0 = None
old_t = 0
limit = WINDINGS_SEQUENCE[0][0]

telemetry = do_sequence()

def draw_all():
    global seq_ndx, old_t, t0
    global stepper, limit, telemetry
    global first_pause

    if t0 is None:
        t0 = perf_counter()

    canvas.delete('all')

    t = (perf_counter()-t0)/5

    i = 0
    while i < len(telemetry)-1:
        if not telemetry[i][0] <= t < telemetry[i+1][0]:
            i += 1
        else:
            break

    x,y=WIDTH/2,HEIGHT/2

    rec_t = telemetry[i][0]
    quarter = telemetry[i][4]
    mags = telemetry[i][2]
    expected_angle = telemetry[i][5]

    colors=["white","red"]
    S = 5
    canvas.create_rectangle(x+100-S,y-S,x+100+S,y+S,fill=colors[mags[0]])
    canvas.create_rectangle(x-S,y-100-S,x+S,y-100+S,fill=colors[mags[1]])
    canvas.create_rectangle(x-100-S,y-S,x-100+S,y+S,fill=colors[mags[2]])
    canvas.create_rectangle(x-S,y+100-S,x+S,y+100+S,fill=colors[mags[3]])

    angle = telemetry[i][1]
    #print(telemetry[i])
    f = telemetry[i][3]
    rx, ry = 100*cos(angle), 100*sin(angle)
    canvas.create_line(x, y, x+rx, y-ry, width=5)
    canvas.create_line(x, y, x+f[0]*100, y-f[1]*100)
    canvas.create_text(10, 10, text=f"Track: {1+quarter/4:.2f} =? {expected_angle:.2f}, time: {rec_t:.3f} s", anchor="w")
    canvas.pack()
    window.update()
    window.after(1, draw_all)



window.after(100, draw_all)
window.mainloop()
