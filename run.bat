@echo off
REM # --release or nothing
REM set TARGET=%1%
REM chcp65011
set TARGET=--release
set RUST_LOG=
set RUST_LOG=accurapple=info,accurapple::debug2=info,wgpu_core=error,accurapple::sound=info,accurapple::speaker2=warn
rem set RUST_LOG=accurapple=debug,accurapple::sound=info,accurapple::diskii_stepper=info,accurapple::diskii=info
set RUST_LOG=accurapple::wozpack=error,%RUST_LOG%
set RUST_LOG=accurapple::wozfile=debug,%RUST_LOG%
set RUST_LOG=accurapple::diskii=warn,%RUST_LOG%
set RUST_LOG=accurapple::emulator=info,%RUST_LOG%
set RUST_LOG=accurapple::all6502=error,%RUST_LOG%
set RUST_LOG=accurapple::stepper2=error,%RUST_LOG%
set RUST_LOG=accurapple::mos6522=error,%RUST_LOG%
set RUST_LOG=accurapple::a2=error,%RUST_LOG%
rem set RUST_LOG=accurapple::diskii=error,wgpu_core=warn
rem set RUST_LOG=accurapple=debug,accurapple::sound=debug
set RUST_BACKTRACE=1

REM cargo run will build if necessary
REM cargo run %TARGET% --   --floppy1 "additional\Mines.DLR-8.dsk"
REM cargo run %TARGET% --  --floppy1 "additional\latecomer_finalv2.dsk"
REM cargo run %TARGET% --  --floppy1 "additional\oldskool.dsk" --script "SOUND_REC_START,VIDEO_REC_START zulu, WAIT_UNTIL 6, KEYS BRUN~OLDSKOOL§, WAIT_UNTIL 65, VIDEO_REC_STOP,SOUND_REC_STOP zulu,QUIT"
REM cargo run %TARGET% -- --debug --floppy1 "additional\conan.dsk" --script "TURBO_START, WAIT_UNTIL 30, TURBO_STOP, SPKR_REC_START, WAIT_UNTIL 40, SPKR_REC_STOP conan, QUIT"
echo %RUST_LOG%
REM cargo run %TARGET% -- --debug --symbols additional\symbols.txt --floppy1 "additional\conan.dsk"
REM balance of power : https://github.com/AppleWin/AppleWin/issues/1022
REM cargo run %TARGET% -- --symbols additional\symbols.txt --floppy1 "additional\AS-S1.dsk"
REM additional\Bug Attack.woz
REM additional\Prince of Persia side A.woz
REM additional\G.I. Joe - Disk 1, Side A.woz
REM additional\POMS_33_DOS.dsk
set A2AUDIT=additional\audit.dsk
set SKYFOX=additional\skyfox_mb.dsk
REM set GOONIES=additional/games/goonies.dsk
set GOONIES=additional/games/The Goonies.woz
set LOWTECH=additional\LOWTECH.WOZ
set LODE_RUNNER=.\additional\woz test images\Lode Runner.woz
set JELLYFISH=.\additional\woz test images\Jellyfish.woz
set CYCLOD=.\additional\woz test images\Cyclod.woz
set CONAN=additional\games\conan.dsk
set BUG_ATTACK=additional\games\Bug Attack.woz
set LEEPERS=additional\games\Lunar Leepers.woz
set CRISIS_MOUNTAIN=.\additional\woz test images\WOZ 2.0\Crisis Mountain - Disk 1, Side A.woz
set MATH_ADV=.\additional\woz test images\WOZ 2.0\First Math Adventures - Understanding Word Problems.woz
set MINER=.\additional\woz test images\WOZ 2.0\Miner 2049er II - Disk 1, Side A.woz
set BLAZING=.\additional\woz test images\WOZ 2.0\Blazing Paddles (Baudville).woz
set KAMUNGA=.\additional\woz test images\WOZ 2.0\Bouncing Kamungas - Disk 1, Side A.woz
set COMMANDO=.\additional\woz test images\WOZ 2.0\Commando - Disk 1, Side A.woz
set DOS33=.\additional\woz test images\WOZ 2.0\DOS 3.3 System Master.woz
set HARD_HAT=.\additional\woz test images\WOZ 2.0\Hard Hat Mack - Disk 1, Side A.woz
set PLANETFALL=.\additional\woz test images\WOZ 2.0\Planetfall - Disk 1, Side A.woz
set PRODOS=.\additional\woz test images\WOZ 2.1 (with FLUX chunks)\ProDOS User's Disk - Disk 1, Side A.woz
set SUNDOG=.\additional\woz test images\Sundog Frozen Legacy v2.0 - Play Side.woz
set LATECOMER=additional\latecomer_finalv2.dsk
set MAD2=additional\MAD2.dsk
set CRAZY_CYCLES=additional\CC.dsk
set DD2=additional\DD2.woz
set DIAGNOSTIC=additional\A2eDiagnostics_v2.1.dsk
set WIZ_TEST=diskii\NEW.DSK
set DARK_LORD=additional\games\dark_lord_1.dsk
set MANIAC=additional/games/Maniac Mansion side A.woz
set POP=additional\games\Prince of Persia side A.woz
set TEST_DRIVE=additional\games\Test Drive - 1.dsk
set STELLAR7=additional\games\Stellar 7.woz
set CHP=additional/CHP.dsk
set MAD2=additional/MAD2.dsk
set SMR=additional/smr-stevie.woz
set GROUIK=additional/test_grouik_mockingboard.dsk
set OLD_SKOOL=additional/oldskool.dsk
REM See http://lukazi.blogspot.com/2017/03/double-high-resolution-graphics-dhgr.html
set DHGR_TEST=additional\A2BestPix_Top1.DSK
REM C:\Users\StephaneC\Downloads\Puyo.DSK
REM BP_CYCLE 27929346
REM 8460540
REM --cpu F6502
$env:RUST_LOG = '%RUST_LOG%'
REM --no-sound

REM Memory leak
REM cargo run %TARGET% -- --cpu F6502 --no-sound --log-stdout --symbols additional\symbols.txt --floppy1 "%BUG_ATTACK%"
REM --script "TURBO_START"
REM cargo run %TARGET% --  --cpu F6502  --symbols additional\symbols.txt --floppy1 "%GOONIES%" --script "WAIT_UNTIL 11, JOYSTICK 1.0 0.0 1 1, WAIT_UNTIL 12, JOYSTICK -1.0 0.0 0 0,WAIT_UNTIL 13, JOYSTICK 1.0 0.0 1 1, WAIT_UNTIL 14, JOYSTICK -1.0 0.0 0 0,WAIT_UNTIL 15, JOYSTICK 1.0 0.0 1 1, WAIT_UNTIL 16, JOYSTICK -1.0 0.0 0 0,WAIT_UNTIL 17, JOYSTICK 1.0 0.0 1 1, WAIT_UNTIL 18, JOYSTICK -1.0 0.0 0 0, WAIT_UNTIL 19, JOYSTICK STOP"

REM cargo run %TARGET% -- --cpu F6502  --symbols additional\symbols.txt --log-stdout --floppy1 "%SMR%"
REM  --script "BP_PC 803"
REM --script "D2_STEPPER_REC_START stepper.txt, WAIT_UNTIL 8, D2_STEPPER_REC_STOP, QUIT"

REM --script "TURBO_START, WAIT_UNTIL 100, VIDEO_REC_START popvid, SOUND_REC_START popsnd, WAIT_UNTIL 120, SOUND_REC_STOP, VIDEO_REC_STOP, QUIT"
REM --script "TURBO_START, WAIT_UNTIL 10, QUIT"

REM cargo run %TARGET% --  --cpu F6502 --symbols additional\symbols.txt --floppy1 "additional\games\F-15 Strike Eagle v1.4.woz" --script "D2_STEPPER_REC_START f15_stepper.txt, WAIT_UNTIL 36, D2_STEPPER_REC_STOP"
REM--script "TURBO_START, WAIT_UNTIL 15, KEYS RUN~PARTY, KEY_RETURN, WAIT_UNTIL 20, TURBO_STOP"


cargo run %TARGET% -- --no-sound --log-stdout --symbols additional\symbols.txt --floppy1 "%OLD_SKOOL%"
REM --script "WAIT_UNTIL 4, SET_GFX_DEBUG_LINE 65, WAIT_UNTIL 5, SAVE_ALL_MEM allmem.bin, IOU_GFX_INPUT scanner.bin, SAVE_GFX_DEBUG gfx_debug.bin, QUIT"
REM --script "BP_PC 801"
REM cargo run %TARGET% --  --cpu F6502 --symbols additional\symbols.txt --floppy1 "%DARK_LORD%" --script "TURBO_START, WAIT_UNTIL 20, KEYS D"
REM --script "WAIT_UNTIL 28, SCREENSHOT dark_lord_composite.png, QUIT"
rem --script "BP_CYCLE 14971443"
REM "BP_CYCLE 27829346, TURBO_START, WAINT_UNTIL 27, TUBO_END"
REM --script "D2_STEPPER_REC_START, WAIT_UNTIL 3, D2_STEPPER_REC_STOP lowtech_stepper.txt, QUIT"
REM --script "D2_STEPPER_REC_START, WAIT_UNTIL 20, D2_STEPPER_REC_STOP stepper.log, QUIT"
REM --script "WAIT_UNTIL 28, SPEAKER_REC_START, SOUND_REC_START, WAIT_UNTIL 58, SOUND_REC_STOP gijoe, SPEAKER_REC_STOP gijoe,QUIT"
REM cargo run %TARGET% -- --symbols additional\symbols.txt --floppy1 "additional\Prince of Persia side A.woz" --script "WAIT_UNTIL 12, SPEAKER_REC_START, SOUND_REC_START, WAIT_UNTIL 55, SOUND_REC_STOP pop, SPEAKER_REC_STOP pop,QUIT"
REM cargo run %TARGET% -- --symbols additional\symbols.txt --floppy1 "additional\pick_a_dilly_pair.dsk" --script "WAIT_UNTIL 22, SPEAKER_REC_START, SOUND_REC_START, WAIT_UNTIL 60, SOUND_REC_STOP picka, SPEAKER_REC_STOP picka,QUIT"
REM cargo run %TARGET% -- --symbols additional\symbols.txt --floppy1 "additional\conan.dsk"
REM --script "WAIT_UNTIL 3, SPEAKER_REC_START, WAIT_UNTIL 18, SPEAKER_REC_STOP bug_attack,QUIT"
REM copy bug_attack.ticks speaker\recordings
REM dir speaker\recordings\bug_attack.ticks
REM python .\speaker\snd.py --file .\speaker\recordings\bug_attack.ticks --filter kegs

REM additional\Prince of Persia side A.woz
REM Balance of Power - Disk 1, Side A.woz
REM cargo run %TARGET% -- --debug --symbols additional\symbols.txt --floppy1 "additional\prince of persia side a (san inc crack).dsk"
REM spy_vs_spy_3.dsk

REM production build
REM cargo build --profile production
REM dir target\production\accurapple.exe
REM ..\..\ffmpeg-2023-01-04-git-4a80db5fc2-full_build\bin\ffmpeg.exe -y -i %TEMP%\accurapple.wav -framerate 50 -i zulu_%%05d.png -pix_fmt yuv420p -vf "crop=560:384:0:0,scale=1400:1080:flags=neighbor,pad=2048:1080:(ow-iw)/2:(oh-ih)/2"  -r 50 -profile:v baseline -preset slow -crf 10 -shortest cc.mp4

REM "C:\Program Files\7-Zip\7z.exe" a accurapple.zip .\target\release\accurapple\accurapple.exe
