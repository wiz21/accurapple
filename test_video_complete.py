import glob
from pathlib import Path
""" Make sure all screenshot of a video are present.
So, make sure that we didn't skip a frame.
"""

last_frame = list(sorted(glob.glob("video_crt*.png")))[-1]
print(f'Last frame is : {last_frame}')

nb = int(last_frame.replace("video_crt_","").replace(".png",""))

i = 1
while i <= nb:
    p = Path(f"video_crt_{i:05}.png")
    if not p.exists():
        print(f"This one is missing: {p}")
        break
    else:
        i += 1
