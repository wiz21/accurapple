# AccurApple

This is a wannabe Apple emulator which strives to be as accurate as possible as
far as graphics, sound and Disk ][ are concerned. In other words, I will
probably not have enough time to emulate all the known peripherals. Although I
am pretty happy with the general structures/algorithms in AccurApple, I must
admit there is still an enormous work ahead as far as testing is concerned.

AccurApple tries to replicate an Apple 2e Revision B wired to a PAL CRT monitor.
That model was used a lot in Europe (most of the other emulators are for the NTSC
monitors).

# How to install

To build it, just run `cargo build --release` (unfortunately, the debug build is too slow). It's known to work on Windows, Linux (I test on Debian Bookworm) and MacOs.

On debian, you may need to install additional packages. For example: `apt-get install libudev`.

# Features

So far we have:
* 128KB support
* HGR, GR, DHGR, 40, 80 columns text, mixed modes
* Proper graphics signal modulation which enables (hopefully) correct mode switching
* VaporLock
* Speaker sound (with improved rendering based on simple harmonic oscillator)
* Keyboard
* Joystick
* MockingBoard
* DSK and WOZ (v2+ and flux tracks) support with rather good floppy simulation (99% of games work, nanosec accuracy, weak bits, disk wobble)
* A functional debugger
* Experimental support for 65C02 (thanks Obscoz for the tremendous support !)
* When available, we display some games covers (nostalgia bonus :-)).

To do's
* Other bus tricks
* Convincing rendition of TV/monitor beamer artefacts
* Testing to figure out all the little bugs that are still there...

The emulator has been tested with this [ROM](https://github.com/AppleWin/AppleWin/blob/master/resource/Apple2e_Enhanced.rom)
and dozens of games.


*Redde Caesari quae sunt Caesaris*:
- Many good ideas were discussed with lots of great and passionated people, be sure to read the documentation where they're all listed.
- Original code started as a fork from [rustyapple](https://github.com/sehugg/rustyapple). There was no license attached to that initial code, see [issue](https://github.com/sehugg/rustyapple/issues/1). It's mostly rewritten now (the original code was 8 years old when I started).
- I have been able to bootstrap this project thanks to the very good code of [Shamus' emulator](http://shamusworld.gotdns.org/apple2/)
- The default 6502 emulation is based on [floooh's](https://github.com/floooh/chips) marvellous code
- The experimental 65c02 is provided by [Obscoz](https://gitlab.com/obscoz/oz65c02)
- The 6502 disassembler is based on [rs6502](https://github.com/simon-whitehead/rs6502)
- The MockingBoard code uses the [AYMPrecise](https://crates.io/crates/aym) library
- Although I don't use it anymore, I had some fun trying to understand the composite emulation technique proposed by John "xot" Leffingwell.
