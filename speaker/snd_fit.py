# dkjshfkjdshf

import os.path
from pathlib import Path
import pickle
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import soundfile
from math import ceil, sqrt, exp
import logging
from logging import debug, info
from scipy.optimize import curve_fit, minimize
from scipy.stats import norm
from numba import jit

FORCE_RELOAD = False

logger = logging.getLogger('SndFit')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class Speaker:
    """Simulates the response of the Apple II speaker."""

    # TODO: move lookahead.evolve into Speaker method

    def __init__(self, sample_rate: float, freq: float, damping: float,
                 scale: float):
        """Initialize the Speaker object
        :arg sample_rate The sample rate of the simulated speaker (Hz)
        :arg freq The resonant frequency of the speaker
        :arg damping The exponential decay factor of the speaker response
        :arg scale Scale factor to normalize speaker position to desired range (cpu freq)
        """
        self.sample_rate = sample_rate
        self.freq = freq
        self.damping = damping
        self.scale = np.float64(scale)  # TODO: analytic expression

        # See _Signal Processing in C_, C. Reid, T. Passin
        # https://archive.org/details/signalprocessing0000reid/
        dt = np.float64(1 / sample_rate)
        w = np.float64(freq * 2 * np.pi * dt)

        d = damping * dt
        e = np.exp(d)
        c1 = 2 * e * np.cos(w)
        c2 = e * e

        # Square wave impulse response parameters
        b2 = 0.0
        b1 = 1.0

        self.c1 = c1
        self.c2 = c2
        self.b1 = b1
        self.b2 = b2

    def tick(self, y1, y2, x1, x2):
        """ y1, y2: speaker position
        x1,x2: applied voltage
        """
        y = self.c1 * y1 - self.c2 * y2 + self.b1 * x1 + self.b2 * x2
        return y

if False:
    inv_scale = 22400 * 0.07759626164027278
    sample_rate = 1_000_000
    speaker = Speaker(sample_rate, freq=3875, damping=-1210, scale=1 / inv_scale)

    with open("../snd_recs/archon.bin", 'rb') as infile:
        buf = infile.read()

        i  = 0
        while i < len(buf) and buf[i] == 0:
            i = i + 1

        j  = len(buf) - 1
        while j > 0 and buf[j] == 0:
            j = j - 1

        snd_array = np.frombuffer(buf[i:j], dtype='uint8').astype(float)
        snd_array = snd_array/128-1 # make it -1 or +1

        snd_array = snd_array #[0:9000000]

        print(np.min(snd_array), np.max(snd_array))

        y1 = y2 = v1 = v2 = 0
        for i in tqdm(range(snd_array.shape[0])):
            y = speaker.tick(y1, y2, v1, v2)
            v2 = v1
            v1 = snd_array[i]
            y2 = y1
            y1 = y
            snd_array[i] = y

        #plt.plot(snd_array)
        #plt.show()

    import librosa

    # normalize
    snd_array = snd_array - np.mean(snd_array)
    snd_array = snd_array / np.max(np.abs(snd_array))

    downsampled_output = librosa.resample(
            y=snd_array,
            orig_sr=1000000,
            target_sr=44100)

    import soundfile
    soundfile.write('test.wav', downsampled_output*128, 44100, "PCM_16")

    exit()

def read_speaker_record(filepath):
    with open(filepath,"rb") as fin:
        data = fin.read()
        # >u8 == big endian, 8 bytes unsigned integer (64 bits)
        return np.frombuffer(data, dtype=">u8")


def find_zero_runs(a):
    # Create an array that is 1 where a is 0, and pad each end with an extra 0.
    iszero = np.concatenate(([0], np.equal(a, 0).view(np.int8), [0]))
    absdiff = np.abs(np.diff(iszero))
    # Runs start and end where absdiff is 1.
    ranges = np.where(absdiff == 1)[0].reshape(-1, 2)
    return ranges

def find_tops(data, above, min_space=1000):
    # Return the X-position of tops
    # First derivative
    der = np.gradient(data)
    sign_changes = []
    for i in range(der.shape[0]-1):
        EPS = 0.0001
        if data[i] < above:
            continue
        if sign_changes and i - sign_changes[-1] < min_space:
            continue

        # Change of sign in the first derivative
        if (data[i] - data[i-1] > 0 and data[i+1] - data[i] < 0) \
          or (data[i] - data[i-2] > 0 and data[i+2] - data[i] < 0) : # and abs(der[i]-der[i+1]) > EPS:
            # Make sure we get the topmost element
            if  data[i] > data[i+1]:
                sign_changes.append(i)
            else:
                sign_changes.append(i+1)
        elif der[i] < -EPS and der[i+1] > EPS and abs(der[i]-der[i+1]) > EPS:
            if  data[i] < data[i+1]:
                sign_changes.append(i)
            else:
                sign_changes.append(i+1)

    # plt.plot(der)
    # plt.plot(data)
    # plt.scatter(sign_changes, data[sign_changes])
    # plt.show()

    return sign_changes

def mass_spring(x, a,b,c,d=1):
    return d*np.exp(-a*x)*np.sin(b*x+c)


def mass_spring_likelihood(params, xdata, ydata, distr):
    a,b,c,d = params
    #print(a,b,c,d)
    ydata_est = mass_spring(xdata,a,b,c,d)
    residuals = np.abs(ydata_est - ydata)
    #print("-"*80)
    #print(residuals[:10])
    #print(distr.pdf(residuals[:10]))
    return -np.sum(distr.logpdf(residuals))

def mass_spring_MLE_fit(ydata, X0, back_in_time=0):
    xdata = np.arange(ydata.shape[0])
    distr = norm(loc=0,scale=0.25/2) # The location (loc) keyword specifies the mean. The scale (scale) keyword specifies the standard deviation.
    #X0 = [0.003,0.055,1.5+3.14/4,0.15]
    lik_model = minimize(mass_spring_likelihood, x0=X0, bounds=[(0,1), (0,1), (-3.1415,3.1415), (0,1)], args=(xdata,ydata,distr), method='L-BFGS-B')

    if back_in_time > 0:
        xdata = np.arange(-back_in_time, ydata.shape[0])

    return mass_spring(xdata,*lik_model.x)

class SegmentsSlices:
    def __init__(self, data):
        self._segments = dict()
        self._nb_experiments = 0
        self._data = data

        if not FORCE_RELOAD and os.path.exists("slices.pickle"):
            with open("slices.pickle","rb") as fin:
                self._segments = pickle.load(fin)

    def is_loaded(self):
        return len(self._segments) > 0

    def save(self):
        with open("slices.pickle","wb") as fout:
            pickle.dump(self._segments, fout)

    def set(self, segment_nb, slice_nb, begin_pos, end_pos):
        if segment_nb not in self._segments:
            self._segments[segment_nb] = dict()

        self._segments[segment_nb][slice_nb] = (int(begin_pos), int(end_pos))

        if self._nb_experiments == 0:
            self._nb_experiments = slice_nb
        else:
            self._nb_experiments = max(self._nb_experiments, slice_nb)

    def get(self, segment, slice):
        begin_pos, end_pos= self._segments[segment][slice]
        return data[begin_pos: end_pos]

    def get_pos(self, segment, slice):
        return self._segments[segment][slice]

    def get_pos_segment(self, segment):
        mb,me = 9_999_999_999, 0
        for b,e in self._segments[segment].values():
            mb = min(mb,b)
            me = max(me,e)
        return mb, me

    def get_slices(self, slice):
        return [slices[slice] for slices in self._segments.values()]


    def reshape(self):
        for exp_id in tqdm(range(self._nb_experiments)):
            min_len = 9999999999
            for seg_id in self._segments.keys():
                min_len = min(min_len, self.get(seg_id, exp_id).shape[0])

            for seg_id in self._segments.keys():
                for slice_id in self._segments[seg_id].keys():
                    begin_pos, end_pos = self._segments[seg_id][slice_id]
                    self._segments[seg_id][slice_id] = (begin_pos, begin_pos+min_len)

def load_accurapple_speaker(fpath):
    accura_ticks = read_speaker_record("zulu.bin")
    accura_ticks = accura_ticks - np.min(accura_ticks)
    wav = np.zeros( shape=(int(np.max(accura_ticks))+1,)).astype(np.int8)
    s = 1
    for tick in accura_ticks:
        wav[tick] = s
        s = -s
    return accura_ticks.astype(int), wav

def analyse_beacons(data: np.ndarray, filepath: Path):
    c = np.convolve(np.abs(data), np.ones(250), mode="same")
    d = data
    d = d/np.max(d)
    c = c/np.max(c)

    mask = (np.convolve((c > 0.025)*1, [1]*10, mode="same") == 0)
    mask = np.abs(d) < 0.025
    mask = np.abs(c) < 0.05
    d[mask] = 0
    zero_runs = find_zero_runs(d)

    # Keep longest zero runs
    zero_runs2 = []
    for j in tqdm(range(zero_runs.shape[0])):
        b,e = zero_runs[j]
        if e-b > 500:
            zero_runs2.append( (b,e) )

    # What's between the zero (silence) runs are the sound runs
    long_runs = []
    for j in tqdm(range(len(zero_runs2)-1)):
        b,e = zero_runs2[j]
        b2,e2 = zero_runs2[j+1]
        long_runs.append( (e,b2) )

    i = 1
    beacons = []

    # Beacon detection, regular beep, higer beeps
    for j in tqdm(range(len(long_runs))):
        b,e = long_runs[j]
        if e-b > 1500 and e-b < 4000:
            b += np.argmax(d[b:e])
            s = d[b:e]
            s_abs = np.abs(s)
            if np.max(s_abs) >= 0.1 and np.sum(s_abs) > 300:
                beacons.append( (b,e) )

    pickle.dump(beacons, open(filepath, 'wb'))
    return beacons


def load_segments_slices(beacons):
    all_data = SegmentsSlices(data)

    if not all_data.is_loaded():
        len_series = None
        for beacon_num in tqdm(range(len(beacons)-1)):
            b, e = beacons[beacon_num]
            b2,e2=beacons[beacon_num+1]
            seg_start = b + AVERAGE_BEACON_LENGTH
            seg_end = b2-1
            segment = data[seg_start:seg_end]

            silence = np.abs(segment)
            NOISE_LEVEL=0.02
            silence[silence < NOISE_LEVEL] = 0
            zr = find_zero_runs(silence)
            midpoints = []
            for b,e in zr:
                if e - b > 2500:
                    midpoints.append(b+(e-b)//2)

            if False:
                plt.plot(segment,lw=0.5)
                for x in midpoints:
                    plt.axvline(x,c="red")
                plt.show()

            CONV_NOISE_LEVEL=0.005
            intervals_lens = []
            for i in range(len(midpoints)-1):
                b_org,e_org=midpoints[i:i+2]
                # Convolution attenuates noise and makes noise detection easier
                slice1 = segment[b_org:e_org]
                CONV_LEN=41
                conv_seg=np.convolve(slice1,1/CONV_LEN*np.ones((CONV_LEN,)))[CONV_LEN//2:]
                conv_seg_abs = np.abs(conv_seg)
                maxpos = np.argmax(np.abs(conv_seg_abs))

                noise_at_the_beginning = conv_seg_abs[0:int(maxpos*0.8)]
                noise_level_beginning = np.max(noise_at_the_beginning)*1.2

                noise_at_the_end = conv_seg_abs[-((conv_seg_abs.shape[0] - maxpos)//4):]
                noise_level_end = np.max(noise_at_the_end)*1.5

                center = np.mean(np.hstack([noise_at_the_beginning, noise_at_the_end]))
                #first_signal_ndx = maxpos-500+np.min(np.argwhere(np.abs(conv_seg[maxpos-500:maxpos]) > noise_level_beginning)[:,0])
                first_signal_ndx = maxpos - 120 - CONV_LEN
                last_signal_ndx = maxpos+np.max(np.argwhere(np.abs(conv_seg[maxpos:]) > noise_level_end)[:,0])
                if False: # i > len(midpoints)-3:
                    # Helps analysing wave's position
                    plt.title(f"{i}-th slice, zero:{center:.3f}")
                    plt.plot(segment[b_org:e_org],lw=1,c="red")
                    plt.plot(conv_seg[0:first_signal_ndx],c="grey")

                    plt.plot(range(last_signal_ndx, conv_seg.shape[0]),
                            conv_seg[last_signal_ndx:conv_seg.shape[0]],c="grey")
                    # Horiz line to show noise levels
                    plt.plot([0,first_signal_ndx],[noise_level_beginning]*2, c="black", lw=1, ls="--")
                    plt.plot([0,first_signal_ndx],[-noise_level_beginning]*2, c="black",lw=1, ls="--")
                    plt.plot([last_signal_ndx,conv_seg.shape[0]],[noise_level_end]*2, c="black",lw=1, ls="--")
                    plt.plot([last_signal_ndx,conv_seg.shape[0]],[-noise_level_end]*2, c="black",lw=1, ls="--")

                    plt.axvline(first_signal_ndx, c="black")
                    plt.axvline(last_signal_ndx, c="black")
                    plt.show()

                all_data.set(beacon_num, i, seg_start+b_org+first_signal_ndx, seg_start+b_org+last_signal_ndx)

                # print(all_data.get_pos_segment(beacon_num))
                # print(seg_start, seg_end)
                #intervals_lens.append(last_signal_ndx-first_signal_ndx)

            # extremes = find_tops(segment, above=0.37)
            # np_extremes = np.array(extremes, dtype=int)
            # tops_ndx = np.argwhere(segment[np_extremes] > 0.37)[:,0]

            # first_serie = tops_ndx

            # if len_series is None:
            #     len_series = len(tops_ndx)

            # if len(tops_ndx) != len_series:
            #     # All series must have the same number of experiments
            #     plt.plot(segment)
            #     plt.scatter(np_extremes[first_serie[:20]], segment[np_extremes[first_serie[:20]]],marker="x", c="orange")
            #     plt.scatter(np_extremes[first_serie[20:]], segment[np_extremes[first_serie[20:]]],marker="x", c="red")
            #     plt.show()

            # intervals_lens = []
            # for i in range(len(first_serie[:-1])):
            #     # From one top to the other
            #     b,e=np_extremes[first_serie[i]], np_extremes[first_serie[i+1]]
            #     d = int((e-b)*0.9)
            #     intervals_lens.append(d)
            #     all_data.set(beacon_num, i, seg_start+b, seg_start+b+d)
            # # we're off by one, fix that.
            # i += 1
            # d = np.mean(intervals_lens)
            # b = np_extremes[first_serie[i]]
            # all_data.set(beacon_num, i,
            #     seg_start+b,
            #     seg_start+b+d)

        all_data.reshape()
        all_data.save()
    return all_data


logger.info("Loading sound")
data, samplerate = soundfile.read('recording_redmi.wav')
logger.info(f"Done loading sound {samplerate}Hz")
FIRST_TWO_TICKS=22 # 2 for the beacon, 20 regular ticks.

K=100_000
# K=2101_500
# K=4101_500
# K=3100_500
# K=3200_500
# K2 = 3000
# K2 = data.shape[0]
# data = data[0:K2]

BEACONS_FILE = Path("beacons")
if os.path.exists(BEACONS_FILE):
    logger.info("Loading beacons offset from file")
    beacons = pickle.load(open(BEACONS_FILE, 'rb'))
else:
    beacons = analyse_beacons(data, BEACONS_FILE)

AVERAGE_BEACON_LENGTH = int(np.min([e-b for b,e in beacons]))
BEACONS_SEGMENTS = [(beacons[i][0], beacons[i+1][0]) for i in range(len(beacons)-1)]
CYCLES_PER_SECOND=50*20280
CYCLES_PER_SAMPLE=CYCLES_PER_SECOND/samplerate

accura_ticks, accura_wav = load_accurapple_speaker("zulu.bin")
all_data = load_segments_slices(beacons)


if False:
    # Comapre most positive, most negative
    # and compare most negative value and peak value right before it

    all_p = []
    all_starts = []
    all_drops = []
    all_after_drops = []
    for experiment in range(10,20):
        p = []
        drops = []
        after_drops = []
        starts = []
        for sn in range(40,100):
            s = all_data.get(experiment,sn)
            tops = np.array(find_tops(np.abs(s),above=0.2,min_space=10))
            most_positive = tops[0]
            tops = tops[(tops < tops[0]+120) & (tops > tops[0]+30)]
            most_negative = tops[np.argmin(s[tops])]


            # Find peak right before valley
            i = tops[np.argmin(s[tops])]-3
            while True:
                if s[i] > s[i-1] and s[i+1] > s[i+2]:
                    break
                else:
                    i -= 1

            ppm = i
            p.append(s[ppm]-s[most_negative])

            # Find valley on the left of the left peak
            while True:
                if s[i-1] > s[i] and s[i+1] < s[i+2]:
                    break
                else:
                    i -= 1
            ppv = i
            drops.append(s[ppm] - s[ppv])
            after_drops.append(s[most_negative:(most_negative+400)])
            starts.append(s[ppm])

            if False:
                plt.plot(s[:300])
                plt.scatter( [most_positive,most_negative],s[[most_positive,most_negative]],c="red")
                plt.scatter( [ppm,ppv],s[[ppm,ppv]],c="blue")
                plt.show()
        all_p.append(p)
        all_drops.append(drops)
        all_starts.append(starts)
        all_after_drops.append(after_drops)

    if True:
        c = ["red", "blue", "green", "pink", "orange"]
        for after_drops in all_after_drops:
            for d in after_drops:
                for ref_ndx, ref in enumerate([-0.19,-0.26,-0.32,-0.36,-0.43]):
                    if abs(d[0] - ref) < 0.025:
                        plt.plot(d-ref, c=c[ref_ndx], alpha=0.1)

        plt.title("Signal (y-realigned) after drop, grouped by position at drop\n"+\
            "Note the end of signal depends on position at drop\n" + \
            "So postion after drop is a good predictor for wave shape")
        plt.show()

        """ 1. Build up size predicts drop size
            2. Drop size predicts position (of valley)
            3. position of valley predicts wave shape
            => Build up size predicts wave shape
            ?? => ?? time between tick predicts build up size ?
        """

    if True:
        """ Showing that the height of the speaker drop
        is somehow function of the height of the build up that
        precedes it.
        """
        ax = plt.subplot(1,2,1)
        # This predicts the dorp size rather well
        for p in all_drops:
            ax.plot(p,c="red",alpha=0.5,label="Speaker buildup size")
        for p in all_starts:
            ax.plot(p,c="blue",alpha=0.5,label="Speaker Y position at drop start")
        for p in all_p:
            ax.plot(p,c="black",alpha=0.5,label="Speaker drop size")

        # This doesn't prodct the drop size very well
        for p, q in zip(all_drops, all_starts):
            ax.plot(np.array(p)+np.array(q),c="orange",alpha=0.5,label="buildup+position")

        handles, labels = ax.get_legend_handles_labels()
        by_label = dict(zip(labels, handles))
        ax.legend(by_label.values(), by_label.keys())
        ax.set_xlabel("Time between first and second tick (no unit yet)")

        ax = plt.subplot(1,2,2)
        c = ["red", "blue", "green", "orange"]
        for p,v in zip(all_p, all_drops):
            for rng_ndx, rng in enumerate([(0,17),(17,30),(30,45),(45,60)]):
                rb,re = rng
                ax.scatter(p[rb:re],v[rb:re],c=c[rng_ndx],alpha=0.5,label="Speaker drop")
        ax.set_xlabel("Speaker drop size")
        ax.set_ylabel("Speaker build up size")
        plt.show()


if False:
    slices = all_data.get_slices(22)
    for b, e in slices:
        plt.plot(data[b:e],alpha=0.5,c="black")
    plt.show()

if True:
    # Show Apple signal and accurapple "ticks"
    for b,e in all_data._segments[8].values():
        plt.plot(range(b,e), data[b:e])


    first_slice_begin, first_slice_end = list(all_data._segments[8].values())[0]
    first_peak = first_slice_begin + np.argmax(data[first_slice_begin:first_slice_end])
    plt.axvline(first_peak, c="black", lw=1, ls="--")#, y=0.05*np.ones_like(h).astype(float))

    last_slice_begin, last_slice_end = list(all_data._segments[8].values())[-1]
    last_peak = last_slice_begin + np.argmax(data[last_slice_begin:last_slice_end])
    plt.axvline(last_peak, c="black", lw=1, ls="--")#, y=0.05*np.ones_like(h).astype(float))

    b,e = all_data.get_pos_segment(8)
    # plt.plot(data[b:e])
    base = 12740*CYCLES_PER_SAMPLE
    accura_ticks= accura_ticks[(accura_ticks > base) & (accura_ticks < 743420*CYCLES_PER_SAMPLE)]
    accura_ticks -= accura_ticks[0]

    last_tick = accura_ticks[-1]
    ratio = (last_peak - first_peak)/last_tick
    accura_ticks = accura_ticks * ratio
    plt.title(f"Ticks/samples: measured: {1/ratio:.4f} (expected:{CYCLES_PER_SAMPLE:.4f})")

    h=accura_ticks[0::2] # odd
    for x in h: #/CYCLES_PER_SAMPLE:
        plt.axvline(x+first_peak, c="red", lw=1, ls="--")#, y=0.05*np.ones_like(h).astype(float))
    h=accura_ticks[1::2] # even
    for x in h: #/CYCLES_PER_SAMPLE:
        plt.axvline(x+first_peak, c="orange", lw=1, ls="--")#, y=0.05*np.ones_like(h).astype(float))
    #plt.scatter(x=h/(1/samplerate*CYCLES_PER_SECOND)-ofs, y=-0.05*np.ones_like(h).astype(float))
    plt.show()


if False:
    # Show the beacons signals
    deltas = []
    P = ceil(sqrt(1+len(beacons)))
    for i in range(len(beacons)):
        b,e = beacons[i]
        s = data[b:e]
        q = int(np.sum(np.abs(s)))

        ax = plt.subplot(P,P,i+1)
        ax.plot(s, linewidth=0.5, label=f"{q}")
        if i >= 1:
            prev_b, prev_e = beacons[i-1]
            delta = b-prev_b
            deltas.append(delta)
            ax.set_title(f"{delta}")
        ax.legend()
        if i > 0:
            ax.tick_params(top=False, bottom=False, left=False, right=False,
                            labelleft=False, labelbottom=False)

    ax = plt.subplot(P,P,i+1+1)
    ax.plot(deltas)
    plt.suptitle(f"{len(beacons)} beacons")
    plt.show()

if False:
    # Show some segments. We skip the first one because they're
    # not clean (surrounding noise and sound)
    for i in range(5,20):
        plt.plot(data[beacons[i]:beacons[i+1]], linewidth=0.1, alpha=0.3, c="black")
    plt.show()

if False:
    def average_slice(ax, beacons, offset):
        all_slices = []
        W = 150
        for i in range(len(beacons)-1):
            b, e = beacons[i]
            slice = data[b+offset:b+offset+W]
            ndx = np.argmax(slice)
            slice = slice[ndx:ndx+W]
            all_slices.append(slice)
            ax.plot(slice, linewidth=0.3, alpha=0.5, c="black")
        all_slices = np.vstack(all_slices)
        avg = np.average(all_slices,axis=0)
        ax.plot(avg,linewidth=1,c="red")
        ax.set_ylim([-0.5, 0.5])
        ax.axvline(np.argmin(avg))
        ax.axvline(np.argmax(avg))
        ax.axhline(0, c="black", linewidth=0.5)

    for ndx, offset in enumerate([262_000, 722_000, 733_000, 738_000]):
        ax = plt.subplot(2,2,ndx+1)
        average_slice(ax, beacons, offset)
    plt.show()


mean_slice_len = round(np.mean([beacons[i+1][0] - beacons[i][0] for i in range(len(beacons)-1)]))

slices = []
for i in range(len(beacons)-1):
    b = beacons[i][0]
    slices.append(data[b:b+mean_slice_len])

averaged_data = np.mean(np.vstack(slices), axis=0)

b, e = beacons[5][0]+AVERAGE_BEACON_LENGTH,beacons[6][0]
s = data[b:e]
s = averaged_data

filtered_ndx= []
for ndx in np.where(s > 0.3)[0]:
    if not filtered_ndx:
        filtered_ndx.append(ndx)
    elif abs(filtered_ndx[-1] - ndx) > 1000:
        filtered_ndx.append(ndx)
    elif s[filtered_ndx[-1]] < s[ndx]:
        s[filtered_ndx[-1]] = s[ndx]



if False:
    # Comparing the signal right after a tick.
    beacon = beacons[2]
    segment = data[beacon[0]:beacon[0]+AVERAGE_BEACON_LENGTH]
    extremes = find_tops(segment)

    # Indices of extreme values
    np_extremes = np.array(extremes, dtype=int)
    tops_ndx = np.argwhere(segment[np_extremes] > 0.16)
    bottoms_ndx = np.argwhere(segment[np_extremes] < -0.16)

    # Last top
    last_bottom_ndx = bottoms_ndx[-1]

    # Where the go-to bottom starts
    x1 = np_extremes[last_bottom_ndx-1][0]

    # Reaching the bottom, the spring now pulls everything up
    x2 = np_extremes[last_bottom_ndx][0]

    # Reaching the top, the spring now pushes everything down
    x3 = np_extremes[last_bottom_ndx+1][0]

    print([x1,x2,x3])
    print(segment[[x1,x2,x3]])
    print(f"{segment[x1]-segment[x2]} - {segment[x2] - segment[x3]}")


    tops = list(filter( lambda ndx: segment[ndx] > 0.16, extremes))
    bottoms = list(filter( lambda ndx: segment[ndx] < -0.16, extremes))
    tops = tops[len(tops)-4:]
    bottoms = bottoms[len(bottoms)-4:]
    plt.plot(segment,c="grey",lw=1)
    for i in (1,3):
        r = range(tops[i],tops[i]+91)
        plt.plot(r, segment[r])

    back_in_time=106
    fit = mass_spring_MLE_fit(segment[2141:],X0=[1.0,1.0,1.0,1.0],back_in_time=back_in_time)
    r = range(2141-back_in_time,len(segment))
    plt.plot( r, fit)
    plt.plot( r, segment[r]-fit)
    # plt.scatter(tops, segment[tops], marker="x", c="red")
    # plt.scatter(bottoms, segment[bottoms], marker="x", c="red")
    #plt.scatter(np_extremes, segment[np_extremes], marker="x", c="red")
    plt.scatter([x1,x2,x3], segment[[x1,x2,x3]], marker="x", c="red")
    plt.plot([x1,x2],segment[[x1,x2]], c="black")
    plt.plot([x2,x3],segment[[x2,x3]], c="black")
    plt.xlim((tops[0],3500))
    plt.axhline(0,c="black",lw=1,ls="--")
    plt.show()
    exit()


if False:
    plt.plot(s)
    plt.scatter(filtered_ndx, s[filtered_ndx], c="orange")
    plt.show()

if False:
    # Analyse the rising edge of each pulse in each beacon.
    all_slices= []
    for begin, end in tqdm(BEACONS_SEGMENTS):
        slice = data[(begin):(begin + AVERAGE_BEACON_LENGTH)]
        sclip = slice.copy()
        sclip[sclip < np.max(sclip)*0.6] = 0
        splits = find_tops(sclip)
        for s in splits[len(splits)-3:]:
            subslice = slice[s-40:min(s+250,len(slice))]
            plt.plot(subslice,c="black",alpha=0.01)
            all_slices.append(subslice)

    avg = np.average( np.vstack(all_slices), axis=0)
    plt.axhline(-0.1)
    plt.axhline(0.1)
    plt.plot(avg, c="yellow", ls="--")

    #r=np.arange(40,120)
    #plt.plot(r,mass_spring(r-40,0.06,0.32,1.1,0.4)-0.05,c="red")

    r=np.arange(49,87)
    X0 = [0.04,0.30,-3.1415/4, 0.20]
    y_offset = 0.075
    plt.plot(r, mass_spring(r-np.min(r),*X0)-y_offset,c="blue")
    plt.plot(r, mass_spring_MLE_fit(avg[r]+y_offset, X0)-0.05, c="green")
    plt.show()

if False:
    cmap = plt.get_cmap("jet")
    logger.info(len(filtered_ndx))
    N=20
    for plt_ndx, bg in enumerate([0,50,99-N]):
        ax = plt.subplot(2,2,plt_ndx+1)
        for i in range(0,N,3):
            ndx = FIRST_TWO_TICKS+bg+i
            b,e = filtered_ndx[ndx]-4, filtered_ndx[ndx+1]
            ln = min(e-b,750)
            musec = (100-(bg+i))*25 # Assembler code's pause.
            extract = s[b:b+ln]

            if bg == 0 and i == 0:
                logger.info("Saving extract")
                soundfile.write('/tmp/extract.wav', extract, samplerate)

            #extract = np.convolve(extract, [1]*5)
            #extract = np.cumsum(extract)
            ax.plot(extract , c=cmap(i/N), linewidth=1, label=f"Δ≈{musec}µs ({musec/(1_000_000/samplerate):.1f} S)")
        ax.set_xlabel(f"Sample (S=1/{samplerate} s)")
        ax.set_ylabel("Amplitude")
        for i in [-0.25,0,0.25]:
            ax.axhline(i, linewidth=1, linestyle="--")
        ax.legend(loc='lower right')
    plt.suptitle("Two speaker ticks separated by Δµs")
    plt.show()


if False:
    from scipy.fft import rfft, rfftfreq

    ndx = 20
    b,e = filtered_ndx[ndx]-4, filtered_ndx[ndx+1]
    ln = min(e-b,750)
    extract = s[b:b+ln]

    # Note the extra 'r' at the front
    yf = rfft(extract/np.max(extract))
    xf = rfftfreq(extract.shape[0], 1 / samplerate)

    ax = plt.subplot(1,2,1)
    ax.plot(extract)
    ax = plt.subplot(1,2,2)
    ax.plot(xf, np.abs(yf))
    plt.show()

s = averaged_data
ndx = 20
b,e = filtered_ndx[ndx]-4, filtered_ndx[ndx+1]
ln = min(e-b,750)
extract = s[b:b+ln]


if False:
    # Trying to fit various parts of a single tick

    # xdata = np.arange(extract.shape[0])
    # ydata = extract
    BREAK=196

    # avg

    base_ydata = None
    N = 14
    N_SLICE = 8
    for i in range(2,2+N):
        if base_ydata is None:
            base_ydata = all_data.get(4,N_SLICE)
        else:
            base_ydata += all_data.get(4,N_SLICE)
    base_ydata = base_ydata * 1/N

    #base_ydata = all_data.get(4,4)
    plt.plot(np.arange(base_ydata.shape[0]-BREAK), base_ydata[:-BREAK], label="Speaker")


    # Fit the end of data (region of about 3.6KHz)
    ydata = base_ydata[BREAK:]
    xdata = np.arange(ydata.shape[0])
    popt, pcov = curve_fit(mass_spring, xdata, ydata)
    lowfreq_fit_y = mass_spring(xdata-BREAK,*popt)
    plt.plot(xdata, lowfreq_fit_y,label="Fit over speaker")

    BREAK2 = 133 #142 #124 # 132
    speaker_minus_fit = (base_ydata[:-BREAK] - lowfreq_fit_y)[BREAK2:235]
    plt.plot(np.arange(BREAK2,235), speaker_minus_fit, label="Speaker - fit")

    #plt.plot(base_ydata[:-BREAK] - lowfreq_fit_y, label="Speaker - fit -part2")

    xdata = np.arange(speaker_minus_fit.shape[0])
    popt2, pcov2 = curve_fit(mass_spring, xdata, speaker_minus_fit)
    fit_y = mass_spring(xdata,*popt2)
    plt.plot(xdata+BREAK2,fit_y,label="Fit over speaker - fit")

    #plt.plot(xdata+BREAK2,fit_y,label="Fit2 over speaker - fit")

    plt.legend()
    plt.show()

    xdata = np.arange(-65,1100)
    xdata2 = np.arange(0,1000)
    ymodel = mass_spring(xdata,*popt)
    ymodel2 = mass_spring(xdata2,*popt2)

    yall = np.zeros((2500,))
    yall[196-65:196-65+ymodel.shape[0]] += ymodel
    yall[133:133+ymodel2.shape[0]] += ymodel2
    plt.plot(yall)
    plt.plot(base_ydata)
    plt.show()



if False:
    # :25,  [0.003,0.055,-0,0.15]
    ndx = 10
    b,e = filtered_ndx[ndx]-4, filtered_ndx[ndx+1]
    ln = min(e-b,750)
    extract = s[b:b+ln]
    if ndx >= FIRST_TWO_TICKS:
        musec = (100-(ndx-FIRST_TWO_TICKS))*25 # Assembler code's pause.
    else:
        musec = "/"

    ydata = extract[5:]
    xdata = np.arange(ydata.shape[0])
    distr = norm(loc=0,scale=0.25) # The location (loc) keyword specifies the mean. The scale (scale) keyword specifies the standard deviation.
    X0 = [0.003,0.055,1.5+3.14/4,0.15]
    lik_model = minimize(mass_spring_likelihood, x0=X0, bounds=[(0,1), (0,1), (-3.1415,3.1415), (0,1)], args=(xdata,ydata,distr), method='L-BFGS-B')
    print(lik_model)
    plt.scatter(xdata,ydata, s=0.5, c="red", label="Data")
    plt.plot(xdata,ydata, c="red", lw=0.5)

    """
    # I measure the x-distance between the mins/maxs of the curve,
    # in the beginning of the curve. This plot shows that they
    # are more or less separated by the same distnace.

    d <- c(11.2, 20, 31.3, 42, 48.3, 57.1, 61.5, 70.9, 75.3, 84.7)
    linreg <- lm(d ~ seq(length(d)))
    plot(d)
    abline( linreg$coefficients)
    linreg$coefficients
    """

    #plt.plot(xdata, func(xdata,*lik_model.x), label="exp()*sin() fit")
    #plt.plot(xdata, func(xdata, *X0), linewidth=1, label="Manual fit")
    #plt.plot(xdata, func(xdata, 0.05,0.34,1,0.35))
    plt.plot(xdata, mass_spring(xdata, 0.02, 0.38, 1, 0.25)+mass_spring(xdata,*lik_model.x), label="Model")

    der = np.gradient(ydata)
    second_der = []
    for i in range(der.shape[0]-1):
        if der[i]*der[i+1] < 0:
            second_der.append(i)
    x = np.array(second_der)
    print(x[1:] - x[0:-1])
    plt.scatter(second_der, ydata[second_der])
    plt.title(f"Δ={musec}µs")
    plt.xlim((0,100))
    plt.legend()
    plt.show()
