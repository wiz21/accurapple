lam <- 1
m <- 1
beta <- 0.7

y <- function (x,h,v0,V) {
    delta <- -( (lam/m)**2 - 4*beta/m )
    delta_over_2 <- sqrt(delta)/2
    a <- (h - V)/2
    b <- -(v0 + a*lam/m)/sqrt(delta)
    2*exp(-lam/(2*m)*x) * (a*cos(delta_over_2*x) -b*sin(delta_over_2*x)) + V
}

dydx <- function (x,h,v0,V) {
    a <- (h - V)/2
    delta <- -( (lam/m)**2 - 4*beta/m )
    b <- -(v0 + a*lam/m)/sqrt(delta)
    lm2 <- -lam/(2*m)
    sd2 <- sqrt(delta)/2
    e <- 2*exp(lm2*x)
    c <- cos(sd2*x)*(a*lm2 - b*sd2)
    s <- sin(sd2*x)*(a*sd2 + b*lm2)
    e*(c-s)
}

x <- seq(0,100)/10
plot(y(x,h=-1,v0=1,V=1), type="l")
lines(y(x,h=-1,v0=2,V=1))
lines(y(x,h=-0.5,v0=-1,V=1),col="red")
lines(dydx(x,h=-0.5,v0=-1,V=1), lty="dashed",col="red")
abline(h=V)


x <- seq(0,100)/10
plot(x, y(x,h=-1,v0=1,V=1), type="l", xlim=c(0,10))
x <- seq(0,70)/10
lines( seq(0,70)/10 +(30/10),
      y(x,h=y(3,h=-1,v0=1,V=1),v0=dydx(3,h=-1,v0=1,V=1),V=1),
      lty="dashed", col="red", lw=2)
lines( seq(0,70)/10 +(30/10),
      y(x,h=y(3,h=-1,v0=1,V=1),v0=dydx(3,h=-1,v0=1,V=1),V=0),
      col="blue", lw=2)
