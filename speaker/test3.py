"""
In this test, I check that what the mic records is acutally the derivative
of the signal emitted by the Apple2.
Conclusion: it is *absolutely not* that ('cos it doesn't sound the
same at all).
"""

import numpy as np
from matplotlib import pyplot as plt
from utils import *

wav, sample_rate = soundfile.read("recordings/test7/test7_webcam.wav")

# Find the ticks
ticks = wav[2_689_000:2_960_000]
g = np.abs(np.gradient(ticks))
plt.plot(g)
tick_starts = []
for x in np.ravel(np.argwhere(g > 0.01)):
    if g[x-1] < 0.01 and g[x-2] < 0.01 and np.max(g[x-1:x+1000]) > 0.1:
        plt.axvline(x-1, lw=0.5, c="red")
        tick_starts.append(x)
plt.show()

# Take the mean of all ticks (notice they're
# all quite close to the mean)

tick_mean = None
for ts in tick_starts:
    seg = ticks[ts:ts+2000]
    seg = seg - np.mean(seg)
    if tick_mean is None:
        tick_mean = seg
    else:
        tick_mean += seg
    plt.plot(seg)
tick_mean /= len(tick_starts)
plt.show()

# Show the mean
plt.plot(tick_mean)
plt.plot(np.cumsum(tick_mean))
plt.axvline(sample_rate/3850, lw=1, c="red")
plt.show()

# Create a sound based on the fact that we have the derivative
# of the Apple2 signal.

snd = np.zeros( (sample_rate*10,) )

for i in range(10*8):
    ofs = int(sample_rate*i/8)
    seg = np.cumsum(tick_mean)
    #seg = tick_mean
    snd[ofs:ofs+seg.shape[0]] = seg

# Test the sound
normalize_wav_and_save(snd, "test_tick.wav", sample_rate)
