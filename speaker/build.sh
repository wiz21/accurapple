#!/bin/sh

acme speaker_test2.a
acme -o noise.o noise.a

RESULT=$?
if [ $RESULT -eq 0 ]; then
    java -jar AppleCommander-1.3.5.13-ac.jar -d NEW.DSK STARTUP
    #java -jar AppleCommander-1.3.5.13-ac.jar -p NEW.DSK STARTUP BIN 0xC00 < STARTUP
    java -jar AppleCommander-1.3.5.13-ac.jar -p NEW.DSK STARTUP BIN 0xC00 < noise.o
    /usr/bin/python3 dsk2nic.py --source NEW.DSK --target SPKR.NIC
    echo Copying to SD card
    cp SPKR.NIC /media/stefan/0AAF-5E6C/SPKR.NIC
    echo Unmounting
    umount /media/stefan/0AAF-5E6C

    # TURBO_START
    export RUST_LOG="accurapple=info,accurapple::sound=error" ; /mnt/data2/rust_target/release/accurapple  --script "LOAD_MEM noise.o 0C00, SET_PC 0C00, SOUND_REC_START, SPEAKER_REC_START, WAIT_UNTIL 450, SPEAKER_REC_STOP recordings/noise, SOUND_REC_STOP recordings/noise, QUIT" --no-greetings
    # export RUST_LOG="accurapple=info,accurapple::sound=error" ; /mnt/data2/rust_target/release/accurapple --no-greetings --script "LOAD_MEM noise.o 0C00, SET_PC 0C00"
fi
