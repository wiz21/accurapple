from math import sin, exp, cos
import matplotlib.pyplot as plt

delta_x = 0.01
t = 0
phi = 1
d = 3.1415*5
u = -2.3

x = 0
s1 = exp(u*x)*sin(d*x+phi)
s2 = exp(u*(x+delta_x))*sin(d*(x+delta_x)+phi)
y = [s1, s2]
K1 = exp(2*u*delta_x)
K2 = exp(u*delta_x)*sin(2*d*delta_x)/sin(d*delta_x)
for i in range(1000):
    s3 = -s1*K1 + s2*K2
    y.append(s3)
    s1 = s2
    s2 = s3

y2 = []
for i in range(1000):
    x = i*delta_x
    y2.append( exp(u*x)*sin(d*x+phi))


plt.plot(y)
plt.plot(y2,ls="--")
plt.show()
