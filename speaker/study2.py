import numpy as np
import matplotlib.pyplot as plt
from utils import read_speaker_record, CPU_FREQ

ticks = read_speaker_record("recordings/noise.ticks")
tbt = ticks[1:] - ticks[:-1]
stops = np.argwhere(tbt > 50000).flatten()
print(stops)

for i in range(len(stops)-1):
    seg = ticks[ stops[i]+1: stops[i+1]+1]
    tbt = seg[1:]-seg[:-1]
    if len(tbt) == 0:
        break
    assert np.all( tbt == tbt[0] )
    print(f"Period: {tbt[0]} cycles, nb ticks:{stops[i+1]-stops[i]} , freq={CPU_FREQ/tbt[0]:.1f} Hz")
