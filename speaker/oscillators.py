from tqdm import tqdm
import numpy as np
#from numba import jit
from math import sqrt, exp, sin, cos
import matplotlib.pyplot as plt

SRC_FREQ=50*20280

def load_accurapple_speaker(filepath):
    with open(filepath,"rb") as fin:
        data = fin.read()
        # >u8 == big endian, 8 bytes unsigned
        # integer (64 bits)
        accura_ticks = np.frombuffer(data, dtype=">u8")

    # Synthetic test:
    #accura_ticks = np.arange(0,1_000_000,2_000)

    accura_ticks = accura_ticks - np.min(accura_ticks)
    if args.short is not None:
        accura_ticks = accura_ticks[accura_ticks < int(args.short)]

    wav = np.ones( shape=(int(np.max(accura_ticks))+1,), dtype=float)
    for i in range(0,(len(accura_ticks) // 2) * 2,2):
        b,e = accura_ticks[i:i+2]
        wav[b:e] = -1.0
    return accura_ticks, np.array(wav)


class Speaker:
    """Simulates the response of the Apple II speaker."""

    # TODO: move lookahead.evolve into Speaker method

    def __init__(self, sample_rate: float, freq: float, damping: float,
                 scale: float):
        """Initialize the Speaker object
        :arg sample_rate The sample rate of the simulated speaker (Hz)
        :arg freq The resonant frequency of the speaker
        :arg damping The exponential decay factor of the speaker response
        :arg scale Scale factor to normalize speaker position to desired range (cpu freq)
        """
        self.sample_rate = sample_rate
        self.freq = freq
        self.damping = damping
        self.scale = np.float64(scale)  # TODO: analytic expression

        # See _Signal Processing in C_, C. Reid, T. Passin
        # https://archive.org/details/signalprocessing0000reid/
        dt = np.float64(1 / sample_rate)
        w = np.float64(freq * 2 * np.pi * dt)

        # Equation 3.17 in the book (page 37)
        d = damping * dt
        e = np.exp(d)
        c1 = 2 * e * np.cos(w)
        c2 = e * e

        # Square wave impulse response parameters
        b2 = 0.0
        b1 = 1.0

        self.c1 = c1
        self.c2 = c2
        self.b1 = b1
        self.b2 = b2

    def tick(self, y1, y2, x1, x2):
        """ y1, y2: speaker position
        x1,x2: applied voltage
        """
        y = self.c1 * y1 - self.c2 * y2 + self.b1 * x1 + self.b2 * x2
        return y


def kennaway(square_wave):
    inv_scale = 22400 * 0.07759626164027278
    sample_rate = SRC_FREQ
    speaker = Speaker(sample_rate, freq=3875, damping=-1210, scale=1 / inv_scale)

    wav = np.zeros_like(square_wave)

    y1 = y2 = v1 = v2 = 0
    for i in tqdm(range(square_wave.shape[0])):
        y = speaker.tick(y1, y2, v1, v2)
        v2 = v1
        v1 = square_wave[i]
        y2 = y1
        y1 = y
        wav[i] = y

    return np.array(wav)



#@jit(nopython=True)
def kena2_helper(ticks):
    m = 1
    # lam = 1
    # beta = 0.7

    #  -lambda/2 ==? dampen
    #  lambda = -2*dampen
    # If lambda is big, then exp(-lam*...) is small => stronger dampening
    lam=0.002353
    #beta=0.2288*0.015
    freq = 6.28*1/(SRC_FREQ/3875) # Hertz
    beta = (4*(freq**2) + lam**2)/4
    K = 21.159

    delta = -( (lam/m)**2 - 4*beta/m )
    assert delta >= 0
    sqrt_delta_over_2 = sqrt(delta)/2
    print(delta)
    print(1/sqrt_delta_over_2)

    def oscillator(x,h,v0,V):
        a = (h - V)/2
        b = -(v0 + a*lam/m)/sqrt(delta)

        r= 2*np.exp(-lam/(2*m)*x) * (a*np.cos(sqrt_delta_over_2*x) - b*np.sin(sqrt_delta_over_2*x)) + V
        return r

        # a_star, b_star, c_star, d_star = [0.06, 0.48, +0.25*3.1415, 0.8]
        # lam_over_m = 2*a_star
        # beta_over_m = (4*(b_star**2)-(2*a_star)**2)/4

        # return d_star * exp(-0.5*lam_over_m*x) * sin( b_star*x + c_star)

    def oscillator_dydx(x,h,v0,V):
        a = (h - V)/2
        b = -(v0 + a*lam/m)/sqrt(delta)

        lm2 = -lam/(2*m)
        e = np.exp(lm2*x)
        c = np.cos(sqrt_delta_over_2*x)*(a*lm2 - b*sqrt_delta_over_2)
        s = np.sin(sqrt_delta_over_2*x)*(a*sqrt_delta_over_2 + b*lm2)
        return 2*e*(c-s)


    voltage = 1
    V = voltage
    h, v0 = 0,0
    current_tick_ndx = 0
    sound = []
    deriv = []

    while current_tick_ndx < ticks.shape[0]-1:
        #sound.append(h)

        x_start = ticks[current_tick_ndx]
        x_end = ticks[current_tick_ndx+1]

        V = voltage
        dx = 0
        while x_start + dx < x_end:
            if dx <= 1 or dx >= x_end-x_start-2:
                print(f"loop: dx={dx} v0={v0}, osc_dydx={oscillator_dydx(dx,h,v0,V)}")
            sound.append( oscillator(dx,h,v0,V) )
            deriv.append( oscillator_dydx(dx,h,v0,V) )
            dx += 1
        print(f"loop+: dx={dx} v0={v0}, osc_dydx={oscillator_dydx(dx,h,v0,V)}")

        old_v0, old_h = v0, h
        v0 = oscillator_dydx(dx,old_h,old_v0,V)
        h = oscillator(dx,old_h,old_v0,V)

        voltage = 1 - voltage

        #space = x_end - x_start
        # if voltage == 1:
        #     if space < 1000*K:
        #         V = 1
        #     elif 1000*K < space < 750000*K:
        #         V = 1 - min((space - 1000),100_000)/100_000
        #     else:
        #         V = 0
        # else:
        #     V = 0

        current_tick_ndx += 1

    return np.array(sound), np.array(deriv)



#file_path = "recordings/archon.ticks"
#ticks, wav_ticks = load_accurapple_speaker(file_path)
B=980
ticks = np.array([0,B,B+1000])
wav_ticks = np.array([1]*B + [0]*1000)
kena1 = kennaway(wav_ticks) * 1/3000
kena2, deriv = kena2_helper(ticks)

kena1 = kena1 / np.max(kena1)
kena2 = kena2 / np.max(kena2)

plt.plot(kena1,label="Kena1")
plt.plot(np.array(kena2),label="Kena2")
plt.plot(deriv,label="deriv")
#plt.plot(np.gradient(kena2), label="grad")
plt.axhline(0,c="black",lw=1)
plt.legend()
plt.show()
