import soundfile
import numpy as np
import matplotlib.pyplot as plt
import librosa

CYCLES_PER_SECOND=50*20280
K = 21.159039269541
N = 16000
NBASE="archon"


with open(f"recordings/{NBASE}.ticks","rb") as fin:
    data = fin.read()
    # >u8 == big endian, 8 bytes unsigned
    # integer (64 bits)
    ticks = np.frombuffer(data, dtype=">u8")
    ticks = ticks - ticks[0]

ticks = ticks[ticks < N]
kena = np.load(f"recordings/{NBASE}.ticks_kena_onemhz.npy") / 18000
kena2 = np.load(f"recordings/{NBASE}.ticks_kena2_onemhz.npy")

wav, samplerate = soundfile.read(f"recordings/{NBASE}_webcam.wav")
wav = wav[:int(N*K)]

upsampled_wav = librosa.resample(
    y=wav,
    res_type='linear',
    orig_sr=samplerate,
    target_sr=CYCLES_PER_SECOND)

upsampled_wav = -0.5*(upsampled_wav - upsampled_wav[0])

kena = kena[0:N]
kena2 = kena2[0:N]

plt.xlim(-5,N)
plt.title(NBASE)
plt.plot(kena,label="Kena",c="blue")
plt.plot(kena2,label="Accurapple",c="black")
plt.plot(upsampled_wav,label="Original",c="green")

plt.step(ticks, ([0.0,0.2]*(ticks.shape[0]//2+1))[:ticks.shape[0]], c="red", lw=1,alpha=0.5)
plt.legend()
plt.xlabel("Cycles")
plt.show()
