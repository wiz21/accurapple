import time
from datetime import datetime
from math import sin, cos, exp, log, pi, sqrt, atan
import numpy as np
import soundfile
from pathlib import Path
from numba import jit
import pyfftw

CPU_FREQ=20280*50
CYCLES_PER_SECOND=CPU_FREQ
TARGET_FREQ=44100


def read_speaker_record(filepath):
    with open(filepath,"rb") as fin:
        data = fin.read()
        # >u8 == big endian, 8 bytes unsigned integer (64 bits)
        return np.frombuffer(data, dtype=">u8")

def load_accurapple_ticks(filepath):
    # Returns ticks (1MHz)

    with open(filepath,"rb") as fin:
        data = fin.read()
        # >u8 == big endian, 8 bytes unsigned
        # integer (64 bits)
        accura_ticks = np.frombuffer(data, dtype=">u8")

    return accura_ticks

def normalize_wav_and_save(wav, outfile_path, sample_rate=TARGET_FREQ, peak=1):
    wav = wav - np.mean(wav)
    wav = wav / np.max(np.abs(wav)) * peak

    # Fade out
    N = int(sample_rate/10)
    wav[wav.shape[0]-N:wav.shape[0]] = wav[wav.shape[0]-N:wav.shape[0]] * np.linspace(1,0,N)

    soundfile.write(outfile_path, wav, sample_rate, "PCM_16")
    print(f"Saved {outfile_path}")
    return wav


@jit(nopython=True)
def fast_oscillator(ticks, sho_freq=3850, voltage=1, h0=0, v0=0, nb_loops=12, weight=1, attenuation=0.01, engine=1):
    weight = weight
    nb_loops = nb_loops
    friction_attenuation = attenuation
    lambda_over_m = 2 * (log(2) - log(friction_attenuation))/ (nb_loops/sho_freq)
    beta_over_m = (lambda_over_m**2 + (4*pi*sho_freq)**2)/4
    delta = 4*beta_over_m - lambda_over_m**2
    dx = 1/CPU_FREQ
    p1 = exp(2*(-lambda_over_m/2)*dx)
    p2 = 2*exp((-lambda_over_m/2)*dx)*cos(sqrt(delta)/2*dx)

    def set_params(h0, v0, voltage):
        V = voltage
        a = (h0-V)/2
        b = - ( v0 + a*lambda_over_m) / sqrt(delta)
        return a,b,V

    def oscillator(x, a, b, V):
        xp = exp(-lambda_over_m/2*x)
        c = a*cos(sqrt(delta)/2*x)
        s = b*sin(sqrt(delta)/2*x)
        y = 2.0 * xp * (c - s) + V
        return y

    def oscillator_dydx(x, a, b, V):
        lm2 = -lambda_over_m/2
        xp = exp(lm2*x)
        c = cos((sqrt(delta)/2 * x)) * (a * lm2 - b * sqrt(delta)/2)
        s = sin((sqrt(delta)/2 * x)) * (a * sqrt(delta)/2 + b * lm2)
        y_prime = 2.0 * xp * (c - s)
        return y_prime

    def evaluate_range(steps, storage, a, b, V):
        for t in range(steps):
            storage.append(oscillator(t/CPU_FREQ, a, b, V))

    def evaluate_range3(steps, storage, s1, s2, V):
        for t in range(steps):
            s3 = -p1*s1 + p2*s2
            storage.append(s3 + V)
            s1, s2 = s2, s3


    a, b, V = set_params(0, 0, voltage)
    k = 2*sqrt(a*a+b*b)
    old_V = V
    old_k = k
    current_tick_ndx = 0
    sound = []

    while current_tick_ndx < ticks.shape[0]-1:
        x_start = ticks[current_tick_ndx]
        x_end = ticks[current_tick_ndx+1]
        dx = x_end - x_start

        if engine == 1:
            evaluate_range(dx, sound, a, b, V)
            h_end = oscillator(dx/CPU_FREQ, a, b, V)
            v_end = oscillator_dydx(dx/CPU_FREQ, a, b, V)

        elif engine == 2:
            if not sound:
                sound.append(h0)
                sound.append(h0+(1/CPU_FREQ)*oscillator_dydx(0,a,b,V))
                evaluate_range3(dx-2, sound, (sound[-2]-old_V)/k, (sound[-1]-old_V)/k,V)
            else:
                # First we need to get the old s1 and s2 as they were computed
                # in the previous iteration:
                # s1 = (sound[-1]-old_V)
                # s2 = (sound[-2]-old_V)
                # These values had initial conditions such that
                # s1 = (sound[-1]-old_V)/old_k
                # Now we have another set of I.C. (hence a new k) and then we have to update that
                # s1 becomes = (sound[-1]-old_V)/old_k * k
                # These s1,s2 were the position relative to a force old_V (and
                # the associated equlibirum position). So we have to translate
                # them so that they are at the right distance of V.
                # If they were at +4 of old_V and old_V is 1, then the
                # real position old_y is 5
                # Now if the new V is -1, the distance
                evaluate_range3(dx, sound,
                                ((sound[-2]-old_V)/old_k*k - V+old_V),
                                ((sound[-1]-old_V)/old_k*k - V+old_V), V)
            old_V = V
            old_k = k
            h_end = v_end = 0

        voltage = - voltage
        # if current_tick_ndx >= 1:
        #     w = ticks[current_tick_ndx+1] - ticks[current_tick_ndx]
        #     if w == 8:
        #         vv = voltage * 30
        #     else:
        #         vv = voltage
        # else:
        #     vv = voltage

        vv = voltage
        a, b, V = set_params(h_end, v_end, vv)
        k = 2*sqrt(a*a+b*b)


        current_tick_ndx += 1

    return np.array(sound)


class Oscillator:
    def __init__(self, sho_freq, V, h0, v0, nb_loops, weight=1, t_to_zero=0.01):

        self.weight = weight
        self.nb_loops = nb_loops
        self.frequency = sho_freq
        self.V = V
        self.lambda_over_m = 2 * (log(2) - log(t_to_zero))/ (self.nb_loops/sho_freq)
        beta_over_m = (self.lambda_over_m**2 + (4*np.pi*sho_freq)**2)/4

        self.a = (h0-V)/2
        self.delta = 4*beta_over_m - self.lambda_over_m**2
        self.b = - ( v0 + self.a*self.lambda_over_m) / sqrt(self.delta)


    def evaluate(self, x):
        xp = 2 * self.weight * np.exp(-self.lambda_over_m*x/2)
        # That's the original function
        c = self.a*np.cos(sqrt(self.delta)/2*x)
        s = self.b*np.sin(sqrt(self.delta)/2*x)
        y = xp * (c - s) + self.V

        # Simplified version
        # y_atan = xp * sqrt((a*a) + (b*b)) * np.sin( sqrt(delta)/2*x - atan(a/b)  ) + V
        return y

    def derivative(self, x):
        lm2 = -self.lambda_over_m/2
        a = self.a
        b = self.b

        e = np.exp((lm2 * x))
        c = np.cos((sqrt(self.delta)/2 * x)) * (a * lm2 - b * sqrt(self.delta)/2)
        s = np.sin((sqrt(self.delta)/2 * x)) * (a * sqrt(self.delta)/2 + b * lm2)
        return 2.0 * self.weight * e * (c - s)


class OscillatorRenderer:
    def __init__(self, v, sho, tick_attenuation = None):
        self.V_start = v
        self.V = v
        self.sho = sho
        self.last_tick = None
        self.chunks = []
        self.tick_attenuation = tick_attenuation

    def render_ticks(self, ticks):
        chunks = []
        for nt in (range(len(ticks))):
            next_tick = ticks[nt]
            if self.last_tick is not None:
                period = (next_tick - self.last_tick)
                xs = np.linspace(0,(period-1)/CYCLES_PER_SECOND,period).astype(np.float32)
                chunks.append(self.sho.evaluate(xs))

                end_of_period = (next_tick - self.last_tick)/CYCLES_PER_SECOND
                v0 = self.sho.derivative(end_of_period)

                if self.tick_attenuation:
                    T = self.tick_attenuation
                    dt = self.V_start*min(next_tick - self.last_tick,T)/T
                    v0 = v0 * min(next_tick - self.last_tick,T)/T
                    assert dt > 0
                    if self.V < 0:
                        v = self.V + dt
                    else:
                        v = self.V - dt
                else:
                    v = -self.V

                self.sho = Oscillator(self.sho.frequency,
                                      v, self.sho.evaluate(end_of_period),
                                      v0,
                                      nb_loops=self.sho.nb_loops)
                self.V = v

            self.last_tick = next_tick
            #self.V = - self.V
        self.chunks = np.hstack(self.chunks + chunks)
        return self.chunks

def load_wav(f, as_mono=True):
    wav, samplerate = soundfile.read(str(f))
    if len(wav) == 2 and as_mono:
        # Mix left and right channels
        wav = wav[:,0] + wav[:,1]
    return wav, samplerate


class WavPair:
    def __init__(self, real, accurapple, sample_rate_real=None, sample_rate_accurapple=None, label= None):
        self.label = label

        def is_a_path(p):
            return type(p) == str or isinstance( p, Path)

        if is_a_path(real) and is_a_path(accurapple):
            self._real_wav, self._real_samplerate = soundfile.read(str(real))
            self._accurapple_wav, self._accurapple_samplerate = soundfile.read(str(accurapple))
        else:
            self._real_wav, self._real_samplerate = real, sample_rate_real
            self._accurapple_wav, self._accurapple_samplerate = accurapple, sample_rate_accurapple

        assert type(self._real_wav) == np.ndarray, f"Wrong type : {type(self._real_wav)}"
        assert self._real_samplerate > 0
        assert type(self._accurapple_wav) == np.ndarray
        assert self._accurapple_samplerate > 0

        print(f"WavPair: real:{self._real_samplerate} Hz Accurapple {self._accurapple_samplerate} Hz")

        if len(self._real_wav.shape) == 2:
            self._real_wav = self._real_wav[:,0]
        if len(self._accurapple_wav.shape) == 2:
            self._accurapple_wav = self._accurapple_wav[:,0]

        self.real_start, self.real_end = 0, self._real_wav.shape[0]
        self.accurapple_start, self.accurapple_end = 0, self._accurapple_wav.shape[0]

    def set_real_slice(self, start, end):
        self.real_start, self.real_end = start, end

    def set_accurapple_slice(self, start, end):
        self.accurapple_start, self.accurapple_end = start, end

    def cut_and_resample(self):
        real_wav=self._real_wav[self.real_start:self.real_end]
        accurapple_wav=self._accurapple_wav[self.accurapple_start:self.accurapple_end]

        from scipy import signal
        if self._accurapple_samplerate < self._real_samplerate or \
            self._accurapple_wav.shape[0] < self._real_wav.shape[0]:
            self._real_wav = real_wav
            self._accurapple_wav = signal.resample(accurapple_wav, real_wav.shape[0])
        elif self._accurapple_samplerate > self._real_samplerate or \
            self._accurapple_wav.shape[0] > self._real_wav.shape[0]:
            self._accurapple_wav = accurapple_wav
            self._real_wav = signal.resample(real_wav, accurapple_wav.shape[0])
        else:
            self._real_wav = real_wav
            self._accurapple_wav = accurapple_wav

        self._real_wav = self._real_wav / np.max(np.abs(self._real_wav))
        self._accurapple_wav = self._accurapple_wav / np.max(np.abs(self._accurapple_wav))

    @property
    def samplerate(self):
        return max(self._real_samplerate, self._accurapple_samplerate)

    @property
    def real_wav(self):
        return self._real_wav

    @property
    def nb_samples(self):
        assert self._real_wav.shape == self._accurapple_wav.shape, f"{self._real_wav.shape } != {self._accurapple_wav.shape}"
        return self._real_wav.shape[0]

    @property
    def accurapple_wav(self):
        return self._accurapple_wav

    def show_wavs(self):
        plt.plot(self._accurapple_wav, label=f"Accurapple {self._accurapple_samplerate}")
        plt.plot(self._real_wav+0.5, label=f"Real {self._real_samplerate}")
        plt.axvline( self.accurapple_start, color="red", label="Accurapple lims")
        plt.axvline( self.accurapple_end, color="red")
        plt.axvline( self.real_start, color="green", label="Real lims")
        plt.axvline( self.real_end, color="green")
        plt.legend()
        plt.show()

    def show_fft_ratio(self, ax, packet_size=880):
        mean = np.zeros( (packet_size//2,) )
        all_f1 = np.zeros( (packet_size//2,) )
        all_f2 = np.zeros( (packet_size//2,) )
        n = 0
        for i in tqdm(range(0,self._accurapple_wav.shape[0]-1880,37)):
            f1 = np.abs(fft.rfft(self._accurapple_wav[i:i+packet_size],n=packet_size))[1:]
            f2 = np.abs(fft.rfft(self._real_wav[i:i+packet_size],n=packet_size))[1:]
            assert f2.shape[0] == packet_size//2, f"{f2.shape[0]}"
            s1 = np.sum(f1)
            s2 = np.sum(f2)
            if s1 == 0 or s2 == 0:
                continue
            f1 = f1 / s1
            f2 = f2 / s2
            f2[f1 == 0] = 0
            f1[f1 == 0] = 1
            all_f1 += f1
            all_f2 += f2
            assert f2.shape[0] == packet_size//2
            ratio = f1/f2
            ratio[ratio > 10] = 10
            #print(ratio.shape)
            mean += ratio
            #plt.plot(ratio,alpha=0.5,lw=0.5,c="black")
            n += 1

        mean = mean / n
        all_f1 = all_f1 / n
        all_f2 = all_f2 / n
        xf = np.fft.rfftfreq(packet_size, 1 / self.samplerate)
        i = 0
        while i < len(xf)-1 and xf[i] < 160000:
            i += 1
        ax.plot(xf[:i],all_f1[:i],alpha=1,lw=1, label="Accurapple")
        ax.plot(xf[:i],all_f2[:i],alpha=1,lw=1, label="Real")
        ax.set_yscale("log")
        ax.set_xlabel("Frequency (hz)")
        ax.set_ylabel("Magnitude")
        ax.legend()
        return mean, xf[:i]


def blackman(N):
    # Compute Blackman window.
    if type(N) == np.ndarray:
        N = N.shape[0]

    n = np.arange(N)
    w = 0.42 - 0.5 * np.cos(2 * np.pi * n / (N - 1)) + \
        0.08 * np.cos(4 * np.pi * n / (N - 1))
    return w


def prepare_lowpass_filter(src_freq, target_freq, N=201):
    # https://tomroelandts.com/articles/how-to-create-a-simple-low-pass-filter
    x = np.linspace(-N//2, N//2, N)
    cutoff = (target_freq/2) / src_freq

    # 2*cutoff, see https://en.wikipedia.org/wiki/Sinc_filter
    sinc = np.sinc(2*cutoff*x)

    # plt.plot(sinc)
    # plt.title("Sinc Impulse Response")
    # plt.xlabel("1MHz samples number")
    # plt.show()

    windowed_sinc = sinc*blackman(N)
    # Normalize
    windowed_sinc = windowed_sinc/ np.sum(windowed_sinc)
    return windowed_sinc


def equalizer(one_mhz_wav, modifier_filter, TARGET_FREQ, param1=None):

    # Downsampling of the 1MHz wav
    lowpass = prepare_lowpass_filter(CPU_FREQ, TARGET_FREQ)
    one_mhz_wav_filtered = np.convolve(one_mhz_wav, lowpass)

    decimation_index = np.linspace(0, one_mhz_wav_filtered.shape[0]-1,
                        round(one_mhz_wav_filtered.shape[0] / (CPU_FREQ / TARGET_FREQ)) ).astype(int)
    wav_44khz = one_mhz_wav_filtered[decimation_index]

    # Overlap and add
    # https://en.wikipedia.org/wiki/Overlap%E2%80%93add_method

    N = 882 # How many sample by frame
    M = 0
    if modifier_filter == "lowpass":
        # I make a test filter which is a low pass one.
        # This is a time-domain convolution filter.
        assert param1 < TARGET_FREQ
        modifier_filter = prepare_lowpass_filter(TARGET_FREQ,int(param1))

        M = len(modifier_filter)
        print(f"N={N}, M={M}")
        assert M < N
        modifier_filter_padded = np.hstack( [modifier_filter, [0] * (N-1)])
        print(f"The padded test filter shape in time domain is M+N-1: {modifier_filter_padded.shape}")

        rfft_filter = pyfftw.builders.rfft(modifier_filter_padded)
        lowpass_padded_fft = rfft_filter(modifier_filter_padded)
        print(f"The padded test filter shape in frequency domain is (M+N-1)/2 + 1: {lowpass_padded_fft.shape}" )
    elif isinstance(modifier_filter, np.ndarray) :
        M = len(modifier_filter)*2 - 1
        lpf = np.zeros( (M,))
        #lpf[M//2] = 1
        lpf[0] = 1
        a = np.fft.rfft(lpf)
        a *= modifier_filter
        filter_convolution = np.fft.irfft(a, M)

        print(f"Using given filter N={N}, M={M}")
        print(modifier_filter)
        #filter_convolution = np.fft.irfft(modifier_filter, M)
        modifier_filter_padded = np.hstack( [filter_convolution, [0] * (N-1)])
        rfft_filter = pyfftw.builders.rfft(modifier_filter_padded)
        lowpass_padded_fft = rfft_filter(modifier_filter_padded)
    else:
        raise Exception("I don't recognize the provided filter")


    old_segment = None
    #wav_out = []
    wav_out = np.hstack([np.zeros_like(wav_44khz), [0]*M])

    output_segment = pyfftw.byte_align(
        np.zeros((M+N-1), dtype=np.float64))

    convo_buffer = pyfftw.byte_align(
        np.zeros((M+N-1)//2+1, dtype=np.complex128))

    segment_padded = pyfftw.byte_align(
        np.zeros((M+N-1), dtype=np.float64))

    pyfftw.interfaces.cache.enable()

    s = datetime.now()
    for i in range(0,(wav_44khz.shape[0]//N)*N,N):
        # I don't pad again (but fftw might destroy its input, not sure)
        segment_padded[0:N] = wav_44khz[i:i+N] # np.hstack( [segment, [0] * (M-1)])

        # This buffer is complex !
        segment_padded_fft = pyfftw.interfaces.numpy_fft.rfft(segment_padded)

        # This buffer is complex !
        if modifier_filter is not None:
            convo_buffer[0:(M+N-1)//2+1] = lowpass_padded_fft * segment_padded_fft
        else:
            convo_buffer[0:(M+N-1)//2+1] = segment_padded_fft

        # For some reasson beyond my understanding, when using builders:
        # - one must let the iFFT produce a new output array as it pleases
        # - one must rebuild a new planner each time...
        # irfft_segment = pyfftw.builders.irfft(convo_buffer)
        # output_segment = irfft_segment(convo_buffer)
        # Therefore I move to the pyfftw.interface modules which does the caching for me
        # and behaves as expected.

        output_segment = pyfftw.interfaces.numpy_fft.irfft(convo_buffer, len(segment_padded))

        if old_segment is None:
            old_segment = output_segment
        else:
            output_segment[0:M-1] += old_segment[N:]
            wav_out[i:i+N] = output_segment[:N]
            old_segment = output_segment

    # Averagin signal to bring it back close to zero.
    # I'm afraid (not thoroughly tested though) that it can be heard
    # as a low pass filter...
    # c = -np.ones( 100 )/100
    # c[-1] += 1
    # wav_out = np.convolve(wav_out, c)

    # Adding a small echo. This helps POP but I'm afraid it removes
    # some sound in Pickadilly

    # c = [0]*47
    # # 50 un peu trop grave
    # # 45 trop aigu
    # c[0] = 0.5
    # c[-1] = 0.5
    # c = 3*np.array(c)
    # wav_out = np.convolve(wav_out, c)

    #wav_out += np.random.random(wav_out.shape[0]) * 0.025

    time_for_frame = ((datetime.now()-s).total_seconds() / wav_out.shape[0]) * (TARGET_FREQ/50)
    print(f"Duration {(datetime.now()-s).total_seconds()}s")
    print(f"Time for frame: {time_for_frame*1000:.2f}ms based on a {TARGET_FREQ/1000:.1f}KHz sample of len {wav_out.shape[0]} (44Khz) samples")

    return wav_out




from collections.abc import Iterator
from contextlib import contextmanager

@contextmanager
def time_it() -> Iterator[None]:
    tic: float = time.perf_counter()
    try:
        yield
    finally:
        toc: float = time.perf_counter()
        print(f"Computation time = {1000*(toc - tic):.3f}ms")


if __name__ == "__main__":
    from matplotlib import pyplot as plt
    ticks = np.array([0,867,3000,3004,4000,4010,4400])
    y=fast_oscillator(ticks, 3850, 1, 0, 0, 10, 1.0, 0.01, engine=1)
    plt.plot(y, label="Classic")
    y=fast_oscillator(ticks, 3850, 1, 0, 0, 10, 1.0, 0.01, engine=2)
    plt.plot(y, label="Recurrence")
    plt.legend()
    plt.show()
