from scipy  import fft
from datetime import datetime
import pyfftw
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import root_scalar
from math import sqrt, exp, atan, log
from scipy.signal import resample
from tqdm import tqdm
import soundfile

SRC_FREQ=20280*50
TARGET_FREQ=44100
CYCLES_PER_SECOND=SRC_FREQ

sho_freq=3875
# Kris Kennaway's measurements
SHO1_FREQUENCY=3875 # 3875
SHO2_FREQUENCY=480
NB_LOOPS_SHO1=6
NB_LOOPS_SHO2=12
# Smaller is wider
SIG_K=2500
V_LOW_FREQ=5000
WEIGHT_LO_FREQ=0.05 # 0.01

def normalize_wav_and_save(wav, outfile_path, sample_rate=TARGET_FREQ, peak=1):
    wav = wav - np.mean(wav)
    wav = wav / np.max(np.abs(wav)) * peak
    soundfile.write(outfile_path, wav, sample_rate, "PCM_16")
    print(f"Saved {outfile_path}")
    return wav

def load_accurapple_speaker(filepath):
    # Returns ticks (1MHz)

    with open(filepath,"rb") as fin:
        data = fin.read()
        # >u8 == big endian, 8 bytes unsigned
        # integer (64 bits)
        accura_ticks = np.frombuffer(data, dtype=">u8")

    return accura_ticks


def get_freq_index(frequencies_scale, frequency):
    return (np.abs(frequencies_scale - frequency)).argmin()

def sigmoid(x,k,t):
    return x - x + 0.5
    return 1/(1+np.exp(-k*(x+t)))

def der_sigmoid(x,k,t):
    return x - x
    return sigmoid(x,k,t)*(1-sigmoid(x,k,t))


class WavPair:
    def __init__(self, real, accurapple, sample_rate_real=None, sample_rate_accurapple=None, label= None):
        self.label = label

        if type(real) == str and type(accurapple) == str:
            self._real_wav, self._real_samplerate = soundfile.read(real)
            self._accurapple_wav, self._accurapple_samplerate = soundfile.read(accurapple)
        else:
            self._real_wav, self._real_samplerate = real, sample_rate_real
            self._accurapple_wav, self._accurapple_samplerate = accurapple, sample_rate_accurapple

        assert type(self._real_wav) == np.ndarray
        assert self._real_samplerate > 0
        assert type(self._accurapple_wav) == np.ndarray
        assert self._accurapple_samplerate > 0

        print(f"WavPair: real:{self._real_samplerate} Hz Accurapple {self._accurapple_samplerate} Hz")

        if len(self._real_wav.shape) == 2:
            self._real_wav = self._real_wav[:,0]
        if len(self._accurapple_wav.shape) == 2:
            self._accurapple_wav = self._accurapple_wav[:,0]

        self.real_start, self.real_end = 0, self._real_wav.shape[0]
        self.accurapple_start, self.accurapple_end = 0, self._accurapple_wav.shape[0]

    def set_real_slice(self, start, end):
        self.real_start, self.real_end = start, end

    def set_accurapple_slice(self, start, end):
        self.accurapple_start, self.accurapple_end = start, end

    def cut_and_resample(self):
        real_wav=self._real_wav[self.real_start:self.real_end]
        accurapple_wav=self._accurapple_wav[self.accurapple_start:self.accurapple_end]

        from scipy import signal
        if self._accurapple_samplerate < self._real_samplerate or \
            self._accurapple_wav.shape[0] < self._real_wav.shape[0]:
            self._real_wav = real_wav
            self._accurapple_wav = signal.resample(accurapple_wav, real_wav.shape[0])
        elif self._accurapple_samplerate > self._real_samplerate or \
            self._accurapple_wav.shape[0] > self._real_wav.shape[0]:
            self._accurapple_wav = accurapple_wav
            self._real_wav = signal.resample(real_wav, accurapple_wav.shape[0])

    @property
    def samplerate(self):
        return max(self._real_samplerate, self._accurapple_samplerate)

    @property
    def real_wav(self):
        return self._real_wav

    @property
    def nb_samples(self):
        assert self._real_wav.shape == self._accurapple_wav.shape, f"{self._real_wav.shape } != {self._accurapple_wav.shape}"
        return self._real_wav.shape[0]

    @property
    def accurapple_wav(self):
        return self._accurapple_wav

    def show_wavs(self):
        plt.plot(self._accurapple_wav, label=f"Accurapple {self._accurapple_samplerate}")
        plt.plot(self._real_wav+0.5, label=f"Real {self._real_samplerate}")
        plt.axvline( self.accurapple_start, color="red", label="Accurapple lims")
        plt.axvline( self.accurapple_end, color="red")
        plt.axvline( self.real_start, color="green", label="Real lims")
        plt.axvline( self.real_end, color="green")
        plt.legend()
        plt.show()

    def show_fft_ratio(self, ax, packet_size=880):
        mean = np.zeros( (packet_size//2,) )
        all_f1 = np.zeros( (packet_size//2,) )
        all_f2 = np.zeros( (packet_size//2,) )
        n = 0
        for i in tqdm(range(0,self._accurapple_wav.shape[0]-1880,37)):
            f1 = np.abs(fft.rfft(self._accurapple_wav[i:i+packet_size],n=packet_size))[1:]
            f2 = np.abs(fft.rfft(self._real_wav[i:i+packet_size],n=packet_size))[1:]
            assert f2.shape[0] == packet_size//2, f"{f2.shape[0]}"
            s1 = np.sum(f1)
            s2 = np.sum(f2)
            if s1 == 0 or s2 == 0:
                continue
            f1 = f1 / s1
            f2 = f2 / s2
            f2[f1 == 0] = 0
            f1[f1 == 0] = 1
            all_f1 += f1
            all_f2 += f2
            assert f2.shape[0] == packet_size//2
            ratio = f1/f2
            ratio[ratio > 10] = 10
            #print(ratio.shape)
            mean += ratio
            #plt.plot(ratio,alpha=0.5,lw=0.5,c="black")
            n += 1

        mean = mean / n
        all_f1 = all_f1 / n
        all_f2 = all_f2 / n
        xf = np.fft.rfftfreq(packet_size, 1 / self.samplerate)
        i = 0
        while i < len(xf)-1 and xf[i] < 160000:
            i += 1
        ax.plot(xf[:i],all_f1[:i],alpha=1,lw=1, label="Accurapple")
        ax.plot(xf[:i],all_f2[:i],alpha=1,lw=1, label="Real")
        ax.set_yscale("log")
        ax.set_xlabel("Frequency (hz)")
        ax.set_ylabel("Magnitude")
        ax.legend()
        return mean, xf[:i]


def join_wav_pairs( loa: list[WavPair] ):
    # I'll align every pair on the biggest one, padding with
    # repeated copies.

    big_size = (max(map(lambda a: a.nb_samples, loa)),)
    smallest_size = min(map(lambda a: a.nb_samples, loa))
    dest_accurapple = np.zeros( big_size)
    dest_real = np.zeros( big_size)
    for a in loa:
        # resize will pad with zero
        print(f"Adding {a.label} which has {a.nb_samples} samples to a big pool of {big_size[0]} samples. Rate:{a.samplerate} Hz")
        dest_accurapple += np.resize(a._accurapple_wav, big_size)
        dest_real += np.resize(a._real_wav, big_size)

    print(f"Retaining only {smallest_size} samples")

    def normalize(a):
        c =  a - np.mean(a)
        return c / np.max(np.abs(c))

    dest_accurapple = normalize(dest_accurapple[:smallest_size])
    dest_real = normalize(dest_real[:smallest_size] / len(loa))

    return dest_real, dest_accurapple



class ExpSin:
    def __init__(self,a,b,c,d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def evaluate(self, x):
        return d*exp(-a*x)*sin(b*x+c)

    def derivative(self, x):
        return d*exp(-a*x)*(b*cos(b*x + c) - a*sin(b*x + c))

class Oscillator:
    def __init__(self, sho_freq, V, h0, v0, nb_loops, weight=1):

        self.weight = weight
        self.nb_loops = nb_loops
        self.frequency = sho_freq
        self.V = V
        self.lambda_over_m = 2 * (log(2) - log(0.01))/ (self.nb_loops/sho_freq)
        beta_over_m = (self.lambda_over_m**2 + (4*np.pi*sho_freq)**2)/4

        self.a = (h0-V)/2
        self.delta = 4*beta_over_m - self.lambda_over_m**2
        self.b = - ( v0 + self.a*self.lambda_over_m) / sqrt(self.delta)


    def evaluate(self, x):
        xp = 2 * self.weight * np.exp(-self.lambda_over_m*x/2)
        # That's the original function
        c = self.a*np.cos(sqrt(self.delta)/2*x)
        s = self.b*np.sin(sqrt(self.delta)/2*x)
        y      = xp * (c - s) + self.V

        # Simplified version
        # y_atan = xp * sqrt((a*a) + (b*b)) * np.sin( sqrt(delta)/2*x - atan(a/b)  ) + V
        return y

    def derivative(self, x):
        lm2 = -self.lambda_over_m/2
        a = self.a
        b = self.b

        e = np.exp((lm2 * x))
        c = np.cos((sqrt(self.delta)/2 * x)) * (a * lm2 - b * sqrt(self.delta)/2)
        s = np.sin((sqrt(self.delta)/2 * x)) * (a * sqrt(self.delta)/2 + b * lm2)
        return 2.0 * self.weight * e * (c - s)

class OscillatorPair:
    def __init__(self):
        self.time_base = 0
        self.volt = 1
        self.sho1 = Oscillator(SHO1_FREQUENCY, self.volt, self.volt, 0, nb_loops=NB_LOOPS_SHO1, weight=1)
        self.sho2 = Oscillator(SHO2_FREQUENCY, self.volt, self.volt, 0, nb_loops=NB_LOOPS_SHO2, weight=1)

        # Expansion of the sigmoid (smaller is wider)
        self.sig_k = SIG_K
        # Center of the sigmoid
        self.sig_t = -0.001 #-0.002
        self.nb_switches = 0

    def switch(self, x):
        h0 = self.evaluate(x)
        v0 = self.derivative(x)

        newt_time_base = x
        x = x - self.time_base

        if self.nb_switches == 0:
            self.volt = 0.0
        else:
            self.volt = 1.0

        self.time_base = newt_time_base
        self.sho1 = Oscillator(SHO1_FREQUENCY, self.volt,h0,v0,nb_loops=NB_LOOPS_SHO1)
        # This one, I just reset it
        # if self.volt == 1:
        #     low_h0 = 0.9
        # else:
        #     low_h0 = 0.1
        #low_h0 = 1 - self.volt
        #self.sho2 = Oscillator(SHO2_FREQUENCY, self.volt, low_h0,0,nb_loops=NB_LOOPS_SHO2)
        if self.volt == 0:
            v0 = -V_LOW_FREQ*self.volt
        else:
            v0 = V_LOW_FREQ*self.volt
        self.sho2 = Oscillator(SHO2_FREQUENCY, 0, 0, v0,nb_loops=NB_LOOPS_SHO2)
        self.nb_switches = (1 + self.nb_switches) % 2

    def evaluate(self, x):
        x = x - self.time_base
        s = sigmoid(x, self.sig_k, self.sig_t)
        #return (1-s)*self.sho1.evaluate(x) + s*self.sho2.evaluate(x)
        return self.sho1.evaluate(x) + WEIGHT_LO_FREQ*self.sho2.evaluate(x)

    def derivative(self, x):
        x = x - self.time_base
        return self.sho1.derivative(x) + WEIGHT_LO_FREQ*self.sho2.derivative(x)

        s = sigmoid(x, self.sig_k, self.sig_t)
        ds = der_sigmoid(x, self.sig_k, self.sig_t)
        # Derivative of (1-sig)*f = -sig'*f + (1-sig)*f'
        d1 = -ds*self.sho1.evaluate(x) + (1-s)*self.sho1.derivative(x)
        # Derivative of sig*g = sig'*g + sig*g'
        d2 = ds*self.sho2.evaluate(x) + s*self.sho2.derivative(x)
        return d1 + d2


class Renderer:
    def __init__(self):
        self.V = 0.5
        self.sho = Oscillator(SHO1_FREQUENCY, -self.V,h0=-self.V,v0=0,nb_loops=NB_LOOPS_SHO1)
        self.last_tick = None
        self.chunks = []

    def render_ticks(self, ticks):
        chunks = []
        for nt in tqdm(range(len(ticks))):
            next_tick = ticks[nt]
            if self.last_tick is not None:
                period = (next_tick - self.last_tick)
                xs = np.linspace(0,(period-1)/CYCLES_PER_SECOND,period).astype(np.float32)
                chunks.append(self.sho.evaluate(xs))

                end_of_period = (next_tick - self.last_tick)/CYCLES_PER_SECOND

                T = 5
                dt = 1*min(next_tick - self.last_tick,T)/T
                assert dt > 0
                if self.V < 0:
                    v = self.V + dt
                else:
                    v = self.V - dt

                #dt = 1

                self.sho = Oscillator(SHO1_FREQUENCY,
                                      v, self.sho.evaluate(end_of_period),
                                      self.sho.derivative(end_of_period),
                                      nb_loops=NB_LOOPS_SHO1)
                self.V = v

            self.last_tick = next_tick
            #self.V = - self.V
        self.chunks = np.hstack(self.chunks + chunks)
        return self.chunks


def blackman(N):
    # Compute Blackman window.
    if type(N) == np.ndarray:
        N = N.shape[0]

    n = np.arange(N)
    w = 0.42 - 0.5 * np.cos(2 * np.pi * n / (N - 1)) + \
        0.08 * np.cos(4 * np.pi * n / (N - 1))
    return w

def prepare_lowpass_filter(src_freq, target_freq, N=201):
    # https://tomroelandts.com/articles/how-to-create-a-simple-low-pass-filter
    x = np.linspace(-N//2, N//2, N)
    cutoff = (target_freq/2) / src_freq

    # 2*cutoff, see https://en.wikipedia.org/wiki/Sinc_filter
    sinc = np.sinc(2*cutoff*x)

    # plt.plot(sinc)
    # plt.title("Sinc Impulse Response")
    # plt.xlabel("1MHz samples number")
    # plt.show()

    windowed_sinc = sinc*blackman(N)
    # Normalize
    windowed_sinc = windowed_sinc/ np.sum(windowed_sinc)
    return windowed_sinc




def sound_gen_test(ticks_file, modifier_filter = None):
    r = Renderer()
    ticks = load_accurapple_speaker(ticks_file)
    ticks = ticks - ticks[0]

    for i in range(len(ticks)):
        if ticks[i] > CYCLES_PER_SECOND*80:
            break

    #one_mhz_wav = r.render_ticks( ticks[:min(len(ticks),i)])
    one_mhz_wav = fast_oscillator( ticks[:min(len(ticks),i)])

    # Downsampling of the 1MHz wav
    lowpass = prepare_lowpass_filter(SRC_FREQ, TARGET_FREQ)
    one_mhz_wav_filtered = np.convolve(one_mhz_wav, lowpass)

    decimation_index = np.linspace(0, one_mhz_wav_filtered.shape[0]-1,
                        round(one_mhz_wav_filtered.shape[0] / (SRC_FREQ / TARGET_FREQ)) ).astype(int)
    wav_44khz = one_mhz_wav_filtered[decimation_index]
    normalize_wav_and_save(wav_44khz, ticks_file.replace(".ticks","_44khz.wav"))
    if modifier_filter is None:
        return

    # Overlap and add
    # https://en.wikipedia.org/wiki/Overlap%E2%80%93add_method

    N = 882 # How many sample by frame

    if modifier_filter == "lowpass":
        # I make a test filter which is a low pass one.
        # This is a time-domain convolution filter.
        modifier_filter = prepare_lowpass_filter(TARGET_FREQ,TARGET_FREQ//2)

        M = len(modifier_filter)
        print(f"N={N}, M={M}")
        assert M < N
        modifier_filter_padded = np.hstack( [modifier_filter, [0] * (N-1)])
        print(f"The padded test filter shape in time domain is M+N-1: {modifier_filter_padded.shape}")

        rfft_filter = pyfftw.builders.rfft(modifier_filter_padded)
        lowpass_padded_fft = rfft_filter(modifier_filter_padded)
        print(f"The padded test filter shape in frequency domain is (M+N-1)/2 + 1: {lowpass_padded_fft.shape}" )
    elif modifier_filter is not None:
        M = len(modifier_filter)*2 - 1
        lpf = np.zeros( (M,))
        #lpf[M//2] = 1
        lpf[0] = 1
        a = np.fft.rfft(lpf)
        a *= modifier_filter
        filter_convolution = np.fft.irfft(a, M)

        print(f"Using given filter N={N}, M={M}")
        #print(modifier_filter)
        #filter_convolution = np.fft.irfft(modifier_filter, M)
        modifier_filter_padded = np.hstack( [filter_convolution, [0] * (N-1)])
        rfft_filter = pyfftw.builders.rfft(modifier_filter_padded)
        lowpass_padded_fft = rfft_filter(modifier_filter_padded)


    old_segment = None
    #wav_out = []
    wav_out = np.hstack([np.zeros_like(wav_44khz), [0]*M])

    output_segment = pyfftw.byte_align(
        np.zeros((M+N-1), dtype=np.float64))

    convo_buffer = pyfftw.byte_align(
        np.zeros((M+N-1)//2+1, dtype=np.complex128))

    segment_padded = pyfftw.byte_align(
        np.zeros((M+N-1), dtype=np.float64))

    pyfftw.interfaces.cache.enable()

    s = datetime.now()
    for i in range(0,(wav_44khz.shape[0]//N)*N,N):
        # I don't pad again (but fftw might destroy its input, not sure)
        segment_padded[0:N] = wav_44khz[i:i+N] # np.hstack( [segment, [0] * (M-1)])

        # This buffer is complex !
        segment_padded_fft = pyfftw.interfaces.numpy_fft.rfft(segment_padded)

        # This buffer is complex !
        if modifier_filter is not None:
            convo_buffer[0:(M+N-1)//2+1] = lowpass_padded_fft * segment_padded_fft
        else:
            convo_buffer[0:(M+N-1)//2+1] = segment_padded_fft

        # For some reasson beyond my understanding, when using builders:
        # - one must let the iFFT produce a new output array as it pleases
        # - one must rebuild a new planner each time...
        # irfft_segment = pyfftw.builders.irfft(convo_buffer)
        # output_segment = irfft_segment(convo_buffer)
        # Therefore I move to the pyfftw.interface modules which does the caching for me
        # and behaves as expected.

        output_segment = pyfftw.interfaces.numpy_fft.irfft(convo_buffer, len(segment_padded))

        if old_segment is None:
            old_segment = output_segment
        else:
            output_segment[0:M-1] += old_segment[N:]
            wav_out[i:i+N] = output_segment[:N]
            old_segment = output_segment

    # Averagin signal to bring it back close to zero.
    # I'm afraid (not thoroughly tested though) that it can be heard
    # as a low pass filter...
    # c = -np.ones( 100 )/100
    # c[-1] += 1
    # wav_out = np.convolve(wav_out, c)

    # Adding a small echo. This helps POP but I'm afraid it removes
    # some sound in Pickadilly

    # c = [0]*47
    # # 50 un peu trop grave
    # # 45 trop aigu
    # c[0] = 0.5
    # c[-1] = 0.5
    # c = 3*np.array(c)
    # wav_out = np.convolve(wav_out, c)

    #wav_out += np.random.random(wav_out.shape[0]) * 0.025
    print(np.max(wav_out))

    time_for_frame = ((datetime.now()-s).total_seconds() / wav_out.shape[0]) * (TARGET_FREQ/50)
    print(f"Duration {(datetime.now()-s).total_seconds()}s")
    print(f"Time for frame: {time_for_frame*1000:.2f}ms based on a {TARGET_FREQ/1000:.1f}KHz sample of len {wav_out.shape[0]} (44Khz) samples")

    normalize_wav_and_save(wav_out, ticks_file.replace(".ticks",".wav"), peak=0.35)

if False:
    lpf = prepare_lowpass_filter(44100, 5000)
    lpf = np.zeros( (201,))
    lpf[111] = 1

    plt.subplot(2,2,1)
    plt.plot(lpf)
    plt.subplot(2,2,2)
    plt.plot(np.abs(np.fft.rfft(lpf)))
    plt.subplot(2,2,3)
    plt.plot(np.imag( np.fft.rfft(lpf)))
    plt.subplot(2,2,4)
    plt.plot(np.fft.irfft(np.fft.rfft(lpf), len(lpf)))
    plt.show()
    exit()

if False:
    f = np.load("filter.npy")
    sound_gen_test("../gijoe.ticks", f)
    sound_gen_test("../pop.ticks", f)
    sound_gen_test("../picka.ticks", f)
    exit()

if False:
    a = np.fromfile("speaker/one_mhz.bin", np.float32)
    b = np.fromfile("speaker/recordings/conan.bin_kena2_onemhz.bin", np.float32)
    plt.plot(a[:1000000],label="Accurapple")
    plt.plot(b[:1000000],label="Kennaway") # -1/3000 for kena
    plt.legend()
    plt.show()

if False:
    V = 0
    sho = Oscillator(V,0,0)
    for i in range(1000,2000,10):
        end_of_period = 10/(50*20280)
        sho = Oscillator(
            V,
            sho.evaluate(end_of_period),
            sho.derivative(end_of_period))
        V = 1-V
        print(sho.evaluate(end_of_period))
    exit()


if False:

    #yf = np.fft.rfft(wav_44khz) # This is expressed in complex numbers :-)
    yf = rfft(wav_44khz)
    yf *= np.sin(np.linspace(0, np.pi, yf.shape[0])) # poor window
    wav_44khz = irfft(yf)


    time_for_frame = ((datetime.now()-s).total_seconds() / wav_44khz.shape[0]) * (TARGET_FREQ/50)
    print(f"start {s}")
    print(f"end {datetime.now()}")
    print(f"Time for frame: {time_for_frame*1000:.2f}ms based on a 44KHz sample of len {wav_44khz.shape[0]} (44Khz) samples")

    xf = np.fft.rfftfreq(wav_44khz.shape[0], 1 / TARGET_FREQ)
    plt.plot(10*np.log10(np.abs(yf)),lw=1)
    #plt.axvline(TARGET_FREQ,ls="--",color="black",label=f"{TARGET_FREQ/1000:.1f} Hz")
    plt.xlabel("Hertz")
    plt.ylabel("dB rel. to full scale")
    plt.legend()
    plt.show()



if False:
    V=0
    sho = Oscillator(V,1,10000,nb_loops=NB_LOOPS_SHO1)
    x = np.linspace(0,NB_LOOPS_SHO1*1.0/sho_freq,1000)
    plt.plot(x,sho.evaluate(x), label="SHO signal")
    d = sho.derivative(x)
    plt.plot(x,d / np.max(d), label="SHO signal's derivative")
    plt.axhline(V,c="black")
    plt.vlines([i*1.0/sho_freq for i in range(NB_LOOPS_SHO1+1)],ymin=V-1,ymax=V+1, colors="black", lw=1)
    plt.legend()
    plt.show()


if False:
    data_real, samplerate_real = soundfile.read("speaker/recordings/pop_real_2e.wav")
    data_emu, samplerate_emu = soundfile.read("pop.wav")
    data_emu = data_emu[:,0]
    real = data_real[0:samplerate_real*24]
    real = real[41037:1011238]
    emu = data_emu[0:samplerate_emu*24]
    emu = emu[42428: 1100151]
    real = real - np.mean(real)
    real[ np.abs(real) < 0.018] = 0

    # plt.subplot(2,1,1)
    # plt.plot(real,label="Real")
    # plt.subplot(2,1,2)
    # plt.plot(emu,label="Emulated")
    # plt.legend()
    # plt.show()


    emulated = resample(emu, len(real))
    real /= np.std(real)
    emulated /= np.std(emulated)

    plt.plot(real,label="Real",lw=1)
    plt.plot(emulated,label="Emulated",lw=1)
    plt.legend()
    plt.show()

    xf = np.fft.rfftfreq(emulated.shape[0], 1 / samplerate_real)
    plt.plot(xf[1:], np.abs(np.fft.rfft(real))[1:],label="Real",lw=0.5,alpha=0.5)
    plt.plot(xf[1:], np.abs(np.fft.rfft(emulated))[1:],label="Emulated",lw=0.5,alpha=0.5)
    plt.legend()
    plt.show()


if False:
    def prep2(x):
        n = 100
        x = np.abs(x)
        # Have the same frequencies in the spectrum (else you can't compare energies!)
        # We assume all FFT's cover the 0..22050 range (nor more no less).
        ndx = np.linspace(0,x.shape[0]-1,22050).astype(int)
        x = np.array( [ np.sum(x[ndx[i]:ndx[i+1]]) for i in range(len(ndx)-1) ] )

        # x = x[ndx]
        x = np.convolve(x, np.ones((n,)), mode="same")
        x = x[0:10000]
        # normalize energy across the spectrum
        x = x / np.sum( x)
        #x = x / x.shape[0]
        print(x.shape)
        print( np.sum(x))
        # Recordings beyond 10KHz are really bad
        # and bad enough they pollute the rest of
        # the computation. Therefore I don't look
        # at them.
        # But basically, what's beyond 10Khz is
        # almost flat line.
        return x

    def prep3(x):

        x = x / np.sum( x)
        x = x / x.shape[0]

        print(f"max={np.max(x)}")
        #x = x / np.max(x)
        return x

    # White noise WAV built with Audacity, played on my PC
    # and recorder with the microphone
    # from scipy.io import wavfile
    # wavfile.read("recordings/white_noise_mic.wav")
    data, samplerate = soundfile.read("recordings/white_noise_mic.wav")
    fft = np.fft.rfft(data)
    print(fft)
    bytez = bytearray()
    import struct
    for x in fft:
        # x.imag
        bytez.extend(bytearray(struct.pack(">f", x.real)))
        bytez.extend(bytearray(struct.pack(">f", x.imag)))
    with open("filter.bin","wb") as fout:
        fout.write(bytez)

    wn_yf = np.abs(fft)
    wn_xf = np.fft.rfftfreq(data.shape[0], 1 / samplerate)

    # Noise program as rendered by the emulator
    data, samplerate = soundfile.read("recordings/noise_kena3.wav")
    kena3_yf = np.abs(np.fft.rfft(data))
    kena3_xf = np.fft.rfftfreq(data.shape[0], 1 / samplerate)

    # Noise program as rendered by the emulator (slow and fast noise)
    kena3_fast_xf = np.load("recordings/noise_kena3.wav1.fft_x.npy")
    kena3_fast_yf = np.load("recordings/noise_kena3.wav1.fft_y.npy")
    kena3_slow_xf = np.load("recordings/noise_kena3.wav2.fft_x.npy")
    kena3_slow_yf = np.load("recordings/noise_kena3.wav2.fft_y.npy")


    # Noise program as rendered by the A2e (slow and fast noise)
    # and recorded with the microphone
    kena3_a2_fast_xf = np.load("recordings/noise_apple2e.wav1.fft_x.npy")
    kena3_a2_fast_yf = np.load("recordings/noise_apple2e.wav1.fft_y.npy")
    kena3_a2_slow_xf = np.load("recordings/noise_apple2e.wav2.fft_x.npy")
    kena3_a2_slow_yf = np.load("recordings/noise_apple2e.wav2.fft_y.npy")

    plt.plot( prep3( prep2(kena3_fast_yf)))
    plt.plot( prep3( prep2(kena3_slow_yf)))
    plt.plot( prep3( prep2(kena3_a2_fast_yf) / prep2(wn_yf)))
    plt.plot( prep3( prep2(kena3_a2_slow_yf) / prep2(wn_yf)))
    plt.show()


if False:
    # Debugging oscillatorpair transitions (switches)
    V=0
    osc = OscillatorPair()
    FIRST_STOP=0.001
    SECOND_STOP=FIRST_STOP+0.003122
    THIRD_STOP=SECOND_STOP+0.01
    x = np.linspace(0,FIRST_STOP,1000)
    plt.plot(x,osc.evaluate(x), label="First SHO pair signal")

    osc.switch(FIRST_STOP)
    x = np.linspace(FIRST_STOP,SECOND_STOP,1000)
    plt.plot(x,osc.evaluate(x), label="Second SHO pair signal")
    #plt.plot(x,osc.sho1.evaluate(x- FIRST_STOP), lw=1, ls="--", label="SHO1 signal")
    #plt.plot(x,osc.sho2.evaluate(x- FIRST_STOP), lw=1, ls="--", label="SHO2 signal")
    #plt.plot(x,sigmoid(x - FIRST_STOP, osc.sig_k, osc.sig_t), lw=1, ls="--", label="Mixing sigmoid")

    osc.switch(SECOND_STOP)
    x = np.linspace(SECOND_STOP,THIRD_STOP,1000)
    plt.plot(x,osc.evaluate(x), label="Third SHO pair signal")
    plt.plot(x,osc.sho1.evaluate(x- SECOND_STOP), lw=1, ls="--", label="SHO1 signal")
    plt.plot(x,osc.sho2.evaluate(x- SECOND_STOP), lw=1, ls="--", label="SHO2 signal")
    plt.plot(x,sigmoid(x - SECOND_STOP, osc.sig_k, osc.sig_t), lw=1, ls="--", label="Mixing sigmoid")


    plt.axhline(V,c="black")
    plt.legend()
    plt.show()


if False:
    accurapple_wav, real_samplerate = soundfile.read("speaker/real_2e/pop_real_2e.wav")
    real_wav, accurapple_samplerate = soundfile.read("speaker/real_2e/pop.wav")
    print(f"{real_samplerate} {accurapple_samplerate}")
    real_wav = real_wav[:,0]
    ticks = load_accurapple_speaker("speaker/recordings/pop.ticks")
    ticks = ticks - ticks[0]
    ticks = ticks / CYCLES_PER_SECOND * accurapple_samplerate
    ticks = ticks / 108661.5 * 108478

    r2 = real_wav[38989+6+30+32:38989+6+30+100000+32]
    c = [0]*47
    # 50 un peu trop grave
    # 45 trop aigu

    c[0] = 0.5
    c[-1] = 0.5
    c = 3*np.array(c)
    r2 = np.convolve(r2, c)

    r2 = np.convolve(real_wav, c)
    normalize_wav_and_save(r2, "pop2.wav")

    plt.plot(accurapple_wav[40560+480:40560+480+114000],label="Real")
    plt.plot(r2,label="AccurappleEcho")
    plt.plot(real_wav[38989+6+30:38989+6+30+100000],label="Accurapple") # -1/3000 for kena
    plt.vlines(ticks[(476 < ticks) & (ticks < 112000)]-476,ymin=-0.3,ymax=+0.3,colors="red")

    # plt.plot(real_wav[88000:114000],label="Real")
    # plt.plot(accurapple_wav[86000+40:112000],label="Accurapple") # -1/3000 for kena
    # s = 8148 + 3254 - 4072 + 7238 - 7258
    # plt.vlines(ticks[(ticks > s) & (ticks < s+26000)]-s,ymin=-1,ymax=+1,colors="red")

    plt.legend()
    plt.show()

if True:
    from utils import load_accurapple_ticks, fast_oscillator
    from scipy import signal
    CPU_FREQ=50*20280
    TARGET_FREQ=44100

    for file_path,outfile_path in [
            #("recordings/gijoe.ticks","gijoe_44khz.wav"),
            #("ticks/pop.ticks","pop_44khz.wav"),
            #("ticks/pickadilly.ticks","picka_44khz.wav")
    ]:
        ticks = load_accurapple_ticks(file_path)
        ticks = ticks - ticks[0]
        print("Generating")
        y = fast_oscillator(ticks)
        max_size = min(int(30e6), y.shape[0])
        print(f"Resampling {max_size} cycles")

        w = signal.resample(y[0:max_size], int(max_size/CPU_FREQ*TARGET_FREQ))
        print(w.shape)
        normalize_wav_and_save(w, outfile_path)

    wp1 = WavPair("real_2e/GI_Joe_30s_jeanfred64.wav", "renders/gijoe_44khz.wav", label="GIJoe")
    wp1.set_real_slice(9944, 721015)
    wp1.set_accurapple_slice(0, 654426)
    wp1.cut_and_resample()

    wp3 = WavPair("real_2e/Price_of_Persia_jeanfred64.wav", "renders/pop_44khz.wav", label="PrinceOfPersia")
    wp3.set_real_slice(28854, 972070)
    wp3.set_accurapple_slice(0, 867940)
    wp3.cut_and_resample()

    wp2 = WavPair("real_2e/picka_jeanfred64.wav", "renders/picka_44khz.wav", label="PickadillyPair")
    wp2.set_real_slice(18538, 724029)
    wp2.set_accurapple_slice(0, 649254)
    #wp2.show_wavs()
    wp2.cut_and_resample()

    real, accurapple = join_wav_pairs([wp1,wp2,wp3])
    wp = WavPair(real, accurapple, 48000, 48000, "Mix")
    normalize_wav_and_save(wp.accurapple_wav, "recon.wav", wp.samplerate)
    # plt.plot(wp.accurapple_wav)
    # plt.show()

    xscale = np.fft.rfftfreq(wp.nb_samples, 1 / wp.samplerate)

    blackman_window = blackman(wp.real_wav)
    fft_real = np.abs(np.fft.rfft( blackman_window * wp.real_wav))
    fft_accurapple = np.abs(np.fft.rfft( blackman_window * wp.accurapple_wav))

    ratio = fft_real / fft_accurapple
    ratio[fft_accurapple < 25] = 1
    ratio = np.clip(ratio,0.1,10)


    fig, axs = plt.subplots(nrows=2)
    axs[0].plot(xscale,fft_real, alpha=0.7, label="Real")
    axs[0].plot(xscale,fft_accurapple, alpha=0.7,label="Accurapple")
    axs[0].legend()
    axs[1].plot(xscale,ratio, label="Real over accurapple (as it is)")
    axs[1].legend()
    plt.show()


    ratio = np.log(1+ratio)

    ratio[fft_accurapple==0] = 1
    # High pass
    print(get_freq_index(xscale,239))
    print(len(xscale))

    # Hi pass
    ratio[:get_freq_index(xscale,400)]=0 # 239
    # Lowpass
    print(f"hipass at: {get_freq_index(xscale,16000)} / {wp.nb_samples} / {fft_real.shape}")
    ratio[get_freq_index(xscale,16000):]=0

    # This seemes necessayr else one can hear a lot of backgorund frequencies
    # after the application of the ratio.
    # The value 200 was determined manually.
    ratio = np.convolve(ratio, np.ones((len(ratio)//100,)), mode="same")


    # We downsample the full FFT spectrum to get one that
    # is 882 "taps" long.
    ls = np.linspace(0, ratio.shape[0], 882).astype(int)
    filter_ = []
    for i in range(len(ls)-1):
        b,e = ls[i], ls[i+1]
        s = np.sum( ratio[b:e] )
        filter_.append(s)
    filter_ = np.array(filter_)
    filter_ = 1+filter_ / np.max(filter_)

    filter_.astype(np.float32).tofile("filter.f32")

    #plt.plot(ratio)
    # plt.show()
    xscale_short = np.fft.rfftfreq(filter_.shape[0]*2-1, 1 / wp.samplerate)
    plt.title("Final filter")
    plt.plot(xscale_short, filter_)
    plt.show()
    #sound_gen_test("ticks/pop.ticks", filter_)
    sound_gen_test("ticks/pickadilly.ticks", filter_)

    data, samplerate = soundfile.read(str("renders/pop.wav"))
    ticks = load_accurapple_speaker("ticks/pop.ticks")
    ticks = (ticks - ticks[0]) / CYCLES_PER_SECOND * samplerate
    ticks = ticks[ (ticks >= 39765) & (ticks <= 73460)]
    ticks = ticks - 39765
    data2, samplerate2 = soundfile.read(str("real_2e/Price_of_Persia_jeanfred64.wav"))

    data = data[39765:73460]
    data2 = data2[72258:108924]
    datax = resample(data, data2.shape[0])
    ticks = ticks/data.shape[0]*data2.shape[0]

    plt.plot(datax, label="Accurapple")
    plt.plot(1+data2, label="Real")
    plt.vlines(ticks, ymin=-0.5, ymax=0.5, lw=1, alpha=0.5, colors="red")
    plt.legend()
    plt.show()




    # sound_gen_test("../gijoe.ticks", filter_)
    # sound_gen_test("../picka.ticks", filter_)

    exit()

if False:
    # wp3 = WavPair("real_apple/GI_Joe_30s_jeanfred64.wav", "../gijoe_44khz.wav", "GIJoe")
    # wp3.set_real_slice(9944, 721015)
    # wp3.set_accurapple_slice(0, 654424)
    # #wp3.show_wavs()
    # wp3.cut_and_resample()

    # wp3 = WavPair("real_apple/GI_Joe_30s_jeanfred64.wav", "../gijoe_kena3.wav", "GIJoe")
    # wp3.set_real_slice(9944, 721015)
    # wp3.set_accurapple_slice(176400, 830857)
    # wp3.cut_and_resample()

    wp3 = WavPair("real_apple/Price_of_Persia_jeanfred64.wav", "../pop_44khz.wav", label="PrinceOfPersia")
    wp3.set_real_slice(28854, 972070)
    wp3.set_accurapple_slice(0, 867940)
    wp3.show_wavs()
    wp3.cut_and_resample()

    # wp3 = WavPair("real_apple/Price_of_Persia_jeanfred64.wav", "../pop_kena3.wav", "PrinceOfPersia")
    # wp3.set_real_slice(28854, 1778618)
    # wp3.set_accurapple_slice(176400, 1786583)
    # wp3.cut_and_resample()

    print(wp3.samplerate)

    xscale = np.fft.rfftfreq(wp3.nb_samples, 1 / wp3.samplerate)
    # Blackman helps a bit to compute the ratio later on.
    # My impression is that it removes unwanted high frequencies.
    blackman_window = blackman(wp3.real_wav)
    fft_real = np.abs(np.fft.rfft( blackman_window * wp3.real_wav))
    fft_accurapple = np.abs(np.fft.rfft( blackman_window * wp3.accurapple_wav))
    # fft_real = np.abs(np.fft.rfft(  wp3.real_wav))
    # fft_accurapple = np.abs(np.fft.rfft( wp3.accurapple_wav))

    plt.plot(xscale, fft_real,alpha=0.5,label="Real")
    plt.plot(xscale, fft_accurapple, alpha=0.5,label="Accurapple")
    #plt.plot(xscale, fft_real - fft_accurapple)
    # #plt.yscale("log")
    plt.xlabel("Frequency(Hz)")
    plt.ylabel("Magnitude")
    # plt.title("GIJoe FFT")
    # # plt.ylim(10e-3,10e4)
    # plt.xlim(0,10000)
    # plt.legend()
    plt.show()

    #fft_accurapple[fft_accurapple==0] = 1
    ratio = fft_real / fft_accurapple

    def get_freq_index(frequencies_scale, frequency):
        return (np.abs(frequencies_scale - frequency)).argmin()

    ratio[fft_accurapple==0] = 1
    # High pass
    print(get_freq_index(xscale,239))
    print(len(xscale))
    #ratio[:get_freq_index(xscale,25)]=0 # 239
    # Lowpass
    ratio[get_freq_index(xscale,20400):]=0

    # This seemes necessayr else one can hear a lot of backgorund frequencies
    # after the application of the ratio.
    # The value 200 was determined manually.
    ratio = np.convolve(ratio, np.ones((len(ratio)//100,)), mode="same")
    plt.plot(ratio)
    plt.show()

    #print(ratio*(1+1j))
    x = np.fft.rfft(wp3.accurapple_wav)

    # Cheap low pass filter
    # ratio = np.ones( ratio.shape)
    # ratio[ ratio.shape[0]//7:] = 0

    reconstruction_fft = np.real(x)*ratio + np.imag(x)*ratio*1j

    #reconstruction_fft = x*0.0001

    #print(reconstruction_fft)
    plt.plot(np.abs(reconstruction_fft), alpha=0.5, color="black", label="reconstruction")

    wav = np.fft.irfft(reconstruction_fft, wp3.accurapple_wav.shape[0])
    normalize_wav_and_save(wav, "recon.wav", wp3.samplerate)
    plt.legend()
    plt.show()

    #wp4 = WavPair("real_apple/GI_Joe_30s_jeanfred64.wav", "../gijoe.wav", "GIJoe")

if False:
    wp = WavPair("real_apple/picka_jeanfred64.wav", "../picka_kena3.wav", label="Pickadilly")
    wp.set_real_slice(18540, 1540208)
    wp.set_accurapple_slice(176400, 1576701)
    wp.cut_and_resample()

    wp2 = WavPair("real_apple/Price_of_Persia_jeanfred64.wav", "../pop_kena3.wav", label="PrinceOfPersia")
    wp2.set_real_slice(28854, 1778618)
    wp2.set_accurapple_slice(176400, 1786583)
    wp2.cut_and_resample()

    wp3 = WavPair("real_apple/GI_Joe_30s_jeanfred64.wav", "../gijoe_kena3.wav", label="GIJoe")
    wp3.set_real_slice(9944, 721015)
    wp3.set_accurapple_slice(176400, 830857)
    wp3.cut_and_resample()
    # wp3.show_wavs()


    def moving_average(x, w):
        return np.convolve(x, np.ones(w), 'same') / w

    def fft_by_blocks(real, accurapple):
        pass

    if False:
        merge_real, merge_accurapple = join_wav_pairs([wp,wp2,wp3])
        m = merge_real.shape[0]
        xf = np.fft.rfftfreq(m, 1 / wp.samplerate)
        plt.subplot(1,2,1)
        plt.plot( xf[1:],moving_average( ( np.abs(fft.rfft(merge_accurapple,m)))[1:],1000),alpha=0.5,lw=1, label="Accurapple")
        plt.plot( xf[1:],moving_average( ( np.abs(fft.rfft(merge_real,m)))[1:],1000),alpha=0.5,lw=1, label="Real")
        plt.legend()
        plt.xlim(0,10000)
        plt.subplot(1,2,2)
        plt.plot( xf[1:],moving_average( ( np.abs(fft.rfft(merge_accurapple,m)))[1:],1000),alpha=0.5,lw=1, label="Accurapple")
        plt.plot( xf[1:],moving_average( ( np.abs(fft.rfft(merge_real,m)))[1:],1000),alpha=0.5,lw=1, label="Real")
        plt.yscale("log")
        plt.xlim(0,10000)
        plt.legend()
        plt.show()


    fig, axis = plt.subplots(2,2)
    print(axis)
    MAX_HZ=10000
    r1, xscale = wp.show_fft_ratio(axis[0,0],packet_size=882)
    axis[0,0].set_title(wp.label)
    axis[0,0].set_xlim(0,MAX_HZ)
    r2, xscale = wp2.show_fft_ratio(axis[1,0],packet_size=882)
    axis[1,0].set_title(wp2.label)
    axis[1,0].set_xlim(0,MAX_HZ)
    r3, xscale = wp3.show_fft_ratio(axis[0,1],packet_size=882)
    axis[0,1].set_title(wp3.label)
    axis[0,1].set_xlim(0,MAX_HZ)
    s = (r1+r2+r3)/3
    s = s / 1 # np.sum(s)
    s = 1/s

    #axis[1,1].set_xlim(0,10000)
    # axis[1,1].plot(xscale,r1[:len(xscale)],label="r1",lw=1)
    # axis[1,1].plot(xscale,r2[:len(xscale)],label="r2",lw=1)

    xscale = np.fft.rfftfreq(882, 1 / 48000)

    # Remove undesired frequencies (kin my experience, these are always
    # mostly noise)
    # s = s * np.abs(np.fft.rfft(prepare_lowpass_filter(wp3.samplerate//2,16000,N=881)))

    # l = np.argwhere(xscale < 10000)[-1][0]
    # print(l)
    # s[l:] = 1

    np.save("filter.npy", s)

    print(xscale.shape)
    axis[1,1].plot(xscale[0:-1], s[0:len(xscale)], label="mean")

    axis[1,1].set_title(f"Ratio. Len={s.shape[0]}")
    axis[1,1].axhline(1, lw=1, c="black")
    axis[1,1].legend()
    plt.show()


    sound_gen_test("../gijoe.ticks", s)
    # sound_gen_test("../pop.ticks", s)
    # sound_gen_test("../picka.ticks", s)

    wp4 = WavPair("real_apple/GI_Joe_30s_jeanfred64.wav", "../gijoe.wav", label="GIJoe")
    fig, axis = plt.subplots(2,2)
    MAX_HZ=20000
    r4, xscale = wp4.show_fft_ratio(axis[0,0],packet_size=882)
    axis[0,0].set_title(wp4.label)
    axis[0,0].set_xlim(0,MAX_HZ)
    plt.show()

    if False:
        max_len = max(real_wav.shape[0], accurapple_wav.shape[0])
        plt.subplot(2,1,1)
        f1 = np.abs(fft.rfft(accurapple_wav,n=max_len))[1:]
        f1 /= np.sum(f1)
        f2 = np.abs(fft.rfft(real_wav,n=max_len))[1:]
        f2 /= np.sum(f2)
        xf = np.fft.rfftfreq(max_len-1, 1 / max(accurapple_samplerate, real_samplerate))
        i = 0
        while xf[i] < 8000:
            i += 1
        plt.plot(xf[:i],f1[:i],alpha=0.5,lw=1, label="Accurapple")
        plt.plot(xf[:i],f2[:i],alpha=0.5,lw=1, label="Real")
        plt.legend()
        plt.subplot(2,1,2)
        r = f2[:i]/f1[:i]
        threshold = 1e-5
        r[ (f1[:i] < threshold) | (f2[:i] < threshold)  ]  = 1
        plt.plot(xf[:i],r)
        plt.show()

        exit()
