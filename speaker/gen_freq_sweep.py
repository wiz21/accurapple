def safe_div(a,b):
    if a % b == 1:
        return (a // b) - 1, b + a % b
    else:
        return a // b, a % b

CYCLES_DEY=2
CYCLES_STA=4

def gen_pause2(cycles):
    if cycles == 0:
        return []
    assert cycles >= 2, f"Cycles == {cycles}"
    if cycles == 2:
        return ["NOP\t; 2c"]
    elif cycles == 3:
        return ["BIT $FF\t; 3c"]
    elif cycles == 4:
        return ["NOP\t; 2c", "NOP\t; 2c"]   # 2 nop's is two bytes, shortest way to wait 4 cycles
    elif cycles == 5:
        return gen_pause2(2) + gen_pause2(3)
    elif cycles == 12+1:
        return gen_pause2(5)*2 + gen_pause2(3)
    elif cycles > 12:
        # +wait_cycles is no good: it destroys X and changes the Carry.
        # else: PHP 3c PHA 3c TXA 2c PHA 3c ... PLA 4c TAX 2c  PLA 4c PLP 4c
        # already: 25 cycles
        big, small = safe_div(cycles, 12)
        return ["JSR dummy_subroutine\t; 12c" for i in range(big)] +  gen_pause2(small)
    else:
        big, small = safe_div(cycles, CYCLES_STA)

        if big >= 1:
            return gen_pause2(CYCLES_STA) * big +  gen_pause2(small)
        else:
            return gen_pause(small)


def gen_tick_pause_loop(cycles):
    # DEY (2cycles) + BNE (3cycles) is the shortest loop we can do
    assert cycles >= 3 + 4 + 2 + 2

    cycles_left = cycles - (3 + 4 + 2) # BNE + BIT SPEAKER + DEY + at least one pause instruciotn
    if cycles_left == CYCLES_DEY:
        code = ["BIT SPEAKER\t; 4c", "DEY\t; 2c"]
    else:
        code = ["BIT SPEAKER\t; 4c"] + gen_pause2(cycles_left) + ["DEY\t; 2c"]

    HALF_SEC = 50*20280/2
    code = [f"pause_{cycles}_cycles\t!zone {{",
            f"   !byte {round(HALF_SEC/(256*cycles))}\t; nb of \"256x\" loops",
            ".loop:"] + code + ["BNE .loop\t; 3c", "   !byte 0", "}"]
    return code

def gen_pause(cycles):
    code = gen_pause2(cycles) + ["RTS" ]
    return code

def freq_to_cycles(f):
    return int(round(50*20280/f))

if __name__ == "__main__":
    # I don't do the frequency high than 22.5 KHz because I can't record them.
    with open("gen_freq.a","w") as fout:
        labels = []
        nb_loops = []
        for cycles in range(freq_to_cycles(22500),freq_to_cycles(6000)):

            n = round(((50*20280/2) / (cycles + 29))) // 256
            n2 = round(((50*20280/2) / (cycles + 29))) % 256
            nb_loops.append(round(((50*20280/2) / (cycles + 29))))

            label = f"pause_{cycles}_cycles"
            labels.append(label)
            code = [f"\n;; {cycles} cycles, {50*20280/cycles:.1f} Hertz. {(n*256+n2)*(cycles + 29)} run cycles."]
            code.append(f"{label}:")
            code.extend(gen_pause(cycles))
            for line in code:
                if line.startswith(".") or "!" in line or ";;" in line or "}" in line or ":" in line:
                    pass
                else:
                    line = "   "+line
                print(line)
                fout.write(line + "\n")
        fout.write("dummy_subroutine\tRTS\n")
        fout.write("gen_freq_ptrs !word " + ",".join(labels) + "\n")
        fout.write("gen_freq_nb_loops !word " + ",".join(map(lambda n: str(n), nb_loops)) + "\n")
        fout.write(f"nb_gen_freq_ptrs = {len(labels)}\n")
        print(f"nb_gen_freq_ptrs = {len(labels)}")
        assert(len(labels) < 127, "too many entries in table")
