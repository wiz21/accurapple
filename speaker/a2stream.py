import numpy as np
import soundfile
# librosa's reasample is better than scipy I think
from librosa import resample
from scipy.signal import butter, lfilter, freqz

def blackman(N):
    # Compute Blackman window.
    if type(N) == np.ndarray:
        N = N.shape[0]

    n = np.arange(N)
    w = 0.42 - 0.5 * np.cos(2 * np.pi * n / (N - 1)) + \
        0.08 * np.cos(4 * np.pi * n / (N - 1))
    return w

def prepare_lowpass_filter(src_freq, target_freq, N=201):
    # https://tomroelandts.com/articles/how-to-create-a-simple-low-pass-filter
    x = np.linspace(-N//2, N//2, N)
    cutoff = (target_freq/2) / src_freq

    # 2*cutoff, see https://en.wikipedia.org/wiki/Sinc_filter
    sinc = np.sinc(2*cutoff*x)

    # plt.plot(sinc)
    # plt.title("Sinc Impulse Response")
    # plt.xlabel("1MHz samples number")
    # plt.show()

    windowed_sinc = sinc*blackman(N)
    # Normalize
    windowed_sinc = windowed_sinc/ np.sum(windowed_sinc)
    return windowed_sinc


def butter_lowpass(cutoff, fs, order=5):
    return butter(order, cutoff, fs=fs, btype='low', analog=False)

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def interleave(a, b):
    c = np.empty((a.size + b.size,), dtype=a.dtype)
    c[0::2] = a
    c[1::2] = b
    return c

#audio, samplerate = soundfile.read(r"C:\Users\StephaneC\Downloads\NEFFEX.wav")
audio, samplerate = soundfile.read(r"sandstorm_compressed.wav")

print(audio.shape)
print(samplerate)
if len(audio.shape) == 2:
    print("To mono")
    audio = audio[:,0] + audio[:,1]
# Only 30 seconds
audio = audio[0:samplerate*60]
lowpass = prepare_lowpass_filter(samplerate,22050,601)
audio = np.convolve(audio, lowpass)
array = resample(audio, orig_sr=samplerate, target_sr=22050)



# Bring back into [0,1]
array -= np.min(array)
array /= np.max(array)
array = np.clip(array * 1.2,0,1)

DUTY_CYCLE=int(round(50*20280/22050))
print(f"DUTY_CYCLE={DUTY_CYCLE}")
array = np.round(array * (DUTY_CYCLE-15) + 4).astype(int)

# Ticks of the carrier
a1 = np.arange(array.shape[0])*DUTY_CYCLE
# Ticks for the actual sound data
a2 = np.arange(array.shape[0])*DUTY_CYCLE + array
r = interleave(a1,a2)

r.astype(">u8").tofile("a2stream.ticks")
