from pathlib import Path

from PySide2.QtWidgets import QApplication, QWidget, QPushButton, QLineEdit, QListWidget, QGridLayout, QSlider, QLabel
from PySide2.QtCore import Qt
from PySide2.QtMultimedia import QSound

import pyqtgraph as pg
from scipy import signal
import numpy as np
from utils import Oscillator, CPU_FREQ, OscillatorRenderer, load_accurapple_ticks, normalize_wav_and_save,WavPair,fast_oscillator, load_wav, equalizer, time_it

def sigmoid(x):
    return 1/(1+np.exp(-x))

# # wp3 = WavPair(Path(__file__).parent.parent / "speaker/real_apple/Price_of_Persia_jeanfred64.wav",
# #               Path(__file__).parent.parent / "pop_44khz.wav", label="PrinceOfPersia")

#wav, sample_rate = load_wav(Path(__file__).parent.parent / "speaker" / "real_2e" / "pop_real_2e_denoised.wav")
# tick_samples = {
#     288 : signal.resample(wav[41038:41503], int((41503-41038)/sample_rate*CPU_FREQ)),
#     168 : signal.resample(wav[44201:44600], int((44600-44201)/sample_rate*CPU_FREQ)),
#     148 : signal.resample(wav[906_926+196:906_926+575+100], int((575+100-196)/sample_rate*CPU_FREQ)),
#     128 : signal.resample(wav[134709:135163], int((135163-134709)/sample_rate*CPU_FREQ)),
#     88 : signal.resample(wav[906_926+3364:906_926+3626+200], int((3626+200-3364)/sample_rate*CPU_FREQ)),
#     68 : signal.resample(wav[906_926+7006:906_926+7306+100], int((7306+100-7006)/sample_rate*CPU_FREQ)),
#     48 : signal.resample(wav[906_926+10172:906_926+10172+450], int((450)/sample_rate*CPU_FREQ)),
#     28 : signal.resample(wav[906_926+13820:906_926+13820+450], int((450)/sample_rate*CPU_FREQ)),
#     18 : signal.resample(wav[232370:232370+474], int((474)/sample_rate*CPU_FREQ)),
#      8 : signal.resample(wav[57316:57785], int((57785-57316)/sample_rate*CPU_FREQ)),
#     }


wav, sample_rate = load_wav(Path(__file__).parent.parent / "speaker" / "real_2e" / "pop_real_2e_jeanfred_denoised.wav")
tick_samples = {
    # 288 : signal.resample(wav[29373:29373+517], int(517/sample_rate*CPU_FREQ)),
    # 168 : signal.resample(wav[33331:33331+517], int(517/sample_rate*CPU_FREQ)),
    # 128 : signal.resample(wav[36771:36771+517], int(517/sample_rate*CPU_FREQ)),
    #88 : signal.resample(wav[40212:40212+514], int(514/sample_rate*CPU_FREQ)),
    #48 : signal.resample(wav[44168:44168+516], int(516/sample_rate*CPU_FREQ)),
    #28 : signal.resample(wav[47606:47606+515], int((450)/sample_rate*CPU_FREQ)),
    # 148 : signal.resample(wav[73028:73028+515], int(515/sample_rate*CPU_FREQ)),

    # 68 : signal.resample(wav[80429:80429+517], int(517/sample_rate*CPU_FREQ)),
    # 18 : signal.resample(wav[90802:90802+517], int(517/sample_rate*CPU_FREQ)),


    # ??  8 : signal.resample(wav[57316:57785], int((57785-57316)/sample_rate*CPU_FREQ)),

    }


from matplotlib import pyplot as plt

FADE_OUT_LEN=500
for k,wav in tick_samples.items():
    m = wav
    m = m - m[0] #np.mean(wav)
    b = 1000
    b2 = b + 2000
    # f = 2*(b / (np.linspace(b,b+b2,len(m))) - b/(b2+b))
    tick_samples[k] = m  * 8 # * f

    # plt.plot(f)
    # plt.show()

    # plt.plot( tick_samples[k] )
    # plt.plot( m*5 )
    # plt.title(f"{k}")
    # plt.show()


# wp3 = WavPair(Path(__file__).parent.parent / "speaker/real_apple/Price_of_Persia_jeanfred64.wav",
#               Path(__file__).parent.parent / "speaker/real_apple/Price_of_Persia_jeanfred64.wav",
#               label="PrinceOfPersia")
# wp3.set_real_slice(28854, 972070)
# wp3.set_accurapple_slice(0, 867940)
# wp3.cut_and_resample()

wp3 = WavPair(Path(__file__).parent.parent / "speaker" / "real_2e" / "pop_real_2e_jeanfred_denoised.wav",
              Path(__file__).parent.parent / "speaker" / "real_2e" / "pop_real_2e_jeanfred_denoised.wav")
# wp3.set_real_slice(906_926, 1_013_075)
# wp3.set_accurapple_slice(906_926, 1_013_075)
wp3.set_real_slice(28854, 280000)
wp3.set_accurapple_slice(28854, 280000)
# wp3.set_real_slice(280000,595000)
# wp3.set_accurapple_slice(280000,595000)
wp3.cut_and_resample()


# all_ticks = load_accurapple_ticks(Path(__file__).parent.parent / "pop.ticks")
all_ticks = load_accurapple_ticks(Path(__file__).parent / "ticks/pop.ticks")
all_ticks = all_ticks - all_ticks[0]
#all_ticks = all_ticks[(all_ticks >= 19_951_142) & (all_ticks < 22_431_507)]
#all_ticks = all_ticks[all_ticks < 6_000_000]
#all_ticks = all_ticks[(all_ticks >= wp3.accurapple_start/wp3.samplerate*CPU_FREQ) & (all_ticks < wp3.accurapple_end/wp3.samplerate*CPU_FREQ)]
all_ticks = all_ticks[(all_ticks >= 0) & (all_ticks < wp3.accurapple_end/wp3.samplerate*CPU_FREQ)]
all_ticks = all_ticks - all_ticks[0]


## Always start by initializing Qt (only once per application)
app = QApplication([])

## Define a top-level widget to hold everything
w = QWidget()
w.setWindowTitle('AccurApple speaker tester')

## Create some widgets to be placed inside
play_btn = QPushButton('Play!')
play_real_btn = QPushButton('Play real!')

slider_time_to_zero = QSlider(orientation=Qt.Orientation.Horizontal)
slider_time_to_zero.setMinimum(1)
slider_time_to_zero.setMaximum(100)

slider_v0 = QSlider(orientation=Qt.Orientation.Horizontal)
slider_v0.setMinimum(-50000)
slider_v0.setMaximum(50000)

slider_atten = QSlider(orientation=Qt.Orientation.Horizontal)
slider_atten.setMinimum(1)
slider_atten.setMaximum(100)

slider_freq = QSlider(orientation=Qt.Orientation.Horizontal)
slider_freq.setMinimum(2000)
slider_freq.setMaximum(6000)

lbl_ttz = QLabel("value ttz")
lbl_atten = QLabel("value attenuation")
lbl_v0 = QLabel("value v0")
lbl_freq = QLabel("value freq")



plot_wav = pg.PlotWidget()
plot_wav.setMouseEnabled(y=False)
#plot_wav.disableAutoRange()
#plot_wav.getAxis("left").unlinkFromView()
#plot_wav.getViewBox().setRange( xRange=(0,all_ticks[-1]/CPU_FREQ), yRange=(-1,+1))
lines1 = pg.PlotCurveItem()
plot_wav.addItem(lines1)
lines1.setData(wp3.real_wav)





plot = pg.PlotWidget()
plot.setMouseEnabled(y=False)
#plot.disableAutoRange()
#plot.getAxis("left").unlinkFromView()
plot.getViewBox().setRange( xRange=(0,all_ticks[-1])) #, yRange=(-2,+2))

lines = pg.PlotCurveItem()
plot.addItem(lines)

sca = pg.ScatterPlotItem()
sca.setSymbol("x")
sca.setPen(color="red")
plot.addItem(sca)


## Create a grid layout to manage the widgets size and position
layout = QGridLayout()
w.setLayout(layout)

pos_lbl=QLabel("Position")
## Add widgets to the layout in their proper positions
layout.addWidget(QLabel("v0"), 2, 0)  # text edit goes in middle-left
layout.addWidget(slider_v0, 2, 1)  # text edit goes in middle-left
layout.addWidget(lbl_v0,2,2)

layout.addWidget(QLabel("TtZ"), 3, 0)  # text edit goes in middle-left
layout.addWidget(slider_time_to_zero, 3, 1)  # text edit goes in middle-left
layout.addWidget(lbl_ttz,3,2)

layout.addWidget(QLabel("Friction"), 4, 0)
layout.addWidget(slider_atten, 4, 1)
layout.addWidget(lbl_atten,4,2)

layout.addWidget(QLabel("Frequency"), 5, 0)
layout.addWidget(slider_freq, 5, 1)
layout.addWidget(lbl_freq,5,2)

layout.addWidget(play_btn, 6, 0)
layout.addWidget(play_real_btn, 6, 1)
layout.addWidget(pos_lbl, 6, 2)


#layout.addWidget(listw, 2, 0)  # list widget goes in bottom-left
layout.addWidget(plot, 0, 0, 1, 3)  # plot goes on right side, spanning 3 rows
layout.addWidget(plot_wav, 1, 0, 1, 3)  # plot goes on right side, spanning 3 rows
## Display the widget as a new window
w.show()

def mouseMoved(evt):
  mousePoint = plot.getViewBox().mapSceneToView(evt[0])
  pos_lbl.setText(f"{round(mousePoint.x())}")
  target = round(mousePoint.x())
  ndx = np.searchsorted(all_ticks, target) - 1
  if target - all_ticks[ndx] > all_ticks[ndx+1] - target:
      ndx += 1
  print(f"{all_ticks[ndx]}-{all_ticks[ndx+1] - all_ticks[ndx]}-{all_ticks[ndx+1]}")

def mouseMovedReal(evt):
  mousePoint = plot_wav.getViewBox().mapSceneToView(evt[0])
  pos_lbl.setText(f"{round(mousePoint.x()) + wp3.real_start}")

proxy = pg.SignalProxy(plot.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)
proxy2 = pg.SignalProxy(plot_wav.scene().sigMouseMoved, rateLimit=60, slot=mouseMovedReal)

t_to_zero = 0.01
v0 = 0
tick_attenuation= 0
last_1mhz = None

# friction_attenuation = 0.05
# SHO1_FREQUENCY=3875 # 3875
# NB_LOOPS_SHO1=12

# friction_attenuation = 0.11
# SHO1_FREQUENCY=3875
# NB_LOOPS_SHO1=24
# lowpassfilter = sample_rate*0.9

friction_attenuation = 0.0001
SHO1_FREQUENCY=4100
NB_LOOPS_SHO1=12
lowpassfilter = sample_rate*0.5



frequency = SHO1_FREQUENCY
slider_freq.setValue(SHO1_FREQUENCY)
lbl_freq.setText(str(SHO1_FREQUENCY))
slider_atten.setValue(int(friction_attenuation*100))
lbl_atten.setText(str(friction_attenuation))

def redraw():
    global t_to_zero, v0, tick_attenuation, last_1mhz, friction_attenuation
    # osc = Oscillator(frequency,0.5,0,v0,NB_LOOPS_SHO1,t_to_zero=t_to_zero)
    # osc_render = OscillatorRenderer(0.5, osc, tick_attenuation)
    # ticks = [0,1000,1010,2000]

    with time_it():
        y = fast_oscillator(all_ticks, frequency, 1, 0, v0, NB_LOOPS_SHO1, 1, friction_attenuation, engine=2)
    #y2 = fast_oscillator(ticks, frequency*0.9, 1, 0, v0, NB_LOOPS_SHO1*5, 1)
    #y = np.log10(10+y)
    # y[10000:] += y[0:-10000]*0.4
    # y[15000:] += y[0:-15000]*0.3
    # y[20000:] += y[0:-20000]*0.2

    if False:
        #fast_oscillator(ticks, sho_freq, voltage, h0, v0, nb_loops, weight, attenuation)
        for t in range(all_ticks.shape[0]-3):
            if t % 2 == 0:
                base = -1
            else:
                base = 1

            if all_ticks[t+2] - all_ticks[t+1] >= 2500 and (t == 0 or all_ticks[t] - all_ticks[t-1] >= 2500):
                d = all_ticks[t+1] - all_ticks[t]
                s = tick_samples[d]
                space = int(all_ticks[t+2] - all_ticks[t])
                ofs = int(all_ticks[t])
                y[ofs:ofs+space] = fast_oscillator(np.array([0,space]),3800,1,0,0,5,0,0.01)

                #+ fast_oscillator(np.array([0,space]),800,base,-base,0,15,0,0.01)

    if True:
        for t in range(all_ticks.shape[0]-3):
            if t % 2 == 0:
                base = -1
            else:
                base = 1

            if all_ticks[t+2] - all_ticks[t+1] >= 2500 and (t == 0 or all_ticks[t] - all_ticks[t-1] >= 2500):
                ofs = int(all_ticks[t])
                FADE_IN=100
                y[ofs:ofs+FADE_IN] = (y[ofs:ofs+FADE_IN] - base)*sigmoid(np.linspace(-4,+4,FADE_IN)) + base


            elif all_ticks[t+2] - all_ticks[t+1] >= 2500 and (t == 0 or all_ticks[t] - all_ticks[t-1] >= 2500):
                d = all_ticks[t+1] - all_ticks[t]
                if d in tick_samples:
                    s = tick_samples[d]
                    space = int(all_ticks[t+2] - all_ticks[t])
                    w = s.shape[0]
                    ofs = int(all_ticks[t])
                    y[ofs:ofs+space] = base
                    if space > w:
                        space = w
                    FADE_OUT_LEN = space//2
                    y[ofs:ofs+space] = base + s[0:space]
                    assert FADE_OUT_LEN < space

                    tail = s[space-FADE_OUT_LEN:space]


                    fader = 1 - sigmoid( np.linspace(-4,+4,FADE_OUT_LEN))
                    y[ofs+space-FADE_OUT_LEN : ofs+space] = base + tail * fader

    last_1mhz = y

    t = np.linspace(0, y.shape[0], y.shape[0])
    lines.setData(x=t, y=last_1mhz)

    sca.setData(x=np.array(all_ticks[:-1]), y=last_1mhz[all_ticks[:-1]])


    # plot.getPlotItem().getAxis("left").setRange(-2,2)
    # data2 = np.sin(np.linspace(0,4*3.1415+v/100,50))


def update_t_to_zero(v):
    global t_to_zero
    t_to_zero = 0.0000001+ v/10000
    lbl_ttz.setText(f"{t_to_zero:.5}")
    redraw()

def update_v0(v):
    global v0
    v0 = v
    lbl_v0.setText(f"{v0}")
    redraw()

def update_tick_attenuation(v):
    global tick_attenuation, friction_attenuation
    # tick_attenuation = v
    friction_attenuation = v/100
    lbl_atten.setText(f"{friction_attenuation}")
    redraw()

def update_freq(v):
    global frequency
    frequency = v
    lbl_freq.setText(f"{frequency}")
    redraw()


last_played = None
def play_sound():
    global last_played
    if id(last_1mhz) != id(last_played):
        last_played = last_1mhz
        #w = signal.resample(last_1mhz, int(last_1mhz.shape[0]/CPU_FREQ*sample_rate))
        w = equalizer(last_1mhz, "lowpass", sample_rate, param1=lowpassfilter)
        normalize_wav_and_save(w, "test.wav", sample_rate = sample_rate)
    QSound.play("test.wav")

def play_sound2():
    if last_1mhz is not None:
        normalize_wav_and_save(wp3.real_wav, "test_real.wav", sample_rate = wp3.samplerate)
        QSound.play("test_real.wav")

slider_time_to_zero.valueChanged.connect( update_t_to_zero)
slider_v0.valueChanged.connect( update_v0)
slider_atten.valueChanged.connect( update_tick_attenuation)
slider_freq.valueChanged.connect( update_freq)

play_btn.clicked.connect(play_sound)
play_real_btn.clicked.connect(play_sound2)

redraw()

## Start the Qt event loop
app.exec_()  # or app.exec_() for PyQt5 / PySide2
