import glob
import os.path
from pathlib import Path
import pickle
import os, psutil
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import soundfile
from math import ceil, sqrt, exp, tan
import logging
from logging import debug, info
import scipy
from scipy.optimize import curve_fit, minimize
from scipy.stats import norm
from scipy.interpolate import interp1d
from zlib import crc32
import librosa

SRC_FREQ=50*20280
TARGET_FREQ=44100

CYCLES_PER_SECOND=50*20280
CYCLES_BETWEEN_EXTREMES=4542
FORCE_RELOAD = True
# FIXME Actually, the test poduces 2*100 ticks
# but somehow, sometimes, one of the last tick doesn't show up
# dunno why.

TICKS_PER_BEACON=99
TICKS_PER_BEACON2=200
K = 21.159039269541

# Number of points one can display in a plot
# in about a second

if os.getenv("HOSTNAME") == "debian":
    PLT_POINTS_PER_CHART=1_000_000
else:
    PLT_POINTS_PER_CHART=1_000_000


logger = logging.getLogger('SndFit')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-t','--ticks',help="Accurapple ticks file")
parser.add_argument('-w','--wav',help="WAV file")
parser.add_argument('-b','--with-beacons', action='store_true', help="Work with beacons or not")
parser.add_argument('--find-beacons', help="Find the beacons. Expects the noise level on the derivative.")
parser.add_argument('--find-beacons-tops', action='store_true', help="Find the beacons tops")
parser.add_argument('--show-beacon', help="Show one beacon + additional info")
parser.add_argument('--show-beacons', action='store_true', help="Show the beacons over the whole signal")
parser.add_argument('--show-beacons-tops', action='store_true', help="Show each beacon and its ticks")
parser.add_argument('--show-data', action='store_true', help="Show original data")
parser.add_argument('--show-segment', help="Show a segment between two beacons.")
parser.add_argument('--show-fft', help="Show FFT for a segment between two beacons. Pass the segment number, starting at 1.")
parser.add_argument('--compare-fft', action='store_true', help="Comapre FFT's (hardcoded data, see code)")
parser.add_argument('--peak-analysis', action='store_true')
parser.add_argument('--compare-segments', required=False, nargs="+")
parser.add_argument('--average-segments', action='store_true')
parser.add_argument('--debug', default=False, action='store_true')
parser.add_argument('--fit', nargs=1, help="fit segment")
parser.add_argument('--ticks-stats', action='store_true', help="")
parser.add_argument('--ticks-analysis', help="Show a tick segment and some stats.")


cli_args = parser.parse_args()

def log_mem_used(msg = None):
    process = psutil.Process(os.getpid())
    print(f"Used memory: {process.memory_info().rss // (1024*1024)}MB. {msg or ''}" )


class BeaconTicks:
    def __init__(self, b, e, ticks):
        self.begin = b
        self.end = e
        self._ticks = ticks
        self._tops = self._ticks[(self.begin <= self._ticks) & (self._ticks <= self.end)][0:TICKS_PER_BEACON*2]

        self._beacon_center = np.mean(self._tops)
        self._mean_distance_between_tops = np.mean(self._tops[1:] - self._tops[:-1])

    def nb_extremes(self):
        return len(self._tops)

    def tops_times(self): # absolute coordinates
        return self._tops

    def tops_values(self):
        return [1] * len(self._tops)

    def center(self):
        return self._beacon_center

    def mean_distance_between_tops(self):
        return self._mean_distance_between_tops

    def ticks(self):
        return self._ticks[(self.begin <= self._ticks) & (self._ticks < self.end)]

class BeaconWav:
    def __init__(self, number, b, e, wav, samples_between_extremes, debug=True):
        self.number = number
        self.begin = b
        self.end = e
        self._wav = wav
        self._find_extremes(samples_between_extremes, debug= debug)
        self._compute_beacon_center()

    def nb_extremes(self):
        return len(self._tops)

    def segment(self):
        """ Beacon segment
        """
        return self._wav[self.begin:self.end]

    def tops_times(self):
         # Absolute coordinates
        return self._tops

    def tops_values(self):
        return self.segment()[self._tops - self.begin]

    def threshold(self):
        return self._threshold

    def center(self) -> float:
        return self._beacon_center

    def mean_distance_between_tops(self):
        return self._mean_distance_between_tops

    def _compute_beacon_center(self):
        if len(self._tops) == TICKS_PER_BEACON2:
            self._beacon_center = np.mean(np.array(self._tops))
        elif len(self._tops) == TICKS_PER_BEACON2-1:
            # Add the "missing" tick
            self._beacon_center = np.mean(np.array([self._tops[0] - self._mean_distance_between_tops] + self._tops))
        else:
            logger.warning(f"The WAV beacon {self.number} structure is weird ({len(self._tops)} peaks ?)")
            self._beacon_center = np.mean(np.array(self._tops))

    def _locate_peaks_valleys(self):
        bb,be = self.begin, self.end
        safety = 10
        segment = self._wav[bb-safety:be]
        self._threshold = np.mean(segment) + 1.5*np.std(segment)
        ex = find_extremes(segment, self._threshold, min_space=10)
        return ex - safety

    def _find_extremes(self,samples_between_extremes, debug=False):
        self._tops = self._find_extremes4(samples_between_extremes)
        #self._tops = self._find_extremes2(samples_between_extremes, debug)
        #print(self._tops)
        t = np.array(self._tops)
        self._mean_distance_between_tops = np.mean(t[1:] - t[:-1])
        return

    def _find_extremes2(self, samples_between_extremes, debug=False):

        def swipe(tops, start, direction, debug=False):
            #k = 215*44100/48000
            k = samples_between_extremes
            i = start
            good_tops_ndx = [start]
            while 1 <= i < len(tops)-1:
                j = i+direction
                while 0 <= j < len(tops):
                    prev_top = tops[i]
                    next_top = tops[j]
                    d = abs(next_top - prev_top)
                    if abs(d - k) <=  10:
                        k = abs(next_top - tops[start])/len(good_tops_ndx)
                        good_tops_ndx.append(j)
                        break
                    else:
                        j += direction
                i = j
            if debug:
                plt.plot(segment,c="black", lw=1)
                pos = np.array(tops)[good_tops_ndx]
                plt.axhline(self.threshold(),c="black",ls="--",label="Threshold for extremes")
                plt.axhline(-self.threshold(),c="black",ls="--")
                plt.scatter(pos,segment[pos],color="red",marker="x",label="Kept tops")
                plt.scatter(tops,segment[tops],color="lime",marker="|",label="Tops")
                plt.axvline(tops[start],c="green",label="Swipe start")
                plt.title(f"Swipe results: {len(good_tops_ndx)} ticks found")
                plt.legend()
                plt.show()
                pass

            return np.array(tops)[good_tops_ndx]

        safety = 150
        bb,be = self.begin-safety, self.end
        segment = self._wav[bb:be]
        self._threshold = np.mean(segment) + 1.5*np.std(segment)

        ticks = find_tops(segment, self._threshold, min_space=10, debug=debug)
        assert len(ticks) >= 99

        # Keep half the tops and amongs them keep
        # the value of the one in the middle
        st = take_strongest_ticks(segment, ticks, 1, TICKS_PER_BEACON2//2)
        middle = st[len(st)//2]

        # Take all the highest tops
        st = take_strongest_ticks(segment, ticks, 1, TICKS_PER_BEACON2, debug=debug)
        # amongst them, pick the one in the middle (we've found above)
        start = st.index(middle)

        # From that point, go right and left to figure out the
        # tops, jumping from one top to the other.
        left = list(sorted(swipe(st, start, -1,debug)))
        right = list(sorted(swipe(st, start, +1,debug)))[1:]
        tops = left + right

        ticks = find_tops(-segment, self._threshold, min_space=5, debug=debug)
        assert len(ticks) >= 99

        st = take_strongest_ticks(segment, ticks, -1, TICKS_PER_BEACON2//2)
        start = st[len(st)//2]
        # Take the bottomest tops
        st = take_strongest_ticks(segment, ticks, -1, TICKS_PER_BEACON2)
        # amongst them, pick the one in the middle
        start = st.index(start)

        left = list(sorted(swipe(st, start, -1, debug)))
        right = list(sorted(swipe(st, start, +1,debug)))[1:]
        bottoms = left + right

        if debug or (len(bottoms)+len(tops) not in (TICKS_PER_BEACON2,TICKS_PER_BEACON2-1)):
            plt.plot(segment,c="grey", lw=1)
            plt.axhline(self.threshold(),c="black",ls="--",label="Threshold for extremes")
            plt.axhline(-self.threshold(),c="black",ls="--")
            plt.scatter(tops,segment[tops],c="red",lw=1,marker="x")
            plt.scatter(bottoms,segment[bottoms],c="red",lw=1,marker="x")
            plt.title(f"{len(tops)} tops, {len(bottoms)} bottoms found")
            plt.show()

        return np.array(sorted(tops+bottoms)) - safety + self.begin

    def _find_extremes3(self):
        SHIFT = 100
        wav = self._wav[self.begin-SHIFT:self.end]
        deriv = np.gradient(wav)


        # Locate the first movement
        i = 0
        while abs(deriv[i]) <= 0.01:
            i += 1
        start = i
        dir = np.sign(np.sum(wav[i:i+5]))

        # Continue to first crossing
        while deriv[i]*deriv[i+1] > 0:
            i = i+1
        first_crossing = i

        # Next crossing
        i += 2 # safety
        while np.sign(deriv[i]) in (0,-dir):
            i = i+1
        next_crossing = i

        # mid point between the two extremees
        # We aim the mid point because that's
        # where the derivative is the biggest
        # and the spread between two extreme
        # is the tallest.
        mid_point = round((first_crossing + next_crossing)/2)

        plt.plot(deriv)
        plt.axvline(start, c="red", label="First movement")
        plt.axvline(first_crossing, c="red", label="First crossing")
        plt.axvline(next_crossing, c="red", label="Second crossing")
        plt.axvline(mid_point, c="green", label="Mid point")
        plt.axvline(mid_point+98, c="green", label="Mid point + crossing space")
        plt.grid()
        plt.show()

        def follow_extremes(mid_point, deriv, dir):
            """ Locate all "big jumps" of direction dir, starting at the mid point.
            """
            distances = []
            mid_points = []
            old_mid = None
            for ndx in range(TICKS_PER_BEACON):
                j = int(round(mid_point))
                while np.sign(deriv[j]) == -dir:
                    j = j-1
                first_extreme = j
                j = int(round(mid_point))
                while np.sign(deriv[j]) == -dir:
                    j = j+1
                next_extreme = j
                mid_point = (first_extreme + next_extreme)/2
                mid_points.append(mid_point)

                if len(distances) == 0:
                    distances = [98*2]
                else:
                    distances.append(mid_point - old_mid)
                old_mid = mid_point
                mid_point += np.mean(distances[max(0,len(distances)-5):len(distances)])
            return mid_points

        def adjust_extremes(points, deriv, wav):
            adjusted = []
            for ndx, p in enumerate(points):
                base_i = int(round(p))
                i = base_i - 1
                while deriv[base_i]*deriv[i] >= 0:
                    i -= 1
                base_i = i - 4 # Continue backwards to pass the extreme
                while deriv[base_i]*deriv[i] >= 0:
                    i -= 1
                adjusted.append(i)

            return adjusted


        mp1 = adjust_extremes(
            follow_extremes(mid_point, deriv, dir),
            deriv, self._wav)
        assert len(mp1) == TICKS_PER_BEACON
        mp2 = adjust_extremes(
            follow_extremes(mid_point+98, deriv, -dir),
            deriv, self._wav)
        assert len(mp2) == TICKS_PER_BEACON

        plt.title("WAV-only tick detection")
        plt.plot(wav,lw=1,c="black")
        plt.axvline(mid_point,c="green", label="midpoint")
        plt.axvline(first_crossing,c="red",label=f"First extreme (at {first_crossing-SHIFT})")
        plt.axvline(next_crossing,c="orange")
        plt.scatter(mp1, wav[mp1], marker="x", c="red", label=f"Detected ticks {len(mp1)}, dir={dir}")
        plt.scatter(mp2, wav[mp2], marker="x", c="orange", label=f"Detected ticks {len(mp2)}, dir={-dir}")
        plt.legend()
        plt.show()

        mp = np.array(sorted(mp1 + mp2)) - SHIFT + self.begin
        return mp


    def _find_extremes4(self, samples_between_extremes):
        SHIFT = 100
        w = self._wav[self.begin-SHIFT:self.end]
        deriv = np.gradient(w)


        # Locate the first movement
        i = 0
        while abs(deriv[i]) < 0.01:
            i += 1
        start = i
        dir = np.sign(np.sum(w[i:i+5]))

        # Continue to first crossing
        # (when derivative crosses the axis, it means we have an extremum)
        while deriv[i]*deriv[i+1] > 0:
            i = i+1
        first_crossing = i

        # Second crossing
        i = first_crossing + int(samples_between_extremes) - (first_crossing - start)//2 # safety
        print(i)
        while deriv[i]*deriv[i+1] > 0:
            i = i+1
        next_crossing = i

        plt.plot(deriv,label="Derivative")
        plt.plot(w,label="Wav",c="grey")
        plt.title("Derivative")
        plt.axvline(start, c="pink", label="First tick")
        plt.axvline(first_crossing, c="red", label="First crossing")
        plt.axvline(next_crossing, c="red", label="Second crossing")
        plt.xlim(start-25, next_crossing+100)
        plt.grid()
        plt.legend()
        plt.show()

        def follow_extremes(mid_point, deriv, dir):
            """ Locate all "big jumps" of direction dir, starting at the mid point.
            """
            distances = []
            mid_points = []
            old_mid = None
            for ndx in range(TICKS_PER_BEACON):
                j = int(round(mid_point)) - 2
                while deriv[j]*deriv[j+1] > 0:
                    j = j+1
                mid_point = j
                mid_points.append(j)

                if len(distances) == 0:
                    distances = [samples_between_extremes]
                else:
                    distances.append(mid_point - old_mid)
                old_mid = mid_point
                mid_point += np.mean(distances[max(0,len(distances)-5):len(distances)])
            return mid_points

        def adjust_extremes(points, deriv, wav):
            return points
            adjusted = []
            for ndx, p in enumerate(points):
                base_i = int(round(p))
                i = base_i - 1
                while deriv[base_i]*deriv[i] >= 0:
                    i -= 1
                base_i = i - 4 # Continue backwards to pass the extreme
                while deriv[base_i]*deriv[i] >= 0:
                    i -= 1
                adjusted.append(i)

            return adjusted


        mp1 = adjust_extremes(
            follow_extremes(first_crossing, deriv, dir),
            deriv, self._wav)
        assert len(mp1) == TICKS_PER_BEACON
        mp2 = adjust_extremes(
            follow_extremes(int(first_crossing+samples_between_extremes/2), deriv, -dir),
            deriv, self._wav)
        assert len(mp2) == TICKS_PER_BEACON

        plt.title("WAV-only tick detection")
        plt.plot(w,lw=1,c="black",label="wav")
        plt.plot(deriv,lw=1,c="grey",label="derivative")
        plt.axvline(first_crossing,c="red",label=f"First extreme (at {first_crossing-SHIFT})")
        plt.axvline(next_crossing,c="orange")
        plt.scatter(mp1, w[mp1], marker="x", c="red", label=f"Detected ticks {len(mp1)}, dir={dir}")
        plt.scatter(mp1, deriv[mp1], marker="o", c="red", label=f"Detected ticks on derivative, dir={dir}")
        plt.scatter(mp2, w[mp2], marker="x", c="orange", label=f"Detected ticks {len(mp2)}, dir={-dir}")
        plt.legend()
        plt.show()

        mp = np.array(sorted(mp1 + mp2)) - SHIFT + self.begin
        return mp


class AlignedBeacon:
    robust_mean_wav: float # Absolute coordinates
    robust_mean_ticks: float
    k: float

    def __init__(self, bw: BeaconWav, bt:BeaconTicks):
        self.beacon_wav = bw
        self.beacon_ticks = bt

        tt_ticks = bt.tops_times()
        tt_wav = bw.tops_times() # absolute coordinates
        assert len(tt_ticks) == len(tt_wav),f"{len(tt_ticks)} != {len(tt_wav)}"

        assert tt_ticks.shape == tt_wav.shape, f"ticks:{tt_ticks.shape} != wav tops:{tt_wav.shape}"

        # Compute the k factor fot this beacon
        self.k = (tt_ticks[-1] - tt_ticks[0]) / (tt_wav[-1] - tt_wav[0])
        self.robust_mean_wav = np.mean(tt_wav)
        self.robust_mean_ticks = np.mean(tt_ticks)

    def ticks_to_wav_beacon_local(self, ticks):
        # The first tick of each of the beacons
        # is the zero of its local coordinates.

        return (ticks - self.beacon_ticks.tops_times()[0])/self.k + self.beacon_wav._tops[0] - self.beacon_wav.begin

class AllBeacons:
    def __init__(self):
        self._ticks = []
        self._wav = []
        self.joined:list[AlignedBeacon] = []

    def append(self, wav_beacon:BeaconWav, ticks_beacon:BeaconTicks):
        assert isinstance(wav_beacon,BeaconWav)
        assert isinstance(ticks_beacon,BeaconTicks)
        self._wav.append(wav_beacon)
        self._ticks.append(ticks_beacon)
        self.joined.append(AlignedBeacon(wav_beacon, ticks_beacon))

    def all_wav_centers(self):
        return [bw.center() for bw in self._wav]

    def nb_segments(self):
        return len(self._wav) - 1

    def __len__(self):
        return len(self._wav)

    def __getitem__(self, i) -> tuple[BeaconWav, BeaconTicks]:
        return (self._wav[i], self._ticks[i])

    def ticks(self, i):
        return self._ticks[i]

    def wav(self, i):
        return self._wav[i]

    def get_data_segment(self, ndx, with_beacons=True, original_ticks=False):
        """ return wav segment and ticks (in wav coordinates(not cycles), relative to the
            beginning of the segment)

            returns a np.array for the wav
            a np.array for the ticks. NOTE! Ticks are xformed so that they match the
            wav. Therefore, their indices are floats (not integers) !
        """
        jb1 = self.joined[ndx]
        jb2 = self.joined[ndx+1]

        if with_beacons:
            wav_begin = int(jb1.beacon_wav.begin)
            segment = data[wav_begin:int(jb2.beacon_wav.end) ]
            ticks = accura_ticks[(jb1.beacon_ticks.begin <= accura_ticks) &
                                (accura_ticks <= jb2.beacon_ticks.end)]
        else:
            wav_begin = int(jb1.beacon_wav.end) + 12000
            segment = data[wav_begin:int(jb2.beacon_wav.begin)-200 ]
            ticks = accura_ticks[(jb1.beacon_ticks.end <= accura_ticks) &
                                (accura_ticks <= jb2.beacon_ticks.begin)]

        k = (jb2.robust_mean_ticks - jb1.robust_mean_ticks) / \
            (jb2.robust_mean_wav - jb1.robust_mean_wav)

        if jb1.beacon_wav.nb_extremes() + jb1.beacon_ticks.nb_extremes() != 2*TICKS_PER_BEACON2:
            fix = -SAMPLE_BETWEEN_BEACONS_IN_EXTREMES/2
        else:
            fix = 0

        # ticks are shifted so that their robust mean matches
        # the one of the WAV. Then they are converted to samples
        # (via *k).
        # Finally their position is translated to be relative to the
        # beginning of the wav.
        #logger.debug(f"the k factor is {k}")

        if not original_ticks:
            ticks = (ticks - jb1.robust_mean_ticks)/k + fix + jb1.robust_mean_wav - wav_begin

        return segment, ticks

    def short_sequences(self, segment_ndx):
        return self._short_sequences

    def peak_analysis(self, segment_ndx,close_threshold,with_beacons):
        self._short_sequences = self._find_ticks_sequences(segment_ndx, close_threshold, with_beacons)
        self._xltd_short_sequences = self._adjust_ticks_sequences(segment_ndx, self._short_sequences, with_beacons)

    def _adjust_ticks_sequences(self, segment_ndx, short_sequences,with_beacons):
        xltd_short_sequences = []
        segment_wav, ticks = self.get_data_segment(segment_ndx, with_beacons)
        grad = np.gradient(segment_wav)

        for sn in tqdm(range(len(short_sequences)-1), leave=False):
            if len(short_sequences[sn]) <= 1:
                continue
            short_sequence = np.array(short_sequences[sn])

            # It is much faster (to query) to build small interp1d
            # each time we need them instead of building a big one.
            begin_s = int(short_sequence[0]-20)
            end_s = int(short_sequence[-1]+20)
            interp_wav = interp1d(np.arange(begin_s, end_s),
                                  segment_wav[begin_s: end_s])
            interp_grad = interp1d(np.arange(begin_s, end_s),
                                  grad[begin_s: end_s])

            # Locate when the first sequence really starts.
            # (The ticks are bit off, so we put back the ticks where
            # the WAV start changing)
            tick = short_sequence[0]
            shift = -1
            while shift > -7 and (abs(interp_grad(tick+shift)) > 0.005 or \
                abs(interp_wav(tick+shift)) > 0.01):
                shift -= 1/20

            # Once we have located the start, we improve it a bit
            # so that the other "ticks" in the sequence are
            # closer to derivative changes

            scores = []
            itick = int(short_sequence[0])
            for shift2 in range(10):
                score = 0
                d = shift+shift2/10
                for t in short_sequence:
                    score += abs(interp_wav(t+d)*interp_grad(t+d))
                scores.append(score)

            shift = shift+np.argmin(scores)/10

            # The ticks translated so that they are where
            # the signal actually changes.
            xltd_short_sequence = short_sequence+shift

            xltd_short_sequences.append(xltd_short_sequence)
        return xltd_short_sequences

    def _find_ticks_sequences(self, segment_ndx, close_threshold=2000,with_beacons=True):
        """ Find short burst of close ticks
        close_threshold: max distance between two close ticks, in wav samples.
        """

        tick_sequences = []

        segment, ticks = self.get_data_segment(segment_ndx, with_beacons)
        logger.debug(f"Segment has {len(ticks)} tick")
        tick_ndx = 0
        while tick_ndx < len(ticks):
            #logger.debug(f"tick {tick_ndx}")
            tick = ticks[tick_ndx]

            tick_sequence = []
            # The tick is the first one or it is
            # far away from the previous one. In both
            # case it is a start of a 2 or 3 click sequence.
            # Unit is samples, not cycles
            if tick_ndx == 0 or tick - ticks[tick_ndx-1] > 500:
                tick_sequence.append(tick)
            else:
                tick_ndx += 1
                continue

            tick_ndx += 1

            # Look for following (but close) ticks
            # How close is specified by the threshold

            while tick_ndx < len(ticks) and \
                  ticks[tick_ndx] - ticks[tick_ndx-1] < close_threshold:
                tick_sequence.append(ticks[tick_ndx])
                tick_ndx += 1

            if len(tick_sequence) >= 2:
                tick_sequences.append(tick_sequence)

        logger.info(f"{len(tick_sequences)} short sequences (>=2 ticks) in this segment")
        return tick_sequences


class Speaker:
    """Simulates the response of the Apple II speaker."""

    # TODO: move lookahead.evolve into Speaker method

    def __init__(self, sample_rate: float, freq: float, damping: float,
                 scale: float):
        """Initialize the Speaker object
        :arg sample_rate The sample rate of the simulated speaker (Hz)
        :arg freq The resonant frequency of the speaker
        :arg damping The exponential decay factor of the speaker response
        :arg scale Scale factor to normalize speaker position to desired range (cpu freq)
        """
        self.sample_rate = sample_rate
        self.freq = freq
        self.damping = damping
        self.scale = np.float64(scale)  # TODO: analytic expression

        # See _Signal Processing in C_, C. Reid, T. Passin
        # https://archive.org/details/signalprocessing0000reid/
        dt = np.float64(1 / sample_rate)
        w = np.float64(freq * 2 * np.pi * dt)

        d = damping * dt
        e = np.exp(d)
        c1 = 2 * e * np.cos(w)
        c2 = e * e

        # Square wave impulse response parameters
        b2 = 0.0
        b1 = 1.0

        self.c1 = c1
        self.c2 = c2
        self.b1 = b1
        self.b2 = b2

    def tick(self, y1, y2, x1, x2):
        """ y1, y2: speaker position
        x1,x2: applied voltage
        """
        y = self.c1 * y1 - self.c2 * y2 + self.b1 * x1 + self.b2 * x2
        return y

if False:
    inv_scale = 22400 * 0.07759626164027278
    sample_rate = 1_000_000
    speaker = Speaker(sample_rate, freq=3875, damping=-1210, scale=1 / inv_scale)

    with open("../snd_recs/archon.bin", 'rb') as infile:
        buf = infile.read()

        i  = 0
        while i < len(buf) and buf[i] == 0:
            i = i + 1

        j  = len(buf) - 1
        while j > 0 and buf[j] == 0:
            j = j - 1

        snd_array = np.frombuffer(buf[i:j], dtype='uint8').astype(float)
        snd_array = snd_array/128-1 # make it -1 or +1

        snd_array = snd_array #[0:9000000]

        print(np.min(snd_array), np.max(snd_array))

        y1 = y2 = v1 = v2 = 0
        for i in tqdm(range(snd_array.shape[0])):
            y = speaker.tick(y1, y2, v1, v2)
            v2 = v1
            v1 = snd_array[i]
            y2 = y1
            y1 = y
            snd_array[i] = y

        #plt.plot(snd_array)
        #plt.show()


    # normalize
    snd_array = snd_array - np.mean(snd_array)
    snd_array = snd_array / np.max(np.abs(snd_array))

    downsampled_output = librosa.resample(
            y=snd_array,
            orig_sr=1000000,
            target_sr=44100)

    import soundfile
    soundfile.write('test.wav', downsampled_output*128, 44100, "PCM_16")

    exit()

def read_speaker_record(filepath):
    with open(filepath,"rb") as fin:
        data = fin.read()
        # >u8 == big endian, 8 bytes unsigned integer (64 bits)
        return np.frombuffer(data, dtype=">u8")


def find_zero_runs(a):
    # Create an array that is 1 where a is 0, and pad each end with an extra 0.
    iszero = np.concatenate(([0], np.equal(a, 0).view(np.int8), [0]))
    absdiff = np.abs(np.diff(iszero))
    # Runs start and end where absdiff is 1.
    ranges = np.where(absdiff == 1)[0].reshape(-1, 2)
    return ranges

def find_tops(data, above, min_space=1000, debug=False):
    # Return the X-position of tops

    # First derivative
    der = np.gradient(data)
    sign_changes = []
    for i in range(der.shape[0]-3):
        EPS = 0.0001
        if data[i] < above:
            continue
        if sign_changes and i - sign_changes[-1] < min_space:
            continue

        # Change of sign in the first derivative
        if (data[i] > data[i-1] and data[i] > data[i+1]) \
          and (data[i] > data[i-2] and data[i] > data[i+2]) : # and abs(der[i]-der[i+1]) > EPS:
            # Make sure we get the topmost element
            if  data[i] > data[i+1]:
                sign_changes.append(i)
            else:
                sign_changes.append(i+1)
        if (data[i] > data[i-1] > data[i-2]) \
           and (data[i] == data[i+1]) \
           and (data[i+1] > data[i+2] > data[i+3]): # and abs(der[i]-der[i+1]) > EPS:
            # Make sure we get the topmost element
            if  data[i] > data[i+1]:
                sign_changes.append(i)
            else:
                sign_changes.append(i+1)
        elif der[i] < -EPS and der[i+1] > EPS and abs(der[i]-der[i+1]) > EPS:
            if  data[i] < data[i+1]:
                sign_changes.append(i)
            else:
                sign_changes.append(i+1)

    if debug:
        plt.title("find tops")
        plt.plot(data, label="data", lw=1)
        plt.plot(der, label="Derivative", lw=1)
        plt.scatter(sign_changes, data[sign_changes], label="sign changes")
        plt.legend()
        plt.show()

    return sign_changes

def find_extremes(data, threshold, min_space=1000):
    if data.shape[0] == 0:
        return np.array([])
    logger.info(f"Find extremes on segment size {data.shape[0]}")
    peaks = find_tops(data, threshold, min_space)
    valleys = find_tops(-data, threshold, min_space)
    return np.array(sorted(peaks+valleys))

def take_strongest_ticks(segment, tops, value_sign, n, debug=False):
    tops_values_pos = sorted(zip(segment[tops]*value_sign, tops),reverse=True)[:n]
    tops_pos = [nv[1] for nv in tops_values_pos]

    if debug:
        plt.title("take_strongest_ticks")
        plt.plot(segment, lw=1)
        plt.scatter(tops_pos,
                    [nv[0] for nv in tops_values_pos],c="red",marker="x")
        plt.show()
    return list(sorted(tops_pos))


def mass_spring(x, a,b,c,d=1.0):
    return d*np.exp(-a*x)*np.sin(b*x+c)


def mass_spring_likelihood(params, xdata, ydata, distr):
    a,b,c,d = params
    #print(a,b,c,d)
    ydata_est = mass_spring(xdata,a,b,c,d)
    residuals = np.abs(ydata_est - ydata)
    #print("-"*80)
    #print(residuals[:10])
    #print(distr.pdf(residuals[:10]))
    return -np.sum(distr.logpdf(residuals))

def mass_spring_MLE_fit(ydata, X0, back_in_time=0):
    xdata = np.arange(ydata.shape[0])
    distr = norm(loc=0,scale=0.25/2) # The location (loc) keyword specifies the mean. The scale (scale) keyword specifies the standard deviation.
    #X0 = [0.003,0.055,1.5+3.14/4,0.15]
    lik_model = minimize(mass_spring_likelihood, x0=X0, bounds=[(0,1), (0,1), (-3.1415,3.1415), (0,1)], args=(xdata,ydata,distr), method='L-BFGS-B')

    if back_in_time > 0:
        xdata = np.arange(-back_in_time, ydata.shape[0])

    return mass_spring(xdata,*lik_model.x)

class SegmentsSlices:
    def __init__(self, data):
        self._segments = dict()
        self._nb_experiments = 0
        self._data = data

        if not FORCE_RELOAD and os.path.exists("slices.pickle"):
            with open("slices.pickle","rb") as fin:
                self._segments = pickle.load(fin)

    def is_loaded(self):
        return len(self._segments) > 0

    def save(self):
        with open("slices.pickle","wb") as fout:
            pickle.dump(self._segments, fout)

    def set(self, segment_nb, slice_nb, begin_pos, end_pos):
        if segment_nb not in self._segments:
            self._segments[segment_nb] = dict()

        self._segments[segment_nb][slice_nb] = (int(begin_pos), int(end_pos))

        if self._nb_experiments == 0:
            self._nb_experiments = slice_nb
        else:
            self._nb_experiments = max(self._nb_experiments, slice_nb)

    def get(self, segment, slice):
        begin_pos, end_pos= self._segments[segment][slice]
        return data[begin_pos: end_pos]

    def get_pos(self, segment, slice):
        return self._segments[segment][slice]

    def get_pos_segment(self, segment):
        mb,me = 9_999_999_999, 0
        for b,e in self._segments[segment].values():
            mb = min(mb,b)
            me = max(me,e)
        return mb, me

    def get_slices(self, slice):
        return [slices[slice] for slices in self._segments.values()]


    def reshape(self):
        for exp_id in tqdm(range(self._nb_experiments)):
            min_len = 9999999999
            for seg_id in self._segments.keys():
                min_len = min(min_len, self.get(seg_id, exp_id).shape[0])

            for seg_id in self._segments.keys():
                for slice_id in self._segments[seg_id].keys():
                    begin_pos, end_pos = self._segments[seg_id][slice_id]
                    self._segments[seg_id][slice_id] = (begin_pos, begin_pos+min_len)

def ticks_to_wav(ticks):
    """ Pay attention, tiks are usually in 44Khz units (so they're floats)
    Here we round these floats!
    """
    ticks = (ticks - np.min(ticks)).astype(int)
    wav = np.zeros( shape=(np.max(ticks)),).astype(float)
    s = 1.0
    for i in range(len(ticks)-1):
        b = ticks[i]
        e = ticks[i+1]
        wav[b:e] = s
        s = -s
    return wav

def load_accurapple_speaker(fpath, wav_freq=None):
    logger.info(f"Loading Accurapple ticks : {fpath}")
    accura_ticks = read_speaker_record(fpath)
    accura_ticks = accura_ticks - np.min(accura_ticks)

    if wav_freq is not None:
        if wav_freq != 1:
            k = 1/CYCLES_PER_SECOND*wav_freq
        else:
            k = 1
        wav = np.zeros( shape=(int(np.max(accura_ticks*k)),)).astype(float)
        s = 1.0
        for i in range(len(accura_ticks)-1):
            b,e= accura_ticks[i:i+2]
            wav[b:e] = s
            s = -s
    else:
        wav = None


    return accura_ticks.astype(int), wav


def analyse_beacons_wav(data: np.ndarray, filepath: Path, pause_duration, beacon_duration, noise_level, debug=False):
    logger.info("Locating 3-beacons mark")

    hist, edges = np.histogram(data, bins=100)
    log_mem_used()

    # There are recordings where the average oscillates around the zero
    # and where average oscillates above the zero.
    # But the zero is always the element that comes most often.
    mx = np.argmax(hist)
    zero = (edges[mx] + edges[mx+1])/2

    SHIFT = 0
    d = data
    # Since I'm looking for silence, regions of no movements
    # are regions of constant signal and constant signal
    # is heard as silence (if long enouhg, which is what I'm after)
    der = np.gradient(d)




    # der = d[1:]-d[0:-1]
    # der = np.hstack([der, np.zeros((1,))])
    c = np.convolve(np.abs(der), np.ones(250), mode="same")
    #der = None #helps GC
    d = d/np.max(d)
    c = c/np.max(c)

    # In front of the 3-beacons beacon, there must be at least
    # three seconds of silence, these will be used to build
    # a rough noise profile
    noisy_silence_start = SHIFT + 10
    noisy_silence_end = SHIFT + 3*samplerate + 10

    # Noise measured on derivatives
    silence_profile_der = der[noisy_silence_start:noisy_silence_end]
    noise_mean = np.mean(silence_profile_der)
    noise_std = np.std(silence_profile_der)
    NB_STD = 30
    noise_threshold_top = noise_mean + NB_STD*noise_std
    noise_threshold_bottom = noise_mean - NB_STD*noise_std

    noise_threshold_top = noise_level
    noise_threshold_bottom = -noise_level


    silence_profile = d[noisy_silence_start:noisy_silence_end]
    silence_mean = np.mean(silence_profile)

    if debug:
        plt.plot(der)
        plt.axhline(noise_threshold_top,c="green",ls="--",label="Noise threshold zone")
        plt.axhline(noise_threshold_bottom,c="green",ls="--")
        plt.axvline(noisy_silence_start,c="blue",ls="--",label="Noise profile zone")
        plt.axvline(noisy_silence_end,c="blue",ls="--")
        plt.legend()
        plt.title("First derivative")
        plt.show()


    log_mem_used()


    if debug and False:
        logger.info("Plotting")
        fig,ax = plt.subplots()
        ax = plt.subplot(2,2,1)
        ax.hist(data,bins=100)
        mean_data = np.mean(data)
        ax.axvline(zero,c="red",ls="--")
        ax.axvline(mean_data,c="green",ls="--")

        ax = plt.subplot(2,2,2)
        ax.plot(d,lw=1) # make it faster
        ax.axhline(zero,c="red",ls="--")
        ax.legend()

        ax = plt.subplot(2,2,3)
        ax.set_title("Convolution binned")
        ax.hist(c,bins=50)

        ax = plt.subplot(2,2,4)
        ax.set_title("Convolution data")
        ax.plot(c[::c.shape[0]//20000])
        #ax.axhline(NOISE_IN_CONV,c="black",ls="--",label="Noise level")
        mean = np.mean(c)
        std = np.std(c)
        ax.axhline(mean,c="red",ls="--",lw=1, label=f"Mean ({noise_mean:.3})")
        ax.axhline(mean+std,c="green",ls="--",lw=1, label=f"Mean+Std({noise_std:.3})")
        ax.legend()
        # ax.axhline(mean-2*std,c="red",ls="--")
        log_mem_used("About to plot")
        plt.show()
        fig.clear()
        plt.close(fig)
        fig = ax = None

    log_mem_used()

    logger.info("denoise")

    mask = (noise_threshold_bottom < der) & (der < noise_threshold_top)
    der[mask] = 0
    logger.info("Looking for zero runs")
    zero_runs = find_zero_runs(der)
    logger.info(f"Got {len(zero_runs)} zero runs")

    print(f"Pause duration={pause_duration}")
    def crunch_small_zones(zero_runs):
        zero_runs2 = []
        for i in range(len(zero_runs)):
            zr = zero_runs[i]
            zr_len = zr[1] - zr[0]
            if i >= 1:
                zr_prev = zero_runs[i-1]
                zr_prev_len = zr_prev[1] - zr_prev[0]
                if zr_len + zr_prev_len >= pause_duration*0.9:
                    print(f"Two long silences at {zr_prev[0]}-{zr_prev[1]} and {zr[0]}-{zr[1]}")
                    if zr[0] - zr_prev[1] < 2000 :
                        logger.info(f"Crunching a space of {zr[0] - zr_prev[1]}")
                        zero_runs2[-1] = [zr_prev[0], zr[1]]
                        continue

            zero_runs2.append( zr)
        zero_runs = np.array(zero_runs2)
        return zero_runs

    # zero_runs = crunch_small_zones(zero_runs)
    # zero_runs = crunch_small_zones(zero_runs)
    # zero_runs = crunch_small_zones(zero_runs)
    # zero_runs = crunch_small_zones(zero_runs)
    # zero_runs = crunch_small_zones(zero_runs)



    # Keep longest zero runs
    # Each beacon is surrounded by two long silences
    all_noises = []
    zero_runs2 = []
    for j in tqdm(range(zero_runs.shape[0])):
        b,e = zero_runs[j]
        if e-b > pause_duration:
            zero_runs2.append( (b,e) )
            l = (e-b)
            # Middle of silence: where I pick noise
            mid = b + l // 2
            k = int((l//2)*0.75)
            all_noises.append( data[mid - k : mid + k] )

    zero_runs2 = crunch_small_zones(zero_runs2)

    logger.info(f"noise {len(all_noises)} units")
    noise = np.hstack( all_noises )
    logger.info(f"Noise: {noise.shape[0]} samples, mean {np.mean(noise)}, std {np.std(noise)}")
    noise_threshold = np.mean(noise) + 4*np.std(noise)
    # plt.hist(noise, bins=177)
    # plt.show()
    log_mem_used()

    if debug:
        plt.title("Signal with silence (in derivative) removed")
        step = max(1, der.shape[0] // PLT_POINTS_PER_CHART)
        step = 1
        plt.plot(range(0,der.shape[0],step), der[::step],c="black",lw=1,label="Derivative")
        plt.plot(range(0,d.shape[0],step), d[::step],c="grey",lw=1,label="wav")
        #plt.axhline(NOISE_IN_CONV,c="red", ls="--", label="NOISE_IN_CONV")
        plt.axhline(noise_threshold,c="black", ls="--", label="Noise")
        plt.vlines([zr[0] for zr in zero_runs2],ymin=0, ymax=0.5, color="red",label="Start zero run")
        plt.vlines([zr[1] for zr in zero_runs2],ymin=0, ymax=-0.5, color="green",label="End zero run",ls="--")
        plt.legend()
        plt.show()

    # What's between the zero (silence) runs are the sound runs

    beacons = []
    for j in range(len(zero_runs2)-1):
        # We look between the zero runs
        b1,e1 = zero_runs2[j]
        b2,e2 = zero_runs2[j+1]
        #silence = e1-b1 + e2-b2
        potential_beacon_len = b2 - e1
        assert potential_beacon_len > 0, "between two spaces there must be something: {b1}-{e1}, {b2}-{e2}"
        print(f"at {e1-SHIFT}: len1:{e1-b1}-len2:{potential_beacon_len}-len3:{e2-b2} {potential_beacon_len}/{beacon_duration}")
        # I put a rather big margin because second silence is usually
        # shorter than the first one.
        if beacon_duration[0] < potential_beacon_len < beacon_duration[1]:
            # Start the beacon on its first peak
            begin = e1 - 50
            # while abs(der[begin]) < 0.01:
            #     begin += 1

            beacons.append( (begin-SHIFT,b2-SHIFT+150) )
            print("Good beacon")

    if debug:
        logger.info("plotting")
        plt.title(f"Detected beacons (starts, {len(beacons)}x)")
        plt.plot(range(0,data.shape[0],data.shape[0]//PLT_POINTS_PER_CHART),
                 data[::(data.shape[0]//PLT_POINTS_PER_CHART)],lw=1)
        for beg,end in beacons:
            plt.axvline(beg,c="red",ls="--")
        plt.show()

    logger.info(f"Saving beacon data in {filepath}")
    pickle.dump(beacons, open(filepath, 'wb'))
    return beacons


def ticks_beacons(data: np.ndarray, filepath: Path, pause_duration, beacon_duration):
    assert filepath is not None

    logger.info("Starting ticks beacon detection")
    silences = data[1:]-data[0:-1]
    # pad_silence = np.array([pause_duration+100])
    # silences = np.hstack( [pad_silence, silences,pad_silence] )
    long_silences = np.argwhere(silences > pause_duration)[:,0]

    # Example: two ticks : 100,200
    # Silence goes from 100+1 to 200-1
    begins = data[long_silences]+1
    ends = data[long_silences+1]-1
    zero_runs = [(-pause_duration*2, -1)] + \
          [(begins[i], ends[i]) for i in range(begins.shape[0])] + \
          [(data[-1]+1, data[-1]+1+pause_duration*2)]

    beacons = []
    for j in range(len(zero_runs)-1):
        # We look between the zero runs
        b1,e1 = zero_runs[j]
        b2,e2 = zero_runs[j+1]
        #silence = e1-b1 + e2-b2
        potential_beacon_len = b2 - e1

        assert potential_beacon_len > 0, "between two silences (ticks) there must be something: {b1}-{e1}, {b2}-{e2}"

        if beacon_duration[0] < potential_beacon_len < beacon_duration[1]:
            beacons.append((e1-1,b2+1))

    alter = np.ones((data.shape[0],))
    alter[::2] = 0
    plt.title(f"AcurrApple ticks for {len(beacons)} beacons")
    plt.step(data,alter,c="grey",lw=1)
    plt.vlines(begins, ymin = -0.1, ymax = 0.5, color="red", lw=2, label="Silence start")
    plt.vlines(ends, ymin = -0.1, ymax = 0.5, color="red", lw=2, ls="--", label="Silence end")
    plt.vlines([b[0] for b in beacons], ymin = 0, ymax = 1, color="green", label="Beacon start")
    plt.legend()
    plt.show()

    logger.info(f"Writing ticks beacons to {filepath}")
    pickle.dump(beacons, open(filepath, 'wb'))
    return beacons





def load_segments_slices(beacons):
    all_data = SegmentsSlices(data)

    if not all_data.is_loaded():
        len_series = None
        for beacon_num in tqdm(range(len(beacons)-1)):
            b, e = beacons[beacon_num]
            b2,e2=beacons[beacon_num+1]
            seg_start = b + AVERAGE_BEACON_LENGTH
            seg_end = b2-1
            segment = data[seg_start:seg_end]

            silence = np.abs(segment)
            NOISE_LEVEL=0.02
            silence[silence < NOISE_LEVEL] = 0
            zr = find_zero_runs(silence)
            midpoints = []
            for b,e in zr:
                if e - b > 2500:
                    midpoints.append(b+(e-b)//2)

            if True:
                plt.plot(np.convolve(np.abs(segment), np.ones(250), mode="same"))
                plt.show()

                h = plt.hist(np.abs(segment),bins=100)
                print(h)
                plt.show()

                plt.plot(segment,lw=0.5)
                for x in midpoints:
                    plt.axvline(x,c="red")
                plt.show()

            CONV_NOISE_LEVEL=0.005
            intervals_lens = []
            for i in range(len(midpoints)-1):
                b_org,e_org=midpoints[i:i+2]
                # Convolution attenuates noise and makes noise detection easier
                slice1 = segment[b_org:e_org]
                CONV_LEN=41
                conv_seg=np.convolve(slice1,1/CONV_LEN*np.ones((CONV_LEN,)))[CONV_LEN//2:]
                conv_seg_abs = np.abs(conv_seg)
                maxpos = np.argmax(np.abs(conv_seg_abs))

                noise_at_the_beginning = conv_seg_abs[0:int(maxpos*0.8)]
                noise_level_beginning = np.max(noise_at_the_beginning)*1.2

                noise_at_the_end = conv_seg_abs[-((conv_seg_abs.shape[0] - maxpos)//4):]
                noise_level_end = np.max(noise_at_the_end)*1.5

                center = np.mean(np.hstack([noise_at_the_beginning, noise_at_the_end]))
                #first_signal_ndx = maxpos-500+np.min(np.argwhere(np.abs(conv_seg[maxpos-500:maxpos]) > noise_level_beginning)[:,0])
                first_signal_ndx = maxpos - 120 - CONV_LEN
                last_signal_ndx = maxpos+np.max(np.argwhere(np.abs(conv_seg[maxpos:]) > noise_level_end)[:,0])
                if False: # i > len(midpoints)-3:
                    # Helps analysing wave's position
                    plt.title(f"{i}-th slice, zero:{center:.3f}")
                    plt.plot(segment[b_org:e_org],lw=1,c="red")
                    plt.plot(conv_seg[0:first_signal_ndx],c="grey")

                    plt.plot(range(last_signal_ndx, conv_seg.shape[0]),
                            conv_seg[last_signal_ndx:conv_seg.shape[0]],c="grey")
                    # Horiz line to show noise levels
                    plt.plot([0,first_signal_ndx],[noise_level_beginning]*2, c="black", lw=1, ls="--")
                    plt.plot([0,first_signal_ndx],[-noise_level_beginning]*2, c="black",lw=1, ls="--")
                    plt.plot([last_signal_ndx,conv_seg.shape[0]],[noise_level_end]*2, c="black",lw=1, ls="--")
                    plt.plot([last_signal_ndx,conv_seg.shape[0]],[-noise_level_end]*2, c="black",lw=1, ls="--")

                    plt.axvline(first_signal_ndx, c="black")
                    plt.axvline(last_signal_ndx, c="black")
                    plt.show()

                all_data.set(beacon_num, i, seg_start+b_org+first_signal_ndx, seg_start+b_org+last_signal_ndx)

                # print(all_data.get_pos_segment(beacon_num))
                # print(seg_start, seg_end)
                #intervals_lens.append(last_signal_ndx-first_signal_ndx)

            # extremes = find_tops(segment, above=0.37)
            # np_extremes = np.array(extremes, dtype=int)
            # tops_ndx = np.argwhere(segment[np_extremes] > 0.37)[:,0]

            # first_serie = tops_ndx

            # if len_series is None:
            #     len_series = len(tops_ndx)

            # if len(tops_ndx) != len_series:
            #     # All series must have the same number of experiments
            #     plt.plot(segment)
            #     plt.scatter(np_extremes[first_serie[:20]], segment[np_extremes[first_serie[:20]]],marker="x", c="orange")
            #     plt.scatter(np_extremes[first_serie[20:]], segment[np_extremes[first_serie[20:]]],marker="x", c="red")
            #     plt.show()

            # intervals_lens = []
            # for i in range(len(first_serie[:-1])):
            #     # From one top to the other
            #     b,e=np_extremes[first_serie[i]], np_extremes[first_serie[i+1]]
            #     d = int((e-b)*0.9)
            #     intervals_lens.append(d)
            #     all_data.set(beacon_num, i, seg_start+b, seg_start+b+d)
            # # we're off by one, fix that.
            # i += 1
            # d = np.mean(intervals_lens)
            # b = np_extremes[first_serie[i]]
            # all_data.set(beacon_num, i,
            #     seg_start+b,
            #     seg_start+b+d)

        all_data.reshape()
        all_data.save()
    return all_data


def beacon_load(data, wav_path:Path, silence_duration, beep_duration, noise_level):
    assert wav_path.exists()
    beacons_file = wav_path.with_suffix(wav_path.suffix + ".beacon")
    if not noise_level and beacons_file.exists():
        logger.info(f"Loading beacons offset from {beacons_file}")
        beacons = pickle.load(open(beacons_file, 'rb'))
    else:
        if wav_path.suffix in (".wav",".flac"):
            beacons = analyse_beacons_wav(data, beacons_file,  silence_duration, beep_duration, float(noise_level),
                                          debug=cli_args.debug)
        elif wav_path.suffix == ".ticks":
            beacons = ticks_beacons(data, beacons_file, silence_duration, beep_duration)
        else:
            raise Exception(f"Unrecognized extension {wav_path.suffix}")
    return beacons




def beacons_show(ax, wav_data, beacons):
    n = wav_data.shape[0]
    step= 10
    r = np.arange(0,n,step)
    ax.plot(r, wav_data[r], lw=1)

    for b, e in beacons:
        ax.axvline(b,c="red", lw=1)
        ax.axvline(e,c="red",ls="--", lw=1)





if cli_args.ticks_stats:

    if cli_args.ticks:
        from scipy.fft import rfft, rfftfreq

        CYCLES_PER_SAMPLE=CYCLES_PER_SECOND/44100

        if False:
            # Here we generate a random signal
            ticks = []
            seed = 123
            s = 0
            N = (0xC000 - 0x2000) // 3
            print(f"{N*4/(50*20280)} seconds")
            rnd = np.round(np.random.random(N)).astype(int)
            for i in tqdm(range(N)):
                # seed = ((seed * 1103515245) + 1) % 4_294_967_295
                # if seed & 2147483648 > 0:
                #     s = 1 - s
                seed = rnd[i]
                if seed  > 0:
                    s = 1 - s
                ticks.extend([s]*(12+4)) # Cycles to cover "JSR+RTS; BIT speaker" or "JSR+RTS; NOP,NOP" (12+4 cycles)
                #ticks.append(s)
                #ticks.extend([0]*60)
            accura_wav = np.array(ticks)

        if True:
            accura_ticks, accura_wav = load_accurapple_speaker(cli_args.ticks, wav_freq=1)
            logger.info(f"AccuraTicks : nb={len(accura_ticks)}")
            accura_wav = accura_wav[int(1.1e7): int(2.7e7)]

        # plt.plot(accura_wav)
        # plt.show()

        # Note the extra 'r' at the front
        logger.info(f"Fourrier on {accura_wav.shape[0]} samples")
        yf = rfft(accura_wav)
        xf = rfftfreq(accura_wav.shape[0], 1 / CYCLES_PER_SECOND)

        logger.info("Plotting")

        i = 0
        while xf[i] < 44100:
            i += 1

        plt.plot(xf[:i], np.abs(yf)[:i])
        plt.yscale('log')
        plt.show()


    all_ticks = sorted(list(glob.glob("recordings/*.ticks")))
    for ticks_file in all_ticks:
        ticks, _ = load_accurapple_speaker(ticks_file)
        nb_frames = ticks[-1] / (SRC_FREQ/50)
        ticks_per_frame = ticks.shape[0] / nb_frames
        d = ticks[1:]-ticks[:-1]
        d = d[d < 1000]
        plt.hist(d, bins=np.arange(0,1000,SRC_FREQ/TARGET_FREQ))
        plt.yscale('log')
        plt.title(f"Time between two ticks; {ticks_per_frame:.1f} ticks per frame\n{ticks_file}")
        plt.xlabel("One bin == one 44Khz sample")
        plt.show()
    exit()



if cli_args.compare_fft:
    # python snd_fit2.py --ticks recordings/noise.ticks --wav recordings/noise_apple2e.wav  --show-fft 1
    # python snd_fit2.py --ticks recordings/noise.ticks --wav recordings/noise_kegs.wav --show-fft 1

    data, samplerate = soundfile.read("recordings/white_noise_mic.wav")
    wn_yf = np.abs(np.fft.rfft(data))
    wn_xf = np.fft.rfftfreq(data.shape[0], 1 / samplerate)

    filt = "kena3" # kena2 (kena at 1Mhz + downasampling), kena3 (at 44KHz), kegs

    freqs = []
    amps = []
    with open("spectrum_webcam_white_noise_audacity.txt") as fin:
        fin.readline()
        for line in fin.readlines():
            freq, amp = list(map( float, line.split()))
            freqs.append(freq)
            amps.append(amp)
    freqs = np.array(freqs)
    amps = np.array(amps)
    amps = np.exp((amps-np.min(amps))/(2*np.log(10)))

    a2e_fft_freqs = np.load("recordings/noise_apple2e.wav1.fft_x.npy")
    a2e_fft_counts = np.abs(np.load("recordings/noise_apple2e.wav1.fft_y.npy"))
    accura_kegs_fft_freqs = np.load(f"recordings/noise_{filt}.wav1.fft_x.npy")
    accura_kegs_fft_counts = np.abs(np.load(f"recordings/noise_{filt}.wav1.fft_y.npy"))

    a2e_fft_freqs2 = np.load("recordings/noise_apple2e.wav2.fft_x.npy")
    a2e_fft_counts2 = np.abs(np.load("recordings/noise_apple2e.wav2.fft_y.npy"))
    accura_kegs_fft_freqs2 = np.load(f"recordings/noise_{filt}.wav2.fft_x.npy")
    accura_kegs_fft_counts2 = np.abs(np.load(f"recordings/noise_{filt}.wav2.fft_y.npy"))

    def prep(x):
        n = 100
        m = x.shape[0]
        print(m)
        x = x-x[-m//4]
        x = x / np.sum(x)
        c = np.convolve(x, np.ones((n,)), mode="same")
        #c = c / np.max(c)
        return c #np.log10(c)

    def prep2(x):
        n = 100
        # Ha ve the same frequencies in the spectrum (else you can't compare energies!)
        ndx = np.linspace(0,x.shape[0]-1,22050).astype(int)
        x = x[ndx]
        x = np.abs(x)
        x = np.convolve(x, np.ones((n,)), mode="same")
        # normalize energy across the spectrum
        x = x / np.sum( x)
        x = x / x.shape[0]
        print(x.shape)
        print( np.sum(x))
        return x

    print("{} {}".format(np.min(a2e_fft_freqs),np.max(a2e_fft_freqs)))
    print("{} {}".format(np.min(a2e_fft_freqs2),np.max(a2e_fft_freqs2)))
    print("{} {}".format(np.min(freqs),np.max(freqs)))

    ax = plt.subplot(2,1,1)
    ax.plot(prep2(a2e_fft_counts), label="seg1")
    ax.plot(prep2(a2e_fft_counts2), label="seg2")
    ax.plot(prep2(wn_yf), label="white noise mic")
    ax.set_title("FFT of A2+ recordings (energy normalized, wav)")
    ax.set_xlabel("Hertz")
    #ax.set_yscale("log")
    ax.legend()

    ax = plt.subplot(2,1,2)
    ax.plot(prep2(a2e_fft_counts)/prep2(wn_yf), label="seg1")
    ax.plot(prep2(a2e_fft_counts2)/prep2(wn_yf), label="seg2")
    ax.plot(prep2(accura_kegs_fft_counts2)/prep2(wn_yf), label="kegs")

    ax.set_title("Idem, MIC effects taken into account")
    #ax.legend()
    #ax.set_yscale("log")
    ax.set_xlabel("Hertz")
    #plt.xlim(0,10000)
    ax.legend()
    plt.show()

    # Make things more comparable
    a2e_fft_counts = a2e_fft_counts / np.sum(a2e_fft_counts) * np.sum(accura_kegs_fft_counts)
    a2e_fft_counts2 = a2e_fft_counts2 / np.sum(a2e_fft_counts2) * np.sum(accura_kegs_fft_counts2)

    n_average = 50
    averager = np.ones( (n_average,)) / n_average
    a2e_fft_counts = np.convolve(a2e_fft_counts, averager, mode="same")
    accura_kegs_fft_counts = np.convolve(accura_kegs_fft_counts, averager, mode="same")
    a2e_fft_counts2 = np.convolve(a2e_fft_counts2, averager, mode="same")
    accura_kegs_fft_counts2 = np.convolve(accura_kegs_fft_counts2, averager, mode="same")

    plt.suptitle("FFT of 2 noise generation experiments",weight="bold")
    ax = plt.subplot(2,1,1)
    CYCLES_PER_SECOND
    ax.set_title(f"Noise: freq_max={CYCLES_PER_SECOND/4:.0f} Hz")
    ax.set_xlim(n_average, a2e_fft_freqs[-n_average])
    ax.plot(a2e_fft_freqs, a2e_fft_counts, lw=1, label="Recorded A2e")
    ax.plot(accura_kegs_fft_freqs, accura_kegs_fft_counts, lw=1, label=f"Accurapple [{filt}]")
    ax.legend()
    #ax.set_yscale('log')

    ax = plt.subplot(2,1,2)
    ax.set_title(f"Noise: freq_max={CYCLES_PER_SECOND/61:.0f} Hz")
    ax.set_xlim(n_average, a2e_fft_freqs[-n_average])
    ax.plot(a2e_fft_freqs2, a2e_fft_counts2, lw=1, label="Recorded A2e")
    ax.plot(accura_kegs_fft_freqs2, accura_kegs_fft_counts2, lw=1, label=f"Accurapple [{filt}]")
    #ax.set_yscale('log')
    ax.set_xlabel("Hertz")
    ax.legend()

    plt.show()
    exit()





# SOUND_FILE=Path('recordings/test2_redmi.wav')
# ACCURA_TICKS_FILE = Path("recordings/test2_accurapple.bin")

if cli_args.wav:
    SOUND_FILE=Path(cli_args.wav)
    logger.info(f"Loading WAV {SOUND_FILE}")
    data, samplerate = soundfile.read(str(SOUND_FILE))
    if len(data.shape) == 2:
        # stereo
        data = data[:,0] # left
    crc_wav = crc32(data.tobytes())

    # Normalize
    data = data.astype(np.float32)
    data = data - np.mean(data)
    data = data / np.max(np.abs(data))
    CYCLES_PER_SAMPLE=CYCLES_PER_SECOND/samplerate
    SAMPLE_BETWEEN_BEACONS_IN_EXTREMES=CYCLES_BETWEEN_EXTREMES/CYCLES_PER_SECOND*samplerate
    logger.info(f"Done loading sound {SOUND_FILE} {samplerate}Hz, {data.shape}, Cycles per sample = {CYCLES_PER_SAMPLE:.2f}, CRC={hex(crc_wav)}")

# Now loading the ticks
if cli_args.ticks:
    ACCURA_TICKS_FILE = Path(cli_args.ticks)
    accura_ticks, accura_wav = load_accurapple_speaker(ACCURA_TICKS_FILE)
    logger.info(f"AccuraTicks : nb={len(accura_ticks)}")

log_mem_used()
if cli_args.show_data:
    ax = plt.subplot(2,1,1)
    ax.set_title("WAV")
    ax.plot(data[::10])
    ax = plt.subplot(2,1,2)
    ax.set_title("Ticks")
    ax.vlines(accura_ticks, ymin=-1,ymax=1, color="black", lw=1)
    #ax.plot(accura_wav)
    plt.show()

#ticks_beacons(accura_ticks, None, 780000, (400000,500000))

# accura wav is 1MHz !
# Load or create beacons for ticks.
accurapple_beacons = beacon_load(accura_ticks, ACCURA_TICKS_FILE, 780000, (2271*200*0.95,2271*200*1.1), noise_level=cli_args.find_beacons)
logger.info(f"Beacons in AccurApple ticks {len(accurapple_beacons)}")
logger.info("Removing groups of three beacons at begin and end of ticks beacons")
accurapple_beacons = accurapple_beacons[3:len(accurapple_beacons)-2]

beacons = beacon_load(
    data, SOUND_FILE,
    silence_duration=int(0.625*samplerate),
    beep_duration=(2271*200/CYCLES_PER_SAMPLE*0.95,2271*200/CYCLES_PER_SAMPLE*1.1),
    noise_level=cli_args.find_beacons)
beacons = beacons[3:len(beacons)-2]

assert len(beacons) == len(accurapple_beacons), \
    f"The number of beacons in wav ({len(beacons)}) and ticks ({len(accurapple_beacons)}) differ. Are the data correct ?"

joined_beacons_name = SOUND_FILE.parent / f"{hex(crc_wav)}.beacons"
if not cli_args.find_beacons and os.path.exists(joined_beacons_name):
    logger.info(f"Loading joined data {joined_beacons_name}")
    with open(joined_beacons_name,"rb") as fin:
        all_beacons = pickle.load(fin)
else:
    logger.info(f"Building joined data {joined_beacons_name}")
    all_beacons = AllBeacons()
    for bi in tqdm(range(min(1000, len(beacons)))):
        bb,be = beacons[bi]

        bw: BeaconWav = BeaconWav(bi, bb,be,data,
                                samples_between_extremes=SAMPLE_BETWEEN_BEACONS_IN_EXTREMES,
                                debug=True)
        bt: BeaconTicks = BeaconTicks( accurapple_beacons[bi][0], accurapple_beacons[bi][1], accura_ticks)
        all_beacons.append(bw,bt)

    with open(joined_beacons_name,"wb") as fout:
        pickle.dump(all_beacons, fout)


if cli_args.show_beacons:
    ax = plt.subplot(2,2,1)
    ax.set_title(f"WAV {len(beacons)} beacons")
    beacons_show(ax, data, beacons)
    # ax.axvline(wav_limits[0], c="black")
    # ax.axvline(wav_limits[1], c="black")
    # ax.set_xlim(rec_wav_limits)
    ax.set_xticklabels([])

    ax = plt.subplot(2,2,2)
    ax.set_title(f"AccurApple ticks {len(accurapple_beacons)} beacons")
    alter = np.ones((accura_ticks.shape[0],))
    alter[::2] = 0
    ax.step(accura_ticks, alter, lw=1)
    for b,e in accurapple_beacons:
        ax.axvline(b,c="red",lw=1)
        ax.axvline(e,c="red",lw=1, ls="--")
    # ax.axvline(ticks_limits[0], c="black", ls="--")
    # ax.axvline(ticks_limits[1], c="black", ls="--")
    # ax.set_xlim(rec_ticks_limits)
    ax.set_xticklabels([])

    ax = plt.subplot(2,2,3)
    test_beacon_ndx=len(beacons)//2

    plt.plot(data[slice(*beacons[test_beacon_ndx]) ])
    b,e = accurapple_beacons[test_beacon_ndx]
    ticks = accura_ticks[(accura_ticks >= b-2000) & (accura_ticks <= e)]
    ticks = (ticks - ticks[0])/K
    for t in ticks:
        ax.axvline(t,c="red",lw=1)
    ax.set_title(f"Beacon {test_beacon_ndx} WAV/ticks({ticks.shape[0]}) alignment check. K={K:.6f}")

    ax = plt.subplot(2,2,4)
    ax.set_title(f"Data after beacon {test_beacon_ndx} WAV/ticks alignment check")

    wav, ticks = all_beacons.get_data_segment(test_beacon_ndx,with_beacons=False)
    plt.plot(wav)
    for t in ticks:
        ax.axvline(t,c="red",lw=1)

    plt.show()




if cli_args.show_beacons_tops:
    for bi in range(len(all_beacons)):
        bw, bt = all_beacons[bi]
        joined_beacons = all_beacons.joined[bi]

        # Our goal is convert the Accura ticks positions
        # to wav ticks positions
        #tt_ticks = bt.tops_times()
        #tt_wav = bw.tops_times()
        # tt_ticks = np.array(tt_ticks)
        # tt_ticks = (tt_ticks - tt_ticks[0]) / joined_beacons.k + tt_wav[0]

        mean_wav = joined_beacons.robust_mean_wav - bw.begin
        mean_ticks = joined_beacons.ticks_to_wav_beacon_local(joined_beacons.robust_mean_ticks)
        ax = plt.subplot(1,1,1)
        ax.set_title(f"Beacon (wav) {bi+1}/{len(beacons)}, k={joined_beacons.k:.3f} Δmeans={abs(mean_wav-mean_ticks):.3f}")
        ax.plot(bw.segment(), lw=1, c="grey", label="WAV signal", zorder=0)
        s = bw.segment()
        interp_wav = interp1d(np.arange(s.shape[0]), s)


        ax.axvline(mean_wav, c="lime",lw=1,ls="-.",label="Mean of wav ticks")
        ax.axvline(mean_ticks, c="magenta",lw=1,ls="--",label="Mean of AApple ticks")

        #ax.axhline(bw.threshold(),c="black",ls="--",label="Threshold for extremes")
        #ax.axhline(-bw.threshold(),c="black",ls="--")

        ax.scatter(bw.tops_times() - bw.begin, bw.tops_values(),
                   c="red",label=f"WAV detected ticks ({len(bw.tops_times())})", marker="x", zorder=1)

        ticks = joined_beacons.ticks_to_wav_beacon_local(bt.tops_times())
        ax.scatter(ticks,
                   #bw.segment()[np.round(ticks).astype(int)],
                   interp_wav(ticks),
                   c="orange", marker="|", zorder=10,label=f"AApple ticks ({len(ticks)})")
        ax.legend()

        #ax.vlines(bw._closest, ymin=-0.1, ymax=0.1, label="closest")

        manager = plt.get_current_fig_manager()
        manager.full_screen_toggle()
        plt.show()



from numpy.lib.stride_tricks import sliding_window_view, as_strided
def strided_app(a, L, S ):  # Window len = L, Stride len/stepsize = S
    nrows = ((a.size-L)//S)+1
    n = a.strides[0]
    print(n)
    return np.lib.stride_tricks.as_strided(a, shape=(nrows,L), strides=(S*n,n))

from scipy.ndimage import maximum_filter1d, minimum_filter1d

if cli_args.show_beacon:
    for beacon_id in range(int(cli_args.show_beacon) - 1, len(all_beacons)):
        wav, ticks = all_beacons[beacon_id]
        w = wav._wav[wav.begin:wav.end]
        deriv = np.gradient(w)

        t = all_beacons.joined[beacon_id].ticks_to_wav_beacon_local(ticks.ticks())
        #t = ticks.ticks() / CYCLES_PER_SAMPLE
        logger.info(f"Ticks to wav: {t.shape[0]} ticks, max={max(t - t[0])}")
        ticks_wav = ticks_to_wav(t)

        logger.info(f"ticks_wav len = {ticks_wav.shape[0]}")
        logger.info(f"wav len = {w.shape[0]}")
        #exit()

        ax = plt.subplot(1,1,1)
        ax.set_title(f"Beacon {beacon_id + 1}/{len(all_beacons)}")
        ax.plot(w, label="WAV")
        ax.plot(deriv,lw=1,ls="--", label="Derivative")
        ax.plot(ticks_wav, label="Ticks")
        ax.legend()

        plt.show()


if cli_args.show_segment:
    seg_start = int(cli_args.show_segment) - 1
    N = 2 # plots per page
    for ndx in range(seg_start,all_beacons.nb_segments()):
        segment, ticks = all_beacons.get_data_segment(ndx, with_beacons=False)
        deriv = np.gradient(segment)

        ax = plt.subplot(N,1,((ndx-seg_start) % N)+1 )
        ax.plot(segment,lw=1,label="wav")
        #ax.plot(deriv,lw=1,ls="--",label="derivative wav")

        # smax = maximum_filter1d(segment, 15)
        # smin = minimum_filter1d(segment, 15)
        # ax.plot(smax-smin)
        #l = segment.shape[0]
        #ax.set_xlim( l//2-1000, l//2+1000)
        ax.set_title(f"Segment {ndx+1}/{all_beacons.nb_segments()}")

        interp_wav = interp1d(range(segment.shape[0]), segment)
        tvalues = interp_wav(ticks)
        ax.scatter(ticks, tvalues, marker="x", c="red", label="Ticks")
        #ax.vlines(ticks,ymin=-1, ymax=1, lw=1, ls="--", colors="red", label="ticks")
        ax.legend()

        if ndx % N == N - 1:
            plt.show()


if cli_args.show_fft:

    if cli_args.show_fft == "beacon":
        ticks = all_beacons.ticks(0).ticks()
        wav = all_beacons.wav(0).segment()
        title = f"FFT for beacon (~220Hz!)"
        # This works only on noise
        deriv = np.gradient(ticks)
        min_period = 4
    else:
        seg_num = int(cli_args.show_fft)
        assert seg_num >= 1, "We count from one for segment numbers"

        ndx = seg_num - 1
        wav, ticks = all_beacons.get_data_segment(ndx, with_beacons=False)
        title = f"FFT for segment {ndx+1}"
        ticks = ticks * CYCLES_PER_SAMPLE
        min_period = 4 # BIT $C030 is 4 cycle

    wav1Mhz = - ticks_to_wav( (ticks).astype(int))
    deriv = np.gradient(ticks)
    logger.info("FFT")
    yf = np.fft.rfft(wav1Mhz)
    xf = np.fft.rfftfreq(wav1Mhz.shape[0], 1 / CYCLES_PER_SECOND)

    ax = plt.subplot(2,2,1)
    ax.set_title(f"{title}, {ticks.shape[0]} ticks at {CYCLES_PER_SECOND} Hz")
    ax.plot(xf, np.abs(yf), lw=1)
    ax.set_yscale('log')
    ax.set_xlabel("Hertz")
    mp = CYCLES_PER_SECOND/min_period
    ax.axvline(mp, label=f"max_freq {round(mp)} Hz ({round(min_period)}c period)", c="black")
    #ax.axvline(mp*2, label=f"max_freq*2 {round(mp*2)} Hz", c="black")
    ax.set_xlim(0, mp*1.1)
    ax.legend()

    # Remove the silence around the segment
    deriv = np.gradient(wav)
    i = 0
    while deriv[i] < 0.05:
        i += 1
    j = deriv.shape[0]-1
    while deriv[j] < 0.05:
        j -= 1
    wav = wav[i:j]
    #wav = wav[200:-200]

    #wav = wav * scipy.signal.windows.hann(wav.shape[0])

    logger.info(f"Fourrier on {wav.shape[0]} samples at {samplerate} Hz. Cycles per sample {CYCLES_PER_SAMPLE}")
    yf = np.fft.rfft(wav)
    xf = np.fft.rfftfreq(wav.shape[0], 1 / samplerate)

    np.save(cli_args.wav + cli_args.show_fft + ".fft_x", xf)
    np.save(cli_args.wav + cli_args.show_fft + ".fft_y", np.abs(yf))

    logger.info("Plotting")

    i = 0
    # while xf[i] < 88200:
    #     i += 1

    ax = plt.subplot(2,2,2)
    ax.set_title(f"{title}, {wav.shape[0]} samples at {samplerate} Hz")
    ax.plot(xf, np.log10(2*np.abs(yf)), lw=1)
    #ax.set_yscale('log')
    ax.set_xlabel("Hertz")
    ax.axvline(44100, c="red", label=f"{samplerate/1000:.1f}KHz")
    ax.axvline(50, c="red", label="50Hz")
    ax.axvline(3875, label=f"SHO freq= {3875} Hz", c="black")
    ax.legend()

    ax = plt.subplot(2,2,4)
    ax.set_title(f"WAV, {wav.shape[0]} samples at {samplerate} Hz")
    ax.plot(wav, lw=1)

    plt.show()


if cli_args.compare_segments:
    segments = [int(x) for x in cli_args.compare_segments]
    ax = plt.subplot(1,1,1)
    ax.set_title(f"Comparing segments:{segments}")
    colors = ["red","orange","blue","green","lime","black"] * 4

    for ndx in segments:
        segment_wav, ticks = all_beacons.get_data_segment(ndx, with_beacons=False)
        grad = np.gradient(segment_wav[:segment_wav.shape[0]//10])
        delta = 1/30
        interp_grad = interp1d(range(grad.shape[0]), grad)

        x = 0
        while interp_grad(x) < 0.01:
            x += 20

        x -= max(0,200)
        while interp_grad(x) < 0.01:
            x += delta

        #fs = all_beacons.short_sequences(ndx)[0]
        ax.plot(np.arange(-x, -x + segment_wav.shape[0]), segment_wav, lw=1,
            c = colors[ndx], label=f"WAV {ndx}")
        ax.vlines(ticks-x,ymin=-1, ymax=1, lw=1, ls="--",
                  colors=[colors[ndx]]*ticks.shape[0], label=f"Ticks {ndx}")
    ax.legend()
    plt.show()


if cli_args.peak_analysis:

    all_p = []
    all_starts = []
    all_drops = []
    all_after_drops = []
    first_change = []
    second_change = []
    all_data = []
    plot_intermediate = False
    plot_thumbnails = False

    logger.info(f"Processing {len(all_beacons)-1} segments")
    logger.info(f"Saving results in {Path.cwd()}")

    for experiment in tqdm(range(int(cli_args.peak_analysis),len(all_beacons)-1)):
    #for experiment in tqdm(range(0,4)):
        p = []
        drops = []
        after_drops = []
        starts = []

        segment_wav, ticks_wav = all_beacons.get_data_segment(experiment,with_beacons=False)
        short_sequences = all_beacons._find_ticks_sequences(experiment,close_threshold=200,with_beacons=False)

        grad = np.gradient(segment_wav)
        delta = 1/30
        #interp_grad = interp1d(np.arange(0, segment_wav.shape[0]), grad)
        #interp_wav = interp1d(np.arange(0, segment_wav.shape[0]), segment_wav)

        if plot_intermediate:
            plt.plot(segment_wav)
            plt.plot(grad, c="black",lw=1, ls="--")
            #plt.xlim(47690-100,47690+100)
            plt.axhline(0,lw=1,c="black")
            for sn in tqdm(range(len(short_sequences)-1), leave=False):
                if len(short_sequences[sn]) <= 1:
                    continue
                plt.vlines(short_sequences[sn][0],-0.1,0.1,colors="yellow")
            #plt.show()

        ax_num=1
        GROWS,GCOLS = 4,8

        for sn in tqdm(range(len(short_sequences)-1), leave=False):
            if len(short_sequences[sn]) <= 1:
                continue
            short_sequence = np.array(short_sequences[sn])

            # It is much faster (to query) to build small interp1d
            # each time we need them instead of building a big one.
            begin_s = int(short_sequence[0]-20)
            end_s = int(short_sequence[-1]+20)
            interp_wav = interp1d(np.arange(begin_s, end_s),
                                  segment_wav[begin_s: end_s])
            interp_grad = interp1d(np.arange(begin_s, end_s),
                                  grad[begin_s: end_s])

            # Locate when the first sequence really starts.
            # (The ticks are bit off, so we put back the ticks where
            # the WAV start changing)
            tick = short_sequence[0]
            shift = -1
            while shift > -7 and (abs(interp_grad(tick+shift)) > 0.005 or \
                abs(interp_wav(tick+shift)) > 0.01):
                shift -= 1/20

            # Once we have located the start, we improve it a bit
            # so that the other "ticks" in the sequence are
            # closer to derivative changes

            scores = []
            itick = int(short_sequence[0])
            for shift2 in range(10):
                score = 0
                d = shift+shift2/10
                for t in short_sequence:
                    score += abs(interp_wav(t+d)*interp_grad(t+d))
                scores.append(score)

            shift = shift+np.argmin(scores)/10

            # The ticks translated so that they are where
            # the signal actually changes.
            xltd_short_sequence = short_sequence+shift

            # Record where we are at the second tick (so under influence of the first one)
            first_change.append( (xltd_short_sequence[1]-xltd_short_sequence[0],
                                  interp_wav(xltd_short_sequence[1])) )
            if len(short_sequence) >= 3:
                # Record where we are at the third tick
                second_change.append( (xltd_short_sequence[1]-xltd_short_sequence[0],
                                    xltd_short_sequence[2]-xltd_short_sequence[1],
                                    interp_wav(xltd_short_sequence[2]-interp_wav(xltd_short_sequence[1]))) )

            # We search fo the next valid extreme right after
            # the second tick (provided it occurs before the third tick)
            tick = xltd_short_sequence[1] + 1
            nb_ticks=len(xltd_short_sequence)
            while (nb_ticks == 2 or (nb_ticks >= 3 and tick < xltd_short_sequence[2])) \
                  and abs(interp_grad(tick)) > 0.01:
                tick += 1/10
            all_data.append(
                (interp_wav(xltd_short_sequence[1]),
                 interp_grad(xltd_short_sequence[1]),
                 interp_wav(tick),
                 tick - xltd_short_sequence[1]))
            if plot_intermediate:
                plt.vlines(tick, ymin=-0.8, ymax=0.8, colors="orange", ls="--")

            # We do it again but for the third tick (if there's one)
            if nb_ticks == 3:
                tick_extreme = xltd_short_sequence[2] + 1
                while abs(interp_grad(tick_extreme)) > 0.01:
                    tick_extreme += 1/10

                all_data.append(
                    # Where we are at the third tick
                    (interp_wav(xltd_short_sequence[2]),
                    interp_grad(xltd_short_sequence[2]),
                    # where the extreme (after the third tick) is
                    interp_wav(tick_extreme),
                    tick_extreme - xltd_short_sequence[2]))

            if plot_intermediate:
                plt.vlines(xltd_short_sequence, ymin=-0.8, ymax=0.8, colors="red", ls="--")
                plt.vlines(short_sequence, ymin=-0.8, ymax=0.8, colors="lime", lw=1, ls="--")

            if plot_thumbnails:
                ax = plt.subplot(GROWS,GCOLS,ax_num)
                ax.set_ylim(-0.9,0.9)
                ax.plot(range(int(short_sequence[0])-20, int(short_sequence[-1])+50),
                         segment_wav[int(short_sequence[0])-20 : int(short_sequence[-1])+50], lw=1)
                ax.vlines(xltd_short_sequence, ymin=-0.8, ymax=0.8, colors="red", ls="--", lw=1)
                ax.vlines(short_sequence, ymin=-0.8, ymax=0.8, colors="lime", ls="dotted", lw=1)
                ax.set_yticklabels([])
                ax.set_xticklabels([])
                ax.grid()
                if ax_num == GROWS*GCOLS:
                    plt.gcf().suptitle(f"Thumbnails for experiment {experiment}/{len(all_beacons)}")
                    plt.show()
                    ax_num = 1
                else:
                    ax_num += 1

        if plot_thumbnails:
            if ax_num > 1:
                plt.gcf().suptitle(f"Thumbnails for experiment {experiment}/{len(all_beacons)}")
                plt.show()

        if len(all_data) > 0:
            a = np.array(all_data)
            np.save("accura_stats",a)

        if plot_intermediate:
            plt.show()

        if False and len(second_change) > 0:
            fig = plt.figure()
            ax = fig.add_subplot(projection='3d')
            records = np.array(second_change)
            ax.scatter(records[:,0],records[:,1],records[:,2],c="black")
            plt.show()

    a = np.array(all_data)
    np.savetxt('output.csv', a, delimiter=',', fmt='%f')
    """
    d <- read.csv("/home/stefan/Projects/accurapple/speaker/output.csv")
    d <- read.csv("C:\\Users\\StephaneC\\works\\accurapple\\speaker\\output.csv")
    library("rgl")
    plot3d(d[,1],d[,2],d[,3],xlab="s(t)",ylab="s'(t)", zlab="next extreme height", c="black")
    plot3d(d[,1],d[,2],d[,4],xlab="s(t)",ylab="s'(t)", c="red")
    legend3d("topright", legend=c("s(t_ext)","t_ext-t0"), pch=16, cex=1, col=c("black","red"))

    plot(d[,3],d[,4])
    """

    # a = np.array(first_change)
    # plt.scatter(a[:,0],a[:,1],marker="x")
    if False and len(all_data) > 0:
        a = np.array(all_data)
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')
        ax.set_title(f"Experiment {experiment}, {a.shape} data")
        ax.scatter(a[:,0],a[:,1],a[:,2],c="black")
        ax.scatter(a[:,0],a[:,1],a[:,3],c="red")
        ax.set_xlabel('s(t_0)')
        ax.set_ylabel("s'(t_0)")
        plt.show()


    if True:
        c = ["red", "blue", "green", "pink", "orange"]
        for after_drops in all_after_drops:
            for d in after_drops:
                for ref_ndx, ref in enumerate([-0.19,-0.26,-0.32,-0.36,-0.43]):
                    if abs(d[0] - ref) < 0.025:
                        plt.plot(d-ref, c=c[ref_ndx], alpha=0.1)

        plt.title("Signal (y-realigned) after drop, grouped by position at drop\n"+\
            "Note the end of signal depends on position at drop\n" + \
            "So postion after drop is a good predictor for wave shape")
        plt.show()

        """ 1. Build up size predicts drop size
            2. Drop size predicts position (of valley)
            3. position of valley predicts wave shape
            => Build up size predicts wave shape
            ?? => ?? time between tick predicts build up size ?
        """

    if True:
        """ Showing that the height of the speaker drop
        is somehow function of the height of the build up that
        precedes it.
        """
        ax = plt.subplot(1,2,1)
        # This predicts the dorp size rather well
        for p in all_drops:
            ax.plot(p,c="red",alpha=0.5,label="Speaker buildup size")
        for p in all_starts:
            ax.plot(p,c="blue",alpha=0.5,label="Speaker Y position at drop start")
        for p in all_p:
            ax.plot(p,c="black",alpha=0.5,label="Speaker drop size")

        # This doesn't prodct the drop size very well
        for p, q in zip(all_drops, all_starts):
            ax.plot(np.array(p)+np.array(q),c="orange",alpha=0.5,label="buildup+position")

        handles, labels = ax.get_legend_handles_labels()
        by_label = dict(zip(labels, handles))
        ax.legend(by_label.values(), by_label.keys())
        ax.set_xlabel("Time between first and second tick (no unit yet)")

        ax = plt.subplot(1,2,2)
        c = ["red", "blue", "green", "orange"]
        for p,v in zip(all_p, all_drops):
            for rng_ndx, rng in enumerate([(0,17),(17,30),(30,45),(45,60)]):
                rb,re = rng
                ax.scatter(p[rb:re],v[rb:re],c=c[rng_ndx],alpha=0.5,label="Speaker drop")
        ax.set_xlabel("Speaker drop size")
        ax.set_ylabel("Speaker build up size")
        plt.show()


if cli_args.average_segments:
    l = []
    nb_seg = all_beacons.nb_segments()
    for experiment in tqdm(range(nb_seg)):
        segment_wav, ticks_wav = all_beacons.get_data_segment(experiment,with_beacons=False)
        l.append(segment_wav.shape[0])
    ZOOM=6 # Bigger than that the ROI is super small. FIXME Maybe it should be == nseg ?
    min_l = min(l)*ZOOM

    segment_wav, _ = all_beacons.get_data_segment(0, with_beacons=False)

    segment_wav = np.interp(
        np.arange(segment_wav.shape[0]*ZOOM)/ZOOM,
        np.arange(segment_wav.shape[0]),
        segment_wav)

    shifted_wavs = np.zeros((segment_wav.shape[0], nb_seg))

    for experiment in tqdm(range(nb_seg)):
        wav, _ = all_beacons.get_data_segment(experiment, with_beacons=False)
        wav = np.interp(
            np.arange(wav.shape[0]*ZOOM)/ZOOM,
            np.arange(wav.shape[0]),
            wav)

        if wav.shape[0] > min_l:
            wav = wav[:min_l]
        elif wav.shape[0] < min_l:
            wav = np.hstack( [wav, np.zeros((l-wav.shape[0],))] )

        N=4*ZOOM
        subseg_mean = wav[N:-N]
        best_i, best_ac = 0, None
        for i in range(2*N):
            corr = np.sum(segment_wav[i:-2*N+i] * subseg_mean)
            #ac = np.sum(np.abs(segment_wav[i:-2*N+i] - subseg))
            if best_ac is None or corr > best_ac:
                best_i, best_ac = i, corr

        shifted_seg = np.hstack([ [0]*best_i, subseg_mean, [0]*(2*N-best_i) ])
        #averaged_wav += shifted_seg
        shifted_wavs[:,experiment] = shifted_seg

        #segment_wav += wav

    shifted_wavs = shifted_wavs[::ZOOM,:]
    shifted_wavs = np.delete(shifted_wavs, slice(3155000*ZOOM,None), axis=0)
    #shifted_wavs = np.delete(shifted_wavs, slice(None, 31490*ZOOM), axis=0)
    mean = np.mean(shifted_wavs,axis=1)
    print(np.mean(np.abs(shifted_wavs.transpose() - mean.transpose())))

    for i in range(nb_seg):
        plt.plot(shifted_wavs[:,i], lw="1", c="black",alpha=0.5)
    plt.plot(mean, c="red", lw="1", label="Average")
    std = np.std(shifted_wavs,axis=1)
    plt.plot(mean-std, c="red", lw="1", ls="--")
    plt.plot(mean+std, c="red", lw="1", ls="--")
    #plt.xlim(31490,31550)
    plt.legend()
    plt.show()


if cli_args.fit:
    assert crc_wav == 0x196e6a19, f"Right now, this has only been tested on the test7 dataset ({crc_wav})"
    nb_seg = int(cli_args.fit[0])
    assert nb_seg == 3, "Works only on segment 3"
    nb_seg -= 1 # 1 based
    segment_wav, ticks = all_beacons.get_data_segment(nb_seg, with_beacons=False)

    BREAK=120 # Start of low freq fit zone

    N = 20
    subseg_mean = np.zeros((1000,))
    for i in range(N):
        t = int(ticks[i*2])
        s = segment_wav[t:t+1000]
        subseg_mean += s
    subseg_mean /= N

    #subseg = np.convolve(subseg, np.ones(10)/10, mode="same")


    r = np.arange(0, len(subseg_mean))


    ax = plt.subplot(2,2,1)
    #ax.plot(r, mass_spring(r-np.min(r),*X0),c="blue")
    #plt.plot(r, mass_spring_MLE_fit(avg[r]+y_offset, X0)-0.05, c="green")
    BREAK_SIGMOID=60
    ydata = subseg_mean[BREAK:]
    xdata = np.arange(ydata.shape[0])
    popt, pcov = curve_fit(mass_spring, xdata, ydata)
    print(f"Low freq fit: {popt}")
    lowfreq_fit_y = mass_spring(np.arange(subseg_mean.shape[0])-BREAK,*popt)
    ax.plot(lowfreq_fit_y,
            label="Low freq Fit over speaker", c="lime")
    ax.plot(subseg_mean, c="black", lw=1, label="WAV")
    ax.axvline(BREAK, lw=1,ls="--",c="grey",label="Start of fitted zone")

    sig = 1/(1 + np.exp(-0.075*(np.arange(subseg_mean.shape[0])-BREAK_SIGMOID)))
    #sig = (sig - sig[0])/np.max(sig)
    ax.plot(sig*lowfreq_fit_y, c="blue", lw=1, label="Sigmoid*LowFreq fit")
    ax.plot(sig, c="blue", lw=1, ls="--", label="Sigmoid")

    ax.axhline(0,lw=1,ls="--",c="grey")
    ax.set_xlabel("44100Hz")
    ax.set_xlim(0,500)
    ax.legend()

    ax = plt.subplot(2,2,2)
    minus_low_lfreq = subseg_mean - sig*lowfreq_fit_y
    ax.plot(np.arange(subseg_mean.shape[0]),
            minus_low_lfreq,
            label="WAV minus sigmoid*Low freq",
            c="blue", lw=1)
    # Manual fit !!!
    # d*np.exp(-a*x)*np.sin(b*x+c)
    X0 = [0.06, 0.28, +1*3.1415, 1]
    print(f"High freq fit: {X0}")

    a_star, b_star, c_star, d_star = X0

    lambda_over_m = 2*a_star
    beta_over_m = ( 4*(b_star**2) + (2*a_star)**2) / 4
    delta = - (lambda_over_m)**2 + 4*beta_over_m
    v0 = sqrt(delta)*tan(c_star)/(4*a_star)
    l = 2*(v0+2*a_star) / sqrt(delta)


    print(f"delta={delta}")
    print(f"v0={v0}")
    print(f"{d_star} ? {l}")

    print(l)
    ax.plot(mass_spring( np.arange(subseg_mean.shape[0]), *X0), color="red", label="fit for high-freq")

    ax.set_xlim(0,300)
    ax.axvline(BREAK, lw=1,ls="--",c="grey",label="Start of low freq fitted zone")
    ax.axhline(0,lw=1,ls="--",c="grey")
    ax.legend()



    ax = plt.subplot(2,2,3)
    for i in range(N):
        t = int(ticks[i*2])
        ax.plot(s, c="black", alpha=0.1)
    ax.plot(subseg_mean, c="red", lw=1, label=f"Mean of {N} signals")
    ax.axhline(0,lw=1,ls="--",c="grey")
    ax.axvline(BREAK, lw=1,ls="--",c="grey",label="Start of fitted zone")
    ax.legend()
    complete_fit = (1-sig)*mass_spring( np.arange(subseg_mean.shape[0]), *X0) + \
        sig*mass_spring( np.arange(subseg_mean.shape[0]), *popt.tolist())
    ax.plot(complete_fit, label="copmplete fit")

    from scipy.fft import fft, fftfreq
    ax = plt.subplot(2,2,4)
    #signal = minus_low_lfreq[15:]
    signal = subseg_mean

    N = signal.shape[0]
    T = 1/samplerate
    yf = fft(signal)
    xf = fftfreq(N, T)[:N//2]
    ax.plot(xf, np.log(2.0/N * np.abs(yf[0:N//2])))
    ax.set_title("FFT of signal")

    plt.show()



if cli_args.ticks_analysis:
    segment_id = int(cli_args.ticks_analysis)
    segment, ticks = all_beacons.get_data_segment(segment_id-1, with_beacons=False, original_ticks=True)

    p = ticks[1:] - ticks[0:-1]

    plt.title(f"Segment {segment_id}, time between ticks, {ticks.shape[0]} ticks\n" +
              f"{np.mean(p)*2:.1f} c. period, {CYCLES_PER_SECOND/(np.mean(p)*2):.1f} Hz")
    #plt.vlines(ticks,ymin=0,ymax=1)
    plt.xlabel("Ticks index")
    plt.ylabel("Cycles")
    plt.plot(p)
    plt.show()
    exit()


exit()

wav_beacons_starts = np.array([b[0] for b in beacons])
ticks_beacons_starts = np.array([b[0] for b in accurapple_beacons])
corr = np.corrcoef(wav_beacons_starts, ticks_beacons_starts)



if False:
    # Add params for WAV (ticks are hardocded I guess, no depends on Apple speed)
    # OR a param for their ratio.
    # So I can have frequencies from accurapple ticks, wav and recordings by redmi
    # or other sources
    # --wav-freq accurapple/redmi/48000
    # --ticks-freq default
    # note that if we have beacons, it's much better to compute the ratio
    # and reuse it: --freq-ratio 21.25589895
    # Woring with ratio is smarter

    ticks = accura_ticks / K
    ax = plt.subplot(1,1,1)
    ax.plot(data)
    ax.vlines(ticks, ymin=-1,ymax=1,color="black",lw=1)
    plt.show()
    exit()

if True:
    # Faire la moyenne des écarts sur les
    # Retirer les sommets qui ne sont pas sur ces moyennes
    # Ajouter des sommets qui le sont.

    beac = beacons[0] # beacons from the wav
    aa_beac = accurapple_beacons[0] # beacons from the tick
    seg_ticks = [t-aa_beac[0] for t in accura_ticks if aa_beac[0] <= t and t <= aa_beac[1]]
    segment = data[beac[0]: beac[1]]

    tops_by_time = find_tops(segment, above=0.5, min_space=10)
    tops = sorted(zip(segment[tops_by_time], tops_by_time),reverse=True)[0:100]
    tops = list(sorted([t[1] for t in tops]))
    plt.plot(segment)
    plt.scatter(tops, segment[tops], c="green")

    N = 24
    d1 = tops[N] - tops[0]
    d2 = seg_ticks[N] - seg_ticks[0]
    cps = d2/d1

    for t in seg_ticks[0:N+1]:
        plt.axvline(tops_by_time[0] + t/cps, c="red", ls="--")

    plt.title(f"{beac}; {aa_beac}")
    plt.show()
    exit()



FIRST_TWO_TICKS=22 # 2 for the beacon, 20 regular ticks.
K=100_000
# K=2101_500
# K=4101_500
# K=3100_500
# K=3200_500
# K2 = 3000
# K2 = data.shape[0]
# data = data[0:K2]







class Segment:
    """ A segment is a beacon, followed by some data.

    Aligned means: the tops are aligned and set to the zero coordinate.
    """

    def __init__(self, wav: np.array, accura_ticks: np.array):
        self._wav = wav
        self._accura_ticks = accura_ticks

        self._match_beacons()


    @property
    def aligned_wav(self):
        return self._wav[self._beacon_tops[0]:]

    @property
    def aligned_ticks_as_samples(self):
        return (self._beacon_ticks - self._beacon_ticks[0]) / K

    @property
    def aligned_wav_tops_pos(self):
        return self._beacon_tops - self._beacon_tops[0]

    @property
    def wav_tops_values(self):
        return self._wav[self._beacon_tops]

    def _match_beacons(self):
        """ If wav and ticks both represent a beacon, match them.
        """

        wav_segment = self._wav

        # These tops are sorted by time.
        tops_pos = find_tops(wav_segment, above=np.max(wav_segment)/2, min_space=1)
        # These are sorted by value
        tops_by_value = sorted(zip(wav_segment[tops_pos], tops_pos),reverse=True)[0:TICKS_PER_BEACON]
        self._beacon_tops = np.array(list(sorted([t[1] for t in tops_by_value])))

        # By convention, the first tick is a top
        # We track the top ticks, not the bottom ones
        self._beacon_ticks = np.array(self._accura_ticks[0::2])





if cli_args.find_beacons_tops:
    for i in range(2):
        wav_beacon = beacons[i]
        wav_segment = data[wav_beacon[0]: wav_beacon[1]]
        aa_beacon = accurapple_beacons[i]
        aa_beacon_ticks = [t for t in accura_ticks if aa_beacon[0] <= t and t <= aa_beacon[1]]
        segment = Segment(wav_segment, aa_beacon_ticks)


        plt.title(f"{len(segment.aligned_wav_tops_pos)} tops ({TICKS_PER_BEACON} expected)")
        plt.plot(segment.aligned_wav)
        plt.scatter(segment.aligned_wav_tops_pos, segment.wav_tops_values,c="red")

        tas = segment.aligned_ticks_as_samples

        for i in range(TICKS_PER_BEACON):
            plt.plot( [segment.aligned_wav_tops_pos[i], tas[i]],
                      [segment.wav_tops_values[i], 1.0], ls="--", c="black", lw=1.0)
        plt.show()



K = 21.159039269541
COLOURS=["red","green","blue","orange","black"]
xlted_ticks = (accura_ticks[200:] - accura_ticks[200]) / K
for z in range(4):
    ax = plt.subplot(2,2,z+1)
    decal = z*2
    col = 0

    plt.suptitle("Whatever before the last tick, the freq. and phase after are always the same.")
    ax.set_title(f"Pause:10 to 0, Pause2:{10-decal}")

    for i in range(10*decal+0,10*(decal+1),2):
        # sidenote: one needs about 1000 samples (22000 cycles)
        # before the voltage *starts* to decrease back to zero after a tick.
        ticks = xlted_ticks[3*i:3*(i+1)]
        d = round(ticks[1] - ticks[0])

        ofs = round(ticks[0] + 690345)

        dex = data[ofs-60:ofs+280]

        tops = find_extremes(dex,threshold=0.1,min_space=1)
        if tops.shape[0] == 0:
            continue
        print(tops)
        ax.scatter(tops-d-60, dex[tops],c=COLOURS[col],marker="+")

        ax.plot(range(-d-60,-d+280), dex, c=COLOURS[col],
            label=f"Pause {10-i%10}")

        for j in range(3):
            ax.axvline(ticks[j] - ticks[0] - d - 6 , c=COLOURS[col], ls="--")

        ax.set_xlim((-60,+200))
        ax.set_ylim((-0.8,+0.8))
        ax.axhline(0, ls="--")
        ax.legend()
        col += 1
plt.show()


for i in range(0,10):
    # Will the previous tick (i length) influence the successor
    # (length = 10) ?
    # i        i+1     i+2
    # | i*t... | 10... |
    base_tick = 200+3*i
    ticks = accura_ticks[base_tick:base_tick+3] - accura_ticks[base_tick+1]
    ofs = round((accura_ticks[base_tick+1]-accura_ticks[200])/K)+690345
    plt.plot(data[ofs:ofs+115])
    for t in ticks:
        plt.axvline(t/K , c="green", ls="--")
plt.show()

plt.plot(data)
for b,e in accurapple_beacons:
    plt.axvline(b/21.159039269541 + beacons[0][0] + 125, c="red")
for t in accura_ticks:
    plt.axvline(t/21.159039269541 + beacons[0][0] + 125, c="green", ls="--")
plt.show()


# plt.plot(accura_wav)
# plt.show()

toi = []
for t in accura_ticks:
    if 1258782 < t and t < 1260000:
        print(t)
        plt.axvline(7.2+8+1.5+(t-1258782)/CYCLES_PER_SAMPLE, c="red", ls="--")
        plt.axvline(22.5+0.8+(t-1258782)/CYCLES_PER_SAMPLE, c="green", ls="--")
        plt.axvline(22.5+0.8+17-6.5+(t-1258782)/CYCLES_PER_SAMPLE, c="blue", ls="--")
plt.plot(data[2397500:2397800])
plt.show()
exit()

ax = plt.subplot(2,1,1)
segment = data[beacons[0][0]:beacons[0][1]]
tops = find_tops(segment, above=0.3, min_space=100)
ax.plot(segment)
ax.scatter(tops, segment[tops],c="red")
ax = plt.subplot(2,1,2)
ax.plot(accura_wav[accurapple_beacons[0][0]:accurapple_beacons[0][1]])
plt.show()
exit()


AVERAGE_BEACON_LENGTH = int(np.min([e-b for b,e in beacons]))
BEACONS_SEGMENTS = [(beacons[i][0], beacons[i+1][0]) for i in range(len(beacons)-1)]

all_data = load_segments_slices(beacons)


exit()

accura_ticks, accura_wav = load_accurapple_speaker("zulu.bin")



if False:
    slices = all_data.get_slices(22)
    for b, e in slices:
        plt.plot(data[b:e],alpha=0.5,c="black")
    plt.show()

if True:
    # Show Apple signal and accurapple "ticks"
    for b,e in all_data._segments[8].values():
        plt.plot(range(b,e), data[b:e])

    last_slice_begin, last_slice_end = list(all_data._segments[8].values())[-1]
    last_peak = last_slice_begin + np.argmax(data[last_slice_begin:last_slice_end])
    plt.axvline(last_peak, c="black", lw=1, ls="--")#, y=0.05*np.ones_like(h).astype(float))

    b,e = all_data.get_pos_segment(8)
    # plt.plot(data[b:e])
    base = 12740*CYCLES_PER_SAMPLE
    accura_ticks= accura_ticks[(accura_ticks > base) & (accura_ticks < 743420*CYCLES_PER_SAMPLE)]
    accura_ticks -= accura_ticks[0]

    last_tick = accura_ticks[-1]
    ratio = (last_peak - b)/last_tick
    accura_ticks = accura_ticks * ratio
    plt.title(f"Ticks/samples: measured: {1/ratio:.4f} (expected:{CYCLES_PER_SAMPLE:.4f})")

    h=accura_ticks[0::2] # odd
    for x in h: #/CYCLES_PER_SAMPLE:
        plt.axvline(x+b, c="red", lw=1, ls="--")#, y=0.05*np.ones_like(h).astype(float))
    h=accura_ticks[1::2] # even
    for x in h: #/CYCLES_PER_SAMPLE:
        plt.axvline(x+b, c="orange", lw=1, ls="--")#, y=0.05*np.ones_like(h).astype(float))
    #plt.scatter(x=h/(1/samplerate*CYCLES_PER_SECOND)-ofs, y=-0.05*np.ones_like(h).astype(float))
    plt.show()


if False:
    # Show the beacons signals
    deltas = []
    P = ceil(sqrt(1+len(beacons)))
    for i in range(len(beacons)):
        b,e = beacons[i]
        s = data[b:e]
        q = int(np.sum(np.abs(s)))

        ax = plt.subplot(P,P,i+1)
        ax.plot(s, linewidth=0.5, label=f"{q}")
        if i >= 1:
            prev_b, prev_e = beacons[i-1]
            delta = b-prev_b
            deltas.append(delta)
            ax.set_title(f"{delta}")
        ax.legend()
        if i > 0:
            ax.tick_params(top=False, bottom=False, left=False, right=False,
                            labelleft=False, labelbottom=False)

    ax = plt.subplot(P,P,i+1+1)
    ax.plot(deltas)
    plt.suptitle(f"{len(beacons)} beacons")
    plt.show()

if False:
    # Show some segments. We skip the first one because they're
    # not clean (surrounding noise and sound)
    for i in range(5,20):
        plt.plot(data[beacons[i]:beacons[i+1]], linewidth=0.1, alpha=0.3, c="black")
    plt.show()

if False:
    def average_slice(ax, beacons, offset):
        all_slices = []
        W = 150
        for i in range(len(beacons)-1):
            b, e = beacons[i]
            slice = data[b+offset:b+offset+W]
            ndx = np.argmax(slice)
            slice = slice[ndx:ndx+W]
            all_slices.append(slice)
            ax.plot(slice, linewidth=0.3, alpha=0.5, c="black")
        all_slices = np.vstack(all_slices)
        avg = np.average(all_slices,axis=0)
        ax.plot(avg,linewidth=1,c="red")
        ax.set_ylim([-0.5, 0.5])
        ax.axvline(np.argmin(avg))
        ax.axvline(np.argmax(avg))
        ax.axhline(0, c="black", linewidth=0.5)

    for ndx, offset in enumerate([262_000, 722_000, 733_000, 738_000]):
        ax = plt.subplot(2,2,ndx+1)
        average_slice(ax, beacons, offset)
    plt.show()


mean_slice_len = round(np.mean([beacons[i+1][0] - beacons[i][0] for i in range(len(beacons)-1)]))

slices = []
for i in range(len(beacons)-1):
    b = beacons[i][0]
    slices.append(data[b:b+mean_slice_len])

averaged_data = np.mean(np.vstack(slices), axis=0)

b, e = beacons[5][0]+AVERAGE_BEACON_LENGTH,beacons[6][0]
s = data[b:e]
s = averaged_data

filtered_ndx= []
for ndx in np.where(s > 0.3)[0]:
    if not filtered_ndx:
        filtered_ndx.append(ndx)
    elif abs(filtered_ndx[-1] - ndx) > 1000:
        filtered_ndx.append(ndx)
    elif s[filtered_ndx[-1]] < s[ndx]:
        s[filtered_ndx[-1]] = s[ndx]



if False:
    # Comparing the signal right after a tick.
    beacon = beacons[2]
    segment = data[beacon[0]:beacon[0]+AVERAGE_BEACON_LENGTH]
    extremes = find_tops(segment)

    # Indices of extreme values
    np_extremes = np.array(extremes, dtype=int)
    tops_ndx = np.argwhere(segment[np_extremes] > 0.16)
    bottoms_ndx = np.argwhere(segment[np_extremes] < -0.16)

    # Last top
    last_bottom_ndx = bottoms_ndx[-1]

    # Where the go-to bottom starts
    x1 = np_extremes[last_bottom_ndx-1][0]

    # Reaching the bottom, the spring now pulls everything up
    x2 = np_extremes[last_bottom_ndx][0]

    # Reaching the top, the spring now pushes everything down
    x3 = np_extremes[last_bottom_ndx+1][0]

    print([x1,x2,x3])
    print(segment[[x1,x2,x3]])
    print(f"{segment[x1]-segment[x2]} - {segment[x2] - segment[x3]}")


    tops = list(filter( lambda ndx: segment[ndx] > 0.16, extremes))
    bottoms = list(filter( lambda ndx: segment[ndx] < -0.16, extremes))
    tops = tops[len(tops)-4:]
    bottoms = bottoms[len(bottoms)-4:]
    plt.plot(segment,c="grey",lw=1)
    for i in (1,3):
        r = range(tops[i],tops[i]+91)
        plt.plot(r, segment[r])

    back_in_time=106
    fit = mass_spring_MLE_fit(segment[2141:],X0=[1.0,1.0,1.0,1.0],back_in_time=back_in_time)
    r = range(2141-back_in_time,len(segment))
    plt.plot( r, fit)
    plt.plot( r, segment[r]-fit)
    # plt.scatter(tops, segment[tops], marker="x", c="red")
    # plt.scatter(bottoms, segment[bottoms], marker="x", c="red")
    #plt.scatter(np_extremes, segment[np_extremes], marker="x", c="red")
    plt.scatter([x1,x2,x3], segment[[x1,x2,x3]], marker="x", c="red")
    plt.plot([x1,x2],segment[[x1,x2]], c="black")
    plt.plot([x2,x3],segment[[x2,x3]], c="black")
    plt.xlim((tops[0],3500))
    plt.axhline(0,c="black",lw=1,ls="--")
    plt.show()
    exit()


if False:
    plt.plot(s)
    plt.scatter(filtered_ndx, s[filtered_ndx], c="orange")
    plt.show()

if False:
    # Analyse the rising edge of each pulse in each beacon.
    all_slices= []
    for begin, end in tqdm(BEACONS_SEGMENTS):
        slice = data[(begin):(begin + AVERAGE_BEACON_LENGTH)]
        sclip = slice.copy()
        sclip[sclip < np.max(sclip)*0.6] = 0
        splits = find_tops(sclip)
        for s in splits[len(splits)-3:]:
            subslice = slice[s-40:min(s+250,len(slice))]
            plt.plot(subslice,c="black",alpha=0.01)
            all_slices.append(subslice)

    avg = np.average( np.vstack(all_slices), axis=0)
    plt.axhline(-0.1)
    plt.axhline(0.1)
    plt.plot(avg, c="yellow", ls="--")

    #r=np.arange(40,120)
    #plt.plot(r,mass_spring(r-40,0.06,0.32,1.1,0.4)-0.05,c="red")

    r=np.arange(49,87)
    X0 = [0.04,0.30,-3.1415/4, 0.20]
    y_offset = 0.075
    plt.plot(r, mass_spring(r-np.min(r),*X0)-y_offset,c="blue")
    plt.plot(r, mass_spring_MLE_fit(avg[r]+y_offset, X0)-0.05, c="green")
    plt.show()

if False:
    cmap = plt.get_cmap("jet")
    logger.info(len(filtered_ndx))
    N=20
    for plt_ndx, bg in enumerate([0,50,99-N]):
        ax = plt.subplot(2,2,plt_ndx+1)
        for i in range(0,N,3):
            ndx = FIRST_TWO_TICKS+bg+i
            b,e = filtered_ndx[ndx]-4, filtered_ndx[ndx+1]
            ln = min(e-b,750)
            musec = (100-(bg+i))*25 # Assembler code's pause.
            extract = s[b:b+ln]

            if bg == 0 and i == 0:
                logger.info("Saving extract")
                soundfile.write('/tmp/extract.wav', extract, samplerate)

            #extract = np.convolve(extract, [1]*5)
            #extract = np.cumsum(extract)
            ax.plot(extract , c=cmap(i/N), linewidth=1, label=f"Δ≈{musec}µs ({musec/(1_000_000/samplerate):.1f} S)")
        ax.set_xlabel(f"Sample (S=1/{samplerate} s)")
        ax.set_ylabel("Amplitude")
        for i in [-0.25,0,0.25]:
            ax.axhline(i, linewidth=1, linestyle="--")
        ax.legend(loc='lower right')
    plt.suptitle("Two speaker ticks separated by Δµs")
    plt.show()


if False:
    from scipy.fft import rfft, rfftfreq

    ndx = 20
    b,e = filtered_ndx[ndx]-4, filtered_ndx[ndx+1]
    ln = min(e-b,750)
    extract = s[b:b+ln]

    # Note the extra 'r' at the front
    yf = rfft(extract/np.max(extract))
    xf = rfftfreq(extract.shape[0], 1 / samplerate)

    ax = plt.subplot(1,2,1)
    ax.plot(extract)
    ax = plt.subplot(1,2,2)
    ax.plot(xf, np.abs(yf))
    plt.show()

s = averaged_data
ndx = 20
b,e = filtered_ndx[ndx]-4, filtered_ndx[ndx+1]
ln = min(e-b,750)
extract = s[b:b+ln]


if False:
    # Trying to fit various parts of a single tick

    # xdata = np.arange(extract.shape[0])
    # ydata = extract
    BREAK=196

    # avg

    base_ydata = None
    N = 14
    N_SLICE = 8
    for i in range(2,2+N):
        if base_ydata is None:
            base_ydata = all_data.get(4,N_SLICE)
        else:
            base_ydata += all_data.get(4,N_SLICE)
    base_ydata = base_ydata * 1/N

    #base_ydata = all_data.get(4,4)
    plt.plot(np.arange(base_ydata.shape[0]-BREAK), base_ydata[:-BREAK], label="Speaker")


    # Fit the end of data (region of about 3.6KHz)
    ydata = base_ydata[BREAK:]
    xdata = np.arange(ydata.shape[0])
    popt, pcov = curve_fit(mass_spring, xdata, ydata)
    lowfreq_fit_y = mass_spring(xdata-BREAK,*popt)
    plt.plot(xdata, lowfreq_fit_y,label="Fit over speaker")

    BREAK2 = 133 #142 #124 # 132
    speaker_minus_fit = (base_ydata[:-BREAK] - lowfreq_fit_y)[BREAK2:235]
    plt.plot(np.arange(BREAK2,235), speaker_minus_fit, label="Speaker - fit")

    #plt.plot(base_ydata[:-BREAK] - lowfreq_fit_y, label="Speaker - fit -part2")

    xdata = np.arange(speaker_minus_fit.shape[0])
    popt2, pcov2 = curve_fit(mass_spring, xdata, speaker_minus_fit)
    fit_y = mass_spring(xdata,*popt2)
    plt.plot(xdata+BREAK2,fit_y,label="Fit over speaker - fit")

    #plt.plot(xdata+BREAK2,fit_y,label="Fit2 over speaker - fit")

    plt.legend()
    plt.show()

    xdata = np.arange(-65,1100)
    xdata2 = np.arange(0,1000)
    ymodel = mass_spring(xdata,*popt)
    ymodel2 = mass_spring(xdata2,*popt2)

    yall = np.zeros((2500,))
    yall[196-65:196-65+ymodel.shape[0]] += ymodel
    yall[133:133+ymodel2.shape[0]] += ymodel2
    plt.plot(yall)
    plt.plot(base_ydata)
    plt.show()



if False:
    # :25,  [0.003,0.055,-0,0.15]
    ndx = 10
    b,e = filtered_ndx[ndx]-4, filtered_ndx[ndx+1]
    ln = min(e-b,750)
    extract = s[b:b+ln]
    if ndx >= FIRST_TWO_TICKS:
        musec = (100-(ndx-FIRST_TWO_TICKS))*25 # Assembler code's pause.
    else:
        musec = "/"

    ydata = extract[5:]
    xdata = np.arange(ydata.shape[0])
    distr = norm(loc=0,scale=0.25) # The location (loc) keyword specifies the mean. The scale (scale) keyword specifies the standard deviation.
    X0 = [0.003,0.055,1.5+3.14/4,0.15]
    lik_model = minimize(mass_spring_likelihood, x0=X0, bounds=[(0,1), (0,1), (-3.1415,3.1415), (0,1)], args=(xdata,ydata,distr), method='L-BFGS-B')
    print(lik_model)
    plt.scatter(xdata,ydata, s=0.5, c="red", label="Data")
    plt.plot(xdata,ydata, c="red", lw=0.5)

    """
    # I measure the x-distance between the mins/maxs of the curve,
    # in the beginning of the curve. This plot shows that they
    # are more or less separated by the same distnace.

    d <- c(11.2, 20, 31.3, 42, 48.3, 57.1, 61.5, 70.9, 75.3, 84.7)
    linreg <- lm(d ~ seq(length(d)))
    plot(d)
    abline( linreg$coefficients)
    linreg$coefficients
    """

    #plt.plot(xdata, func(xdata,*lik_model.x), label="exp()*sin() fit")
    #plt.plot(xdata, func(xdata, *X0), linewidth=1, label="Manual fit")
    #plt.plot(xdata, func(xdata, 0.05,0.34,1,0.35))
    plt.plot(xdata, mass_spring(xdata, 0.02, 0.38, 1, 0.25)+mass_spring(xdata,*lik_model.x), label="Model")

    der = np.gradient(ydata)
    second_der = []
    for i in range(der.shape[0]-1):
        if der[i]*der[i+1] < 0:
            second_der.append(i)
    x = np.array(second_der)
    print(x[1:] - x[0:-1])
    plt.scatter(second_der, ydata[second_der])
    plt.title(f"Δ={musec}µs")
    plt.xlim((0,100))
    plt.legend()
    plt.show()
