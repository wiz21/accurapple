import pdb
import os
import subprocess
from datetime import datetime
from pathlib import Path
import numpy as np
from scipy.signal import kaiserord, lfilter, firwin, freqz, resample, butter, lfilter, freqz
import scipy.io.wavfile as wavfile
from scipy.io.wavfile import write, read
import matplotlib.pyplot as plt
from tqdm import tqdm
from math import floor, pi, sin, sqrt, log, ceil, exp, sin, cos
import subprocess
import numba
import soundfile
#import librosa
from numba import jit
from utils import load_accurapple_ticks, normalize_wav_and_save, fast_oscillator, CPU_FREQ, equalizer

SRC_FREQ=50*20280
TARGET_FREQ=44100
CYCLES_PER_SAMPLE=SRC_FREQ/TARGET_FREQ

def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w

def get_first_nz(vec):
    """Get the first nonzero element position of a vector

    :param vec: the vector
    :type vec: iterable
    """
    if not isinstance(vec, np.ndarray) or vec.dtype != 'bool':
        vec = np.array(vec) > 0
    return np.argmax(vec)

def low_pass_fft_filter(data, band_limit, sampling_rate):
    cutoff_index = int(band_limit * data.size / sampling_rate)
    print("FFT")
    F = np.fft.rfft(data)
    F[cutoff_index + 1:] = 0
    print("inv FFT")
    return np.fft.irfft(F, n=data.size).real


def kaiser_config(cutoff, sample_rate):
    print("kaiser_config")
    # The Nyquist rate of the signal.
    nyq_rate = sample_rate / 2.0

    # The desired width of the transition from pass to stop,
    # relative to the Nyquist rate.  We'll design the filter
    # with a 5 Hz transition width.
    width = 100.0/nyq_rate # was 10

    # The desired attenuation in the stop band, in dB.
    ripple_db = 50.0 # Bigger values create shadows

    # Compute the order and Kaiser parameter for the FIR filter.
    N, beta = kaiserord(ripple_db, width)

    # The cutoff frequency of the filter.
    cutoff_hz = cutoff

    # Use firwin with a Kaiser window to create a lowpass FIR filter.
    taps = firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))

    print(taps.shape)
    return taps

def kaiser(signal, cutoff, sample_rate, order):
    return lfilter( kaiser_config(cutoff, sample_rate), [1.0], signal)

    # The following code replicates the lfilter(...) function
    # It's here as an example of the future rust implementation
    # f = kaiser_config(cutoff, sample_rate)
    # print(f)
    # r = np.zeros(len(signal))
    # for i in range(len(signal) - len(f)):
    #     r[i] = np.sum(signal[i:i+len(f)] * f)
    # return r

def brute_filter(array):
    array=array[::SRC_FREQ//TARGET_FREQ]
    return array

def fft_filter(array):
    array = low_pass_fft_filter(array,TARGET_FREQ,1000000)
    array = array[::SRC_FREQ//TARGET_FREQ]
    return array

def kaiser_filter(array):
    # Low pass filtering
    array = kaiser(array, TARGET_FREQ, SRC_FREQ, 5)

    # Downsampling
    array = array[::SRC_FREQ//TARGET_FREQ]
    return array

def scipy_filter(array):
    # FIXME Doesn't work !
    number_of_samples = round(len(array) * float(TARGET_FREQ) / SRC_FREQ)
    array = resample(array[0:1_000_000], number_of_samples)
    return array

def non_zero_runs(a):
    # From https://stackoverflow.com/questions/24885092/finding-the-consecutive-zeros-in-a-numpy-array
    # Create an array that is 1 where a is 0, and pad each end with an extra 0.
    iszero = np.concatenate(([0], np.not_equal(a, 0).view(np.int8), [0]))
    absdiff = np.abs(np.diff(iszero))
    # Zero-runs start and end where absdiff is 1.
    ranges = np.where(absdiff == 1)[0].reshape(-1, 2)
    return ranges

def zero_runs(a):
    # From https://stackoverflow.com/questions/24885092/finding-the-consecutive-zeros-in-a-numpy-array

    # Create an array that is 1 where a is 0, and pad each end with an extra 0.
    iszero = np.concatenate(([0], np.equal(a, 0).view(np.int8), [0]))
    absdiff = np.abs(np.diff(iszero))
    # Runs start and end where absdiff is 1.
    ranges = np.where(absdiff == 1)[0].reshape(-1, 2)
    return ranges

def convolution_product(outer, inner):
    """
    To understand this code, imagine a table.

    Imagine the inner convolution (applied first)
    is 3 elements long, [i,j,k].
    The outer convolution (applied second) is [a,b,c].

                    inner
            ---------------------
    o   a * ! i ! j ! k ! 0 ! 0 !
    u       ---------------------
    t   b * ! 0 ! i ! j ! k ! 0 !
    e       ---------------------
    r   c * ! 0 ! 0 ! i ! j ! k !
            ---------------------

    then we sum the rows...
    """


    t = np.zeros( (len(outer), len(inner) + len(outer)) )

    for i in range(len(outer)):
        t[i, i:i+len(inner)] = inner
        t[i,:] = t[i,:] * outer[i]

    r = np.sum(t, axis=0)
    assert r.shape == (len(inner) + len(outer),), f"{r.shape}"
    return r

def normalize_wav_and_save(wav, outfile_path):
    wav = wav - np.mean(wav)
    wav = wav / np.max(np.abs(wav))
    soundfile.write(outfile_path, wav, TARGET_FREQ, "PCM_16")
    print(f"Saved {outfile_path}")
    return wav


def filter_downsample(accura_record_path, outfile_path, alpha=1.0):

    accura_ticks, cpu_wav = load_accurapple_speaker(accura_record_path, ticks_alpha=alpha)

    N = cpu_wav.shape[0]
    sampling_points = np.linspace(0,N-1, int(N/(SRC_FREQ/TARGET_FREQ))).astype(dtype="int")
    downsampled= cpu_wav[sampling_points]

    wav = np.array(downsampled)
    soundfile.write(outfile_path, wav, TARGET_FREQ, "PCM_16")
    return wav


def filter_averaging_downsample(accura_record_path, outfile_path, alpha=1.0):
    print("filter_averaging_downsample")
    accura_ticks, cpu_wav = load_accurapple_speaker(accura_record_path, zero_based=False, as_ticks=True, ticks_alpha=alpha)

    N = cpu_wav.shape[0]
    ds_N = int(N/(SRC_FREQ/TARGET_FREQ))
    sampling_points = np.linspace(0,N-1, ds_N).astype(dtype="int")
    print(f"{N} {ds_N} {N/ds_N:.2f}")
    res = []
    for i in range(len(sampling_points)-1):
        res.append(
            np.mean(
                cpu_wav[sampling_points[i]:sampling_points[i+1]]))

    if args.plot:
        fig, axs = plt.subplots(2, 2)
        axs[0,0].plot(cpu_wav[:20_000])
        axs[1,0].step(np.arange(len(accura_ticks)-1), accura_ticks[1:]-accura_ticks[:-1])
        axs[0,1].step(np.arange(len(res)), res)
        plt.show()

    wav = np.array(res)
    soundfile.write(outfile_path, wav, TARGET_FREQ, "PCM_16")
    return accura_ticks, wav


def filter_44b(accura_record_path, outfile_path):
    accura_ticks, array = load_accurapple_speaker(accura_record_path)

    print(array.shape)
    # The initial signal is 1Mhz and we'll downsample to 1Mhz/SUB.
    # So we can safely remove some bits. I tested with my ears
    # and decimating by a factor 4 makes no difference.
    # If we think about it the fastest way one can trigger
    # the speak on the apple is with repeatedly doing LDA $SPEAKER
    # This is 3 cycles. At that point, we're still a 300KHz !
    # So cutting by a factor of 4 is not that much.
    # Actually, the fastest way is ASL $SPEAKER which is a sequence of
    # 3 cycles, 1 cycle Read, 1 cycle, 1 cycle Write.
    # FIXME there also phantom writes I think...
    # So at some point there are 2 speaker triggers 2 cycles apart.
    # But on average, it remain about 3 cycles between clicks.

    SUB=4
    array = array[::SUB]

    # We compute a FIR filter. It's a low pass filter.
    # Once the filter will have been run, we'll perform
    # a decimation to keep only the data we need.
    # Lire ceci: http://digitalsoundandmusic.com/7-3-1-convolution-and-time-domain-filtering/
    # et l'article qui suit, ça explique tout avec des algos!

    # Cutoff. In practice, it sounds better if we go that low.
    # I guess that it's because the speaker can't render
    # much better than that.

    f_cutoff = TARGET_FREQ
    f_samp = SRC_FREQ/SUB

    # Total size of the convolution array. There may be an optimal
    # way to compute this. This one was done with my ear.
    N=32

    f_cutoff = f_cutoff/f_samp
    w_c = 2*pi*f_cutoff
    middle = N//2    # Integer division, dropping remainder*/

    fir_filter = np.zeros(N+1)
    for i in range(-N//2,N//2+1):
        if i == 0:
            fir_filter[middle] = 2*f_cutoff
        else:
            fir_filter[i + middle] = sin(w_c*i)/(pi*i)

    # We apply a first averaging filter (before the FIR lowpass)
    # There's no physical justification, but it sounds better to me.
    # Moreover, it seems that it smooths the curves and this improves
    # the way the low pass filter responds. On Archon, removing the
    # prefilter induces a lot of noise.

    # Archon sounds MUCH better with the prefilter; DarkLord sounds better with 32
    # Ghostbusters seems ok with 32, 16
    # array = np.convolve(array, np.ones(16,dtype="float"))
    # array = np.convolve(array,  h, 'valid')
    prefilter = np.ones(3+64//SUB,dtype="float")

    # Trying to simulate reverb... Very naive attempt
    # prefilter1 = np.hstack( [np.sin(np.linspace(0,pi,60//SUB))*0.2,
    #                          np.zeros(177//SUB),
    #                          np.array([1])] )
    # prefilter2 = np.hstack( [np.sin(np.linspace(0,pi,80//SUB))*0.1,
    #                          np.zeros(277//SUB),
    #                          np.array([1])] )
    # prefilter3 = np.hstack( [np.sin(np.linspace(0,pi,100//SUB))*0.05,
    #                          np.zeros(377//SUB),
    #                          np.array([1])] )
    # prefilters = [prefilter1, prefilter2, prefilter3]
    # mw = max([f.shape[0] for f in prefilters])
    # for i in range(len(prefilters)):
    #     f = prefilters[i]
    #     if mw > f.shape[0]:
    #         prefilters[i] = np.pad(f, pad_width=[(mw - f.shape[0],0)])
    # prefilter = sum(prefilters) / len(prefilters) # sum all prefilters (yes it works :-) )

    # Reverb again. This time I say that a sound far in the past muse
    # be heard lightly whereas a sound that just passed must be heard
    # bigger.

    ls = np.linspace(1,4,267//SUB)
    prefilter = ls*ls*ls
    prefilter = prefilter - np.min(prefilter)
    print(prefilter)
    # Notice that I merge the two filters in a single one. This
    # reduces the number of multiplications in the convolution significantly.
    double_filter = convolution_product(fir_filter, prefilter)

    # Once the filter has been applied, I decimate the output
    # to keep only the frequencies I need.

    # I use linspace because of numerical issues with np.arange
    # (see numpy's doc)

    decimation = np.round(
        np.linspace(0, len(array)-1,
                    int(len(array) / ((SRC_FREQ/SUB) / TARGET_FREQ)))).astype(dtype="int")


    # We'll then apply another filter afterwards, on the decimated data.
    # Again, there's no physical justification, but it sounds better to me.
    #post_decimation_filter = np.array([0.5,1,2,1,0.5]) # Good for archon
    post_decimation_filter = np.array([1,1,1]) # Good for Bourree and Archon

    # Once all filtering is done (in float) I move back to uint8
    # for proper WAV output. This is done by a simple offset+scale
    # but the question is: what values for the scale and offset ?
    # So here I compute the extrema of my filter.
    # These computations rely on the fact that the last convolution
    # is made of positive terms only.

    # WARNING! These are extremum and there are not often met in real life.
    cmax = np.sum(double_filter[double_filter > 0]) * np.sum(post_decimation_filter[post_decimation_filter > 0])
    cmin = np.sum(double_filter[double_filter < 0]) * np.sum(post_decimation_filter[post_decimation_filter > 0])

    # Now that we have all building blocks, we run them all

    start = datetime.now()

    array = np.convolve(array, double_filter)
    array = array[decimation]
    #array = np.convolve(array, post_decimation_filter)
    array = ((array - cmin) * (1/ (cmax - cmin))).astype(dtype="float")

    z = datetime.now()-start
    total_time = z.seconds + z.microseconds/1000000
    print(f"{100*total_time / (array.shape[0] / 1_000_000):.1f}% of the time needed")

    soundfile.write(outfile_path, array, 48000, "PCM_16")
    return array


def filter_44(array):
    # This filter is specific to Archon...

    # Check this out : https://www.youtube.com/watch?v=LcxwNH2Yp3A
    # le channel parle aussi de convolution !

    # Lire ceci: http://digitalsoundandmusic.com/7-3-1-convolution-and-time-domain-filtering/
    # et l'article qui suit, ça explique tout avec des algos!

    iszero = np.concatenate(([0], np.not_equal(array, 0).view(np.int8), [0]))
    absdiff = np.abs(np.diff(iszero))
    # Zero-runs start and end where absdiff is 1.
    positions = np.where(absdiff == 1)[0]

    ranges = non_zero_runs(array)
    ups = ranges[:,1] - ranges[:,0]
    down_ranges = zero_runs(array)
    downs = down_ranges[:,1] - down_ranges[:,0]

    print("Ups --------------------------------------------------")
    print(np.unique(ups, return_counts=True))
    print("Downs ------------------------------------------------")
    print(np.unique(downs, return_counts=True))

    f = TARGET_FREQ / SRC_FREQ
    s = np.zeros(round(len(array) * f)+1, dtype="float")
    for i in tqdm(range(ranges.shape[0])):
        start, end = ranges[i]
        # Archon has 2 kind of pulses: 8 cycles or 15 cycles.
        v = (end - start) / 4

        # I do sums to conserve energy (does this even make sense ?)
        p = start*f
        r = floor(p)

        if p*f == r:
            s[r] += v
        else:
            d = p - r
            s[r] += v*(1-d)
            s[r+1] += v*d

    #cv = [0.5,2,0.5]
    cv = [0.2,0.5,1.5,0.5,0.2]

    array = np.convolve(s, cv, 'valid') / sum(cv)

    return array


    #print(positions)


def spring_filter(array):
    print("Spring filter")
    res = []
    buf = []
    p = 0
    v = 0
    i = 0
    old_s = None
    s = 1
    sig = +1
    for s in tqdm(array[0:30*1_000_000]):
        # s is 0 or 1

        if old_s != s: # tick !
            old_s = s
            sig = -sig
            p = sig
        else:
            # Stop en 40ms : 0.8
            # 0.5: horrible
            # 0.8: super aigu mais pas de click
            # 0.9: click, porteuse audible, son très bof
            # 0.95: clicks mais son intéressant
            # 0.995: click de partout

            p = p*0.5

        buf.append(p)
        #print(p)

    array = np.array(buf, dtype=float)
    plt.plot(array[2_010_000:2_011_000])
    plt.show()

    SUB=2
    array = array[::SUB]


    # We compute a FIR filter. It's a low pass filter.
    # Once the filter will have been run, we'll perform
    # a decimation to keep only the data we need.
    # Lire ceci: http://digitalsoundandmusic.com/7-3-1-convolution-and-time-domain-filtering/
    # et l'article qui suit, ça explique tout avec des algos!

    # Cutoff. In practice, it sounds better if we go that low.
    # I guess that it's because the speaker can't render
    # much better than that.

    f_c = TARGET_FREQ / 4
    f_samp = SRC_FREQ/SUB
    N=256

    f_c = f_c/f_samp
    w_c = 2*pi*f_c
    middle = N//2    # Integer division, dropping remainder*/

    h = np.zeros(N+1)
    for i in range(-N//2,N//2+1):
        if i == 0:
            h[middle] = 2*f_c
        else:
            h[i + middle] = sin(w_c*i)/(pi*i)

    decimation = np.round(
        np.linspace(0, len(array)-1,
                    int(len(array) / ((SRC_FREQ/SUB) / TARGET_FREQ)))).astype(dtype="int")

    array = np.convolve(array, h)
    array = array[decimation]

    return scale_to_wav(array)




def spring_filter2(array_org):
    print("Spring filter 2")
    array_org = array_org[int(1.5*1_000_000):]
    res = []
    buf = []
    p = 0
    v = 0
    i = 0
    old_s = None
    s = 1
    acc = 0
    sig = +1
    K = 30000
    dec = K
    sqrtK = log(K)

    for s in tqdm(array_org[0:30_000_000]):
        # s is 0 or 1

        if old_s != s: # tick !
            old_s = s
            sig = -sig
            acc = sig*2
            v = 0
            dec = K
        dec = max(1,dec-1)

        v = v + acc
        p = p + v

        if p > 1:
            p = 1
            v = 0
            acc = 0
        elif p < -1:
            p = -1
            v = 0
            acc = 0


        buf.append(p* log(dec)/sqrtK)
        #print(p)

    array = np.array(buf, dtype=float)
    # Adding noise is useless
    #array = array + np.random.uniform(size=len(array))*0.05
    s,e=2.414, 2.415
    rng = range(int(s*SRC_FREQ), int(e*SRC_FREQ))
    plt.plot(rng, array_org[rng])
    plt.plot(rng, array[rng])
    ls = np.linspace(s,e,20)*SRC_FREQ
    plt.xticks(ls, np.linspace(s,e,20))
    plt.show()

    # The CPU can't load $C030 faster than 1 time every 3 cycles.
    SUB=3
    array = array[::SUB]


    # We compute a FIR filter. It's a low pass filter.
    # Once the filter will have been run, we'll perform
    # a decimation to keep only the data we need.
    # Lire ceci: http://digitalsoundandmusic.com/7-3-1-convolution-and-time-domain-filtering/
    # et l'article qui suit, ça explique tout avec des algos!

    # Cutoff. In practice, it sounds better if we go that low.
    # I guess that it's because the speaker can't render
    # much better than that.

    f_c = 3000*4 # Response of the speaker is 3KHz I need to multiply that else it sound awful
    f_samp = SRC_FREQ/SUB # Original sample frequency, already divided by SUB
    N=4096

    f_c = f_c/f_samp
    w_c = 2*pi*f_c
    middle = N//2    # Integer division, dropping remainder*/

    h = np.zeros(N+1)
    for i in range(-N//2,N//2+1):
        if i == 0:
            h[middle] = 2*f_c
        else:
            h[i + middle] = sin(w_c*i)/(pi*i)

    decimation = np.round(
        np.linspace(0, len(array)-1,
                    int(len(array) / ((SRC_FREQ//SUB) / TARGET_FREQ)))).astype(dtype="int")

    array = np.convolve(array, np.flip(h))
    array = array[decimation]


    return scale_to_wav(array)


def load_accurapple_speaker(filepath, zero_based=False, as_ticks=False, ticks_alpha=1.0):
    # Returns (ticks, wav)

    with open(filepath,"rb") as fin:
        data = fin.read()
        # >u8 == big endian, 8 bytes unsigned
        # integer (64 bits)
        accura_ticks = np.frombuffer(data, dtype=">u8")

    # Synthetic test:
    #accura_ticks = np.arange(0,1_000_000,2_000)

    accura_ticks = accura_ticks - np.min(accura_ticks)
    if args.short is not None:
        accura_ticks = accura_ticks[accura_ticks < int(args.short)]

    # Alpha allows to time stretch the ticks
    accura_ticks = (accura_ticks.astype(dtype=float) *  ticks_alpha).astype(int)


    if as_ticks:
        wav = np.zeros( shape=(int(np.max(accura_ticks))+1,), dtype=float)
        wav[accura_ticks] = 1.0
    elif not zero_based:
        # Builds a signal which oscillates between +1 and -1
        wav = np.ones( shape=(int(np.max(accura_ticks))+1,), dtype=float)
        for i in range(0, (len(accura_ticks) // 2) * 2, 2):
            b,e = accura_ticks[i:i+2]
            wav[b:e] = -1.0
    else:
        # Builds a signal which oscillates between 0 and +1
        wav = np.zeros( shape=(int(np.max(accura_ticks))+1,), dtype=float)
        for i in range(0, (len(accura_ticks) // 2) * 2, 2):
            b,e = accura_ticks[i:i+2]
            wav[b:e] = 1.0

    print(f"Total duration {len(wav)/SRC_FREQ:.1f}s")
    return accura_ticks, np.array(wav)

def scale_to_wav_float32(array):
    nmin = np.min(array)
    nmax = np.max(array)
    return (array - nmin)/(nmax-nmin) - 0.5



def scale_to_wav(array):
    amin = np.min(array)
    amax = np.max(array)
    print(amin, amax)
    return (16+224*(array - amin) / (amax - amin)).astype('uint8')


@jit(nopython=True)
def resample_helper(a, mode):
    """ a: samples at SRC_FREQ
    mode = 1 == integration, 2 == decimation
    """
    samples = []
    l = a.shape[0]
    intervals = np.linspace(0,l,int(l/(SRC_FREQ/TARGET_FREQ)))

    i_ndx = 1
    s_ndx = 0
    while i_ndx < intervals.shape[0]-10:
        beg_interv = intervals[i_ndx]
        end_interv = intervals[i_ndx+1]

        if mode == 1:
            # Integration
            sample = a[s_ndx] * (1 - (beg_interv - int(beg_interv)))
            s_ndx += 1
            while s_ndx < end_interv:
                sample += a[s_ndx]
                s_ndx += 1
            sample += a[s_ndx] * (end_interv - int(end_interv))
        else:
            # Decimation
            sample = a[int(round(beg_interv + end_interv)/2)]

        samples.append(sample)

        i_ndx += 1

    return np.array(samples)

from time import perf_counter
class catchtime:
    def __enter__(self):
        self.time = perf_counter()
        return self

    def __exit__(self, type, value, traceback):
        self.time = perf_counter() - self.time
        self.readout = f'Time: {self.time:.3f} seconds'
        print(self.readout)

def resample_rough(a, mode = 1):
    with catchtime() as t:
        return resample_helper(a, mode)



class Speaker:
    """Simulates the response of the Apple II speaker."""

    # TODO: move lookahead.evolve into Speaker method

    def __init__(self, sample_rate: float, freq: float, damping: float,
                 scale: float):
        """Initialize the Speaker object
        :arg sample_rate The sample rate of the simulated speaker (Hz)
        :arg freq The resonant frequency of the speaker
        :arg damping The exponential decay factor of the speaker response
        :arg scale Scale factor to normalize speaker position to desired range (cpu freq)
        """
        self.sample_rate = sample_rate
        self.freq = freq
        self.damping = damping
        self.scale = np.float64(scale)  # TODO: analytic expression

        # See _Signal Processing in C_, C. Reid, T. Passin
        # https://archive.org/details/signalprocessing0000reid/
        dt = np.float64(1 / sample_rate)
        w = np.float64(freq * 2 * np.pi * dt)

        # Equation 3.17 in the book (page 37)
        d = damping * dt
        e = np.exp(d)
        c1 = 2 * e * np.cos(w)
        c2 = e * e

        # Square wave impulse response parameters
        b2 = 0.0
        b1 = 1.0

        self.c1 = c1
        self.c2 = c2
        self.b1 = b1
        self.b2 = b2

    def tick(self, y1, y2, x1, x2):
        """ y1, y2: speaker position
        x1,x2: applied voltage
        """
        y = self.c1 * y1 - self.c2 * y2 + self.b1 * x1 + self.b2 * x2
        return y



def kennaway(file_path, outfile_path):
    inv_scale = 22400 * 0.07759626164027278
    sample_rate = SRC_FREQ
    speaker = Speaker(sample_rate, freq=3875, damping=-1210, scale=1 / inv_scale)

    accura_ticks, snd_array = load_accurapple_speaker(file_path)
    wav = []

    y1 = y2 = v1 = v2 = 0
    for i in tqdm(range(snd_array.shape[0])):
        y = speaker.tick(y1, y2, v1, v2)
        v2 = v1
        v1 = snd_array[i]
        y2 = y1
        y1 = y
        snd_array[i] = y

    if args.plot:
        plt.title("Kena")
        plt.plot(snd_array[10000:30000])
        plt.show()

    if args.save1mhz:
        fn = file_path + '_kena_onemhz.npy'
        print(f"Saving to {fn} as a numpy array")
        np.save(fn, snd_array)

        fn = file_path + '_kena_onemhz.bin'
        print(f"Saving to {fn} as a bunch of float32's")
        snd_array.astype(np.float32).tofile(fn)

    # normalize
    snd_array = snd_array - np.mean(snd_array)
    snd_array = snd_array / np.max(np.abs(snd_array))


    print(f"Downsampling start : {datetime.now()}")

    downsampled_output = librosa.resample(
            y=snd_array,
            orig_sr=SRC_FREQ,
            target_sr=TARGET_FREQ)

    print(f"Downsampling done : {datetime.now()}")

    downsampled_output = downsampled_output - np.mean(downsampled_output)
    downsampled_output = downsampled_output / np.max(np.abs(downsampled_output))

    soundfile.write(outfile_path, downsampled_output, TARGET_FREQ, "PCM_16")

def kegs(file_path, outfile_path):
    K = 21.159039269541 # CPU freq / target frq (48KHz)

    measured_freq = 48000

    K = K / TARGET_FREQ *  measured_freq

    ticks, snd_array = load_accurapple_speaker(file_path)
    ticks = ticks / K
    wav = np.zeros( (int(np.max(ticks))+20,), dtype=float)
    sign = +1
    sample_ndx = 0
    t_ndx = 0
    #pdb.set_trace()
    with tqdm(total=ticks.shape[0]) as pbar:
        while t_ndx < ticks.shape[0]:
            #print(f"ticks[t_ndx]={ticks[t_ndx]}")
            assert sample_ndx  < wav.shape[0]
            pbar.update(1)

            # sample_ndx is in SAMPLE_RATE.
            # ticks have been brought back to SAMPLE_RATE
            while sample_ndx < int(ticks[t_ndx]):
                wav[sample_ndx] = sign
                sample_ndx += 1

            #print(f"-- sample_ndx={sample_ndx}/{wav.shape[0]}, ticks[t_ndx]={ticks[t_ndx]}")
            h1 = (ticks[t_ndx] - sample_ndx)*sign
            t_ndx += 1
            sign = -sign
            assert abs(h1)<1, f"h ??? {h1} {sample_ndx}, {ticks[t_ndx]}"
            #print(h1)

            while t_ndx < ticks.shape[0] and ticks[t_ndx] < sample_ndx+1:
                #print(f"sample_ndx={sample_ndx} t_ndx={t_ndx}, ticks[t_ndx]={ticks[t_ndx]}")
                h1 += (ticks[t_ndx] - ticks[t_ndx-1]) * sign
                #print(f"  {h1}")
                t_ndx += 1
                sign = -sign

            assert abs(h1) < 1, f"h ??? {h1}"
            h1 += (sample_ndx+1 - ticks[t_ndx-1] )*sign
            wav[sample_ndx] = h1

            sample_ndx += 1


    # Add enough silence at the beginning so that snd_fit2.py
    # can build a noise profile
    wav = np.hstack( [np.zeros((4*TARGET_FREQ,)), wav, np.zeros((TARGET_FREQ*2,)) ] )
    soundfile.write(outfile_path, scale_to_wav_float32(wav), TARGET_FREQ, "PCM_16")



def apply_on_ticks_at_44khz(ticks, func):
    ticks = ticks / SRC_FREQ * TARGET_FREQ
    dist_between_ticks = ticks[1:] - ticks[:-1]

    current_tick_ndx = 0
    current_44sample = 0

    fill = 0
    if fill + dist_between_ticks[current_tick_ndx] < 1.0:
        pass



@jit(nopython=True)
def kena2_helper(ticks):
    m = 1
    # lam = 1
    # beta = 0.7

    # If lambda is big, then exp(-lam*...) is small
    lam=0.002353
    #beta=0.2288*0.015
    freq = 2*pi*1/(SRC_FREQ/3875) # Hertz
    beta = (lam**2 + 4*(freq**2))/4
    K = 21.159

    delta = -( (lam/m)**2 - 4*beta/m )
    assert delta >= 0
    sqrt_delta_over_2 = sqrt(delta)/2
    print(delta)
    print(1/sqrt_delta_over_2)

    def oscillator(x,h,v0,V):
        a = (h - V)/2

        b = -(v0 + a*lam/m)/sqrt(delta)

        r= 2*np.exp(-lam/(2*m)*x) * (a*np.cos(sqrt_delta_over_2*x) - b*np.sin(sqrt_delta_over_2*x)) + V
        return r

        # a_star, b_star, c_star, d_star = [0.06, 0.48, +0.25*3.1415, 0.8]
        # lam_over_m = 2*a_star
        # beta_over_m = (4*(b_star**2)-(2*a_star)**2)/4

        # return d_star * exp(-0.5*lam_over_m*x) * sin( b_star*x + c_star)

    def oscillator_dydx(x,h,v0,V):
        a = (h - V)/2
        b = -(v0 + a*lam/m)/sqrt(delta)

        lm2 = -lam/(2*m)
        e = np.exp(lm2*x)
        c = np.cos(sqrt_delta_over_2*x)*(a*lm2 - b*sqrt_delta_over_2)
        s = np.sin(sqrt_delta_over_2*x)*(a*sqrt_delta_over_2 + b*lm2)
        return 2*e*(c-s)


    voltage = 1
    h, v0 = 0,0
    current_tick_ndx = 0
    sound = []

    while current_tick_ndx < ticks.shape[0]-1:
        #sound.append(h)

        x_start = ticks[current_tick_ndx]
        x_end = ticks[current_tick_ndx+1]

        V = voltage # FIXME Bugg !remove that !
        dx = 0
        while x_start + dx < x_end:
            sound.append( oscillator(dx,h,v0,V) )
            dx += 1

        old_v0, old_h = v0, h
        h = oscillator(dx,old_h,old_v0,V)
        v0 = oscillator_dydx(dx,old_h,old_v0,V)

        voltage = 1 - voltage

        space = x_end - x_start
        if voltage == 1:
            # Values were measured in 44KHz samples
            if space < 1000*K:
                V = 1
            elif 1000*K <= space < 3000*K:
                print("In the voltage drop zone")
                V = 1 - (space - 1000*K)/((3000-1000)*K)
            else:
                V = 0
        else:
            V = 0

        current_tick_ndx += 1

    return np.array(sound)

def integral_helper(x):
    m = 1
    lam=0.002353
    freq = 2*pi*1/(SRC_FREQ/3875) # Hertz
    beta = (4*(freq**2) + lam**2)/4
    delta = -( (lam/m)**2 - 4*beta/m )

    a = 2
    b = lam/(2*m)
    c = sqrt(delta)/2
    d = (h - V)/2
    e = -(v0 + d*lam/m)/sqrt(np.delta)
    return a*((b*d + c*e)*np.cos(c*x) + (-b*e + c*d)*np.sin(c*x))*np.exp(b*x)/(b**2 + c**2)

def integral(file_path, outfile_path):
    ticks, _ = load_accurapple_speaker(file_path)

    # FIXME the "int" will not be exact
    sample_limits = np.linspace( ticks[0], ticks[-1], int(SRC_FREQ/TARGET_FREQ))

    # Merge the two (sorted) arrays. This is not as crazy as it seems, see
    # SO : https://stackoverflow.com/questions/59263172/numpy-how-to-merge-two-sorted-arrays-into-a-larger-sorted-array
    c = np.concatenate((ticks,sample_limits))
    c.sort(kind="stable") # Will sort in place

    interval_durations = c[1:] - c[:-1]

    ih = integral_helper(c)
    integrals = ih[1:] - ih[:-1]

    cndx = 0
    for sndx in range(1, sample_limits.shape[0]):
        start = cndx
        while c[cndx] < sample_limits[sndx]:
            cndx += 1
        samples.append(integrals[start:cndx] * interval_durations[start:cndx])

    soundfile.write(outfile_path, np.array(samples), TARGET_FREQ, "PCM_16")


def kena2(file_path, outfile_path, alpha=1):

    ticks, _ = load_accurapple_speaker(file_path,ticks_alpha=alpha)

    if args.plot:
        print("plotting")
        plt.title("Kena2")
        plt.vlines(x=ticks, ymin=0, ymax=1)
        plt.show()

    with catchtime() as _:
        sound = kena2_helper(ticks)

    # print("Filtering")
    # import scipy.signal as sig
    # # Order, freq
    # sos = sig.butter(180, 10000, fs=SRC_FREQ, btype='lowpass', analog=False, output='sos')
    # sound = sig.sosfilt(sos, sound)

    if args.plot:
        print("plotting")
        plt.title("Kena2")
        plt.plot(sound[10000:30000])
        plt.show()
    if args.save1mhz:
        fn = file_path + '_kena2_onemhz.npy'
        print(f"Saving to {fn} as a numpy array")
        np.save(fn, sound)
        fn = file_path + '_kena2_onemhz.bin'
        print(f"Saving to {fn} as a bunch of float32's")
        sound.astype(np.float32).tofile(fn)

    def butter_lowpass(cutoff, fs, order=5):
        return butter(order, cutoff, fs=fs, btype='low', analog=False)

    def butter_lowpass_filter(data, cutoff, fs, order=5):
        b, a = butter_lowpass(cutoff, fs, order=order)
        y = lfilter(b, a, data)
        return y

    print(f"Downsampling {SRC_FREQ}Hz -> {TARGET_FREQ}Hz start : {datetime.now()}")
    # downsampled_output = librosa.resample(
    #     y=sound,
    #     res_type='kaiser_best',
    #     orig_sr=SRC_FREQ,
    #     target_sr=TARGET_FREQ)

    lowpass_filtered_sound = butter_lowpass_filter(sound, TARGET_FREQ, SRC_FREQ)
    downsampled_output = resample_rough(sound)

    print(f"Downsampling done : {datetime.now()}")


    # print("Equalizer")
    # _, eq = wavfile.read("filter.wav")
    # print(eq.shape)
    # downsampled_output = np.convolve(downsampled_output*0.5, eq)

    print("Normalizing")
    downsampled_output = downsampled_output - np.mean(downsampled_output)
    downsampled_output = downsampled_output / np.max(np.abs(downsampled_output))

    downsampled_output = np.hstack( [np.ones((4*TARGET_FREQ,))*downsampled_output[0],
                                     downsampled_output,
                                     np.ones((TARGET_FREQ*2,))*downsampled_output[-1] ] )
    soundfile.write(outfile_path, downsampled_output, TARGET_FREQ, "PCM_16")

    return downsampled_output


def wav2ogg(fpath):
    cmd = [
        r"C:\Users\StephaneC\ffmpeg-2023-01-04-git-4a80db5fc2-full_build\bin\ffmpeg.exe",
        "-y", # overwrite
        "-i",
        f"{fpath}",
        f"{fpath.replace('.wav','.ogg')}"
        ]
    print(cmd)
    subprocess.run(cmd, stdout=subprocess.DEVNULL)

def voltage(tick_ndx, cycles_since_last_tick):
    K = SRC_FREQ/TARGET_FREQ

    if tick_ndx % 2 == 0:
        return 0
    else:
        # Values were measured in 44KHz samples => I mul by K
        if cycles_since_last_tick < 1000*K:
            return 1
        elif 1000*K <= cycles_since_last_tick < 3000*K:
            return 1 - (cycles_since_last_tick - 1000*K)/((3000-1000)*K)
        else:
            return 0


class TickGroup:
    def __init__(self, lo_lim, hi_lim):
        self.lo_lim, self.hi_lim = lo_lim, hi_lim
        self._ticks = []
        self._len = self.hi_lim - self.lo_lim

    def add(self, tick, h):
        self._ticks.append((tick,h))

    def mean_sample(self):
        s = 0

        i = 0
        while i < len(self._ticks):
            tick, h = self._ticks[i]
            if i < len(self._ticks) - 1:
                next_tick, _ = self._ticks[i+1]
            else:
                next_tick = self._hi_lim

            weight = (next_tick - tick)/self._len
            s += weight * h

        return s


class Oscillator:
    """
    Note: Caching the cos/sin/exp based on x is not a good
    idea. It only works with hi-freq sound engines (in which
    case the cache is small < 10000 entries). Else, the
    cache can get very big (a few 100_000's entries).
    """
    def __init__(self):
        self.m = 1
        # If lambda is big, then exp(-lam*...) is small
        self.lam=0.002353
        freq = 2*pi*1/(SRC_FREQ/3675) # Hertz
        beta = (4*(freq**2) + self.lam**2)/4
        #K = 21.159

        self.delta = -( (self.lam/self.m)**2 - 4*beta/self.m)
        assert self.delta >= 0, "We want an underdamped oscillator"
        self.sqrt_delta_over_2 = sqrt(self.delta)/2

    def compute_a_b(self,h,v0,V):
        a = (h - V)/2
        b = -(v0 + a*self.lam/self.m)/sqrt(self.delta)
        return a,b

    def oscillator(self, x,h,v0,V):
        a,b = self.compute_a_b(h,v0,V)

        e = np.exp(-self.lam/(2*self.m)*x)
        c = np.cos(self.sqrt_delta_over_2*x)
        s = np.sin(self.sqrt_delta_over_2*x)

        r= 2*e* (a * c - b*s) + V
        return r

    def oscillator_dydx(self, x,h,v0,V):
        a,b = self.compute_a_b(h,v0,V)
        lm2 = -self.lam/(2*self.m)

        e = np.exp(lm2*x)
        c = np.cos(self.sqrt_delta_over_2*x)*(a*lm2 - b*self.sqrt_delta_over_2)
        s = np.sin(self.sqrt_delta_over_2*x)*(a*self.sqrt_delta_over_2 + b*lm2)
        return 2*e*(c-s)



class Sampler:
    def __init__(self):
        self.ic_h = 0
        self.ic_v0 = 0
        self.last_compute_point = 0 # Expressed in ticks
        self.V = voltage(0,0)
        self.last_tick = 0
        self.tick_ndx = 0
        self._subs = []
        self.bound_ndx = 0
        self.oscillator = Oscillator()
        self.samples = []


    def current_bounds(self):
        b = round(self.bound_ndx * CYCLES_PER_SAMPLE)
        e = round((self.bound_ndx+1) * CYCLES_PER_SAMPLE)
        return b,e

    def next_bounds(self):
        self.bound_ndx += 1
        return self.current_bounds()

    def add_tick(self, tick, recur=False):
        b,e = self.current_bounds()
        last_tick_ndx = self.tick_ndx-1+1
        V = voltage(last_tick_ndx, cycles_since_last_tick=tick - self.last_tick)

        if b <= tick < e:
            # Tick is inside current sample (= 1/44100 of a second)

            # Compute h based on last ticks
            dx = tick - self.last_tick
            h = self.oscillator.oscillator(dx,self.ic_h,self.ic_v0,V)

            # We're inside the sample so we produce
            # only a part of the sample
            weight_in_subs = min(tick-b, tick-self.last_tick)
            self._subs.append( (h, weight_in_subs) )

            # Establish the new wave parameters based on the
            # current tick
            new_ic_h = h
            new_ic_v0 = self.oscillator.oscillator_dydx(dx,self.ic_h,self.ic_v0,V)

            self.ic_h, self.ic_v0 = new_ic_h, new_ic_v0

            self.tick_ndx += 1
            self.last_tick = tick
        else:
            # Tick is past the current sample
            # Complete current sample
            dx = e - self.last_tick
            h = self.oscillator.oscillator(dx,self.ic_h,self.ic_v0,V)
            weight_in_subs = dx
            self._subs.append((h,weight_in_subs))

            sample = 0
            for h,w in self._subs:
                sample += h*w
            self._subs.clear()
            self.samples.append(sample/CYCLES_PER_SAMPLE)


            # Do "full samples"
            nb, ne = self.next_bounds()
            assert tick - e >= 0
            for i in range(int((tick - e) / CYCLES_PER_SAMPLE)):
                dx += CYCLES_PER_SAMPLE
                h = self.oscillator.oscillator(dx,self.ic_h,self.ic_v0,V)
                self.samples.append(h)
                nb, ne = self.next_bounds()

            if tick == ne:
                # Protects agains a rounding error
                dx += CYCLES_PER_SAMPLE
                h = self.oscillator.oscillator(dx,self.ic_h,self.ic_v0,V)
                self.samples.append(h)
                nb, ne = self.next_bounds()

            assert nb <= tick  < ne, f"{nb} ({nb/CYCLES_PER_SAMPLE}) <= {tick}  < {ne}; K={CYCLES_PER_SAMPLE}"
            # Start new sample
            assert recur == False
            self.add_tick(tick, recur=True)



def kena3(file_path, outfile_path):
    ticks, _ = load_accurapple_speaker(file_path)
    assert ticks[0] == 0

    sampler = Sampler()

    for t in tqdm(ticks):
        sampler.add_tick(t)

    wav = np.array(sampler.samples)
    wav = wav - np.mean(wav)
    wav = wav / np.max(np.abs(wav))

    wav = np.hstack( [np.ones((4*TARGET_FREQ,))*wav[0],
                      wav,
                      np.ones((TARGET_FREQ*2,))*wav[-1] ] )

    soundfile.write(outfile_path, wav, TARGET_FREQ, "PCM_16")

def kena4(file_path, outfile_path):
    # python snd.py --file ticks/pop.ticks --filter kena4

    if Path(outfile_path).exists():
        return

    from scipy import signal

    ticks = load_accurapple_ticks(file_path)
    ticks = ticks - ticks[0]
    #ticks = ticks[:ticks.shape[0]//3]
    print("Generating")

    #y = fast_oscillator(ticks, nb_loops=24, attenuation=0.1, engine=2) # Best so far:
    # bad     y = fast_oscillator(ticks, nb_loops=124, attenuation=0.1, engine=2)
    #y = fast_oscillator(ticks, sho_freq=3100, nb_loops=12, attenuation=0.1, engine=2)
    y = fast_oscillator(ticks, sho_freq=3870, nb_loops=12, attenuation=0.01, engine=2)

    w = equalizer(y, np.fromfile("filter.f32"),TARGET_FREQ)

    if args.plot:
        fig, axs = plt.subplots(1, 1)
        axs.plot(y[1_000_000:1_050_000])
        plt.show()

    # >30 sec leads to crash on my PC (and even under one needs a lot of memory)
    max_size = min(int(45e6), y.shape[0])
    print(f"Resampling {max_size} cycles")

    #w = signal.resample(y[0:max_size], int(max_size/CPU_FREQ*TARGET_FREQ))
    #w = np.convolve([0.125,0.5,0.6,0.5,0.125],w)
    print(w.shape)
    normalize_wav_and_save(w, outfile_path)


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--file', nargs="+", default='sound.bin', help="File to process")
parser.add_argument('--filter', default='kegs', help="Type of filter")
parser.add_argument('--no-play', action='store_true', default=False,
                    help="Disable sound play at the end of the conversion (usefull if you don't have oggenc and mpv tools installed).")
parser.add_argument('--plot', action='store_true', default=False)
parser.add_argument('--short', default=None)
parser.add_argument('--save1mhz', action='store_true', default=False)
parser.add_argument('--noise', help="Frequency in hertz")

args = parser.parse_args()

files_to_process = args.file

if args.noise:
    print("Generating noise")
    target_freq = int(args.noise)

    # Random uniform distribution
    # noise = np.round(np.random.random(target_freq*2))

    # Linear congruential
    N = target_freq * 2
    v = 123456789
    r = []
    zero = [0] * 60
    for i in tqdm(range(N // len(zero))):
        v = v * 1664525 + 1
        v = v & 0xFFFFFFFF
        r.append(v >> 31)
        r.extend(zero)

    noise = np.array(r, dtype=float)
    soundfile.write("noise.wav", noise, target_freq, "PCM_16")
    exit()

if len(args.file) == 1 and os.path.isdir(args.file[0]):
    import glob
    files_to_process = glob.glob(args.file[0] + os.sep + "*.ticks")

if "," in args.filter:
    filters = args.filter.split(",")
elif args.filter == "all":
    filters = ["kegs","kena2"]
else:
    filters = [args.filter]

Q=ceil(sqrt(len(files_to_process)))

sp = None
for fndx, fpath in enumerate(files_to_process):
    for f in [f.strip() for f in filters]:
        print(f"Processing {fpath} with filter '{f}' at {datetime.now()}")

        if ".bin" in fpath:
            file_out = fpath.replace(".bin",f"_{f}.wav")
        else:
            file_out = fpath.replace(".ticks",f"_{f}.wav")

        if f == "kegs":
            kegs(fpath, file_out)
        elif f == "kena":
            kennaway(fpath, file_out)
        elif f == "kena2":
            kena2(fpath, file_out)
        elif f == "kena3":
            kena3(fpath, file_out)
        elif f == "kena4":
            kena4(fpath, file_out)
        elif f == "44b":
            filter_44b(fpath, file_out)
        elif f == "downsample":
            filter_downsample(fpath, file_out)
        elif f == "average":
            filter_averaging_downsample(fpath, file_out)
        elif f == "impulse":
            print("Loading Convolution")
            filter2, samplerate = soundfile.read("IR_apple2e.wav")

            cycles_per_sample = SRC_FREQ/samplerate
            filter_len_in_cycles =int(round(filter2.shape[0] * cycles_per_sample))
            interp_filter = np.interp( np.linspace(0, filter2.shape[0], filter_len_in_cycles),
                                       range(filter2.shape[0]),
                                       filter2)
            print(f"Convolution size is {interp_filter.shape[0]}")

            print("Loading ticks")
            ticks, wav_ticks = load_accurapple_speaker("recordings/archon.ticks", as_ticks=True)
            print(wav_ticks.shape)
            print("Applying Convolution")
            c = np.convolve(wav_ticks, np.flip(interp_filter),  "valid")
            c = scale_to_wav_float32(c)

            print("Resampling")
            downsampled_output = librosa.resample(
                y=c,
                orig_sr=SRC_FREQ,
                target_sr=TARGET_FREQ)

            file_out = "out.wav"
            print(f"Saving {file_out}")
            soundfile.write(file_out, downsampled_output, TARGET_FREQ, "PCM_16")
            exit()


        elif f == "test":
            # plt.plot(interp_filter)
            # plt.plot(filter2)
            # plt.show()

            ticks = load_accurapple_speaker("recordings/archon.ticks", as_ticks=True)

            #convo = np.flip(filter2)
            #W = filter2.shape[0]


            alpha_ticks = 36356/36413 # 28117/28166
            ticks, speaker = filter_averaging_downsample("./speaker/recordings/archon.ticks", file_out, alpha=alpha_ticks)
            ticks = ticks / SRC_FREQ * TARGET_FREQ
            synthesis = kena2("./speaker/recordings/archon.ticks", file_out, alpha=alpha_ticks)
            audio, samplerate = soundfile.read("./speaker/recordings/archon_webcam_denoised.wav")
            audio = audio[1605:]
            W = 2*120
            N_EQ = 2*80000

            # speaker = - filter_downsample("./speaker/recordings/test7/test7.ticks", file_out)
            # synthesis = kena2("./speaker/recordings/test7/test7.ticks", file_out)
            # audio, samplerate = soundfile.read("./speaker/recordings/test7/test7_webcam.wav")
            # audio = audio[524084:524084+100000]
            # W = 90
            # N_EQ = W*100

            plt.vlines(ticks[ticks < N_EQ], ymin=-1,ymax=1)
            plt.plot(speaker[:N_EQ],  label="original speaker") # [10000-W:11000-W]
            plt.plot(audio[:N_EQ],label="real") # [10000:11000]
            plt.plot(synthesis[:N_EQ],label="synthesis") # [10000:11000]
            plt.legend()
            plt.show()

            print("Building equations")
            coef = []
            rhs = []
            for i in tqdm(range(N_EQ)):
                ndx = int(i*1)
                coef.append( speaker[ndx:ndx+W] )
                rhs.append(audio[ndx+W])
            coef = np.vstack(coef)
            rhs = np.array(rhs).reshape((N_EQ,))

            print("solving")
            res = np.linalg.lstsq(coef, rhs)
            convo = res[0]
            convo = convo / np.max(np.abs(convo))



            plt.title(f"Speaker's Impulse Response\n{N_EQ} equations")
            cflip = np.flip(convo)
            plt.plot(np.arange(-np.argmax(cflip), -np.argmax(cflip) + cflip.shape[0]),
                     cflip, label="Convolution")
            filter2 = filter2 / np.max(np.abs(filter2))
            plt.plot(np.arange(-np.argmax(filter2), -np.argmax(filter2) + filter2.shape[0]),
                     filter2, label="Measured")
            plt.legend()
            plt.show()

            # Don't froget np.conv flips the conv array first !
            # So I have to undo it first !
            c = np.convolve(speaker, np.flip(convo),  "valid")
            c = scale_to_wav_float32(c)
            plt.plot(c[0:N_EQ],  label="conv") # [10000-W:11000-W]
            plt.plot(audio[0+W:N_EQ+W],label="real") # [10000:11000]
            plt.legend()
            plt.show()

            print(f"Saving {file_out}")
            soundfile.write(file_out, c, TARGET_FREQ, "PCM_16")


        else:
            raise Exception(f"Unrecognized filter {args.filter}")
        #wav2ogg(tgt)

        dpath = file_out.replace(".wav", ".ogg")
        print("Converting to OGG")
        os.system(f"oggenc -q 9 -o {dpath} {file_out}")
        print(datetime.now())

        if not args.no_play:
            if len(files_to_process) == 1:
                #os.system(f"mpv {file_out}")
                #sp = subprocess.Popen(["mpv","--really-quiet",file_out])
                #sp = subprocess.Popen([r"C:\Program Files\VideoLAN\VLC\vlc.exe", "--intf", file_out])
                from playsound import playsound
                print("Playing sound")
                playsound(file_out)

if sp is not None:
    sp.wait()



if False:
    nb_microsecs = len(array)
    #array = array[get_first_nz(array):]
    array = array.astype(float) / 255

    print(f"Filtering, {nb_microsecs} µs")
    #array = spring_filter2(array)
    #array = spring_filter(array)
    array = filter_44b(array)
    #array = filter_44(array)
    # array = kaiser_filter(array)
    #array = scale_to_wav(array)


    # print("Writing 'resampled.wav'")
    # write('resampled.wav', TARGET_FREQ, array)
