import subprocess
import os
import shlex
import shutil
import re

"""
Other games with sound
----------------------

rpg/2400 AD
rpg/ali baba
/mnt/data2/roms/Apple/asi_games/rpg/dragon_wars/dragon_war_a.dsk
action/still need to do all subdirectories
action/bruce_lee
action/indiana jones
action/gremlins
action/goonies
action/ladytut
action/pipe dream
action/superboulderdash II
action/quasar (some sounds)
action/rad_warrior
action/realm of impossibility
action/renegade
action/run for it
action/star destroyer
action/short circuit
action/The movie monster game
action/acid trip
action/beach landing (nice intro picture)
action/beyond pinball
action/carnival (buzzer only)
action/chrono warrior (music + digi voice)
action/cross city
action/death race
action/dr dracupig
action/evolution (buzzer)
action/force7 (take the good) music in game
action/gauntlet
action/gemstone warrior (music at start)
action/ghostbuster
action/goonies

hadron (no zike) <-- stopped here

simulation/la crackdown
???/marble madness
sports/championship wrestnling
miss pacman
rpg/2400AD
rpg/BardTale III
rpg/BardTale II
adventure/lucifer realm
"""

FILES = [ # 7,22,34
    ("/mnt/data2/roms/Apple/asi_games/action/ThunderChopper.dsk","TURBO_START, WAIT_UNTIL 4, KEYS A, WAIT_UNTIL 21, SPEAKER_REC_START thunderchopper.ticks, WAIT_UNTIL 49, SPEAKER_REC_STOP, SCREENSHOT thunderchopper.png, QUIT" ,"thunderchopper"),
    #("/mnt/data2/roms/Apple/asi_games/action/Alien-Clonepure_V1_2.dsk",11,29,"alien_clonepure"),
    #("/mnt/data2/roms/Apple/asi_games/action/plasmania/Plasmania (4am crack).dsk",5,11,"plasmania"),
    # ("/mnt/data2/roms/Apple/asi_games/action/deathsword/Deathsword (4am crack).dsk",34,118,"death_sword"),
    # ("/mnt/data2/roms/Apple/asi_games/action/captain_goodnight/captain goodnight a (san inc crack).dsk",15,28,"captain_goodnight"),
    # ("/mnt/data2/roms/Apple/asi_games/action/Up N Down (4am crack).dsk",14,45,"upndown"),
    # ("/mnt/data2/roms/Apple/asi_games/action/The Rocky Horror Show (4am crack).dsk",63,129,"rockyhorror"),
    # ("/mnt/data2/roms/Apple/asi_games/action/Into the Eagle's Nest (clean crack).dsk",17,73,"eaglenest"),
    # ("/mnt/data2/roms/Apple/asi_games/action/G.I. Joe (4am crack) side A.dsk",26,43,"gijoe"),
    # ("/mnt/data2/roms/Apple/asi_games/sports/california_games/california-games-1of2.dsk","TURBO_START, WAIT_UNTIL 5,KEYS 1,WAIT_UNTIL 23, SPEAKER_REC_START, WAIT_UNTIL 50, SCREENSHOT california, WAIT_UNTIL 109, SPEAKER_REC_STOP california, QUIT","california"),
    # ("/mnt/data2/roms/Apple/asi_games/rpg/phm_pegasus/PMH Pegasus - Front.dsk","TURBO_START, WAIT_UNTIL 7,KEYS 1,WAIT_UNTIL 21, SPEAKER_REC_START, SCREENSHOT pegasus, WAIT_UNTIL 34, SPEAKER_REC_STOP pegasus, QUIT","pegasus"),
    # ("/mnt/data2/roms/Apple/asi_games/rpg/legacy_of_ancients/legacy_of_ancients_1.dsk",13,166,"legacyancients"),
    # ("/mnt/data2/roms/Apple/asi_games/rpg/dragon_wars/dragon_war_a.dsk",23,53,"dragonwars"),
    # ("/mnt/data2/roms/Apple/asi_games/misc/pick_a_dilly_pair.dsk", 23, 60, "pickadilly"),
    # ("/mnt/data2/roms/Apple/RT.SYNTH.DSK", "TURBO_START, WAIT_UNTIL 12,KEYS 1,WAIT_UNTIL 15,KEYS §,WAIT_UNTIL 20,KEYS PACK1.VP§,WAIT_UNTIL 26,KEYS Y§,WAIT_UNTIL 27,KEYS START.MUSIC§,WAIT_UNTIL 33, SPEAKER_REC_START, SCREENSHOT rtsynth, WAIT_UNTIL 34, KEYS P, WAIT_UNTIL 39, SPEAKER_REC_STOP rtsynth, QUIT", "rtsynth"),
    # ("/mnt/data2/roms/Apple/archon_i.dsk","TURBO_START, WAIT_UNTIL 7, KEY_SPACE, WAIT_UNTIL 8, KEY_SPACE, WAIT_UNTIL 9, KEY_SPACE, WAIT_UNTIL 10, KEY_SPACE, WAIT_UNTIL 17, SPEAKER_REC_START, WAIT_UNTIL 40, SCREENSHOT archon, WAIT_UNTIL 48, SPEAKER_REC_STOP archon, QUIT","archon"),
    # ("/mnt/data2/roms/Apple/Boulder Dash (4am and san inc crack)/Boulder Dash (4am and san inc crack).dsk",7,29,"boulder_dash"),
    # ("/mnt/data2/roms/Apple/Conan (4am crack) side A.dsk",31,106,"conan"),
    # ("/mnt/data2/roms/Apple/seadragon (krakowicz).dsk",9,16,"seadragon"),
    # ("/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/adventure/dark_lord/dark_lord_1.dsk",7,57,"darklord"),
    # ("/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/rpg/wind_walker/wind_walker_1.dsk",93,140,"windwalker"),
    # ("/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/action/Ballblazer (4am crack).dsk",12,25,"ballblazer"),
    # ("/mnt/data2/roms/Apple/Miami_Sound_Machine3_Dckd.dsk",7,114,"miami"),
    # ("/mnt/data2/roms/Apple/asimov/mirrors.apple2.org.za/ftp.apple.asimov.net/images/games/action/impossible_mission/Impossible Mission II (4am crack) side A.dsk",22,77,"im2")
         ]


def move_file(src_file, dst_dir):
    dst_file = os.path.join(dst_dir,src_file)

    if os.path.exists(dst_file):
        # in case of the src and dst are the same file
        if os.path.samefile(src_file, dst_file):
            return
        os.remove(dst_file)
    shutil.move(src_file, dst_dir)

for file_info in FILES:
    if len(file_info) == 4:
        dsk_path, start_time, end_time, out_path = file_info
        script = "TURBO_START, WAIT_UNTIL {start_time}, SPEAKER_REC_START {out_path}, WAIT_UNTIL {screenshot}, SCREENSHOT {out_path}, WAIT_UNTIL {end_time}, SPEAKER_REC_STOP, QUIT"
        script = script.format(out_path=out_path,start_time=start_time,end_time=end_time,screenshot=max((start_time+end_time)//2,end_time-1))
        #script = script.format()
    else:
        dsk_path, script, out_path = file_info
        start_time = end_time = None

    if not os.path.exists(dsk_path):
        print(f"Can't see {dsk_path}")
        continue


    cmd = f"cargo run --release -- --floppy1 \"{dsk_path}\" --script \"{script}\""
    print(cmd)
    subprocess.run(shlex.split(cmd))

    move_file(f"{out_path}.ticks", "recordings")
    move_file(f"{out_path}.png", "recordings")



    nic_name = re.sub(r'\(.+\)','',os.path.basename(dsk_path).strip().replace(' ','_')).upper()[:8] + ".NIC"
    cmd = f"/usr/bin/python3 dsk2nic.py -s \"{dsk_path}\" -t \"{nic_name}\""
    print(cmd)
    subprocess.run(shlex.split(cmd))
