import numpy as np
import subprocess
import os
import shlex
import shutil
import re
import glob

LINE_RE = re.compile(r"[^0-9]*([0-9]+) (.*)")
NAME_RE = re.compile(r"/.*/([^(]+).*\.dsk")

SPKR_RE = re.compile(r"SPKR: ([0-9]+) (.*)")

seen_dsk = set()

if  os.path.exists("games_with_sound.txt"):
    with open("games_with_sound.txt") as fin:
        for line in fin.readlines():
            line = line.strip()
            match = LINE_RE.match(line.strip())
            dsk_path = match.groups()[1]
            seen_dsk.add(dsk_path)

            spkr_match = SPKR_RE.match(line)
            if spkr_match:
                if True:
                    size = int(match.groups()[0])
                    dsk_path = match.groups()[1]

                    if size > 100000:

                        nic_name = re.sub(r'\(.+\)','',os.path.basename(dsk_path).strip().replace(' ','_').replace('.','_').replace('__','_')).upper()[:8] + ".NIC"
                        print(f"{dsk_path} -> {nic_name}")
                        cmd = f"/usr/bin/python3 dsk2nic.py -s \"{dsk_path}\" -t \"{nic_name}\""
                        subprocess.run(shlex.split(cmd))

                if False:
                    short_name = NAME_RE.match(dsk_path).groups()[0].strip().replace(" ","_").replace("'","_").replace("]","I").replace("[","I").replace(".","_").replace(",","_")

                    script = f"TURBO_START, WAIT_UNTIL 3, SPEAKER_REC_START, WAIT_UNTIL 60, SPEAKER_REC_STOP {short_name}, QUIT"
                    cmd = f"/mnt/data2/rust_target/release/accurapple --no-greetings --floppy1 \"{dsk_path}\" --script \"{script}\""
                    try:
                        subprocess.run(shlex.split(cmd), timeout=25)
                    except subprocess.TimeoutExpired as ex:
                        pass

                    with open(f"{short_name}.ticks","rb") as fin:
                        data = fin.read()
                        # >u8 == big endian, 8 bytes unsigned
                        # integer (64 bits)
                        ticks = np.frombuffer(data, dtype=">u8")
                        first_second = int(ticks[0] / 1000000)
                        last_second = int(ticks[-1] / 1000000 + 1)
                        print(f"{short_name} {first_second} {last_second}")

                        script = f"TURBO_START, WAIT_UNTIL {last_second - 2}, SCREENSHOT {short_name}, WAIT_UNTIL 70, QUIT"
                        cmd = f"/mnt/data2/rust_target/release/accurapple --no-greetings  --floppy1 \"{dsk_path}\" --script \"{script}\""
                        subprocess.run(shlex.split(cmd))

            else:
                #print(f"No music: {line}")
                pass


exit()

all_games_dsk = sorted(list(glob.glob("/mnt/data2/roms/Apple/asi_games/**/*.dsk")))
for ndx_dsk, dsk_path in enumerate(all_games_dsk):
    if dsk_path in seen_dsk or "file_based" in dsk_path:
        print(f"skip {dsk_path}")
        continue

    print(f"{dsk_path} {ndx_dsk+1}/{len(all_games_dsk)}")
    # dsk_path = "/mnt/data2/roms/Apple/asi_games/sports/winter_games.dsk"

    #dsk_path = "/mnt/data2/roms/Apple/asi_games/action/G.I. Joe (4am crack) side A.dsk"

    if os.path.exists("game_sound.ticks"):
        os.remove("game_sound.ticks")

    scripts = ["TURBO_START, WAIT_UNTIL 3, SPEAKER_REC_START, WAIT_UNTIL 60, SPEAKER_REC_STOP game_sound, QUIT",
               "TURBO_START, WAIT_UNTIL 3, SPEAKER_REC_START, WAIT_UNTIL 10, KEYS Y, WAIT_UNTIL 60, SPEAKER_REC_STOP game_sound, QUIT"]

    for script_ndx, script in enumerate(scripts):
        cmd = f"/mnt/data2/rust_target/release/accurapple --no-greetings  --floppy1 \"{dsk_path}\" --script \"{script}\""
        try:
            subprocess.run(shlex.split(cmd), timeout=25)
        except subprocess.TimeoutExpired as ex:
            pass

        with open("games_with_sound.txt","a") as fout:
            if os.path.exists("game_sound.ticks"):
                with open("game_sound.ticks","rb") as fin:
                    data = fin.read()
                    # >u8 == big endian, 8 bytes unsigned
                    # integer (64 bits)
                    ticks = np.frombuffer(data, dtype=">u8")

                    if ticks.shape[0] > 7_000:
                        print("Sound")
                        ticks = ticks - np.min(ticks)
                        fout.write(f"SPKR: {ticks.shape[0]} {dsk_path}\n")
                        break
                    else:
                        print(f"Too short sound {ticks.shape[0]}")

            if script_ndx == len(scripts) - 1:
                print("No sound at all")
                fout.write(f"{ticks.shape[0]} {dsk_path}\n")
