// Code generation makes ugly code...
#![allow(warnings, unused)]
use log::{debug, error, info, trace};

use std::cell::{RefCell};
use std::rc::Rc;
use std::thread;
use std::io;
use std::io::Write;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;

// use std::time::Duration;
// use std::thread::sleep;
// use std::time::Instant;
use clap::{Arg, App, SubCommand};

type m6510_out_t = fn(data: u8, user_data: u8);
type m6510_in_t = fn(user_data: u8) -> u8;

struct m6502_desc_t {
    bcd_disabled: bool,              /* set to true if BCD mode is disabled */
    //m6510_in_cb: m6510_in_t,         /* optional port IO input callback (only on m6510) */
    //m6510_out_cb: m6510_out_t,       /* optional port IO output callback (only on m6510) */
    m6510_user_data: u8,          /* optional callback user data */
    m6510_io_pullup: u8,        /* IO port bits that are 1 when reading */
    m6510_io_floating: u8      /* unconnected IO port pins */
}

pub struct Cycle6502 {
    pub IR: u16,        /* internal instruction register */
    pub PC: u16,        /* internal program counter register */
    pub AD: u16,        /* ADL/ADH internal register */
    pub A: u8,  /* regular registers */
    pub X: u8,  /* regular registers */
    pub Y: u8,  /* regular registers */
    pub S: u8,  /* regular registers */
    pub P: u8,  /* regular registers */
    pub PINS: u64,      /* last stored pin state (do NOT modify) */
    pub irq_pip: u16,
    nmi_pip: u16,
    brk_flags: u8,  /* M6502_BRK_* */
    bcd_enabled: u8,
    // /* 6510 IO port state */
    // user_data: u8,
    // in_cb: m6510_in_t ,
    // out_cb: m6510_out_t ,
    // io_ddr: u8,     /* 1: output, 0: input */
    // io_inp: u8,     /* last port input */
    // io_out: u8,     /* last port output */
    // io_pins: u8,    /* current state of IO pins (combined input/output) */
    // io_pullup: u8,
    // io_floating: u8,
    // io_drive: u8
    pub cycles: u64,
    pub last_instr_pc: u16,
}

pub const M6502_CF: u8 =     (1<<0);  /* zero */
pub const M6502_ZF: u8 =     (1<<1);  /* zero */
pub const M6502_IF: u8 =     (1<<2);  /* IRQ disable */
pub const M6502_DF: u8 =     (1<<3);  /* decimal mode */
pub const M6502_BF: u8 =     (1<<4);  /* BRK command */
pub const M6502_XF: u8 =     (1<<5);  /* unused */
pub const M6502_VF: u8 =     (1<<6);  /* overflow */
pub const M6502_NF: u8 =     (1<<7);  /* negative */


pub const M6502_RW: u64 = 1<<24; /* out: memory read or write access */
pub const M6502_SYNC: u64 = 1<<25;      /* out: start of a new instruction */
pub const M6502_IRQ: u64 = 1<<26;     /* in: maskable interrupt requested */
pub const M6502_NMI: u64 = 1<<27;      /* in: non-maskable interrupt requested */
pub const M6502_RDY: u64 = 1<<28;      /* in: freeze execution at next read cycle */
const M6502_RES: u64 = 1<<30;

const M6502_BRK_IRQ: u8 = (1<<0);  /* IRQ was triggered */
const M6502_BRK_NMI: u8 =  (1<<1);  /* NMI was triggered */
const M6502_BRK_RESET: u8 = (1<<2);  /* RES was triggered */

fn addi8tou16(long: u16, short: u8) -> u16 {

    if short == 0 {
	return long
    } else if short & 0x80 == 0 {
	// short is positive.
	return long.wrapping_add(short as u16);
    } else  {
	// short is negative.
	// !x == -x-1. Example x=0xFF (-1) => !x == 0 => !x+1 == 1
	let s: u8 = (!short & 0x7F) + 1;
	return long.wrapping_sub( s as u16);
    }
}

/* set 16-bit address in 64-bit pin mask */
// #define _SA(addr) pins=(pins&~0xFFFF)|((addr)&0xFFFFULL)

macro_rules! SA {
    ($pins:expr, $addr:expr) => {
	$pins = ($pins & !0xFFFF) | ((u64::from($addr)) & 0xFFFF)
    }
}

/* extract 16-bit addess from pin mask */
// #define _GA() ((uint16_t)(pins&0xFFFFULL))

macro_rules! GA {
    ($pins:expr) => {
	($pins & 0xFFFF) as u16
    }
}

macro_rules! ON {
    ($pins:expr, $m:expr) => {
	$pins = $pins | $m;
    }
}

macro_rules! OFF {
    ($pins:expr, $m:expr) => {
	$pins = $pins & !$m;
    }
}

macro_rules! FETCH {
    ($pins:expr, $c:expr) => {
	SA!($pins, $c.PC);
	ON!($pins, M6502_SYNC);
    }
}

// #define _SD(data) pins=((pins&~0xFF0000ULL)|(((data&0xFF)<<16)&0xFF0000ULL))
// FIXME The last & is not necessary
macro_rules! SD {
    ($pins:expr, $data:expr) => {
	$pins = ($pins & !0xFF0000) | (((($data as u64) & 0xFF)<<16) & 0xFF0000);
    }
}

/* extract 8-bit data from 64-bit pin mask */
macro_rules! GD {
    ($pins:expr) => {
	(($pins & 0x00FF0000)>>16) as u8
    }
}

macro_rules! RD {
    ($pins:expr) => {
	ON!($pins, M6502_RW)
    }
}

// #define _WR() _OFF(M6502_RW);

macro_rules! WR {
    ($pins:expr) => {
	OFF!($pins, M6502_RW)
    }
}



// #define M6502_SET_DATA(p,d) {p=(((p)&~0xFF0000ULL)|(((d)<<16)&0xFF0000ULL));}
/* extract 16-bit address bus from 64-bit pins */
//#define M6502_GET_ADDR(p) ((uint16_t)(p&0xFFFFULL))

macro_rules! M6502_GET_ADDR {
    ($pins:expr) => {
	($pins & 0xFFFF) as u16;
    }
}

/* extract 8-bit data bus from 64-bit pins */
// #define M6502_GET_DATA(p) ((uint8_t)((p&0xFF0000ULL)>>16))
macro_rules! M6502_GET_DATA {
    ($pins:expr) => {
	(($pins & 0x00FF0000) >> 16) as u8;
    }
}

/* merge 8-bit data bus value into 64-bit pins */
// #define M6502_SET_DATA(p,d) {p=(((p)&~0xFF0000ULL)|(((d)<<16)&0xFF0000ULL));}
macro_rules! M6502_SET_DATA {
    ($pins:expr, $data:expr) => {
	$pins = (($pins & !0xFF0000)|((($data as u64)<<16)&0xFF0000));
    }
}


// #define _M6502_NZ(p,v) ((p&~(M6502_NF|M6502_ZF))|((v&0xFF)?(v&M6502_NF):M6502_ZF))

macro_rules! M6502_NZ {
    ($p:expr, $v:expr) => {
	($p & !(M6502_NF|M6502_ZF)) |
	if ($v&0xFF) != 0 {$v & M6502_NF} else {M6502_ZF};
    }
}

// #define _SAD(addr,data) pins=(pins&~0xFFFFFF) | ((((data)&0xFF)<<16)&0xFF0000ULL) | ((addr)&0xFFFFULL)
macro_rules! SAD {
    ($pins:expr, $addr:expr, $data:expr) => {
	$pins = ($pins & !0xFFFFFF) | (((($data as u64)&0xFF)<<16) & 0xFF0000) | (($addr as u64) & 0xFFFF);
    }
}


// const fn optick( opcode: u8, subtick: u8) -> u16 {
//     return 5;
// }

impl Cycle6502 {

    // #define _NZ(v) c->P=((c->P&~(M6502_NF|M6502_ZF))
    //    | ((v&0xFF) ? (v&M6502_NF) : M6502_ZF))

    fn NZ(&mut self, v:u8) {
	self.P = self.NZNoUpdate(v);
    }

    fn NZNoUpdate(&self, v:u8) -> u8 {
	return (self.P & !(M6502_NF|M6502_ZF)) |
	   if v & 0xFF != 0 {v & M6502_NF } else {M6502_ZF};
    }


    fn adc(&mut self, val: u8) {
	let cpu = self;

	if (cpu.bcd_enabled != 0 && (cpu.P & M6502_DF != 0)) {
            /* decimal mode (credit goes to MAME) */
	    //println!("BCD adc A=${:02X} v=${:02X}", cpu.A, val);
            let c:u8 = if(cpu.P & M6502_CF != 0) {1} else {0};
            cpu.P &= !(M6502_NF|M6502_VF|M6502_ZF|M6502_CF);
            let mut al:u8 = (cpu.A & 0x0F) + (val & 0x0F) + c;
            if (al > 9) {
		al = al.wrapping_add(6);
            }
            let mut ah:u8 = (cpu.A >> 4) + (val >> 4) + if al > 0x0F {1} else {0};
            if 0 == cpu.A.wrapping_add( val.wrapping_add(c))  {
		cpu.P |= M6502_ZF;
            }
            else if (ah & 0x08 != 0) {
		cpu.P |= M6502_NF;
            }
            if (!(cpu.A^val) & (cpu.A^(ah<<4)) & 0x80 != 0) {
		cpu.P |= M6502_VF;
            }
            if (ah > 9) {
		ah = ah.wrapping_add(6);
		//ah += 6;
            }
            if (ah > 15) {
		cpu.P |= M6502_CF;
            }

            cpu.A = (ah << 4) | (al & 0x0F);

	}
	else {
	    /* default mode */
	    let sum:u16 = (cpu.A as u16) + (val as u16) + (if cpu.P & M6502_CF != 0 {1} else {0});
	    cpu.P &= !(M6502_VF|M6502_CF);
	    cpu.NZ((sum & 0xFF) as u8);
	    if (!(cpu.A^val) & (cpu.A^(sum as u8)) & 0x80 != 0) {
		cpu.P |= M6502_VF;
	    }
	    if (sum & 0xFF00 != 0) {
		cpu.P |= M6502_CF;
	    }
	    cpu.A = (sum & 0xFF) as u8;
	}
    }

    fn arr(&mut self) {

    }

    fn sbx(&mut self, v: u8) {

    }

    fn sbc(&mut self, val: u8) {
	let cpu = self;
	if (cpu.bcd_enabled != 0) && (cpu.P & M6502_DF != 0) {
            /* decimal mode (credit goes to MAME) */
            let c:u8 = if cpu.P & M6502_CF != 0 {0} else {1};
            cpu.P &= !(M6502_NF|M6502_VF|M6502_ZF|M6502_CF);
            let diff: u16 = (cpu.A as u16).wrapping_sub(val as u16).wrapping_sub(c as u16);
            let mut al:u8 = (cpu.A & 0x0F).wrapping_sub(val & 0x0F).wrapping_sub(c);

	    //println!("BCD sbc A=${:02X} v=${:02X} diff=${:04X} al=${:02X}", cpu.A, val, diff, al);

	    //if (al as i8 < 0) {
	    if (al & 0x80 != 0) {
		al = al.wrapping_sub(6);
            }

            let mut ah: u8 = (cpu.A >> 4).wrapping_sub(val >> 4).wrapping_sub(if al & 0x80 != 0 {1} else {0});
	    //if (0 == (uint8_t)diff) {
	    if (0 == diff & 0xFF) {
		cpu.P |= M6502_ZF;
	    }
	    else if (diff & 0x80 != 0) {
		cpu.P |= M6502_NF;
	    }
	    if ((cpu.A^val) & (cpu.A^((diff & 0xFF) as u8) & 0x80) != 0) {
		cpu.P |= M6502_VF;
	    }
            if (diff & 0xFF00 == 0) {
		cpu.P |= M6502_CF;
            }
            if (ah & 0x80 != 0) {
		ah = ah.wrapping_sub(6);
            }
            cpu.A = (ah<<4) | (al & 0x0F);
	}
	else {
	    /* default mode */
	    let carry: bool = cpu.P & M6502_CF != 0;
	    let diff: u16 = (cpu.A as u16).wrapping_sub(val as u16).wrapping_sub(if carry {0} else {1});

	    cpu.P &= !(M6502_VF|M6502_CF);
	    cpu.NZ(diff as u8);

	    let diff8: u8 = (diff & 0xFF) as u8;
	    if ((cpu.A^val) & (cpu.A^diff8) & 0x80 != 0) {
		cpu.P |= M6502_VF;
	    }
	    if (diff & 0xFF00 == 0) {
		cpu.P |= M6502_CF;
	    }
	    //println!("SBC A={} v={} carry={} VF={}: diff={}", cpu.A, val, carry, cpu.P & M6502_VF, diff);
	    cpu.A = diff8;
	}
    }

    fn bit(&mut self, v: u8) {
	let cpu = self; // Rename as in original code (I can't rename the self parameter in method's prototype, rust forbids)
	let t: u8 = cpu.A & v;
	cpu.P &= !(M6502_NF|M6502_VF|M6502_ZF);
	if (t == 0) {
            cpu.P |= M6502_ZF;
	}
	cpu.P |= v & (M6502_NF|M6502_VF);
    }

    fn cmp(&mut self, r: u8, v: u8) {
	let t: u16 = (r as u16).wrapping_sub(v as u16);
	self.P = (self.NZNoUpdate(t as u8) & !M6502_CF) | if t & 0xFF00 != 0 {0} else {M6502_CF};
    }

    fn lsr(&mut self, v: u8) -> u8 {
	self.P = (self.NZNoUpdate(v >> 1) & !M6502_CF) | if v & 0x01 != 0 {M6502_CF} else {0};
	return v >> 1;
    }

    fn asl(&mut self, v: u8) -> u8 {
	self.P = (self.NZNoUpdate(v << 1) & !M6502_CF) | if(v & 0x80 != 0) {M6502_CF} else {0};
	return v << 1;
    }

    fn ror(&mut self, mut v: u8) -> u8 {
	let cpu = self; // Rename as in original code (I can't rename the self parameter in method's prototype, rust forbids)
	let carry: bool = cpu.P & M6502_CF != 0;

	cpu.P &= !(M6502_NF|M6502_ZF|M6502_CF);
	if (v & 1 != 0) {
            cpu.P |= M6502_CF;
	}
	v = v >> 1;
	if (carry) {
            v |= 0x80;
	}
	cpu.NZ(v);
	return v;
    }

    fn rol(&mut self, mut v: u8) -> u8 {
	let cpu = self;
	let carry: bool = cpu.P & M6502_CF != 0;
	let v_in = v;

	cpu.P &= !(M6502_NF|M6502_ZF|M6502_CF);
	if (v & 0x80 != 0) {
            cpu.P |= M6502_CF;
	}
	v = v << 1;
	if (carry) {
            v = v | 1;
	}
	cpu.NZ(v);
	//cpu.P = M6502_NZ!(cpu.P, v); // FIXME use cpu.NZ()
	//println!("ROL {:02X} -> {:02X}", v_in, v);
	return v;
    }

    pub fn tick( self: &mut Self, pins_: u64) -> u64 {
	let c = self; // Rename as in original code (I can't rename the self parameter in method's prototype, rust forbids)
	let mut pins = pins_;

	if (pins & (M6502_SYNC|M6502_IRQ|M6502_NMI|M6502_RDY|M6502_RES) != 0) {

            if ((pins & M6502_IRQ != 0) && (0 == (c.P & M6502_IF))) {
		trace!("6502 sees IRQ on cycle {}", c.cycles);
		c.irq_pip |= 0x100;
            }

            if ((pins & (M6502_RW|M6502_RDY)) == (M6502_RW|M6502_RDY)) {
		trace!("pip << 1");
		//M6510_SET_PORT(pins, c->io_pins);
		c.PINS = pins;
		c.irq_pip = c.irq_pip << 1;
		return pins;
            }

	    //println!("pins OUT: {:#032b}", pins_);

	    if (pins & M6502_SYNC != 0) {
		c.IR = (GD!(pins) as u16) << 3;
		c.last_instr_pc = c.PC;
		//println!("M6502_SYNC pins OUT: {:#032b} {:04X}", pins_, c.last_instr_pc);
		OFF!(pins, M6502_SYNC);

		if 0 != (c.irq_pip & 0x400) {
		    trace!("6502 will BRK on cycle {}", c.cycles);
                    c.brk_flags |= M6502_BRK_IRQ;
		}
		c.irq_pip &= 0x3FF;
		c.nmi_pip &= 0x3FF;

		// if interrupt or reset was requested, force a BRK instruction
		if (c.brk_flags != 0) {
                    c.IR = 0;
                    c.P &= !M6502_BF;
                    pins &= !M6502_RES;
		}
		else {
                    c.PC = c.PC.wrapping_add(1);
		}
	    }
	}
	RD!(pins);
 	match(c.IR) {
	    $decode_block
	    _ => panic!("Unrecognized IR {:04X} at PC:{:04X}", c.IR, c.PC)

	}

	c.IR = c.IR + 1;
	c.PINS = pins;
	c.irq_pip <<= 1;

	c.cycles += 1;
	return pins
    }

    pub fn start_cpu_at( &mut self, addr: u16, pins: u64, opcode: u8) -> u64{

	let mut pins_after: u64 = pins;

	self.PC = addr;
	// fetch :
	//  SA!($pins, $c.PC); // set address on pins
	//  ON!($pins, M6502_SYNC); // ask for sync
	SD!(pins_after, opcode);
	FETCH!(pins_after, self);

	// tick:
	// if (pins & M6502_SYNC != 0) {
        //     c.IR = (GD!(pins) as u16) <<3;

	pins_after
    }

    // return Cycle6502 {
    // 	IR: 0,        /* internal instruction register */
    // 	PC: addr.wrapping_sub(1),        /* internal program counter register */
    // 	AD: addr.wrapping_sub(1) & 0x00FF,        /* ADL/ADH internal register */
    // 	A: 0,  /* regular registers */
    // 	X: 0,  /* regular registers */
    // 	Y: 0,  /* regular registers */
    // 	S: 0,  /* regular registers */
    // 	P: M6502_ZF,  /* regular registers */
    // 	PINS: M6502_RW | M6502_SYNC | M6502_RES,      /* last stored pin state (do NOT modify) */
    // 	irq_pip: 0,
    // 	nmi_pip: 0,
    // 	brk_flags: 0,  /* M6502_BRK_* */
    // 	bcd_enabled: 1,
    // 	cycles: 0,
    // 	last_instr_pc: 0
    // };
}


pub fn reset_cpu() -> Cycle6502 {
    return Cycle6502 {
	IR: 0,        /* internal instruction register */
	PC: 0,        /* internal program counter register */
	AD: 0,        /* ADL/ADH internal register */
	A: 0,  /* regular registers */
	X: 0,  /* regular registers */
	Y: 0,  /* regular registers */
	S: 0,  /* regular registers */
	P: M6502_ZF,  /* regular registers */
	PINS: M6502_RW | M6502_SYNC | M6502_RES,      /* last stored pin state (do NOT modify) */
	irq_pip: 0,
	nmi_pip: 0,
	brk_flags: M6502_BRK_RESET, // RES was triggered  /* M6502_BRK_* */
	bcd_enabled: 1,
	cycles: 0,
	last_instr_pc: 0
    };
}


fn test(mut ram: Vec<u8>) {

    // let mut a = m6502_desc_t {
    // 	bcd_disabled: true,              /* set to true if BCD mode is disabled */
    // 	m6510_user_data: 0,          /* optional callback user data */
    // 	m6510_io_pullup: 0,        /* IO port bits that are 1 when reading */
    // 	m6510_io_floating: 0      /* unconnected IO port pins */
    // };

    // let mut cpu = Cycle6502 {
    // 	IR: 0,        /* internal instruction register */
    // 	PC: 0x3FF,        /* internal program counter register */
    // 	AD: 0xFF,        /* ADL/ADH internal register */
    // 	A: 0,  /* regular registers */
    // 	X: 0,  /* regular registers */
    // 	Y: 0,  /* regular registers */
    // 	S: 0,  /* regular registers */
    // 	P: M6502_ZF,  /* regular registers */
    // 	PINS: M6502_RW | M6502_SYNC | M6502_RES,      /* last stored pin state (do NOT modify) */
    // 	irq_pip: 0,
    // 	nmi_pip: 0,
    // 	brk_flags: 0,  /* M6502_BRK_* */
    // 	bcd_enabled: 1,
    // 	cycles: 0,
    // 	last_instr_pc: 0
    // };

    // let mut cpu = start_cpu_at( 0x400);
    // M6502_SET_DATA!(pins, ram[0x400]);
    // // Set pins :
    // ON!(pins, M6502_SYNC);

    let mut cpu = reset_cpu();
    ram[0xFFFF] = 0x04;
    ram[0xFFFE] = 0x00;
    ram[0xFFFD] = 0x04;
    ram[0xFFFC] = 0x00;

    let mut pins: u64 = cpu.PINS;

    let mut prev_instr_pc: u16 = 0;
    let mut disassembler = rs6502::Disassembler::new();

    const limit: i64 = 30; // 100_664_000;

    for i in 0..limit {

	pins = cpu.tick(pins);

	if cpu.cycles & 0x7FFFFF == 0 {
	    println!("{}",cpu.cycles)
	}

	// if i > limit - 100_000_000  {
	//     println!("jjj {} {} {}", i, limit - 100_000, i > limit - 100_000);
	// }

	if (true ||  prev_instr_pc != cpu.last_instr_pc) {
	    if true || i > limit - 5_000_000  {
		//println!("jjj {} {} {}", i, limit - 100_000, i > limit - 100_000);

		//if pins & M6502_SYNC {
		let offset = prev_instr_pc as usize;
		disassembler.code_offset = offset as u16;
		let m:[u8; 4] = [ram[offset], ram[offset+1], ram[offset+2], ram[offset+3]];
		let asm = disassembler.disassemble(&m);
		println!("{: <31} SP=01{:02X} A={:02X}, X={:02X}, Y={:02X} flags:{}{}{}{}{}{}{}{}|{:08b}={:02X} IR: ${:04X}/{:}, PC: ${:04x} AD: ${:04x} | {} ",
			 asm, cpu.S, cpu.A, cpu.X, cpu.Y,
			 if cpu.P & M6502_NF != 0 {"N"} else {"-"},
			 if cpu.P & M6502_VF != 0 {"V"} else {"-"},
			 if cpu.P & M6502_XF != 0 {"X"} else {"-"},
			 if cpu.P & M6502_BF != 0 {"B"} else {"-"},
			 if cpu.P & M6502_DF != 0 {"D"} else {"-"},
			 if cpu.P & M6502_IF != 0 {"I"} else {"-"},
			 if cpu.P & M6502_ZF != 0 {"Z"} else {"-"},
			 if cpu.P & M6502_CF != 0 {"C"} else {"-"},
			 cpu.P, cpu.P,
			 cpu.IR>>3, cpu.IR & 7, cpu.PC, cpu.AD, cpu.cycles);

	    }
	    prev_instr_pc = cpu.last_instr_pc
	}


	let addr: u16 = M6502_GET_ADDR!(pins);

	let read: bool = pins & M6502_RW != 0;
	//println!("pins addr = {:04x} {}",addr, if read {"RD"} else {"WT"});

	if (read) {
	    // Mem read
	    M6502_SET_DATA!(pins, ram[addr as usize]);
	    // println!("   Read : {:02X}",ram[addr as usize]);
	} else {
	    //println!("   Write: {:02X}",M6502_GET_DATA!(pins));
	    // a memory write
	    ram[addr as usize] = M6502_GET_DATA!(pins);
        }
    }
    //cpu.regs_to_str();
    // Should end at $346B
    println!("{} Cycles ran",cpu.cycles);
}


use rs6502::Disassembler;
use rs6502::Assembler;
use std::time::Duration;
use std::thread::sleep;
use std::time::Instant;

fn main() {
     let asm = "
        .ORG $800
        LDA #$DE
        LDA #$AD
        CLC
        ADC #$1
        SBC #$1
        ROL
        ROR
    ";

    let mut assembler = rs6502::Assembler::new();
    let segments = assembler.assemble_string(asm, None).unwrap();

    let mut ram: Vec<u8> = vec![0; 65536];
    let seg = &segments[0];
    let addr: usize = seg.address as usize;

    ram[addr .. addr + seg.code.len()].copy_from_slice(&seg.code);
    ram[0xFFFF] = 0x08;
    ram[0xFFFE] = 0x00;

    let cli = App::new("Apple 2 Emulator")
        .version("alpha")
        .arg(Arg::with_name("ram")
             .short("ram")
             .long("ram")
             .value_name("FILE")
             .help("RAM file")
             .takes_value(true)
             .required(true))
             .get_matches();

    let mut file = BufReader::new(File::open(cli.value_of("ram").unwrap()).unwrap());
    file.read(&mut ram[0..65536]);
    // ram[0xFFFF] = 0x04;
    // ram[0xFFFE] = 0x00;

    let start_time = Instant::now();
    test(ram);
    println!("Elapsed: {}", start_time.elapsed().as_millis());

}
