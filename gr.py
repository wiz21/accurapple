import numpy as np
from PIL import Image

APPLE_YRES=192

COLORS = { 0: np.array((0, 0, 0)),  # Black
           8: np.array((148, 12, 125)),  # Magenta
           4: np.array((99, 77, 0)),  # Brown
           12: np.array((249, 86, 29)),  # Orange
           2: np.array((51, 111, 0)),  # Dark green
           10: np.array((126, 126, 126)),  # Grey2
           6: np.array((67, 200, 0)),  # Green
           14: np.array((221, 206, 23)),  # Yellow
           1: np.array((32, 54, 212)),  # Dark blue
           9: np.array((188, 55, 255)),  # Violet
           5: np.array((126, 126, 126)),  # Grey1
           13: np.array((255, 129, 236)),  # Pink
           3: np.array((7, 168, 225)),  # Med blue
           11: np.array((158, 172, 255)),  # Light blue
           7: np.array((93, 248, 133)),  # Aqua
           15: np.array((255, 255, 255)),  # White
          }


def text_address(display_line):
    y = display_line // 8
    if 0 <= y <= 7:
        ofs = 0
    elif 8 <= y <= 15:
        ofs = 0x28
    elif 16 <= y <= 23:
        ofs = 0x50
    else:
        raise Exception(f"{y}")

    i = y % 8
    return ofs + 0x80 * i


with open("mem.bin","rb") as file_in:
    main_ram = file_in.read(0x10000)
    main_bank2_ram = file_in.read(0x1000)
    aux_ram = file_in.read(0x10000)
    aux_bank2_ram = file_in.read(0x1000)

    print(len(main_ram))
    print(len(main_bank2_ram))
    print(len(aux_ram))
    print(len(aux_bank2_ram))

image2 = np.ones((APPLE_YRES,40,3),dtype="uint8")

for y in range(APPLE_YRES):
    ofs = text_address(y) + 0x400
    half_line = (y // 4) % 2
    for px in range(40):

        ram = aux_ram
        if half_line % 2 == 0:
            pA = ram[ofs+px] & 0b00001111
        else:
            pA = (ram[ofs+px] & 0b11110000) >> 4

        image2[y,  px,:] = COLORS[pA]


Image.fromarray(image2,"RGB").resize((40*8, 192*2), Image.NEAREST).show()
