import subprocess
PATHS = [
    "additional/LOWTECH.WOZ",
    "additional/MAD3.dsk",
    #"additional/MADEF.dsk",
    "additional/boulder_dash.dsk",
    "additional/Articfox (clean crack).dsk",
    "data/CHP.dsk",
    "additional/Elite (compatiboot).dsk",
    "additional/mario_bros.dsk",
    #"additional/oldskool.dsk",
    #"additional/black_magic.dsk",
    "additional/Galaxian (Thunder Mountain) (4am crack).dsk"
    ]

while False:
    for path in PATHS:
        subprocess.run(f".\\target\\release\\accurapple.exe --floppy1 \"{path}\" --script \"WAIT_UNTIL 60,QUIT\"")


from PIL import Image, ImageDraw, ImageColor
img = Image.open("additional/study.hgr_gr.png")
draw = ImageDraw.Draw(img)
for i in range(-100,100):
    x = 212 + round(i*84/14)
    if i % 14 == 0:
        color = ImageColor.getrgb("green")
    elif (i % 14) % 2 == 0:
        color = ImageColor.getrgb("Blue")
    elif (i % 14) % 2 == 1:
        color = ImageColor.getrgb("DarkBlue")

    draw.line( (x,0,x,1000), color)
img.show()

