import re
import numpy as np

from time import sleep
import json
import numpy as np
import struct
import glob
from pprint import pprint
from internetarchive import get_item, search_items
from pathlib import Path

COLLECTIONS = ["softwarelibrary_apple_woz_games", "wozaday"]

for collection in COLLECTIONS:
    #continue

    WOZ_DIR = Path(collection)

    if not WOZ_DIR.exists():
        WOZ_DIR.mkdir()

    search = search_items(f'collection:{collection}')

    # Read the collection item
    # item = get_item('wozaday')
    # pprint(item.metadata)

    # Read the items matching a collection

    if not WOZ_DIR.exists():
        WOZ_DIR.mkdir()

    downloaded = 0
    for result in list(search):
        identifier = result['identifier']
        if "IIgs" not in identifier:
            store_dir = WOZ_DIR/identifier
            if not store_dir.exists():
                store_dir.mkdir()

                item = get_item(result['identifier'])

                # pprint(item.metadata)

                with open(store_dir / 'metadata.json', 'w') as f:
                    json.dump(item.metadata, f)

                print(item.metadata['title'])
                item.download(glob_pattern="*.woz", exclude_pattern="extra", destdir=str(WOZ_DIR))
                downloaded += 1

                if downloaded > 4000:
                    break

                sleep(abs(np.random.normal(3,2)))
            else:
                print(f"Skip {identifier}")



"""
0 	57 4F 5A 32 	The ASCII string ‘WOZ2’. 0x325A4F57
4 	FF 	Make sure that high bits are valid (no 7-bit data transmission)
5 	0A 0D 0A 	LF CR LF – File translators will often try to convert these.
8 	xx xx xx xx 	CRC32 of all remaining data in the file. The method used to generate the CRC is described in Appendix A.
"""
with open("wozaday_list.txt","w",encoding="utf-8") as wozlist:
    for dn in sorted(glob.glob(f"{COLLECTIONS[1]}/*")):
        metadata_path = Path(f'{dn}/metadata.json')

        if metadata_path.exists():
            with open(metadata_path, 'r') as f:
                metadata = json.load(f)

            for fn in glob.glob(f"{dn}/*.woz"):
                with open(fn, mode='rb') as filein:
                    woz = filein.read()

                    if len(woz) > 0:
                        magic, _, crc = struct.unpack("<4s4sI",woz[:12])
                        wozlist.write(f"{metadata['title'].replace('(woz-a-day collection)','')}\t{crc:04X}\n")


with open(f"{COLLECTIONS[0]}_list.txt","w",encoding="utf-8") as wozlist:
    for dn in sorted(glob.glob(f"{COLLECTIONS[0]}/*")):
        metadata_path = Path(f'{dn}/metadata.json')

        if metadata_path.exists():
            with open(metadata_path, 'r') as f:
                metadata = json.load(f)

            title = metadata["title"]
            for fn in glob.glob(f"{dn}/*.woz"):
                with open(fn, mode='rb') as filein:
                    woz = filein.read()

                    if len(woz) > 0:
                        magic, _, crc = struct.unpack("<4s4sI",woz[:12])
                        wozlist.write(f"{metadata['title'].replace('(woz-a-day collection)','')}\t{crc:04X}\n")
#exit()



