"""
To extract Grouik's data set at:

http://ctrl.pomme.reset.free.fr/index.php/2014-2017/

In Linux, this is much faster:
find Downloads/DSK -iname "*.dsk" | xargs -d "\n" crc32 | sed -r "s/([0-9a-f]+)\t(.*)/\2\t\1/" > grouik.txt
"""
