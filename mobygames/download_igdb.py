import json
import requests
from igdb_credentials import *

params = {
    "client_id" : IGDB_CLIENT_ID,
    "client_secret" : IGDB_SECRET,
    "grant_type": "client_credentials"
}
r = requests.post("https://id.twitch.tv/oauth2/token", params=params)
print(r.status_code)
d = json.loads(r.text)
access_token = d["access_token"]

headers = {
    "Client-ID" : IGDB_CLIENT_ID,
    "Authorization" :  f"Bearer {access_token}"
}

body = """fields name;
limit 500;"""
r = requests.post("https://api.igdb.com/v4/platforms", headers=headers, data=body)
print(r.status_code)
d = json.loads(r.text)
for entry in d:
    if "Apple II" == entry["name"]:
        platform = entry["id"]

offset = 0
all_games = []
while offset < 10000:
    body = f"""fields name;
    where release_dates.platform = (75);
    offset {offset};
    limit 500;
    """
    r = requests.post("https://api.igdb.com/v4/games", headers=headers, data=body)
    print(r.status_code)
    d = json.loads(r.text)
    if not d:
        break
    all_games.extend(d)
    offset += 500


with open("igdb.txt","w",encoding="utf-8") as fout:
    for n in all_games:
        name, url = n["name"], n["id"]
        fout.write(f"{name}\t{url}\n")
