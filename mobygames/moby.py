"""
On Moby's game page, there's this link that seems easy to scrape.

<meta property="og:image" content="https://cdn.mobygames.com/covers/4159933-choplifter-apple-ii-front-cover.jpg">
"""
import argparse
import pickle
import os
import re
import glob
from PIL import Image
import io
from pprint import pprint
import requests
import json
from time import sleep
import time
from bs4 import BeautifulSoup
import urllib.parse
from crc import Calculator, Crc8, Crc32
from pathlib import Path
from io import BytesIO
import more_itertools as mit
from datetime import datetime
import numpy as np 
import struct

MOBY_NAMES="moby_names.txt"
WOZ_DIR = Path("wozaday")

#r = requests.get('https://api.mobygames.com/v1/platforms?api_key=moby_gamvUz58ZQS76WPuleLzp1aoORe')
#print(r.status_code)
#print(r.json())


# r = ast.literal_eval(reply)
# for pd in r["platforms"]:
#     if pd["platform_name"] == "Apple II":
#         print(pd)

def split_long_name(name, words):
    if not name:
        return []
    if name in words:
        return [name]
    if name == "tothe":
        return ["to","the"]
    if name == "ofthe":
        return ["of","the"]
    if name == "onthe":
        return ["on","the"]
    if name == "bythe":
        return ["by","the"]
    #print(f"Splitting {name}")
    n = name
    j = 0
    best_len = 0
    while j < len(n)-1:
        for k in range(j+1,len(n)+1):
            if n[j:k] in words and k-j > best_len:
                best_len = k-j
                best_j = j
                best_k = k
                #print("nubest " + n[best_j:best_k])
        j += 1

    if best_len > 0:
        #print(n[:best_j], "/", n[best_j:best_k], "/", n[best_k:])
        r = list(filter(len, split_long_name(n[:best_j],words) + [n[best_j:best_k]] + split_long_name(n[best_k:], words)))
        #print(r)
        #sleep(5)
        return r
    else:
        return [name]

def non_semantic_canonize(name):
    name = re.sub(r'([a-z])([A-Z])', r'\1 \2', name)
    name = name.lower()
    name = re.sub(r'[ _]f.dsk', ".dsk", name, flags=re.IGNORECASE)
    name = re.sub(r'[ -]?([fs][12])?.dsk', "", name, flags=re.IGNORECASE)
    name = name.replace(".dsk","").replace(".woz","")
    name = re.sub(r'\(.+$','',name.strip().replace("_"," "))
    name = re.sub(r'side (a|b)',"",name,flags=re.IGNORECASE)
    name = name.replace("a2","")

    name = " " + name + " "
    name = name.replace("- boot","").replace("- game","").replace("- back","").replace("- front","")
    name = name.replace(" s1","").replace(" s2","").replace(" s3","").replace(" s4","")
    name = name.replace("-s1","").replace("-s2","").replace("-s3","").replace("-s4","")
    name = re.sub(r'((D|d)isk ?[12])? ?((s|S)ide ?[12ab])?', "", name, flags=re.IGNORECASE)
    name = name.replace("disk1","").replace("disk2","").replace(" disk","")
    name = name.replace("disk 1","").replace("disk 2","").replace("disk 3","")
    name = re.sub(r'disk [12] side [12]', "", name, flags=re.IGNORECASE)
    name = re.sub(r"([0-9]+)",r" \1 ",name)
    name = name.replace("+"," ")
    name = name.replace("  "," ").replace("  "," ").strip()
    name = name.replace("-"," ")
    name = name.replace("[alt]","")
    name = name.replace("cracked","")

    name = name.replace("articfox","arcticfox")
    return name

def canonize_name(name, words):
    name = non_semantic_canonize(name)

    name = re.sub(r"[ _]the[ _]"," ",name)
    name = re.sub(r"[ _]of[ _]"," ",name).replace("'s","")
    name = name.replace("super"," super ")
    name = name.replace("vs"," vs ")
    name = name.replace("-crack","")
    name = name.replace(" ]["," 2")
    name = name.replace(" iii"," 3")
    name = name.replace(" ii"," 2")
    name = name.replace(" 2e","")
    name = name.replace(" hr","").replace(" and ","").replace("etc","")
    name = name.replace("ofthe"," of the ")
    name = name.replace(" fixed","")

    good_words_list = []
    if True:
        split_names = []
        for n in name.split():
            #print(n)
            s = split_long_name(n, words)
            split_names.extend( s)
        #print(split_names)
        if True:# or len(name) < 12:

            for name_part in split_names:

                if re.match(r"([0-9]+)", name_part):
                    good_words_list.append(name_part)
                    continue

                best_perm = None
                best_score = None
                for perm in [list(map("".join, x)) for x in mit.partitions(name_part)]:
                    good_words = 0
                    used_chars = 0

                    for word in perm:
                        if len(word) >= 3 and word in words:
                            good_words += 1
                            used_chars += len(word)
                            #print(f"{word} good_words:{good_words} used_chars:{used_chars}")
                        elif word[-1] == 's' and word[:-1] in words: # Plural
                            good_words += 1
                            used_chars += len(word)
                            #print(f"<s> {word} good_words:{good_words} used_chars:{used_chars}")
                        elif len(word) == 1:
                            good_words -= 3
                        else:
                            good_words -= 1

                    if good_words >= 1:
                        score = (len(name_part) - used_chars) * (len(perm)-good_words)
                        if best_score is None or score < best_score:
                            #print(f"score={score} words score:{good_words} used_chars:{used_chars} {perm}")
                            best_perm = perm
                            best_score = score
                        else:
                            #print(f"               bad score={score} words score:{good_words} used_chars:{used_chars} {perm}")
                            pass

                if best_perm is not None:
                    #print(f"BEST {' '.join(best_perm)}")
                    good_words_list.append(' '.join(best_perm))
                else:
                    good_words_list.append(name_part)


            #exit()
            #print(f"[{name}]")
            #continue

        # print(f"Name is: {name} -> {' '.join(good_words_list)}")
        good_words_list = filter(lambda n: n != 'alt' and (len(n) > 1 or re.match("[0-9]",n)), good_words_list)
        name = ' '.join(good_words_list)

        if name.endswith(" 1"):
            name = name[0:-2]

        return name


class GameData:
    def __init__(self, dsk_path):
        self.has_long_music = False
        self.game_id = None
        self.dsk_path = dsk_path
        self.image = None
        self.id_retrieve_date = None

        # Compute CRC
        suffix = Path(dsk_path).suffix.lower()
        if suffix == ".dsk":
            with open(dsk_path, "rb") as dsk:
                calculator = Calculator(Crc32.CRC32, optimized=True)
                self.checksum = calculator.checksum(dsk.read())
        elif suffix == ".woz":
            with open(dsk_path, "rb") as iostream:
                header_raw = iostream.read(8)
                crc_raw = iostream.read(4)
                self.checksum = int.from_bytes(crc_raw, byteorder="little")
        else:
            raise Exception(f"Unrecognized suffix: {Path(dsk_path).suffix}")

        self.name = None

    def __str__(self):
        return f"{self.checksum:08X} {self.game_id or -1:6} {self.image} {self.name} {self.dsk_path}"

    def set_game_id(self, game_id):
        if game_id != self.game_id:
            self.game_id = game_id
            self.id_retrieve_date = datetime.now()

    def clear_game_id(self):
        self.game_id = None
        self.id_retrieve_date = datetime.now()

    # def load_moby(self, game_id):
    #     url = f'https://api.mobygames.com/v1/games/{game_id}?api_key={MOBY_API_KEY}'
    #     # This should work but doesn't. Signaled on the discord server.
    #     #url = f'https://api.mobygames.com/v1/games/?api_key={MOBY_API_KEY}&id={game_id}'
    #     r = requests.get(url)
    #     if r.status_code == 200:
    #         r = r.json()
    #         #pprint(r['sample_cover'])
    #         self.game_id = int(game_id)
    #     else:
    #         print(f"Unable to load game from Moby at url:{url}")

    def figure_real_name(self, words):
        self.name = canonize_name(
            re.sub(r'.*/',"",self.dsk_path,flags=re.IGNORECASE), words)

def show_all_games(all_games):
    print(f"Checksum GameID Name                 Path")
    for v in sorted( all_games.values(), key=lambda i: i.name.lower()):
        img_hint = "/"
        if hasattr(v, "image"):
            if v.image == True:
                img_hint = "Y"
            if v.image == False:
                img_hint = "N"

        rdate = ""
        if v.game_id:
            if v.id_retrieve_date is not None:
                rdate = str(v.id_retrieve_date)

        print(f"{v.checksum:08X} {v.game_id or -1:>6} ({img_hint}) {v.name:20} {str(v.dsk_path)[-100:]} {rdate}")


def find_moby_game_id(name):
    #print(f"Calling Moby for '{name}'")
    url = f"https://www.mobygames.com/search/?q={urllib.parse.quote(name)}"
    try:
        r = requests.get(url, timeout=10)
    except requests.exceptions.Timeout as to:
        print("Moby has timed out")
        return None

    if True:
        soup = BeautifulSoup(r.content, "html.parser")
        url = None
        game_id = None
        for s in soup.find_all("td"):
            if "Apple II" in str(s):

                bad_guess = False
                for word in name.split():
                    if word not in r.text:
                        bad_guess= True
                        break

                if bad_guess:
                    continue

                url = s.find("a").get("href")

                RE = re.compile(".*/([0-9]+)/.*")
                m = RE.match(url)
                if m:
                    game_id = m.groups()[0]
                    break

    if game_id is None:
        r = requests.get(f'https://api.mobygames.com/v1/games?api_key={MOBY_API_KEY}&platform=31&title={name}')

        if r.status_code == 200:
            r = r.json()

            if "games" in r and len(r["games"]) >= 1:
                #pprint(r["games"][0])
                game_id = r["games"][0]['game_id']

        elif r.status_code == 429: # Too many requests
            t = time.localtime()
            current_time = time.strftime("%H:%M:%S", t)
            print(f"Too many requests, sleeping 10 minutes. It is {current_time}. Mesg: {r.content}")
            sleep(60*10)

        else:
            print(f"404! {r.content}")

    sleep(abs(np.random.normal(10,5)))

    return game_id

def ensure_moby_id(moby_id):
    url = f"https://www.mobygames.com/game/{moby_id}"
    try:
        r = requests.get(url, timeout=10)
    except requests.exceptions.Timeout as to:
        print("Moby has timed out")
        return None

    if r.status_code == 200:
        print("found!")
        soup = BeautifulSoup(r.content, "html.parser")
        for s in soup.find_all("title"):
            print(s)
        return True
    else:
        print(r.status_code)
        print(f"{moby_id} not found on Moby at {url}")
        return False


def select_games(all_games, matcher):
    print(f"Matching games names with '{matcher}'")
    selected = dict()
    for key, game in all_games.items():
        if matcher == "*" or (matcher == "no_id" and game.game_id is None) or (matcher.lower() in str(Path(game.dsk_path) or "").lower()):
            selected[key] = game
            #print(f"matched {key} {game.name} {game.dsk_path}")
    print(f"Matched {len(selected)} games")
    return selected



def read_wozaday(all_games):
    for wozdir in glob.glob(str(WOZ_DIR / "*")):
        try:
            metadata = json.load(open(Path(wozdir) / 'metadata.json'))
            name = metadata['title'].replace("(woz-a-day collection)","").replace("800K","").strip()

            for wozfile in glob.glob(str(Path(wozdir).absolute()/"*.woz")):
                with open(wozfile, mode='rb') as filein:
                    woz = filein.read()

                    if len(woz) >= 12:
                        magic, _, crc = struct.unpack("<4s4sI",woz[:12])

                        if wozfile not in all_games:
                            print(f"Adding: {name} {magic} {crc:04X} {wozfile}")
                            gd = GameData(wozfile)
                            print(f"{gd.checksum:08X}")
                            gd.name = name
                            all_games[wozfile] = gd
                        else:
                            pass
                    else:
                        print(f"Bad WOZ ??? {wozfile}")
        except KeyboardInterrupt:
            break
        except Exception as ex:
            print(f"Error {ex}")

print("Reading words")
words = set()
with open("20k.txt","r") as fin:
    for word in fin.readlines():
        word = word.strip()
        if True or not word[0].isupper():
            words.add(word.lower())
with open("words.txt","r") as fin:
    for word in fin.readlines():
        word = word.strip()
        if not re.match(r".*([0-9]+).*", word[0]):
            words.add(word.lower())
with open("special_words.txt","r") as fin:
    for word in fin.readlines():
        word = word.strip()
        if True or not word[0].isupper():
            words.add(word.lower())
print(f"Done reading {len(words)} words")


with open("moby_api_key.txt","r") as fin:
    MOBY_API_KEY=fin.readline().strip()

GAME_DATABASE_FILE="game_database.pickle"
if not os.path.exists(GAME_DATABASE_FILE):
    all_games = dict()
else:
    print(f"Loading game database at {GAME_DATABASE_FILE}")
    with open(GAME_DATABASE_FILE, "rb") as pin:
        all_games = pickle.load(pin)


# print(split_long_name("awacs_battleatsea_journeytothecenteroftheearth_palaceinthunderland_specialsampler".replace("_",""), words))
# exit()


all_games2 = {}
for k, gd in all_games.items():
    if "file_based" not in str(gd.dsk_path):
        all_games2[k] = gd
    if not hasattr( gd, "id_retrieve_date"):
        gd.id_retrieve_date = None

print(len(all_games), len(all_games2))
all_games = all_games2

print(f"{len(all_games)} games in stock")

all_games_by_checksum = {}
for k, gd in all_games.items():
    all_games_by_checksum[gd.checksum] = gd

game_groups = dict()
for g in all_games.values():
    if g.game_id in (-1, None) and "file_based" not in str(g.dsk_path):
        if g.name not in game_groups:
            game_groups[g.name] = []
        game_groups[g.name].append(g)

print(f"{len(game_groups)} group names")


cli_parser = argparse.ArgumentParser()
cli_parser.add_argument("--force-ids", nargs=2)
cli_parser.add_argument("--show",action="store_true", help="Show the current database")
cli_parser.add_argument("--to-rust",action="store_true")
cli_parser.add_argument("--dl-pictures", action="store", const="-1", type=int, nargs="?")
cli_parser.add_argument("--collect-disks")
cli_parser.add_argument("--load-ids",action="store",nargs="?",const="*",help="Load MobyGames ID using the current 'name'")
cli_parser.add_argument("--analyse-names",action="store",nargs="?",const="*", help="Reanalyse names selected by matcher or *")
cli_parser.add_argument("--analyse-names2",action="store",nargs="?",const="*", help="Reanalyse names selected by matcher or *")
cli_parser.add_argument("--zap",action="store_true")
cli_parser.add_argument("--match",action="store",nargs=1,help="match names")
cli_parser.add_argument("--grab-list",action="store_true",help=f"Grab list of all games from MobyGames and put them in {MOBY_NAMES}")
cli_parser.add_argument("--grab-wozaday",action="store_true",help=f"Read WOZ's downloaded from the WozADay collection")
cli_parser.add_argument("--unidentified",action="store_true",help="List of unidentified games")
cli_parser.add_argument("--apply",action="store_true",help="Apply fixes if set.")


cli_args = cli_parser.parse_args()

if cli_args.dl_pictures:
    print("download")
    moby_id = cli_args.dl_pictures

    for gd in all_games.values():
        gd: GameData

        if gd.image is not True and gd.game_id is not None and ((moby_id == -1 and int(gd.game_id) >= 0) or (int(gd.game_id) == moby_id)):
            print(gd)
            nb_exceptions = 0

            if gd.image == None:
                try:
                    r = requests.get(f"https://api.mobygames.com/v1/games/{gd.game_id}?api_key={MOBY_API_KEY}")
                    print(r)
                    if r.status_code == 200:
                        r = r.json()
                        image_url = r["sample_cover"]["thumbnail_image"]
                        extension = image_url.split(".")[-1]
                        response = requests.get(image_url, stream=True)
                        img = Image.open(BytesIO(response.content))
                        img.save(f'moby_{gd.game_id}.{img.format}')
                        gd.image = True

                        with open(GAME_DATABASE_FILE, "wb") as pout:
                            pickle.dump(all_games, pout)

                        if moby_id == -1:
                            sleep(5)

                    elif r.status_code == 404:
                        print("No image")
                        gd.image = False

                except Exception as ex:
                    nb_exceptions += 1
                    print(ex)
                    print(r)
                    if nb_exceptions > 3:
                        break


    exit()

if cli_args.to_rust:
    RUST_DB=Path("../data/moby_keys.bin")
    print(f"Generating rust database in {RUST_DB}")
    with open(RUST_DB,"wb") as fout:
        for gd in all_games.values():
            gd: GameData
            if gd.game_id is not None:
                gid = int(gd.game_id)
                if gid >= 0:
                    fout.write(gd.checksum.to_bytes(4,"little"))
                    fout.write(int(gd.game_id).to_bytes(4,"little"))
    exit()

if cli_args.show:
    show_all_games(all_games)
    print(f"{len(all_games)} games")
    exit()

if cli_args.analyse_names:

    matcher = cli_args.analyse_names
    print(f"Matching against: {matcher}")

    for key, game in all_games.items():
        if matcher == "*" or (matcher == "no_id" and game.game_id is None) or matcher in str(game.dsk_path or "").lower():

            if True or matcher != "*":
                print(f"Matched {game.dsk_path}")

            current_name = game.name
            game.figure_real_name(words)
            new_name = game.name
            if new_name.strip():
                if current_name != new_name:
                    print(f"{key[:50]: <50} {game.name} (was: {current_name})")
                    game.clear_game_id()
            else:
                print(f"{game.dsk_path} has blank name ?")

    print(f"Saving {GAME_DATABASE_FILE}")
    with open(GAME_DATABASE_FILE, "wb") as pout:
        pickle.dump(all_games, pout)

    exit()

def flatten(xss):
    return [x for xs in xss for x in xs]

def canonize_curated_name(name):
    # Curated names are not always that curated
    name = name.replace("'N"," n ")
    name = name.lower()
    name = name.replace("'s"," ")
    name = name.replace("_"," ").replace("a2_","").replace(":"," ").replace("!"," ")
    name = name.replace(","," ")
    name = name.replace("-"," ")
    name = name.replace("g.i.","gi")
    name = name.strip()
    return name


if cli_args.load_ids:
    matcher = cli_args.load_ids
    matched_games = select_games(all_games, matcher)
    cache = dict()

    try:
        for key, gd in matched_games.items():
            if gd.game_id is None:
                name = gd.name
                if name not in cache:
                    game_id = find_moby_game_id(name)
                    cache[name] = game_id            
                else:
                    game_id = cache[name]

                if game_id is not None:
                    print(f"Game id found at moby {game_id} for '{name}'")
                    gd.set_game_id(game_id)            
                else:
                    print(f"Moby doesn't know [{name}] on Apple ][")
    except KeyboardInterrupt as ex:
        print("Quitting")
    except Exception as ex:
        print(ex)



if cli_args.analyse_names2:

    matcher = cli_args.analyse_names2
    print(f"Matching against: {matcher}")

    print("Building vocabulary out of MobyGames")
    MOBY_NAMES_ID = dict()
    with open(MOBY_NAMES) as fin:
        for line in fin.readlines():
            title, moby_id = line.split("\t")
            MOBY_NAMES_ID[title] = int(moby_id)
    print(len(MOBY_NAMES_ID))

    canonized_moby_names = {
        moby_id: canonize_curated_name(fn).split()
         for fn,moby_id in MOBY_NAMES_ID.items()
    }

    moby_names = [canonize_curated_name(fn).split() for fn in MOBY_NAMES_ID.keys()]

    print("Building vocabulary out of archive.org")
    with open("archive_org.txt") as fin:
        list_of_names = moby_names + [canonize_curated_name(fn).split() for fn in fin.readlines()]

    print(f"{len(list_of_names)} names to analyze")
    all_words = set()
    for splitted_name in list_of_names:
        words = splitted_name
        words = flatten([w.split("-") for w in words])
        all_words.update(words)

    print(f"Dictionary is {len(all_words)} words")

    all_words_by_size = list(reversed(sorted(all_words, key=lambda n: (len(n),n))))
    all_words_by_size = [w for w in all_words_by_size if len(w) >= 2]
    #print(all_words_by_size)

    nb_fails = 0
    matched_games = select_games(all_games, matcher)
    for key, game in matched_games.items():
        # name = canonize_name(Path(game.dsk_path).name, words)
        original_name = non_semantic_canonize( Path(game.dsk_path).name)

        name = original_name
        name_repl = name
        success= False
        for word in all_words_by_size:
            if word in name:
                name = name.replace(word," ").strip()
                name_repl = name_repl.replace(word, f"_{word.upper()}_")
                if not name:
                    success = True
                    break

        # Try against the name known to MobyGames
        best_name = None
        best_game_id = None
        best_matching_chars= 0
        loglines = []
        for gid, moby_name in canonized_moby_names.items():
            matched_words = []
            matching_chars = 0
            name = original_name
            for word in reversed(sorted(moby_name,key=lambda n:len(n))) :
                if word in name:
                    matched_words.append(word)
                    name = name.replace(word," ").strip()
                    matching_chars += len(word)
                    if not name:
                        break

            if matching_chars > best_matching_chars:
                best_matching_chars = matching_chars
                best_matched_words = matched_words
                best_name = moby_name
                best_game_id = gid
                loglines.append(f"\tbest: {best_matching_chars}, {best_name}, matched words:{matched_words}, moby name:{moby_name}")

        if best_matching_chars >= 0.8*len(original_name) or len(best_matched_words) >= len(set(best_name)):
            new_name = ' '.join(best_name)
            print(f"GOOD {original_name} -> {new_name} ")
            if cli_args.apply:
                if game.game_id is None or int(game.game_id) != best_game_id or game.name != original_name:
                    print(f"FIXED {game.game_id} -> {best_game_id} | {game.dsk_path}")
                    game.game_id = best_game_id
                    game.name = new_name

        elif best_matching_chars > 0:
            #continue

            for l in loglines:
                print(l)
            print(f"FAIL {original_name} -> best moby: {' '.join(best_name or [])} -> repl:{name_repl} -> score: {best_matching_chars:.1f} < {0.8*len(original_name):.1f}")
            nb_fails += 1
        else:
            print(f"EPIC FAIL {original_name}")
            nb_fails += 1

    print(f"failed {nb_fails} out of {len(matched_games)}. Success rate {(len(matched_games)-nb_fails)/len(matched_games)*100:.1f}%")


if cli_args.zap:

    for key, game in all_games.items():
        game.image = None
        game.game_id = None

    print(f"Saving {GAME_DATABASE_FILE}")
    with open(GAME_DATABASE_FILE, "wb") as pout:
        pickle.dump(all_games, pout)

    exit()


if cli_args.force_ids:
    forced_id = cli_args.force_ids[0]
    assert int(forced_id) > 0, "I expect a moby ID !"

    if ensure_moby_id(forced_id) is True:

        all_games_dsk = sorted([p for p in Path(cli_args.force_ids[1]).glob("**/*")
                                if p.suffix.lower() in {".dsk", ".woz"}])

        if not all_games_dsk:
            print(f"Nothing found at {cli_args.force_ids[1]}")
            exit()

        for ndx_dsk, dsk_path in enumerate(all_games_dsk):
            gd = GameData(dsk_path)
            gd.figure_real_name(words)

            if dsk_path.name in all_games and gd.checksum == all_games[dsk_path.name].checksum:
                print(f"Forcing MobyID {forced_id} onto {gd.name} checksum:{gd.checksum:08X} current moby id:{all_games_by_checksum[gd.checksum].game_id}")
                all_games_by_checksum[gd.checksum].set_game_id(forced_id)
            elif dsk_path.name in all_games and gd.checksum != all_games[dsk_path.name].checksum:
                print(f"I know {gd.name} but with a different checksum")
            else:
                print(f"I don't know {gd.name} at {dsk_path.name}.")

        print(f"Saving {GAME_DATABASE_FILE}")
        with open(GAME_DATABASE_FILE, "wb") as pout:
            pickle.dump(all_games, pout)

    exit()


if cli_args.collect_disks:

    already_seen_names = set()
    all_games_dsk = sorted([p for p in Path(cli_args.collect_disks).glob("**/*")
                            if p.suffix.lower() in {".dsk", ".woz"}])

    try:
        games_done = 0
        non_file = 0
        with_id = 0
        for ndx_dsk, dsk_path in enumerate(all_games_dsk):
            gd = GameData(dsk_path)
            gd.figure_real_name(words)

            # if gd.name in already_seen_names:
            #     print(f"I already checked {gd.name}")
            #     continue
            # else:
            #     already_seen_names.add(gd.name)

            if dsk_path.name not in all_games or gd.name != all_games[dsk_path.name].name or gd.checksum != all_games[dsk_path.name].checksum:
                all_games[dsk_path.name] = gd
                print(f"Collected {dsk_path.name} with name {gd.name} --> {gd.checksum:08X}")
            else:
                print(f"I know {dsk_path.name} with name '{gd.name}' --> {gd.checksum:08X}")
                gd = all_games[dsk_path.name]

            if cli_args.load_ids: # and gd.game_id is None:
                name = gd.name
                game_id = find_moby_game_id(name)
                if game_id is not None:
                    print(f"Game id found at moby {game_id} for '{name}'")
                    if name in game_groups:
                        for ggd in game_groups[name]:
                            ggd.set_game_id(game_id)
                    else:
                        gd.set_game_id(game_id)
                    sleep(10)
                else:
                    print(f"Moby doesn't know [{name}] on Apple ][")

                games_done += 1
            else:
                if all_games[dsk_path.name].game_id is not None:
                    with_id += 1
    except KeyboardInterrupt as ex:
        print("Quitting")
    finally:
        print(f"{games_done} games done. Saving game database")

        with open(GAME_DATABASE_FILE, "wb") as pout:
            pickle.dump(all_games, pout)

    exit()

if cli_args.grab_wozaday:
    try:
        read_wozaday(all_games)
    except KeyboardInterrupt as ex:
        print("Quitting")
    except Exception as ex:
        print(ex)


if cli_args.grab_list:
    names = []
    RE_GAME_ID_IN_HREF = re.compile("https://www.mobygames.com/game/([0-9]+)")

    for year in range(1977,2023):
        url = f"https://www.mobygames.com/platform/apple2/year:{year}/"
        r = requests.get(url, timeout=10)

        while True:

            if r.status_code != 200:
                print("ouch")
                exit()

            soup = BeautifulSoup(r.content, "html.parser")

            for s in soup.find_all("a", href=re.compile("www.mobygames.com/game")):
                print(s.attrs["href"])
                game_id = RE_GAME_ID_IN_HREF.match(s.attrs["href"]).groups()[0]
                names.append( (s.string, game_id) )

            next_page_link = soup.find_all("a", href=re.compile("page:"), string=re.compile("Next"))
            print(next_page_link)
            sleep(2)
            if next_page_link:
                next_link = next_page_link[0].attrs['href']
                print(next_link)
                r = requests.get(f"https://www.mobygames.com/{next_link}", timeout=10)
            else:
                break

    with open(MOBY_NAMES,"w") as fout:
        for name in sorted(names):
            fout.write(f"{name[0]}\t{name[1]}\n")

if cli_args.unidentified:
    for gd in all_games.values():
        if gd.game_id is None:
            print(f"{gd.name:<80}\t{gd.dsk_path}")
    exit()

if cli_args.match:
    all_games_by_names = dict()
    for gd in all_games.values():
        all_games_by_names[gd.name] = gd

    # https://archive.org/advancedsearch.php#raw
    woz_names = set()
    with open(cli_args.match[0]) as fin:
        list_of_names = [fn.strip().replace("_"," ").replace("a2_","").lower() for fn in fin.readlines()]
        woz_names = set([canonize_name(fn, words) for fn in list_of_names])
        print(f"{len(list_of_names)} names to analyze")

        # Make a vocabulary

        all_words = set()
        for name in list_of_names:
            words = name.split()
            all_words.update(words)

        all_words = list(sorted(all_words, key=lambda n:len(n)))
        print(all_words)



        exit()


        with open("canonized.txt","w") as fout:
            for cn in sorted(woz_names):
                fout.write(f"{cn}\n")

        matches = 0
        for name in sorted(all_games_by_names.keys()):
            cname = canonize_name(name, words)
            if cname in woz_names:
                print(f"{cname}")
                matches += 1
            else:
                print(f"FAILED {cname}")

        print(f"{matches} matches")


if cli_args.apply:
    print(f"Saving {GAME_DATABASE_FILE}")
    with open(GAME_DATABASE_FILE, "wb") as pout:
        pickle.dump(all_games, pout)

exit()

try:
    names_done = 0
    non_file = 0
    with_id = 0
    import random

    # First gather all games that have the same name (since they will
    # ultimately lead to the same Moby ID)

    names_to_do = []
    names_dones = set()
    for gd in reversed(sorted(all_games.values(), key=lambda g:g.id_retrieve_date or datetime(1999,1,1))):
        if gd.game_id is None:
            names_to_do.append(gd.name)
        else:
            names_dones.add(gd.name)

    print(f"{len(names_to_do)} names to do; {len(names_dones)} already done")


    #for name in random.sample(list(names_to_do), len(names_to_do)):
    for name in names_to_do:
        names_done += 1

        game_id = find_moby_game_id(name)
        if game_id is not None:
            print(f"Game id found at moby {game_id} for '{name}'")
            for gd in game_groups[name]:
                gd.set_game_id(game_id)
            sleep(5 + random.random()*10)
        else:
            print(f"Moby doesn't know [{name}] on Apple ][")

        if names_done % 10 == 0:
            with open(GAME_DATABASE_FILE, "wb") as pout:
                pickle.dump(all_games, pout)

        # sleep(2)
        # # More precise, but doesn't include thumbnails...
        # #url = f'https://api.mobygames.com/v1/games/{game_id}/platforms/31?api_key={MOBY_API_KEY}'
        # url = f'https://api.mobygames.com/v1/games/{game_id}?api_key={MOBY_API_KEY}'
        # # This should work but doesn't. Signaled on the discord server.
        # #url = f'https://api.mobygames.com/v1/games/?api_key={MOBY_API_KEY}&id={game_id}'
        # print(url)
        # r = requests.get(url)
        # print(r.status_code)
        # r = r.json()
        # pprint(r['sample_cover'])
        # exit()

except KeyboardInterrupt as ex:
    print("Quitting")
except Exception as error:
    print(error)
finally:

    print(f"{names_done} names done. Saving game database")

    #show_all_games(all_games)

    with open(GAME_DATABASE_FILE, "wb") as pout:
        pickle.dump(all_games, pout)
