import requests
from bs4 import BeautifulSoup
import string
from tqdm import tqdm

all_names=[]
for letter in tqdm(list(string.ascii_lowercase) + ["09"]):
    r = requests.get(f"https://www.uvlist.net/platforms/games-list/21/{letter}",
                    headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"})
    if r.status_code == 200:
        html = BeautifulSoup(r.text, "lxml")
        names=[tag.text for tag in  html.find_all("span", itemprop="name")]
        urls = [tag['href'] for tag in html.find_all("a", itemprop="url")]
        #print(names)
        all_names.extend( zip(names, urls))
    else:
        print("error")

with open("universal.txt","w",encoding="utf-8") as fout:
    for n in all_names:
        name, url = n
        fout.write(f"{name}\t{url}\n")

        


