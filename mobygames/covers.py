import re
import shutil
import requests
from bs4 import BeautifulSoup
import string
from tqdm import tqdm
from posixpath import basename
import json
from pathlib import Path
from time import sleep
import numpy as np

DB_FILE= Path("../data/covers_url.json")

db = dict()
if DB_FILE.exists():
    with open("../data/covers_url.json") as db_out:
        js = db_out.read()
        if js:
            print(js)
            db = json.loads(js)
            print(db)




with open("moby_names.txt") as moby_names:

    all_games = moby_names.readlines()
    for line_ndx, line in enumerate(all_games):

        name, moby_id = line.strip().split("\t")
        moby_id = int(moby_id)
        print(name)

        if str(moby_id) in db:
            continue

        # 819 bard's tale
        # 8127 choplifter
        r = requests.get(f"https://www.mobygames.com/game/{moby_id}",
                         headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"})

        if r.status_code == 200:
            html = BeautifulSoup(r.text, "lxml")
            #   <a class="nav-link" href="/game/8127/choplifter/covers/">Covers</a>
            urls = html.find_all(lambda tag: tag.name == "a" and tag.text == "Covers")
            print(urls[0]["href"])
            r = requests.get(f"https://www.mobygames.com{urls[0]['href']}")
            html = BeautifulSoup(r.text, "lxml")
            urls = html.find_all(lambda tag: tag.name == "a" and tag.text == "Apple II" and tag.parent.name == "h2")

            if not urls:
                if "any covers for this game" in r.text:
                    print("Moby has nothing")
                    db[int(moby_id)] = ""
                    continue
                else:
                    url = html.find("figure").find("a")["href"]
            else:
                url = urls[0].parent.parent.find("figure").find("a")["href"]



            # The actual image link
            r = requests.get(url)
            html = BeautifulSoup(r.text, "lxml")
            url = html.find("figure").find("img")["src"]
            print(url)
            # print(url)
            # print(basename(url))
            # response = requests.get(url, stream=True)
            # with open(basename(url), 'wb') as fout:
            #     response.raw.decode_content = True
            #     shutil.copyfileobj(response.raw, fout)

            db[int(moby_id)] = url

            print(f"Progress: {len(db)}/{line_ndx+1}/{len(all_games)}")

            with open("../data/covers_url.json","w") as db_out:
                db_out.write(json.dumps(db, indent=4))

            sleep(abs(np.random.normal(10,5)))
        else:
            print("Error")
