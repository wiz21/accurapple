import json
from pprint import pprint
from typing import List, Set
from tqdm import tqdm
import re
from enum import IntEnum
from pathlib import Path
import more_itertools as mit
from Levenshtein import distance
import unicodedata
from datetime import datetime

DEBUG_WORD="?"
#DEBUG_WORD="down"

REPL_CURATED=[
    (r"rev\. 1", " "),
    (r"rev\. 2", " "),
    (r"rev\. 3", " "),
    (r"rev\. 4", " "),
    (r"(^| |_)d'", " "),
    (r"v2\.01" , " "),
    (r" v2\.1", " "),
    (r"2\.1" , " "),
    ("01 dec 81"," "),
    ("01 jan 82"," "),
    ("05 sep 81"," "),
    ("1984 10 26"," "),
    ("v1985-12-02", " "),
    ("061984"," "),
    ("1982"," "),
    ("1985"," "),
    (" vol.", " volume"),
    ("3-d", "3d"),
    ("exodus: ultima iii", "ultima iii"),
    ("dunjonquest:",""),
    ("carribean", "caribbean"),
    ("the destiny knight", ""), # Bard's Tale dedup
    ("thief of fate", ""),
    ("tales of the unknown" , ""),
    ("the dictator strikes back" , ""),
    ("arcticfox", "arctic fox"),
    ("beach-head", "beach head"),
    ("burn-out" , "burn out"),
    ("and the islands of fear", ""),
    ("zytrowar", "zytro war"),
    (r"earth orbit station.*", "earth orbit station"),
    ("and other greek myths", ""),
    ("eos: ", ""),
    ("amazin'", "amazing"),
    ("f-15", "f15"),
    (r"\$100,000" , "100000"),
    ("airsim-1" , "airsim #1"),
    ("airsim 1" , "airsim #1"),
    ("airsim-3" , "airsim #3"),
    ("airsim 3" , "airsim #3"),
    ("acey-deucey" , "acey deucey"),
    ("acey-deucy" , "acey deucey"),
    ("acey-ducey" , "acey deucey"),
    ("ack-ack" , "ack ack"),
    ("alge-blaster" , "alge blaster"),
    ("apple-oids" , "apple oids"),
    ("b i z a r r o" , "bizarro"),
    ("bridge-it", "bridge it"),
    ("bubble-head", "bubble head"),
    ("eggs-it","eggs it"),
    ("1941-1945" , "1941 1945"),
    ("123s" , "1-2-3s"),
    ("level c" , " "),
    ("level d" , " "),
    ("level e" , " "),
    ("explorer 2" , "explorer #2"),
    ("//e" , " "),
    ("bird-brain", "bird brain"),
    ("blackjack" , "black jack"),
    ("apshai trilogy", "apshai"),
    ("dungeonquest",""),
    ("bejeweled 2","bejeweled #2"),
    (r"1941-1945: Fleet Carrier.*", ""),
]

for ndx in range(len(REPL_CURATED)):
    regex, repl = REPL_CURATED[ndx]
    REPL_CURATED[ndx] = (re.compile(regex), repl)



REPL_DSK = [
    ("'n "," and "),
    ("\"n\""," and "),
    ("&","and"),
    (" n "," and "),
    (" 'n' "," and "),
    ("_n_"," and "),
    ("a2fs1" ," "),
    ("a2fs2" ," "),
    ("tjs -",""),
    (r"earth ?orbit ?station.*", "earth orbit station"),
    (r"electronic ?arts", ""),
    (r"et_comes_back", "e.t. comes back"),
    ("eliza 3.0", "eliza"),
    ("exodus ultima iii", "ultima iii"),
    ("excalibur_s.*","excalibur quest"),
    ("1_world_builders", ""),
    ("empire_boot", "empire"),
    ("empire_map", "empire"),
    ("empire_program", "empire"),
    ("b24","b-24 combat simulator"),
    (r"archon_i$","archon #1"),
    (r"archon2$","archon #2"),
    (r"frogger2$","frogger #2"),
    (r"chess7$","chess 7.0"),
    (r"chess 7$","chess 7.0"),
    ("chess 70","chess 7.0"),
    ("chess_v7","chess 7.0"),
    ("chez21","chez 21"),
    ("citee ","cite "),
    ("ball_blazer", "ballblazer"),
    ("carribean", "caribbean"),
    ("face maker", "facemaker"),
    ("fantavision - data","fantavision"),
    ("r97-851218",""),
    ("r29-860820",""),
    ("r97 serial 851218 interpreter K", ""),
    ("1984 infocom r47", ""),
    ("1984 infocom r56", ""),
    ("1983 infocom r8", ""),
    ("1982 infocom r15", ""),
    ("1983 infocom r20", ""),
    ("1983 infocom r10", ""),
    ("1983 infocom r26 re rip", ""),
    ("19xx box office", ""),
    (r"v2\.01" , " "),
    (r" v2\.1", " "),
    (r"2\.1" , " "),
    ("v5",""),
    ("v6",""),
    ("1.90",""),
    ("belowtheroot","below the root"),
    (" iic", " "),
    (" face 1",""),
    (" face 2",""),
    (" face 3",""),
    (" 2.1",""),
    ("a2bejwld","bejeweld"),
    (r"_err$",""),
    ("err - ",""),
    (r"_1a$",""),
    (r"_1b$",""),
    (r"_2c$",""),
    (r"_2d$",""),
    (r"_3e$",""),
    (r"_3f$",""),
    (r"_4g$",""),
    (r"_4h$",""),
    (r"_5i$",""),
    (r"_5j$",""),
    (r"_6k$",""),
    (r"_6l$",""),
    (" forty" ," 40 "),
    ("_triad" ," "),
    ("journey disk",""),
    ("boot disk",""),
    ("game disk",""),
    (r"ancient\.art","ancient art of war"),
    (r"[^d] 007" ,"james bond 007"),
    (r"^bronzedragon.*","bronze dragon"),
    ("cardsharkii" ,"card shark ii"),
    ("l'" ," "),
    ("f15se" ,"f15 strike eagle"),
    ("f-15 strike eagle" ,"f15 strike eagle"),
    ("fligh " ,"flight "),
    ("flightsimulator2","flight simulator #2"),
    ("fs ii","flight simulator #2"),
    (r"^apshai", "temple of apshai"),
    ("gi ","g.i. "),
    (r"v1\.3" ," "),
    ("impossiblemission2","impossible mission #2"),
    ("infiltrator2" ,"infiltrator #2"),
    ("infocomic" ," "),
    ("err-"," "),
    ("good_1",""),
    ("good_2",""),
    ("dapshai"," apshai"),
    ("bopnwrestle","bop and wrestle"),
    ("divers",""),
    ("kumungas", "kamungas"),
    ("upndown", "up and down"),
    ("upanddown", "up and down"),
    (" vol.", " volume"),
]

for ndx in range(len(REPL_DSK)):
    regex, repl = REPL_DSK[ndx]
    REPL_DSK[ndx] = (re.compile(regex), repl)


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

RE_VERSION = re.compile("#[0-9]+")

class MediaType(IntEnum):
    DSK = 1
    WOZ = 2

class DiskImage:
    def __init__(self, type_img: MediaType, crc:int, path: Path = None, url:str = None):
        self._type = type_img
        if path is not None:
            self._path = path.name
        else:
            self._path = None
        self._url = url
        self._crc = crc

    @property
    def crc(self):
        return self._crc

    @property
    def name(self) -> str:
        if self._path:
            return self._path
        if self._url:
            return self._url
        return "?name?"

    def to_dict(self):
        d = {}
        d["type"] = self._type.name
        d["crc"] = f"0x{self._crc:08X}"
        if self._path:
            d["name"] = self._path
        if self._url:
            d["url"] = self._url
        return d

    @classmethod
    def from_dict(cls, d):
        disk_image = DiskImage( MediaType[d["type"]], int(d["crc"], 0))
        disk_image._path = d.get("name")
        disk_image._url = d.get("url")
        return disk_image


    def __str__(self):
        if self._path:
            return self._path
        else:
            return "/"

class Game:
    def __init__(self, name, canonized_name):
        self.disk_images: List[DiskImage] = []
        self._curated_name = None
        if isinstance(name, str):
            self._known_names = set([name])
        elif hasattr(name, "__iter__"):
            self._known_names = set(name)
        else:
            self._known_names = set()

        self._moby_id = None
        self._igdb_id = None
        self._universal_id = None
        self._canonized_name = canonized_name

    def images_names(self):
        return [dsk.name for dsk in self.disk_images]

    def name(self):
        if self._known_names:
            return next(reversed(sorted(self._known_names, key=lambda n:len(n))))
        else:
            return ""

    def canonized_name(self):
        return self._canonized_name

    @property
    def moby_id(self):
        return self._moby_id

    def set_moby_id(self, mid):
        self._moby_id = mid

    def set_igdb_id(self, mid):
        self._igdb_id = mid

    def set_universal_id(self, mid):
        self._universal_id = mid

    def add_disk_image(self, media: DiskImage):
        self.disk_images.append(media)

    def merge(self, other: "Game"):
        self._known_names.update(other._known_names)
        self.disk_images.extend(other.disk_images)
        if self._moby_id is None:
            self._moby_id = other._moby_id
        if self._universal_id is None:
            self._universal_id = other._universal_id
        if self._igdb_id is None:
            self._igdb_id = other._igdb_id


    def to_dict(self):
        d= {
            "names": list(self._known_names),
            "canonized_name": self._canonized_name,
        }
        if self._moby_id is not None:
            d["moby_id"] = self._moby_id
        if self._igdb_id is not None:
            d["igdb_id"] = self._igdb_id
        if self._universal_id is not None:
            d["universal_id"] = self._universal_id
        if self.disk_images:
            d["disk_images"] = [img.to_dict() for img in self.disk_images]
        return d

    @classmethod
    def from_dict(cls, d):
        game = Game( d["names"], d["canonized_name"])
        game._moby_id = d.get("moby_id", None)
        game._igdb_id = d.get("igdb_id", None)
        game._universal_id = d.get("universal_id", None)
        if "disk_images" in d:
            game.disk_images = [DiskImage.from_dict(dsk_img) for dsk_img in d["disk_images"]]
        return game

    def __str__(self):
        n = ", ".join(self._known_names)
        moby = self._moby_id or ""

        return f"{moby} {n}"

    def __repr__(self):
        return str(self)

def canonize_roman_numbers(name):
    for ndx, n in enumerate(['i','ii','iii','iv','v','vi','vii','viii']):
        name = re.sub(f' {n}([ \\:,]|$)', f' #{ndx+1} ',name)
    return name

def canonize_cut_end(name: str):
    m = RE_VERSION.search(name)
    if m:
        name = name[0:m.end()]

    ndx = name.find(":")
    if ndx > 0:
        name = name[:ndx]

    return name

def split_in_words(name, words):
    split_names = []

    # First split on existing on spaces to make words
    # shorter (and help performances)
    for n in name.split(" "):
        if words:
            split_names.extend(split_long_name(n, words))
        else:
            split_names.append(n)

    return split_names


def canonize_curated_name(orig_name, debug=False):
    name = remove_accents(orig_name.lower())

    for w, repl in REPL_CURATED:
        name = re.sub(w, repl, name)


    # Removals
    name = name.replace("(included game)"," ")
    name = re.sub(r'800k', " ", name)
    name = re.sub(r'r[0-9][0-9].*', " ", name)
    name = re.sub(r'r[0-9]$', " ", name)
    name = re.sub(r'v[ 0-9]+(\.[0-9])', " ", name)
    name = re.sub(r'!', " ", name)
    name = re.sub(r'(^| )the ', " ", name)
    name = re.sub(r'^a ', " ", name)
    name = re.sub(r' of ', " ", name)
    name = name.replace("·"," ")
    name = name.replace("$","")
    name = name.replace("/"," ")
    name = name.replace("?"," ")
    name = name.replace("..."," ")
    name = name.replace(","," ")
    name = name.replace(" - ", " ")
    name = name.replace(" 's","'s")
    name = name.replace("♦"," ")
    name = name.replace("side b"," ")
    name = name.replace("a2 fs1"," ")
    name = name.replace(" 1.5"," ")
    name = re.sub(r'apple *((ii)|(//))e?( |$)', " ", name)

    name = name.replace("a real american hero"," ") # dedup GIJoe
    name = name.replace("threeedeep"," ") # dedup frogger

    name = name.replace("romancing the throne"," ") # dedup king's quest
    name = name.replace("to heir is human"," ") # dedup king's quest
    name = name.replace("the perils of rosella"," ") # dedup king's quest
    name = name.replace("quest for the crown"," ") # dedup king's quest

    name = name.replace("the wizard of frobozz"," ") # dedup zork
    name = name.replace("and the islands of fear"," ") # dedup captain goodnight
    name = name.replace("and the soldiers of the future"," ") # dedup captain power




    # Replacements
    name = name.replace("vs.","vs")
    name = name.replace("st.","street")
    name = name.replace("221 b","221b")
    name = name.replace("$100,000","100000")
    name = name.replace("2400 ad","2400 a.d.")
    name = name.replace(" 'o "," o ")
    name = name.replace(" 'r "," r ")
    name = name.replace(" o' "," o ")
    name = name.replace(" 'em"," them")
    name = name.replace("3-d ","3d ")
    name = name.replace("r77","")
    # name = name.replace("b-1","b1 ")
    # name = name.replace("b-24","b24 ")
    # name = name.replace("f-15","f15 ")
    # name = name.replace("mx-151","mx151")
    # name = name.replace("u-boat","uboat")
    # name = name.replace("j-bird","jbird")
    name = name.replace("j bird","j-bird")
    name = name.replace("Programs-1","Programs")
    name = name.replace("'n "," and ")
    name = name.replace("\"n\""," and ")
    name = name.replace("&","and")
    name = name.replace(" n "," and ")
    name = name.replace(" 'n' "," and ")
    #name = name.replace("1,2,3","123")
    name = name.replace("1 2 3","1-2-3")
    name = name.replace("1 - 2 - 3","1-2-3")
    name = name.replace("tic tac toe","tic-tac-toe")



    # Version numbers
    name = name.replace("colossus chess 4","colossus chess #4")
    name = name.replace("superprintii","super print ii")
    name = name.replace("space raiders 2","space raiders #2")
    name = name.replace("vol. 2","volume 2")
    name = name.replace("vol. 3","volume 3")
    name = name.replace("volume one","volume 1")
    name = name.replace("formula i","formula")
    name = name.replace("formula 1","formula")
    name = name.replace("][","#2")
    name = name.replace("genius 2","genius #2")
    name = name.replace("coveted mirror 2","coveted mirror #2")
    name = name.replace("Computer Golf 2", "Computer Golf #2")
    name = name.replace("$take$", "stakes")

    # Abbreviations
    name = name.replace("i. q. ", "i.q.")
    name = name.replace("phm ", "p.h.m. ")
    name = name.replace("l a ", "l.a. ")
    name = name.replace("g i ", "g.i. ")
    name = name.replace("2041 ad ", "2041 a.d.")
    name = re.sub(r'^ae$', "a.e.", name.strip())

    name = name.replace("dr.", "dr")
    name = name.replace("mr.", "mr")
    name = name.replace("mrs.", "mrs")
    name = name.replace("ms.", "ms")
    name = name.replace("i presume", "presume")


    name = name.replace("M-ss-ng L-nks","Missing Links")
    name = name.replace("apple //","apple 2")
    name = name.replace("apple ii","apple 2")
    for n in ["majesty","david","iggy","israel", "bard", "bc", "yeager", "winfield",
              "fathom", "hound", "eagle", "ernie", "jenny", "king", "lucifer", "mabel",
              "macarthur", "merlin", "mickey", "miner", "ming", "montezuma",
              "wigglesworth", "mummy", "murphy", "oldorf", "oliver", "paul",
              "pandora", "pharaoh", "rocky", "newton", "vohaul", "spy",
              "tawala", "teddy", "bard", "demon", "hitchiker", "world",
              "valentine", "wally", "woolly", "tempest"]:
        name = name.replace(n+"s",n+"'s")
    name = name.replace("s' ", " ")
    # if "king" in name:
    #     print(name)
    name = canonize_roman_numbers(name)
    if debug:
        print(name, " --- ", canonize_cut_end(name), " -- ", orig_name)
    name = canonize_cut_end(name)
    name = re.sub(r'apple ', " ", name)
    name = re.sub(r' +', " ", name).strip()

    name = " ".join(remove_articles(  split_in_words(name, None)))



    #print(f"{orig_name:80}{name}")
    return name

    name = re.sub(r'[ _]f.dsk', ".dsk", name, flags=re.IGNORECASE)
    name = re.sub(r'[ -]?([fs][12])?.dsk', "", name, flags=re.IGNORECASE)
    name = name.replace(".dsk","").replace(".woz","")
    name = re.sub(r'\(.+$','',name.strip().replace("_"," "))
    name = re.sub(r'side (a|b)',"",name,flags=re.IGNORECASE)
    name = name.replace("a2","")

    name = " " + name + " "
    name = name.replace("- boot","").replace("- game","").replace("- back","").replace("- front","")
    name = name.replace(" s1","").replace(" s2","").replace(" s3","").replace(" s4","")
    name = name.replace("-s1","").replace("-s2","").replace("-s3","").replace("-s4","")
    #name = re.sub(r'((D|d)isk ?[12])? ?((s|S)ide ?[12ab])?', "", name, flags=re.IGNORECASE)
    name = name.replace("disk1","").replace("disk2","").replace(" disk","")
    name = name.replace("disk 1","").replace("disk 2","").replace("disk 3","")

    name = re.sub(r'disk [12] side [12]', "", name, flags=re.IGNORECASE)
    name = re.sub(r"([0-9]+)",r" \1 ",name)
    name = name.replace("+"," ")
    name = name.replace("-"," ")
    name = name.replace("[alt]","")
    name = name.replace("cracked","")

    name = name.replace("articfox","arcticfox")
    return name


def split_long_name(name, words: dict):
    debug = False

    if not name:
        return []
    if name in words:
        if debug:
            print(f"short {name} {words[name]}")
        return [words[name]]
    if name == "tothe":
        return ["to","the"]
    if name == "ofthe":
        return ["of","the"]
    if name == "onthe":
        return ["on","the"]
    if name == "bythe":
        return ["by","the"]
    if debug:
        print(f"Splitting {name}")
    #print(list(sorted(words)))
    n = name
    j = 0
    best_len = 0

    length = len(n)
    done = False
    while not done and length >= 2:
        i = 0
        while not done and i <= len(n) - length:
            if debug:
                print(n[i:i+length])
            if n[i:i+length] in words:
                best_len = length
                best_j = i
                best_k = i + length
                done = True
            i += 1
        length -= 1


    # while j < len(n)-2:
    #     for k in reversed(range(j+2,len(n)+1)):
    #         if n[j:k] in words and k-j > best_len:
    #             best_len = k-j
    #             best_j = j
    #             best_k = k
    #             #print("nubest " + n[best_j:best_k])
    #     j += 1

    if best_len > 0:
        #print(n[:best_j], "/", n[best_j:best_k], "/", n[best_k:])
        r = list(filter(len, split_long_name(n[:best_j],words) + [n[best_j:best_k]] + split_long_name(n[best_k:], words)))
        if debug:
            print(r)
        #sleep(5)
        return r
    else:
        return [name]


def non_semantic_canonize(name):
    name = re.sub(r'([a-z])([A-Z])', r'\1 \2', name)
    name = name.lower()

    for old, new in { r"gold_finger": "goldfinger",
                      r"\$100,000" : "100000",
                      r"untouched 2012crack": "",
                      r"^the ": "",
                      r"48k": "",
                      r"disk 3": "",
                      r"j20 -" : "",
                       r"g28" : "",
                        r'_1$': " ",
                        r'_2$': " ",
                        r"h36" : "" }.items():
        name = re.sub(old, new, name)

    name = re.sub(r'[ _]f.dsk', ".dsk", name, flags=re.IGNORECASE)
    name = re.sub(r'[ -]?(f[12])?.dsk', "", name, flags=re.IGNORECASE)
    name = name.replace(".dsk","").replace(".woz","")
    name = re.sub(r'\(.+$','',name.strip().replace("_"," "))
    name = re.sub(r'side (a|b)',"",name,flags=re.IGNORECASE)
    name = name.replace("a2","")

    name = " " + name + " "
    name = name.replace("- boot","").replace("- game","").replace("- back","").replace("- front","")
    name = name.replace(" s1","").replace(" s2","").replace(" s3","").replace(" s4","")
    name = name.replace(".s1","").replace(".s2","").replace(".s3","").replace(".s4","")
    name = name.replace("-s1","").replace("-s2","").replace("-s3","").replace("-s4","")
    name = name.replace("j20 - side a","")

    name = re.sub(r'(D|d)isk ?[12]', " ", name, flags=re.IGNORECASE)
    name = re.sub(r'(s|S)ide ?[12ab]', " ", name, flags=re.IGNORECASE)
    name = name.replace("disk1","").replace("disk2","").replace(" disk","")
    name = name.replace("disk 1","").replace("disk 2","").replace("disk 3","")
    name = re.sub(r'disk [12] side [12]', "", name, flags=re.IGNORECASE)
    name = re.sub(r' [abc] *$', " ", name, flags=re.IGNORECASE)
    #name = re.sub(r"([0-9]+)",r" \1 ",name)
    name = name.replace("+"," ")
    name = name.replace("  "," ").replace("  "," ").strip()
    name = name.replace("-"," ")
    name = name.replace("[alt]","")
    name = name.replace("cracked","")

    name = name.replace("articfox","arcticfox")
    return name

def remove_articles(words):
    # FIXME Not the right place to do it: "d'apshai" is often considered as a single word...
    return list(filter(lambda n: n not in ['the','of',"l'","d'","le","la"], words))


def canonize_dsk_name(original_name, words, debug=False):
    if debug:
        print(f"canonize_dsk_name({original_name}...)")

    name = re.sub(r'([a-z])([A-Z])', r'\1 \2', original_name)
    name = remove_accents(name.lower())
    name = name.replace(".dsk","")

    if debug:
        print(f"canonize_dsk_name--> {name}")

    for w, repl in REPL_DSK:
        name = re.sub(w, repl, name)

    if debug:
        print("canonize_dsk_name >1--->", name)

    name = non_semantic_canonize(name)

    name = re.sub(r"(^| |_)l'"," ",name)
    name = re.sub(r"(^| |_)d'"," ",name)
    name = re.sub(r"(^| |_)le[ _]"," ",name)
    name = re.sub(r"(^| |_)la[ _]"," ",name)

    if debug:
        print("canonize_dsk_name >2--->", name)

    name = re.sub(r"(^| |_)the[ _]"," ",name)
    name = re.sub(r"(^| |_)of[ _]"," ",name)
    name = name.replace("super"," super ")
    name = name.replace("vs"," vs ")
    name = name.replace("-crack","")
    name = name.replace(" ]["," 2")
    name = name.replace(" 2e","")
    name = name.replace(" hr","").replace("etc","")
    name = name.replace("ofthe","")
    name = name.replace("fixed","")
    name = name.replace("cavaliercomputer","")

    if not words:
        name = re.sub(" +"," ",name).strip()
        return name

    if debug:
        print("canonize_dsk_name >3--->", name)

    good_words_list = []
    if True:
        split_names = []

        # First split on existing on spaces to make words
        # shorter (and help performances)
        for n in name.split(" "):
            s = split_long_name(n, words)
            split_names.extend( s)

        if debug:
            print("split>",split_names)

        split_names = remove_articles(split_names)

        if debug:
            print("split>",split_names)

        if True:# or len(name) < 12:

            for name_part in split_names:

                if re.match(r"([0-9]+)", name_part):
                    good_words_list.append(name_part)
                    continue

                best_partition = None
                best_score = None
                for partition in [list(map("".join, x)) for x in mit.partitions(name_part)]:
                    good_words = 0
                    used_chars = 0

                    for word in partition:
                        if len(word) >= 3 and word in words:
                            good_words += 1
                            used_chars += len(word)
                            #print(f"{word} good_words:{good_words} used_chars:{used_chars}")
                        elif word[-1] == 's' and word[:-1] in words: # Plural
                            good_words += 1
                            used_chars += len(word)
                            #print(f"<s> {word} good_words:{good_words} used_chars:{used_chars}")
                        elif len(word) == 1:
                            good_words -= 3
                        else:
                            good_words -= 1

                    if good_words >= 1:
                        score = (len(name_part) - used_chars) * (len(partition)-good_words)
                        if best_score is None or score < best_score:
                            #print(f"score={score} words score:{good_words} used_chars:{used_chars} {perm}")
                            best_partition = partition
                            best_score = score
                        else:
                            #print(f"               bad score={score} words score:{good_words} used_chars:{used_chars} {perm}")
                            pass

                if best_partition is not None:
                    #print(f"BEST {' '.join(best_perm)}")
                    good_words_list.append(' '.join([words.get(w,w) for w in  best_partition]))
                else:
                    good_words_list.append(name_part)


            #exit()
            #print(f"[{name}]")
            #continue

        # print(f"Name is: {name} -> {' '.join(good_words_list)}")
        good_words_list = filter(lambda n: n not in set(['alt','the']) and (len(n) > 1 or re.match("[0-9]",n)), good_words_list)


        name = ' '.join(good_words_list)
        name = canonize_roman_numbers(name)

        if debug:
            print("===>", name )

        name = canonize_cut_end(name)

        if name.endswith(" 1"):
            name = name[0:-2]

        return name



def read_names(f):
    with open(f, encoding="utf-8") as fin:
        r = [l.strip() for l in fin.readlines()]
        print(f"{len(r)} in {f}")
        return r


def canonize_curated_names(names):
    """ This will canonize names that are already somewhat
    "clean". Those are coming, at the moment, from the MobyGames
    web site and the Woz-a-Day collection.
    """

    r = []
    for name in tqdm(names):
        cname = canonize_curated_name(name)
        r.append( (name, cname) )

    # for name, cname in sorted(r, key=lambda n:n[1]):
    #     print(f"{name:80}{cname}")

    return [n[1] for n in r]

def canonize_asimov_names(names, vocabulary):
    r = []
    for name in names:
        cname = canonize_dsk_name( Path(name).name, vocabulary)
        r.append( (name, cname) )

    # for name, cname in sorted(r, key=lambda n:n[1]):
    #     print(f"{name:80}{cname}")

    return [n[1] for n in r]


def names_alike(current_names, new_name):
    words_of_name = list(reversed(sorted(new_name.split(" "),key=lambda n:len(n))))
    #print(words_of_name)

    best_name = []
    best_matched_words = []
    best_matching_chars= 0
    loglines = []

    debug = "bard" in new_name

    for current_name in current_names:
        matched_words = []
        matching_chars = 0

        name = current_name
        #print(name)
        for word in words_of_name:
            #print(f"   {word} in? {name}")
            if word in name:
                matched_words.append(word)
                name = name.replace(word," ").strip()
                matching_chars += len(word)
                if not name:
                    # All words of current_name are matched.
                    break

        if matching_chars > best_matching_chars:
            best_matching_chars = matching_chars
            best_matched_words = matched_words
            best_name = current_name
            loglines.append(f"\tbest: {best_matching_chars}, {best_name}, matched words:{matched_words}, current name:{current_name}")

    # if debug:
    #     print("\n".join(loglines))

    if best_matching_chars >= 0.8*max(len(new_name), len(best_name)) or (len(best_matched_words) > 0 and len(best_matched_words) >= len(set(best_name))):
        #print(f"GOOD {best_name} ~= {new_name} ")
        # if cli_args.apply:
        #     if game.game_id is None or int(game.game_id) != best_game_id or game.name != original_name:
        #         print(f"FIXED {game.game_id} -> {best_game_id} | {game.dsk_path}")
        #         game.game_id = best_game_id
        #         game.name = new_name
        return best_name

    elif best_matching_chars > 0:
        return False

        # for l in loglines:
        #     print(l)
        # print(f"FAIL {new_name} -> best moby: {best_name} -> score: {best_matching_chars:.1f} < {0.8*len(new_name):.1f}")
        #nb_fails += 1
    else:
        #print(f"EPIC FAIL {new_name}")
        #nb_fails += 1
        return False

def make_variant(word):
    return [word, word.replace("'","").replace("-","").replace(".","")]

def make_vocabulary(all_games):
    words = dict()

    # Using the dictionary, we can better parse the DSK names.
    for name in [game.canonized_name() for game in all_games]:
        split = name.split(" ")
        # if list(filter(lambda n: len(n) == 1 and n not in "0123456789", split)) or "10" in split:
        #     print(f"Name split is bizarre : {name} --> {split}")
        #     #exit()

        for word in split:
            # Remove some edge cases
            if word not in ["i.", "e.", "s.", "u."]:
                for variant in make_variant(word):
                    words[variant] = word

    with open("special_words.txt") as sw:
        for word in [w.strip() for w in sw.readlines()]:
            for variant in make_variant(word.lower()):
                words[variant] = word.lower()

    return words

def load_game_database(name) -> List[Game]:
    with open(name, "r", encoding="utf-8") as fin:
        data = json.loads(fin.read())
        all_games: List[Game] = []
        for game in data["games"]:
            g = Game.from_dict(game)
            all_games.append( g)
        return all_games


def merging(all_games, desc="Merging"):
    if DEBUG_WORD != "?":
        all_games = list(filter(lambda game: DEBUG_WORD in game.canonized_name(), all_games))

    print(f"merging {len(all_games)}")

    games_to_merge = set(all_games)

    for game_ref in tqdm(sorted( all_games, key=lambda g: g.canonized_name()), desc=desc):
        if game_ref not in games_to_merge:
            continue

        debug = DEBUG_WORD in game_ref.name().lower()

        if debug:
            print(f"\nAnalysing: {game_ref.name()} / {game_ref.canonized_name()}")

        best_d = 10000
        best_games = []
        for game in games_to_merge:
            debug = DEBUG_WORD in game_ref.name().lower() or DEBUG_WORD in game.name().lower()
            if game != game_ref:
                improved = False
                d = None
                l1 = len(game.canonized_name())
                l2 = len(game_ref.canonized_name())

                if l1 > 0 and l2 > 0:
                    if not (("#" in game_ref.canonized_name()) ^ ("#" in game.canonized_name())):

                        if "#" in game.canonized_name():
                            m1 = RE_VERSION.findall(game.canonized_name())
                            m2 = RE_VERSION.findall(game_ref.canonized_name())
                            if m1[0] != m2[0]:
                                continue

                        if l1 == 0 or l2 == 0:
                            print(game.to_dict())
                            print(game_ref.to_dict())

                        d = distance(game_ref.canonized_name(), game.canonized_name()) / min(l1, l2)
                    else:
                        if '#1' in game.canonized_name() and '#' not in game_ref.canonized_name() or \
                        '#' not in game.canonized_name() and '#1' in game_ref.canonized_name():
                            m1 = game.canonized_name().replace('#1','')
                            m2 = game_ref.canonized_name().replace('#1','')
                            if m1 in m2 or m2 in m1:
                                d = 0
                if d is not None and d < 0.1:
                    if d == best_d:
                        best_games.append(game)
                        improved = True
                    elif d < best_d:
                        best_d = d
                        best_games = [game]
                        improved = True
                    if debug and improved:
                        print(d, [g.name() +"/"+ g.canonized_name() for g in best_games])
        #print(f"Merging {best_d} {str(game_ref)} {best_games}")
        if best_d <= 1:
            for bg in best_games:
                game_ref.merge(bg)
                games_to_merge.remove(bg)

    return games_to_merge

def database_to_rust(all_games):
    RUST_DB=Path("../data/moby_keys.bin")
    print(f"Generating rust database in {RUST_DB}")

    with open(RUST_DB,"wb") as fout:
        for game in all_games:
            if game.moby_id:
                for media in game.disk_images:
                    if media.crc:
                        fout.write(media.crc.to_bytes(4,"little"))
                        fout.write(int(game.moby_id).to_bytes(4,"little"))


all_games: List[Game] = []

# First, read the mostly clean names. We will use them
# to build a dictionary of accepted words
with open("moby_names.txt", encoding="utf-8") as fin:
    names_moby = [l.strip().split("\t") for l in fin.readlines()]

with open("wozaday_list.txt", encoding="utf-8") as fin:
    names_wozaday = list(map( lambda a: (a[0].strip(), a[1]), [l.split("\t") for l in fin.readlines()]))

with open("softwarelibrary_apple_woz_games_list.txt", encoding="utf-8") as fin:
    names_woz_internet_archive = list(map( lambda a: (a[0].strip(), a[1]), [l.split("\t") for l in fin.readlines()]))

with open("universal.txt", encoding="utf-8") as fin:
    names_universal = [l.strip().split("\t") for l in fin.readlines()]

with open("asimov.txt", encoding="utf-8") as fin:
    names_azimov = [l.strip().split("\t") for l in fin.readlines()]

with open("igdb.txt", encoding="utf-8") as fin:
    names_igdb = [l.strip().split("\t") for l in fin.readlines()]

with open("grouik.txt", encoding="utf-8") as fin:
    names_grouik = []
    for pair in [l.strip().split("\t") for l in fin.readlines()]:
        if len(pair) == 2:
            names_grouik.append(pair)
        else:
            print(f"Bad name {pair}")

names_azimov = names_azimov + names_grouik

for name, moby_id in tqdm(names_moby, desc="mobygames"):
    cname = canonize_curated_name(name)        #exit()
    game = Game(name, cname)
    game.set_moby_id(int(moby_id))
    all_games.append(game)

for name, igdb_id in tqdm(names_igdb, desc="igdb"):
    cname = canonize_curated_name(name)        #exit()
    game = Game(name, cname)
    game.set_igdb_id(int(igdb_id))
    all_games.append(game)

for name, crc in tqdm(sorted(names_wozaday), desc="wozaday"):
    cname = canonize_curated_name(name)
    game = Game(name, cname)
    img = DiskImage(MediaType.WOZ, int(crc, 16), path=None, url=f"http://archive.org/wozaday/{name}")
    game.add_disk_image(img)
    all_games.append(game)

for name, universal_id in tqdm(sorted(names_universal), desc="universal"):
    cname = canonize_curated_name(name)
    game = Game(name, cname)
    game.set_universal_id(universal_id)
    all_games.append(game)

filtered = []
for game in all_games:
    EXCLUDES=["microzine", "math", "basic", "copy", "algebra"]
    exclude = False
    for n in EXCLUDES:
        if n in game.canonized_name():
            exclude = True
            break
    if not exclude:
        filtered.append(game)
all_games  = filtered


# I merge the curated names first because that's where I have the least
# probability of failing.
# Merging games takes time, so we cache the results
CURATE_MERGED_NAMES = Path("curated_names.json")
if not CURATE_MERGED_NAMES.exists():
    all_games = list(merging(all_games, "Merging curated"))
    with open(CURATE_MERGED_NAMES,"w", encoding="utf-8") as fout:
        d = { "games" : [game.to_dict() for game in sorted(all_games, key=lambda g:g.canonized_name())] }
        json.dump(d, fout, indent=2)
else:
    all_games = load_game_database(CURATE_MERGED_NAMES)
print(f"There are {len(all_games)} games after merging curated names")


vocabulary = make_vocabulary(all_games)
with open("vocabulary.txt","w",encoding="utf-8") as vocab:
    for k in sorted(vocabulary.keys()):
        v = vocabulary[k]
        vocab.write(f"{k} -- {v}\n")

# for game in all_games:
#     words = [vocabulary.get(word) for word in game.canonized_name().split()]
#     if words != game.canonized_name().split():
#         print(game.canonized_name())
#         print(words)

for name, woz_crc in tqdm(names_woz_internet_archive, desc="eating WOZ i.a."):
    debug = DEBUG_WORD in remove_accents(name.lower())
    cname = canonize_dsk_name( name, vocabulary, debug)

    print(f"{name:80} {cname}")
    game = Game(None, cname)
    img = DiskImage(MediaType.WOZ, int(woz_crc, 16), Path(name), url=f"{name}")
    game.add_disk_image(img)
    all_games.append(game)


for name, dsk_crc in tqdm(names_azimov[:50000], desc="eating dsk"):
    #print(name)
    if "collections" not in name and "file_based" not in name:
        debug = DEBUG_WORD in remove_accents(name.lower())
        m = re.match(".*/(rpg|action|adventure|board|misc|simulation|sports|strategy)/(.*/)?(.*)/.*.dsk", name)

        if m:
            dsk_name = m.groups()[-1]
            dir_based = dsk_name not in set(["archon", "avalon_hill", "ancient_art_of_war", "impossible_mission", "infocom", "misc", "Dunjonquest", "beyond_wolfenstein", "chess", "frogger"])
        else:
            dir_based = False

        if dir_based:
            cname = canonize_dsk_name( dsk_name, vocabulary, debug)
        else:
            dsk_name = Path(name).name
            cname = canonize_dsk_name( dsk_name, vocabulary, debug)

        if debug:
            print(f"building with {cname}")

        game = Game(None, cname)
        img = DiskImage(MediaType.DSK, int(dsk_crc, 16), Path(name), url=f"{name.replace('/mnt/data2/roms/Apple/asi_games','https://mirrors.apple2.org.za/ftp.apple.asimov.net/images/games')}")
        game.add_disk_image(img)
        all_games.append(game)

        if debug:
            print(f"canonized: {game.canonized_name()}, original: {['','DIRECTORY'][dir_based]} |{dsk_name}| {img.name}")


with open("all_games.txt", "w", encoding="utf-8") as fout:
    for game in sorted( all_games, key=lambda g: g.canonized_name()):
        if not game.name():
            fout.write(f"{game.canonized_name():60}{game.images_names()}\n")

games_to_merge = merging(all_games)

print(f"{len(games_to_merge)}")
if DEBUG_WORD == "?":
    with open("db.json","w", encoding="utf-8") as fout:
        json_list = []
        for game in sorted(games_to_merge, key=lambda g:g.canonized_name()):
            json_list.append(game.to_dict())

        #pprint(game.to_dict(), stream=fout, sort_dicts=False)
        json.dump( {"creation-date" : datetime.now().strftime("%d %B %Y"),
                    "author" : "This file was built by Stéphane Champailler",
                    "license" : "https://creativecommons.org/licenses/by-sa/4.0/",
                    "number-of-titles" : len(games_to_merge),
                    "games" : json_list }, fout, indent=4)
    database_to_rust(load_game_database("db.json"))
else:
    for game in sorted(games_to_merge, key=lambda g:g.canonized_name()):
        pprint(game.to_dict(), sort_dicts=False)

exit()



for asi_name in sorted(  names_azimov[0:1000] ):
    ref_name = canonize_asimov_names([asi_name], [])[0]
    best_d = 10000
    best_name = None
    for name in sorted( set([game.canonized_name() for game in all_games])):
        d = distance(ref_name, name)
        if d == best_d:
            best_name += "; " + name
        elif d < best_d:
            best_d = d
            best_name = name
    print(f"{asi_name} /// {ref_name} /// {best_name}")






print("ASIMOV")
DEBUG_WORD="callisto"
#names_azimov = list(filter(lambda s: DEBUG_WORD in s.lower(), names_azimov))
for name in tqdm(names_azimov[:50000]):
    if "collections" not in name and "file_based" not in name:
        debug = DEBUG_WORD in name.lower()
        m = re.match(".*/(rpg|action|adventure|board|misc|simulation|sports|strategy)/(.*/)?(.*)/.*.dsk", name)
        if m:
            dsk_name = m.groups()[-1]
            cname = canonize_dsk_name( dsk_name, vocabulary, debug)
        else:
            dsk_name = Path(name).name
            cname = canonize_dsk_name( dsk_name, vocabulary, debug)

        game = Game(None, cname)
        img = DiskImage(MediaType.DSK, 0x10101010, Path(name), url=f"{name.replace('/mnt/data2/roms/Apple/asi_games','https://mirrors.apple2.org.za/ftp.apple.asimov.net/images/games')}")
        game.add_disk_image(img)
        all_games.append(game)

        if debug:
            print(f"canonized: {game.canonized_name()}, original: {game.disk_images[0]}")

print(f"{len(canon_names)} names")
#exit()

similar_names = dict()

for game in tqdm(all_games):
    similar = names_alike( list(similar_names.keys()), game.canonized_name())
    if not similar:
        if DEBUG_WORD in game.canonized_name():
            print("NEW ", game.canonized_name())

        similar_names[game.canonized_name()] = game
    else:
        if DEBUG_WORD in game.canonized_name():
            print("MERGE ", game.canonized_name())
        similar_names[similar].merge(game)


# for k, game in similar_names.items():
#     print(f"{k} -> {game.to_dict()}")

# print(len(similar_names))

with open("db.json","w", encoding="utf-8") as fout:
    for game in sorted(similar_names.values(), key=lambda d:d.canonized_name()):
        pprint(game.to_dict(), stream=fout, sort_dicts=False)
