import glob
import subprocess

# crc32 with all arguments on one command line is *much* faster
with open("asimov.txt","w",encoding="utf-8") as asi_list:
    files = list(sorted(glob.glob("/mnt/data2/roms/Apple/asi_games/**/*.dsk", recursive=True)))
    res = subprocess.run(["crc32"] + files, stdout=subprocess.PIPE)
    crc = res.stdout.decode("ascii").strip()
    
    for line in crc.split('\n'):
        crc, dsk = line.split("\t")
        asi_list.write(f"{dsk}\t{crc.upper()}\n")
        
