from datetime import datetime


with open("thedate.txt","w") as fout:
    txt = datetime.now().strftime('%Y-%m-%d %H:%M')
    fout.write(f"!text \"{txt}\" ; len = {len(txt)}")
