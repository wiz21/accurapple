APPLE_YRES=192
convtab = bytearray([255]*256)

"""
ALTCHRSET VID7 VID6 RA10 RA9
          $80  $40
-------------------------------------------------------
0          0    0    0    0  INVERSE control/special
0          0    1   flash 0  Flash INVERSE
0          1    0    1    0  Normal control/special
0          1    1    1    1  normal upper/lower
"""

CONTROL_SPECIAL=" "*32 + r""" !"#$%&'()*+,-./0123456789:;<=>?"""
LOWER_UPPER = """@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~"""
for ndx, c in enumerate(CONTROL_SPECIAL):
    c_ascii = ord(c)
    convtab[c_ascii] = ndx + 0x80
for ndx, c in enumerate(LOWER_UPPER):
    c_ascii = ord(c)
    convtab[c_ascii] = ndx + 0x80 + 0x40

convtab[ord("\n")] = 0x8D
# convtab[ord(' ')] = 0x20 #0x60 -> space inverse;  0x20 -> 0x60 space inverse; 0xA0 -> `
# convtab[ord('.')] = 0xAE

with open("apple2c.ct","wb") as fout:
    fout.write(convtab)


def text_address(y):
    if 0 <= y <= 7:
        ofs = 0
    elif 8 <= y <= 15:
        ofs = 0x28
    elif 16 <= y <= 23:
        ofs = 0x50
    else:
        raise Exception(f"{y}")

    i = y % 8
    return ofs + 0x80 * i

def hgr_address( y):
    #assert page == 0x2000 or page == 0x4000, "I'll work only for legal pages"
    assert 0 <= y < APPLE_YRES, "You're outside Apple's veritcal resolution"

    if 0 <= y < 64:
        ofs = 0
    elif 64 <= y < 128:
        ofs = 0x28
    else:
        ofs = 0x50

    i = (y % 64) // 8
    j = (y % 64) % 8

    return ofs + 0x80*i + 0x400*j


# print(", ".join(map(lambda n: f"{n:04X}", [text_address(i)+0x400 for i in range(24)])))
# print()
print(", ".join(map(lambda n: f"${n:04X}", [hgr_address(i)+0x2000 for i in range(192)])))
