#!/usr/bin/env python3
# dsk2nic - Apple II floppy image format converter, from DSK to NIC
# rename it to po2nic.py to convert from PO to NIC
# or run: ln -s ./dsk2nic.py po2nic.py
__version__ = '0.1'
from optparse import OptionParser
import sys, getopt, re

def main(argv=None):
  p = OptionParser(version="ver:%s" % __version__)
  if(sys.argv[0][-9:] == 'po2nic.py'):
    p.add_option('-s', '--source', dest='sourceFile', help="source (*.po) FILE", metavar="FILE")
    isPo2Nic = True
  else:
    p.add_option('-s', '--source', dest='sourceFile', help="source (*.dsk) FILE", metavar="FILE")
    isPo2Nic = False

  p.add_option('-t', '--target', dest='targetFile', help="target (*.nic) FILE", metavar="FILE")
  p.add_option('-d', '--dsk', action='store_true', dest='dsk', help="convert from .dsk file")
  p.add_option('-p', '--po', action='store_true', dest='po', help="convert from .po file")
  opts, args = p.parse_args(argv)

  if(opts.sourceFile):
    dskFilename = opts.sourceFile
  else:
    if(len(argv)>1):
      dskFilename = argv[1]
    else:
      p.print_help()
      return 1

  if(opts.targetFile):
    nicFilename = opts.targetFile
  else:
    if(opts.dsk):
        isPo2Nic = False
    if(opts.po):
        isPo2Nic = True
    if(isPo2Nic):
      if(dskFilename[-3:].upper()=="DSK"):
        print("you are converting %s as a .po file" % dskFilename, file=sys.stderr)
        return 1
      nicFilename = re.sub('\.po$', '', dskFilename, flags=re.IGNORECASE) + ".nic"
    else:
      if(dskFilename[-2:].upper()=="PO"):
        print("you are converting %s as a .dsk file" % dskFilename, file=sys.stderr)
        return 1
      nicFilename = re.sub('\.dsk$', '', dskFilename, flags=re.IGNORECASE) + ".nic"

  dskFile = open(dskFilename, mode="rb")
  nicFile = open(nicFilename, mode="wb")

  dsk2nicSectorMap = [0, 0x7, 0xe, 0x6, 0xd, 0x5, 0xc, 0x4, 0xb, 0x3, 0xa, 0x2, 0x9, 0x1, 0x8, 0xf]
  po2nicSectorMap = [0, 0x8, 0x1, 0x9, 0x2, 0xa, 0x3, 0xb, 0x4, 0xc, 0x5, 0xd, 0x6, 0xe, 0x7, 0xf]

  if(isPo2Nic):
    sectorMap = po2nicSectorMap
  else:
    sectorMap = dsk2nicSectorMap

  for track in range(35):
    for sector in range(16):
      dskFile.seek((track*16 + sectorMap[sector])*256, 0)
      dskSector = dskFile.read(256)
      for b in nicSector(254, track, sector, dskSector):
        nicFile.write(b.to_bytes(1, "big"))

  dskFile.close()
  nicFile.close()

  return 0

def nicSector(vol, trk, sctr, dskSector):
  dskSectorData = bytearray(dskSector)
  encode6And2 = [ \
    0x96, 0x97, 0x9a, 0x9b, 0x9d, 0x9e, 0x9f, 0xa6, 0xa7, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb2, 0xb3, \
    0xb4, 0xb5, 0xb6, 0xb7, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xcb, 0xcd, 0xce, 0xcf, 0xd3, \
    0xd6, 0xd7, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe5, 0xe6, 0xe7, 0xe9, 0xea, 0xeb, 0xec, \
    0xed, 0xee, 0xef, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff  ]
  flipBits1 = [ 0, 2,  1,  3  ]
  flipBits2 = [ 0, 8,  4,  12 ]
  flipBits3 = [ 0, 32, 16, 48 ]
  nicData = []

# preamble gap
  nicData.extend([0xff]*22)
# sync bytes
  nicData.extend([0x03, 0xfc, 0xff, 0x3f, 0xcf, 0xf3, 0xfc, 0xff, 0x3f, 0xcf, 0xf3, 0xfc])
# address field
  nicData.extend([0xd5, 0xaa, 0x96])
  checksum = vol^trk^sctr
  nicData.extend([((vol>>1)|0xaa)&0xff, (vol|0xaa)&0xff, ((trk>>1)|0xaa)&0xff, (trk|0xaa)&0xff, \
                  ((sctr>>1)|0xaa)&0xff, (sctr|0xaa)&0xff, ((checksum>>1)|0xaa)&0xff, (checksum|0xaa)&0xff])
  nicData.extend([0xde, 0xaa, 0xeb])
# gap between address and data
  nicData.extend([0xff]*5)
# data field
  nicData.extend([0xd5, 0xaa, 0xad])
  checksum = 0
  data0 = 0
  dskSectorData.extend([0, 0])
  nicData.extend([0]*343)
  for i in range(86):
    data = flipBits1[dskSectorData[i]&3] | flipBits2[dskSectorData[i+86]&3] | flipBits3[dskSectorData[i+172]&3]
    data &= 0xff
    nicData[0x38+i] = encode6And2[data0^data]
    data0 = data
  for i in range(256):
    data = dskSectorData[i]>>2
    data &= 0xff
    nicData[0x38+0x56+i] = encode6And2[data^data0]
    data0 = data
  nicData[0x38+0x56+0x100] = encode6And2[data]
  nicData.extend([0xde, 0xaa, 0xeb])
# ending gap
  nicData.extend([0xff]*14)
# padding to 512 bytes
  nicData.extend([0]*96)
# write a nic sector (512 bytes)
  return nicData

if __name__ == "__main__":
  sys.exit(main(sys.argv))
