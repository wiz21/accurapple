CYCLES_PER_FRAME = 20280

NOP_OPCODE = $EA

LETTER_SPACE = $A0

KEY_RIGHT	= $15
KEY_LEFT	= $8
KEY_ESC		= $1B
KEY_DOWN	= $A
KEY_UP		= $B


AUXMOVE = $C311
A1L = $3C
A1H = $3D
A2L = $3E
A2H = $3F
A4L = $42
A4H = $43
CH              = $24
CV              = $25
OURCH        =     $57b
OURCV        =     $5fb
VTAB=$FC22
;; See http://www.applelogic.org/files/AIIMP.pdf (A2 monitor peeled)

COUT = $FDED
COUT1 = $FDF0
COUTZ = $FDF6


STORE80OFF    =      $C000			 ; (W)
STORE80ON     =      $C001			 ; (W)
RAMRDOFF      =      $c002           ; turn off RAMRD
RAMRDON       =      $c003           ; turn on RAMRD
RAMWRTOFF     =      $c004           ; turn off RAMWRT
RAMWRTON      =      $c005           ; turn on RAMWRT
VERTBLANK     =      $c019           ; vertical blank
CLR80COL      =      $c000           ; disable 80 column store
KBD           =      $c000           ; read keyboard
KBDSTRB       =      $c010           ; clear keyboard strobe
VBLINTON      =      $c05b           ; enable VBL interrupt
VSYNCIRF      =      $c070           ; reset VSYNC IRF (Interrupt Flag)
IOUOFF        =      $c078           ; disable IOU access
IOUON         =      $c079           ; enable IOU access
RAMRW_D000    =      $c08b           ; read and write RAM in $d000-$ffff

; ** Screen soft switches **
TEXTOFF   =   $C050
TEXTON    =   $C051
MIXEDOFF  =   $C052
MIXEDON   =   $C053
PAGE1     =   $C054
PAGE2     =   $C055
LORES     =   $C056
HIRES     =   $C057
COL80ON   =   $C00D           ; (W) turn on 80 columns
COL80OFF  =   $C00C	      ; (W)
ALTCHARSETOFF    =   $c00e	     ; (W)
ALTCHARSETON    =    $c00f	     ; (W)
AN3OFF    =   $C05E           ; enable double-width graphics
AN3ON     =   $C05F           ; disable double-width graphics

	JMP skip_utils

!macro wait_redraw {
	JMP .skipper
	!align 255, 0, NOP_OPCODE ; fill with NOP_OPCODE until (* & and_value(255) ) == equal_value(0)
.fine_wait:
	N = CYCLES_PER_FRAME - 1 - 6 - 3 - 1 - 2
	LDA # (N - 34) >> 8	; 2
	LDX # (N - 34) & 255	; 2
	JMP delay_256a_x_33_clocks
.skipper:

	;; The general idea is this:
	;; Detectct the start of the drawing area. When done
	;; a few cycles have passed so we're inside the drawing area.
	;; Now, we wait a little less than the duration of a full
	;; frame so that we end up a bit "higher" on the screen, we do
	;; that with a big step then with 1 cycle steps.
	;; We continue until we're back in the VBL area, just above the
	;; drawing area. Doing it like that allows us to be on the
	;; very cycle where the frame draw start.

.sd1:	lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
        bpl     .sd1              ;   IIe : wait for VBLANK start
        lda     VSYNCIRF        ; IIc : reset VSYNC IRF
.sd2:	lda     VERTBLANK       ; IIe : wait for VBLANK end
        bmi     .sd2
.sd3:	lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
        bpl     .sd3              ;   IIe : wait for VBLANK start


	;; Returning from JSR is 6 cycles
	N2 = CYCLES_PER_FRAME - 6 - 4
	LDA # (N2 - 34) >> 8	; 2
	LDX # (N2 - 34) & 255	; 2
	JSR delay_256a_x_33_clocks		; 3

	;; at this point we're in the drawing zone

	!for X,1,16 {
	;; If no branch, This will have the total effect of waiting
	;; CYCLES_PER_FRAME minus one cycle.
	lda     VERTBLANK       ; 4
	;; A=$80=Drawing; A=0=VBL
        bpl     .zed		; 2 or 3 branch if in the VBL part
	JSR .fine_wait
	}

.zed:
	+wait_cycles2 20280-3	; eats the last BPL to put us on the very first HBL pixel.
}

!macro wait_cycles .N {
;; DEPRECATED use short_wait
	LDA # (.N - 36) >> 8
	LDX # (.N - 36) & 255
	JSR delay_256a_x_33_clocks
}

!macro wait_cycles2 .N {
	!if .N < 38 {
		!error "Must wait at least 38 cycles !"
	}
	; We wait N cycles.
	; Loading the timer parameters with LDA and LDX
	; takes 4 cycles.
	; The delay routine will wait A*256 + X + 33 cycles
	; => A*256 + X + 33 + 4 = N
	; => A*256 + X = N - (33 + 4)
	LDA # (.N - 33 - 4) >> 8
	LDX # (.N - 33 - 4) & 255
	JSR delay_256a_x_33_clocks
}

!macro short_wait .N {
	!if .N = 0 {
		; do nothing
	}
	!if .N = 1 or .N < 0 {
		!error "Can't wait 1 or -N cycles !"
	}
	!if .N >= 38 {
		+wait_cycles2 .N
	}
	!if .N < 38 and .N > 10 {
		!if .N MOD 10 != 1 {
			!for i, 1, .N DIV 10 {
				+short_wait 10
			}
			+short_wait .N MOD 10
		} else {
			+short_wait .N - 2
			+short_wait 2
		}
	}
	!if .N = 2 {
		NOP
	}
	!if .N = 3 {
		JMP .next
.next:
	}
	!if .N = 4 {
		NOP
		NOP
	}
	!if .N = 5 {
JMP +
+
NOP
	}
	!if .N = 6 {
		NOP	; 2
		NOP	; 2
		NOP	; 2
	}
	!if .N = 7 {
		JMP +		; 3
+
		NOP		; 2
		NOP		; 2
	}
	!if .N = 8 {
		STA $100
		STA $100
	}
	!if .N = 9 {
		STA $100
		JMP +
+
		NOP
	}
	!if .N = 10 {
		STA $100
		STA $100
		NOP
	}
}

!macro RROL {
	PHA
	ROL
	PLA
	ROL
}

!macro RROR {
	PHA
	ROR
	PLA
	ROR
}

page_copy	!zone {
	;; A = page source
	;; Y = page dest
	;; X = nb pages to copy
	STA .smc_page_source
	STY .smc_page_dest
	LDY #0
.copy_loop_page:
.copy_loop:
	.smc_page_source = * + 2
	LDA $AA00,Y
	.smc_page_dest = * + 2
	STA $AA00,Y
	INY
	BNE .copy_loop
	INC .smc_page_source
	INC .smc_page_dest
	DEX
	BNE .copy_loop_page
	RTS
}


page_fill	!zone {
	;; A = value
	;; Y = page dest
	;; X = nb pages to fill
	STY .smc_page_dest
	LDY #0
.copy_loop_page:
.copy_loop:
	.smc_page_dest = * + 2
	STA $AA00,Y
	INY
	BNE .copy_loop
	INC .smc_page_dest
	DEX
	BNE .copy_loop_page
	RTS
}


; ----------------------------------------------------------------------------
; enable_vbl
; ----------------------------------------------------------------------------
enable_vbl:
        sei                     ; disable interrupts
        bit     IOUON
        bit     VBLINTON
        bit     IOUOFF
        lda     VSYNCIRF
        rts


; ----------------------------------------------------------------------------
; switch_display
; ----------------------------------------------------------------------------
switch_display:
sd1:	lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
        bpl     sd1              ;   IIe : wait for VBLANK start
        lda     VSYNCIRF        ; IIc : reset VSYNC IRF
sd2:	lda     VERTBLANK       ; IIe : wait for VBLANK end
        bmi     sd2
sd3:	lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
        bpl     sd3              ;   IIe : wait for VBLANK start

	;; once here, we're close to the drawn area of the screen
        rts


text_lines_ofs !word $0400, $0480, $0500, $0580, $0600, $0680, $0700, $0780, $0428, $04A8, $0528, $05A8, $0628, $06A8, $0728, $07A8, $0450, $04D0, $0550, $05D0, $0650, $06D0, $0750, $07D0

hgr_addr:
	!word $2000, $2400, $2800, $2C00, $3000, $3400, $3800, $3C00, $2080, $2480, $2880, $2C80, $3080, $3480, $3880, $3C80, $2100, $2500, $2900, $2D00, $3100, $3500, $3900, $3D00, $2180, $2580, $2980, $2D80, $3180, $3580, $3980, $3D80, $2200, $2600, $2A00, $2E00, $3200, $3600, $3A00, $3E00, $2280, $2680, $2A80, $2E80, $3280, $3680, $3A80, $3E80, $2300, $2700, $2B00, $2F00, $3300, $3700, $3B00, $3F00, $2380, $2780, $2B80, $2F80, $3380, $3780, $3B80, $3F80, $2028, $2428, $2828, $2C28, $3028, $3428, $3828, $3C28, $20A8, $24A8, $28A8, $2CA8, $30A8, $34A8, $38A8, $3CA8, $2128, $2528, $2928, $2D28, $3128, $3528, $3928, $3D28, $21A8, $25A8, $29A8, $2DA8, $31A8, $35A8, $39A8, $3DA8, $2228, $2628, $2A28, $2E28, $3228, $3628, $3A28, $3E28, $22A8, $26A8, $2AA8, $2EA8, $32A8, $36A8, $3AA8, $3EA8, $2328, $2728, $2B28, $2F28, $3328, $3728, $3B28, $3F28, $23A8, $27A8, $2BA8, $2FA8, $33A8, $37A8, $3BA8, $3FA8, $2050, $2450, $2850, $2C50, $3050, $3450, $3850, $3C50, $20D0, $24D0, $28D0, $2CD0, $30D0, $34D0, $38D0, $3CD0, $2150, $2550, $2950, $2D50, $3150, $3550, $3950, $3D50, $21D0, $25D0, $29D0, $2DD0, $31D0, $35D0, $39D0, $3DD0, $2250, $2650, $2A50, $2E50, $3250, $3650, $3A50, $3E50, $22D0, $26D0, $2AD0, $2ED0, $32D0, $36D0, $3AD0, $3ED0, $2350, $2750, $2B50, $2F50, $3350, $3750, $3B50, $3F50, $23D0, $27D0, $2BD0, $2FD0, $33D0, $37D0, $3BD0, $3FD0

waiter !zone {
	!align 255, 0
	;;;;;;;;;;;;;;;;;;;;;;;;
	;; Delays A:X clocks+overhead
	;; Time: 256*A+X+33 clocks (including JSR)
	;; Clobbers A. Preserves X,Y. Has relocations.
	;;;;;;;;;;;;;;;;;;;;;;;;
	;;  https://www.nesdev.org/wiki/Delay_code#256%C3%97A_+_X_+_33_cycles_of_delay,_clobbers_A,_Z&N,_C,_V

.wloop1:	; 5 cycles done, do 256-5 more.
	sbc #1			; 2 cycles - Carry was set from cmp
	pha                     ; 3
	lda #(256-27 - 16)     ; 2
	jsr .delay_a_27_clocks  ; 240
	pla                     ; 4
delay_256a_x_33_clocks:
	cmp #1			; +2
	bcs .wloop1			; +3
	; 0-255 cycles remain, overhead = 4
	txa 			; -1+2; 6; +27 = 33

.delay_a_27_clocks:
        sec
.L:     	sbc #5
        bcs .L  ;  6 6 6 6 6  FB FC FD FE FF
        adc #3  ;  2 2 2 2 2  FE FF 00 01 02
        bcc .L4  ;  3 3 2 2 2  FE FF 00 01 02
        lsr     ;  - - 2 2 2  -- -- 00 00 01
        beq .L5  ;  - - 3 3 2  -- -- 00 00 01
.L4:     lsr     ;  2 2 - - 2  7F 7F -- -- 00
.L5:     bcs .L6  ;  2 3 2 3 2  7F 7F 00 00 00
.L6:     rts     ;
}


!macro set_cursor_at .x, .y {
	LDA #.x
	STA CH
	STA OURCH
	LDA #.y
	STA CV
	STA OURCV
	TABV = $fb5b
	JSR TABV
}


!macro print_text_at .txt, .x, .y {
	+set_cursor_at .x, .y
	LDA #<.txt
	LDY #>.txt
	JSR print_para

}

!macro print_one_text_at .txt, .x, .y {
	JMP .skip
.internal_text:
	!text .txt
	!byte 0
.skip:
	+set_cursor_at .x, .y
	LDA #<.internal_text
	LDY #>.internal_text
	JSR print_para
}

!macro print_one_text .txt {
	JMP .skip
.internal_text:
	!text .txt
	!byte 0
.skip:
	LDA #<.internal_text
	LDY #>.internal_text
	JSR print_para
}

!macro switch_txt40 {
    STA TEXTON ; 4C
    STA LORES   ; 4
    STA COL80OFF
    STA AN3ON
}

!macro switch_txt80 {
    STA TEXTON ; 4C
    STA LORES   ; 4
    STA COL80ON
    STA AN3ON
}

!macro switch_gr {
    STA TEXTOFF ; 4C
    STA LORES   ; 4
    STA COL80OFF
    STA AN3ON
}

!macro switch_hgr {
    STA TEXTOFF ; 4C
    STA HIRES   ; 4
    STA COL80OFF
    STA AN3ON
}

!macro switch_hgr_col80 {
    STA TEXTOFF ; 4C
    STA HIRES   ; 4
    STA COL80ON
    STA AN3ON
}

!macro switch_hgr_undelayed {
    STA TEXTOFF ; 4C
    STA HIRES   ; 4
    STA COL80OFF
    STA AN3OFF
}

!macro switch_dgr {
    STA TEXTOFF ; 4C
    STA LORES   ; 4
    STA COL80ON
    STA AN3OFF
}

!macro switch_dhgr {
    STA TEXTOFF ; 4C
    STA HIRES   ; 4
    STA COL80ON	; 4
    STA AN3OFF	; 4
    ;; LDA PAGE1
}


print_para	!zone {
	;; print text at Y:A
	;; LDA #<.hello2
	;; LDY #>.hello2
	;; Text must be 0 terminated. \n will do cr/lf

	STA .smc12 + 1
	STY .smc12 + 2
.loop:
.smc12:
	LDA $1000
	BEQ .done2
	JSR COUT ; 0x8D is \n
	;JSR COUT1
	CLC
	LDA .smc12+1
	ADC #1
	STA .smc12+1
	LDA .smc12+2
	ADC #0
	STA .smc12+2
	JMP .loop
.done2:
	RTS
}

print_80col !zone {
	;; print text at Y:A in 80COL memory layout
	;; I have made this function to be able to mix writing to TXT40 and TXT80.
	;; If one uses the firmware for that, it tends to convert TXT40 to 80 when switching
	;; firmare to/from 80 cols which makes it impossible to mix TXT40 and 80 on the same page.

	;; LDA #<.hello2
	;; LDY #>.hello2
	;; Text must be 0 terminated.
	;; Will go wrong if length of text is more than 80.

	STA .smc12 + 1
	STY .smc12 + 2

	LDA CV
	CLC
	ROL
	TAY
	CLC
	LDA text_lines_ofs, Y
	ADC CH
	STA .smc3 + 1
	LDA text_lines_ofs+1, Y
	ADC #0
	STA .smc3 + 2

	STA STORE80ON		; 80STORE set
	LDA HIRES		; HIRES set

	LDY #0
	LDX #$FF

.loop:
	INX
.smc12 LDA $ABCD, X
	BNE .do_print
	STA STORE80OFF		; 80STORE set
	LDA PAGE1
	RTS

.do_print:
	PHA
	TXA
	AND #1
	BNE .page2
	LDA PAGE2		; selects RAM
	JMP .page_set
.page2
	LDA PAGE1		; selects AUXRAM
.page_set
	PLA

.smc3 STA $1234, Y

	TXA
	AND #1
	BEQ .loop

	INY
	JMP .loop

	RTS
}


             ;ldx   #22
             ;ldy   #5
             ;jsr   gotoXY

             ;ldx   #>mySTRING
             ;ldy   #<mySTRING
             ;jsr   printSTRING
             ;rts


plot_dhgr !zone {
    .POINTER = $FE
	;; .FIRST_LINE = 10
	;; .LAST_LINE = .FIRST_LINE + 4

    STA .line
    STX .end_line_smc + 1
    STA STORE80ON		; 80STORE set
    LDA HIRES

.loop_line:
    LDA .line
	CMP #128
	BCC .small
	LDX #>(hgr_addr+256)
	STX .smc11 + 2
	STX .smc12 + 2
.small:
    CLC
    ROL
    TAX
.smc11    LDA hgr_addr,x
    STA .POINTER
    INX
.smc12    LDA hgr_addr,x
    STA .POINTER + 1

    LDA #0
    STA .counter
    LDA #1
    STA .color_counter
.loop_fill_line:
    LDA .color_counter
    AND #15
    STA .color
    ASL
    ASL
    ASL
    ASL
    ORA .color
    +RROR
    STA .color
    INC .color_counter

    LDX PAGE2           ; Selects AUX
    LDY .counter
    STA (.POINTER),Y
    +RROL
    LDX PAGE1           ; Selects RAM
    STA (.POINTER),Y

    INC .counter

    LDX PAGE2           ; Selects RAM
    LDY .counter
    LDA .color
    +RROL
    +RROL
    STA (.POINTER),Y
    LDX PAGE1           ; Selects AUX
    LDA #255
    STA (.POINTER),Y

    INC .counter


    LDA .counter
    CMP #32
    BMI .loop_fill_line

    INC .line
    LDA .line
.end_line_smc:
    CMP #0
    BEQ .done
	JMP .loop_line
.done:
    STX STORE80OFF
    LDY PAGE1
    RTS

.counter !byte 0
.color_counter !byte 0
.color !byte 0
.line !byte 0
.masked_counter !byte 0
}

clear_video_ram !zone {
    STA STORE80ON		; 80STORE set
    LDA HIRES
    BIT PAGE1
    LDA #0
    LDY #$04
    LDX #4
    JSR page_fill
    LDA #0
    LDY #$08
    LDX #4
    JSR page_fill

    BIT PAGE2
    LDA #0
    LDY #$04
    LDX #4
    JSR page_fill
    LDA #0
    LDY #$08
    LDX #4
    JSR page_fill

    BIT PAGE1
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

    BIT PAGE2
    LDA #0
    LDY #$20
    LDX #32
    JSR page_fill

    BIT PAGE1
	RTS
}

clear_text_ram !zone {
	LDA #LETTER_SPACE
	LDY #$04
	LDX #8
	JSR page_fill

	LDA #LETTER_SPACE
	LDY #$04
	LDX #8
	JSR page_fill

	STA STORE80ON		; 80STORE set
	LDA HIRES
	BIT PAGE2
	LDA #LETTER_SPACE
	LDY #$04
	LDX #8
	JSR page_fill

	BIT PAGE1
	LDA #LETTER_SPACE
	LDY #$04
	LDX #8
	JSR page_fill

	+switch_txt40
	RTS
}


!macro print_byte .OFFSET {
	;; value is in X
	;; destination is OFFSET
	LDA #<.OFFSET
	STA $FC
	LDA #>.OFFSET
	STA $FD
	JSR print_byte_in_X
}

print_byte_in_X !zone {
	;; Prints X in addr located at what's on the stack
	TXA
	PHA
	CLC
	ROL
	ROL
	ROL
	ROL
	ROL
	AND #$F
	TAX
	LDA chars, X
	LDY #0
	STA ($FC),Y

	PLA
	AND #$F
	TAX
	LDA chars, X
	LDY #1
	STA ($FC),Y

	RTS
chars !text "0123456789ABCDEF"
}

!macro inc16 .offset {
	INC .offset 		; LSB
	BNE .done
	INC .offset + 1
.done:
	}

!macro add_const16 .offset, .const {
	CLC
	LDA .offset
	ADC #<.const
	STA .offset
	LDA .offset + 1
	ADC #>.const
	STA .offset + 1
}

!macro sub_const16 .offset, .const {
	SEC
	LDA .offset
	SBC #<.const
	STA .offset
	LDA .offset + 1
	SBC #>.const
	STA .offset + 1
}

skip_utils:
